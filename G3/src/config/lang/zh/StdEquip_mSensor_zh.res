﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
zh

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			mSensor					mSensor				mSensor				mSensor
3		32			15			Comm state				Comm state			通信状态			通信状态
4		32			15			Existence State			Existence State		是否存在				是否存在
5		32			15			Yes						Yes					是					是
6		32			15			Communication Fail		Comm Fail			通讯中断				通讯中断
7		32			15			Existent				Existent			存在					存在
8		32			15			Comm OK					Comm OK				通讯正常				通讯正常

11		32			15			Voltage DC Chan 1		Voltage DC Chan1		直流电压1		直流电压1
12		32			15			Voltage DC Chan 2		Voltage DC Chan2		直流电压2		直流电压2
13		32			15			Post Temp Chan 1		Post Temp Chan1		温度1			温度1
14		32			15			Post Temp Chan 2		Post Temp Chan2		温度2			温度2
15		32			15			AC Ripple Chan 1		AC Ripple Chan1		交流波动1		交流波动1
16		32			15			AC Ripple Chan 2		AC Ripple Chan2		交流波动2		交流波动2
17		32			15			Impedance Chan 1		Impedance Chan1		阻抗1			阻抗1
18		32			15			Impedance Chan 2		Impedance Chan2		阻抗2			阻抗2
19		32			15			DC Volt Status1			DC Volt Status1			直流电压状态1	直流电压状态1
20		32			15			Post Temp Status1		Post Temp Status1		温度状态1		温度状态1
21		32			15			Impedance Status1		Impedance Status1		阻抗状态1		阻抗状态1
22		32			15			DC Volt Status2			DC Volt Status2			直流电压状态2	直流电压状态2
23		32			15			Post Temp Status2		Post Temp Status2		温度状态2		温度状态2
24		32			15			Impedance Status2		Impedance Status2		阻抗状态2		阻抗状态2


25		32			15				DC Volt1No Value					DCVolt1NoValue				电压1无值					电压1无值	
26		32			15				DC Volt1Invalid						DCVolt1Invalid				电压1无效					电压1无效	
27		32			15				DC Volt1Busy						DCVolt1Busy					电压1忙						电压1忙		
28		32			15				DC Volt1Out of Range				DCVolt1OutRange				电压1错误					电压1错误	
29		32			15				DC Volt1Over Range					DCVolt1OverRange			电压1过高					电压1过高	
30		32			15				DC Volt1Under Range					DCVolt1UndRange				电压1过低					电压1过低	
31		32			15				DC Volt1Volt Supply Issue			DCVolt1SupplyIss			电压1供电故障				电压1供电故障
32		32			15				DC Volt1Module Harness				DCVolt1Harness				电压1模块故障				电压1模块故障
33		32			15				DC Volt1Low							DCVolt1Low					电压1低						电压1低		
34		32			15				DC Volt1High						DCVolt1High					电压1高						电压1高		
35		32			15				DC Volt1Too Hot						DCVolt1Too Hot				电压1过热					电压1过热	
36		32			15				DC Volt1Too Cold					DCVolt1Too Cold				电压1过冷					电压1过冷	
37		32			15				DC Volt1Calibration Err				DCVolt1CalibrErr			电压1校准告警				电压1校准告警
38		32			15				DC Volt1Calibration Overflow		DCVolt1CalibrOF				电压1校准溢出				电压1校准溢出
39		32			15				DC Volt1Not Used					DCVolt1Not Used				电压1未用					电压1未用	
40		32			15				Temp1No Value						Temp1 NoValue				温度1无值					温度1无值	
41		32			15				Temp1Invalid						Temp1 Invalid				温度1无效					温度1无效	
42		32			15				Temp1Busy							Temp1 Busy					温度1忙						温度1忙		
43		32			15				Temp1Out of Range					Temp1 OutRange				温度1错误					温度1错误	
44		32			15				Temp1Over Range						Temp1OverRange				温度1过高					温度1过高	
45		32			15				Temp1Under Range					Temp1UndRange				温度1过低					温度1过低	
46		32			15				Temp1Volt Supply					Temp1SupplyIss				温度1供电故障				温度1供电故障
47		32			15				Temp1Module Harness					Temp1Harness				温度1模块故障				温度1模块故障
48		32			15				Temp1Low							Temp1 Low					温度1低						温度1低		
49		32			15				Temp1High							Temp1 High					温度1高						温度1高		
50		32			15				Temp1Too Hot						Temp1 Too Hot				温度1过热					温度1过热	
51		32			15				Temp1Too Cold						Temp1 Too Cold				温度1过冷					温度1过冷	
52		32			15				Temp1Calibration Err				Temp1 CalibrErr				温度1校准告警				温度1校准告警
53		32			15				Temp1Calibration Overflow			Temp1 CalibrOF				温度1校准溢出				温度1校准溢出
54		32			15				Temp1Not Used						Temp1 Not Used				温度1未用					温度1未用	
55		32			15				Impedance1No Value					Imped1NoValue				阻抗1无值					阻抗1无值	
56		32			15				Impedance1Invalid					Imped1Invalid				阻抗1无效					阻抗1无效	
57		32			15				Impedance1Busy						Imped1Busy					阻抗1忙						阻抗1忙		
58		32			15				Impedance1Out of Range				Imped1OutRange				阻抗1错误					阻抗1错误	
59		32			15				Impedance1Over Range				Imped1OverRange				阻抗1过高					阻抗1过高	
60		32			15				Impedance1Under Range				Imped1UndRange				阻抗1过低					阻抗1过低	
61		32			15				Impedance1Volt Supply				Imped1SupplyIss				阻抗1供电故障				阻抗1供电故障
62		32			15				Impedance1Module Harness			Imped1Harness				阻抗1模块故障				阻抗1模块故障
63		32			15				Impedance1Low						Imped1Low					阻抗1低						阻抗1低		
64		32			15				Impedance1High						Imped1High					阻抗1高						阻抗1高		
65		32			15				Impedance1Too Hot					Imped1Too Hot				阻抗1过热					阻抗1过热	
66		32			15				Impedance1Too Cold					Imped1Too Cold				阻抗1过冷					阻抗1过冷	
67		32			15				Impedance1Calibration Err			Imped1CalibrErr				阻抗1校准告警				阻抗1校准告警
68		32			15				Impedance1Calibration Overflow		Imped1CalibrOF				阻抗1校准溢出				阻抗1校准溢出
69		32			15				Impedance1Not Used					Imped1Not Used				阻抗1未用					阻抗1未用	

70		32			15				DC Volt2No Value					DCVolt2NoValue				电压2无值					电压2无值	
71		32			15				DC Volt2Invalid						DCVolt2Invalid				电压2无效					电压2无效	
72		32			15				DC Volt2Busy						DCVolt2Busy					电压2忙						电压2忙		
73		32			15				DC Volt2Out of Range				DCVolt2OutRange				电压2错误					电压2错误	
74		32			15				DC Volt2Over Range					DCVolt2OverRange			电压2过高					电压2过高	
75		32			15				DC Volt2Under Range					DCVolt2UndRange				电压2过低					电压2过低	
76		32			15				DC Volt2Volt Supply Issue			DCVolt2SupplyIss			电压2供电故障				电压2供电故障
77		32			15				DC Volt2Module Harness				DCVolt2Harness				电压2模块故障				电压2模块故障
78		32			15				DC Volt2Low							DCVolt2Low					电压2低						电压2低		
79		32			15				DC Volt2High						DCVolt2High					电压2高						电压2高		
80		32			15				DC Volt2Too Hot						DCVolt2Too Hot				电压2过热					电压2过热	
81		32			15				DC Volt2Too Cold					DCVolt2Too Cold				电压2过冷					电压2过冷	
82		32			15				DC Volt2Calibration Err				DCVolt2CalibrErr			电压2校准告警				电压2校准告警
83		32			15				DC Volt2Calibration Overflow		DCVolt2CalibrOF				电压2校准溢出				电压2校准溢出
84		32			15				DC Volt2Not Used					DCVolt2Not Used				电压2未用					电压2未用	
85		32			15				Temp2No Value						Temp2 NoValue				温度2无值					温度2无值	
86		32			15				Temp2Invalid						Temp2 Invalid				温度2无效					温度2无效	
87		32			15				Temp2Busy							Temp2 Busy					温度2忙						温度2忙		
88		32			15				Temp2Out of Range					Temp2 OutRange				温度2错误					温度2错误	
89		32			15				Temp2Over Range						Temp2OverRange				温度2过高					温度2过高	
90		32			15				Temp2Under Range					Temp2UndRange				温度2过低					温度2过低	
91		32			15				Temp2Volt Supply					Temp2SupplyIss				温度2供电故障				温度2供电故障
92		32			15				Temp2Module Harness					Temp2Harness				温度2模块故障				温度2模块故障
93		32			15				Temp2Low							Temp2 Low					温度2低						温度2低		
94		32			15				Temp2High							Temp2 High					温度2高						温度2高		
95		32			15				Temp2Too Hot						Temp2 Too Hot				温度2过热					温度2过热	
96		32			15				Temp2Too Cold						Temp2 Too Cold				温度2过冷					温度2过冷	
97		32			15				Temp2Calibration Err				Temp2 CalibrErr				温度2校准告警				温度2校准告警
98		32			15				Temp2Calibration Overflow			Temp2 CalibrOF				温度2校准溢出				温度2校准溢出
99		32			15				Temp2Not Used						Temp2 Not Used				温度2未用					温度2未用	
100		32			15				Impedance2No Value					Imped2NoValue				阻抗2无值					阻抗2无值	
101		32			15				Impedance2Invalid					Imped2Invalid				阻抗2无效					阻抗2无效	
103		32			15				Impedance2Busy						Imped2Busy					阻抗2忙						阻抗2忙		
104		32			15				Impedance2Out of Range				Imped2OutRange				阻抗2错误					阻抗2错误	
105		32			15				Impedance2Over Range				Imped2OverRange				阻抗2过高					阻抗2过高	
106		32			15				Impedance2Under Range				Imped2UndRange				阻抗2过低					阻抗2过低	
107		32			15				Impedance2Volt Supply				Imped2SupplyIss				阻抗2供电故障				阻抗2供电故障
108		32			15				Impedance2Module Harness			Imped2Harness				阻抗2模块故障				阻抗2模块故障
109		32			15				Impedance2Low						Imped2Low					阻抗2低						阻抗2低		
110		32			15				Impedance2High						Imped2High					阻抗2高						阻抗2高		
111		32			15				Impedance2Too Hot					Imped2Too Hot				阻抗2过热					阻抗2过热	
112		32			15				Impedance2Too Cold					Imped2Too Cold				阻抗2过冷					阻抗2过冷	
113		32			15				Impedance2Calibration Err			Imped2CalibrErr				阻抗2校准告警				阻抗2校准告警
114		32			15				Impedance2Calibration Overflow		Imped2CalibrOF				阻抗2校准溢出				阻抗2校准溢出
115		32			15				Impedance2Not Used					Imped2Not Used				阻抗2未用					阻抗2未用	
