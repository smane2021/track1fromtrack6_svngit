﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
zh

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			EIB-4				EIB-4			EIB-4			EIB-4
9		32			15			Bad Battery Block		Bad Batt Block		电池块异常		电池块异常
10		32			15			Load 1				Load 1			负载电流1		负载电流1
11		32			15			Load 2				Load 2			负载电流2		负载电流2
12		32			15			Relay Output 9				Relay Output 9		继电器9					继电器9
13		32			15			Relay Output 10				Relay Output 10		继电器10				继电器10
14		32			15			Relay Output 11				Relay Output 11		继电器11				继电器11
15		32			15			Relay Output 12				Relay Output 12		继电器12				继电器12
16		32			15			EIB Communication Fail			EIB Comm Fail		EIB通信中断				EIB通信中断
17		32			15			State					State			State					State
18		32			15			Shunt 2 Full Current			Shunt 2 Current		分流器2额定电流				分流器2额定电流
19		32			15			Shunt 3 Full Current			Shunt 3 Current		分流器3额定电流				分流器3额定电流
20		32			15			Shunt 2 Full Voltage			Shunt 2 Voltage		分流器2额定电压				分流器2额定电压
21		32			15			Shunt 3 Full Voltage			Shunt 3 Voltage		分流器3额定电压				分流器3额定电压
22		32			15			Load 1					Load 1			负载1使能				负载1使能
23		32			15			Load 2					Load 2			负载2使能				负载2使能
24		32			15			Enabled					Enabled			允许					允许
25		32			15			Disabled				Disabled		不允许					不允许
26		32			15			Active				Active		闭合			闭合
27		32			15			Not Active			Not Active			断开			断开                   
28		32			15			State					State			State					State
29		32			15			No					No			否					否
30		32			15			Yes					Yes			是					是
31		32			15			EIB Communication Fail			EIB Comm Fail		通信中断				通信中断
32		32			15			Voltage 1				Voltage 1		电压1					电压1
33		32			15			Voltage 2				Voltage 2		电压2					电压2
34		32			15			Voltage 3				Voltage 3		电压3					电压3
35		32			15			Voltage 4				Voltage 4		电压4					电压4
36		32			15			Voltage 5				Voltage 5		电压5					电压5
37		32			15			Voltage 6				Voltage 6		电压6					电压6
38		32			15			Voltage 7				Voltage 7		电压7					电压7
39		32			15			Voltage 8				Voltage 8		电压8					电压8
40		32			15			Number of Battery Shunts		Num Batt Shunts		电池数					电池数
41		32			15			0					0			0					0
42		32			15			1					1			1					1
43		32			15			2					2			2					2
44		32			15			Load 3				Load 3			负载电流3		负载电流3
45		32			15			3					3			3					3
46		32			15			Number of Load Shunts			Num Load Shunts		负载数					负载数
47		32			15			Shunt 1 Full Current			Shunt 1 Current		分流器1额定电流				分流器1额定电流
48		32			15			Shunt 1 Full Voltage			Shunt 1 Voltage		分流器1额定电压				分流器1额定电压
49		32			15			Voltage Type				Voltage Type		电压类型				电压类型
50		32			15			48 (Block4)				48 (Block4)		48(Block4)				48(Block4)
51		32			15			Mid Point				Mid Point		Mid point				Mid point
52		32			15			24 (Block2)				24 (Block2)		24(Block2)				24(Block2)
53		32			15			Block Voltage Diff (12V)		Blk V Diff(12V)		Block电压差(12V)			Block电压差(12V)
54		32			15			Relay Output 13				Relay Output 13		继电器13				继电器13
55		32			15			Block Voltage Diff (Mid)		Blk V Diff(Mid)		Block电压差(Mid)			Block电压差(Mid)
56		32			15			Block In-Use Num			Block In-Use		Block块数				Block块数
78		32			15			Relay 9 Test				Relay 9 Test		测试继电器9				测试继电器9
79		32			15			Relay 10 Test				Relay 10 Test		测试继电器10				测试继电器10
80		32			15			Relay 11 Test				Relay 11 Test		测试继电器11				测试继电器11
81		32			15			Relay 12 Test				Relay 12 Test		测试继电器12				测试继电器12
82		32			15			Relay 13 Test				Relay 13 Test		测试继电器13				测试继电器13
83		32			15			Not Used				Not Used		不使用					不使用
84		32			15			General					General			通用					通用
85		32			15			Load					Load			负载					负载
86		32			15			Battery					Battery			电池					电池
87		32			15			Shunt1 Set As				Shunt1SetAs		分流器1设置为				分流器1设置为
88		32			15			Shunt2 Set As				Shunt2SetAs		分流器2设置为				分流器2设置为
89		32			15			Shunt3 Set As				Shunt3SetAs		分流器3设置为				分流器3设置为
90		32			15			Shunt 1		Shunt 1		分流器1读数				分流器1读数
91		32			15			Shunt 2		Shunt 2		分流器2读数				分流器2读数
92		32			15			Shunt 3		Shunt 3		分流器3读数				分流器3读数
93		32			15			Temperature1				Temp1			温度1					温度1
94		32			15			Temperature2				Temp2			温度2					温度2
95		32			15			Load1 Alarm Flag			Load1 Alarm Flag	负载1告警标志				负载1告警标志
96		32			15			Load2 Alarm Flag			Load2 Alarm Flag	负载2告警标志				负载2告警标志
97		32			15			Load3 Alarm Flag			Load3 Alarm Flag	负载3告警标志				负载3告警标志
98		32			15			Current1 High Current			Curr 1 Hi		电流1过流				电流1过流
99		32			15			Current1 Very High Current		Curr 1 Very Hi		电流1过过流				电流1过过流
100		32			15			Current2 High Current			Curr 2 Hi		电流2过流				电流2过流
101		32			15			Current2 Very High Current		Curr 2 Very Hi		电流2过过流				电流2过过流
102		32			15			Current3 High Current			Curr 3 Hi		电流3过流				电流3过流
103		32			15			Current3 Very High Current		Curr 3 Very Hi		电流3过过流				电流3过过流
104		32			15			DO1 Normal State			DO1 Normal		DO1类型					DO1类型
105		32			15			DO2 Normal State			DO2 Normal		DO2类型					DO2类型
106		32			15			DO3 Normal State			DO3 Normal		DO3类型					DO3类型
107		32			15			DO4 Normal State			DO4 Normal		DO4类型					DO4类型
108		32			15			DO5 Normal State			DO5 Normal		DO5类型					DO5类型
109		32			15			Non-Energized				Non-Energized		正常					正常
110		32			15			Energized				Energized		反逻辑					反逻辑

500		32			15			Current1 Break Size		Curr1 Brk Size		电流1电流告警阈值		电流1告警阈值																														
501		32			15			Current1 High 1 Current Limit	Curr1 Hi1 Lmt		电流1过流点		电流1过电流点																															
502		32			15			Current1 High 2 Current Limit	Curr1 Hi2 Lmt		电流1过过电流点		电流1过过流点																															
503		32			15			Current2 Break Size		Curr2 Brk Size		电流2电流告警阈值		电流2告警阈值																														
504		32			15			Current2 High 1 Current Limit	Curr2 Hi1 Lmt		电流2过流点		电流2过电流点																															
505		32			15			Current2 High 2 Current Limit	Curr2 Hi2 Lmt		电流2过过电流点		电流2过过流点																															
506		32			15			Current3 Break Size		Curr3 Brk Size		电流3电流告警阈值		电流3告警阈值																														
507		32			15			Current3 High 1 Current Limit	Curr3 Hi1 Lmt		电流3过流点		电流3过电流点																															
508		32			15			Current3 High 2 Current Limit	Curr3 Hi2 Lmt		电流3过过电流点		电流3过过流点																															
509		32			15			Current1 High 1 Current		Curr1 Hi1Cur		电流1过流		电流1过流				
510		32			15			Current1 High 2 Current		Curr1 Hi2Cur		电流1过过流		电流1过过流				
511		32			15			Current2 High 1 Current		Curr2 Hi1Cur		电流2过流		电流2过流				
512		32			15			Current2 High 2 Current		Curr2 Hi2Cur		电流2过过流		电流2过过流				
513		32			15			Current3 High 1 Current		Curr3 Hi1Cur		电流3过流		电流3过流				
514		32			15			Current3 High 2 Current		Curr3 Hi2Cur		电流3过过流		电流3过过流				
