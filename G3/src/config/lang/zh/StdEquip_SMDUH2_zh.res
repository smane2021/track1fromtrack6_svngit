﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
zh

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		母排电压				母排电压
2		32			15			Load 1 Current				Load 1 Current		负载电流1				负载电流1
3		32			15			Load 2 Current				Load 2 Current		负载电流2				负载电流2
4		32			15			Load 3 Current				Load 3 Current		负载电流3				负载电流3
5		32			15			Load 4 Current				Load 4 Current		负载电流4				负载电流4
6		32			15			Load 5 Current				Load 5 Current		负载电流5				负载电流5
7		32			15			Load 6 Current				Load 6 Current		负载电流6				负载电流6
8		32			15			Load 7 Current				Load 7 Current		负载电流7				负载电流7
9		32			15			Load 8 Current				Load 8 Current		负载电流8				负载电流8
10		32			15			Load 9 Current				Load 9 Current		负载电流9				负载电流9
11		32			15			Load 10 Current				Load 10 Current		负载电流10				负载电流10
12		32			15			Load 11 Current				Load 11 Current		负载电流11				负载电流11
13		32			15			Load 12 Current				Load 12 Current		负载电流12				负载电流12
14		32			15			Load 13 Current				Load 13 Current		负载电流13				负载电流13
15		32			15			Load 14 Current				Load 14 Current		负载电流14				负载电流14
16		32			15			Load 15 Current				Load 15 Current		负载电流15				负载电流15
17		32			15			Load 16 Current				Load 16 Current		负载电流16				负载电流16
18		32			15			Load 17 Current				Load 17 Current		负载电流17				负载电流17
19		32			15			Load 18 Current				Load 18 Current		负载电流18				负载电流18
20		32			15			Load 19 Current				Load 19 Current		负载电流19				负载电流19
21		32			15			Load 20 Current				Load 20 Current		负载电流20				负载电流20
22		32			15			Power1					Power1			功率1					功率1
23		32			15			Power2					Power2			功率2					功率2
24		32			15			Power3					Power3			功率3					功率3
25		32			15			Power4					Power4			功率4					功率4
26		32			15			Power5					Power5			功率5					功率5
27		32			15			Power6					Power6			功率6					功率6
28		32			15			Power7					Power7			功率7					功率7
29		32			15			Power8					Power8			功率8					功率8
30		32			15			Power9					Power9			功率9					功率9
31		32			15			Power10					Power10			功率10					功率10
32		32			15			Power11					Power11			功率11					功率11
33		32			15			Power12					Power12			功率12					功率12
34		32			15			Power13					Power13			功率13					功率13
35		32			15			Power14					Power14			功率14					功率14
36		32			15			Power15					Power15			功率15					功率15
37		32			15			Power16					Power16			功率16					功率16
38		32			15			Power17					Power17			功率17					功率17
39		32			15			Power18					Power18			功率18					功率18
40		32			15			Power19					Power19			功率19					功率19
41		32			15			Power20					Power20			功率20					功率20
42		32			15			Energy Yesterday in Channel 1		CH1EnergyYTD		1路昨日电量				1路昨日电量
43		32			15			Energy Yesterday in Channel 2		CH2EnergyYTD		2路昨日电量				2路昨日电量
44		32			15			Energy Yesterday in Channel 3		CH3EnergyYTD		3路昨日电量				3路昨日电量
45		32			15			Energy Yesterday in Channel 4		CH4EnergyYTD		4路昨日电量				4路昨日电量
46		32			15			Energy Yesterday in Channel 5		CH5EnergyYTD		5路昨日电量				5路昨日电量
47		32			15			Energy Yesterday in Channel 6		CH6EnergyYTD		6路昨日电量				6路昨日电量
48		32			15			Energy Yesterday in Channel 7		CH7EnergyYTD		7路昨日电量				7路昨日电量
49		32			15			Energy Yesterday in Channel 8		CH8EnergyYTD		8路昨日电量				8路昨日电量
50		32			15			Energy Yesterday in Channel 9		CH9EnergyYTD		9路昨日电量				9路昨日电量
51		32			15			Energy Yesterday in Channel 10		CH10EnergyYTD		10路昨日电量				10路昨日电量
52		32			15			Energy Yesterday in Channel 11		CH11EnergyYTD		11路昨日电量				11路昨日电量
53		32			15			Energy Yesterday in Channel 12		CH12EnergyYTD		12路昨日电量				12路昨日电量
54		32			15			Energy Yesterday in Channel 13		CH13EnergyYTD		13路昨日电量				13路昨日电量
55		32			15			Energy Yesterday in Channel 14		CH14EnergyYTD		14路昨日电量				14路昨日电量
56		32			15			Energy Yesterday in Channel 15		CH15EnergyYTD		15路昨日电量				15路昨日电量
57		32			15			Energy Yesterday in Channel 16		CH16EnergyYTD		16路昨日电量				16路昨日电量
58		32			15			Energy Yesterday in Channel 17		CH17EnergyYTD		17路昨日电量				17路昨日电量
59		32			15			Energy Yesterday in Channel 18		CH18EnergyYTD		18路昨日电量				18路昨日电量
60		32			15			Energy Yesterday in Channel 19		CH19EnergyYTD		19路昨日电量				19路昨日电量
61		32			15			Energy Yesterday in Channel 20		CH20EnergyYTD		20路昨日电量				20路昨日电量
62		32			15			Total Energy in Channel 1		CH1TotalEnergy		1路总电量				1路总电量
63		32			15			Total Energy in Channel 2		CH2TotalEnergy		2路总电量				2路总电量
64		32			15			Total Energy in Channel 3		CH3TotalEnergy		3路总电量				3路总电量
65		32			15			Total Energy in Channel 4		CH4TotalEnergy		4路总电量				4路总电量
66		32			15			Total Energy in Channel 5		CH5TotalEnergy		5路总电量				5路总电量
67		32			15			Total Energy in Channel 6		CH6TotalEnergy		6路总电量				6路总电量
68		32			15			Total Energy in Channel 7		CH7TotalEnergy		7路总电量				7路总电量
69		32			15			Total Energy in Channel 8		CH8TotalEnergy		8路总电量				8路总电量
70		32			15			Total Energy in Channel 9		CH9TotalEnergy		9路总电量				9路总电量
71		32			15			Total Energy in Channel 10		CH10TotalEnergy		10路总电量				10路总电量
72		32			15			Total Energy in Channel 11		CH11TotalEnergy		11路总电量				11路总电量
73		32			15			Total Energy in Channel 12		CH12TotalEnergy		12路总电量				12路总电量
74		32			15			Total Energy in Channel 13		CH13TotalEnergy		13路总电量				13路总电量
75		32			15			Total Energy in Channel 14		CH14TotalEnergy		14路总电量				14路总电量
76		32			15			Total Energy in Channel 15		CH15TotalEnergy		15路总电量				15路总电量
77		32			15			Total Energy in Channel 16		CH16TotalEnergy		16路总电量				16路总电量
78		32			15			Total Energy in Channel 17		CH17TotalEnergy		17路总电量				17路总电量
79		32			15			Total Energy in Channel 18		CH18TotalEnergy		18路总电量				18路总电量
80		32			15			Total Energy in Channel 19		CH19TotalEnergy		19路总电量				19路总电量
81		32			15			Total Energy in Channel 20		CH20TotalEnergy		20路总电量				20路总电量
82		32			15			Normal					Normal			正常					正常
83		32			15			Low					Low			低于下限				低于下限
84		32			15			High					High			高于上限				高于上限
85		32			15			Under Voltage				Under Voltage		欠压					欠压
86		32			15			Over Voltage				Over Voltage		过压					过压
87		32			15			Shunt 1 Current Alarm			Shunt 1 Alarm		分流器1告警				分流器1告警
88		32			15			Shunt 2 Current Alarm			Shunt 2 Alarm		分流器2告警				分流器2告警
89		32			15			Shunt 3 Current Alarm			Shunt 3 Alarm		分流器3告警				分流器3告警
90		32			15			Bus Voltage Alarm			BusVolt Alarm		母排电压告警				母排电压告警
91		32			15			SMDUH Fault				SMDUH Fault		SMDUH故障				SMDUH故障
92		32			15			Shunt 2 Over Current			Shunt 2 OverCur		分流器2过流				分流器2过流
93		32			15			Shunt 3 Over Current			Shunt 3 OverCur		分流器3过流				分流器3过流
94		32			15			Shunt 4 Over Current			Shunt 4 OverCur		分流器4过流				分流器4过流
95		32			15			Times of Communication Fail		Times Comm Fail		通信失败次数				通信失败次数
96		32			15			Existent				Existent		存在					存在
97		32			15			Not Existent				Not Existent		不存在					不存在
98		32			15			Very Low				Very Low		低于下下限				低于下下限
99		32			15			Very High				Very High		高于上上限				高于上上限
100		32			15			Switch					Switch			Swich					Swich
101		32			15			LVD1 Fail				LVD 1 Fail		LVD1控制失败				LVD1控制失败
102		32			15			LVD2 Fail				LVD 2 Fail		LVD2控制失败				LVD2控制失败
103		32			15			High Temperature Disconnect 1		HTD 1			HTD1高温下电允许			HTD1下电允许
104		32			15			High Temperature Disconnect 2		HTD 2			HTD2高温下电允许			HTD2下电允许
105		32			15			Battery LVD				Battery LVD		电池下电				电池下电
106		32			15			No Battery				No Battery		无电池					无电池
107		32			15			LVD 1					LVD 1			LVD1					LVD1
108		32			15			LVD 2					LVD 2			LVD2					LVD2
109		32			15			Battery Always On			Batt Always On		有电池但不下电				有电池但不下电
110		32			15			Barcode					Barcode			Barcode					Barcode
111		32			15			DC Over Voltage				DC Over Volt		直流过压点				直流过压点
112		32			15			DC Under Voltage			DC Under Volt		直流欠压点				直流欠压点
113		32			15			Over Current 1				Over Curr 1		过流点1					过流点1
114		32			15			Over Current 2				Over Curr 2		过流点2					过流点2
115		32			15			Over Current 3				Over Curr 3		过流点3					过流点3
116		32			15			Over Current 4				Over Curr 4		过流点4					过流点4
117		32			15			Existence State				Existence State		是否存在				是否存在
118		32			15			Communication Fail			Comm Fail		通讯中断				通讯中断
119		32			15			Bus Voltage Status			Bus Volt Status		电压状态				电压状态
120		32			15			Comm OK					Comm OK			通讯正常				通讯正常
121		32			15			All Batteries Comm Fail			AllBattCommFail		都通讯中断				都通讯中断
122		32			15			Communication Fail			Comm Fail		通讯中断				通讯中断
123		32			15			Rated Capacity				Rated Capacity		标称容量				标称容量
124		32			15			Load 5 Current				Load 5 Current		负载电流5				负载电流5
125		32			15			Shunt 1 Voltage				Shunt 1 Voltage		分流器1电压				分流器1电压
126		32			15			Shunt 1 Current				Shunt 1 Current		分流器1电流				分流器1电流
127		32			15			Shunt 2 Voltage				Shunt 2 Voltage		分流器2电压				分流器2电压
128		32			15			Shunt 2 Current				Shunt 2 Current		分流器2电流				分流器2电流
129		32			15			Shunt 3 Voltage				Shunt 3 Voltage		分流器3电压				分流器3电压
130		32			15			Shunt 3 Current				Shunt 3 Current		分流器3电流				分流器3电流
131		32			15			Shunt 4 Voltage				Shunt 4 Voltage		分流器4电压				分流器4电压
132		32			15			Shunt 4 Current				Shunt 4 Current		分流器4电流				分流器4电流
133		32			15			Shunt 5 Voltage				Shunt 5 Voltage		分流器5电压				分流器5电压
134		32			15			Shunt 5 Current				Shunt 5 Current		分流器5电流				分流器5电流
135		32			15			Normal					Normal			正常					正常
136		32			15			Fail					Fail			故障					故障
137		32			15			Hall Calibrate Point 1			HallCalibrate1		Hall校准电流点1				Hall校准1
138		32			15			Hall Calibrate Point 2			HallCalibrate2		Hall校准电流点2				Hall校准2
139		32			15			Energy Clear				EnergyClear		电量清零				电量清零
140		32			15			All Channels				All Channels		所有分路				所有分路
141		32			15			Channel 1				Channel 1		分路1					分路1
142		32			15			Channel 2				Channel 2		分路2					分路2
143		32			15			Channel 3				Channel 3		分路3					分路3
144		32			15			Channel 4				Channel 4		分路4					分路4
145		32			15			Channel 5				Channel 5		分路5					分路5
146		32			15			Channel 6				Channel 6		分路6					分路6
147		32			15			Channel 7				Channel 7		分路7					分路7
148		32			15			Channel 8				Channel 8		分路8					分路8
149		32			15			Channel 9				Channel 9		分路9					分路9
150		32			15			Channel 10				Channel 10		分路10					分路10
151		32			15			Channel 11				Channel 11		分路11					分路11
152		32			15			Channel 12				Channel 12		分路12					分路12
153		32			15			Channel 13				Channel 13		分路13					分路13
154		32			15			Channel 14				Channel 14		分路14					分路14
155		32			15			Channel 15				Channel 15		分路15					分路15
156		32			15			Channel 16				Channel 16		分路16					分路16
157		32			15			Channel 17				Channel 17		分路17					分路17
158		32			15			Channel 18				Channel 18		分路18					分路18
159		32			15			Channel 19				Channel 19		分路19					分路19
160		32			15			Channel 20				Channel 20		分路20					分路20
161		32			15			Hall Calibrate Channel			CalibrateChan		Hall校准分路				Hall校准分路
162		32			15			Hall Coeff 1				Hall Coeff 1		Hall系数1				Hall系数1
163		32			15			Hall Coeff 2				Hall Coeff 2		Hall系数2				Hall系数2
164		32			15			Hall Coeff 3				Hall Coeff 3		Hall系数3				Hall系数3
165		32			15			Hall Coeff 4				Hall Coeff 4		Hall系数4				Hall系数4
166		32			15			Hall Coeff 5				Hall Coeff 5		Hall系数5				Hall系数5
167		32			15			Hall Coeff 6				Hall Coeff 6		Hall系数6				Hall系数6
168		32			15			Hall Coeff 7				Hall Coeff 7		Hall系数7				Hall系数7
169		32			15			Hall Coeff 8				Hall Coeff 8		Hall系数8				Hall系数8
170		32			15			Hall Coeff 9				Hall Coeff 9		Hall系数9				Hall系数9
171		32			15			Hall Coeff 10				Hall Coeff 10		Hall系数10				Hall系数10
172		32			15			Hall Coeff 11				Hall Coeff 11		Hall系数11				Hall系数11
173		32			15			Hall Coeff 12				Hall Coeff 12		Hall系数12				Hall系数12
174		32			15			Hall Coeff 13				Hall Coeff 13		Hall系数13				Hall系数13
175		32			15			Hall Coeff 14				Hall Coeff 14		Hall系数14				Hall系数14
176		32			15			Hall Coeff 15				Hall Coeff 15		Hall系数15				Hall系数15
177		32			15			Hall Coeff 16				Hall Coeff 16		Hall系数16				Hall系数16
178		32			15			Hall Coeff 17				Hall Coeff 17		Hall系数17				Hall系数17
179		32			15			Hall Coeff 18				Hall Coeff 18		Hall系数18				Hall系数18
180		32			15			Hall Coeff 19				Hall Coeff 19		Hall系数19				Hall系数19
181		32			15			Hall Coeff 20				Hall Coeff 20		Hall系数20				Hall系数20
182		32			15			All Days				All Days		全部天数				全部天数
183		32			15			SMDUH 2					SMDUH 2			SMDUH 2					SMDUH 2
184		32			15			Reset Energy Channel X			RstEnergyChanX		通道电量清零				通道电量清零
185		32			15			Load1 Alarm Flag			Load1 Alarm Flag	负载1告警标志				负载1告警标志
186		32			15			Load2 Alarm Flag			Load2 Alarm Flag	负载2告警标志				负载2告警标志
187		32			15			Load3 Alarm Flag			Load3 Alarm Flag	负载3告警标志				负载3告警标志
188		32			15			Load4 Alarm Flag			Load4 Alarm Flag	负载4告警标志				负载4告警标志
189		32			15			Load5 Alarm Flag			Load5 Alarm Flag	负载5告警标志				负载5告警标志
190		32			15			Load6 Alarm Flag			Load6 Alarm Flag	负载6告警标志				负载6告警标志
191		32			15			Load7 Alarm Flag			Load7 Alarm Flag	负载7告警标志				负载7告警标志
192		32			15			Load8 Alarm Flag			Load8 Alarm Flag	负载8告警标志				负载8告警标志
193		32			15			Load9 Alarm Flag			Load9 Alarm Flag	负载9告警标志				负载9告警标志
194		32			15			Load10 Alarm Flag			Load10 Alarm Flag	负载10告警标志				负载10告警标志
195		32			15			Load11 Alarm Flag			Load11 Alarm Flag	负载11告警标志				负载11告警标志
196		32			15			Load12 Alarm Flag			Load12 Alarm Flag	负载12告警标志				负载12告警标志
197		32			15			Load13 Alarm Flag			Load13 Alarm Flag	负载13告警标志				负载13告警标志
198		32			15			Load14 Alarm Flag			Load14 Alarm Flag	负载14告警标志				负载14告警标志
199		32			15			Load15 Alarm Flag			Load15 Alarm Flag	负载15告警标志				负载15告警标志
200		32			15			Load16 Alarm Flag			Load16 Alarm Flag	负载16告警标志				负载16告警标志
201		32			15			Load17 Alarm Flag			Load17 Alarm Flag	负载17告警标志				负载17告警标志
202		32			15			Load18 Alarm Flag			Load18 Alarm Flag	负载18告警标志				负载18告警标志
203		32			15			Load19 Alarm Flag			Load19 Alarm Flag	负载19告警标志				负载19告警标志
204		32			15			Load20 Alarm Flag			Load20 Alarm Flag	负载20告警标志				负载20告警标志
205		32			15			Current1 High Current			Curr 1 Hi		电流1过流				电流1过流
206		32			15			Current1 Very High Current		Curr 1 Very Hi		电流1过过流				电流1过过流
207		32			15			Current2 High Current			Curr 2 Hi		电流2过流				电流2过流
208		32			15			Current2 Very High Current		Curr 2 Very Hi		电流2过过流				电流2过过流
209		32			15			Current3 High Current			Curr 3 Hi		电流3过流				电流3过流
210		32			15			Current3 Very High Current		Curr 3 Very Hi		电流3过过流				电流3过过流
211		32			15			Current4 High Current			Curr 4 Hi		电流4过流				电流4过流
212		32			15			Current4 Very High Current		Curr 4 Very Hi		电流4过过流				电流4过过流
213		32			15			Current5 High Current			Curr 5 Hi		电流5过流				电流5过流
214		32			15			Current5 Very High Current		Curr 5 Very Hi		电流5过过流				电流5过过流
215		32			15			Current6 High Current			Curr 6 Hi		电流6过流				电流6过流
216		32			15			Current6 Very High Current		Curr 6 Very Hi		电流6过过流				电流6过过流
217		32			15			Current7 High Current			Curr 7 Hi		电流7过流				电流7过流
218		32			15			Current7 Very High Current		Curr 7 Very Hi		电流7过过流				电流7过过流
219		32			15			Current8 High Current			Curr 8 Hi		电流8过流				电流8过流
220		32			15			Current8 Very High Current		Curr 8 Very Hi		电流8过过流				电流8过过流
221		32			15			Current9 High Current			Curr 9 Hi		电流9过流				电流9过流
222		32			15			Current9 Very High Current		Curr 9 Very Hi		电流9过过流				电流9过过流
223		32			15			Current10 High Current			Curr 10 Hi		电流10过流				电流10过流
224		32			15			Current10 Very High Current		Curr 10 Very Hi		电流10过过流				电流10过过流
225		32			15			Current11 High Current			Curr 11 Hi		电流11过流				电流11过流
226		32			15			Current11 Very High Current		Curr 11 Very Hi		电流11过过流				电流11过过流
227		32			15			Current12 High Current			Curr 12 Hi		电流12过流				电流12过流
228		32			15			Current12 Very High Current		Curr 12 Very Hi		电流12过过流				电流12过过流
229		32			15			Current13 High Current			Curr 13 Hi		电流13过流				电流13过流
230		32			15			Current13 Very High Current		Curr 13 Very Hi		电流13过过流				电流13过过流
231		32			15			Current14 High Current			Curr 14 Hi		电流14过流				电流14过流
232		32			15			Current14 Very High Current		Curr 14 Very Hi		电流14过过流				电流14过过流
233		32			15			Current15 High Current			Curr 15 Hi		电流15过流				电流15过流
234		32			15			Current15 Very High Current		Curr 15 Very Hi		电流15过过流				电流15过过流
235		32			15			Current16 High Current			Curr 16 Hi		电流16过流				电流16过流
236		32			15			Current16 Very High Current		Curr 16 Very Hi		电流16过过流				电流16过过流
237		32			15			Current17 High Current			Curr 17 Hi		电流17过流				电流17过流
238		32			15			Current17 Very High Current		Curr 17 Very Hi		电流17过过流				电流17过过流
239		32			15			Current18 High Current			Curr 18 Hi		电流18过流				电流18过流
240		32			15			Current18 Very High Current		Curr 18 Very Hi		电流18过过流				电流18过过流
241		32			15			Current19 High Current			Curr 19 Hi		电流19过流				电流19过流
242		32			15			Current19 Very High Current		Curr 19 Very Hi		电流19过过流				电流19过过流
243		32			15			Current20 High Current			Curr 20 Hi		电流20过流				电流20过流
244		32			15			Current20 Very High Current		Curr 20 Very Hi		电流20过过流				电流20过过流
