﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
zh

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			IPLU						IPLU					IPLU					IPLU
8		32			15			Battery Block 1 Voltage		Batt Blk1 Volt			电池块1电压				电池块1电压
9		32			15			Battery Block 2 Voltage		Batt Blk2 Volt			电池块2电压				电池块2电压
10		32			15			Battery Block 3 Voltage		Batt Blk3 Volt			电池块3电压				电池块3电压
11		32			15			Battery Block 4 Voltage		Batt Blk4 Volt			电池块4电压				电池块4电压
12		32			15			Battery Block 5 Voltage		Batt Blk5 Volt			电池块5电压				电池块5电压
13		32			15			Battery Block 6 Voltage		Batt Blk6 Volt			电池块6电压				电池块6电压
14		32			15			Battery Block 7 Voltage		Batt Blk7 Volt			电池块7电压				电池块7电压
15		32			15			Battery Block 8 Voltage		Batt Blk8 Volt			电池块8电压				电池块8电压
16		32			15			Battery Block 9 Voltage		Batt Blk9 Volt			电池块9电压				电池块9电压
17		32			15			Battery Block 10 Voltage	Batt Blk10 Volt			电池块10电压			电池块10电压
18		32			15			Battery Block 11 Voltage	Batt Blk11 Volt			电池块11电压			电池块11电压
19		32			15			Battery Block 12 Voltage	Batt Blk12 Volt			电池块12电压			电池块12电压
20		32			15			Battery Block 13 Voltage	Batt Blk13 Volt			电池块13电压			电池块13电压
21		32			15			Battery Block 14 Voltage	Batt Blk14 Volt			电池块14电压			电池块14电压
22		32			15			Battery Block 15 Voltage	Batt Blk15 Volt			电池块15电压			电池块15电压
23		32			15			Battery Block 16 Voltage	Batt Blk16 Volt			电池块16电压			电池块16电压
24		32			15			Battery Block 17 Voltage	Batt Blk17 Volt			电池块17电压			电池块17电压
25		32			15			Battery Block 18 Voltage	Batt Blk18 Volt			电池块18电压			电池块18电压
26		32			15			Battery Block 19 Voltage	Batt Blk19 Volt			电池块19电压			电池块19电压
27		32			15			Battery Block 20 Voltage	Batt Blk20 Volt			电池块20电压			电池块20电压
28		32			15			Battery Block 21 Voltage	Batt Blk21 Volt			电池块21电压			电池块21电压
29		32			15			Battery Block 22 Voltage	Batt Blk22 Volt			电池块22电压			电池块22电压
30		32			15			Battery Block 23 Voltage	Batt Blk23 Volt			电池块23电压			电池块23电压
31		32			15			Battery Block 24 Voltage	Batt Blk24 Volt			电池块24电压			电池块24电压
32		32			15			Battery Block 25 Voltage	Batt Blk25 Volt			电池块25电压			电池块25电压
33		32			15			Temperature1				Temperature1			温度1					温度1
34		32			15			Temperature2				Temperature2			温度2					温度2
35		32			15			Battery Current				Battery Curr			电池电流				电池电流
36		32			15			Battery Voltage				Battery Volt			电池电压				电池电压

40		32			15			Battery Block High			Batt Blk High			块电压高				块电压高
41		32			15			Battery Block Low			Batt Blk Low			块电压低				块电压低

50		32			15			IPLU No Response			IPLU No Response		IPLU通信失败			IPLU通信失败
51		32			15			Battery Block1 Alarm		Batt Blk1 Alm			电池块1告警				电池块1告警
52		32			15			Battery Block2 Alarm		Batt Blk2 Alm			电池块2告警				电池块2告警
53		32			15			Battery Block3 Alarm		Batt Blk3 Alm			电池块3告警				电池块3告警
54		32			15			Battery Block4 Alarm		Batt Blk4 Alm			电池块4告警				电池块4告警
55		32			15			Battery Block5 Alarm		Batt Blk5 Alm			电池块5告警				电池块5告警
56		32			15			Battery Block6 Alarm		Batt Blk6 Alm			电池块6告警				电池块6告警
57		32			15			Battery Block7 Alarm		Batt Blk7 Alm			电池块7告警				电池块7告警
58		32			15			Battery Block8 Alarm		Batt Blk8 Alm			电池块8告警				电池块8告警
59		32			15			Battery Block9 Alarm		Batt Blk9 Alm			电池块9告警				电池块9告警
60		32			15			Battery Block10 Alarm		Batt Blk10 Alm			电池块10告警			电池块10告警
61		32			15			Battery Block11 Alarm		Batt Blk11 Alm			电池块11告警			电池块11告警
62		32			15			Battery Block12 Alarm		Batt Blk12 Alm			电池块12告警			电池块12告警
63		32			15			Battery Block13 Alarm		Batt Blk13 Alm			电池块13告警			电池块13告警
64		32			15			Battery Block14 Alarm		Batt Blk14 Alm			电池块14告警			电池块14告警
65		32			15			Battery Block15 Alarm		Batt Blk15 Alm			电池块15告警			电池块15告警
66		32			15			Battery Block16 Alarm		Batt Blk16 Alm			电池块16告警			电池块16告警
67		32			15			Battery Block17 Alarm		Batt Blk17 Alm			电池块17告警			电池块17告警
68		32			15			Battery Block18 Alarm		Batt Blk18 Alm			电池块18告警			电池块18告警
69		32			15			Battery Block19 Alarm		Batt Blk19 Alm			电池块19告警			电池块19告警
70		32			15			Battery Block20 Alarm		Batt Blk20 Alm			电池块20告警			电池块20告警
71		32			15			Battery Block21 Alarm		Batt Blk21 Alm			电池块21告警			电池块21告警
72		32			15			Battery Block22 Alarm		Batt Blk22 Alm			电池块22告警			电池块22告警
73		32			15			Battery Block23 Alarm		Batt Blk23 Alm			电池块23告警			电池块23告警
74		32			15			Battery Block24 Alarm		Batt Blk24 Alm			电池块24告警			电池块24告警
75		32			15			Battery Block25 Alarm		Batt Blk25 Alm			电池块25告警			电池块25告警

76		32			15			Battery Capacity			Battery Capacity		电池容量				电池容量
77		32			15			Capacity Percent			Capacity Percent		容量百分比				容量百分比

78		32			15			Enable						Enable					使能					使能
79		32			15			Disable						Disable					不使能					不使能
84		32			15			no							no						否						否
85		32			15			yes							yes						是						是





103		32			15			Existence State				Existence State		是否存在				是否存在
104		32			15			Existent					Existent			存在					存在
105		32			15			Not Existent				Not Existent		不存在					不存在
106		32			15			IPLU Communication Fail		IPLU Comm Fail		IPLU通信中断			IPLU通信中断
107		32			15			Communication OK			Comm OK				通信正常				通信正常
108		32			15			Communication Fail			Comm Fail			通信中断				通信中断
109		32			15			Rated Capacity				Rated Capacity				额定容量		额定容量
110		32			15			Used by Batt Management		Used by Batt Management		电池管理		电池管理
116		32			15			Battery Current Imbalance	Battery Current Imbalance	电池不平衡		电池不平衡
