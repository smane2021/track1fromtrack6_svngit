﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
zh

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1	32		15		Temperature 1		Temperature 1		温度1			温度1
2	32		15		Temperature 2		Temperature 2		温度2			温度2
3	32		15		Temperature 3		Temperature 3		温度3			温度3
4	32		15		Humidity		Humidity		湿度			湿度
5	32		15		Temperature 1 Alarm		Temp 1 Alm		温度1告警		温度1告警
6	32		15		Temperature 2 Alarm		Temp 2 Alm		温度2告警		温度2告警
7	32		15		Temperature 3 Alarm		Temp 3 Alm		温度3告警		温度3告警
8	32		15		Humidity Alarm		Humidity Alm		湿度告警		湿度告警
9	32		15		Fan 1 Alarm		Fan 1 Alm		风机1告警		风机1告警
10	32		15		Fan 2 Alarm		Fan 2 Alm		风机2告警		风机2告警
11	32		15		Fan 3 Alarm		Fan 3 Alm		风机3告警		风机3告警
12	32		15		Fan 4 Alarm		Fan 4 Alm		风机4告警		风机4告警
13	32		15		Fan 5 Alarm		Fan 5 Alm		风机5告警		风机5告警
14	32		15		Fan 6 Alarm		Fan 6 Alm		风机6告警		风机6告警
15	32		15		Fan 7 Alarm		Fan 7 Alm		风机7告警		风机7告警
16	32		15		Fan 8 Alarm		Fan 8 Alm		风机8告警		风机8告警
17	32		15		DI 1 Alarm		DI 1 Alm		DI1告警		DI1告警
18	32		15		DI 2 Alarm		DI 2 Alm		DI2告警		DI2告警
19	32		15		Fan Type		Fan Type		风机类型		风机类型
20	32		15		With Fan 3		With Fan 3		风机3存在状态		风机3存在状态
21	32		15		Fan 3 Ctrl Logic	F3 Ctrl Logic		风机组3控制逻辑		风机组3控制逻辑
22	32		15		With Heater		With Heater		加热器存在状态		加热器存在状态
23	32		15		With Temp and Humidity Sensor	With T Hum Sen	温湿度传感器		温湿度传感器
24	32		15		Fan 1 State		Fan 1 State		风机1状态		风机1状态
25	32		15		Fan 2 State		Fan 2 State		风机2状态		风机2状态
26	32		15		Fan 3 State		Fan 3 State		风机3状态		风机3状态
27	32		15		Temperature Sensor Fail	T Sensor Fail		温度传感器故障		温度传感器故障
28	32		15		Heat Change		Heat Change		热交换型		热交换型
29	32		15		Forced Vent		Forced Vent		直通风型		直通风型
30	32		15		Not Existence		Not Exist		不存在			不存在
31	32		15		Existence		Exist		存在			存在
32	32		15		Heater Logic		Heater Logic		加热器逻辑		加热器逻辑
33	32		15		ETC Logic		ETC Logic		ETC逻辑			ETC逻辑
34	32		15		Stop			Stop			停止			停止
35	32		15		Start			Start			启动			启动
36	32		15		Temperature 1 Over		Temp1 Over		温度1过温点		温度1过温点
37	32		15		Temperature 1 Under		Temp1 Under		温度1欠温点		温度1欠温点
38	32		15		Temperature 2 Over		Temp2 Over		温度2过温点		温度2过温点
39	32		15		Temperature 2 Under		Temp2 Under		温度2欠温点		温度2欠温点
40	32		15		Temperature 3 Over		Temp3 Over		温度3过温点		温度3过温点
41	32		15		Temperature 3 Under		Temp3 Under		温度3欠温点		温度3欠温点
42	32		15		Humidity Over		Humidity Over		湿度过湿点		湿度过湿点
43	32		15		Humidity Under		Humidity Under		湿度欠湿点		湿度欠湿点
44	32		15		Temperature 1 Sensor State		Temp1 Sensor		温度1传感器状态		温度1传感器状态
45	32		15		Temperature 2 Sensor State		Temp2 Sensor		温度2传感器状态		温度2传感器状态
46	32		15		DI1 Alarm Type		DI1 Alm Type		DI1告警类型		DI1告警类型
47	32		15		DI2 Alarm Type		DI2 Alm Type		DI2告警类型		DI2告警类型
48	32		15		No. of Fans in Fan Group 1		Num of FanG1		风机组1风机数		风机组1风机数
49	32		15		No. of Fans in Fan Group 2		Num of FanG2		风机组2风机数		风机组2风机数
50	32		15		No. of Fans in Fan Group 3		Num of FanG3		风机组3风机数		风机组3风机数
51	32		15		Temperature Sensor of Fan1		T Sensor of F1		风机1温度传感器		风机1温度传感器
52	32		15		Temperature Sensor of Fan2		T Sensor of F2		风机2温度传感器		风机2温度传感器
53	32		15		Temperature Sensor of Fan3		T Sensor of F3		风机3温度传感器		风机3温度传感器
54	32		15		Temperature Sensor of Heater1	T Sensor of H1	1加热温度传感器		1加热温度传感器
55	32		15		Temperature Sensor of Heater2	T Sensor of H2	2加热温度传感器		2加热温度传感器
56	32		15		HEX Fan Group 1 50% Speed Temp		H1 50%Speed Temp		热风机1半速温度		热风机1半速温度
57	32		15		HEX Fan Group 1 100% Speed Temp		H1 100%Speed Temp		热风机1全速温度		热风机1全速温度
58	32		15		HEX Fan Group 2 Start Temp			H2 Start Temp			热风机2启动温度		热风机2启动温度
59	32		15		HEX Fan Group 2 100% Speed Temp		H2 100%Speed Temp		热风机2全速温度		热风机2全速温度	
60	32		15		HEX Fan Group 2 Stop Temp			H2 Stop Temp			热风机2停止温度		热风机2停止温度
61	32		15		Fan Filter Fan G1 Start Temp		FV1 Start Temp		直通风机1开始温度		直通风机1开始温度
62	32		15		Fan Filter Fan G1 Max Speed Temp	FV1 Full Temp		直通风机1全速温度		直通风机1全速温度
63	32		15		Fan Filter Fan G1 Stop Temp			FV1 Stop Temp		直通风机1停止温度		直通风机1停止温度
64	32		15		Fan Filter Fan G2 Start Temp		FV2 Start Temp		直通风机2开始温度		直通风机2开始温度
65	32		15		Fan Filter Fan G2 max speed Temp	FV2 Full Temp		直通风机2全速温度		直通风机2全速温度
66	32		15		Fan Filter Fan G2 Stop Temp			FV2 Stop Temp		直通风机2停止温度		直通风机2停止温度
67	32		15		Fan Filter Fan G3 Start Temp		F3 Start Temp		风机3开始温度		风机3开始温度
68	32		15		Fan Filter Fan G3 max speed Temp	F3 Full Temp		风机3全速温度		风机3全速温度
69	32		15		Fan Filter Fan G3 Stop Temp			F3 Stop Temp		风机3停止温度		风机3停止温度
70	32		15		Heater1 Start Temperature	Heater1 Start Temp	加热器1开始温度		加热器1开始温度
71	32		15		Heater1 Stop Temperature	Heater1 Stop Temp	加热器1停止温度		加热器1停止温度
72	32		15		Heater2 Start Temperature	Heater2 Start Temp	加热器2开始温度		加热器2开始温度
73	32		15		Heater2 Stop Temperature	Heater2 Stop Temp	加热器2停止温度		加热器2停止温度
74	32		15		Fan Group 1 Max Speed		FG1 Max Speed		风机组1最大速度		风机组1最大速度
75	32		15		Fan Group 2 Max Speed		FG2 Max Speed		风机组2最大速度		风机组2最大速度
76	32		15		Fan Group 3 Max Speed		FG3 Max Speed		风机组3最大速度		风机组3最大速度
77	32		15		Fan Minimum Speed			Fan Min Speed		风机组最小速度		风机组最小速度
78	32		15		Close			Close			闭合告警		闭合告警
79	32		15		Open			Open			断开告警		断开告警
80	32		15		Self Rectifier Alarm		Self Rect Alm		自成模块告警		自成模块告警
81	32		15		With FCUP		With FCUP		存在风机控制板		存在风机控制板
82	32		15		Normal			Normal			正常			正常
83	32		15		Low				Low			低		低
84	32		15		High				High			高		高
85	32		15		Alarm				Alarm			告警		告警
86	32		15		Times of Communication Fail	Times Comm Fail		通信失败次数	通信失败次数
87	32		15		Existence State			Existence State		是否存在	是否存在
88	32		15		Comm OK				Comm OK			通讯正常	通讯正常
89	32		15		All Batteries Comm Fail		AllBattCommFail		都通讯中断	都通讯中断
90	32		15		Communication Fail		Comm Fail		通讯中断	通讯中断
91	32		15		FCUPLUS				FCUPLUS			风机控制器	风机控制器
92	32		15		Heater1 State			Heater1 State			加热器1状态	加热器1状态
93	32		15		Heater2 State			Heater2 State			加热器2状态	加热器2状态
94	32		15		Temperature 1 Low		Temp 1 Low			温度1低		温度1低
95	32		15		Temperature 2 Low		Temp 2 Low			温度2低		温度2低
96	32		15		Temperature 3 Low		Temp 3 Low			温度3低		温度3低
97	32		15		Humidity Low			Humidity Low			湿度过低	湿度过低
98	32		15		Temperature 1 High		Temp 1 High			温度1高		温度1高
99	32		15		Temperature 2 High		Temp 2 High			温度2高		温度2高
100	32		15		Temperature 3 High		Temp 3 High			温度3高		温度3高
101	32		15		Humidity High			Humidity High			湿度过高	湿度过高
102	32		15		Temperature 1 Sensor Fail		T1 Sensor Fail		温度传感器1故障		温度1故障
103	32		15		Temperature 2 Sensor Fail		T2 Sensor Fail		温度传感器2故障		温度2故障
104	32		15		Temperature 3 Sensor Fail		T3 Sensor Fail		温度传感器3故障		温度3故障
105	32		15		Humidity Sensor Fail			Hum Sensor Fail		湿度传感器故障		湿度传感器故障
106	32		15		0					0			0			0
107	32		15		1					1			1			1
108	32		15		2					2			2			2
109	32		15		3					3			3			3
110	32		15		4					4			4			4
111	32		15		Enter Test Mode				Enter Test Mode				进入测试模式		进入测试模式
112	32		15		Exit Test Mode				Exit Test Mode				退出测试模式		退出测试模式
113	32		15		Start Fan Group 1 Test		StartFanG1Test				开始风机1测试		风机1测试	
114	32		15		Start Fan Group 2 Test		StartFanG2Test				开始风机2测试		风机2测试	
115	32		15		Start Fan Group 3 Test		StartFanG3Test				开始风机3测试		风机3测试
116	32		15		Start Heater1 Test			StartHeat1Test				开始加热器1测试		加热器1测试
117	32		15		Start Heater2 Test			StartHeat2Test				开始加热器2测试		加热器2测试
118	32		15		Clear						Clear						清除				清除
120	32		15		Clear Fan1 Run Time			ClrFan1RunTime				清风扇1运行时间		清风扇1时间
121	32		15		Clear Fan2 Run Time			ClrFan2RunTime				清风扇2运行时间		清风扇2时间
122	32		15		Clear Fan3 Run Time			ClrFan3RunTime				清风扇3运行时间		清风扇3时间
123	32		15		Clear Fan4 Run Time			ClrFan4RunTime				清风扇4运行时间		清风扇4时间
124	32		15		Clear Fan5 Run Time			ClrFan5RunTime				清风扇5运行时间		清风扇5时间
125	32		15		Clear Fan6 Run Time			ClrFan6RunTime				清风扇6运行时间		清风扇6时间
126	32		15		Clear Fan7 Run Time			ClrFan7RunTime				清风扇7运行时间		清风扇7时间
127	32		15		Clear Fan8 Run Time			ClrFan8RunTime				清风扇8运行时间		清风扇8时间
128	32		15		Clear Heater1 Run Time		ClrHtr1RunTime				清加热器1运行时间	清加热1时间
129	32		15		Clear Heater2 Run Time		ClrHtr2RunTime				清加热器2运行时间	清加热2时间
130	32		15		Fan1 Speed Tolerance		Fan1 Spd Toler			风扇1公差			风扇1公差
131	32		15		Fan2 Speed Tolerance		Fan2 Spd Toler			风扇2公差			风扇2公差
132	32		15		Fan3 Speed Tolerance		Fan3 Spd Toler			风扇3公差			风扇3公差
133	32		15		Fan1 Rated Speed			Fan1 Rated Spd			风扇1额定速度		风扇1额速
134	32		15		Fan2 Rated Speed			Fan2 Rated Spd			风扇2额定速度		风扇2额速
135	32		15		Fan3 Rated Speed			Fan3 Rated Spd			风扇3额定速度		风扇3额速
136	32		15		Heater Test Time			HeaterTestTime			加热器测试时间		加热测时间
137	32		15		Heater Test Temp Delta		HeaterTempDelta			加热器温差			加热器温差
140	32		15		Running Mode				Running Mode			运行模式			运行模式
141	32		15		FAN1 Status					FAN1Status				风扇1状态			风扇1状态
142	32		15		FAN2 Status					FAN2Status				风扇2状态			风扇2状态	
143	32		15		FAN3 Status					FAN3Status				风扇3状态			风扇3状态	
144	32		15		FAN4 Status					FAN4Status				风扇4状态			风扇4状态		
145	32		15		FAN5 Status					FAN5Status				风扇5状态			风扇5状态		
146	32		15		FAN6 Status					FAN6Status				风扇6状态			风扇6状态		
147	32		15		FAN7 Status					FAN7Status				风扇7状态			风扇7状态		
148	32		15		FAN8 Status					FAN8Status				风扇8状态			风扇8状态		
149	32		15		Heater1 Test Status			Heater1Status			加热器1状态			加热器1状态	
150	32		15		Heater2 Test Status			Heater2Status			加热器2状态			加热器2状态	
151	32		15		FAN1 Test Result			FAN1TestResult			风扇1测试结果		测风1结果
152	32		15		FAN2 Test Result			FAN2TestResult			风扇2测试结果		测风2结果
153	32		15		FAN3 Test Result			FAN3TestResult			风扇3测试结果		测风3结果
154	32		15		FAN4 Test Result			FAN4TestResult			风扇4测试结果		测风4结果
155	32		15		FAN5 Test Result			FAN5TestResult			风扇5测试结果		测风5结果
156	32		15		FAN6 Test Result			FAN6TestResult			风扇6测试结果		测风6结果
157	32		15		FAN7 Test Result			FAN7TestResult			风扇7测试结果		测风7结果
158	32		15		FAN8 Test Result			FAN8TestResult			风扇8测试结果		测风8结果
159	32		15		Heater1 Test Result			Heater1TestRslt			加热器1测试结果		测加热1结果
160	32		15		Heater2 Test Result			Heater2TestRslt			加热器2测试结果		测加热2结果
171	32		15		FAN1 Run Time				FAN1RunTime				风扇1运行时间		风扇1时间
172	32		15		FAN2 Run Time				FAN2RunTime				风扇2运行时间		风扇2时间
173	32		15		FAN3 Run Time				FAN3RunTime				风扇3运行时间		风扇3时间
174	32		15		FAN4 Run Time				FAN4RunTime				风扇4运行时间		风扇4时间
175	32		15		FAN5 Run Time				FAN5RunTime				风扇5运行时间		风扇5时间
176	32		15		FAN6 Run Time				FAN6RunTime				风扇6运行时间		风扇6时间
177	32		15		FAN7 Run Time				FAN7RunTime				风扇7运行时间		风扇7时间
178	32		15		FAN8 Run Time				FAN8RunTime				风扇8运行时间		风扇8时间
179	32		15		Heater1 Run Time			Heater1RunTime			加热器1运行时间		加热器1时间
180	32		15		Heater2 Run Time			Heater2RunTime			加热器2运行时间		加热器2时间

181	32		15		Normal						Normal					正常				正常
182	32		15		Test						Test					测试				测试
183	32		15		NA							NA						未初始化			未初始化
184	32		15		Stop						Stop					停止				停止
185	32		15		Run							Run						运行				运行
186	32		15		Test						Test					测试				测试

190	32		15		FCUP is Testing				FCUPIsTesting			FCUP正在测试		FCUP正在测
191	32		15		FAN1 Test Fail				FAN1TestFail			风扇1测试失败		风1测失败
192	32		15		FAN2 Test Fail				FAN2TestFail			风扇2测试失败		风2测失败
193	32		15		FAN3 Test Fail				FAN3TestFail			风扇3测试失败		风3测失败
194	32		15		FAN4 Test Fail				FAN4TestFail			风扇4测试失败		风4测失败
195	32		15		FAN5 Test Fail				FAN5TestFail			风扇5测试失败		风5测失败
196	32		15		FAN6 Test Fail				FAN6TestFail			风扇6测试失败		风6测失败
197	32		15		FAN7 Test Fail				FAN7TestFail			风扇7测试失败		风7测失败
198	32		15		FAN8 Test Fail				FAN8TestFail			风扇8测试失败		风8测失败
199	32		15		Heater1 Test Fail			Heater1TestFail			加热器1测试失败		加热1测失败
200	32		15		Heater2 Test Fail			Heater2TestFail			加热器2测试失败		加热2测失败
201	32		15		Fan is Test					Fan is Test				风扇在测试			风扇在测试
202	32		15		Heater is Test				Heater is Test			加热器在测试		加热器在测
203	32		15		Version 106					Version 106				106版本				106版本
204	32		15		Fan1 DownLimit Speed			Fan1 DnLmt Spd			Fan1下限速度		Fan1 DnLmt Spd
205	32		15		Fan2 DownLimit Speed			Fan2 DnLmt Spd			Fan2下限速度		Fan2 DnLmt Spd
206	32		15		Fan3 DownLimit Speed			Fan3 DnLmt Spd			Fan3下限速度		Fan3 DnLmt Spd
