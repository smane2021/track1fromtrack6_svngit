﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			BMS				BMS		BMS				BMS
2	32			15			Rated Capacity				Rated Capacity		标称容量			标称容量
3	32			15			Communication Fail			Comm Fail		通讯中断		通讯中断
4	32			15			Existence State				Existence State		是否存在		是否存在
5	32			15			Existent				Existent		存在			存在
6	32			15			Not Existent				Not Existent		不存在			不存在
7	32			15			Battery Voltage				Batt Voltage		电池电压		电池电压
8	32			15			Battery Current				Batt Current		电池电流		电池电流
9	32			15			Battery Rating (Ah)			Batt Rating(Ah)		电池容量(Ah)		电池容量(Ah)
10	32			15			Battery Capacity (%)			Batt Cap (%)		电池容量(%)		电池容量(%)
11	32			15			Bus Voltage				Bus Voltage		母排电压		母排电压
12	32			15			Battery Center Temperature(AVE)		Batt Temp(AVE)		电芯平均温度		电芯平均温度
29	32			15			Yes					Yes			是			是
30	32			15			No					No			否			否
13	32			15			Max Cell Temperature			Max Cell Temp		最高电池温度		最高电池温度
31	32			15			High Voltage Alarm			HighVoltage		整体过压告警		整体过压告警
32	32			15			Low Voltage Alarm			LowVoltage		整体欠压告警		整体欠压告警
33	32			15			High Temperature Alarm			HighBattTemp		电池高温告警		电池高温告警
34	32			15			Low Temperature Alarm			LowBattTemp		电池低温告警		电池低温告警
35	32			15			Charge Over Current Alarm		ChargeOverCurr		充电过流告警		充电过流告警
36	32			15			Discharge Over Current Alarm		DisCharOverCurr		放电过流告警		放电过流告警
37	32			15			Battery State Of Health			Battery SOH		健康状况		健康状况
39	32			15			FET High Temperature Alarm		FETHighTemp		FET高温警报	FET高温警报
40	32			15			Tray Voltage Imbalance Alarm		TrayVoltImb		电压不平衡报警		电压不平衡报警
41	32			15			Current Limit Alarm			Current Limit		限流报警	限流报警
42	32			15			Voltage Sensor Alarm			VoltSensorAlm		电压传感器警报		电压传感器警报
43	32			15			Temperature Sensor Alarm		TempSensorAlm		温度传感器报警		温度传感器报警
44	32			15			Current Sensor Error Alarm		CurrSensorAlm		电流传感器错误警报			电流传感器错误警报
45	32			15			Cell Temperature Imbalance Alarm	CellTempImbal		电池温度不平衡警报	电池温度不平衡警报
46	32			15			PIN Error				PIN Error		PIN码错误		PIN码错误
47	32			15			High Voltage Protect			HighVoltProt		整体过压保护		整体过压保护
48	32			15			Low Voltage Protect			LowVoltageProt		整体欠压保护		整体欠压保护
49	32			15			High Temperature Protect		HighTempProt		高温保护		高温保护
50	32			15			High Charge Current Protect		HighChgCurrProt		高充电电流保护		高充电电流保护
51	32			15			High Discharge Current Protect		HiDischCurrProt		高放电电流保护		高放电电流保护
52	32			15			FET High Temperature Protect		FETHiTempProt		FET高温保护		FET高温保护
53	32			15			Voltage Sensor Error Protect		VltSensorProt		电压传感器错误保护		电压传感器错误保护
54	32			15			FET Failure Protect			FETFailureProt		FET故障保护		FET故障保护
55	32			15			Current Sensor Error Protect		CurrSensorProt		电流传感器错误保护		电流传感器错误保护
56	32			15			Cell Temp Imbalance Protect		CellTempImbProt		电池温度失衡保护		电池温度失衡保护
57	32			15			Cell Voltage Imbalance Protect		CellVltImbProt		电池电压不平衡保护			电池电压不平衡保护
58	32			15			Shunt Wire Error Protect		ShuntWireProt		分流线错误保护		分流线错误保护
71	32			15			Device Address			Device Address		通讯地址		通讯地址



