﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Current				Batt Current		Batteriestrom				Batt.-Strom
2		32			15			Battery Capacity			Batt Capacity		Batteriekapazität			Batt.-Kapazität
3		32			15			Battery Voltage				Batt Voltage		BAtteriespannung			Batt.-Spannung
4		32			15			Ambient Temperature			Ambient Temp		Umgebungstemperatur			Umgebungstemp.
5		32			15			Acid Temperature			Acid Temp		Batteriesäuretemperatur			Batt.-Säuretemp
6		32			15			Total Time of Batt Temp GT 30ºC		ToT GT 30ºC		Gesamtzeit Batt.Temp gr.30ºC		t Temp.gr. 30ºC
7		32			15			Total Time of Batt Temp LT 10ºC		ToT LT 10ºC		Geasmtzeit Batt.Temp kl.10ºC		T Temp.kl. 10ºC
8		32			15			Battery Block 1 Voltage			Block 1 Volt		Spannung Batt.-Block 1			U Batt.Block 1
9		32			15			Battery Block 2 Voltage			Block 2 Volt		Spannung Batt.-Block 2			U Batt.Block 2
10		32			15			Battery Block 3 Voltage			Block 3 Volt		Spannung Batt.-Block 3			U Batt.Block 3
11		32			15			Battery Block 4 Voltage			Block 4 Volt		Spannung Batt.-Block 4			U Batt.Block 4
12		32			15			Battery Block 5 Voltage			Block 5 Volt		Spannung Batt.-Block 5			U Batt.Block 5
13		32			15			Battery Block 6 Voltage			Block 6 Volt		Spannung Batt.-Block 6			U Batt.Block 6
14		32			15			Battery Block 7 Voltage			Block 7 Volt		Spannung Batt.-Block 7			U Batt.Block 7
15		32			15			Battery Block 8 Voltage			Block 8 Volt		Spannung Batt.-Block 8			U Batt.Block 8
16		32			15			Battery Block 9 Voltage			Block 9 Volt		Spannung Batt.-Block 9			U Batt.Block 9
17		32			15			Battery Block 10 Voltage		Block 10 Volt		Spannung Batt.-Block 10			U Batt.Block 10
18		32			15			Battery Block 11 Voltage		Block 11 Volt		Spannung Batt.-Block 11			U Batt.Block 11
19		32			15			Battery Block 12 Voltage		Block 12 Volt		Spannung Batt.-Block 12			U Batt.Block 12
20		32			15			Battery Block 13 Voltage		Block 13 Volt		Spannung Batt.-Block 13			U Batt.Block 13
21		32			15			Battery Block 14 Voltage		Block 14 Volt		Spannung Batt.-Block 14			U Batt.Block 14
22		32			15			Battery Block 15 Voltage		Block 15 Volt		Spannung Batt.-Block 15			U Batt.Block 15
23		32			15			Battery Block 16 Voltage		Block 16 Volt		Spannung Batt.-Block 16			U Batt.Block 16
24		32			15			Battery Block 17 Voltage		Block 17 Volt		Spannung Batt.-Block 17			U Batt.Block 17
25		32			15			Battery Block 18 Voltage		Block 18 Volt		Spannung Batt.-Block 18			U Batt.Block 18
26		32			15			Battery Block 19 Voltage		Block 19 Volt		Spannung Batt.-Block 19			U Batt.Block 19
27		32			15			Battery Block 20 Voltage		Block 20 Volt		Spannung Batt.-Block 20			U Batt.Block 20
28		32			15			Battery Block 21 Voltage		Block 21 Volt		Spannung Batt.-Block 21			U Batt.Block 21
29		32			15			Battery Block 22 Voltage		Block 22 Volt		Spannung Batt.-Block 22			U Batt.Block 22
30		32			15			Battery Block 23 Voltage		Block 23 Volt		Spannung Batt.-Block 23			U Batt.Block 23
31		32			15			Battery Block 24 Voltage		Block 24 Volt		Spannung Batt.-Block 24			U Batt.Block 24
32		32			15			Battery Block 25 Voltage		Block 25 Volt		Spannung Batt.-Block 25			U Batt.Block 25
33		32			15			Shunt Voltage				Shunt Voltage		Spannung Shunt				Spannung Shunt
34		32			15			Battery Leakage				Batt Leakage		Batteriesäure läuft aus			Undichte Batt.
35		32			15			Low Acid Level				Low Acid Level		Niedriger Batt.Säurepegel		Niedr.Säurepeg.
36		32			15			Battery Disconnected			Battery Discon		Batterie abgeschaltet			Batt.abgeschalt
37		32			15			High Battery Temperature		High Batt Temp		Hohe Batterietemperatur			Hohe Batt.Temp.
38		32			15			Low Battery Temperature			Low Batt Temp		Niedrige Batterietemperatur		Niedr.Batt.Temp
39		32			15			Block Voltage Difference		Block Volt Diff		Blockspannungsdifferenz			Block U Diff.
40		32			15			Battery Shunt Size			Batt Shunt Size		Batterie Shunt Größe			Batt.Shuntwert
41		32			15			Block Voltage difference		Block Volt Diff		Blockspannungsdifferenz			Block U Diff.
42		32			15			High Battery Temp Limit			High Temp Limit		Limit Batt.Temp hoch			Lim.Bat.T.hoch
43		32			15			High Battery Temp Limit Hyst		High Temp Hyst		Hysterese Batt.-Temp hoch		Hyst.Bat.T.hoch
44		32			15			Low Battery Temp Limit			Low Temp Limit		Limit Batt.Temp niedrig			Lim.Bat.T.niedr
45		32			15			Low Battery Temp Limit Hyst		Low Temp Hyst		Hysterese Batt.-Temp niedrig		Hyst.Bat.T.nied
46		32			15			Current Limit Exceeded			Over Curr Limit		Limit Überstrom				Limit Überstrom
47		32			15			Battery Leakage				Battery Leakage		Batteriesäure läuft aus			Undichte Batt.
48		32			15			Low Acid Level				Low Acid Level		Niedriger Batt.Säurepegel		Niedr.Säurepeg.
49		32			15			Battery Disconnected			Battery Discon		Batterie abgeschaltet			Batt.abgeschalt
50		32			15			High Battery Temperature		High Batt Temp		Hohe Batterietemperatur			Hohe Batt.Temp.
51		32			15			Low Battery Temperature			Low Batt Temp		Niedrige Batterietemperatur		Niedr.Batt.Temp
52		32			15			Cell Voltage Difference			Cell Volt Diff		Zellenspannungsdifferenz		Zelle U Diff.
53		32			15			SM-BAT Unit Failure			SM-BAT Failure		SM-BAT Fehler				SM-BAT Fehler
54		32			15			Battery Disconnected			Battery Discon		Batterie abgeschaltet			Batt.abgeschalt
55		32			15			No					No			Nein					Nein
56		32			15			Yes					Yes			Ja					Ja
57		32			15			No					No			Nein					Nein
58		32			15			Yes					Yes			Ja					Ja
59		32			15			No					No			Nein					Nein
60		32			15			Yes					Yes			Ja					Ja
61		32			15			No					No			Nein					Nein
62		32			15			Yes					Yes			Ja					Ja
63		32			15			No					No			Nein					Nein
64		32			15			Yes					Yes			Ja					Ja
65		32			15			No					No			Nein					Nein
66		32			15			Yes					Yes			Ja					Ja
67		32			15			No					No			Nein					Nein
68		32			15			Yes					Yes			Ja					Ja
69		32			15			SM Battery				SM Batt			SM BAT					SM Bat
70		32			15			Over Battery Current			Ov Batt Current		Batterieüberstrom			Batt.Überstrom
71		32			15			Battery Capacity(%)			Batt Cap(%)		Batteriekapazität (%)			Batt.-Kapaz.(%)
72		32			15			SM-BAT Failure				SM-BAT Fail		SM-BAT Fehler				SM-BAT Fehler
73		32			15			No					No			Nein					Nein
74		32			15			Yes					Yes			Ja					Ja
75		32			15			AI 4					AI 4			Analoger Eingang 4			Analog.Eing.4
76		32			15			AI 7					AI 7			Analoger Eingang 7			Analog.Eing.7
77		32			15			DI 4					DI 4			Digitaler Eingang 4			Digit.Eing.4
78		32			15			DI 5					DI 5			Digitaler Eingang 5			Digit.Eing.5
79		32			15			DI 6					DI 6			Digitaler Eingang 6			Digit.Eing.6
80		32			15			DI 7					DI 7			Digitaler Eingang 7			Digit.Eing.7
81		32			15			DI 8					DI 8			Digitaler Eingang 8			Digit.Eing.8
82		32			15			Relay 1 Status				Relay 1 Status		Status Relais 1				Stat.Relais 1
83		32			15			Relay 2 Status				Relay 2 Status		Status Relais 2				Stat.Relais 2
84		32			15			No					No			Nein					Nein
85		32			15			Yes					Yes			Ja					Ja
86		32			15			No					No			Nein					Nein
87		32			15			Yes					Yes			Ja					Ja
88		32			15			No					No			Nein					Nein
89		32			15			Yes					Yes			Ja					Ja
90		32			15			No					No			Nein					Nein
91		32			15			Yes					Yes			Ja					Ja
92		32			15			No					No			Nein					Nein
93		32			15			Yes					Yes			Ja					Ja
94		32			15			Off					Off			Aus					Aus
95		32			15			On					On			An					An
96		32			15			Off					Off			Aus					Aus
97		32			15			On					On			An					An
98		32			15			Relay 1 On/Off				Relay1 On/Off		Relais 1 An/Aus				Relais1 An/Aus
99		32			15			Relay 2 On/Off				Relay2 On/Off		Relais 2 An/Aus				Relais2 An/Aus
100		32			15			AI 2					AI 2			Analoger Eingang 2			Analog.Eing.2
101		32			15			Battery Temperature Sensor Failure	T Sensor Fail		Temp.Sensor Fehler			TempSens.Fehler
102		32			15			Low Capacity				Low Capacity		Niedrige Kapazität			Niedr.Kapaz.
103		32			15			Existence State				Existence State		Aktueller Status			Akt.Status
104		32			15			Existent				Existent		vorhanden				vorhanden
105		32			15			Non-Existent				Non-Existent		nicht vorhanden				n.vorhanden
106		32			15			Battery Not Responding			Not Responding		keine Antwort				keine Antwort
107		32			15			Response				Response		antwortet				antwortet
108		32			15			Not Responding				Not Responding		keine Antwort				keine Antwort
109		32			15			Rated Capacity				Rated Capacity		Nominale Kapazität			nom.Kapazität
110		32			15			Used by Battery Management		Batt Manage		Benutzt vom Batt.-Management		Batt.Manager
111		32			15			SM-BAT High Temperature Limit		SMBATHiTempLim		SMBAT Limit Temp hoch			Lim.Temp.hoch
112		32			15			SM-BAT Low Temperature Limit		SMBATLoTempLim		SMBAT Limit Temp niedrig		Lim.Temp.niedr.
113		32			15			SM-BAT Enable Temperature Sensor	SMBATTempEnable		SMBAT TempSensor aktivieren		TempSens.aktiv
114		32			15			Battery Not Responding			Not Responding		SMBAT antwortet nicht			keine Antwort
115		32			15			Temperature Sensor not Used		TempSens Unused		SMBAU TempSensor deaktivieren		TempSens.deakt.
116		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		Batterie Stromunsymmetrie Alarm		BattStromsymmet
