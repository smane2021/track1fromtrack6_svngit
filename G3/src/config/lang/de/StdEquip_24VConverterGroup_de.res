﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			24V Converter Group			24V Conv Group		24V DC/DC Wandlergruppe			24VDC/DC Gruppe
19		32			15			Shunt 3 Rated Current			Shunt 3 Current		Max. Shunt Strom			Shunt 3 Strom
21		32			15			Shunt 3 Rated Voltage			Shunt 3 Voltage		Max. Shunt Spannung			Shunt 3 Spann.
26		32			15			Closed					Closed			Geschlossen				Geschlossen
27		32			15			Open					Open			Offen					Offen
29		32			15			No					No			Nein					Nein
30		32			15			Yes					Yes			Ja					Ja
3002		32			15			Converter Installed			Conv Installed		Wechselrichter installiert		WR installiert
3003		32			15			No					No			Nein					Nein
3004		32			15			Yes					Yes			Ja					Ja
3005		32			15			Under Voltage				Under Volt		Unterspannung				Unterspannung
3006		32			15			Over Voltage				Over Volt		Überspannung				Überspannung
3007		32			15			Over Current				Over Current		Überstrom				Überstrom
3008		32			15			Under Voltage				Under Volt		Unterspannung				Unterspannung
3009		32			15			Over Voltage				Over Volt		Überspannung				Überspannung
3010		32			15			Over Current				Over Current		Überstrom				Überstrom
3011		32			15			Voltage					Voltage			Spannung				Spannung
3012		32			15			Total Current				Total Current		Gesamtstrom				Gesamtstrom
3013		32			15			Input Current				Input Current		Eingangsstrom				Eingangsstrom
3014		32			15			Efficiency				Efficiency		Wirkungsgrad				Wirkungsgrad
