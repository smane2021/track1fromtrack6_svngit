﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Li-Ion Battery String			LiBattString		Li-Ion batterie				LIIon-Batt
2		32			15			Batt Voltage				Batt Voltage		Batteriespannung			Spannung
3		32			15			Battery Terminal Voltage		Bat TermVoltg		Batterieterminalspannung		TermTemperatur
4		32			15			Battery Current				Bat Current		Batteriestrom				Strom
5		32			15			Cell Temperature			Cell Temp		Zelltemperatur				ZellTemperatur
6		32			15			Switch Temperature			Switch Temp		Swichtemperatur				SwitchTemp.
7		32			15			State of Charge				StateOfCharge		Ladestatus				Ladestatus
8		32			15			Battery LED Status			BatLED Status		LED status				LED-status
9		32			15			Battery Relay Status			BatRelyStatus		Batterierelaisstatus			Relais Status
10		32			15			Charge Enabled				Charge Enab		Ladung aktiv				Ladung Aktiv
11		32			15			Battery Discharging			Bat Discharg		Batterie Entladung			Entladung
12		32			15			Battery Charging			Bat Charg		Batterie Ladung				Ladung
13		32			15			DisCharging				DisCharging		Entladung				Entladung
14		32			15			Charging(5A)				Charging(5A)		Ladung(5A)				Ladung(5A)
15		32			15			DisCharging(5A)				DisCharging(5A)		Entladung(5A)				Entladung(5A)
16		32			15			DisChar Enabled14			DisCharEnabled14	Entladung aktiviert			Entladung akt
17		32			15			Char Enabled15				Char Enabled15		Ladung aktiviert			Ladung aktiv
18		32			15			Battery Temperature Fault		BattTemp Fault		Batterie Temperaturfehler		Temp.fehler
19		32			15			Battery Current Fault			Current Fault		Strom Fehler				Strom Fehler
20		32			15			Battery Hardware Failure		Hardware Fail		Hardware Fehler				HW Fehler
21		32			15			Battery Over-voltage			Over-volt		Überspannung				Überspannung
22		32			15			Battery Low-Voltage			Low-volt		Unterspannung				Unterspannung
23		32			15			Cell Volt Deviation			CellVoltDeviat		Zellspannung Differenz			Zellsp.Diff
24		32			15			Low Cell Voltage			Lo Cell Volt		Zellspannung tief			Zellsp tief
25		32			15			High Cell Voltage			Hi Cell Volt		Zellspannung hoch			Zellsp hoch
26		32			15			High Cell Temperature			Hi Cell Temp		Zelltemperatur hoch			Zelltemp hoch
27		32			15			High Switch DisTemp			HiSwitchDisTemp		Switchtemperatur hoch			Switchtemp hoch
28		32			15			Charge Short Circuit			Char ShortCirc		Ladung Kurzscluss			Ldg Kurzschluss
29		32			15			DisChar Short Circuit			DisCharShortCirc	Entladung Kurzschluss			EntLdg Kurzschl
30		32			15			High Switch CharTemp			HiSwitchCharTemp	Hohe SwitchChartemperatur		SwitchTemp hoch
31		32			15			Hardware Fail 20			HardwareFail20		Hardwarefehler 20			HW Fehler 20
32		32			15			Hardware Fail 21			HardwareFail21		Hardwarefehler 21			HW Fehler 21
33		32			15			High Charge Curr			Hi Charge Curr		Hoher Ladestrom				Ladestrom hoch
34		32			15			High DisCharge Curr			Hi DisCharCurr		Hoher Entladestrom			Entladestr hoch
35		32			15			Comm Interrupt				Comm Interr		Kommunikationsfehler			Komm Fehler
36		32			15			Address Of Li-Ion Module		Module Address		Ion Moduladresse			Moduladresse
37		32			15			Alarm					Alarm			Alarm					Alarm
38		32			15			Normal					Normal			Normal					Normal
39		32			15			Full On Green				FullOnGreen		Grün Dauerhaft				Grün Dauerhaft
40		32			15			Blinking Green				Blink Green		Grün Blinkend				Grün Blinkend
41		32			15			Full On Yellow				FullOnYellow		Gelb Dauerhaft				Gelb Dauerhaft
42		32			15			Blinking Yellow				Blink Yellow		Gelb Blinkend				Gelb Blinkend
43		32			15			Blinking Red				Blink Red		Rot Blinkend				Rot Blinkend
44		32			15			Full On Red				FullOnRed		Rot Dauerhaft				Rot Dauerhaft
45		32			15			LED Off					LED Off			LED abgeschaltet			LED aus
46		32			15			Batt Rating(Ah)				Batt Rating(Ah)		Batteriekapazität(Ah)			BattKapaz(Ah)
48		32			15			Battery Disconnected Status		BattDisconnStat		Batterie Status				Batt Status
49		32			15			Battery Disconnected			Bat Discon		Abgetrennt				Abgetrennt
96		32			15			Rated Capacity				Rated Capacity		Batteriekapazität			BattKapaz
99		32			15			Interrupt Stat				Interrupt Stat		Kommunikationstatus			KommStatus
100		32			15			Exist Stat				Exist Stat		Kommunikation				Kommunikation
101		32			15			True					True			Ja					Ja
102		32			15			False					False			Nein					Nein
