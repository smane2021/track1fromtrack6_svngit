﻿#
#  Locale language support:de
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
de


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			Output AC Voltage			Output AC Volt			Ausgang AC Spannung		Ausgang AC Span
2	32			15			Output AC Current			Output AC Curr			Ausgang AC Aktuell		Ausgang AC Aktu
3	32			15			Output Apparent Power			Apparent Power			Scheinbare Kraft		Scheinb Kraft
4	32			15			Output Active Power			Active Power			Aktive Macht		Aktive Macht
5	32			15			Input AC Voltage			Input AC Volt			Eingangs-AC-Spannung	Eingan-AC-Span	
6	32			15			Input AC Current			Input AC Curr			Eingangs-AC-Aktuell	Eingan-AC-Aktu	
7	32			15			Input AC Power				Input AC Power		Eingangs AC netz	EingangsACnetz
8	32			15			Input AC Power				Input AC Power		Eingangs AC netz	EingangsACnetz
9	32			15			Input AC Frequency			Input AC Freq		Eingangs-AC-Frequenz		Eingan-DC-Freq
10	32			15			Input DC Voltage			Input DC Volt			Eingangs-DC-Spannung	Eingan-DC-Span	
11	32			15			Input DC Current			Input DC Curr			Eingangs-DC-Aktuell	Eingan-DC-Aktu	
12	32			15			Input DC Power				Input DC Power		Eingangs DC netz	EingangsDCnetz
13	32			15			Communication Failure			Comm Fail		Kommunikation Ausfallen		Komm Ausfallen
14	32			15			Existence State				Existence State		Existenzstaat		Existenzstaat
	
98	32			15			TSI Inverter				TSI Inverter		TSI Wandler		TSI Wandler
101	32			15			Fan Failure				Fan Fail		Lüfterdefekt		Lüfterdefekt	
102	32			15			Too Many Starts				Too Many Starts		Zu viele Starts		Zu viele Starts	
103	32			15			Overload Too Long			Overload Long		Überladung zu lang		Überladung lang	
104	32			15			Out Of Sync				Out Of Sync		Nicht synchron		Nicht synchron	
105	32			15			Temperature Too High			Temp Too High		Temperatur zu hoch	Temp zu hoch	
106	32			15			Communication Bus Failure		Com Bus Fail		Kommunikationsbusfehler	Kommbusfehler	
107	32			15			Communication Bus Confilct		Com BusConfilct		Kommunikationsbus-Konflikt	Kommbus-Konflik
108	32			15			No Power Source				No Power		Keine Energie		Keine Energie
109	32			15			Communication Bus Failure		Com Bus Fail		Kommunikationsbusfehler	Kommbusfehler	
110	32			15			Phase Not Ready				Phase Not Ready		Phase nicht bereit	PhaseNichBereit
111	32			15			Inverter Mismatch			Inv Mismatch		Inverter Mismatch	Inverter Mismatch
112	32			15			Backfeed Error				Backfeed Error		Nachspeise Fehler	Nachspeise Fehler	
113	32			15			T2S Communication Bus Failure		Com Bus Fail		T2S Kommunikationsbusfehler	Kommbusfehler	
114	32			15			T2S Communication Bus Failure		Com Bus Fail		T2S Kommunikationsbusfehler	Kommbusfehler	
115	32			15			Overload Current			Overload Curr		Überlaststrom		Überlaststrom
116	32			15			Communication Bus Mismatch		Com Bus Mismatch	Kommunikationsbus-Mismatch	Kommbus-Mismatch
117	32			15			Temperature Derating			Temp Derating		Temperatur-Derating	Temp-Derating
118	32			15			Overload Power				Overload Power		Überlastleistung	Überlastleistung	
119	32			15			Undervoltage Derating			Undervolt Derat		Unterspannung Derating	UnterspanDerat	
120	32			15			Fan Failure				Fan Failure		Lüfterdefekt		Lüfterdefekt	
121	32			15			Remote Off				Remote Off		Fern Aus		Fern Aus	
122	32			15			Manually Off				Manually Off		Manuell Aus		Manuell Aus	
123	32			15			Input AC Voltage Too Low		Input AC Too Low	AC-Eingangsspannung zu niedrig		AC-EingaNiedrig
124	32			15			Input AC Voltage Too High		Input AC Too High	AC-Eingangsspannung zu Hoch		AC-EingaHoch
125	32			15			Input AC Voltage Too Low		Input AC Too Low	AC-Eingangsspannung zu niedrig		AC-EingaNiedrig
126	32			15			Input AC Voltage Too High		Input AC Too High	AC-Eingangsspannung zu Hoch		AC-EingaHoch
127	32			15			Input AC Voltage Inconformity		Input AC Inconform	Eingangs-AC-Spannungskonformität	Einga-AC-konfor
128	32			15			Input AC Voltage Inconformity		Input AC Inconform	Eingangs-AC-Spannungskonformität	Einga-AC-konfor
129	32			15			Input AC Voltage Inconformity		Input AC Inconform	Eingangs-AC-Spannungskonformität	Einga-AC-konfor
130	32			15			Power Disabled				Power Disabled		Deaktiviert		Deaktiviert	
131	32			15			Input AC Inconformity			Input AC Inconform	Eingangs-AC-konformität	Einga-AC-konfor
132	32			15			Input AC THD Too High			Input AC THD High	Eingang AC THD zu hoch	EingaACTHD hoch
133	32			15			Output AC Not Synchronized		AC Not Syned		Ausgang AC nicht synchronisiert		AC nicht synch
134	32			15			Output AC Not Synchronized		AC Not Synced		Ausgang AC nicht synchronisiert		AC nicht synch
135	32			15			Inverters Not Synchronized		Inverters Not Synced	Nicht synchron		Nicht synchron	
136	32			15			Synchronization Failure			Sync Failure		Synchronisierungsfehler	Synchfehler	
137	32			15			Input AC Voltage Too Low		AC Voltage Too Low	AC-Eingangsspannung zu niedrig		AC-EingaNiedrig
138	32			15			Input AC Voltage Too High		AC Voltage Too High	AC-Eingangsspannung zu Hoch		AC-EingaHoch
139	32			15			Input AC Frequency Too Low		AC Frequency Low	Eingangs-AC-Frequenz zu niedrig		FreqZuNiedrig
140	32			15			Input AC Frequency Too High		AC Frequency High	Eingangs-AC-Frequenz zu Hoch		FreqZuHoch
141	32			15			Input DC Voltage Too Low		Input DC Too Low		DC-Eingangsspannung zu niedrig		DC-EingaNiedrig
142	32			15			Input DC Voltage Too High		Input DC Too High		DC-Eingangsspannung zu Hoch		DC-EingaHoch
143	32			15			Input DC Voltage Too Low		Input DC Too Low		DC-Eingangsspannung zu niedrig		DC-EingaNiedrig
144	32			15			Input DC Voltage Too High		Input DC Too High		DC-Eingangsspannung zu Hoch		DC-EingaHoch
145	32			15			Input DC Voltage Too Low		Input DC Too Low		DC-Eingangsspannung zu niedrig		DC-EingaNiedrig
146	32			15			Input DC Voltage Too Low		Input DC Too Low		DC-Eingangsspannung zu niedrig		DC-EingaNiedrig
147	32			15			Input DC Voltage Too High		Input DC Too High		DC-Eingangsspannung zu Hoch		DC-EingaHoch
148	32			15			Digital Input 1 Failure			DI1 Failure		DI1 Fehler		DI1 Fehler
149	32			15			Digital Input 2 Failure			DI2 Failure		DI1 Fehler		DI1 Fehler
150	32			15			Redundancy Lost				Redundancy Lost		Redundanz verloren	Redund verlo
151	32			15			Redundancy+1 Lost			Redund+1 Lost		Redundanz+1 verloren	Redund+1 verlo
152	32			15			System Overload				Sys Overload		Systemüberladung	Sysüberladung
153	32			15			Main Source Lost			Main Lost		Hauptverlust		Hauptverlust	
154	32			15			Secondary Source Lost			Secondary Lost		Sekundär verloren	SekundärVerlo
155	32			15			T2S Bus Failure				T2S Bus Failure		T2S Busfehler		T2S Busfehler	
156	32			15			T2S Failure				T2S Failure		T2S fehler		T2S fehler	
157	32			15			Log Full				Log Full		Log Voll	Log Voll
158	32			15			T2S Flash Error				Flash Error		T2S Flash Fehler		Flash Fehler	
159	32			15			Check Log File				Check Log File		Überprüfen Sie Log-Datei	ÜberSieLog-Date	
160	32			15			Module Lost				Module Lost		Modul verloren		Modul verloren	
