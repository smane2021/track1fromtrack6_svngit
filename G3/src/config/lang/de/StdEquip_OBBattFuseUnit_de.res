﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Fuse 1 Voltage				Fuse 1 Voltage		Sicherung 1 Spannung			Sich.1 Spann.
2		32			15			Fuse 2 Voltage				Fuse 2 Voltage		Sicherung 2 Spannung			Sich.2 Spann.
3		32			15			Fuse 3 Voltage				Fuse 3 Voltage		Sicherung 3 Spannung			Sich.3 Spann.
4		32			15			Fuse 4 Voltage				Fuse 4 Voltage		Sicherung 4 Spannung			Sich.4 Spann.
5		32			15			Fuse 1 Alarm				Fuse 1 Alarm		Alarm Sicherung 1			Alarm Sich.1
6		32			15			Fuse 2 Alarm				Fuse 2 Alarm		Alarm Sicherung 2			Alarm Sich.2
7		32			15			Fuse 3 Alarm				Fuse 3 Alarm		Alarm Sicherung 3			Alarm Sich.3
8		32			15			Fuse 4 Alarm				Fuse 4 Alarm		Alarm Sicherung 4			Alarm Sich.4
9		32			15			Battery Fuse				Battery Fuse		Batteriesicherung			Batt.Sicherung
10		32			15			On					On			Normal					Normal
11		32			15			Off					Off			Aus					Aus
12		32			15			Fuse 1 Status				Fuse 1 Status		Status Sicherung 1			Status Sich.1
13		32			15			Fuse 2 Status				Fuse 2 Status		Status Sicherung 2			Status Sich.2
14		32			15			Fuse 3 Status				Fuse 3 Status		Status Sicherung 3			Status Sich.3
15		32			15			Fuse 4 status				Fuse 4 Status		Status Sicherung 4			Status Sich.4
16		32			15			State					State			Status					Status
17		32			15			Failure					Failure			Fehler					Fehler
18		32			15			No					No			Nein					Nein
19		32			15			Yes					Yes			Ja					Ja
20		32			15			Battery Fuse Number			BattFuse Number		Nummer Batteriesich.			Nr.Batt.Sich.
21		32			15			0					0			0					0
22		32			15			1					1			1					1
23		32			15			2					2			2					2
24		32			15			3					3			3					3
25		32			15			4					4			4					4
26		32			15			5					5			5					5
27		32			15			6					6			6					6
28		32			15			Fuse 5 Voltage				Fuse 5 Voltage		Spannung				Sicherung 5
29		32			15			Fuse 6 Voltage				Fuse 6 Voltage		Spannung				Sicherung 6
30		32			15			Fuse 5 Alarm				Fuse 5 Alarm		Alarm Sicherung 5			Alarm Sich.5
31		32			15			Fuse 6 Alarm				Fuse 6 Alarm		Alarm Sicherung 6			Alarm Sich.6
32		32			15			Fuse 5 Status				Fuse 5 Status		Status Sicherung 5			Status Sich.5
33		32			15			Fuse 6 Status				Fuse 6 Status		Status Sicherung 6			Status Sich.6
