﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			BMS				BMS		BMS				BMS
2	32			15			Rated Capacity				Rated Capacity		Nennleistung				Nennleistung
3	32			15			Communication Fail			Comm Fail		Komm Fehlgeschlagen			Komm Fehl
4	32			15			Existence State				Existence State		Existenz Staat				Existenz Staat
5	32			15			Existent				Existent		Existieren				Existieren
6	32			15			Not Existent				Not Existent		Nicht beständig				Nicht beständig
7	32			15			Battery Voltage				Batt Voltage		Batteriespannung			Batt Spannung
8	32			15			Battery Current				Batt Current		Batteriestrom				Batteriestrom
9	32			15			Battery Rating (Ah)			Batt Rating(Ah)		Batterie Bewertung (Ah)			BattBewertung
10	32			15			Battery Capacity (%)			Batt Cap (%)		Batteriekapazität(%)			BattKap(%)
11	32			15			Bus Voltage				Bus Voltage		Busspannung				Busspannung
12	32			15			Battery Center Temperature(AVE)		Batt Temp(AVE)		Batterie-Center-Temperatur(AVE)		Batt Temp(AVE)
13	32			15			Ambient Temperature			Ambient Temp		Umgebungstemperatur			UmgebungsTemp
29	32			15			Yes					Yes			Ja					Ja
30	32			15			No					No			Nein					Nein
31	32			15			Single Over Voltage Alarm		SingleOverVolt		Single Überspannung Alarm		SingleÜberspan
32	32			15			Single Under Voltage Alarm		SingleUnderVolt		Single Unter Spannung Alarm		SingleUnterSpan
33	32			15			Whole Over Voltage Alarm		WholeOverVolt		Ganze Überspannungsalarm		Ganze Überspan
34	32			15			Whole Under Voltage Alarm		WholeUnderVolt		Ganze Unter Spanalarm			Ganze Unter Span
35	32			15			Charge Over Current Alarm		ChargeOverCurr		Ladung über Strom			LadungüberStrom
36	32			15			Discharge Over Current Alarm		DisCharOverCurr		Entladung über Strom Alarm		EntladüberStrom
37	32			15			High Battery Temperature Alarm		HighBattTemp		Hohe Batterietemperaturalarm		HoheBattTemp
38	32			15			Low Battery Temperature Alarm		LowBattTemp		Niedrige BattTempalarm			NiedrigeBatTemp
39	32			15			High Ambient Temperature Alarm		HighAmbientTemp		Hohe Umgebungstemperatur		Hohe UmgebTemp
40	32			15			Low Ambient Temperature Alarm		LowAmbientTemp		Niedriger Umgebungs-Temp		NiedrUmgebTemp
41	32			15			High PCB Temperature Alarm		HighPCBTemp		Hohe PCB-Temperaturalarm		Hohe PCB-Temp
42	32			15			Low Battery Capacity Alarm		LowBattCapacity		Niedriger BatKapalarm			NiedrigerBatKap
43	32			15			High Voltage Difference Alarm		HighVoltDiff		Hochspannungsdifferenzalarm		HochSpanDiff
44	32			15			Single Over Voltage Protect		SingOverVProt		Single Überspannung Schützen		SingleÜberVSch
45	32			15			Single Under Voltage Protect		SingUnderVProt		Single UnterSpannungSchützen		SingleUnterVSch
46	32			15			Whole Over Voltage Protect		WholeOverVProt		Ganze ÜberspannungsSchützen		Ganze ÜberVSch
47	32			15			Whole Under Voltage Protect		WholeUnderVProt		Ganze UnterspannungsSchützen		GanzeUnterVSch
48	32			15			Short Circuit Protect			ShortCircProt		Kurzschlussschutz			Kurzschlusssch
49	32			15			Over Current Protect			OverCurrProt		Überstromschutz				Überstromsch
50	32			15			Charge High Temperature Protect		CharHiTempProt		Ladung Hochtemperaturschutz		LadungHochTemp
51	32			15			Charge Low Temperature Protect		CharLoTempProt		Ladung Niedrige Tempschützen		LadungNiedrTemp
52	32			15			Discharge High Temp Protect		DischHiTempProt		Entladung Hochtemperaturschutz		EntladHochTemp
53	32			15			Discharge Low Temp Protect		DischLoTempProt		Entladung Niedrige Temp Schützen	EntladNiedTemp
54	32			15			Front End Sample Comm Fault		SampCommFault		Front End Sample Comm Fehler		SampCommFehler
55	32			15			Temp Sensor Disconnect Fault		TempSensDiscFlt		Temp Sensor Trennfehler			TempSensTrefehl
56	32			15			Charging				Charging		Aufladeng				Aufladen
57	32			15			Discharging				Discharging		Entladung				Entladung
58	32			15			Charging MOS Breakover			CharMOSBrkover		Aufladung MOS Breakover			AufMOSBrkover
59	32			15			Dischanging MOS Breakover		DischMOSBrkover		Entladen MOS Kippstrom			EntlaMOSKippstr
60	32			15			Communication Address			CommAddress		Kommunikationsadresse			Kommadresse
61	32			15			Current Limit Activation		CurrLmtAct		Strombegrenzung Aktivierung		StrombegrenAkt



