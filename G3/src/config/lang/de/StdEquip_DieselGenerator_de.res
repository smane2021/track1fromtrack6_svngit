﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Diesel Battery Voltage			Dsl Batt Volt		GenSet Batteriespannung			U Batt.GenSet
2		32			15			Diesel Running				Diesel Running		GenSet läuft				GenSet läuft
3		32			15			Relay 2 Status				Relay2 Status		Status Relais 2				Status Relais2
4		32			15			Relay 3 Status				Relay3 Status		Status Relais 3				Status Relais3
5		32			15			Relay 4 Status				Relay4 Status		Status Relais 4				Status Relais4
6		32			15			Diesel Failure Status			Diesel Fail		Fehler GenSet				GenSet Fehler
7		32			15			Diesel Connected Status			Diesel Cnnected		GenSet angeschlossen			GenSet angeschl
8		32			15			Low Fuel Level Status			Low Fuel Level		Niedriger Dieselstand			wenig Diesel
9		32			15			High Water Temp Status			High Water Temp		Wassertemperatur hoch			Wassertemp.hoch
10		32			15			Low Oil Pressure Status			Low Oil Pressur		Niedriger Öldruck			Öldruck niedr.
11		32			15			Start Diesel				Start Diesel		Start GenSet				Start GenSet
12		32			15			Relay 2 On/Off				Relay 2 On/Off		Relais 2 Ein/Aus			Relais2 Ein/Aus
13		32			15			Relay 3 On/Off				Relay 3 On/Off		Relais 3 Ein/Aus			Relais3 Ein/Aus
14		32			15			Relay 4 On/Off				Relay 4 On/Off		Relais 4 Ein/Aus			Relais4 Ein/Aus
15		32			15			Battery Voltage Limit			Batt Volt Limit		Batteriespannungsbegrenzung		U Batt.begrenzt
16		32			15			Relay 1 Pulse Time			RLY 1 Puls Time		Relais 1 Pulszeit			Pulszeit Rel.1
17		32			15			Relay 2 Pulse Time			RLY 2 Puls Time		Relais 2 Pulszeit			Pulszeit Rel.2
18		32			15			Relay 1 Pulse Time			RLY 1 Puls Time		Relais 1 Pulszeit			Pulszeit Rel.1
19		32			15			Relay 2 Pulse Time			RLY 2 Puls Time		Relais 2 Pulszeit			Pulszeit Rel.2
20		32			15			Low DC Voltage				Low DC Voltage		Niedrige DC-Spannung			DC-Span.niedrig
21		32			15			Diesel Supervision Failure		Supervision Fail	Fehler GenSet Überwachung		G.S.Überw.Fehl.
22		32			15			Diesel Generator Failure		Generator Fail		Fehler GenSet				Fehler GenSet
23		32			15			Diesel Generator Connected		Gen Connected		GenSet angeschlossen			GenSet angeschl
24		32			15			Not Running				Not Running		läuft nicht				läuft nicht
25		32			15			Running					Running			läuft					läuft
26		32			15			Off					Off			Aus					Aus
27		32			15			On					On			An					An
28		32			15			Off					Off			Aus					Aus
29		32			15			On					On			An					An
30		32			15			Off					Off			Aus					Aus
31		32			15			On					On			An					An
32		32			15			No					No			Nein					Nein
33		32			15			Yes					Yes			Ja					Ja
34		32			15			No					No			Nein					Nein
35		32			15			Yes					Yes			Ja					Ja
36		32			15			Off					Off			Aus					Aus
37		32			15			On					On			An					An
38		32			15			Off					Off			Aus					Aus
39		32			15			On					On			An					An
40		32			15			Off					Off			Aus					Aus
41		32			15			On					On			An					An
42		32			15			Diesel Generator			Diesel Genrator		GenSet					Genset
43		32			15			Mains Connected				Mains Connected		Netz angeschlossen			Netz angeschl.
44		32			15			Diesel Shutdown				Diesel Shutdown		GenSet Stopp				GenSet Stopp
45		32			15			Low Fuel Level				Low Fuel Level		Niedriger Dieselstand			Wenig Diesel
46		32			15			High Water Temperature			High Water Temp		Wassertemperatur hoch			Wassertemp.hoch
47		32			15			Low Oil Pressure			Low Oil Press		Niedriger Öldruck			Öldruck niedr.
48		32			15			Supervision Fail			SMAC Fail		SM-AC Fehler				SM-AC Fehler
49		32			15			Low Oil Pressure Alarm Status		Low Oil Press		Niedriger Öldruck Status		Stat.Öldr.nied
50		32			15			Reset Low Oil Pressure Alarm		Clr OilPressArm		Rücks.Alarm niedr.Öldr.			Reset Alm.Öldr
51		32			15			State					State			Status					Status
52		32			15			Existence State				Existence State		Status vorhandene			Stat.vorhandene
53		32			15			Existent				Existent		vorhanden				vorhanden
54		32			15			Non-Existent				Non-Existent		nicht vorhanden				n. vorhanden
55		32			15			Total Run Time				Total Run Time		Laufzeit gesamt				Laufzeit gesamt
56		32			15			Maintenance Time Limit			Mtn Time Limit		Zeitlimit Wartung			Zeitlim.Wartung
57		32			15			Clear Total Run Time			Clr Run Time		Löschen gesamte Laufzeit		LöschenLaufzeit
58		32			15			Periodical Maintenance Required		Mtn Required		Regelmäßige Wartung notwendig		Wartung notwend
59		32			15			Maintenance Countdown Timer		Mtn DownTimer		Zeit bis zur nächsten Wartung		Wartungstimer
