﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Voltage L1-N				Volt L1-N		Spannung L1-N				Spann. L1-N
2		32			15			Voltage L2-N				Volt L2-N		Spannung L2-N				Spann. L2-N
3		32			15			Voltage L3-N				Volt L3-N		Spannung L3-N				Spann. L3-N
4		32			15			Voltage L1-L2				Volt L1-L2		Spannung L1-L2				Spann. L1-L2
5		32			15			Voltage L2-L3				Volt L2-L3		Spannung L2-L3				Spann. L2-L3
6		32			15			Voltage L3-L1				Volt L3-L1		Spannung L3-L1				Spann. L2-L1
7		32			15			Current L1				Curr L1			Strom L1				Strom L1
8		32			15			Current L2				Curr L2			Strom L2				Strom L2
9		32			15			Current L3				Curr L3			Strom L3				Strom L3
10		32			15			Real Power L1				Real Power L1		Watt L1					Watt L1
11		32			15			Real Power L2				Real Power L2		Watt L2					Watt L2
12		32			15			Real Power L3				Real Power L3		Watt L3					Watt L3
13		32			15			Apparent Power L1			Apparent Pow L1		VA L1					VA L1
14		32			15			Apparent Power L2			Apparent Pow L2		VA L2					VA L2
15		32			15			Apparent Power L3			Apparent Pow L3		VA L3					VA L3
16		32			15			Reactive Power L1			Reactive Pow L1		VAR L1					VAR L1
17		32			15			Reactive Power L2			Reactive Pow L2		VAR L2					VAR L2
18		32			15			Reactive Power L3			Reactive Pow L3		VAR L3					VAR L3
19		32			15			AC Frequency				AC Frequency		Netzfrequenz				Netzfrequenz
20		32			15			Communication State			Comm State		Kommunikationsstatus			Komm.Status
21		32			15			Existence State				Existence State		bestehender Status			bestehen.Status
22		32			15			AC Meter				AC Meter		AC Meter				AC Meter
23		32			15			On					On			An					An
24		32			15			Off					Off			Aus					Aus
25		32			15			Existent				Existent		vorhanden				vorhanden
26		32			15			Not Existent				Not Existent		nicht vorhanden				nicht vorhanden
27		32			15			Communication Fail			Comm Fail		Kommunikationsfehler			Komm.fehler
28		32			15			Phase Voltage				Phase Voltage		V L-N ACC				V L-N ACC
29		32			15			Line Voltage				Line Voltage		V L-L ACC				V L-L ACC
30		32			15			Total Real Power			Total Real Pow		Watt ACC				Watt ACC
31		32			15			Total Apparent Power			Total Appar Pow		VAR ACC					VAR ACC
32		32			15			Total Reactive Power			Total React Pow		VAR ACC					VAR ACC
33		32			15			DMD Watt ACC				DMD Watt ACC		DMD Watt ACC				DMD Watt ACC
34		32			15			DMD VA ACC				DMD VA ACC		DMD VA ACC				DMD VA ACC
35		32			15			PF L1					PF L1			PF L1					PF L1
36		32			15			PF L2					PF L2			PF L2					PF L2
37		32			15			PF L3					PF L3			PF L3					PF L3
38		32			15			PF ACC					PF ACC			PF ACC					PF ACC
39		32			15			Phase Sequence				Phase Sequence		Phasensequenz				Phasensequenz
40		32			15			L1-L2-L3				L1-L2-L3		L1-L2-L3				L1-L2-L3
41		32			15			L1-L3-L2				L1-L3-L2		L1-L3-L2				L1-L3-L2
42		32			15			Nominal Line Voltage			NominalLineVolt		Nominale Netzpannung			Nom.Netzspann.
43		32			15			Nominal Phase Voltage			Nominal PH-Volt		Nominale Phasenspannung			Nom.Phasensp.
44		32			15			Nominal Frequency			Nominal Freq		Nominale Frequenz			Nom.Frequenz
45		32			15			Mains Failure Alarm Limit 1		Mains Fail Alm1		Netzfehler Alarmschwelle 1		Netzfehler Alm1
46		32			15			Mains Failure Alarm Limit 2		Mains Fail Alm2		Netzfehler Alarmschwelle 2		Netzfehler Alm2
47		32			15			Frequency Alarm Limit			Freq Alarm Lmt		Netzfrequenz Alarmschwelle		Freq.fehler Alm
48		32			15			Current Alarm Limit			Curr Alm Limit		Alarmschwelle Strom			Strom Alm Limit

51		32			15			Line L1-L2 Over Voltage			L1-L2 Over Volt		Überspannung 1 Phasen AB		A-B Überspan1
52		32			15			Line L1-L2 Over Voltage 2		L1-L2 Over Volt2	Überspannung 2 Phasen AB		A-B Überspan2
53		32			15			Line L1-L2 Under Voltage		L1-L2 UnderVolt		Unterspannung 1 Phasen AB		A-B Unterspan1
54		32			15			Line L1-L2 Under Voltage 2		L1-L2 UnderVolt2	Unterspannung 2 Phasen AB		A-B Unterspan2

55		32			15			Line L2-L3 Over Voltage			L2-L3 Over Volt		Überspannung 1 Phasen BC		B-C Überspan1
56		32			15			Line L2-L3 Over Voltage 2		L2-L3 Over Volt2	Überspannung 2 Phasen BC		B-C Überspan2
57		32			15			Line L2-L3 Under Voltage		L2-L3 UnderVolt		Unterspannung 1 Phasen BC		B-C Unterspan1
58		32			15			Line L2-L3 Under Voltage 2		L2-L3 UnderVolt2	Unterspannung 2 Phasen BC		B-C Unterspan2

59		32			15			Line L3-L1 Over Voltage			L3-L1 Over Volt		Überspannung 1 Phasen CA		C-A Überspan1
60		32			15			Line L3-L1 Over Voltage 2		L3-L1 Over Volt2	Überspannung 2 Phasen CA		C-A Überspan2
61		32			15			Line L3-L1 Under Voltage		L3-L1 UnderVolt		Unterspannung 1 Phasen CA		C-A Unterspan1
62		32			15			Line L3-L1 Under Voltage 2		L3-L1 UnderVolt2	Unterspannung 2 Phasen CA		C-A Unterspan2

63		32			15			Phase L1 Over Voltage			L1 Over Volt		Überspannung 1 Phase A			PH-A Überspan1
64		32			15			Phase L1 Over Voltage 2			L1 Over Volt2		Überspannung 2 Phase A			PH-A Überspan2
65		32			15			Phase L1 Under Voltage			L1 UnderVolt		Unterspannung 1 Phase A			PH-A Unterspan1
66		32			15			Phase L1 Under Voltage 2		L1 UnderVolt2		Unterspannung 2 Phase A			PH-A Unterspan2

67		32			15			Phase L2 Over Voltage			L2 Over Volt		Überspannung 1 Phase B			PH-B Überspan1
68		32			15			Phase L2 Over Voltage 2			L2 Over Volt2		Überspannung 2 Phase B			PH-B Überspan2
69		32			15			Phase L2 Under Voltage 1		L2 UnderVolt		Unterspannung 1 Phase B			PH-B Unterspan1
70		32			15			Phase L2 Under Voltage 2		L2 UnderVolt2		Unterspannung 2 Phase B			PH-B Unterspan2

71		32			15			Phase L3 Over Voltage			L3 Over Volt		Überspannung 1 Phase C			PH-C Überspan1
72		32			15			Phase L3 Over Voltage 2			L3 Over Volt2		Überspannung 2 Phase C			PH-C Überspan2
73		32			15			Phase L3 Under Voltage			L3 UnderVolt		Unterspannung 1 Phase C			PH-C Unterspan1
74		32			15			Phase L3 Under Voltage 2		L3 UnderVolt2		Unterspannung 2 Phase C			PH-C Unterspan2

75		32			15			Mains Failure				Mains Failure		Netzfehler				Netzfehler
76		32			15			Severe Mains Failure			SevereMainsFail		Schwerer Netzfehler			Schw.Netzfehler
77		32			15			High Frequency				High Frequency		Frequenz hoch				Frequenz hoch
78		32			15			Low Frequency				Low Frequency		Frequenz niedrig			Frequen.niedrig
79		32			15			Phase L1 High Current			L1 High Curr		Hoher Strom Phase A			PH-A Strom hoch
80		32			15			Phase L2 High Current			L2 High Curr		Hoher Strom Phase B			PH-B Strom hoch
81		32			15			Phase L3 High Current			L3 High Curr		Hoher Strom Phase C			PH-C Strom hoch

82		32			15			DMD W MAX				DMD W MAX		DMD W MAX				DMD W MAX
83		32			15			DMD VA MAX				DMD VA MAX		DMD VA MAX				DMD VA MAX
84		32			15			DMD A MAX				DMD A MAX		DMD A MAX				DMD A MAX
85		32			15			KWH(+) TOT				KWH(+) TOT		KWH(+) TOT				KWH(+) TOT
86		32			15			KVARH(+) TOT				KVARH(+) TOT		KVARH(+) TOT				KVARH(+) TOT
87		32			15			KWH(+) PAR				KWH(+) PAR		KWH(+) PAR				KWH(+) PAR
88		32			15			KVARH(+) PAR				KVARH(+) PAR		KVARH(+) PAR				KVARH(+) PAR
89		32			15			Energy L1				Energy L1		KWH(+) L1				KWH(+) L1
90		32			15			Energy L2				Energy L2		KWH(+) L2				KWH(+) L2
91		32			15			Energy L3				Energy L3		KWH(+) L3				KWH(+) L3
92		32			15			KWH(+) T1				KWH(+) T1		KWH(+) T1				KWH(+) T1
93		32			15			KWH(+) T2				KWH(+) T2		KWH(+) T2				KWH(+) T2
94		32			15			KWH(+) T3				KWH(+) T3		KWH(+) T3				KWH(+) T3
95		32			15			KWH(+) T4				KWH(+) T4		KWH(+) T4				KWH(+) T4
96		32			15			KVARH(+) T1				KVARH(+) T1		KVARH(+) T1				KVARH(+) T1
97		32			15			KVARH(+) T2				KVARH(+) T2		KVARH(+) T2				KVARH(+) T2
98		32			15			KVARH(+) T3				KVARH(+) T3		KVARH(+) T3				KVARH(+) T3
99		32			15			KVARH(+) T4				KVARH(+) T4		KVARH(+) T4				KVARH(+) T4
100		32			15			KWH(-) TOT				KWH(-) TOT		KWH(-) TOT				KWH(-) TOT
101		32			15			KVARH(-) TOT				KVARH(-) TOT		KVARH(-) TOT				KVARH(-) TOT
102		32			15			HOUR					HOUR			Stunde					Stunde
103		32			15			COUNTER 1				COUNTER 1		Zähler 1				Zähler 1
104		32			15			COUNTER 2				COUNTER 2		Zähler 2				Zähler 2
105		32			15			COUNTER 3				COUNTER 3		Zähler 3				Zähler 3
