﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			System Voltage				System Voltage		Systemspannung				Systemspannung
2		32			15			Number of Rectifiers			No of Rects		Anzahl Gleichrichter			GR Anzahl
3		32			15			Rectifier Total Current			Rect Tot Curr		Gesamtstrom Gleichrichter		GR Gesamtstrom
4		32			15			Rectifier Lost				Rectifier Lost		Gleichrichter verloren			GR verloren
5		32			15			All Rectifiers Comm Fail		AllRectCommFail		Alle GR Komm.-fehler			Alle GR Komfehl
6		32			15			Communication Failure			Comm Failure		Kommunikationsfehler			Komm.fehler
7		32			15			Existence State				Existence State		Aktueller Status			Akt.Status
8		32			15			Existent				Existent		Existente				Existente
9		32			15			Non-Existent				Non-Existent		nicht vorhanden				nicht vorhanden
10		32			15			Normal					Normal			Normal					Normal
11		32			15			Failure					Failure			Fehler					Fehler
12		32			15			Rectifer Current Limit			Current Limit		GR Strombegrenzung			GR I Begrenzt
13		32			15			Rectifier Trim				Rect Trim		GR Trimmung				GR Trimmung
14		32			15			DC On/Off Control			DC On/Off Ctrl		DC An/Aus Kontrolle			DC An/Aus Kontr
15		32			15			AC On/Off Control			AC On/Off Ctrl		AC An/Aus Kontrolle			AC An/Aus Kontr
16		32			15			Rectifier LEDs Control			LEDs Control		LED Kontrolle				LED Kontrolle
17		32			15			Switch Off All				Switch Off All		Alle Ausschalten			Alle Ausschalt.
18		32			15			Switch On All				Switch On All		Alle Einschalten			Alle Einschalt.
19		32			15			Flash All				Flash All		Alle Blinken				Alle Blinken
20		32			15			Stop Flash				Stop Flash		Stop Blinken				Stop Blinken
21		32			32			Current Limit Control			Curr-Limit Ctl		Strombegrenzungskontrolle		I Begrenz.Kontr
22		32			32			Full Capacity Control			Full-Cap Ctl		Kapazitätskontrolle			Kapazitätskontr
23		32			15			Reset Rectifier Lost Alarm		Reset Rect Lost		Reset GR verloren Alarm			Reset GR verl.
24		32			15			Reset Cycle Alarm			Reset Cycle Al		Reset GR zyklus Alarm			Reset GR Zyklus
25		32			15			Clear					Clear			Löschen					Löschen
26		32			15			Rectifier Group I			Rect Group I		Gleichrichtergruppe I			GR Gruppe I
27		32			15			E-Stop Function				E-Stop Function		Notaus Funktion				Notaus Funktion
36		32			15			Normal					Normal			Normal					Normal
37		32			15			Failure					Failure			Fehler					Fehler
38		32			15			Switch Off All				Switch Off All		Alle GR Ausschalten			Alle GR Aus
39		32			15			Switch On All				Switch On All		Alle GR Einschalten			Alle GR Ein
83		32			15			No					No			Nein					Nein
84		32			15			Yes					Yes			Ja					Ja
96		32			15			Input Current Limit			InputCurrLimit		Eingangsstrombegrenzung			Eing.I Limit
97		32			15			Mains Failure				Mains Failure		Netzfehler				Netzfehler
98		32			15			Clear Rectifier Comm Fail Alarm		Clear Comm Fail		Lösche GR Fehler Alarm			Lösche GRFehler
99		32			15			System Capacity Used			Sys Cap Used		Gebrauchte Systemkapazität		Gebr.Sys.Kapaz.
100		32			15			Maximum Used Capacity			Max Cap Used		Max.gebrauchte Systemkapazität		Max.gebr.Kapaz.
101		32			15			Minimum Used Capacity			Min Cap Used		Min.gebrauchte Systemkapazität		Min.gebr.Kapaz.
102		32			15			Total Rated Current			Total Rated Cur		Nennstrom gesamt			Ges. Nennstrom
103		32			15			Total Rectifiers Communicating		Num Rects Comm		Anzahl kommunizierender GR		Anz.Kommuniz.GR
104		32			15			Rated Voltage				Rated Voltage		Spannung nominal			Spann.nominal
105		32			15			Fan Speed Control			Fan Speed Ctrl		Kontrolle Lüfterdrehzahl		Ktrl Lüfterdreh
106		32			15			Full Speed				Full Speed		Lüfter Max. Drehzahl			Fan Max.Drehz.
107		32			15			Automatic Speed				Auto Speed		Autom. Lüfterdrehzahl			Autom. Lüftdreh
108		32			15			Confirm Rectifier ID/Phase		Confirm ID/PH		Bestätige Gleichrichter ID/Phase	Bestät.ID/Phase
109		32			15			Yes					Yes			ja					ja
110		32			15			Multiple Rectifiers Fail		Multi-Rect Fail		Mehrfacher GR Ausfall			Mult. GR Ausfall
111		32			15			Total Output Power			OutputPower		Ausgangsleistung gesamt			Ges. P Ausgang
