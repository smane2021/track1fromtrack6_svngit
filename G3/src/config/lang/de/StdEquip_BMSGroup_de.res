﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			BMS Group			BMS Group		BMS-Gruppe			BMS-Gruppe
2		32			15			Number of BMS		NumOfBMS		Nummer der BMS		NumderBMS
3		32			15			Communication Fail		Comm Fail		Komm Fehl	Komm Fehl
4		32			15			Existence State			Existence State		Existenz Staat			Existenz Staat
5		32			15			Existent			Existent		Existieren			Existieren
6		32			15			Not Existent			Not Existent		Nicht Existent			Nicht Existent
7		32			15			BMS Existence State	BMSState		BMS Existenz Staat	BMSStaat

