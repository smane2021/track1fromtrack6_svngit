﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Fuse Tripped			Batt Fuse Trip		Batteriesicherung 1 ausgelöst		BatSich.1ausgel
2		32			15			Battery 2 fuse tripped			Batt2 Fuse Trip		Batteriesicherung 2 ausgelöst		BatSich.2ausgel
3		32			15			Battery 1 Voltage			Batt 1 Voltage		Spannung Batterie 1			U Batterie 1
4		32			15			Battery 1 Current			Batt 1 Current		Strom Batterie 1			I Batterie 1
5		32			15			Battery 1 Temperature			Batt 1 Temp		Temperatur Batterie 1			Temp Batt. 1
6		32			15			Battery 2 Voltage			Batt 2 Voltage		Spannung Batterie 2			U Batterie 2
7		32			15			Battery 2 Current			Batt 2 Current		Strom Batterie 2			I Batterie 2
8		32			15			Battery 2 Temperature			Batt 2 Temp		Temperatur Batterie 2			Temp Batt. 2
9		32			15			CSU Battery				CSU Battery		CSU Batterie				CSU Batterie
10		32			15			CSU Battery Failure			CSU BattFailure		CSU Batteriefehler			CSU Batt.Fehler
11		32			15			No					No			Nein					Nein
12		32			15			Yes					Yes			Ja					Ja
13		32			15			Battery 2 Connected			Batt2 Connected		Batterie 2 angeschlossen		Bat2 angeschl.
14		32			15			Battery 1 Connected			Batt1 Connected		Batterie 1 angeschlossen		Bat1 angeschl.
15		32			15			Existent				Existent		vorhanden				vorhanden
16		32			15			Non-Existent				Non-Existent		nicht vorhanden				n. vorhanden
