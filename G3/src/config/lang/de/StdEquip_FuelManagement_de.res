﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			16			Fuel Tank				Fuel Tank		Diesel Tank				Dieseltank
2		32			15			Fuel Height				Fuel Height		Höhe Dieselstand			Höhe Dslstand
3		32			15			Fuel Volume				Fuel Volume		Dieselvolumen				Dieselvolumen
4		32			15			Fuel Percent				Fuel Percent		Prozent Dieselstand			% Dieselstand
5		32			15			Fuel Theft Status			Theft Status		Dieseldiebstahl Status			Diebstahl Stat.
6		32			15			No					No			Nein					Nein
7		32			15			Yes					Yes			Ja					Ja
8		32			15			Multi-Shape Height Error		Multi HeightErr		Formfehler Höhe				Fehler Höhe
9		32			15			Confirm Tank Configuration		Confirm Tank		Tankkonfig.bestätigen			Bestät.Tankgr.
10		32			15			Reset Theft Alarm			Reset Theft Alr		Diebstahlalarm rücksetzen		Diebst.Alm.Res.
11		32			15			Fuel Tank Type				Tank Type		Dieseltank Typ				Dieseltank Typ
12		32			15			Square Tank Length			Square Length		4-Eckig Länge				4-Eck Länge
13		32			15			Square Tank Width			Square Width		4-Eckig Tiefe				4-Eck Tiefe
14		32			15			Square Tank Height			Square Height		4-Eckig Höhe				4-Eck Höhe
15		32			15			Standing Cylinder Tank Diameter		Standing Diameter	Zylinderdurchmesser stehend		Zyl.Durchm.steh
16		32			15			Standing Cylinder Tank Height		Standing Height		Zylinderhöhe stehend			Zyl.Höhe steh.
17		32			15			Lying Cylinder Tank Diameter		Lying Diameter		Zylinderdurchmesser liegend		Zyl.Durchm.lieg
18		32			15			Lying Cylinder Tank Length		Lying Length		Zylinderhöhe liegend			Zyl.Höhe lieg.
20		32			15			Number of Calibration Points		Num of Calib		Anzahl Kalibirierungspunkte		Anz.Kalibr.pkt
21		32			15			Height of Calibration Point 1		Height Calib1		Höhe Kalibirierungspunkt 1		Höhe Punkt 1
22		32			15			Volume of Calibration Point 1		Volume Calib1		Volumen Kalibirierungspunkt 1		Volumen Punkt 1
23		32			15			Height of Calibration Point 2		Height Calib2		Höhe Kalibirierungspunkt 2		Höhe Punkt 2
24		32			15			Volume of Calibration Point 2		Volume Calib2		Volumen Kalibirierungspunkt 2		Volumen Punkt 2
25		32			15			Height of Calibration Point 3		Height Calib3		Höhe Kalibirierungspunkt 3		Höhe Punkt 3
26		32			15			Volume of Calibration Point 3		Volume Calib3		Volumen Kalibirierungspunkt 3		Volumen Punkt 3
27		32			15			Height of Calibration Point 4		Height Calib4		Höhe Kalibirierungspunkt 4		Höhe Punkt 4
28		32			15			Volume of Calibration Point 4		Volume Calib4		Volumen Kalibirierungspunkt 4		Volumen Punkt 4
29		32			15			Height of Calibration Point 5		Height Calib5		Höhe Kalibirierungspunkt 5		Höhe Punkt 5
30		32			15			Volume of Calibration Point 5		Volume Calib5		Volumen Kalibirierungspunkt 5		Volumen Punkt 5
31		32			15			Height of Calibration Point 6		Height Calib6		Höhe Kalibirierungspunkt 6		Höhe Punkt 6
32		32			15			Volume of Calibration Point 6		Volume Calib6		Volumen Kalibirierungspunkt 6		Volumen Punkt 6
33		32			15			Height of Calibration Point 7		Height Calib7		Höhe Kalibirierungspunkt 7		Höhe Punkt 7
34		32			15			Volume of Calibration Point 7		Volume Calib7		Volumen Kalibirierungspunkt 7		Volumen Punkt 7
35		32			15			Height of Calibration Point 8		Height Calib8		Höhe Kalibirierungspunkt 8		Höhe Punkt 8
36		32			15			Volume of Calibration Point 8		Volume Calib8		Volumen Kalibirierungspunkt 8		Volumen Punkt 8
37		32			15			Height of Calibration Point 9		Height Calib9		Höhe Kalibirierungspunkt 9		Höhe Punkt 9
38		32			15			Volume of Calibration Point 9		Volume Calib9		Volumen Kalibirierungspunkt 9		Volumen Punkt 9
39		32			15			Height of Calibration Point 10		Height Calib10		Höhe Kalibirierungspunkt 10		Höhe Punkt 10
40		32			15			Volume of Calibration Point 10		Volume Calib10		Volumen Kalibirierungspunkt 10		Volumen Pnkt 10
41		32			15			Height of Calibration Point 11		Height Calib11		Höhe Kalibirierungspunkt 11		Höhe Punkt 11
42		32			15			Volume of Calibration Point 11		Volume Calib11		Volumen Kalibirierungspunkt 11		Volumen Pnkt 11
43		32			15			Height of Calibration Point 12		Height Calib12		Höhe Kalibirierungspunkt 12		Höhe Punkt 12
44		32			15			Volume of Calibration Point 12		Volume Calib12		Volumen Kalibirierungspunkt 12		Volumen Pnkt 12
45		32			15			Height of Calibration Point 13		Height Calib13		Höhe Kalibirierungspunkt 13		Höhe Punkt 13
46		32			15			Volume of Calibration Point 13		Volume Calib13		Volumen Kalibirierungspunkt 13		Volumen Pnkt 13
47		32			15			Height of Calibration Point 14		Height Calib14		Höhe Kalibirierungspunkt 14		Höhe Punkt 14
48		32			15			Volume of Calibration Point 14		Volume Calib14		Volumen Kalibirierungspunkt 14		Volumen Pnkt 14
49		32			15			Height of Calibration Point 15		Height Calib15		Höhe Kalibirierungspunkt 15		Höhe Punkt 15
50		32			15			Volume of Calibration Point 15		Volume Calib15		Volumen Kalibirierungspunkt 15		Volumen Pnkt 15
51		32			15			Height of Calibration Point 16		Height Calib16		Höhe Kalibirierungspunkt 16		Höhe Punkt 16
52		32			15			Volume of Calibration Point 16		Volume Calib16		Volumen Kalibirierungspunkt 16		Volumen Pnkt 16
53		32			15			Height of Calibration Point 17		Height Calib17		Höhe Kalibirierungspunkt 17		Höhe Punkt 17
54		32			15			Volume of Calibration Point 17		Volume Calib17		Volumen Kalibirierungspunkt 17		Volumen Pnkt 17
55		32			15			Height of Calibration Point 18		Height Calib18		Höhe Kalibirierungspunkt 18		Höhe Punkt 18
56		32			15			Volume of Calibration Point 18		Volume Calib18		Volumen Kalibirierungspunkt 18		Volumen Pnkt 18
57		32			15			Height of Calibration Point 19		Height Calib19		Höhe Kalibirierungspunkt 19		Höhe Punkt 19
58		32			15			Volume of Calibration Point 19		Volume Calib19		Volumen Kalibirierungspunkt 19		Volumen Pnkt 19
59		32			15			Height of Calibration Point 20		Height Calib20		Höhe Kalibirierungspunkt 20		Höhe Punkt 20
60		32			15			Volume of Calibration Point 20		Volume Calib20		Volumen Kalibirierungspunkt 20		Volumen Pnkt 20
62		32			15			Square					Square			4-Eckiger Tank				4-Eck Tank
63		32			15			Standing Cylinder			Standing Cyl		Zylinder stehend			Zylinder V
64		32			15			Lying Cylinder				Lying Cylinder		Zylinder liegend			Zylinder H
65		32			15			Multi Shape				Multi Shape		Multi Form				Multi-Form
66		32			15			Low Fuel Level Limit			Lo Level Limit		Niedr.Dieselstand Limit			Lo Level Limit
67		32			15			High Fuel Level Limit			Hi Level Limit		Hoher Dieselstand Limit			Hi Level Limit
68		32			15			Maximum Consumption Speed		Max Consumpt		Max. Verbrauch				Max. Verbrauch
71		32			15			High Fuel Level Alarm			Hi Fuel Level		Hoher Dieselstand Alarm			Hi Fuel Level
72		32			15			Low Fuel Level Alarm			Lo Fuel Level		Niedr.Dieselstand Alarm			Lo Fuel Level
73		32			15			Fuel Theft Alarm			Fuel Theft Alrm		Diesel Diebstahl Alarm			Diebstahl Alarm
74		32			15			Square Height Error			Square Hght Err		Fehler 4-Eck Höhe			Err.4Eck Höhe
75		32			15			Standing Cylinder Height Error		Stand Hght Err		Fehler Zyl.stehend Höhe			Err.Zyl.V Höhe
76		32			15			Lying Cylinder Height Error		Lying Hght Err		Fehler Zyl.liegend Höhe			Err.Zyl.H Höhe
77		32			15			Multi-Shape Height Error		Multi Hght Err		Fehler Multiform Höhe			Err.Multifhöhe
78		32			15			Fuel Tank Config Error			Fuel Config Error	Fehler Dieseltank Konfiguration		Err.Konfig.Tank
80		32			15			Fuel Tank Config Error Status		Config Error Status	Status Dieseltank Konfiguration		Err.Konfig.Stat
