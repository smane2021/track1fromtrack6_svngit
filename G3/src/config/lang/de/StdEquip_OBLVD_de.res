﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			LVD Unit				LVD			LVD					LVD
11		32			15			Connected				Connected		Geschlossen				Geschlossen
12		32			15			Disconnected				Disconnected		Geöffnet				Geöffnet
13		32			15			No					No			Nein					Nein
14		32			15			Yes					Yes			Ja					Ja
21		32			15			LVD1 Status				LVD1 Status		Status LVD1				Status LVD1
22		32			15			LVD2 Status				LVD2 Status		Status LVD2				Status LVD2
23		32			15			LVD1 Failure				LVD1 Failure		Fehler LVD1				Fehler LVD1
24		32			15			LVD2 Failure				LVD2 Failure		Fehler LVD2				Fehler LVD2
25		32			15			Communication Failure			Comm Failure		Kommunikationsfehler			Komm.Fehler
26		32			15			State					State			Status					Status
27		32			15			LVD1 Control				LVD1 Control		Kontrolle LVD1				Kontrolle LVD1
28		32			15			LVD2 Control				LVD2 Control		Kontrolle LVD2				Kontrolle LVD2
29		32			15			LVD Disabled				LVD Disabled		LVD Disabled				LVD Disabled
31		32			15			LVD1					LVD1			LVD1					LVD1
32		32			15			LVD1 Mode				LVD1 Mode		Modus LVD1				Modus LVD1
33		32			15			LVD1 Voltage				LVD1 Voltage		Spannung LVD1				Spannung LVD1
34		32			15			LVD1 Reconnect Voltage			LVD1 ReconnVolt		Wiedereinsch.Spannung LVD1		Einsch.Sp.LVD1
35		32			15			LVD1 Reconnect Delay			LVD1 ReconDelay		Verzögerung LVD1			Verzöger. LVD1
36		32			15			LVD1 Time				LVD1 Time		Zeit LVD1				Zeit LVD1
37		32			15			LVD1 Dependency				LVD1 Dependency		Abhängigkeit LVD1			Abhängigk.LVD1
41		32			15			LVD2					LVD2			LVD2					LVD2
42		32			15			LVD2 Mode				LVD2 Mode		Modus LVD2				Modus LVD2
43		32			15			LVD2 Voltage				LVD2 Voltage		Spannung LVD2				Spannung LVD2
44		32			15			LVR2 Reconnect Voltage			LVR2 ReconnVolt		Wiedereinsch.Spannung LVD2		Einsch.Sp.LVD2
45		32			15			LVD2 Reconnect Delay			LVD2 ReconDelay		Verzögerung LVD2			Verzöger. LVD2
46		32			15			LVD2 Time				LVD2 Time		Zeit LVD2				Zeit LVD2
47		32			15			LVD2 Dependency				LVD2 Dependency		Abhängigkeit LVD2			Abhängigk.LVD2
51		32			15			Disabled				Disabled		Deaktiviert				Deaktiviert
52		32			15			Enabled					Enabled			Aktiviert				Aktiviert
53		32			15			By Voltage				By Voltage		Spannung				Spannung
54		32			15			By Time					By Time			Zeit					Zeit
55		32			15			None					None			Kein					Kein
56		32			15			LVD1					LVD1			LVD1					LVD1
57		32			15			LVD2					LVD2			LVD2					LVD2
103		32			15			High Temp Disconnect 1			HTD1			Temp.Abschaltung 1			Temp.Abschalt.1
104		32			15			High Temp Disconnect 2			HTD2			Temp.Abschaltung 2			Temp.Abschalt.2
105		32			15			Battery LVD				Batt LVD		Batterie LVD				Batterie LVD
106		32			15			No Battery				No Batt			Keine Batterie				Keine Batterie
107		32			15			LVD1					LVD1			LVD1					LVD1
108		32			15			LVD2					LVD2			LVD2					LVD2
109		32			15			Battery Always On			BattAlwaysOn		Batterie immer an			Batt.immer an
110		32			15			LVD Contactor Type			LVD Type		LVD Kontaktor Typ			LVD Typ
111		32			15			Bistable				Bistable		Bistabil				Bistabil
112		32			15			Monostable				Monostable		Monostabil 1				Monostabil 1
113		32			15			Monostable with Sampler			Mono with Samp		Monostabil 2				Monostabil 2
116		32			15			LVD1 Disconnected			LVD1 Disconnect		LVD1 offen				LVD1 offen
117		32			15			LVD2 Disconnected			LVD2 Disconnect		LVD2 offen				LVD2 offen
118		32			15			LVD1 Monostable with Sampler		LVD1 Mono Sample	LVD1 Monostabil 1			LVD1 Monost.1
119		32			15			LVD2 Monostable with Sampler		LVD2 Mono Sample	LVD2 Monostabil 2			LVD2 Monost.2
125		32			15			State					State			Status					Status
126		32			15			LVD1 Voltage(24V)			LVD1 Voltage		Spannung LVD1(24V)			U LVD1 (24V)
127		32			15			LVD1 Reconnect Voltage(24V)		LVD1 ReconnVolt		Wiedereinsch.Spannung LVD1(24V)		WU LVD1 (24V)
128		32			15			LVD2 Voltage(24V)			LVD2 Voltage		Spannung LVD2(24V)			U LVD2 (24V)
129		32			15			LVD2 Reconnect Voltage(24V)		LVD2 ReconnVolt		Wiedereinsch.Spannung LVD2(24V)		WU LVD2 (24V)
130		32			15			LVD 3					LVD 3			LVD 3					LVD 3
200		32			15			Connect					Connect			Geschlossen				Geschlossen
201		32			15			Disconnect				Disconnect		Geöffnet				Geöffnet
