﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			EIB Group				EIB Group		EIB Gruppe				EIB Gruppe
2		32			15			Load Current				Load Current		Laststrom				Laststrom
3		32			15			DC Distribution				DC			DC-Verteiler				DC-Verteiler
4		32			15			Load Shunt				Load Shunt		Last Shunt				Last Shunt
5		32			15			Disabled				Disabled		Deaktiviert				Deaktiviert
6		32			15			Enabled					Enabled			Aktiviert				Aktiviert
7		32			15			Existence State				Existence State		Status vorhandend			Status vorh.
8		32			15			Existent				Existent		vorhanden				vorhanden
9		32			15			Non-Existent				Non-Existent		nicht vorhanden				n.vorhanden
