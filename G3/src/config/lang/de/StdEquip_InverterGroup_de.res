﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Inverter Group				Invt Group			Wechselrichtergruppe				Invt Group
2		32			15			Total Current				Tot Invt Curr		Gesamtstrom				Tot Invt Curr

4		32			15			Inverter Capacity Used			Sys Cap Used		Verwendete Wechselrichterkapazität				Sys Cap Used
5		32			15			Maximum Capacity Used			Max Cap Used		Maximal verwendete Kapazität				Max Cap Used
6		32			15			Minimum Capacity Used			Min Cap Used		Verwendete Mindestkapazität				Min Cap Used
7		32			15			Total Inverters Communicating	Num Invts Comm			Total Inverters kommunizieren				Num Invts Comm
8		32			15			Valid Inverters					Valid Inverters			Gültige Wechselrichter					Valid Inverters
9		32			15			Number of Inverters				Num of Invts			Anzahl der Wechselrichter					Num of Invts
10		32			15			Number of Phase1				Num of Phase1		Anzahl der Phasen1						Num of Phase1
11		32			15			Number of Phase2				Num of Phase2		Anzahl der Phasen2						Num of Phase2
12		32			15			Number of Phase3				Num of Phase3		Anzahl der Phasen3						Num of Phase3
13		32			15			Current of Phase1				Curr of Phase1		Strom von Phase1						Strom von Phase1
14		32			15			Current of Phase2				Curr of Phase2		Strom von Phase2						Strom von Phase2
15		32			15			Current of Phase3				Curr of Phase3		Strom von Phase3						Strom von Phase3
16		32			15			Power_kW of Phase1				Pwr_kW_Phase1		Leistung_kW von Phase1					Pwr_kW_Phase1
17		32			15			Power_kW of Phase2				Pwr_kW_Phase2		Leistung_kW von Phase2					Pwr_kW_Phase2
18		32			15			Power_kW of Phase3				Pwr_kW_Phase3		Leistung_kW von Phase3					Pwr_kW_Phase3
19		32			15			Power_kVA of Phase1				Pwr_kVA_Phase1		Leistung_kVA von Phase1					Pwr_kVA_Phase1
20		32			15			Power_kVA of Phase2				Pwr_kVA_Phase2		Leistung_kVA von Phase2					Pwr_kVA_Phase2
21		32			15			Power_kVA of Phase3				Pwr_kVA_Phase3		Leistung_kVA von Phase3					Pwr_kVA_Phase3
22		32			15			Rated Current					Rated Current			Nennstrom					Nennstrom
23		32			15			Input Total Energy				Input Energy			Gesamtenergie eingeben					Input Energy
24		32			15			Output Total Energy				Output Energy			Gesamtenergie ausgeben					Output Energy	
25		64			15			Inverter Sys Set ASYNC			Sys Set ASYNC			Wechselrichtersystem ASYNC einstellen				Sys Set ASYNC
26		64			15			Inverter AC Phase Abnormal		ACPhaseAbno				Wechselrichter-Wechselstromphase abnormal				ACPhaseAbno
27		32			15			Inverter REPO					REPO					REPO						REPO
28		64			15			Inverter Output Freq ASYNC		OutputFreqASYNC			Wechselrichterausgangsfrequenz ASYNC				OutputFreqASYNC
29		32			15			Output On/Off			O/p On/Off Ctrl		Ausgangs-Ein / Aus-Steuerung				O/p On/Off Ctrl
30		32			15			Inverters LED Control			Invt LED Ctrl			Wechselrichter LED-Steuerung				Invt LED Ctrl
31		32			15			Fan Speed				Fan Speed			Lüfterdrehzahlregelung				Fan Speed
32		32			15			Invt Work Mode					Invt Work Mode			Arbeitsmodus aufrufen				Invt Work Mode
33		32			15			Output Voltage Level			O/p Vltge Level	Ausgangsspannungspegel				O/p Vltge Level
34		32			15			Output Frequency					Output Frequency			Frequenzniveau				Frequenzniveau
35		32			15			Source Ratio					Source Ratio			Quellenverhältnis					Source Ratio
36		32			15			Normal					Normal			Normal					Normal
37		32			15			Fail					Fail			Scheitern					Scheitern
38		32			15			Switch Off All				Switch Off All		Alle ausschalten				Switch Off All
39		32			15			Switch On All				Switch On All		Alle einschalten				Switch On All
42		32			15			All Flashing				All Flashing		Alles blinkt				Alles blinkt
43		32			15			Stop Flashing				Stop Flashing		Hör auf zu blinken				Stop Flashing
44		32			15			Full Speed				Full Speed		Vollgas					Vollgas
45		32			15			Automatic Speed				Auto Speed		Automatische Geschwindigkeit				Auto Speed
54		32			15			Disabled				Disabled		Behindert					Behindert
55		32			15			Enabled					Enabled			aktiviert					aktiviert
56		32			15			100V					100V			100V				100V
57		32			15			110V					110V			110V				110V
58		32			15			115V					115V			115V				115V
59		32			15			120V					120V			120V				120V
60		32			15			125V					125V			125V				125V
61		32			15			200V					200V			200V				200V
62		32			15			208V					208V			208V				208V
63		32			15			220V					220V			220V				220V
64		32			15			230V					230V			230V				230V
65		32			15			240V					240V			240V				240V
66		32			15			Auto					Auto			Auto				Auto
67		32			15			50Hz					50Hz			50Hz				50Hz
68		32			15			60Hz					60Hz			60Hz				60Hz
69		32			15			Input DC Current		Input DC Curr		Eingangsgleichstrom				Input DC Curr
70		32			15			Input AC Current		Input AC Curr		Eingangswechselstrom				Input AC Curr
71		32			15			Inverter Work Status	InvtWorkStatus			Wechselrichter-Arbeitsstatus				InvtWorkStatus
72		32			15			Off					Off						aus							aus
73		32			15			No					No						auf							auf
74		32			15			Yes					Yes						Ja							Ja
75		32			15			Part On				Part On					Teil Ein						Teil Ein
76		32			15			All On				All On					Alles an						Alles an

77		32			15			DC Low Voltage Off		DCLowVoltOff		DC-Niederspannung Aus			DCLowVoltOff
78		32			15			DC Low Voltage On		DCLowVoltOn		DC-Niederspannung Ein			DCLowVoltOn
79		32			15			DC High Voltage Off	DCHiVoltOff		DC-Hochspannung aus			DCHiVoltOff
80		32			15			DC High Voltage On	DCHighVoltOn		DC-Hochspannung ein			DCHighVoltOn
81		32			15			Last Inverters Quantity		Last Invts Qty		Menge der letzten Wechselrichter				Last Invts Qty
82		32			15			Inverter Lost				Inverter Lost		Wechselrichter verloren				Inverter Lost
83		32			15			Inverter Lost				Inverter Lost		Wechselrichter verloren				Inverter Lost
84		64			15			Clear Inverter Lost Alarm		Clear Invt Lost		Alarm für verlorenen Wechselrichter löschen			Clear Invt Lost
86		64			15			Confirm Inverter ID/Phase			Confirm ID/PH			Bestätigen Sie die Wechselrichter-ID/Phase			ID/PH bestätigen
92		32			15			E-Stop Function				E-Stop Function		Not-Aus-Funktion				E-Stop Function
93		32			15			AC Phases				AC Phases		Wechselstromphasen				AC Phases
94		32			15			Single Phase				Single Phase		Einzelphase					Einzelphase
95		32			15			Three Phases				Three Phases		Drei Phasen					Drei Phasen
101		32			15			Sequence Start Interval			Start Interval		Sequenzstartintervall				Start Interval
104		64			15			All Inverters Communication Failure		AllInvtCommFail		Alle Wechselrichter Kommunikationsfehler			AllInvtCommFail
113		64			15			Invt Info Change (M/S Internal Use)	InvtInfo Change		Invt Info Change (M / S Interner Gebrauch)			InvtInfo Change
119		32			15			Clear Inverter Comm Fail Alarm		ClrInvtCommFail		Clear Inverter Comm Fail Alarm			ClrInvtCommFail
126		32			15			None					None			Keiner					Keiner
143		32			15			Existence State				Existence State		Existenzzustand				Existenzzustand
144		32			15			Existent				Existent		Existent					Existent
145		32			15			Not Existent				Not Existent		Nicht vorhanden					Nicht vorhanden
146		32			15			Total Output Power			Output Power		Gesamtausgangsleistung				Output Power
147		32			15			Total Slave Rated Current		Total RatedCurr		Gesamt-Slave-Nennstrom				Total RatedCurr
148		32			15			Reset Inverter IDs			Reset Invt IDs		Wechselrichter-IDs zurücksetzen				Reset Invt IDs
149		32			15			HVSD Voltage Difference (24V)		HVSD Volt Diff		HVSD-Spannungsdifferenz (24 V)				HVSD Volt Diff
150		32			15			Normal Update				Normal Update		Normales Update				Normales Update
151		32			15			Force Update				Force Update		Update erzwingen				Force Update
152		32			15			Update OK Number			Update OK Num		OK-Nummer aktualisieren				Update OK Num
153		32			15			Update State				Update State		Status aktualisieren				Update State
154		32			15			Updating				Updating		Aktualisierung					Aktualisierung
155		32			15			Normal Successful			Normal Success		Normal erfolgreich				Normal Success
156		32			15			Normal Failed				Normal Fail		Normal fehlgeschlagen				Normal Fail
157		32			15			Force Successful			Force Success		Erfolgreich erzwingen				Force Success
158		32			15			Force Failed				Force Fail		Force fehlgeschlagen				Force Fail
159		32			15			Communication Time-Out			Comm Time-Out		Kommunikations-Timeout				Comm Time-Out
160		32			15			Open File Failed			Open File Fail		Öffnen der Datei fehlgeschlagen				Open File Fail
161		32			15			AC mode						AC mode					AC-Modus				AC-Modus
162		32			15			DC mode						DC mode					DC-Modus				DC-Modus

#163		32			15			Safety mode					Safety mode				Sicherheitsmodus				Safety mode
#164		32			15			Idle  mode					Idle  mode				Ruhezustand				Ruhezustand
165		32			15			Max used capacity			Max used cap		Maximale genutzte Kapazität				Max used cap
166		32			15			Min used capacity			Min used cap		Min. Verbrauchte Kapazität				Min used cap
167		32			15			Average Voltage				Invt Voltage			Durchschnittliche Spannung				Invt Voltage
168		32			15			Invt Redundancy Number		Invt Redu Num			Invt Redundanznummer			Invt Redu Num
169		32			15			Inverter High Load			Invt High Load		Wechselrichter hohe Last				Invt High Load
170		32			15			Rated Power					Rated Power				Nennleistung				Nennleistung

171		32			15			Clear Fault					Clear Fault				Fehler löschen				Fehler löschen
172		32			15			Reset Energy				Reset Energy			Energie zurücksetzen				Reset Energy

173		32			15			Syncronization Phase Failure			SynErrPhaType			Syn Err Phasentyp			SynErrPhaType
174		32			15			Syncronization Voltage Failure			SynErrVoltType			Syn Err Volt Typ			SynErrVoltType
175		32			15			Syncronization Frequency Failure			SynErrFreqLvl			Syn Err Freq Level			SynErrFreqLvl
176		32			15			Syncronization Mode Failure			SynErrWorkMode			Syn Err Arbeitsmodus			SynErrWorkMode
177		32			15			Syn Err Low Volt TR			SynErrLVoltTR			Syn Err Low Volt TR			SynErrLVoltTR
178		32			15			Syn Err Low Volt CB			SynErrLVoltCB			Syn Err Low Volt CB			SynErrLVoltCB
179		32			15			Syn Err High Volt TR		SynErrHVoltTR			Syn Err High Volt TR			SynErrHVoltTR
180		32			15			Syn Err High Volt CB		SynErrHVoltCB			Syn Err High Volt CB			SynErrHVoltCB
181		32			15			Set Para To Invt			SetParaToInvt			Setze Para auf Invt			SetParaToInvt
182		32			15			Get Para From Invt			GetParaFromInvt			Holen Sie sich Para von Invt			GetParaFromInvt
183		32			15			Power Used					Power Used				Stromverbrauch				Stromverbrauch
184		32			15			Input AC Voltage			Input AC Volt			Eingangswechselspannung 			Eingang AC Volt	
#185     64          15          Input AC Voltage Phase A       InpVoltPhA           Eingangswechselspannung Phase A.      InpVoltPhA
185		32			15			Input AC Phase A			Input AC VolPhA			AC-Phase A eingeben 			Input AC VolPhA	
186		32			15			Input AC Phase B			Input AC VolPhB			AC-Phase B eingeben 			Input AC VolPhB
187		32			15			Input AC Phase C			Input AC VolPhC			AC-Phase C eingeben 			Input AC VolPhC		
188		32			15			Total output voltage			Total output V			Gesamtausgangsspannung			Total output V
189		32			15			Power in kW					PowerkW				Leistung in kW				LeistkW
190		32			15			Power in kVA					PowerkVA				Leistung in kVA				LeistskVA
197		32			15			Maximum Power Capacity					Max Power Cap				Maximale Leistungskapazität				MaxLeiskapa
