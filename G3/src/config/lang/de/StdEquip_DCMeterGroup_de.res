﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			DC Meter Group				DCMeter Group		DC Meter Gruppe				DC Meter Gruppe
2		32			15			DC Meter Number				DCMeter Num		DC Meter Nummer				DC Meter Nr.
3		32			15			Communication Failure			Comm Fail		Kommunikationsfehler			Komm.Fehler
4		32			15			Communication State			Existence		Kommunikation				Kommunikation
5		32			15			Activ					Activ			Aktiv					Aktiv
6		32			15			Non active				Non active		Inaktiv					Inaktiv
11		32			15			DC Meter Lost				DCMeter Lost		DCMeter verloren			DCMeter verl.
12		32			15			DC Meter Num Last Time			DCMeter Num Last	Anzahl DCMeter Vorher			DCMeter Vorher
13		32			15			Clear DC Meter Lost Alarm		ClrDCMeterLost		DCMeter verloren löschen		Verl. löschen
14		32			15			Yes					Yes			Ja					Ja
15		32			15			Total Energy Consumption		TotalEnergy		Totaler Energieverbrauch		Total Energie
