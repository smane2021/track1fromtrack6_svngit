﻿
#  Web Private Configuration File, Version 1.00
#                All rights reserved.
# Copyright(c) 2021, Vertiv Tech Co., Ltd.
#
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
#Standard mandatory section

[LOCAL_LANGUAGE]
de

[LOCAL_LANGUAGE_VERSION]
1.00


#Define the web pages's resource file
#The field of Default Name is the english language
#The field of Local Name must be transfered  the local language according to the Default Name
#Define web pages number
[NUM_OF_PAGES]
83

#WebPages Resource
[control_cgi.htm:Number]
0

[control_cgi.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[data_sampler.htm:Number]
0

[data_sampler.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE


[dialog.htm:Number]
6

[dialog.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE		Description
1		ID_OK			32			N/A			OK			N/A		O.K.			N/A			NA
2		ID_CANCEL		32			N/A			Cancel			N/A		Abbruch			N/A			NA
3		ID_SIGNAL_NAME		32			N/A			Name			N/A		Name			N/A			NA
4		ID_SAMPLER		32			N/A			Status			N/A		Status			N/A			NA
5		ID_SAMPLE_CHANNEL	32			N/A			Status Channel		N/A		Status Kanal		N/A			NA
6		ID_SIGNAL_NEW_NAME	32			N/A			New Name		N/A		Neuer Name		N/A			NA


[editsignalname.js:Number]
0

[editsignalname.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[equip_data.htm:Number]
12

[equip_data.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN										ABBR_IN_EN	FULL_IN_LOCALE											ABBR_IN_LOCALE
1		ID_TIPS0	32			N/A			Failed to communicate with								N/A		Fehler bei der Kommunikation mit								N/A
2		ID_TIPS1	64			N/A			[OK]Reconnect. [Cancel]Stop exploring							N/A		[OK]Neu verbinden,[Abbruch]Stop Erkundung							N/A
3		ID_TIPS2	64			N/A			Failed to communicate with the application						N/A		Fehler bei der Kommunikation mit der Applikation						N/A
4		ID_TIPS3	128			N/A			Number of rectifiers has changed, the pages will be refreshed.				N/A		Die Anzahl der Gleichrichter hat sich geändert, Seiten wird neu geladen				N/A
5		ID_TIPS4	128			N/A			System Configuration has changed, the pages will be refreshed.				N/A		Die Systemkonfiguration hat sich geändert, Seiten werden neu geladen				N/A
6		ID_TIPS5	128			N/A			Number of DC Distributions has changed, the pages will be refreshed.			N/A		Die Anzahl der Verteiler hat sich geändert, Seiten werden neu geladen				N/A
7		ID_TIPS6	128			N/A			Number of batteries has changed, the pages will be refreshed.				N/A		Die Anzahl der Batterien hat sich geändert, Seiten werden neu geladen				N/A
8		ID_TIPS8	256			N/A			Monitoring is in Auto Configuration Process. Please wait a moment (About 1 minute).	N/A		Monitoring ist im Auto-Konfigurationsprzess. Bitten waren Sie einen Moment (ca. 1 Min.)		N/A
9		ID_TIPS9	256			N/A			System Configuration has changed, the pages will be refreshed.				N/A		Die Systemkonfiguration hat sich geändert, Seiten werden neu geladen				N/A
10		ID_TIPS10	256			N/A			Number of converters has changed, the pages will be refreshed.				N/A		Die Anzahl der DC/DC Wandler hat sich geändert, Seiten werden neu geladen			N/A
11		ID_TIPS11	256			N/A			Controller in System Expansion Secondary Mode.						N/A		Kontroller ist in Systemerweiterungsmodus (Sekundär)						N/A
12		ID_TIPS12	256			N/A			System configuration changed. The pages will be refreshed.				N/A		Die Systemkonfiguration hat sich geändert, Seiten werden neu geladen				N/A



[j01_tree_maker.js:Number]
1

[j01_tree_maker.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_DIR		32			N/A			/cgi-bin/eng/				N/A		/cgi-bin/loc/		N/A

[j02_tree_view.js:Number]
36

[j02_tree_view.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE				ABBR_IN_LOCALE
1		ID_SCUP			16		N/A			System					N/A		System					N/A
2		ID_DEVICE_EXPLORE	32		N/A			DEVICES					N/A		EINHEITEN				N/A
3		ID_SYSTEM		16		N/A			MAINTENANCE				N/A		WARTUNG					N/A
4		ID_NETWORK_SETTING	32		N/A			Network Configuration			N/A		Netzwerk				N/A
5		ID_NMS_SETTING		32		N/A			NMSV2 Configuration			N/A		NMSV2 Konfiguration			N/A
6		ID_ESR_SETTING		32		N/A			MC Configuration			N/A		MC Konfiguration			N/A
7		ID_USER			64		N/A			User Configuration			N/A		Benutzer				N/A
8		ID_MAINTENANCE		16		N/A			CONFIGURATION				N/A		KONFIGURATION				N/A
9		ID_FILE_MANAGE		32		N/A			Upload/Download				N/A		Upload/Download				N/A
10		ID_MODIFY_CFG		64		N/A			Site Information Modification		N/A		Standort Information			N/A
11		ID_TIME_CFG		64		N/A			Time Settings				N/A		Zeiteinstellungen			N/A
12		ID_QUERY		16		N/A			QUERY					N/A		ABFRAGE					N/A
13		ID_SITEMAP		16		N/A			Site Map				N/A		Site Map				N/A
14		ID_ALARM		32		N/A			ALARMS					N/A		ALARME					N/A
15		ID_ACTIVE_ALARM		32		N/A			Active					N/A		Aktive					N/A
16		ID_HISTORY_ALARM	32		N/A			Alarm History				N/A		Alarm Historie				N/A
17		ID_QUERY_HIS_DATA	32		N/A			Data History				N/A		Daten Historie				N/A
18		ID_QUERY_LOG_DATA	32		N/A			System Log				N/A		System Log				N/A
19		ID_QUERY_BATT_DATA	32		N/A			Battery Test Log			N/A		Batterietest Log			N/A
20		ID_CLEAR_DATA		32		N/A			Clear Data				N/A		Daten löschen				N/A
21		ID_RESTORE_DEFAULT	32		N/A			Restore Default				N/A		Rücksetzung auf Standardwerte		N/A
22		ID_PRODUCT_INFO		32		N/A			Site Inventory				N/A		Systeminventur				N/A
23		ID_EDIT_CFGFILE		64		N/A			Configuration Info Modification		N/A		Konfiguration Info Modifikation		N/A
24		ID_YDN23_SETTING	32		N/A			MC Configuration			N/A		MC Konfiguration			N/A
25		ID_MODIFY_CFG1		64		N/A			Site Information Modification		N/A		Site Information			N/A
26		ID_MODIFY_CFG2		64		N/A			Equipment Information Modification	N/A		Geräte Information			N/A
27		ID_MODIFY_CFG3		64		N/A			Signal Information Modification		N/A		Signal Information			N/A
28		ID_EDIT_CFGFILE1	64		N/A			Configuration Of Alarm Suppression	N/A		Alarmunterdrückung			N/A
29		ID_EDIT_CFGFILE2	64		N/A			Configuration Of Alarm Relays		N/A		Alarm Relais				N/A
30		ID_EDIT_CFGFILE3	64		N/A			Configuration Of PLC			N/A		PLC					N/A
31		ID_USER_DEF_PAGES	64		N/A			QUICK SETTINGS				N/A		EINSTELLUNGEN				N/A
32		ID_EDIT_CFGGCPS		32		N/A			Power Split				N/A		Power Split				N/A
33		ID_GET_PARAM		32		N/A			Get Parameter Settings			N/A		Parameter Abrufen		N/A
34		ID_AUTO_CONFIG		32		N/A			Auto Configuration			N/A		Auto-Konfiguration			N/A
35		ID_SYS_STATUS		32		N/A			SYSTEM STATUS				N/A		SYSTEM ÜBERBLICK			N/A
36		ID_NMSV3_SETTING		32			N/A			NMSV3 Configuration			N/A		NMSV3 Konfiguration			N/A


[j04_gfunc_def.js:Number]
0

[j04_gfunc_def.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[j05_signal_def.js:Number]
0

[j05_signal_def.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[j07_equiplist_def.js:Number]
0

[j07_equiplist_def.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[j31_menu_script.js:Number]
0

[j31_menu_script.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[j31_table_script.js:Number]
0


[j31_table_script.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[j50_event_log.js:Number]
0

[j50_event_log.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[j76_device_def.htm:Number]
0

[j76_device_def.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[j77_equiplist_def.htm:Number]
0

[j77_equiplist_def.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[login.htm:Number]
41

[login.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN	FULL_IN_LOCALE				ABBR_IN_LOCALE
1		ID_USER_NAME		8			N/A			User				N/A		Benutzer				N/A
2		ID_PASSWORD		16			N/A			Password			N/A		Passwort				N/A
3		ID_CANCEL		16			N/A			Cancel				N/A		Abbruch					N/A
4		ID_OK			8			N/A			OK				N/A		O.K.					N/A
5		ID_LOGIN		16			N/A			Login				N/A		Login					N/A
6		ID_TIPS0		64			N/A			Incorrect user or password.	N/A		Falscher Benutzer oder Passwort		N/A
7		ID_TIPS1		64			N/A			Incorrect user or password.	N/A		Falscher Benutzer oder Passwort		N/A
8		ID_TIPS2		64			N/A			Your browser isn't supported.	N/A		Ihr Browser wird nicht unterstützt	N/A
9		ID_TIPS3		64			N/A		The cookie was blocked, you may not explore the pages normally.		N/A		Das Cookie wurde nicht akzeptiert. Die Seiten werden möglicherweise nicht korrekt dargestellt.		N/A
10		ID_TIPS4		128			N/A		The system supports Microsoft IE 5.5 or above. \nYour browser is not fully supported. Please update.		N/A	Das System unterstützt Microsoft IE 5.5 oder höher. \nIhr Browser wird nicht 100%ig unterstützt. Bitte Updaten!		N/A
11		ID_ERROR0		64			N/A			Unknown error			N/A		Unbekannter Fehler			N/A
12		ID_ERROR1		64			N/A			Success				N/A		Erfolgreich				N/A
13		ID_ERROR2		64			N/A			Incorrect user or password.	N/A		Falscher Benutzer oder Passwort		N/A
14		ID_ERROR3		64			N/A			Incorrect user or password.	N/A		Falscher Benutzer oder Passwort		N/A
15		ID_ERROR4		64			N/A			Failed to communicate with the application.	N/A		Kommunikation mit der Applikation nicht möglich		N/A
16		ID_ERROR5		64			N/A			Over 5 connections, please wait.	N/A	Über 5 Verbindungen, bitte warten!	N/A
17		ID_ENGLISH_VERSION	16			N/A			English				N/A		Englisch				N/A
18		ID_LOCAL_VERSION	16			N/A			German				N/A		Deutsch				N/A
19		ID_SERIAL_NUMBER	20			N/A			ACU+ Serial Number		N/A		ACU+ Seriennummer			N/A
20		ID_HARDWARE_VERSION	32			N/A			Hardware Version		N/A		Hardware Version			N/A
21		ID_SOFTWARE_VERSION	32			N/A			Software Version		N/A		Software Version			N/A
22		ID_PRODUCT_NUM		32			N/A			Product Model			N/A		Modell					N/A
23		ID_PRO_NUMBER		32			N/A			Controller			N/A		Kontroller				N/A
24		ID_CFG_VERSION		22			N/A			Configuration Version		N/A		Konfigurationsversion			N/A
25		ID_LOCAL2_VERSION	16			N/A			Local 2				N/A		Lokal 2					N/A
26		ID_ERROR6		128			N/A			Controller is starting up, \nplease wait.	N/A	Kontroller startet, \nbitte warten!	N/A
27		ID_OPT_RESOLUTION	32			N/A			Optimal Resolution		N/A		Optimale Auflösung			N/A
28		ID_ERROR7		256			N/A			Monitoring is in Auto Configuration Process. Please wait a moment (About 1 minute).	N/A	Monitoring ist im Auto-Konfigurationsprzess. Bitten waren Sie einen Moment (ca. 1 Min.)	N/A
29		ID_ERROR8		64			N/A			Controller in System Expansion Secondary Mode.						N/A	Kontroller ist in Systemerweiterungsmodus (Sekundär)!		N/A
30		ID_LOGIN		64			N/A			Login				N/A		Login					N/A
31		ID_SITENAME		64			N/A			Site Name			N/A		Site Name				N/A
32		ID_SYSTEMNAME		64			N/A			System Name			N/A		System Name				N/A
33		ID_TEXT1		128			N/A			You are requesting access	N/A		Sie wünschen Zugang zu			N/A
34		ID_TEXT2		64			N/A			located at			N/A		in					N/A
35		ID_TEXT3		256			N/A			The user name and password for this device is set by the system administrator		N/A	Benutzername und Passwort für diese Einheit werden vom Administrator vergeben		N/A
36		ID_LOGIN		64			N/A			Login				N/A		Login					N/A
37		ID_CSSDIR		32			N/A			/cgi-bin/eng			N/A		/cgi-bin/loc				N/A
38		ID_CSSDIR2		32			N/A			/cgi-bin/eng			N/A		/cgi-bin/loc				N/A
39		ID_CSSDIR3		32			N/A			/cgi-bin/eng			N/A		/cgi-bin/loc				N/A
40		ID_LOGIN2		64			N/A			Login				N/A		Login					N/A
41		ID_CSSDIR4		32			N/A			/cgi-bin/eng			N/A		/cgi-bin/loc				N/A

[p01_main_frame.htm:Number]
2

[p01_main_frame.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_EXPLORE	32			16			Controller Web Explorer	N/A			ACU+ WebBrowser		N/A
2		ID_SITE		16			16			Site			N/A			Site			N/A


[p02_tree_view.htm:Number]
15

[p02_tree_view.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN						ABBR_IN_EN	FULL_IN_LOCALE						ABBR_IN_LOCALE
1		ID_TIPS0		64			N/A			This page was locked, please login again.		N/A		Diese Seite wurde blockiert, bitte erneut anmelden.	N/A
2		ID_LANGUAGE_VERSION	64			N/A			Local Web Pages Link					N/A		Lokaler Web Seiten Link					N/A
3		ID_SITE_MAP		64			N/A			Site Map						N/A		Site Map						N/A
4		ID_ALARM		128			N/A			Please wait for the requested page to be loaded.	N/A		Bitte warten bis die angeforderte Seite geladen wurde.	N/A
5		ID_SYS_STATUS		32			N/A			System Status						N/A		System Status						N/A
6		ID_SERIAL_NUMBER	32			N/A			Serial Number						N/A		Seriennummer						N/A
7		ID_HARDWARE_VERSION	32			N/A			Hardware Version					N/A		Hardware Version					N/A
8		ID_SOFTWARE_VERSION	32			N/A			Software Version					N/A		Software Version					N/A
9		ID_PRODUCT_NUM		32			N/A			Product Model						N/A		Modell							N/A
10		ID_CFG_VERSION		32			N/A			Configuration Version					N/A		Konfiguration Version					N/A
11		ID_SYSTEMNAME		64			N/A			System Name						N/A		Systemname						N/A
12		ID_RECTNAME		32			N/A			Rectifiers						N/A		Gleichrichter						N/A
13		ID_RECTNAME2		32			N/A			Rectifiers						N/A		Gleichrichter						N/A
14		ID_SYS_SPEC		64			N/A			System Specifications					N/A		System Sepzifikationen					N/A
15		ID_CONTROL_SPEC		64			N/A			Controller Specifications				N/A		Kontroller Spezifikationen				N/A


[p03_main_menu.htm:Number]
0

[p03_main_menu.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE


[p04_main_title.htm:Number]
3

[p04_main_title.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_SAMPLE	16			N/A			STATUS		N/A		STATUS			N/A
2		ID_CONTROL	16			N/A			CONTROL		N/A		KONTROLLE		N/A
3		ID_SETTING	16			N/A			SETTINGS	N/A		EINSTELLUNGEN		N/A



[p05_equip_sample.htm:Number]
8


[p05_equip_sample.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL		MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_INDEX		16				N/A			Index		N/A		Index			N/A
2		ID_SIGNAL_NAME		16				N/A			Name		N/A		Name			N/A
3		ID_SIGNAL_VALUE		16				N/A			Value		N/A		Wert			N/A
4		ID_SIGNAL_UNIT		16				N/A			Unit		N/A		Einheit			N/A
5		ID_SAMPLE_TIME		16				N/A			Time		N/A		Zeit			N/A
6		ID_SAMPLER		16				N/A			Status		N/A		Status			N/A
7		ID_CHANNEL		16				N/A			Channel		N/A		Kanal			N/A
8		ID_NO_SIG		64				N/A		There is no Status signal in this equipment.	N/A	In diesem Gerät gibt es kein Statussignal!		N/A

[p06_equip_control.htm:Number]
24

[p06_equip_control.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE					ABBR_IN_LOCALE
1		ID_INDEX		16			N/A			Index					N/A		Index						N/A
2		ID_SIGNAL_NAME		16			N/A			Name					N/A		Name						N/A
3		ID_SIGNAL_VALUE		16			N/A			Value					N/A		Wert						N/A
4		ID_SIGNAL_UNIT		16			N/A			Unit					N/A		Einheit						N/A
5		ID_SAMPLE_TIME		16			N/A			Time					N/A		Zeit						N/A
6		ID_SET_VALUE		16			N/A			Set value				N/A		Neuer Wert					N/A
7		ID_SET			16			N/A			Set					N/A		Setzen						N/A
8		ID_ERROR0		32			N/A			Failure.				N/A		Fehler!						N/A
9		ID_ERROR1		32			N/A			Success.				N/A		Erfolgreich					N/A
10		ID_ERROR2		64			N/A			Failure, conflict in settings.		N/A		Fehler, Einstellungskonflikt!			N/A
11		ID_ERROR3		32			N/A			Failure, you are not authorized.	N/A		Fehler, Sie sind nicht berechtigt!		N/A
12		ID_ERROR4		64			N/A			No information to send.			N/A		Keine Information zu senden			N/A
13		ID_ERROR5		64			N/A			Failure, Controller is hardware protected.	N/A	Fehler, Kontroller ist HW geschützt!		N/A
14		ID_SET_TYPE		16			N/A			Set					N/A		Setzen						N/A
15		ID_SHOW_TIPS0		64			N/A			Greater than the max value.		N/A		Größer als max. Wert				N/A
16		ID_SHOW_TIPS1		64			N/A			Less than the min value.		N/A		Kleiner als min. Wert				N/A
17		ID_SHOW_TIPS2		64			N/A			Can't be empty.				N/A		Darf nicht leer sein				N/A
18		ID_SHOW_TIPS3		64			N/A			Enter a number please.			N/A		Geben Sie einen Wert ein			N/A
19		ID_SHOW_TIPS4		64			N/A			Are you sure?				N/A		Sind Sie sicher?				N/A
20		ID_SHOW_TIPS5		64			N/A			Failure, you are not authorized.	N/A		Fehler, Sie sind nicht berechtigt!		N/A
21		ID_TIPS1		64			N/A			Send control				N/A		Senden						N/A
22		ID_SAMPLER		16			N/A			Status					N/A		Status						N/A
23		ID_CHANNEL		16			N/A			Channel					N/A		Kanal						N/A
24		ID_NO_SIG		64			N/A			There is no Control signal in this equipment.	N/A	In diesem Gerät gibt es kein Kontrollsignal!	N/A

[p07_equip_setting.htm:Number]
29


[p07_equip_setting.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN	FULL_IN_LOCALE						ABBR_IN_LOCALE
1		ID_INDEX		16			N/A			Index						N/A		Index							N/A
2		ID_SIGNAL_NAME		16			N/A			Name						N/A		Name							N/A
3		ID_SIGNAL_VALUE		16			N/A			Value						N/A		Wert							N/A
4		ID_SIGNAL_UNIT		16			N/A			Unit						N/A		Einheit							N/A
5		ID_SAMPLE_TIME		16			N/A			Time						N/A		Zeit							N/A
6		ID_SET_VALUE		16			N/A			Set Value					N/A		Neuer Wert						N/A
7		ID_SET			16			N/A			Set						N/A		Setzen							N/A
8		ID_ERROR0		32			N/A			Failure.					N/A		Fehler!							N/A
9		ID_ERROR1		32			N/A			Success.					N/A		Erfolgreich!						N/A
10		ID_ERROR2		64			N/A			Failure, conflict in setting.			N/A		Fehler, Einstellungskonflikt!				N/A
11		ID_ERROR3		32			N/A			Failure, you are not authorized.		N/A		Fehler, Sie sind nicht berechtigt!			N/A
12		ID_ERROR4		64			N/A			No information to send.				N/A		Keine Information zu senden				N/A
13		ID_ERROR5		128			N/A			Failure, Controller is hardware protected	N/A		Fehler, Kontroller ist HW geschützt!			N/A
14		ID_SET_TYPE		16			N/A			Set						N/A		Setzen							N/A
15		ID_SHOW_TIPS0		64			N/A			Greater than the max value:			N/A		Größer als max. Wert:					N/A
16		ID_SHOW_TIPS1		64			N/A			Less than the min value:			N/A		Kleiner als min. Wert:					N/A
17		ID_SHOW_TIPS2		64			N/A			Can't be null.					N/A		Darf nicht Null sein					N/A
18		ID_SHOW_TIPS3		64			N/A			Input number please.				N/A		Geben Sie einen Wert ein				N/A
19		ID_SHOW_TIPS4		64			N/A			The control value is equal to the last value.	N/A		Der Wert ist gleich dem letzten Wert			N/A
20		ID_SHOW_TIPS5		64			N/A			Failure, you are not authorized.		N/A		Fehler, Sie sind nicht berechtigt!			N/A
21		ID_TIPS1		64			N/A			Send setting					N/A		Einstellungen senden					N/A
22		ID_SAMPLER		16			N/A			Status						N/A		Status							N/A
23		ID_CHANNEL		16			N/A			Channel						N/A		Kanal							N/A
24		ID_MONTH_ERROR		32			N/A			Incorrect month.				N/A		Falscher Monat!						N/A
25		ID_DAY_ERROR		32			N/A			Incorrect day.					N/A		Falscher Tag!						N/A
26		ID_HOUR_ERROR		32			N/A			Incorrect hour.					N/A		Falsche Stunde!						N/A
27		ID_FORMAT_ERROR		64			N/A			Incorrect format, should be			N/A		Falsches Format, sollte sein				N/A
28		ID_RELOAD_PAGE		64			N/A			./eng/p07_equip_setting.htm			N/A		./loc/p07_equip_setting.htm				N/A
29		ID_NO_SIG		64			N/A			There is no Setting signal in this equipment.	N/A		In diesem Gerät gibt es kein Einstellungssignal.	N/A
#30		ID_ADJUST_CONFIRM		64			N/A			Confirm the voltage has been supplied to the bus.	N/A	Bestätigung Spannung an der Busschiene		N/A

[p08_alarm_frame.htm:Number]
1

[p08_alarm_frame.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_DIR		32			N/A			/cgi-bin/eng/		N/A		/cgi-bin/loc/		N/A

[p09_alarm_title.htm:Number]
9

[p09_alarm_title.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_ALL_ALARM		16			N/A			All Alarms	N/A		Alle Alarme		N/A
2		ID_OBSERVATION_ALARM	16			N/A			Observation	N/A		Wartungsalarm		N/A
3		ID_MAJOR_ALARM		16			N/A			Major		N/A		Dringender Alarm	N/A
4		ID_CRITICAL_ALARM	16			N/A			Critical	N/A		Kritischer Alarm	N/A
5		ID_AUTO			16			N/A			Auto Popup	N/A		Auto Popup		N/A
6		ID_ALL_ALARM1		16			N/A			All Alarms	N/A		Alle Alarme		N/A
7		ID_OBSERVATION_ALARM1	16			N/A			Observation	N/A		Wartungsalarm		N/A
8		ID_MAJOR_ALARM1		16			N/A			Major		N/A		Dringender Alarm	N/A
9		ID_CRITICAL_ALARM1	16			N/A			Critical	N/A		Kritischer Alarm	N/A

[p10_alarm_show.htm:Number]
8

[p10_alarm_show.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_INDEX		16			N/A			Index				N/A		Index			N/A
2		ID_SIGNAL_NAME		16			N/A			Name				N/A		Name			N/A
3		ID_ALARM_LEVEL		16			N/A			Alarm Level			N/A		Alarmlevel		N/A
4		ID_SAMPLE_TIME		16			N/A			Time				N/A		Zeit			N/A
5		ID_RELATIVE_DEVICE	16			N/A			Device				N/A		Gerät			N/A
6		ID_OA			16			N/A			OA				N/A		Wartungsalarm		N/A
7		ID_MA			16			N/A			MA				N/A		Dringender Alarm	N/A
8		ID_CA			16			N/A			CA				N/A		Kritischer Alarm	N/A

[p11_network_config.htm:Number]
21

#In ID_TIPS1,ID_TIPS2, ID_TIPS4, the '\n' and 'nnn.nnn.nnn.nnn','10.75.14.171' is about the format of show pages, so that you don't need to tranfer it.
[p11_network_config.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN											ABBR_IN_EN	FULL_IN_LOCALE										ABBR_IN_LOCALE
1		ID_SCUP_IP		32			N/A			Controller Network IP										N/A		IP Adresse										N/A
2		ID_SCUP_MASK		8			N/A			Mask												N/A		Mask											N/A
3		ID_SCUP_GATEWAY		8			N/A			Gateway												N/A		Gateway											N/A
4		ID_SAVE_PARAMETER	16			N/A			Save Parameter											N/A		Einstell.sichern									N/A
5		ID_SCUP_NETWORK_HEAD	32			N/A			Controller network parameter settings.								N/A		Kontroller Netzwerkeinstellungen							N/A
6		ID_ERROR0		32			N/A			Unknown error.											N/A		Unbekannter Fehler!									N/A
7		ID_ERROR1		32			N/A			Success, Controller is rebooting.								N/A		Erfolgreich! Kontroller startet neu.							N/A
8		ID_ERROR2		32			N/A			Failure, incorrect input.									N/A		Fehler: Falsche Eingabe									N/A
9		ID_ERROR3		32			N/A			Failure, incomplete information.								N/A		Fehler: Information nicht komplett							N/A
10		ID_ERROR4		32			N/A			Failure, you are not authorized.								N/A		Fehler: Sie sind nicht berechtigt!							N/A
11		ID_ERROR5		32			N/A			Failure, Controller is hardware protected							N/A		Fehler: Kontroller ist HW geschützt!							N/A
12		ID_TIPS0		32			N/A			Set network parameter										N/A		Netzwerkeinstellungen übernehmen							N/A
13		ID_TIPS1		128			N/A			IP address error.\nFormat shall be 'nnn.nnn.nnn.nnn', e.g. 10.75.14.171				N/A		Fehler IP Adresse!\nFormat soll sein: 'nnn.nnn.nnn.nnn', e.g. 10.75.14.171		N/A
14		ID_TIPS2		128			N/A			IP mask error.\nFormat shall be'nnn.nnn.nnn.nnn', e.g. 255.255.255.0				N/A		Fehler IP Mask!\nFormat soll sein:'nnn.nnn.nnn.nnn', e.g. 255.255.255.0			N/A
15		ID_TIPS3		32			N/A			IP & Mask error.										N/A		Fehler IP + Mask									N/A
16		ID_TIPS4		128			N/A			Gateway IP error.\nCorrect shall be'nnn.nnn.nnn.nnn' e.g. 10.75.14.254, 0.0.0.0 as no gateway	N/A		Gateway IP Fehler\nFormat soll sein: 'nnn.nnn.nnn.nnn' e.g. 10.75.14.254, 0.0.0.0 = keine gateway	N/A
17		ID_TIPS5		64			N/A			IP, gateway and mask mismatch. Try again.							N/A		IP, Gateway and Mask passen nicht. Erneut eingeben.					N/A
18		ID_TIPS6		64			N/A			Please wait, Controller is rebooting...								N/A		Bitte warten, Kontroller startet neu...							N/A
19		ID_TIPS7		64			N/A			Since parameters have been modified, Controller is rebooting...					N/A		Einstellungen wurden verändert, Kontroller startet neu...				N/A
20		ID_TIPS8		64			N/A			Controller card homepage will be refreshed after.						N/A		Kontroller Homepage wird aktualisiert.							N/A
21		ID_ERROR6		32			N/A			Failure, DHCP is ON.										N/A		Fehler: DHCP ist aktiv.									N/A


[p12_nms_config.htm:Number]
40

[p12_nms_config.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN										ABBR_IN_EN		FULL_IN_LOCALE					ABBR_IN_LOCALE
1		ID_ERROR0		16			N/A			Unknown error.										N/A			Unbekannter Fehler!				N/A
2		ID_ERROR1		64			N/A			Failure, the NMS already exists.							N/A			Fehler: NMS besteht bereits			N/A
3		ID_ERROR2		32			N/A			Success											N/A			Erfolgreich					N/A
4		ID_ERROR3		64			N/A			Failure, incomplete information.							N/A			Fehler, Information nicht komplett!		N/A
5		ID_ERROR4		64			N/A			Failure, you are not authorized.							N/A			Fehler, Sie sind nicht berechtigt!		N/A
6		ID_ERROR5		64			N/A			Controller is hardware protected, can't be modified.					N/A			Kontroller ist HW geschützt! Kann nicht geändert werden.	N/A
7		ID_ERROR6		64			N/A			Failure, the maximum number is exceeded.						N/A			Fehler: Max. Anzahl überschritten		N/A
8		ID_NMS_HEAD1		32			N/A			NMS Configuration									N/A			NMS Konfiguration				N/A
9		ID_NMS_HEAD2		32			N/A			Current NMS										N/A			Aktuelle NMS					N/A
10		ID_NMS_IP		16			N/A			NMS IP											N/A			IP NMS						N/A
11		ID_NMS_AUTHORITY	16			N/A			Authority										N/A			Berechtigung					N/A
12		ID_NMS_TRAP		32			N/A			Accepted Trap Level									N/A			Akzeptierte Trap Level				N/A
13		ID_NMS_IP		16			N/A			NMS IP											N/A			IP NMS						N/A
14		ID_NMS_AUTHORITY	16			N/A			Authority										N/A			Berechtigung					N/A
15		ID_NMS_TRAP		32			N/A			Accepted Trap Level									N/A			Akzeptierte Trap Level				N/A
16		ID_NMS_ADD		20			N/A			Add New NMS										N/A			Neue NMS einfügen				N/A
17		ID_NMS_MODIFY		32			N/A			Modify NMS										N/A			NMS ändern					N/A
18		ID_NMS_DELETE		32			N/A			Delete NMS										N/A			NMS löschen					N/A
19		ID_NMS_PUBLIC		32			N/A			Public Community									N/A			Öffentliche Comm.				N/A
20		ID_NMS_PRIVATE		32			N/A			Private Community									N/A			Private Comm.					N/A
21		ID_NMS_LEVEL0		16			N/A			Not Used										N/A			nicht gebraucht					N/A
22		ID_NMS_LEVEL1		16			N/A			No Access										N/A			Kein Zugang					N/A
23		ID_NMS_LEVEL2		32			N/A			Query Authority										N/A			Abfrage Berechtigung				N/A
24		ID_NMS_LEVEL3		32			N/A			Control Authority									N/A			Berechtigungskontrolle				N/A
25		ID_NMS_LEVEL4		32			N/A			Administrator										N/A			Administrador					N/A
26		ID_NMS_TRAP_LEVEL0	16			N/A			Not Used										N/A			nicht gebraucht					N/A
27		ID_NMS_TRAP_LEVEL1	16			N/A			All Alarms										N/A			Alle Alarme					N/A
28		ID_NMS_TRAP_LEVEL2	16			N/A			Major Alarms										N/A			Dringende Alarme				N/A
29		ID_NMS_TRAP_LEVEL3	16			N/A			Critical Alarms										N/A			Kritische Alarme				N/A
30		ID_NMS_TRAP_LEVEL4	16			N/A			No traps										N/A			Keine Traps					N/A
31		ID_TIPS0		128			N/A			Incorrect IP address of NMS.\nFormat should be 'nnn.nnn.nnn.nnn',\ne.g.,10.76.8.29	N/A			Falsche NMS IP Adresse.\nFormat soll sein: 'nnn.nnn.nnn.nnn'\ne.g.: 10.76.8.29	N/A
32		ID_TIPS1		128			N/A			Public Community and Private Community can't be empty. Please try again.		N/A			Öffentliche + Private Comm. dürfen nicht leer sein. Bitte erneut versuchen.	N/A
33		ID_TIPS2		128			N/A			already exists, please try again.							N/A			Besteht bereits, bitte erneut versuchen.					N/A
34		ID_TIPS3		128			N/A			does not exist,so the password can't be modified, please try again.			N/A			Besteht nicht, daher kann das Passwort nicht geändert werden. Erneut versuchen.	N/A
35		ID_TIPS4		128			N/A			Please select one or more NMS before clicking this button.				N/A			Bitte eine oder mehrere NMS auswählen bevor sie hier klicken.			N/A
36		ID_TIPS5		128			N/A			NMS Info Configuration									N/A			NMS Konfigurationsinformation							N/A
37		ID_NMS_PUBLIC		128			N/A			Public Community									N/A			Öffentliche Community								N/A
38		ID_NMS_PRIVATE		128			N/A			Private Community									N/A			Private Community								N/A
39		ID_TRAP_HEAD		128			N/A			Change NMS's Trap (All current NMS's trap will be changed)				N/A			NMS's Traps ändern (alle aktuellen NMS's Traps werden geändert)			N/A
40		ID_SET_TRAP		128			N/A			Change Trap										N/A			Trap ändern									N/A


[p13_esr_config.htm:Number]
93

[p13_esr_config.htm]
#Sequence ID	RES_ID				MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN						ABBR_IN_EN		FULL_IN_LOCALE							ABBR_IN_LOCALE
1		ID_ERROR0			64			N/A			Success.						N/A			Erfolgreich!								N/A
2		ID_ERROR1			64			N/A			Failure.						N/A			Fehler!								N/A
3		ID_ERROR2			64			N/A			Failure, MC Service was stopped.			N/A			Fehler, MC Service gestoppt.					N/A
4		ID_ERROR3			64			N/A			Failure, invalid parameter.				N/A			Fehler, Einstellung nicht gültig!				N/A
5		ID_ERROR4			64			N/A			Failure, invalid data.					N/A			Fehler, Daten nicht gültig!					N/A
6		ID_ERROR5			64			N/A			Controller is hardware protected, can't be modified.	N/A			Kontroller ist HW geschützt! Kann nicht geändert werden.	N/A
7		ID_ERROR6			64			N/A			service is busy, config cannot be changed now.		N/A			Service ist aktiv, Konfig. kann jetzt nicht geändert werden.	N/A
8		ID_ERROR7			64			N/A			non-shared port has already been occupied.		N/A			Nicht freigegebener Port wurde bereits belegt			N/A
9		ID_ERROR8			64			N/A			Failure, you are not authorized.			N/A			Fehler! Sie sind nicht berechtigt!				N/A
10		ID_ESR_HEAD			32			N/A			MC Configuration					N/A			MC Konfiguration						N/A
11		ID_PROTOCOL_TYPE		32			N/A			Protocol Type						N/A			Protokolltyp							N/A
12		ID_PROTOCOL_MEDIA		32			N/A			Protocol Media						N/A			Protokoll Medien						N/A
13		ID_CALLBACK_IN_USE		32			N/A			Callback in Use						N/A			Rückruf aktiv							N/A
14		ID_REPORT_IN_USER		32			N/A			Report in Use						N/A			Report aktiv							N/A
15		ID_MAX_ALARM_REPORT		32			N/A			Max Alarm Report Attempts				N/A			Max. Alarm Reportversuch					N/A
16		ID_RANGE_FROM			32			N/A			Range							N/A			Bereich								N/A
17		ID_CALL_ELAPSE_TIME		32			N/A			Call Elapse Time					N/A			Abgelaufene Rückrufzeit						N/A
18		ID_RANGE_FROM			32			N/A			Range							N/A			Bereich								N/A
19		ID_MAIN_REPORT_PHONE		32			N/A			Primary Report Phone Number				N/A			Erste Telefonnummer						N/A
20		ID_SECOND_REPORT_PHONE		32			N/A			Secondary Report Phone Number				N/A			Zweite Telefonnummer						N/A
21		ID_CALLBACK_PHONE		32			N/A			Callback Phone Number					N/A			Rückruf Telefonnummer						N/A
22		ID_REPORT_IP			32			N/A			Primary Report IP					N/A			Erste IP Adresse						N/A
23		ID_SECOND_IP			32			N/A			Secondary Report IP					N/A			Zweite IP Adresse						N/A
24		ID_SECURITY_IP			32			N/A			Security Connection IP					N/A			Security Connection IP						N/A
25		ID_SECURITY_IP			32			N/A			Security Connection IP					N/A			Security Connection IP						N/A
26		ID_SAFETY_LEVEL			32			N/A			Safety Level						N/A			Sicherheitsstufe						N/A
27		ID_MODIFY			32			N/A			Modify							N/A			Ändern								N/A
28		ID_CCID				16			N/A			CCID							N/A			CCID								N/A
29		ID_SOCID			16			N/A			SOCID							N/A			SOCID								N/A
30		ID_PROTOCOL0			16			N/A			EEM							N/A			EEM								N/A
31		ID_PROTOCOL1			16			N/A			RSOC							N/A			RSOC								N/A
32		ID_PROTOCOL2			16			N/A			SOCTPE							N/A			SOCTPE								N/A
33		ID_MEDIA0			16			N/A			Leased Line						N/A			Standleitung							N/A
34		ID_MEDIA1			16			N/A			Modem							N/A			Modem								N/A
35		ID_MEDIA2			16			N/A			TCP/IP							N/A			TCP/IP								N/A
36		ID_USE0				16			N/A			Not used						N/A			Nicht benutzt							N/A
37		ID_USE1				16			N/A			Used							N/A			Benutzt								N/A
38		ID_SECURITY0			128			N/A			All commands are available				N/A			Alle Befehle erlaubt						N/A
39		ID_SECURITY1			128			N/A			Only read commands are available			N/A			Nur Lesen-Befehle erlaubt					N/A
40		ID_SECURITY2			128			N/A			Only the Call Back ('CB') command are available		N/A			Nur der Rückruf-Befehl ('CB') ist erlaubt			N/A
41		ID_TIPS1			128			N/A			IP address error.\nFormat shall be 'nnn.nnn.nnn.nnn' e.g. 10.75.14.171	N/A	Fehler IP Adresse.\nFormat soll sein: 'nnn.nnn.nnn.nnn' e.g. 10.75.14.171	N/A
42		ID_TIPS2			128			N/A			IP mask error.\nFormat shall be 'nnn.nnn.nnn.nnn' e.g. 255.255.255.0	N/A	Fehler Mask Adresse.\nFormat soll sein: 'nnn.nnn.nnn.nnn' e.g. 255.255.255.0	N/A
43		ID_TIPS3			32			N/A			IP & Mask error.					N/A			Fehler IP + Mask						N/A
44		ID_TIPS4			128			N/A			Gateway IP error.\nFormat shall be 'nnn.nnn.nnn.nnn' e.g. 10.75.14.254, 0.0.0.0 as no gateway	N/A	Fehler Gateway IP Adresse.\nFormat soll sein:'nnn.nnn.nnn.nnn' e.g. 10.75.14.254, 0.0.0.0 = kein Gateway	N/A
45		ID_TIPS5			128			N/A			IP, gateway and mask mismatch. Try again.		N/A			Discrepanz IP Gateway. Neuer Versuch				N/A
46		ID_TIPS6			128			N/A			CCID error, enter a number please.			N/A			Fehler CCID, bitte eine Nummer eingeben!			N/A
47		ID_TIPS7			128			N/A			SOCID error, enter a number please.			N/A			Fehler SOCID, bitte eine Nummer eingeben!			N/A
48		ID_TIPS8			128			N/A			Max alarm report alarm attempt is invalid.		N/A			Max alarm report alarm attempt is invalid.			N/A
49		ID_TIPS9			128			N/A			Max call elapse time is invalid.			N/A			Max call elapse time is invalid.				N/A
50		ID_TIPS10			128			N/A			Primary report phone number is invalid.			N/A			Erste Telefonnummer ist ungültig.				N/A
51		ID_TIPS11			128			N/A			Secondary report phone number is invalid.		N/A			Zweite Telefonnummer ist ungültig.				N/A
52		ID_TIPS12			128			N/A			Callback report phone number is invalid.		N/A			Rückruf Telefonnummer ist ungültig.				N/A
53		ID_TIPS13			128			N/A			Primary report IP is invalid.				N/A			Erste IP Adresse ist ungültig.					N/A
54		ID_TIPS14			128			N/A			Secondary report IP is invalid.				N/A			Zweite IP Adresse ist ungültig.					N/A
55		ID_TIPS15			128			N/A			Security IP 1 is invalid.				N/A			Security IP 1 ist ungültig.					N/A
56		ID_TIPS16			128			N/A			Security IP 2 is invalid.				N/A			Security IP 2 ist ungültig.					N/A
57		ID_TIPS17			128			N/A			MC Configuration.					N/A			MC Konfiguration						N/A
58		ID_TIPS18			128			N/A			Can't be 0.						N/A			Kann nicht 0 sein.						N/A
59		ID_TIPS19			128			N/A			Input error.						N/A			Eingabefehler.							N/A
60		ID_COMMON_PARAM			64			N/A			Protocol media common config				N/A			Protokoll Media Grundeinstellung				N/A
61		ID_MEDIA_PARAM0			64			N/A			Serial port parameters					N/A			Serielle Port Einstellungen					N/A
62		ID_MEDIA_PARAM1			64			N/A			Serial port parameters & phone number			N/A			Serielle Port + Telefon Einstellungen				N/A
63		ID_MEDIA_PARAM2			32			N/A			TCP/IP Port Number					N/A			TCP/IP Port Nummer						N/A
64		ID_RANGE_FROM1			64			N/A			Range 0-255						N/A			Bereich 0-255							N/A
65		ID_RANGE_FROM2			64			N/A			Range 0-600						N/A			Bereich 0-600							N/A
66		ID_TIPS20			64			N/A			input error						N/A			Eingabefehler.							N/A
67		ID_TIPS21			64			N/A			Max alarm report attempts input error			N/A			Max alarm report attempts input error				N/A
68		ID_TIPS22			64			N/A			Max alarm report attempts input error			N/A			Max alarm report attempts input error				N/A
69		ID_TIPS23			64			N/A			Input error						N/A			Eingabefehler.							N/A
70		ID_TIPS24			64			N/A			The port input error					N/A			Port Eingabefehler.						N/A
71		ID_NO_PROTOCOL_TIPS		128			N/A			Please enter protocol					N/A			Bitte Protokoll einstellen.					N/A
72		ID_YDN23_RANGE_FROM1		64			N/A			Range 0-5						N/A			Bereich 0-5							N/A
73		ID_YDN23_RANGE_FROM2		64			N/A			Range 0-300						N/A			Bereich 0-300							N/A
74		ID_YDN23_1ST_REPORT_PHONE	32			N/A			Primary Report Phone Number				N/A			Erste Telefonnummer						N/A
75		ID_YDN23_2ND_REPORT_PHONE	32			N/A			Secondary Report Phone Number				N/A			Zweite Telefonnummer						N/A
76		ID_YDN23_3RD_REPORT_PHONE	32			N/A			Third Report Phone Number				N/A			Dritte Telefonnummer						N/A
77		ID_YDN23_REPORT_IN_USER		32			N/A			Report in Use						N/A			Bericht in Gebrauch						N/A
78		ID_YDN23_SELF_ADDRESS		16			N/A			YDN23 Address						N/A			YDN23 Adresse							N/A
79		ID_YDN23_MAX_ALARM_REPORT	32			N/A			Max Alarm Report Attempts				N/A			Max Alarm Report Attempts					N/A
80		ID_YDN23_CALL_ELAPSE_TIME	32			N/A			Call Elapse Time					N/A			Abgelaufene Rückrufzeit						N/A
81		ID_PROTOCOL3			16			N/A			YDN23							N/A			YDN23								N/A
82		ID_TIPS8			128			N/A			Max alarm report alarm attempt is invalid.		N/A			Max alarm report alarm attempt is invalid.			N/A
83		ID_TIPS9			128			N/A			Max call elapse time is invalid.			N/A			Max call elapse time is invalid.				N/A
84		ID_TIPS21			64			N/A			Max alarm report alarm attempt is invalid.		N/A			Max alarm report alarm attempt is invalid.			N/A
85		ID_TIPS22			64			N/A			Max alarm report alarm attempt is invalid.		N/A			Max alarm report alarm attempt is invalid.			N/A
86		ID_TIPS23			64			N/A			Input error.						N/A			Eingabefehler							N/A
87		ID_RANGE_FROM1			64			N/A			Range 0-255						N/A			Bereich 0-255							N/A
88		ID_RANGE_FROM2			64			N/A			Range 0-600						N/A			Bereich 0-600							N/A
89		ID_MAIN_REPORT_PHONE		32			N/A			Primary Report Phone Number				N/A			Erste Telefonnummer						N/A
90		ID_SECOND_REPORT_PHONE		32			N/A			Secondary Report Phone Number				N/A			Zweite Telefonnummer						N/A
91		ID_CALLBACK_PHONE		32			N/A			Callback Phone Number					N/A			Rückruf Telefonnummer						N/A
92		ID_MAX_ALARM_REPORT		32			N/A			Max Alarm Report Attempts				N/A			Max Alarm Report Attempts					N/A
93		ID_CALL_ELAPSE_TIME		32			N/A			Call Elapse Time					N/A			Abgelaufene Rückrufzeit						N/A

[p14_user_config.htm:Number]
38

[p14_user_config.htm]
#Sequence ID	RES_ID				MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN									ABBR_IN_EN	FULL_IN_LOCALE								ABBR_IN_LOCALE
1		ID_WEB_HEAD			32			N/A			User Configuration								N/A		Benutzereinstellungen							N/A
2		ID_CURRENT_USER			32			N/A			Current users									N/A		Aktuelle Benutzter							N/A
3		ID_USER_NAME_INTABLE		32			N/A			User Name									N/A		Benutzername								N/A
4		ID_USER_AUTHORITY_INTABLE	32			N/A			Authority									N/A		Berechtigungen								N/A
5		ID_PASSWORD			32			N/A			Password									N/A		Passwort								N/A
6		ID_USER_NAME			32			N/A			User Name									N/A		Benutzername								N/A
7		ID_USER_AUTHORITY		32			N/A			Authority									N/A		Berechtigungen								N/A
8		ID_CONFIRM			32			N/A			Confirm										N/A		Bestätigung								N/A
9		ID_USER_ADD			32			N/A			Add New User									N/A		Neuen Benutzer hinzufügen						N/A
10		ID_USER_MODIFY			32			N/A			Modify User									N/A		Benutzer modifizieren							N/A
11		ID_USER_DELETE			32			N/A			Delete Selected User								N/A		Ausgewählten Benutzer löschen						N/A
12		ID_ERROR0			32			N/A			Unknown error.									N/A		Unbekannter Fehler							N/A
13		ID_ERROR1			32			N/A			Success										N/A		Erfolgreich								N/A
14		ID_ERROR2			64			N/A			Failure, incomplete information.						N/A		Fehler! Information nicht vollständig!					N/A
15		ID_ERROR3			64			N/A			Failure, the user name already exists.						N/A		Fehler! Benutzername besteht bereits!					N/A
16		ID_ERROR4			64			N/A			Failure, you are not authorized.						N/A		Fehler! Sie sind nicht berechtigt!					N/A
17		ID_ERROR5			64			N/A			Controller is hardware protected, can't be modified.				N/A		Kontroller ist HW geschützt! Kann nicht geändert werden.		N/A
18		ID_ERROR6			64			N/A			Failure, only you can change your own password.					N/A		Fehler! Nur Sie können Ihr eigenes Passwort ändern.			N/A
19		ID_ERROR7			64			N/A			Failure, deleting admin is not allowed.						N/A		Fehler! Admin zu löschen ist nicht erlaubt.				N/A
20		ID_ERROR8			64			N/A			Failure, can't delete the logged in user.					N/A		Fehler! Kann den eigeloggten Benutzer nicht löschen.			N/A
21		ID_ERROR9			128			N/A			The user already exists, please try again.					N/A		Benutzer besteht bereits, bitte erneut versuchen.			N/A
22		ID_ERROR10			128			N/A			Failure, too many users.							N/A		Fehler! Zu viele Benutzer.						N/A
23		ID_ERROR11			128			N/A			Failure, user does not exist.							N/A		Fehler! Benutzer exisiert nicht!					N/A
24		ID_TIPS1			32			N/A			Please enter a user name.							N/A		Bitte geben Sie einen Benutzernamen ein.				N/A
25		ID_TIPS2			128			N/A			The user name cannot start or end with space.					N/A		Der Benutzername kann nicht mit einem Leerzeichen beginnen o. enden!	N/A
26		ID_TIPS3			128			N/A			The inputs for 'Password' and 'Confirm' must be the same, please try agian.	N/A		Die Eingaben für Passwort und Bestäigung müssen gleich sein.		N/A
27		ID_TIPS4			128			N/A			Please rememeber the password.							N/A		Bitte wiederholen Sie das Passwort.					N/A
28		ID_TIPS5			128			N/A			A password must be entered.							N/A		Passwort erforderlich!							N/A
29		ID_TIPS6			128			N/A			Please remember the password.							N/A		Bitte wiederholen Sie das Passwort.					N/A
30		ID_TIPS7			128			N/A			already exists, please try again.						N/A		Ein Passwort muss eingegeben werden.					N/A
31		ID_TIPS8			128			N/A			does not exist,so the password can't be modified, please try again.		N/A		besteht nicht, Passwort kann nicht geändert werden, bitte erneut versuchen.	N/A
32		ID_TIPS9			128			N/A			Please select one or more users before clicking this button.			N/A		Wählen Sie einen oder mehrere Benutzer bevor sie hier klicken!		N/A
33		ID_AUTHORITY_LEVEL0		32			N/A			Browser										N/A		Browser									N/A
34		ID_AUTHORITY_LEVEL1		32			N/A			Operator									N/A		Operator								N/A
35		ID_AUTHORITY_LEVEL2		32			N/A			Engineer									N/A		Engineer								N/A
36		ID_AUTHORITY_LEVEL3		32			N/A			Administrator									N/A		Administrator								N/A
37		ID_INVALIDATE_CHAR		64			N/A			Invalid character in input.							N/A		Ungültiges Zeichen bei der Eingabe!					N/A
38		ID_TIPS10			128			N/A			The following characters must not be included in user name, please try again.	N/A		Folgende Zeichen dürfen im Benutzernamen nicht verwendet werden.	N/A

[p15_show_acutime.htm:Number]
0

[p15_show_acutime.htm.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[p16_filemanage_title.htm:Number]
8

[p16_filemanage_title.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE			ABBR_IN_LOCALE
1		ID_ERROR0	32			N/A			Unknown error.				N/A		Unbekannter Fehler!		N/A
2		ID_ERROR1	32			N/A			Success					N/A		Erfolgreich!			N/A
3		ID_ERROR2	32			N/A			Failure, not enough space.		N/A		Fehler, nicht genug Platz	N/A
4		ID_ERROR3	32			N/A			Failure, you are not authorized.	N/A		Fehler, Sie sind nicht berechtigt	N/A
5		ID_ERROR4	32			N/A			Success, stop Controller		N/A		Erfolgreich, stoppe Kontroller	N/A
6		ID_ERROR5	32			N/A			Success, start Controller		N/A		Erfolgreich, starte Kontroller	N/A
7		ID_UPLOAD	32			N/A			Upload					N/A		Upload				N/A
8		ID_DOWNLOAD	32			N/A			Download				N/A		Download			N/A
9		ID_TIPS0	128			N/A			Do you want to quit the download pages? Click Yes to start Controller, Click No to cancel this operation.	N/A	Möchten Sie diese Seite verlassen? Klicken Sie JA um den Kontroller zu starten, Klicken Sie NEIN um abzubrechen.	N/A

[p17_login_overtime.htm:Number]
5

[p17_login_overtime.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_LOGIN_HEAD		32			N/A			Overtime		N/A			Zeitüberschreitung		N/A
2		ID_OK			32			N/A			Login again		N/A			Erneut einloggen		N/A
3		ID_CANCEL		32			N/A			Cancel			N/A			Abbruch			N/A
4		ID_EXPLORE_INFO		256			N/A			Login time expired, so you can only explore data.<br> Click OK to login again if you want to control device.<br>Click Cancel to continue exploring data.<br>	N/A		Zeit überschritten, sie haben nur Leseberechtigung.<br>Klicken Sie Erneut einloggen um sich erneut anzumelden.<br>Klicken Sie auf Abbruch um mit Leseberechtigung fortzufahren.<br>		N/A
5		ID_TIPS0		64			N/A			You have connected to Controller. This window will be closed.		N/A				Sie haben sich mit dem Kontroller verbunden. Dieses Fenster wird geschlossen.		N/A

[p18_restore_default.htm:Number]
8

[p18_restore_default.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN									ABBR_IN_EN	FULL_IN_LOCALE							ABBR_IN_LOCALE
1		ID_HEAD			128			N/A			Restore to Default								N/A		Rücksetzen auf Voreinstellungen					N/A
2		ID_TIPS0		128			N/A			Restore to default configuration. The system will be rebooted.			N/A		Rücksetzen auf Voreinstellungskonfiguration. Der Kontroller wird dabei neu gestartet.	N/A
3		ID_RESTORE_DEFAULT	64			N/A			Restore to Default								N/A		Rücksetzen auf Voreinstellungen					N/A
4		ID_TIPS1		128			N/A			Restore to default configuration will force system to reboot. Are you sure?	N/A		Rücksetzen auf Voreinstellungskonfiguration erzwingt einen Neustart. Sind Sie sicher?	N/A
5		ID_TIPS2		128			N/A			Controller is hardware protected, can't be restored!				N/A		Kontroller ist HW geschützt. Kann nicht zurückgesetzt werden.	N/A
6		ID_TIPS3		128			N/A			Are you sure you want to reboot Controller?					N/A		Sind Sie sicher den Kontroller neu zu starten?			N/A
7		ID_TIPS4		128			N/A			Failure, you are not authorized!						N/A		Fehler, Sie sind nicht berechtigt!				N/A
8		ID_START_SCUP		32			N/A			Reboot Controller								N/A		Kontroller wird neu gestartet.					N/A

[p19_time_config.htm:Number]
28

[p19_time_config.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN																ABBR_IN_EN	FULL_IN_LOCALE							ABBR_IN_LOCALE
1		ID_TIME_HEAD		32			N/A			Time Settings																N/A		Zeiteinstellungen						N/A
2		ID_TIME_HEAD		32			N/A			Time Settings																N/A		Zeiteinstellungen						N/A
3		ID_SELECT1		64			N/A			Get Time Automatically from the Following Time Servers											N/A		Zeit automatisch von den folgenden Zeitservern beziehen!	N/A
4		ID_PRIMARY_SERVER	32			N/A			Primary Server																N/A		Erster Server							N/A
5		ID_SECONDARY_SERVER	32			N/A			Secondary Server															N/A		Zweiter Server							N/A
6		ID_TIMER_INTERVAL	64			N/A			Interval to Adjust Time															N/A		Aktualisierungsintervall					N/A
7		ID_SPECIFY_TIME		32			N/A			Specify Time																N/A		Zeit spezifizieren						N/A
8		ID_GET_TIME		32			N/A			Get Local Time																N/A		Lokale Zeit							N/A
9		ID_DATE			16			N/A			Date																	N/A		Datum								N/A
10		ID_TIME			16			N/A			Time																	N/A		Zeit								N/A
11		ID_SUBMIT		16			N/A			Apply																	N/A		Bestätigen							N/A
12		ID_ERROR0		32			N/A			Unknown error.																N/A		Unbekannter Fehler!						N/A
13		ID_ERROR1		16			N/A			Success.																N/A		Erfolgreich!							N/A
14		ID_ERROR2		128			N/A			Failure, incorrect time setting.													N/A		Fehler, falsche Zeiteinstellung!				N/A
15		ID_ERROR3		128			N/A			Failure, incomplete information.													N/A		Fehler, Information nicht vollständig!				N/A
16		ID_ERROR4		128			N/A			Failure, you are not authorized.													N/A		Fehler, Sie sind nicht berechtigt!				N/A
17		ID_ERROR5		64			N/A			Controller is hardware protected, can't be modified.											N/A		Kontroller ist HW geschützt. Kann nicht zurückgesetzt werden.	N/A
18		ID_TIPS0		128			N/A			Incorrect IP address of primary server.\nCorrect format should be 'nnn.nnn.nnn.nnn', e.g, 129.9.1.10.\n0.0.0.0 means no time server	N/A		Falsche IP-Adresse des ersten Zeitservers.\nFormat soll sein: 'nnn.nnn.nnn.nnn', e.g, 129.9.1.10. '0.0.0.0' = kein Zeitserver.		N/A
19		ID_TIPS1		128			N/A			Incorrect IP address of secondary server.\nCorrect format should be 'nnn.nnn.nnn.nnn', e.g., 129.9.1.10.\n0.0.0.0 means no time server	N/A		Falsche IP-Adresse des zweiten Zeitservers.\nFormat soll sein: 'nnn.nnn.nnn.nnn', e.g, 129.9.1.10. '0.0.0.0' = kein Zeitserver.		N/A
20		ID_TIPS2		128			N/A			Incorrect time interval.\nTime interval should be a positive integer.									N/A		Falscher Aktual.intervall.\nIntervall muss postiver Integer sein.	N/A
21		ID_TIPS3		128			N/A			Synchronizing time, please wait...													N/A		Zeitsychronisierung, bitte warten...				N/A
22		ID_TIPS4		128			N/A			Incorrect date setting.\nCorrect format should be 'yyyy/mm/dd', e.g., 2000/09/30							N/A		Falsches Datumsformat.\nFormat soll sein: 'yyyy/mm/dd', e.g. 2000/09/30 	N/A
23		ID_TIPS5		128			N/A			Incorrect time setting. \nCorrect format should be 'hh:mm:ss', e.g., 8:23:08								N/A		Falsches Zeitformat.\nFormat soll sein: 'hh:mm:ss', e.g. 8:23:08		N/A
24		ID_TIPS6		128			N/A			Date must be set between '1970/01/01 00:00:00' and '2038/01/01 00:00:00'.								N/A		Datum muss zwischen '1999/01/01 00:00:00' und '2038/02/06 00:00:00' liegen.	N/A
25		ID_MINUTES		16			N/A			Minutes																	N/A		Minuten								N/A
26		ID_ZONE			16			N/A			Local Zone																N/A		Lokale Zeitzone							N/A
27		ID_GET_ZONE		32			N/A			Get Local Zone																N/A		Lokale Zeitzone holen						N/A
28		ID_TIPS7		128			N/A			The time server has been set, so the time will be set by time server.									N/A		Der Zeitserver ist eingerichtet, die Zeit wird nun vom Zeitserver bereitgestellt!		N/A

[p20_history_frame.htm:Number]
0

[p20_history_frame.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[p21_history_title.htm:Number]
4

[p21_history_title.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_DATA			16			N/A			Data History			N/A		Datenhistorie		N/A
2		ID_ALARM		16			N/A			Alarm History			N/A		Alarmhistorie		N/A
3		ID_LOG			16			N/A			Log				N/A		Bericht			N/A
4		ID_BAT			16			N/A			Battery				N/A		Batterie		N/A


[p22_history_dataquery.htm:Number]
53

[p22_history_dataquery.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN	FULL_IN_LOCALE					ABBR_IN_LOCALE
1		ID_HISTORY_HEAD		32			N/A			Query Data History				N/A		Abfrage Datenhistorie				N/A
2		ID_DEVICE		16			N/A			Device						N/A		Gerät						N/A
3		ID_SIGNAL		16			N/A			Signal Name					N/A		Signalname					N/A
4		ID_VALUE		8			N/A			Value						N/A		Wert						N/A
5		ID_QUERY_TYPE		16			N/A			Query Type					N/A		Abfrage Typ					N/A
6		ID_QUERY		8			N/A			Query						N/A		Abfragen					N/A
7		ID_HISTORY		16			N/A			History						N/A		Historie					N/A
8		ID_STATISTIC		16			N/A			Statistics					N/A		Statistiken					N/A
9		ID_YEAR			8			N/A			Year						N/A		Jahr						N/A
10		ID_MONTH		8			N/A			Month						N/A		Monat						N/A
11		ID_DAY			8			N/A			Day						N/A		Tag						N/A
12		ID_FROM			8			N/A			From						N/A		Von						N/A
13		ID_TO			8			N/A			To						N/A		Bis						N/A
14		ID_YEAR			8			N/A			Year						N/A		Jahr						N/A
15		ID_MONTH		8			N/A			Month						N/A		Monat						N/A
16		ID_DAY			8			N/A			Day						N/A		Tag						N/A
17		ID_ERROR0		32			N/A			Unknown error.					N/A		Unbekannter Fehler				N/A
18		ID_ERROR1		32			N/A			Success.					N/A		Erfolgreich					N/A
19		ID_ERROR2		32			N/A			No data to be queried.				N/A		Keine Daten					N/A
20		ID_ERROR3		32			N/A			Failure						N/A		Fehler						N/A
21		ID_ERROR4		32			N/A			Failure, you are not authorized.		N/A		Fehler, Sie sind nicht berechtigt!		N/A
22		ID_ERROR5		64			N/A			Failed to communicate with Controller.		N/A		Kommunikationsfehler mit dem Kontroller		N/A
23		ID_QUERY		16			N/A			Query						N/A		Abfragen					N/A
24		ID_SIGNAL		16			N/A			Signal						N/A		Signal						N/A
25		ID_VALUE		16			N/A			Value						N/A		Wert						N/A
26		ID_UNIT			16			N/A			Unit						N/A		Gerät						N/A
27		ID_TIME			16			N/A			Time						N/A		Zeit						N/A
28		ID_DEVICE_NAME		16			N/A			Device Name					N/A		Gerätename					N/A
29		ID_INDEX		16			N/A			Index						N/A		Index						N/A
30		ID_ALARM_STATUS		16			N/A			Status						N/A		Status						N/A
31		ID_INDEX		16			N/A			Index						N/A		Index						N/A
32		ID_DEVICE		16			N/A			Device						N/A		Gerät						N/A
33		ID_SIGNAL		16			N/A			Singal						N/A		Signal						N/A
34		ID_CURRENT_VALUE	16			N/A			Current Value					N/A		Aktueller Wert					N/A
35		ID_AVERAGE_VALUE	16			N/A			Average Value					N/A		Durchschnittswert				N/A
36		ID_STAT_TIME		16			N/A			Statistics Time					N/A		Statistikzeit					N/A
37		ID_MAX_VALUE		16			N/A			Max Value					N/A		Max. Wert					N/A
38		ID_MAX_TIME		16			N/A			Max Time					N/A		Max. Zeit					N/A
39		ID_MIN_VALUE		16			N/A			Min Value					N/A		Min. Wert					N/A
40		ID_MIN_TIME		16			N/A			Min Time					N/A		Min. Zeit					N/A
41		ID_UNIT			16			N/A			Unit						N/A		Einheit						N/A
42		ID_DOWNLOAD		32			N/A			Download					N/A		Download					N/A
43		ID_TIPS			64			N/A			End time should be bigger than start time	N/A		Endzeit muss größer als die Startzeit sein!	N/A
44		ID_MONTH_ERROR		32			N/A			Incorrect month.				N/A		Falsche Monatseingabe!				N/A
45		ID_DAY_ERROR		32			N/A			Incorrect day.					N/A		Falsche Tageseingabe!				N/A
46		ID_HOUR_ERROR		32			N/A			Incorrect hour.					N/A		Falsche Stundeneingabe!				N/A
47		ID_FORMAT_ERROR		64			N/A			Incorrect format.				N/A		Falsches Format!				N/A
48		ID_YEAR_ERROR		32			N/A			Incorrect year.					N/A		Falsche Jahreseingabe!				N/A
49		ID_MINUTE_ERROR		32			N/A			Incorrect minute.				N/A		Falsche Minuteneingabe!				N/A
50		ID_SECOND_ERROR		32			N/A			Incorrect second.				N/A		Falsche Sekundeneingabe!			N/A
51		ID_INCORRECT_PARAM	64			N/A			Incorrect parameter.				N/A		Falsche Einstellungen!				N/A
52		ID_ALL_DEVICE		64			N/A			All Devices					N/A		Alle Geräte					N/A
53		ID_MAX_NUMBER_TIPS	128			N/A			The maximum number of records to be shown is 500.	N/A	Maximal 500 Einträge können angezeigt werden.	N/A



[p23_history_alarmquery.htm:Number]
43

[p23_history_alarmquery.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN	FULL_IN_LOCALE					ABBR_IN_LOCALE
1		ID_ALARM_HEAD		64			N/A			Query Alarm History				N/A		Abfrage Alarmhistorie				N/A
2		ID_YEAR			16			N/A			Year						N/A		Jahr						N/A
3		ID_MONTH		16			N/A			Month						N/A		Monat						N/A
4		ID_DAY			16			N/A			Day						N/A		Tag						N/A
5		ID_FROM			16			N/A			From						N/A		Von						N/A
6		ID_TO			16			N/A			To						N/A		Bis						N/A
7		ID_YEAR			16			N/A			Year						N/A		Jahr						N/A
8		ID_MONTH		16			N/A			Month						N/A		Monat						N/A
9		ID_DAY			16			N/A			Day						N/A		Tag						N/A
10		ID_DEVICE_NAME		16			N/A			Device						N/A		Gerät						N/A
11		ID_QUERY		16			N/A			Query						N/A		Abfragen					N/A
12		ID_DOWNLOAD		16			N/A			Download					N/A		Download					N/A
13		ID_INDEX		16			N/A			Index						N/A		Index						N/A
14		ID_DEVICE		16			N/A			Device						N/A		Gerät						N/A
15		ID_ALARM_LEVEL		16			N/A			Alarm Level					N/A		Alarmlevel					N/A
16		ID_VALUE		16			N/A			Value						N/A		Wert						N/A
17		ID_START_TIME		16			N/A			Start Time					N/A		Startzeit					N/A
18		ID_END_TIME		16			N/A			End Time					N/A		Endzeit						N/A
19		ID_ERROR0		32			N/A			Unknown error.					N/A		Unbekannter Fehler				N/A
20		ID_ERROR1		32			N/A			Success.					N/A		Erfolgreich					N/A
21		ID_ERROR2		32			N/A			No data.					N/A		Keine Daten					N/A
22		ID_ERROR3		32			N/A			Failure.					N/A		Fehler						N/A
23		ID_ERROR4		32			N/A			Failure, you are not authorized.		N/A		Fehler, Sie sind nicht berechtigt!		N/A
24		ID_ERROR5		64			N/A			Failed to communicate with the Controller.	N/A		Kommunikationsfehler mit dem Kontroller		N/A
25		ID_ERROR6		64			N/A			Cleared successfully.				N/A		Löschung erfolgreich				N/A
26		ID_OA			16			N/A			OA						N/A		Wartungsalarm					N/A
27		ID_MA			16			N/A			MA						N/A		Dringender Alarm				N/A
28		ID_CA			16			N/A			CA						N/A		Kritischer Alarm				N/A
29		ID_SIGNAL_NAME		16			N/A			Name						N/A		Name						N/A
30		ID_CLEAR_CLARM		16			N/A			Clear Alarm					N/A		Alarm löschen					N/A
31		ID_SET			16			N/A			Set						N/A		Setzen						N/A
32		ID_DOWNLOAD		32			N/A			Download					N/A		Download					N/A
33		ID_TIPS			64			N/A			End time should be bigger than start time	N/A		Endzeit muss größer als die Startzeit sein!	N/A
34		ID_MONTH_ERROR		32			N/A			Incorrect month.				N/A		Falsche Monatseingabe!				N/A
35		ID_DAY_ERROR		32			N/A			Incorrect day.					N/A		Falsche Tageseingabe!				N/A
36		ID_HOUR_ERROR		32			N/A			Incorrect hour.					N/A		Falsche Stundeneingabe!				N/A
37		ID_FORMAT_ERROR		64			N/A			Incorrect format.				N/A		Falsches Format!				N/A
38		ID_YEAR_ERROR		32			N/A			Incorrect year.					N/A		Falsche Jahreseingabe!				N/A
39		ID_MINUTE_ERROR		32			N/A			Incorrect minute.				N/A		Falsche Minuteneingabe!				N/A
40		ID_SECOND_ERROR		32			N/A			Incorrect second.				N/A		Falsche Sekundeneingabe!			N/A
41		ID_INCORRECT_PARAM	64			N/A			Incorrect parameter.				N/A		Falsche Einstellungen!				N/A
42		ID_ALL_DEVICE		64			N/A			All Devices					N/A		Alle Geräte					N/A
43		ID_MAX_NUMBER_TIPS	128			N/A			Maximum number of records to be shown is 500.	N/A		Maximal 500 Einträge können angezeigt werden.	N/A


[p24_history_logquery.htm:Number]
93

[p24_history_logquery.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN	FULL_IN_LOCALE					ABBR_IN_LOCALE
1		ID_LOG_HEAD		32			N/A			Query History Log				N/A		Abfrage Historielog				N/A
2		ID_YEAR			16			N/A			Year						N/A		Jahr						N/A
3		ID_MONTH		16			N/A			Month						N/A		Monat						N/A
4		ID_DAY			16			N/A			Day						N/A		Day						N/A
5		ID_FROM			16			N/A			From						N/A		Von						N/A
6		ID_TO			16			N/A			To						N/A		Bis						N/A
7		ID_YEAR			16			N/A			Year						N/A		Jahr						N/A
8		ID_MONTH		16			N/A			Month						N/A		Monat						N/A
9		ID_DAY			16			N/A			Day						N/A		Tag						N/A
10		ID_QUERY_TYPE		16			N/A			Query Type					N/A		Abfrage Typ					N/A
11		ID_QUERY		16			N/A			Query						N/A		Abfragen						N/A
12		ID_CONTROL_LOG		16			N/A			Control Log					N/A		Kontroll Log					N/A
13		ID_SYSTEM_LOG		16			N/A			System Log					N/A		Systemp Log					N/A
14		ID_ERROR0		32			N/A			Unknown error.					N/A		Unbekannter Fehler!				N/A
15		ID_ERROR1		32			N/A			Control log success.				N/A		Kontroll Log erfolgreich!			N/A
16		ID_ERROR2		32			N/A			No data to be queried.				N/A		Keine Daten					N/A
17		ID_ERROR3		32			N/A			Failure.					N/A		Fehler!						N/A
18		ID_ERROR4		64			N/A			Failure, you are not authorized.		N/A		Fehler, Sie sind nicht berechtigt!		N/A
19		ID_ERROR5		32			N/A			Failed to communicate with Controller.		N/A		Kommunikationsfehler mit dem Kontroller		N/A
20		ID_ERROR6		32			N/A			Acquired battery log successfully.		N/A		Batterieprotok.erfolgr.erstellt			N/A
21		ID_ERROR7		32			N/A			Acquired system log successfully.		N/A		System Log.erfolgr.erstellt			N/A
22		ID_BATTERY_TEST_REASON0	64			N/A			Start planned test				N/A		Start geplannten Batt.test			N/A
23		ID_BATTERY_TEST_REASON1	64			N/A			Start manual test				N/A		Start manuellen Batt.test			N/A
24		ID_BATTERY_TEST_REASON2	64			N/A			Start AC fail test				N/A		Start Netzausfalltest				N/A
25		ID_BATTERY_TEST_REASON3	64			N/A			Start test for Master Power start test		N/A		Start Test für Master - Starttest		N/A
26		ID_BATTERY_TEST_REASON4	64			N/A			Other reason					N/A		Andere Ursache					N/A
27		ID_BATTERY_END_REASON0	64			N/A			End by manual					N/A		Manuell beendet					N/A
28		ID_BATTERY_END_REASON1	64			N/A			End by alarm					N/A		Beendet wegen Alarm				N/A
29		ID_BATTERY_END_REASON2	64			N/A			Time up						N/A		Zeit über					N/A
30		ID_BATTERY_END_REASON3	64			N/A			Capacity full					N/A		Gesamtkapazität					N/A
31		ID_BATTERY_END_REASON4	64			N/A			End by voltage					N/A		Beendet wegen Spannung				N/A
32		ID_BATTERY_END_REASON5	64			N/A			End by AC fail					N/A		Beendet wegen Netzausfall			N/A
33		ID_BATTERY_END_REASON6	64			N/A			End by AC restore				N/A		Beendet wegen Netzwiederkehr			N/A
34		ID_BATTERY_END_REASON7	64			N/A			End by AC fail test discharge			N/A		Beendet wegen Netzausfall			N/A
35		ID_BATTERY_END_REASON8	64			N/A			End test for Master Power stop test		N/A		Beende Test für Master - Stoptest		N/A
36		ID_BATTERY_END_REASON9	128			N/A			End a PowerSplit BT for Auto/Man turn to Man	N/A		End a PowerSplit BT for Auto/Man turn to Man	N/A
37		ID_BATTERY_END_REASON10	128			N/A			End PowerSplit Man-BT for Auto/Man turn to Auto	N/A		End PowerSplit Man-BT for Auto/Man turn to Auto	N/A
38		ID_BATTERY_END_REASON11	64			N/A			End by other reason				N/A		Beendet wegen anderem Grund			N/A
39		ID_BATTERY_TEST_RESULT0	64			N/A			No result					N/A		Kein Ergebnis					N/A
40		ID_BATTERY_TEST_RESULT1	64			N/A			Good result					N/A		Gutes Ergebnis					N/A
41		ID_BATTERY_TEST_RESULT2	64			N/A			Bad result					N/A		Schlechtes Ergebnis				N/A
42		ID_BATTERY_TEST_RESULT3	64			N/A			It's a Power Split Test				N/A		Power Split Test				N/A
43		ID_BATTERY_TEST_RESULT4	64			N/A			Other result					N/A		Anderer Grund					N/A
44		SYS_LOG_HEAD0		64			N/A			Index						N/A		Index						N/A
45		SYS_LOG_HEAD1		64			N/A			Task Name					N/A		Aufgabenname					N/A
46		SYS_LOG_HEAD2		64			N/A			Information Level				N/A		Informationslevel				N/A
47		SYS_LOG_HEAD3		64			N/A			Log Time					N/A		Log Zeit					N/A
48		SYS_LOG_HEAD4		64			N/A			Information					N/A		Information					N/A
49		CTL_LOG_HEAD0		64			N/A			Index						N/A		Index						N/A
50		CTL_LOG_HEAD1		64			N/A			Equipment Name					N/A		Gerätename					N/A
51		CTL_LOG_HEAD2		64			N/A			Signal Name					N/A		Signalname					N/A
52		CTL_LOG_HEAD3		64			N/A			Control Value					N/A		Kontrollwert					N/A
53		CTL_LOG_HEAD4		64			N/A			Unit						N/A		Einheit						N/A
54		CTL_LOG_HEAD5		64			N/A			Time						N/A		Zeit						N/A
55		CTL_LOG_HEAD6		64			N/A			Sender Name					N/A		Name Sender					N/A
56		CTL_LOG_HEAD7		64			N/A			Sender Type					N/A		Typ Sender					N/A
57		CTL_LOG_HEAD8		64			N/A			Control Result					N/A		Resultat Kontrolle				N/A
58		ID_CTL_RESULT0		64			N/A			Success						N/A		Erfolgreich					N/A
59		ID_CTL_RESULT1		64			N/A			No memory					N/A		Kein freier Speicher				N/A
60		ID_CTL_RESULT2		64			N/A			Time out					N/A		Zeitüberschreitung				N/A
61		ID_CTL_RESULT3		64			N/A			Failure						N/A		Fehler						N/A
62		ID_CTL_RESULT4		64			N/A			Communication busy				N/A		Kommun. beschäftigt				N/A
63		ID_CTL_RESULT5		64			N/A			Control was suppressed				N/A		Kontrolle unterdrückt				N/A
64		ID_CTL_RESULT6		64			N/A			Control was disabled				N/A		Kontrolle deaktiviert				N/A
65		ID_CTL_RESULT7		64			N/A			Control was disabled				N/A		Kontrolle deaktiviert				N/A
66		ID_DOWNLOAD		32			N/A			Download					N/A		Download					N/A
67		ID_TIPS			64			N/A			End time should be bigger than start time	N/A		Endzeit muss größer als die Startzeit sein!	N/A
68		ID_MONTH_ERROR		32			N/A			Incorrect month.				N/A		Falsche Monatseingabe!				N/A
69		ID_DAY_ERROR		32			N/A			Incorrect day.					N/A		Falsche Tageseingabe!				N/A
70		ID_HOUR_ERROR		32			N/A			Incorrect hour.					N/A		Falsche Stundeneingabe!				N/A
71		ID_FORMAT_ERROR		64			N/A			Incorrect format.				N/A		Falsches Format!				N/A
72		ID_YEAR_ERROR		32			N/A			Incorrect year.					N/A		Falsche Jahreseingabe!				N/A
73		ID_MINUTE_ERROR		32			N/A			Incorrect minute.				N/A		Falsche Minuteneingabe!				N/A
74		ID_SECOND_ERROR		32			N/A			Incorrect second.				N/A		Falsche Sekundeneingabe!			N/A
75		ID_INCORRECT_PARAM	64			N/A			Incorrect parameter.				N/A		Falsche Einstellungen!				N/A
76		ID_DISEL_TEST_REASON0	64			N/A			Planned test					N/A		Geplanter Test					N/A
77		ID_DISEL_TEST_REASON1	64			N/A			Manual start					N/A		Manueller Start					N/A
78		ID_DISEL_TEST_RESULT0	64			N/A			Normal						N/A		Normal						N/A
79		ID_DISEL_TEST_RESULT1	64			N/A			Manual Stop					N/A		Manueller Stop					N/A
80		ID_DISEL_TEST_RESULT2	64			N/A			Time is up					N/A		Zeit ist zu hoch				N/A
81		ID_DISEL_TEST_RESULT3	64			N/A			In Manual Mode					N/A		In manuellem Modus				N/A
82		ID_DISEL_TEST_RESULT4	64			N/A			Low Battery Voltage				N/A		Niedrige Batt.-Spannung				N/A
83		ID_DISEL_TEST_RESULT5	64			N/A			High Water Temperature				N/A		Hohe Wassertemperatur				N/A
84		ID_DISEL_TEST_RESULT6	64			N/A			Low Oil Pressure				N/A		Niedriger Öldruck				N/A
85		ID_DISEL_TEST_RESULT7	64			N/A			Low Fuel Level					N/A		Niedriger Kraftstoffstand			N/A
86		ID_DISEL_TEST_RESULT8	64			N/A			Diesel Failure					N/A		Fehler Dieselgenerator				N/A
87		ID_DISEL_TEST_HEAD0	64			N/A			Index						N/A		Index						N/A
88		ID_DISEL_TEST_HEAD1	64			N/A			Start Time					N/A		Startzeit					N/A
89		ID_DISEL_TEST_HEAD2	64			N/A			End Time					N/A		Endzeit						N/A
90		ID_DISEL_TEST_HEAD3	64			N/A			Start Reason					N/A		Grund für Start					N/A
91		ID_DISEL_TEST_HEAD4	64			N/A			Test Result					N/A		Testergebnis					N/A
92		ID_DISEL_LOG		64			N/A			Diesel Test Log					N/A		Dieselgen. Testprotokoll			N/A
93		ID_MAX_NUMBER_TIPS	128			N/A			The maximum number of records to be shown is 500.	N/A	Maximal 500 Einträge können angezeigt werden.	N/A


[p25_online_frame.htm:Number]
0

[p25_online_frame.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[p26_online_title.htm:Number]
3

[p26_online_title.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL		MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_MODIFY_SCUP		16				N/A			Modify Controller		N/A		ACU+ ändern		N/A
2		ID_MODIFY_DEVICE	16				N/A			Modify Device			N/A		Gerät ändern		N/A
3		ID_MODIFY_ALARM		16				N/A			Modify Signal			N/A		Signal ändern		N/A

[p27_online_modifysystem.htm:Number]
21

[p27_online_modifysystem.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR		FULL_IN_EN							ABBR_IN_EN	FULL_IN_LOCALE							ABBR_IN_LOCALE
1		ID_ERROR0		32				N/A			Failure.							N/A		Fehler								N/A
2		ID_ERROR1		32				N/A			Success.							N/A		Erfolgreich							N/A
3		ID_ERROR2		32				N/A			Unknown error							N/A		Unbekannter Fehler						N/A
4		ID_ERROR3		32				N/A			Failure, you are not authorized.				N/A		Fehler, Sie sind nicht berechtigt!				N/A
5		ID_ERROR4		32				N/A			Failed to communicate						N/A		Kommunikationsfehler						N/A
6		ID_DEVICE		32				N/A			Device Name							N/A		Gerätename							N/A
7		ID_SIGNAL		32				N/A			Signal Name							N/A		Signalname							N/A
8		ID_VALUE		32				N/A			Value								N/A		Wert								N/A
9		ID_SETTING_VALUE	32				N/A			Set Value							N/A		Neuer Wert							N/A
10		ID_SET			32				N/A			Set								N/A		Setzen								N/A
11		ID_TIPS0		128				N/A			Input Error.							N/A		Eingabefehler							N/A
12		ID_TIPS1		128				N/A			Invalid characters were included in input.\nPlease try again.	N/A		Ungültige Zeichen in der Eingabe.\nBitte erneut versuchen!	N/A
13		ID_TIPS2		128				N/A			Modify								N/A		Ändern								N/A
14		ID_SIGNAL_TYPE		32				N/A			Signal Type							N/A		Signaltyp							N/A
15		ID_ERROR5		32				N/A			Failure, Controller is hardware protected			N/A		Fehler, Kontroller ist HW geschützt!				N/A
16		ID_SET2			32				N/A			Set								N/A		Setzen								N/A
17		ID_SITE			32				N/A			Site								N/A		Site								N/A
18		ID_ERROR5		64				N/A			The length must be no more than 32 characters			N/A		Max. 32 Zeichen erlaubt						N/A
19		ID_SITE_NAME		64				N/A			Site Name							N/A		Site Name							N/A
20		ID_SITE_LOCATION	64				N/A			Site Location							N/A		Site Lokation							N/A
21		ID_SITE_DESCTIPTION	64				N/A			Site Description						N/A		Site Beschreibung						N/A


[p28_online_modifydevice.htm:Number]
23

[p28_online_modifydevice.htm]
#Sequence ID	RES_ID				MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN									ABBR_IN_EN	FULL_IN_LOCALE											ABBR_IN_LOCALE
1		ID_ERROR0			32			N/A			Failure.									N/A		Fehler!												N/A
2		ID_ERROR1			32			N/A			Success.									N/A		Erfolgreich											N/A
3		ID_ERROR2			32			N/A			Unknown error.									N/A		Unbekannter Fehler										N/A
4		ID_ERROR3			32			N/A			Failure, you are not authorized.						N/A		Fehler, Sie sind nicht berechtigt!								N/A
5		ID_ERROR4			64			N/A			Failed to communicate								N/A		Kommunikationsfehler										N/A
6		ID_DEVICE			32			N/A			Device Name									N/A		Gerätename											N/A
7		ID_NEWDEVICE			64			N/A			New Device Name									N/A		Neuer Gerätename										N/A
8		ID_SET				16			N/A			Set										N/A		Setzen												N/A
9		ID_TIPS0			64			N/A			Enter device name please.							N/A		Geben Sie neuen Gerätenamen ein.								N/A
10		ID_TIPS1			128			N/A			None of these characters must not be included in user name.\nPlease try again.	N/A		Keine dieser Zeichen dürfen im Benutzernamen verwendet werden. \nBitte erneut versuchen!	N/A
11		ID_TIPS2			32			N/A			No Device									N/A		Kein Gerät											N/A
12		ID_TIPS3			32			N/A			Modify Device									N/A		Gerät ändern											N/A
13		ID_SET				16			N/A			Set										N/A		Setzen												N/A
14		ID_MODIY_FULL_NAME		16			N/A			Full Name									N/A		Voller Name											N/A
15		ID_MODIY_ABBR_NAME		16			N/A			Abbr Name									N/A		Abkürzung											N/A
16		ID_INDEX			16			N/A			Index										N/A		Index												N/A
17		ID_DEVICE_ABBR_NAME		32			N/A			Device Abbr Name								N/A		Abgeküzter Gerätename										N/A
18		ID_MODIFY_NAME_TYPE		32			N/A			Modify Name Type								N/A		Namenstyp ändern										N/A
19		ID_TOOLONG_NAME16		64			N/A			The length must be no more than 16 characters					N/A		Max. 16 Zeichen erlaubt										N/A
20		ID_TOOLONG_NAME32		64			N/A			The length must be no more than 32 characters					N/A		Max. 32 Zeichen erlaubt										N/A
21		ID_ERROR5			64			N/A			Failure, Controller is hardware protected					N/A		Fehler! Kontroller ist HW geschützt!								N/A
22		ID_INVALID_CHAR			64			N/A			Invalid character.								N/A		Nicht erlaubtes Zeichen										N/A
23		ID_TIPS4			128			N/A			[Tips]The new device name will be showed in device tree after you reconnect.	N/A		[TIPP]Der neue Gerätename wird nach erneuter Verbindung angezeigt!				N/A

[p29_online_modifyalarm.htm:Number]
48

[p29_online_modifyalarm.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN	FULL_IN_LOCALE						ABBR_IN_LOCALE
1		ID_ERROR0		32			N/A			Failure.					N/A		Fehler!							N/A
2		ID_ERROR1		32			N/A			Success.					N/A		Erfolgreich						N/A
3		ID_ERROR2		32			N/A			Unknown error.					N/A		Unbekannter Fehler					N/A
4		ID_ERROR3		32			N/A			Failure, you are not authorized.		N/A		Fehler, Sie sind nicht berechtigt!			N/A
5		ID_ERROR4		32			N/A			Failed to communicate				N/A		Kommunikationsfehler					N/A
6		ID_ERROR5		64			N/A			Failure, Controller is hardware protected	N/A		Fehler: Kontroller ist HW geschützt!			N/A
7		ID_ALARM_HEAD		32			N/A			Query Device Type				N/A		Abfragen Gerätetyp						N/A
8		ID_SINGAL_EXPLORE	32			N/A			Explore Signals					N/A		Signale durchsuchen					N/A
9		ID_INDEX		32			N/A			Index						N/A		Index							N/A
10		ID_SIGNAL_NAME		32			N/A			Full Signal Name				N/A		Voller Signalname					N/A
11		ID_ALARM_LEVEL		32			N/A			Alarm Level					N/A		Alarmlevel						N/A
12		ID_NEW_NAME		32			N/A			New Name					N/A		Neuer Name						N/A
13		ID_NEW_LEVEL		32			N/A			New Level					N/A		Neuer Level						N/A
14		ID_SET			8			N/A			Set						N/A		Setzen							N/A
15		ID_INDEX		16			N/A			Index						N/A		Index							N/A
16		ID_SIGNAL_NAME		32			N/A			Full Signal Name				N/A		Voller Signalname					N/A
17		ID_NEW_NAME		16			N/A			New Name					N/A		Neuer Name						N/A
18		ID_SET			8			N/A			Set						N/A		Setzen							N/A
19		ID_DEVICE_NAME		16			N/A			Device						N/A		Gerät							N/A
20		ID_TYPE			16			N/A			Type						N/A		Typ							N/A
21		ID_NA			16			N/A			NA						N/A		Kein Alarm						N/A
22		ID_OA			16			N/A			OA						N/A		Wartungsalarm						N/A
23		ID_MA			16			N/A			MA						N/A		Dringender Alarm					N/A
24		ID_CA			16			N/A			CA						N/A		Kritischer Alarm					N/A
25		ID_SAMPLE_SIGNAL	32			N/A			Status Signal					N/A		Signalstatus						N/A
26		ID_CONTROL_SIGNAL	32			N/A			Control Signal					N/A		Signalkontrolle						N/A
27		ID_SETTING_SIGNAL	32			N/A			Setting Signal					N/A		Signaleinstellungen					N/A
28		ID_ALARM_SIGNAL		32			N/A			Alarm Signal					N/A		Alarm Signal						N/A
29		ID_NO_SAMPLE		32			N/A			No Status Signal				N/A		Kein Signalstatus					N/A
30		ID_NO_CONTROL		32			N/A			No Control Signal				N/A		Keine Signalkontrolle					N/A
31		ID_NO_SETTING		32			N/A			No Setting Signal				N/A		Keine Signaleinstellungen				N/A
32		ID_NO_ALARM		32			N/A			No Alarm Signal					N/A		Kein Alarmsignal					N/A
33		ID_NO_SIGNAL_TYPE	32			N/A			No this signal type				N/A		Signaltyp n.vorhanden					N/A
34		ID_VAR_SET		8			N/A			Set						N/A		Setzen							N/A
35		ID_SIGNAL_TYPE		32			N/A			Signal Type					N/A		Signaltyp						N/A
36		ID_SHOW_TIPS0		32			N/A			The new name can't be blank.			N/A		Neuer Name darf nicht leer sein				N/A
37		ID_SET			8			N/A			Set						N/A		Setzen							N/A
38		ID_SIGNAL_ABBR_NAME	32			N/A			Signal Abbr Name				N/A		Abgeküzter Signalname					N/A
39		ID_SIGNAL_ABBR_NAME	32			N/A			Signal Abbr Name				N/A		Abgeküzter Signalname					N/A
40		ID_MODIY_FULL_NAME	32			N/A			Full Name					N/A		Voller Name						N/A
41		ID_MODIY_ABBR_NAME	32			N/A			Abbr Name					N/A		Abgekürzter Name					N/A
42		ID_MODIY_FULL_NAME	32			N/A			Full Name					N/A		Voller Name						N/A
43		ID_MODIY_ABBR_NAME	32			N/A			Abbr Name					N/A		Abgekürzter Name					N/A
44		ID_NEW_NAME_TYPE	32			N/A			Modify type					N/A		Typ ändern						N/A
45		ID_NEW_NAME_TYPE	32			N/A			Modify type					N/A		Typ ändern						N/A
46		ID_INVALID_CHAR		64			N/A			Invalid character				N/A		Zeichen nicht erlaubt					N/A
47		ID_TOOLONG_NAME16	64			N/A			The length must be no more than 16 characters	N/A		Max. 16 Zeichen erlaubt					N/A
48		ID_TOOLONG_NAME32	64			N/A			The length must be no more than 32 characters	N/A		Max. 32 Zeichen erlaubt					N/A

[p30_acu_signal_value.htm:Number]
13

[p30_acu_signal_value.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL		MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN	FULL_IN_LOCALE				ABBR_IN_LOCALE
1		ID_ERROR0		32			N/A			Failure.					N/A		Fehler!					N/A
2		ID_ERROR1		32			N/A			Success.					N/A		Erfolgreich				N/A
3		ID_ERROR2		32			N/A			Unknown error.					N/A		Unbekannter Fehler			N/A
4		ID_ERROR3		32			N/A			Failure, you are not authorized.		N/A		Fehler, Sie sind nicht berechtigt!	N/A
5		ID_ERROR4		32			N/A			Failed to communicate				N/A		Kommunikationsfehler			N/A
6		ID_ERROR5		32			N/A			Failure, Controller is hardware protected	N/A		Fehler: Kontroller ist HW geschüzt!	N/A
7		ID_TIPS2		32			N/A			No device					N/A		Kein Gerät				N/A
8		ID_DEVICE		32			N/A			Equipment					N/A		Ausstattung				N/A
9		ID_PRODUCT_NUMBER	32			N/A			Product Number					N/A		Produktnummer				N/A
10		ID_PRODUCT_VERSION	32			N/A			Product Revision				N/A		Produktrevision				N/A
11		ID_PRODUCT_SERIAL	32			N/A			Serial Number					N/A		Seriennummer				N/A
12		ID_PRODUCT_SWVERSION	32			N/A			Software Revision				N/A		Softwarerevision			N/A
13		ID_PRODUCT_INFO_HEAD	32			N/A			Product Information				N/A		Produktinformation			N/A

[p31_close_system.htm:Number]
9

[p31_close_system.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN								ABBR_IN_EN	FULL_IN_LOCALE						ABBR_IN_LOCALE
1		ID_HEAD			16			N/A			Stop Controller								N/A		Kontr. stoppen						N/A
2		ID_TIPS			128			N/A			Upload/Download needs to stop the controller. Do you want to stop the controller?	N/A		Zum Upload/Download muss der Kontroller gestoppt werden. Kontroller stoppen?	N/A
3		ID_CLOSE_SCUP		16			N/A			Stop Controller								N/A		Kontr. stoppen						N/A
4		ID_CANCEL		16			N/A			Cancel									N/A		Abbruch							N/A
5		ID_ERROR0		32			N/A			Unknown error.								N/A		Unbekannter Fehler!					N/A
6		ID_ERROR1		128			N/A			Controller was stopped successfully. You can download file.		N/A		Kontroller wurde erfolgreich gestoppt. Download kann gestartet werden.	N/A
7		ID_ERROR2		64			N/A			Fail to stop Controller.						N/A		Kontroller konnte nicht gestoppt werden			N/A
8		ID_ERROR3		64			N/A			You are not authorized to stop the controller.				N/A		Sie sind nicht berechtigt den Kontroller zu stoppen!	N/A
9		ID_ERROR4		64			N/A			Failed to communicate with the controller.				N/A		Kommunikationsfehler mit dem Kontroller			N/A

[p32_start_system.htm:Number]
9


[p32_start_system.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN								ABBR_IN_EN	FULL_IN_LOCALE					ABBR_IN_LOCALE
1		ID_HEAD			16			N/A			Start Controller							N/A		Kontr. starten						N/A
2		ID_TIPS			128			N/A			Download is ready. Do you want to start Controller?			N/A		Der Download ist fertig. Kontroller neu starten?	N/A
3		ID_START_SCUP		16			N/A			Start Controller							N/A		Kontr. starten						N/A
4		ID_CANCEL		16			N/A			Cancel									N/A		Abbruch							N/A
5		ID_ERROR0		32			N/A			Unknown error.								N/A		Unbekannter Fehler!					N/A
6		ID_ERROR1		128			N/A			Controller was started successfully. You can connect in 1 minute.	N/A		Kontroller wurde erfolgreich gestartet. Verbindung in ca. 1 Min. möglich.	N/A
7		ID_ERROR2		64			N/A			Fail to stop the controller.						N/A		Kontroller konnte nicht gestartet werden		N/A
8		ID_ERROR3		64			N/A			You are not authorized to start the controller!				N/A		Sie sind nicht berechtigt den Kontroller zu starten!	N/A
9		ID_ERROR4		64			N/A			Failed to communicate with the controller.				N/A		Kommunikationsfehler mit dem Kontroller			N/A

[p33_replace_file.htm:Number]
43


[p33_replace_file.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN							ABBR_IN_EN	FULL_IN_LOCALE						ABBR_IN_LOCALE
1		ID_REPLACE_HEAD		16			N/A			Replace File							N/A		Datei ersetzen						N/A
2		ID_REPLACE_UPLOADED	32			N/A			Has been downloaded						N/A		Wurde gedownloaded					N/A
3		ID_SELECE_FILE_TYPE	64			N/A			Please select download file type				N/A		Bitte wählen Sie Download Dateityp			N/A
4		ID_FILE_CONFIGURE	16			N/A			Configuration File						N/A		Konfigurationsdatei					N/A
5		ID_FILE_APPLICATION	32			N/A			Application File						N/A		Applikationsdatei					N/A
6		ID_SELECT_REPLACE_FILE	64			N/A			Please select the file that is to be replaced			N/A		Bitte wählen Sie die zu ersetzende Datei		N/A
7		ID_REPLACE		16			N/A			Replace								N/A		Ersetzen						N/A
8		ID_RETURN		16			N/A			Return								N/A		Zurück							N/A
9		ID_DOWNLOAD_HEAD	32			N/A			Upload File							N/A		Upload Datei						N/A
10		ID_FILE1		32			N/A			Select File							N/A		Wählen Sie die Datei					N/A
11		ID_FILE0		32			N/A			File in Controller						N/A		Datei im Kontroller					N/A
12		ID_ERROR0		32			N/A			Unknown error.							N/A		Unbekannter Fehler					N/A
13		ID_ERROR1		64			N/A			File successfully uploaded.					N/A		Datei erfolgreich geuploaded!				N/A
14		ID_ERROR2		64			N/A			Failed to download file.					N/A		Dateidownload fehlgeschlagen!				N/A
15		ID_ERROR3		64			N/A			Failed to download file, the file is too large.			N/A		Dateidownload fehlgeschlagen! Datei ist zu groß!	N/A
16		ID_ERROR4		64			N/A			Failure, you are not authorized.				N/A		Fehler, Sie sind nicht berechtigt!			N/A
17		ID_ERROR5		64			N/A			Controller successfully started.				N/A		Kontroller erfolgreich gestartet			N/A
18		ID_ERROR6		64			N/A			Successfully to download file.					N/A		Datei erfolgreich gedownloaded				N/A
19		ID_ERROR7		64			N/A			Failed to download file.					N/A		Dateidownload fehlgeschlagen!				N/A
20		ID_ERROR8		64			N/A			Failed to upload file.						N/A		Dateiupload fehlgeschlagen!				N/A
21		ID_ERROR9		64			N/A			File successfully uploaded.					N/A		Datei erfolgreich geuploaded!				N/A
22		ID_UPLOAD		64			N/A			Upload								N/A		Upload							N/A
23		ID_DOWNLOAD		64			N/A			Upload								N/A		Upload							N/A
24		ID_STARTSCUP		64			N/A			Start Controller						N/A		Kontr. starten						N/A
25		ID_SHOWTIPS0		64			N/A			Are you sure you want to start the Controller?			N/A		Sind Sie sicher den Kontroller zu starten?		N/A
26		ID_SHOWTIPS1		128			N/A			Please reboot Controller before you leave this page. \n Are you sure to leave?		N/A	Bitte den Kontroller neu starten bevor Sie diese Seite verlassen\nSind Sie sicher?	N/A
27		ID_SHOWTIPS2		128			N/A			Controller will reboot. Close this IE and wait a couple of minutes before connecting.	N/A	Kontroller wird neu gestartet.Bitte schliessen Sie den IE und warten einige Minuten bevor erneuter Verbindung!	N/A
28		ID_SHOWTIPS3		64			N/A			It's time to start the controller.					N/A		Zeit den Kontroller neu zu starten!		N/A
29		ID_SHOWTIPS4		64			N/A			This format isn't supported, select again please.		N/A		Dieses Format wird nicht unterstützt, bitte erneut versuchen!	N/A
30		ID_CONFIG_TAR		32			N/A			Configuration Package						N/A		Konfigurationsdatei Paket				N/A
31		ID_LANG_TAR		32			N/A			Language Package						N/A		Sprachdatei Paket					N/A
32		ID_PROGRAM_TAR		32			N/A			Program package							N/A		Programmdatei Paket					N/A
33		ID_SHOWTIPS5		64			N/A			The file name can't be blank.					N/A		Der Dateiname darf nicht leer bleiben!			N/A
34		ID_SHOWTIPS6		64			N/A			Are you sure you want to upload					N/A		Sind Sie sicher den Upload zu starten?			N/A
35		ID_SHOWTIPS7		128			N/A			Incorrect file type or unallowed character included. Please upload *.tar.gz or *.tar.		N/A	Falsche Datei oder n.erlaubte Zeichen. Bitte nur *tar.gz oder *.tar verwenden!	N/A
36		ID_SHOWTIPS8		128			N/A			Are you sure you want to start Controller?			N/A		Sind Sie sicher den Kontroller neu zu starten?		N/A
37		ID_TIPS6		64			N/A			Please wait, Controller is rebooting...				N/A		Bitte warten, Kontroller startet neu...			N/A
38		ID_TIPS7		64			N/A			Since parameters have been modified, Controller is rebooting...	N/A		Einstellungen wurden verändert, Kontroller startet neu...	N/A
39		ID_TIPS8		64			N/A			Controller homepage will be refreshed after			N/A		Kontrollerhomepage wird danach aktualisiert		N/A
40		ID_SOLUTION_FILE	64			N/A			Solution File							N/A		Solution File						N/A
41		ID_UPLOAD		64			N/A			Download							N/A		Download						N/A
42		ID_SHOWTIPS9		512			N/A			Caution: Only Configure Package (file format of tar or tar.gz) can be uploaded. If the uploaded file is NOT correct, the controller will run abnormally. The controller must be restarted manually after the upload.	N/A		Achtung: Nur Konfig.dateipakete (Dateiformate tar oder tar.gz) dürfen hochgeladen werden. Bei falschen Dateien wird der Kontroller nicht korrekt funktionieren. Der Kontroller muss nach dem Upload manuell neu gestartet werden.		N/A
43		ID_ERROR10		64			N/A			Failed to upload file, the controller is hardware protected.	N/A		Datei Upload nicht erfolgreich. Kontroller ist HW geschützt!	N/A


[p34_history_batterylogquery.htm:Number]
909


[p34_history_batterylogquery.htm]
#Sequence ID	RES_ID					MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN						ABBR_IN_EN	FULL_IN_LOCALE						ABBR_IN_LOCALE
1		ID_BATTERY_TEST_START_REASON0		64			N/A			Planned battery test.					N/A		Geplanter Batterietest					N/A
2		ID_BATTERY_TEST_START_REASON1		64			N/A			Manual start of battery test				N/A		Manueller Start Batterietest				N/A
3		ID_BATTERY_TEST_START_REASON2		64			N/A			Mains fail start of battery test			N/A		Starte Batt.Test bei Netzausfall			N/A
4		ID_BATTERY_TEST_START_REASON3		64			N/A			Master controlled start of battery test			N/A		Master kontrollierter Start Batt.Test			N/A
5		ID_BATTERY_TEST_START_REASON4		64			N/A			Other reason						N/A		Anderer Grund						N/A
6		ID_BATTERY_TEST_END_REASON0		64			N/A			Manual stop of test					N/A		Manueller Test Stopp					N/A
7		ID_BATTERY_TEST_END_REASON1		64			N/A			Test stopped due to alarm				N/A		Test wegen Alarm beendet				N/A
8		ID_BATTERY_TEST_END_REASON2		64			N/A			Test stopped due to time-out				N/A		Test wegen Zeitüberschreitung gestoppt			N/A
9		ID_BATTERY_TEST_END_REASON3		64			N/A			Test stopped due to capacity limit exceeded		N/A		Test wegen Kapazitätslimit überschritten gestoppt	N/A
10		ID_BATTERY_TEST_END_REASON4		64			N/A			Test stopped due to voltage limit exceeded		N/A		Test wegen Spannungslimit überschritten gestoppt	N/A
11		ID_BATTERY_TEST_END_REASON5		64			N/A			Test stopped due to mains fail				N/A		Test wegen Netzausfall gestoppt				N/A
12		ID_BATTERY_TEST_END_REASON6		64			N/A			Mains fail test stopped due to mains restored		N/A		Test wegen Netzwiederkehr gestoppt			N/A
13		ID_BATTERY_TEST_END_REASON7		64			N/A			Mains fail test stopped due to test disabled		N/A		Test wegen Deaktivierung gestoppt			N/A
14		ID_BATTERY_TEST_END_REASON8		64			N/A			Master controlled stop of battery test			N/A		Master kontrollierter Stopp Batt.Test			N/A
15		ID_BATTERY_TEST_END_REASON9		64			N/A			PowerSplit test stopped due to manual mode		N/A		PowerSplit Test gestoppt wegen Manuell Modus		N/A
16		ID_BATTERY_TEST_END_REASON10		64			N/A			PowerSplit test stopped due to automatic mode		N/A		PowerSplit Test gestoppt wegen Automtaik Modus		N/A
17		ID_BATTERY_TEST_END_REASON11		64			N/A			Stopped due to other reason				N/A		Gestoppt wegen anderem Grund				N/A
18		ID_BATTERY_TEST_RESULT0			16			N/A			No test result						N/A		Kein Testresultat					N/A
19		ID_BATTERY_TEST_RESULT1			16			N/A			Battery is OK						N/A		Batterie ist O.K.					N/A
20		ID_BATTERY_TEST_RESULT2			16			N/A			Battery is bad						N/A		Batterie ist defekt					N/A
21		ID_BATTERY_TEST_RESULT3			16			N/A			It's a Power Split Test					N/A		PowerSplit Test						N/A
22		ID_BATTERY_TEST_RESULT4			16			N/A			Other result						N/A		Anderes Resultat					N/A
23		ID_ERROR0				64			N/A			Unknown error.						N/A		Unbekannter Fehler					N/A
24		ID_ERROR1				64			N/A			Acquired control log successfully.			N/A		Kontrollprotokoll erfolgreich erstellt			N/A
25		ID_ERROR2				64			N/A			No data to be queried.					N/A		Keine Daten						N/A
26		ID_ERROR3				64			N/A			Failure.						N/A		Fehler!							N/A
27		ID_ERROR4				64			N/A			Failure, you are not authorized.			N/A		Fehler, Sie sind nicht berechtigt!			N/A
28		ID_ERROR5				64			N/A			Failed to communicate with the controller.		N/A		Kommunikationsfehler mit dem Kontroller			N/A
29		ID_ERROR6				64			N/A			Acquired battery log successfully.			N/A		Batterieprotokoll erfolgreich erstellt			N/A
30		ID_ERROR7				64			N/A			Acquired System log successfully.			N/A		Systemprotokoll erfolgreich erstellt			N/A
31		ID_LOG_HEAD				16			N/A			Battery test log query					N/A		Batt.testprotok.Abfrage					N/A
32		ID_TIPS					32			N/A			Select Battery Test log:				N/A		Batterietestprotokoll wählen				N/A
33		ID_QUERY				16			N/A			Query							N/A		Abfragen							N/A
34		ID_HEAD0				32			N/A			Battery 1 Current					N/A		Batteriestrom 1						N/A
35		ID_HEAD1				32			N/A			Battery 1 Voltage					N/A		Batteriespannung 1					N/A
36		ID_HEAD2				32			N/A			Battery 1 Capacity					N/A		Batteriekapazität 1					N/A
37		ID_HEAD3				32			N/A			Battery 2 Current					N/A		Batteriestrom 2						N/A
38		ID_HEAD4				32			N/A			Battery 2 Voltage					N/A		Batteriespannung 2					N/A
39		ID_HEAD5				32			N/A			Battery 2 Capacity					N/A		Batteriekapazität 2					N/A
40		ID_HEAD6				32			N/A			EIB1 Battery 1 Current					N/A		EIB1 Batteriestrom 1					N/A
41		ID_HEAD7				32			N/A			EIB1 Battery 1 Voltage					N/A		EIB1 Batteriespannung 1					N/A
42		ID_HEAD8				32			N/A			EIB1 Battery 1 Capacity					N/A		EIB1 Batteriekapazität 1				N/A
43		ID_HEAD9				32			N/A			EIB1 Battery 2 Current					N/A		EIB1 Batteriestrom 2					N/A
44		ID_HEAD10				32			N/A			EIB1 Battery 2 Voltage					N/A		EIB1 Batteriespannung 2					N/A
45		ID_HEAD11				32			N/A			EIB1 Battery 2 Capacity					N/A		EIB1 Batteriekapazität 2				N/A
46		ID_HEAD12				32			N/A			EIB2 Battery 1 Current					N/A		EIB2 Batteriestrom 1					N/A
47		ID_HEAD13				32			N/A			EIB2 Battery 1 Voltage					N/A		EIB2 Batteriespannung 1					N/A
48		ID_HEAD14				32			N/A			EIB2 Battery 1 Capacity					N/A		EIB2 Batteriekapazität 1				N/A
49		ID_HEAD15				32			N/A			EIB2 Battery 2 Current					N/A		EIB2 Batteriestrom 2					N/A
50		ID_HEAD16				32			N/A			EIB2 Battery 2 Voltage					N/A		EIB2 Batteriespannung 2					N/A
51		ID_HEAD17				32			N/A			EIB2 Battery 2 Capacity					N/A		EIB2 Batteriekapazität 2				N/A
52		ID_HEAD18				32			N/A			EIB3 Battery 1 Current					N/A		EIB3 Batteriestrom 1					N/A
53		ID_HEAD19				32			N/A			EIB3 Battery 1 Voltage					N/A		EIB3 Batteriespannung 1					N/A
54		ID_HEAD20				32			N/A			EIB3 Battery 1 Capacity					N/A		EIB3 Batteriekapazität 1				N/A
55		ID_HEAD21				32			N/A			EIB3 Battery 2 Current					N/A		EIB3 Batteriestrom 2					N/A
56		ID_HEAD22				32			N/A			EIB3 Battery 2 Voltage					N/A		EIB3 Batteriespannung 2					N/A
57		ID_HEAD23				32			N/A			EIB3 Battery 2 Capacity					N/A		EIB3 Batteriekapazität 2				N/A
58		ID_HEAD24				32			N/A			EIB4 Battery 1 Current					N/A		EIB4 Batteriestrom 1					N/A
59		ID_HEAD25				32			N/A			EIB4 Battery 1 Voltage					N/A		EIB4 Batteriespannung 1					N/A
60		ID_HEAD26				32			N/A			EIB4 Battery 1 Capacity					N/A		EIB4 Batteriekapazität 1				N/A
61		ID_HEAD27				32			N/A			EIB4 Battery 2 Current					N/A		EIB4 Batteriestrom 2					N/A
62		ID_HEAD28				32			N/A			EIB4 Battery 2 Voltage					N/A		EIB4 Batteriespannung 2					N/A
63		ID_HEAD29				32			N/A			EIB4 Battery 2 Capacity					N/A		EIB4 Batteriekapazität 2				N/A
64		ID_BATTERY_TEST_SUMMARY0		32			N/A			Index							N/A		Index							N/A
65		ID_BATTERY_TEST_SUMMARY1		32			N/A			Record Time						N/A		Aufnahmezeit						N/A
66		ID_BATTERY_TEST_SUMMARY2		32			N/A			System Voltage						N/A		Systemspannung						N/A
67		ID_BATTERY_SUMMARY_HEAD0		32			N/A			Start Time						N/A		Startzeit						N/A
68		ID_BATTERY_SUMMARY_HEAD1		32			N/A			End Time						N/A		Endzeit							N/A
69		ID_BATTERY_SUMMARY_HEAD2		32			N/A			Start Reason						N/A		Grund für Start						N/A
70		ID_BATTERY_SUMMARY_HEAD3		32			N/A			End Reason						N/A		Grund für Ende						N/A
71		ID_BATTERY_SUMMARY_HEAD4		32			N/A			Test Result						N/A		Testresultat						N/A
72		ID_DOWNLOAD				32			N/A			Download						N/A		Download						N/A
73		ID_HEAD30				32			N/A			SMDU1 Battery 1 Current					N/A		SMDU1 Batteriestrom 1					N/A
74		ID_HEAD31				32			N/A			SMDU1 Battery 1 Voltage					N/A		SMDU1 Batteriespannung 1				N/A
75		ID_HEAD32				32			N/A			SMDU1 Battery 1 Capacity				N/A		SMDU1 Batteriekapazität 1				N/A
76		ID_HEAD33				32			N/A			SMDU1 Battery 2 Current					N/A		SMDU1 Batteriestrom 2					N/A
77		ID_HEAD34				32			N/A			SMDU1 Battery 2 Voltage					N/A		SMDU1 Batteriespannung 2				N/A
78		ID_HEAD35				32			N/A			SMDU1 Battery 2 Capacity				N/A		SMDU1 Batteriekapazität 2				N/A
79		ID_HEAD36				32			N/A			SMDU1 Battery 3 Current					N/A		SMDU1 Batteriestrom 3					N/A
80		ID_HEAD37				32			N/A			SMDU1 Battery 3 Voltage					N/A		SMDU1 Batteriespannung 3				N/A
81		ID_HEAD38				32			N/A			SMDU1 Battery 3 Capacity				N/A		SMDU1 Batteriekapazität 3				N/A
82		ID_HEAD39				32			N/A			SMDU1 Battery 4 Current					N/A		SMDU1 Batteriestrom 4					N/A
83		ID_HEAD40				32			N/A			SMDU1 Battery 4 Voltage					N/A		SMDU1 Batteriespannung 4				N/A
84		ID_HEAD41				32			N/A			SMDU1 Battery 4 Capacity				N/A		SMDU1 Batteriekapazität 4				N/A
85		ID_HEAD42				32			N/A			SMDU2 Battery 1 Current					N/A		SMDU2 Batteriestrom 1					N/A
86		ID_HEAD43				32			N/A			SMDU2 Battery 1 Voltage					N/A		SMDU2 Batteriespannung 1				N/A
87		ID_HEAD44				32			N/A			SMDU2 Battery 1 Capacity				N/A		SMDU2 Batteriekapazität 1				N/A
88		ID_HEAD45				32			N/A			SMDU2 Battery 2 Current					N/A		SMDU2 Batteriestrom 2					N/A
89		ID_HEAD46				32			N/A			SMDU2 Battery 2 Voltage					N/A		SMDU2 Batteriespannung 2				N/A
90		ID_HEAD47				32			N/A			SMDU2 Battery 2 Capacity				N/A		SMDU2 Batteriekapazität 2				N/A
91		ID_HEAD48				32			N/A			SMDU2 Battery 3 Current					N/A		SMDU2 Batteriestrom 3					N/A
92		ID_HEAD49				32			N/A			SMDU2 Battery 3 Voltage					N/A		SMDU2 Batteriespannung 3				N/A
93		ID_HEAD50				32			N/A			SMDU2 Battery 3 Capacity				N/A		SMDU2 Batteriekapazität 3				N/A
94		ID_HEAD51				32			N/A			SMDU2 Battery 4 Current					N/A		SMDU2 Batteriestrom 4					N/A
95		ID_HEAD52				32			N/A			SMDU2 Battery 4 Voltage					N/A		SMDU2 Batteriespannung 4				N/A
96		ID_HEAD53				32			N/A			SMDU2 Battery 4 Capacity				N/A		SMDU2 Batteriekapazität 4				N/A
97		ID_HEAD54				32			N/A			SMDU3 Battery 1 Current					N/A		SMDU3 Batteriestrom 1					N/A
98		ID_HEAD55				32			N/A			SMDU3 Battery 1 Voltage					N/A		SMDU3 Batteriespannung 1				N/A
99		ID_HEAD56				32			N/A			SMDU3 Battery 1 Capacity				N/A		SMDU3 Batteriekapazität 1				N/A
100		ID_HEAD57				32			N/A			SMDU3 Battery 2 Current					N/A		SMDU3 Batteriestrom 2					N/A
101		ID_HEAD58				32			N/A			SMDU3 Battery 2 Voltage					N/A		SMDU3 Batteriespannung 2				N/A
102		ID_HEAD59				32			N/A			SMDU3 Battery 2 Capacity				N/A		SMDU3 Batteriekapazität 2				N/A
103		ID_HEAD60				32			N/A			SMDU3 Battery 3 Current					N/A		SMDU3 Batteriestrom 3					N/A
104		ID_HEAD61				32			N/A			SMDU3 Battery 3 Voltage					N/A		SMDU3 Batteriespannung 3				N/A
105		ID_HEAD62				32			N/A			SMDU3 Battery 3 Capacity				N/A		SMDU3 Batteriekapazität 3				N/A
106		ID_HEAD63				32			N/A			SMDU3 Battery 4 Current					N/A		SMDU3 Batteriestrom 4					N/A
107		ID_HEAD64				32			N/A			SMDU3 Battery 4 Voltage					N/A		SMDU3 Batteriespannung 4				N/A
108		ID_HEAD65				32			N/A			SMDU3 Battery 4 Capacity				N/A		SMDU3 Batteriekapazität 4				N/A
109		ID_HEAD66				32			N/A			SMDU4 Battery 1 Current					N/A		SMDU4 Batteriestrom 1					N/A
110		ID_HEAD67				32			N/A			SMDU4 Battery 1 Voltage					N/A		SMDU4 Batteriespannung 1				N/A
111		ID_HEAD68				32			N/A			SMDU4 Battery 1 Capacity				N/A		SMDU4 Batteriekapazität 1				N/A
112		ID_HEAD69				32			N/A			SMDU4 Battery 2 Current					N/A		SMDU4 Batteriestrom 2					N/A
113		ID_HEAD70				32			N/A			SMDU4 Battery 2 Voltage					N/A		SMDU4 Batteriespannung 2				N/A
114		ID_HEAD71				32			N/A			SMDU4 Battery 2 Capacity				N/A		SMDU4 Batteriekapazität 2				N/A
115		ID_HEAD72				32			N/A			SMDU4 Battery 3 Current					N/A		SMDU4 Batteriestrom 3					N/A
116		ID_HEAD73				32			N/A			SMDU4 Battery 3 Voltage					N/A		SMDU4 Batteriespannung 3				N/A
117		ID_HEAD74				32			N/A			SMDU4 Battery 3 Capacity				N/A		SMDU4 Batteriekapazität 3				N/A
118		ID_HEAD75				32			N/A			SMDU4 Battery 4 Current					N/A		SMDU4 Batteriestrom 4					N/A
119		ID_HEAD76				32			N/A			SMDU4 Battery 4 Voltage					N/A		SMDU4 Batteriespannung 4				N/A
120		ID_HEAD77				32			N/A			SMDU4 Battery 4 Capacity				N/A		SMDU4 Batteriekapazität 4				N/A
121		ID_HEAD78				32			N/A			SMDU5 Battery 1 Current					N/A		SMDU5 Batteriestrom 1					N/A
122		ID_HEAD79				32			N/A			SMDU5 Battery 1 Voltage					N/A		SMDU5 Batteriespannung 1				N/A
123		ID_HEAD80				32			N/A			SMDU5 Battery 1 Capacity				N/A		SMDU5 Batteriekapazität 1				N/A
124		ID_HEAD81				32			N/A			SMDU5 Battery 2 Current					N/A		SMDU5 Batteriestrom 2					N/A
125		ID_HEAD82				32			N/A			SMDU5 Battery 2 Voltage					N/A		SMDU5 Batteriespannung 2				N/A
126		ID_HEAD83				32			N/A			SMDU5 Battery 2 Capacity				N/A		SMDU5 Batteriekapazität 2				N/A
127		ID_HEAD84				32			N/A			SMDU5 Battery 3 Current					N/A		SMDU5 Batteriestrom 3					N/A
128		ID_HEAD85				32			N/A			SMDU5 Battery 3 Voltage					N/A		SMDU5 Batteriespannung 3				N/A
129		ID_HEAD86				32			N/A			SMDU5 Battery 3 Capacity				N/A		SMDU5 Batteriekapazität 3				N/A
130		ID_HEAD87				32			N/A			SMDU5 Battery 4 Current					N/A		SMDU5 Batteriestrom 4					N/A
131		ID_HEAD88				32			N/A			SMDU5 Battery 4 Voltage					N/A		SMDU5 Batteriespannung 4				N/A
132		ID_HEAD89				32			N/A			SMDU5 Battery 4 Capacity				N/A		SMDU5 Batteriekapazität 4				N/A
133		ID_HEAD90				32			N/A			SMDU6 Battery 1 Current					N/A		SMDU6 Batteriestrom 1					N/A
134		ID_HEAD91				32			N/A			SMDU6 Battery 1 Voltage					N/A		SMDU6 Batteriespannung 1				N/A
135		ID_HEAD92				32			N/A			SMDU6 Battery 1 Capacity				N/A		SMDU6 Batteriekapazität 1				N/A
136		ID_HEAD93				32			N/A			SMDU6 Battery 2 Current					N/A		SMDU6 Batteriestrom 2					N/A
137		ID_HEAD94				32			N/A			SMDU6 Battery 2 Voltage					N/A		SMDU6 Batteriespannung 2				N/A
138		ID_HEAD95				32			N/A			SMDU6 Battery 2 Capacity				N/A		SMDU6 Batteriekapazität 2				N/A
139		ID_HEAD96				32			N/A			SMDU6 Battery 3 Current					N/A		SMDU6 Batteriestrom 3					N/A
140		ID_HEAD97				32			N/A			SMDU6 Battery 3 Voltage					N/A		SMDU6 Batteriespannung 3				N/A
141		ID_HEAD98				32			N/A			SMDU6 Battery 3 Capacity				N/A		SMDU6 Batteriekapazität 3				N/A
142		ID_HEAD99				32			N/A			SMDU6 Battery 4 Current					N/A		SMDU6 Batteriestrom 4					N/A
143		ID_HEAD100				32			N/A			SMDU6 Battery 4 Voltage					N/A		SMDU6 Batteriespannung 4				N/A
144		ID_HEAD101				32			N/A			SMDU6 Battery 4 Capacity				N/A		SMDU6 Batteriekapazität 4				N/A
145		ID_HEAD102				32			N/A			SMDU7 Battery 1 Current					N/A		SMDU7 Batteriestrom 1					N/A
146		ID_HEAD103				32			N/A			SMDU7 Battery 1 Voltage					N/A		SMDU7 Batteriespannung 1				N/A
147		ID_HEAD104				32			N/A			SMDU7 Battery 1 Capacity				N/A		SMDU7 Batteriekapazität 1				N/A
148		ID_HEAD105				32			N/A			SMDU7 Battery 2 Current					N/A		SMDU7 Batteriestrom 2					N/A
149		ID_HEAD106				32			N/A			SMDU7 Battery 2 Voltage					N/A		SMDU7 Batteriespannung 2				N/A
150		ID_HEAD107				32			N/A			SMDU7 Battery 2 Capacity				N/A		SMDU7 Batteriekapazität 2				N/A
151		ID_HEAD108				32			N/A			SMDU7 Battery 3 Current					N/A		SMDU7 Batteriestrom 3					N/A
152		ID_HEAD109				32			N/A			SMDU7 Battery 3 Voltage					N/A		SMDU7 Batteriespannung 3				N/A
153		ID_HEAD110				32			N/A			SMDU7 Battery 3 Capacity				N/A		SMDU7 Batteriekapazität 3				N/A
154		ID_HEAD111				32			N/A			SMDU7 Battery 4 Current					N/A		SMDU7 Batteriestrom 4					N/A
155		ID_HEAD112				32			N/A			SMDU7 Battery 4 Voltage					N/A		SMDU7 Batteriespannung 4				N/A
156		ID_HEAD113				32			N/A			SMDU7 Battery 4 Capacity				N/A		SMDU7 Batteriekapazität 4				N/A
157		ID_HEAD114				32			N/A			SMDU8 Battery 1 Current					N/A		SMDU8 Batteriestrom 1					N/A
158		ID_HEAD115				32			N/A			SMDU8 Battery 1 Voltage					N/A		SMDU8 Batteriespannung 1				N/A
159		ID_HEAD116				32			N/A			SMDU8 Battery 1 Capacity				N/A		SMDU8 Batteriekapazität 1				N/A
160		ID_HEAD117				32			N/A			SMDU8 Battery 2 Current					N/A		SMDU8 Batteriestrom 2					N/A
161		ID_HEAD118				32			N/A			SMDU8 Battery 2 Voltage					N/A		SMDU8 Batteriespannung 2				N/A
162		ID_HEAD119				32			N/A			SMDU8 Battery 2 Capacity				N/A		SMDU8 Batteriekapazität 2				N/A
163		ID_HEAD120				32			N/A			SMDU8 Battery 3 Current					N/A		SMDU8 Batteriestrom 3					N/A
164		ID_HEAD121				32			N/A			SMDU8 Battery 3 Voltage					N/A		SMDU8 Batteriespannung 3				N/A
165		ID_HEAD122				32			N/A			SMDU8 Battery 3 Capacity				N/A		SMDU8 Batteriekapazität 3				N/A
166		ID_HEAD123				32			N/A			SMDU8 Battery 4 Current					N/A		SMDU8 Batteriestrom 4					N/A
167		ID_HEAD124				32			N/A			SMDU8 Battery 4 Voltage					N/A		SMDU8 Batteriespannung 4				N/A
168		ID_HEAD125				32			N/A			SMDU8 Battery 4 Capacity				N/A		SMDU8 Batteriekapazität 4				N/A
169		ID_HEAD126				32			N/A			EIB1 Battery 3 Current					N/A		EIB1 Batteriestrom 3					N/A
170		ID_HEAD127				32			N/A			EIB1 Battery 3 Voltage					N/A		EIB1 Batteriespannung 3					N/A
171		ID_HEAD128				32			N/A			EIB1 Battery 3 Capacity					N/A		EIB1 Batteriekapazität 3				N/A
172		ID_HEAD129				32			N/A			EIB2 Battery 3 Current					N/A		EIB2 Batteriestrom 3					N/A
173		ID_HEAD130				32			N/A			EIB2 Battery 3 Voltage					N/A		EIB2 Batteriespannung 3					N/A
174		ID_HEAD131				32			N/A			EIB2 Battery 3 Capacity					N/A		EIB2 Batteriekapazität 3				N/A
175		ID_HEAD132				32			N/A			EIB3 Battery 3 Current					N/A		EIB3 Batteriestrom 3					N/A
176		ID_HEAD133				32			N/A			EIB3 Battery 3 Voltage					N/A		EIB3 Batteriespannung 3					N/A
177		ID_HEAD134				32			N/A			EIB3 Battery 3 Capacity					N/A		EIB3 Batteriekapazität 3				N/A
178		ID_HEAD135				32			N/A			EIB4 Battery 3 Current					N/A		EIB4 Batteriestrom 3					N/A
179		ID_HEAD136				32			N/A			EIB4 Battery 3 Voltage					N/A		EIB4 Batteriespannung 3					N/A
180		ID_HEAD137				32			N/A			EIB4 Battery 3 Capacity					N/A		EIB4 Batteriekapazität 3				N/A
181		ID_HEAD138				32			N/A			EIB1 Block 1 Voltage					N/A		EIB1 Blockspannung 1					N/A
182		ID_HEAD139				32			N/A			EIB1 Block 2 Voltage					N/A		EIB1 Blockspannung 2					N/A
183		ID_HEAD140				32			N/A			EIB1 Block 3 Voltage					N/A		EIB1 Blockspannung 3					N/A
184		ID_HEAD141				32			N/A			EIB1 Block 4 Voltage					N/A		EIB1 Blockspannung 4					N/A
185		ID_HEAD142				32			N/A			EIB1 Block 5 Voltage					N/A		EIB1 Blockspannung 5					N/A
186		ID_HEAD143				32			N/A			EIB1 Block 6 Voltage					N/A		EIB1 Blockspannung 6					N/A
187		ID_HEAD144				32			N/A			EIB1 Block 7 Voltage					N/A		EIB1 Blockspannung 7					N/A
188		ID_HEAD145				32			N/A			EIB1 Block 8 Voltage					N/A		EIB1 Blockspannung 8					N/A
189		ID_HEAD146				32			N/A			EIB2 Block 1 Voltage					N/A		EIB2 Blockspannung 1					N/A
190		ID_HEAD147				32			N/A			EIB2 Block 2 Voltage					N/A		EIB2 Blockspannung 2					N/A
191		ID_HEAD148				32			N/A			EIB2 Block 3 Voltage					N/A		EIB2 Blockspannung 3					N/A
192		ID_HEAD149				32			N/A			EIB2 Block 4 Voltage					N/A		EIB2 Blockspannung 4					N/A
193		ID_HEAD150				32			N/A			EIB2 Block 5 Voltage					N/A		EIB2 Blockspannung 5					N/A
194		ID_HEAD151				32			N/A			EIB2 Block 6 Voltage					N/A		EIB2 Blockspannung 6					N/A
195		ID_HEAD152				32			N/A			EIB2 Block 7 Voltage					N/A		EIB2 Blockspannung 7					N/A
196		ID_HEAD153				32			N/A			EIB2 Block 8 Voltage					N/A		EIB2 Blockspannung 8					N/A
197		ID_HEAD154				32			N/A			EIB3 Block 1 Voltage					N/A		EIB3 Blockspannung 1					N/A
198		ID_HEAD155				32			N/A			EIB3 Block 2 Voltage					N/A		EIB3 Blockspannung 2					N/A
199		ID_HEAD156				32			N/A			EIB3 Block 3 Voltage					N/A		EIB3 Blockspannung 3					N/A
200		ID_HEAD157				32			N/A			EIB3 Block 4 Voltage					N/A		EIB3 Blockspannung 4					N/A
201		ID_HEAD158				32			N/A			EIB3 Block 5 Voltage					N/A		EIB3 Blockspannung 5					N/A
202		ID_HEAD159				32			N/A			EIB3 Block 6 Voltage					N/A		EIB3 Blockspannung 6					N/A
203		ID_HEAD160				32			N/A			EIB3 Block 7 Voltage					N/A		EIB3 Blockspannung 7					N/A
204		ID_HEAD161				32			N/A			EIB3 Block 8 Voltage					N/A		EIB3 Blockspannung 8					N/A
205		ID_HEAD162				32			N/A			EIB4 Block 1 Voltage					N/A		EIB4 Blockspannung 1					N/A
206		ID_HEAD163				32			N/A			EIB4 Block 2 Voltage					N/A		EIB4 Blockspannung 2					N/A
207		ID_HEAD164				32			N/A			EIB4 Block 3 Voltage					N/A		EIB4 Blockspannung 3					N/A
208		ID_HEAD165				32			N/A			EIB4 Block 4 Voltage					N/A		EIB4 Blockspannung 4					N/A
209		ID_HEAD166				32			N/A			EIB4 Block 5 Voltage					N/A		EIB4 Blockspannung 5					N/A
210		ID_HEAD167				32			N/A			EIB4 Block 6 Voltage					N/A		EIB4 Blockspannung 6					N/A
211		ID_HEAD168				32			N/A			EIB4 Block 7 Voltage					N/A		EIB4 Blockspannung 7					N/A
212		ID_HEAD169				32			N/A			EIB4 Block 8 Voltage					N/A		EIB4 Blockspannung 8					N/A
213		ID_HEAD170				32			N/A			Temperature 1						N/A		Temperatur 1						N/A
214		ID_HEAD171				32			N/A			Temperature 2						N/A		Temperatur 2						N/A
215		ID_HEAD172				32			N/A			Temperature 3						N/A		Temperatur 3						N/A
216		ID_HEAD173				32			N/A			Temperature 4						N/A		Temperatur 4						N/A
217		ID_HEAD174				32			N/A			Temperature 5						N/A		Temperatur 5						N/A
218		ID_HEAD175				32			N/A			Temperature 6						N/A		Temperatur 6						N/A
219		ID_HEAD176				32			N/A			Temperature 7						N/A		Temperatur 7						N/A
220		ID_HEAD177				32			N/A			Battery 1 Current					N/A		Batteriestrom 1						N/A
221		ID_HEAD178				32			N/A			Battery 1 Voltage					N/A		Batteriespannung 1					N/A
222		ID_HEAD179				32			N/A			Battery 1 Capacity					N/A		Batteriekapazität 1					N/A
223		ID_HEAD180				32			N/A			LargeDUBattery 1 Current				N/A		Gr.DU Batteriestrom 1					N/A
224		ID_HEAD181				32			N/A			LargeDUBattery 1 Voltage				N/A		Gr.DU Batteriespannung 1				N/A
225		ID_HEAD182				32			N/A			LargeDUBattery 1 Capacity				N/A		Gr.DU Batteriekapazität 1				N/A
226		ID_HEAD183				32			N/A			LargeDUBattery 2 Current				N/A		Gr.DU Batteriestrom 2					N/A
227		ID_HEAD184				32			N/A			LargeDUBattery 2 Voltage				N/A		Gr.DU Batteriespannung 2				N/A
228		ID_HEAD185				32			N/A			LargeDUBattery 2 Capacity				N/A		Gr.DU Batteriekapazität 2				N/A
229		ID_HEAD186				32			N/A			LargeDUBattery 3 Current				N/A		Gr.DU Batteriestrom 3					N/A
230		ID_HEAD187				32			N/A			LargeDUBattery 3 Voltage				N/A		Gr.DU Batteriespannung 3				N/A
231		ID_HEAD188				32			N/A			LargeDUBattery 3 Capacity				N/A		Gr.DU Batteriekapazität 3				N/A
232		ID_HEAD189				32			N/A			LargeDUBattery 4 Current				N/A		Gr.DU Batteriestrom 4					N/A
233		ID_HEAD190				32			N/A			LargeDUBattery 4 Voltage				N/A		Gr.DU Batteriespannung 4				N/A
234		ID_HEAD191				32			N/A			LargeDUBattery 4 Capacity				N/A		Gr.DU Batteriekapazität 4				N/A
235		ID_HEAD192				32			N/A			LargeDUBattery 5 Current				N/A		Gr.DU Batteriestrom 5					N/A
236		ID_HEAD193				32			N/A			LargeDUBattery 5 Voltage				N/A		Gr.DU Batteriespannung 5				N/A
237		ID_HEAD194				32			N/A			LargeDUBattery 5 Capacity				N/A		Gr.DU Batteriekapazität 5				N/A
238		ID_HEAD195				32			N/A			LargeDUBattery 6 Current				N/A		Gr.DU Batteriestrom 6					N/A
239		ID_HEAD196				32			N/A			LargeDUBattery 6 Voltage				N/A		Gr.DU Batteriespannung 6				N/A
240		ID_HEAD197				32			N/A			LargeDUBattery 6 Capacity				N/A		Gr.DU Batteriekapazität 6				N/A
241		ID_HEAD198				32			N/A			LargeDUBattery 7 Current				N/A		Gr.DU Batteriestrom 7					N/A
242		ID_HEAD199				32			N/A			LargeDUBattery 7 Voltage				N/A		Gr.DU Batteriespannung 7				N/A
243		ID_HEAD200				32			N/A			LargeDUBattery 7 Capacity				N/A		Gr.DU Batteriekapazität 7				N/A
244		ID_HEAD201				32			N/A			LargeDUBattery 8 Current				N/A		Gr.DU Batteriestrom 8					N/A
245		ID_HEAD202				32			N/A			LargeDUBattery 8 Voltage				N/A		Gr.DU Batteriespannung 8				N/A
246		ID_HEAD203				32			N/A			LargeDUBattery 8 Capacity				N/A		Gr.DU Batteriekapazität 8				N/A
247		ID_HEAD204				32			N/A			LargeDUBattery 9 Current				N/A		Gr.DU Batteriestrom 9					N/A
248		ID_HEAD205				32			N/A			LargeDUBattery 9 Voltage				N/A		Gr.DU Batteriespannung 9				N/A
249		ID_HEAD206				32			N/A			LargeDUBattery 9 Capacity				N/A		Gr.DU Batteriekapazität 9				N/A
250		ID_HEAD207				32			N/A			LargeDUBattery 10 Current				N/A		Gr.DU Batteriestrom 10					N/A
251		ID_HEAD208				32			N/A			LargeDUBattery 10 Voltage				N/A		Gr.DU Batteriespannung 10				N/A
252		ID_HEAD209				32			N/A			LargeDUBattery 10 Capacity				N/A		Gr.DU Batteriekapazität 10				N/A
253		ID_HEAD210				32			N/A			LargeDUBattery 11 Current				N/A		Gr.DU Batteriestrom 11					N/A
254		ID_HEAD211				32			N/A			LargeDUBattery 11 Voltage				N/A		Gr.DU Batteriespannung 11				N/A
255		ID_HEAD212				32			N/A			LargeDUBattery 11 Capacity				N/A		Gr.DU Batteriekapazität 11				N/A
256		ID_HEAD213				32			N/A			LargeDUBattery 12 Current				N/A		Gr.DU Batteriestrom 12					N/A
257		ID_HEAD214				32			N/A			LargeDUBattery 12 Voltage				N/A		Gr.DU Batteriespannung 12				N/A
258		ID_HEAD215				32			N/A			LargeDUBattery 12 Capacity				N/A		Gr.DU Batteriekapazität 12				N/A
259		ID_HEAD216				32			N/A			LargeDUBattery 13 Current				N/A		Gr.DU Batteriestrom 13					N/A
260		ID_HEAD217				32			N/A			LargeDUBattery 13 Voltage				N/A		Gr.DU Batteriespannung 13				N/A
261		ID_HEAD218				32			N/A			LargeDUBattery 13 Capacity				N/A		Gr.DU Batteriekapazität 13				N/A
262		ID_HEAD219				32			N/A			LargeDUBattery 14 Current				N/A		Gr.DU Batteriestrom 14					N/A
263		ID_HEAD220				32			N/A			LargeDUBattery 14 Voltage				N/A		Gr.DU Batteriespannung 14				N/A
264		ID_HEAD221				32			N/A			LargeDUBattery 14 Capacity				N/A		Gr.DU Batteriekapazität 14				N/A
265		ID_HEAD222				32			N/A			LargeDUBattery 15 Current				N/A		Gr.DU Batteriestrom 15					N/A
266		ID_HEAD223				32			N/A			LargeDUBattery 15 Voltage				N/A		Gr.DU Batteriespannung 15				N/A
267		ID_HEAD224				32			N/A			LargeDUBattery 15 Capacity				N/A		Gr.DU Batteriekapazität 15				N/A
268		ID_HEAD225				32			N/A			LargeDUBattery 16 Current				N/A		Gr.DU Batteriestrom 16					N/A
269		ID_HEAD226				32			N/A			LargeDUBattery 16 Voltage				N/A		Gr.DU Batteriespannung 16				N/A
270		ID_HEAD227				32			N/A			LargeDUBattery 16 Capacity				N/A		Gr.DU Batteriekapazität 16				N/A
271		ID_HEAD228				32			N/A			LargeDUBattery 17 Current				N/A		Gr.DU Batteriestrom 17					N/A
272		ID_HEAD229				32			N/A			LargeDUBattery 17 Voltage				N/A		Gr.DU Batteriespannung 17				N/A
273		ID_HEAD230				32			N/A			LargeDUBattery 17 Capacity				N/A		Gr.DU Batteriekapazität 17				N/A
274		ID_HEAD231				32			N/A			LargeDUBattery 18 Current				N/A		Gr.DU Batteriestrom 18					N/A
275		ID_HEAD232				32			N/A			LargeDUBattery 18 Voltage				N/A		Gr.DU Batteriespannung 18				N/A
276		ID_HEAD233				32			N/A			LargeDUBattery 18 Capacity				N/A		Gr.DU Batteriekapazität 18				N/A
277		ID_HEAD234				32			N/A			LargeDUBattery 19 Current				N/A		Gr.DU Batteriestrom 19					N/A
278		ID_HEAD235				32			N/A			LargeDUBattery 19 Voltage				N/A		Gr.DU Batteriespannung 19				N/A
279		ID_HEAD236				32			N/A			LargeDUBattery 19 Capacity				N/A		Gr.DU Batteriekapazität 19				N/A
280		ID_HEAD237				32			N/A			LargeDUBattery 20 Current				N/A		Gr.DU Batteriestrom 20					N/A
281		ID_HEAD238				32			N/A			LargeDUBattery 20 Voltage				N/A		Gr.DU Batteriespannung 20				N/A
282		ID_HEAD239				32			N/A			LargeDUBattery 20 Capacity				N/A		Gr.DU Batteriekapazität 20				N/A
283		ID_HEAD240				32			N/A			Temperature 8						N/A		Temperatur 8						N/A
284		ID_HEAD241				32			N/A			Temperature 9						N/A		Temperatur 9						N/A
285		ID_HEAD242				32			N/A			Temperature 10						N/A		Temperatur 10						N/A
286       	ID_HEAD243			32			N/A			SMBattery1 Current							N/A		SM Batterie1 Strom				N/A
287       	ID_HEAD244			32			N/A			SMBattery1 Voltage							N/A		SM Batterie1 Spannung				N/A
288       	ID_HEAD245			32			N/A			SMBattery1 Capacity							N/A		SM Batterie1 Kapazität			N/A
289       	ID_HEAD246			32			N/A			SMBattery2 Current							N/A		SM Batterie2 Strom				N/A
290       	ID_HEAD247			32			N/A			SMBattery2 Voltage							N/A		SM Batterie2 Spannung			N/A
291       	ID_HEAD248			32			N/A			SMBattery2 Capacity							N/A		SM Batterie2 Kapazität			N/A
292       	ID_HEAD249			32			N/A			SMBattery3 Current							N/A		SM Batterie3 Strom				N/A
293       	ID_HEAD250			32			N/A			SMBattery3 Voltage							N/A		SM Batterie3 Spannung				N/A
294       	ID_HEAD251			32			N/A			SMBattery3 Capacity							N/A		SM Batterie3 Kapazität			N/A
295       	ID_HEAD252			32			N/A			SMBattery4 Current							N/A		SM Batterie4 Strom			N/A
296       	ID_HEAD253			32			N/A			SMBattery4 Voltage							N/A		SM Batterie4 Spannung			N/A
297       	ID_HEAD254			32			N/A			SMBattery4 Capacity							N/A		SM Batterie4 Kapazität				N/A
298       	ID_HEAD255			32			N/A			SMBattery5 Current							N/A		SM Batterie5 Strom			N/A
299       	ID_HEAD256			32			N/A			SMBattery5 Voltage							N/A		SM Batterie5 Spannung				N/A
300       	ID_HEAD257			32			N/A			SMBattery5 Capacity							N/A		SM Batterie5 Kapazität			N/A
301       	ID_HEAD258			32			N/A			SMBattery6 Current							N/A		SM Batterie6 Strom			N/A
302       	ID_HEAD259			32			N/A			SMBattery6 Voltage							N/A		SM Batterie6 Spannung			N/A
303       	ID_HEAD260			32			N/A			SMBattery6 Capacity							N/A		SM Batterie6 Kapazität				N/A
304       	ID_HEAD261			32			N/A			SMBattery7 Current							N/A		SM Batterie7 Strom				N/A
305       	ID_HEAD262			32			N/A			SMBattery7 Voltage							N/A		SM Batterie7 Spannung			N/A
306       	ID_HEAD263			32			N/A			SMBattery7 Capacity							N/A		SM Batterie7 Kapazität			N/A
307       	ID_HEAD264			32			N/A			SMBattery8 Current							N/A		SM Batterie8 Strom				N/A
308       	ID_HEAD265			32			N/A			SMBattery8 Voltage							N/A		SM Batterie8 Spannung				N/A
309       	ID_HEAD266			32			N/A			SMBattery8 Capacity							N/A		SM Batterie8 Kapazität			N/A
310       	ID_HEAD267			32			N/A			SMBattery9 Current							N/A		SM Batterie9 Strom				N/A
311       	ID_HEAD268			32			N/A			SMBattery9 Voltage							N/A		SM Batterie9 Spannung				N/A
312       	ID_HEAD269			32			N/A			SMBattery9 Capacity							N/A		SM Batterie9 Kapazität				N/A
313       	ID_HEAD270			32			N/A			SMBattery10 Current							N/A		SM Batterie10 Strom				N/A
314       	ID_HEAD271			32			N/A			SMBattery10 Voltage							N/A		SM Batterie10 Spannung				N/A
315       	ID_HEAD272			32			N/A			SMBattery10 Capacity							N/A		SM Batterie10 Kapazität				N/A
316       	ID_HEAD273			32			N/A			SMBattery11 Current							N/A		SM Batterie11 Strom				N/A
317       	ID_HEAD274			32			N/A			SMBattery11 Voltage							N/A		SM Batterie11 Spannung				N/A
318       	ID_HEAD275			32			N/A			SMBattery11 Capacity							N/A		SM Batterie11 Kapazität				N/A
319       	ID_HEAD276			32			N/A			SMBattery12 Current							N/A		SM Batterie12 Strom			N/A
320       	ID_HEAD277			32			N/A			SMBattery12 Voltage							N/A		SM Batterie12 Spannung				N/A
321       	ID_HEAD278			32			N/A			SMBattery12 Capacity							N/A		SM Batterie12 Kapazität				N/A
322       	ID_HEAD279			32			N/A			SMBattery13 Current							N/A		SM Batterie13 Strom				N/A
323       	ID_HEAD280			32			N/A			SMBattery13 Voltage							N/A		SM Batterie13 Spannung				N/A
324       	ID_HEAD281			32			N/A			SMBattery13 Capacity							N/A		SM Batterie13 Kapazität				N/A
325       	ID_HEAD282			32			N/A			SMBattery14 Current							N/A		SM Batterie14 Strom				N/A
326       	ID_HEAD283			32			N/A			SMBattery14 Voltage							N/A		SM Batterie14 Spannung				N/A
327       	ID_HEAD284			32			N/A			SMBattery14 Capacity							N/A		SM Batterie14 Kapazität				N/A
328       	ID_HEAD285			32			N/A			SMBattery15 Current							N/A		SM Batterie15 Strom				N/A
329       	ID_HEAD286			32			N/A			SMBattery15 Voltage							N/A		SM Batterie15 Spannung				N/A
330       	ID_HEAD287			32			N/A			SMBattery15 Capacity							N/A		SM Batterie15 Kapazität				N/A
331       	ID_HEAD288			32			N/A			SMBattery16 Current							N/A		SM Batterie16 Strom				N/A
332       	ID_HEAD289			32			N/A			SMBattery16 Voltage							N/A		SM Batterie16 Spannung				N/A
333       	ID_HEAD290			32			N/A			SMBattery16 Capacity							N/A		SM Batterie16 Kapazität				N/A
334       	ID_HEAD291			32			N/A			SMBattery17 Current							N/A		SM Batterie17 Strom				N/A
335       	ID_HEAD292			32			N/A			SMBattery17 Voltage							N/A		SM Batterie17 Spannung				N/A
336       	ID_HEAD293			32			N/A			SMBattery17 Capacity							N/A		SM Batterie17 Kapazität				N/A
337       	ID_HEAD294			32			N/A			SMBattery18 Current							N/A		SM Batterie18 Strom				N/A
338       	ID_HEAD295			32			N/A			SMBattery18 Voltage							N/A		SM Batterie18 Spannung				N/A
339       	ID_HEAD296			32			N/A			SMBattery18 Capacity							N/A		SM Batterie18 Kapazität				N/A
340       	ID_HEAD297			32			N/A			SMBattery19 Current							N/A		SM Batterie19 Strom				N/A
341       	ID_HEAD298			32			N/A			SMBattery19 Voltage							N/A		SM Batterie19 Spannung				N/A
342       	ID_HEAD299			32			N/A			SMBattery19 Capacity							N/A		SM Batterie19 Kapazität				N/A
343       	ID_HEAD300			32			N/A			SMBattery20 Current							N/A		SM Batterie20 Strom				N/A
344       	ID_HEAD301			32			N/A			SMBattery20 Voltage							N/A		SM Batterie20 Spannung				N/A
345       	ID_HEAD302			32			N/A			SMBattery20 Capacity							N/A		SM Batterie20 Kapazität				N/A
346       	ID_HEAD303			32			N/A			SMDU1Battery5 Current							N/A		SMDU1 Batterie5 Strom				N/A
347       	ID_HEAD304			32			N/A			SMDU1Battery5 Voltage							N/A		SMDU1 Batterie5 Spannung				N/A
348       	ID_HEAD305			32			N/A			SMDU1Battery5 Capacity							N/A		SMDU1 Batterie5 Kapazität				N/A
349       	ID_HEAD306			32			N/A			SMDU2Battery5 Current							N/A		SMDU2 Batterie5 Strom			N/A
350       	ID_HEAD307			32			N/A			SMDU2Battery5 Voltage							N/A		SMDU2 Batterie5 Spannung				N/A
351       	ID_HEAD308			32			N/A			SMDU2Battery5 Capacity							N/A		SMDU2 Batterie5 Kapazität					N/A
352       	ID_HEAD309			32			N/A			SMDU3Battery5 Current							N/A		SMDU3 Batterie5 Strom				N/A
353       	ID_HEAD310			32			N/A			SMDU3Battery5 Voltage							N/A		SMDU3 Batterie5 Spannung				N/A
354       	ID_HEAD311			32			N/A			SMDU3Battery5 Capacity							N/A		SMDU3 Batterie5 Kapazität					N/A
355       	ID_HEAD312			32			N/A			SMDU4Battery5 Current							N/A		SMDU4 Batterie5 Strom				N/A
356       	ID_HEAD313			32			N/A			SMDU4Battery5 Voltage							N/A		SMDU4 Batterie5 Spannung				N/A
357       	ID_HEAD314			32			N/A			SMDU4Battery5 Capacity							N/A		SMDU4 Batterie5 Kapazität					N/A
358       	ID_HEAD315			32			N/A			SMDU5Battery5 Current							N/A		SMDU5 Batterie5 Strom				N/A
359       	ID_HEAD316			32			N/A			SMDU5Battery5 Voltage							N/A		SMDU5 Batterie5 Spannung				N/A
360       	ID_HEAD317			32			N/A			SMDU5Battery5 Capacity							N/A		SMDU5 Batterie5 Kapazität					N/A
361       	ID_HEAD318			32			N/A			SMDU6Battery5 Current							N/A		SMDU6 Batterie5 Strom				N/A
362       	ID_HEAD319			32			N/A			SMDU6Battery5 Voltage							N/A		SMDU6 Batterie5 Spannung				N/A
363       	ID_HEAD320			32			N/A			SMDU6Battery5 Capacity							N/A		SMDU6 Batterie5 Kapazität					N/A
364       	ID_HEAD321			32			N/A			SMDU7Battery5 Current							N/A		SMDU7 Batterie5 Strom				N/A
365       	ID_HEAD322			32			N/A			SMDU7Battery5 Voltage							N/A		SMDU7 Batterie5 Spannung				N/A
366       	ID_HEAD323			32			N/A			SMDU7Battery5 Capacity							N/A		SMDU7 Batterie5 Kapazität					N/A
367       	ID_HEAD324			32			N/A			SMDU8Battery5 Current							N/A		SMDU8 Batterie5 Strom				N/A
368       	ID_HEAD325			32			N/A			SMDU8Battery5 Voltage							N/A		SMDU8 Batterie5 Spannung				N/A
369       	ID_HEAD326			32			N/A			SMDU8Battery5 Capacity							N/A		SMDU8 Batterie5 Kapazität					N/A
370       	ID_HEAD327			32			N/A			SMBRCBattery1 Current							N/A		SMBRC Batterie1 Strom				N/A
371       	ID_HEAD328			32			N/A			SMBRCBattery1 Voltage							N/A		SMBRC Batterie1 Spannung				N/A
372       	ID_HEAD329			32			N/A			SMBRCBattery1 Capacity							N/A		SMBRC Batterie1 Kapazität				N/A
373       	ID_HEAD330			32			N/A			SMBRCBattery2 Current							N/A		SMBRC Batterie2 Strom				N/A
374       	ID_HEAD331			32			N/A			SMBRCBattery2 Voltage							N/A		SMBRC Batterie2 Spannung				N/A
375       	ID_HEAD332			32			N/A			SMBRCBattery2 Capacity							N/A		SMBRC Batterie2 Kapazität				N/A
376       	ID_HEAD333			32			N/A			SMBRCBattery3 Current							N/A		SMBRC Batterie3 Strom				N/A
377       	ID_HEAD334			32			N/A			SMBRCBattery3 Voltage							N/A		SMBRC Batterie3 Spannung				N/A
378       	ID_HEAD335			32			N/A			SMBRCBattery3 Capacity							N/A		SMBRC Batterie3 Kapazität				N/A
379       	ID_HEAD336			32			N/A			SMBRCBattery4 Current							N/A		SMBRC Batterie4 Strom				N/A
380       	ID_HEAD337			32			N/A			SMBRCBattery4 Voltage							N/A		SMBRC Batterie4 Spannung				N/A
381       	ID_HEAD338			32			N/A			SMBRCBattery4 Capacity							N/A		SMBRC Batterie4 Kapazität				N/A
382       	ID_HEAD339			32			N/A			SMBRCBattery5 Current							N/A		SMBRC Batterie5 Strom				N/A
383       	ID_HEAD340			32			N/A			SMBRCBattery5 Voltage							N/A		SMBRC Batterie5 Spannung				N/A
384       	ID_HEAD341			32			N/A			SMBRCBattery5 Capacity							N/A		SMBRC Batterie5 Kapazität				N/A
385       	ID_HEAD342			32			N/A			SMBRCBattery6 Current							N/A		SMBRC Batterie6 Strom				N/A
386       	ID_HEAD343			32			N/A			SMBRCBattery6 Voltage							N/A		SMBRC Batterie6 Spannung				N/A
387       	ID_HEAD344			32			N/A			SMBRCBattery6 Capacity							N/A		SMBRC Batterie6 Kapazität				N/A
388       	ID_HEAD345			32			N/A			SMBRCBattery7 Current							N/A		SMBRC Batterie7 Strom				N/A
389       	ID_HEAD346			32			N/A			SMBRCBattery7 Voltage							N/A		SMBRC Batterie7 Spannung				N/A
390       	ID_HEAD347			32			N/A			SMBRCBattery7 Capacity							N/A		SMBRC Batterie7 Kapazität				N/A
391       	ID_HEAD348			32			N/A			SMBRCBattery8 Current							N/A		SMBRC Batterie8 Strom				N/A
392       	ID_HEAD349			32			N/A			SMBRCBattery8 Voltage							N/A		SMBRC Batterie8 Spannung				N/A
393       	ID_HEAD350			32			N/A			SMBRCBattery8 Capacity							N/A		SMBRC Batterie8 Kapazität				N/A
394       	ID_HEAD351			32			N/A			SMBRCBattery9 Current							N/A		SMBRC Batterie9 Strom				N/A
395       	ID_HEAD352			32			N/A			SMBRCBattery9 Voltage							N/A		SMBRC Batterie9 Spannung				N/A
396       	ID_HEAD353			32			N/A			SMBRCBattery9 Capacity							N/A		SMBRC Batterie9 Kapazität				N/A
397       	ID_HEAD354			32			N/A			SMBRCBattery10 Current							N/A		SMBRC Batterie10 Strom				N/A
398       	ID_HEAD355			32			N/A			SMBRCBattery10 Voltage							N/A		SMBRC Batterie10 Spannung				N/A
399       	ID_HEAD356			32			N/A			SMBRCBattery10 Capacity							N/A		SMBRC Batterie10 Kapazität				N/A
400       	ID_HEAD357			32			N/A			SMBRCBattery11 Current							N/A		SMBRC Batterie11 Strom				N/A
401       	ID_HEAD358			32			N/A			SMBRCBattery11 Voltage							N/A		SMBRC Batterie11 Spannung				N/A
402       	ID_HEAD359			32			N/A			SMBRCBattery11 Capacity							N/A		SMBRC Batterie11 Kapazität				N/A
403       	ID_HEAD360			32			N/A			SMBRCBattery12 Current							N/A		SMBRC Batterie12 Strom			N/A
404       	ID_HEAD361			32			N/A			SMBRCBattery12 Voltage							N/A		SMBRC Batterie12 Spannung			N/A
405       	ID_HEAD362			32			N/A			SMBRCBattery12 Capacity							N/A		SMBRC Batterie12 Kapazität				N/A
406       	ID_HEAD363			32			N/A			SMBRCBattery13 Current							N/A		SMBRC Batterie13 Strom				N/A
407       	ID_HEAD364			32			N/A			SMBRCBattery13 Voltage							N/A		SMBRC Batterie13 Spannung				N/A
408       	ID_HEAD365			32			N/A			SMBRCBattery13 Capacity							N/A		SMBRC Batterie13 Kapazität			N/A
409       	ID_HEAD366			32			N/A			SMBRCBattery14 Current							N/A		SMBRC Batterie14 Strom			N/A
410       	ID_HEAD367			32			N/A			SMBRCBattery14 Voltage							N/A		SMBRC Batterie14 Spannung				N/A
411       	ID_HEAD368			32			N/A			SMBRCBattery14 Capacity							N/A		SMBRC Batterie14 Kapazität			N/A
412       	ID_HEAD369			32			N/A			SMBRCBattery15 Current							N/A		SMBRC Batterie15 Strom			N/A
413       	ID_HEAD370			32			N/A			SMBRCBattery15 Voltage							N/A		SMBRC Batterie15 Spannung				N/A
414       	ID_HEAD371			32			N/A			SMBRCBattery15 Capacity							N/A		SMBRC Batterie15 Kapazität				N/A
415       	ID_HEAD372			32			N/A			SMBRCBattery16 Current							N/A		SMBRC Batterie16 Strom				N/A
416       	ID_HEAD373			32			N/A			SMBRCBattery16 Voltage							N/A		SMBRC Batterie16 Spannung				N/A
417       	ID_HEAD374			32			N/A			SMBRCBattery16 Capacity							N/A		SMBRC Batterie16 Kapazität				N/A
418       	ID_HEAD375			32			N/A			SMBRCBattery17 Current							N/A		SMBRC Batterie17 Strom				N/A
419       	ID_HEAD376			32			N/A			SMBRCBattery17 Voltage							N/A		SMBRC Batterie17 Spannung				N/A
420       	ID_HEAD377			32			N/A			SMBRCBattery17 Capacity							N/A		SMBRC Batterie17 Kapazität				N/A
421       	ID_HEAD378			32			N/A			SMBRCBattery18 Current							N/A		SMBRC Batterie18 Strom				N/A
422       	ID_HEAD379			32			N/A			SMBRCBattery18 Voltage							N/A		SMBRC Batterie18 Spannung				N/A
423       	ID_HEAD380			32			N/A			SMBRCBattery18 Capacity							N/A		SMBRC Batterie18 Kapazität			N/A
424       	ID_HEAD381			32			N/A			SMBRCBattery19 Current							N/A		SMBRC Batterie19 Strom				N/A
425       	ID_HEAD382			32			N/A			SMBRCBattery19 Voltage							N/A		SMBRC Batterie19 Spannung			N/A
426       	ID_HEAD383			32			N/A			SMBRCBattery19 Capacity							N/A		SMBRC Batterie19 Kapazität				N/A
427       	ID_HEAD384			32			N/A			SMBRCBattery20 Current							N/A		SMBRC Batterie20 Strom				N/A
428       	ID_HEAD385			32			N/A			SMBRCBattery20 Voltage							N/A		SMBRC Batterie20 Spannung			N/A
429       	ID_HEAD386			32			N/A			SMBRCBattery20 Capacity							N/A		SMBRC Batterie20 Kapazität				N/A
430       	ID_HEAD387			32			N/A			SMBAT/BRC1 BLOCK1  Voltage						N/A		SMBAT/BRC1 Block 1 Spannung 				N/A
431       	ID_HEAD388			32			N/A			SMBAT/BRC1 BLOCK2  Voltage						N/A		SMBAT/BRC1 Block 2 Spannung 				N/A
432       	ID_HEAD389			32			N/A			SMBAT/BRC1 BLOCK3  Voltage						N/A		SMBAT/BRC1 Block 3 Spannung 				N/A
433       	ID_HEAD390			32			N/A			SMBAT/BRC1 BLOCK4  Voltage						N/A		SMBAT/BRC1 Block 4 Spannung 				N/A
434       	ID_HEAD391			32			N/A			SMBAT/BRC1 BLOCK5  Voltage						N/A		SMBAT/BRC1 Block 5 Spannung				N/A
435       	ID_HEAD392			32			N/A			SMBAT/BRC1 BLOCK6  Voltage						N/A		SMBAT/BRC1 Block 6 Spannung 				N/A
436       	ID_HEAD393			32			N/A			SMBAT/BRC1 BLOCK7  Voltage						N/A		SMBAT/BRC1 Block 7 Spannung 				N/A
437       	ID_HEAD394			32			N/A			SMBAT/BRC1 BLOCK8  Voltage						N/A		SMBAT/BRC1 Block 8 Spannung 				N/A
438       	ID_HEAD395			32			N/A			SMBAT/BRC1 BLOCK9  Voltage						N/A		SMBAT/BRC1 Block 9 Spannung 				N/A
439       	ID_HEAD396			32			N/A			SMBAT/BRC1 BLOCK10 Voltage						N/A		SMBAT/BRC1 Block 10 Spannung				N/A
440       	ID_HEAD397			32			N/A			SMBAT/BRC1 BLOCK11 Voltage						N/A		SMBAT/BRC1 Block 11 Spannung				N/A
441       	ID_HEAD398			32			N/A			SMBAT/BRC1 BLOCK12 Voltage						N/A		SMBAT/BRC1 Block 12 Spannung				N/A
442       	ID_HEAD399			32			N/A			SMBAT/BRC1 BLOCK13 Voltage						N/A		SMBAT/BRC1 Block 13 Spannung				N/A
443       	ID_HEAD400			32			N/A			SMBAT/BRC1 BLOCK14 Voltage						N/A		SMBAT/BRC1 Block 14 Spannung				N/A
444       	ID_HEAD401			32			N/A			SMBAT/BRC1 BLOCK15 Voltage						N/A		SMBAT/BRC1 Block 15 Spannung				N/A
445       	ID_HEAD402			32			N/A			SMBAT/BRC1 BLOCK16 Voltage						N/A		SMBAT/BRC1 Block 16 Spannung			N/A
446       	ID_HEAD403			32			N/A			SMBAT/BRC1 BLOCK17 Voltage						N/A		SMBAT/BRC1 Block 17 Spannung				N/A
447       	ID_HEAD404			32			N/A			SMBAT/BRC1 BLOCK18 Voltage						N/A		SMBAT/BRC1 Block 18 Spannung				N/A
448       	ID_HEAD405			32			N/A			SMBAT/BRC1 BLOCK19 Voltage						N/A		SMBAT/BRC1 Block 19 Spannung			N/A
449       	ID_HEAD406			32			N/A			SMBAT/BRC1 BLOCK20 Voltage						N/A		SMBAT/BRC1 Block 20 Spannung				N/A
450       	ID_HEAD407			32			N/A			SMBAT/BRC1 BLOCK21 Voltage						N/A		SMBAT/BRC1 Block 21 Spannung			N/A
451       	ID_HEAD408			32			N/A			SMBAT/BRC1 BLOCK22 Voltage						N/A		SMBAT/BRC1 Block 22 Spannung				N/A
452       	ID_HEAD409			32			N/A			SMBAT/BRC1 BLOCK23 Voltage						N/A		SMBAT/BRC1 Block 23 Spannung				N/A
453       	ID_HEAD410			32			N/A			SMBAT/BRC1 BLOCK24 Voltage						N/A		SMBAT/BRC1 Block 24 Spannung				N/A
454       	ID_HEAD411			32			N/A			SMBAT/BRC2 BLOCK1  Voltage						N/A		SMBAT/BRC2 Block 1 Spannung				N/A
455       	ID_HEAD412			32			N/A			SMBAT/BRC2 BLOCK2  Voltage						N/A		SMBAT/BRC2 Block 2 Spannung 				N/A
456       	ID_HEAD413			32			N/A			SMBAT/BRC2 BLOCK3  Voltage						N/A		SMBAT/BRC2 Block 3 Spannung  				N/A
457       	ID_HEAD414			32			N/A			SMBAT/BRC2 BLOCK4  Voltage						N/A		SMBAT/BRC2 Block 4 Spannung 				N/A
458       	ID_HEAD415			32			N/A			SMBAT/BRC2 BLOCK5  Voltage						N/A		SMBAT/BRC2 Block 5 Spannung 				N/A
459       	ID_HEAD416			32			N/A			SMBAT/BRC2 BLOCK6  Voltage						N/A		SMBAT/BRC2 Block 6 Spannung 				N/A
460       	ID_HEAD417			32			N/A			SMBAT/BRC2 BLOCK7  Voltage						N/A		SMBAT/BRC2 Block 7 Spannung 				N/A
461       	ID_HEAD418			32			N/A			SMBAT/BRC2 BLOCK8  Voltage						N/A		SMBAT/BRC2 Block 8 Spannung 				N/A
462       	ID_HEAD419			32			N/A			SMBAT/BRC2 BLOCK9  Voltage						N/A		SMBAT/BRC2 Block 9 Spannung  				N/A
463       	ID_HEAD420			32			N/A			SMBAT/BRC2 BLOCK10 Voltage						N/A		SMBAT/BRC2 Block 10 Spannung 			N/A
464       	ID_HEAD421			32			N/A			SMBAT/BRC2 BLOCK11 Voltage						N/A		SMBAT/BRC2 Block 11 Spannung 				N/A
465       	ID_HEAD422			32			N/A			SMBAT/BRC2 BLOCK12 Voltage						N/A		SMBAT/BRC2 Block 12 Spannung 				N/A
466       	ID_HEAD423			32			N/A			SMBAT/BRC2 BLOCK13 Voltage						N/A	  SMBAT/BRC2 Block 13 Spannung 				N/A
467       	ID_HEAD424			32			N/A			SMBAT/BRC2 BLOCK14 Voltage						N/A		SMBAT/BRC2 Block 14 Spannung 				N/A
468       	ID_HEAD425			32			N/A			SMBAT/BRC2 BLOCK15 Voltage						N/A		SMBAT/BRC2 Block 15 Spannung 				N/A
469       	ID_HEAD426			32			N/A			SMBAT/BRC2 BLOCK16 Voltage						N/A		SMBAT/BRC2 Block 16 Spannung 				N/A
470       	ID_HEAD427			32			N/A			SMBAT/BRC2 BLOCK17 Voltage						N/A		SMBAT/BRC2 Block 17 Spannung 				N/A
471       	ID_HEAD428			32			N/A			SMBAT/BRC2 BLOCK18 Voltage						N/A		SMBAT/BRC2 Block 18 Spannung 				N/A
472       	ID_HEAD429			32			N/A			SMBAT/BRC2 BLOCK19 Voltage						N/A		SMBAT/BRC2 Block 19 Spannung 				N/A
473       	ID_HEAD430			32			N/A			SMBAT/BRC2 BLOCK20 Voltage						N/A		SMBAT/BRC2 Block 20 Spannung 			N/A
474       	ID_HEAD431			32			N/A			SMBAT/BRC2 BLOCK21 Voltage						N/A		SMBAT/BRC2 Block 21 Spannung 				N/A
475       	ID_HEAD432			32			N/A			SMBAT/BRC2 BLOCK22 Voltage						N/A		SMBAT/BRC2 Block 22 Spannung 				N/A
476       	ID_HEAD433			32			N/A			SMBAT/BRC2 BLOCK23 Voltage						N/A		SMBAT/BRC2 Block 23 Spannung 				N/A
477       	ID_HEAD434			32			N/A			SMBAT/BRC2 BLOCK24 Voltage						N/A		SMBAT/BRC2 Block 24 Spannung 				N/A
478       	ID_HEAD435			32			N/A			SMBAT/BRC3 BLOCK1  Voltage						N/A		SMBAT/BRC3 Block 1 Spannung  				N/A
479       	ID_HEAD436			32			N/A			SMBAT/BRC3 BLOCK2  Voltage						N/A		SMBAT/BRC3 Block 2 Spannung 				N/A
480       	ID_HEAD437			32			N/A			SMBAT/BRC3 BLOCK3  Voltage						N/A		SMBAT/BRC3 Block 3 Spannung 				N/A
481       	ID_HEAD438			32			N/A			SMBAT/BRC3 BLOCK4  Voltage						N/A		SMBAT/BRC3 Block 4 Spannung 				N/A
482       	ID_HEAD439			32			N/A			SMBAT/BRC3 BLOCK5  Voltage						N/A		SMBAT/BRC3 Block 5 Spannung				N/A
483       	ID_HEAD440			32			N/A			SMBAT/BRC3 BLOCK6  Voltage						N/A		SMBAT/BRC3 Block 6 Spannung				N/A
484       	ID_HEAD441			32			N/A			SMBAT/BRC3 BLOCK7  Voltage						N/A		SMBAT/BRC3 Block 7 Spannung				N/A
485       	ID_HEAD442			32			N/A			SMBAT/BRC3 BLOCK8  Voltage						N/A		SMBAT/BRC3 Block 8 Spannung 				N/A
486       	ID_HEAD443			32			N/A			SMBAT/BRC3 BLOCK9  Voltage						N/A		SMBAT/BRC3 Block 9 Spannung				N/A
487       	ID_HEAD444			32			N/A			SMBAT/BRC3 BLOCK10 Voltage						N/A		SMBAT/BRC3 Block 10 Spannung				N/A
488       	ID_HEAD445			32			N/A			SMBAT/BRC3 BLOCK11 Voltage						N/A		SMBAT/BRC3 Block 11 Spannung				N/A
489       	ID_HEAD446			32			N/A			SMBAT/BRC3 BLOCK12 Voltage						N/A		SMBAT/BRC3 Block 12 Spannung				N/A
490       	ID_HEAD447			32			N/A			SMBAT/BRC3 BLOCK13 Voltage						N/A		SMBAT/BRC3 Block 13 Spannung				N/A
491       	ID_HEAD448			32			N/A			SMBAT/BRC3 BLOCK14 Voltage						N/A		SMBAT/BRC3 Block 14 Spannung				N/A
492       	ID_HEAD449			32			N/A			SMBAT/BRC3 BLOCK15 Voltage						N/A		SMBAT/BRC3 Block 15 Spannung				N/A
493       	ID_HEAD450			32			N/A			SMBAT/BRC3 BLOCK16 Voltage						N/A		SMBAT/BRC3 Block 16 Spannung				N/A
494       	ID_HEAD451			32			N/A			SMBAT/BRC3 BLOCK17 Voltage						N/A		SMBAT/BRC3 Block 17 Spannung				N/A
495       	ID_HEAD452			32			N/A			SMBAT/BRC3 BLOCK18 Voltage						N/A		SMBAT/BRC3 Block 18 Spannung				N/A
496       	ID_HEAD453			32			N/A			SMBAT/BRC3 BLOCK19 Voltage						N/A		SMBAT/BRC3 Block 19 Spannung				N/A
497       	ID_HEAD454			32			N/A			SMBAT/BRC3 BLOCK20 Voltage						N/A	  SMBAT/BRC3 Block 20 Spannung				N/A
498       	ID_HEAD455			32			N/A			SMBAT/BRC3 BLOCK21 Voltage						N/A		SMBAT/BRC3 Block 21 Spannung				N/A
499       	ID_HEAD456			32			N/A			SMBAT/BRC3 BLOCK22 Voltage						N/A		SMBAT/BRC3 Block 22 Spannung				N/A
500       	ID_HEAD457			32			N/A			SMBAT/BRC3 BLOCK23 Voltage						N/A		SMBAT/BRC3 Block 23 Spannung				N/A
501       	ID_HEAD458			32			N/A			SMBAT/BRC3 BLOCK24 Voltage						N/A		SMBAT/BRC3 Block 24 Spannung				N/A
502       	ID_HEAD459			32			N/A			SMBAT/BRC4 BLOCK1  Voltage						N/A		SMBAT/BRC4 Block 1 Spannung 				N/A
503       	ID_HEAD460			32			N/A			SMBAT/BRC4 BLOCK2  Voltage						N/A		SMBAT/BRC4 Block 2 Spannung 				N/A
504       	ID_HEAD461			32			N/A			SMBAT/BRC4 BLOCK3  Voltage						N/A		SMBAT/BRC4 Block 3 Spannung  				N/A
505       	ID_HEAD462			32			N/A			SMBAT/BRC4 BLOCK4  Voltage						N/A		SMBAT/BRC4 Block 4 Spannung 				N/A
506       	ID_HEAD463			32			N/A			SMBAT/BRC4 BLOCK5  Voltage						N/A		SMBAT/BRC4 Block 5 Spannung  				N/A
507       	ID_HEAD464			32			N/A			SMBAT/BRC4 BLOCK6  Voltage						N/A		SMBAT/BRC4 Block 6 Spannung  				N/A
508       	ID_HEAD465			32			N/A			SMBAT/BRC4 BLOCK7  Voltage						N/A		SMBAT/BRC4 Block 7 Spannung  				N/A
509       	ID_HEAD466			32			N/A			SMBAT/BRC4 BLOCK8  Voltage						N/A		SMBAT/BRC4 Block 8 Spannung  				N/A
510       	ID_HEAD467			32			N/A			SMBAT/BRC4 BLOCK9  Voltage						N/A		SMBAT/BRC4 Block 9 Spannung  				N/A
511       	ID_HEAD468			32			N/A			SMBAT/BRC4 BLOCK10 Voltage						N/A		SMBAT/BRC4 Block 10 Spannung 				N/A
512       	ID_HEAD469			32			N/A			SMBAT/BRC4 BLOCK11 Voltage						N/A		SMBAT/BRC4 Block 11 Spannung 				N/A
513       	ID_HEAD470			32			N/A			SMBAT/BRC4 BLOCK12 Voltage						N/A		SMBAT/BRC4 Block 12 Spannung 				N/A
514       	ID_HEAD471			32			N/A			SMBAT/BRC4 BLOCK13 Voltage						N/A		SMBAT/BRC4 Block 13 Spannung 				N/A
515       	ID_HEAD472			32			N/A			SMBAT/BRC4 BLOCK14 Voltage						N/A		SMBAT/BRC4 Block 14 Spannung 				N/A
516       	ID_HEAD473			32			N/A			SMBAT/BRC4 BLOCK15 Voltage						N/A		SMBAT/BRC4 Block 15 Spannung 				N/A
517       	ID_HEAD474			32			N/A			SMBAT/BRC4 BLOCK16 Voltage						N/A		SMBAT/BRC4 Block 16 Spannung 				N/A
518       	ID_HEAD475			32			N/A			SMBAT/BRC4 BLOCK17 Voltage						N/A		SMBAT/BRC4 Block 17 Spannung 				N/A
519       	ID_HEAD476			32			N/A			SMBAT/BRC4 BLOCK18 Voltage						N/A		SMBAT/BRC4 Block 18 Spannung 				N/A
520       	ID_HEAD477			32			N/A			SMBAT/BRC4 BLOCK19 Voltage						N/A		SMBAT/BRC4 Block 19 Spannung 				N/A
521       	ID_HEAD478			32			N/A			SMBAT/BRC4 BLOCK20 Voltage						N/A		SMBAT/BRC4 Block 20 Spannung 				N/A
522       	ID_HEAD479			32			N/A			SMBAT/BRC4 BLOCK21 Voltage						N/A		SMBAT/BRC4 Block 21 Spannung 				N/A
523       	ID_HEAD480			32			N/A			SMBAT/BRC4 BLOCK22 Voltage						N/A		SMBAT/BRC4 Block 22 Spannung 				N/A
524       	ID_HEAD481			32			N/A			SMBAT/BRC4 BLOCK23 Voltage						N/A		SMBAT/BRC4 Block 23 Spannung 				N/A
525       	ID_HEAD482			32			N/A			SMBAT/BRC4 BLOCK24 Voltage						N/A		SMBAT/BRC4 Block 24 Spannung 				N/A
526       	ID_HEAD483			32			N/A			SMBAT/BRC5 BLOCK1  Voltage						N/A		SMBAT/BRC5 Block 1 Spannung  				N/A
527       	ID_HEAD484			32			N/A			SMBAT/BRC5 BLOCK2  Voltage						N/A		SMBAT/BRC5 Block 2 Spannung				N/A
528       	ID_HEAD485			32			N/A			SMBAT/BRC5 BLOCK3  Voltage						N/A		SMBAT/BRC5 Block 3 Spannung				N/A
529       	ID_HEAD486			32			N/A			SMBAT/BRC5 BLOCK4  Voltage						N/A		SMBAT/BRC5 Block 4 Spannung 				N/A
530       	ID_HEAD487			32			N/A			SMBAT/BRC5 BLOCK5  Voltage						N/A		SMBAT/BRC5 Block 5 Spannung				N/A
531       	ID_HEAD488			32			N/A			SMBAT/BRC5 BLOCK6  Voltage						N/A		SMBAT/BRC5 Block 6 Spannung 				N/A
532       	ID_HEAD489			32			N/A			SMBAT/BRC5 BLOCK7  Voltage						N/A		SMBAT/BRC5 Block 7 Spannung 				N/A
533       	ID_HEAD490			32			N/A			SMBAT/BRC5 BLOCK8  Voltage						N/A		SMBAT/BRC5 Block 8 Spannung 				N/A
534       	ID_HEAD491			32			N/A			SMBAT/BRC5 BLOCK9  Voltage						N/A		SMBAT/BRC5 Block 9 Spannung 				N/A
535       	ID_HEAD492			32			N/A			SMBAT/BRC5 BLOCK10 Voltage						N/A		SMBAT/BRC5 Block 10 Spannung				N/A
536       	ID_HEAD493			32			N/A			SMBAT/BRC5 BLOCK11 Voltage						N/A		SMBAT/BRC5 Block 11 Spannung				N/A
537       	ID_HEAD494			32			N/A			SMBAT/BRC5 BLOCK12 Voltage						N/A		SMBAT/BRC5 Block 12 Spannung				N/A
538       	ID_HEAD495			32			N/A			SMBAT/BRC5 BLOCK13 Voltage						N/A		SMBAT/BRC5 Block 13 Spannung				N/A
539       	ID_HEAD496			32			N/A			SMBAT/BRC5 BLOCK14 Voltage						N/A		SMBAT/BRC5 Block 14 Spannung				N/A
540       	ID_HEAD497			32			N/A			SMBAT/BRC5 BLOCK15 Voltage						N/A		SMBAT/BRC5 Block 15 Spannung				N/A
541       	ID_HEAD498			32			N/A			SMBAT/BRC5 BLOCK16 Voltage						N/A		SMBAT/BRC5 Block 16 Spannung				N/A
542       	ID_HEAD499			32			N/A			SMBAT/BRC5 BLOCK17 Voltage						N/A		SMBAT/BRC5 Block 17 Spannung				N/A
543       	ID_HEAD500			32			N/A			SMBAT/BRC5 BLOCK18 Voltage						N/A		SMBAT/BRC5 Block 18 Spannung				N/A
544       	ID_HEAD501			32			N/A			SMBAT/BRC5 BLOCK19 Voltage						N/A		SMBAT/BRC5 Block 19 Spannung				N/A
545       	ID_HEAD502			32			N/A			SMBAT/BRC5 BLOCK20 Voltage						N/A		SMBAT/BRC5 Block 20 Spannung				N/A
546       	ID_HEAD503			32			N/A			SMBAT/BRC5 BLOCK21 Voltage						N/A		SMBAT/BRC5 Block 21 Spannung				N/A
547       	ID_HEAD504			32			N/A			SMBAT/BRC5 BLOCK22 Voltage						N/A		SMBAT/BRC5 Block 22 Spannung				N/A
548       	ID_HEAD505			32			N/A			SMBAT/BRC5 BLOCK23 Voltage						N/A		SMBAT/BRC5 Block 23 Spannung				N/A
549       	ID_HEAD506			32			N/A			SMBAT/BRC5 BLOCK24 Voltage						N/A		SMBAT/BRC5 Block 24 Spannung				N/A
550       	ID_HEAD507			32			N/A			SMBAT/BRC6 BLOCK1  Voltage						N/A		SMBAT/BRC6 Block 1 Spannung				N/A
551       	ID_HEAD508			32			N/A			SMBAT/BRC6 BLOCK2  Voltage						N/A		SMBAT/BRC6 Block 2 Spannung	 				N/A
552       	ID_HEAD509			32			N/A			SMBAT/BRC6 BLOCK3  Voltage						N/A		SMBAT/BRC6 Block 3 Spannung	 				N/A
553       	ID_HEAD510			32			N/A			SMBAT/BRC6 BLOCK4  Voltage						N/A		SMBAT/BRC6 Block 4 Spannung	 				N/A
554       	ID_HEAD511			32			N/A			SMBAT/BRC6 BLOCK5  Voltage						N/A		SMBAT/BRC6 Block 5 Spannung					N/A
555       	ID_HEAD512			32			N/A			SMBAT/BRC6 BLOCK6  Voltage						N/A		SMBAT/BRC6 Block 6 Spannung	 				N/A
556       	ID_HEAD513			32			N/A			SMBAT/BRC6 BLOCK7  Voltage						N/A		SMBAT/BRC6 Block 7 Spannung	 				N/A
557       	ID_HEAD514			32			N/A			SMBAT/BRC6 BLOCK8  Voltage						N/A		SMBAT/BRC6 Block 8 Spannung	 				N/A
558       	ID_HEAD515			32			N/A			SMBAT/BRC6 BLOCK9  Voltage						N/A		SMBAT/BRC6 Block 9 Spannung					N/A
559       	ID_HEAD516			32			N/A			SMBAT/BRC6 BLOCK10 Voltage						N/A		SMBAT/BRC6 Block 10 Spannung					N/A
560       	ID_HEAD517			32			N/A			SMBAT/BRC6 BLOCK11 Voltage						N/A		SMBAT/BRC6 Block 11 Spannung					N/A
561       	ID_HEAD518			32			N/A			SMBAT/BRC6 BLOCK12 Voltage						N/A		SMBAT/BRC6 Block 12 Spannung					N/A
562       	ID_HEAD519			32			N/A			SMBAT/BRC6 BLOCK13 Voltage						N/A		SMBAT/BRC6 Block 13 Spannung					N/A
563       	ID_HEAD520			32			N/A			SMBAT/BRC6 BLOCK14 Voltage						N/A		SMBAT/BRC6 Block 14 Spannung				N/A
564       	ID_HEAD521			32			N/A			SMBAT/BRC6 BLOCK15 Voltage						N/A		SMBAT/BRC6 Block 15 Spannung				N/A
565       	ID_HEAD522			32			N/A			SMBAT/BRC6 BLOCK16 Voltage						N/A		SMBAT/BRC6 Block 16 Spannung					N/A
566       	ID_HEAD523			32			N/A			SMBAT/BRC6 BLOCK17 Voltage						N/A		SMBAT/BRC6 Block 17 Spannung					N/A
567       	ID_HEAD524			32			N/A			SMBAT/BRC6 BLOCK18 Voltage						N/A		SMBAT/BRC6 Block 18 Spannung					N/A
568       	ID_HEAD525			32			N/A			SMBAT/BRC6 BLOCK19 Voltage						N/A		SMBAT/BRC6 Block 19 Spannung					N/A
569       	ID_HEAD526			32			N/A			SMBAT/BRC6 BLOCK20 Voltage						N/A		SMBAT/BRC6 Block 20 Spannung					N/A
570       	ID_HEAD527			32			N/A			SMBAT/BRC6 BLOCK21 Voltage						N/A		SMBAT/BRC6 Block 21 Spannung					N/A
571       	ID_HEAD528			32			N/A			SMBAT/BRC6 BLOCK22 Voltage						N/A		SMBAT/BRC6 Block 22 Spannung					N/A
572       	ID_HEAD529			32			N/A			SMBAT/BRC6 BLOCK23 Voltage						N/A		SMBAT/BRC6 Block 23 Spannung					N/A
573       	ID_HEAD530			32			N/A			SMBAT/BRC6 BLOCK24 Voltage						N/A		SMBAT/BRC6 Block 24 Spannung					N/A
574       	ID_HEAD531			32			N/A			SMBAT/BRC7 BLOCK1  Voltage						N/A		SMBAT/BRC7 Block 1 Spannung					N/A
575       	ID_HEAD532			32			N/A			SMBAT/BRC7 BLOCK2  Voltage						N/A		SMBAT/BRC7 Block 2 Spannung					N/A
576       	ID_HEAD533			32			N/A			SMBAT/BRC7 BLOCK3  Voltage						N/A		SMBAT/BRC7 Block 3 Spannung					N/A
577       	ID_HEAD534			32			N/A			SMBAT/BRC7 BLOCK4  Voltage						N/A		SMBAT/BRC7 Block 4 Spannung					N/A
578       	ID_HEAD535			32			N/A			SMBAT/BRC7 BLOCK5  Voltage						N/A		SMBAT/BRC7 Block 5 Spannung	 				N/A
579       	ID_HEAD536			32			N/A			SMBAT/BRC7 BLOCK6  Voltage						N/A		SMBAT/BRC7 Block 6 Spannung					N/A
580       	ID_HEAD537			32			N/A			SMBAT/BRC7 BLOCK7  Voltage						N/A		SMBAT/BRC7 Block 7 Spannung					N/A
581       	ID_HEAD538			32			N/A			SMBAT/BRC7 BLOCK8  Voltage						N/A		SMBAT/BRC7 Block 8 Spannung					N/A
582       	ID_HEAD539			32			N/A			SMBAT/BRC7 BLOCK9  Voltage						N/A		SMBAT/BRC7 Block 9 Spannung	 				N/A
583       	ID_HEAD540			32			N/A			SMBAT/BRC7 BLOCK10 Voltage						N/A		SMBAT/BRC7 Block 10 Spannung					N/A
584       	ID_HEAD541			32			N/A			SMBAT/BRC7 BLOCK11 Voltage						N/A		SMBAT/BRC7 Block 11 Spannung					N/A
585       	ID_HEAD542			32			N/A			SMBAT/BRC7 BLOCK12 Voltage						N/A		SMBAT/BRC7 Block 12 Spannung					N/A
586       	ID_HEAD543			32			N/A			SMBAT/BRC7 BLOCK13 Voltage						N/A		SMBAT/BRC7 Block 13 Spannung					N/A
587       	ID_HEAD544			32			N/A			SMBAT/BRC7 BLOCK14 Voltage						N/A		SMBAT/BRC7 Block 14 Spannung					N/A
588       	ID_HEAD545			32			N/A			SMBAT/BRC7 BLOCK15 Voltage						N/A		SMBAT/BRC7 Block 15 Spannung					N/A
589       	ID_HEAD546			32			N/A			SMBAT/BRC7 BLOCK16 Voltage						N/A		SMBAT/BRC7 Block 16 Spannung					N/A
590       	ID_HEAD547			32			N/A			SMBAT/BRC7 BLOCK17 Voltage						N/A		SMBAT/BRC7 Block 17 Spannung					N/A
591       	ID_HEAD548			32			N/A			SMBAT/BRC7 BLOCK18 Voltage						N/A		SMBAT/BRC7 Block 18 Spannung					N/A
592       	ID_HEAD549			32			N/A			SMBAT/BRC7 BLOCK19 Voltage						N/A		SMBAT/BRC7 Block 19 Spannung					N/A
593       	ID_HEAD550			32			N/A			SMBAT/BRC7 BLOCK20 Voltage						N/A		SMBAT/BRC7 Block 20 Spannung					N/A
594       	ID_HEAD551			32			N/A			SMBAT/BRC7 BLOCK21 Voltage						N/A		SMBAT/BRC7 Block 21 Spannung					N/A
595       	ID_HEAD552			32			N/A			SMBAT/BRC7 BLOCK22 Voltage						N/A		SMBAT/BRC7 Block 22 Spannung					N/A
596       	ID_HEAD553			32			N/A			SMBAT/BRC7 BLOCK23 Voltage						N/A		SMBAT/BRC7 Block 23 Spannung					N/A
597       	ID_HEAD554			32			N/A			SMBAT/BRC7 BLOCK24 Voltage						N/A		SMBAT/BRC7 Block 24 Spannung					N/A
598       	ID_HEAD555			32			N/A			SMBAT/BRC8 BLOCK1  Voltage						N/A		SMBAT/BRC8 Block 1 Spannung					N/A
599       	ID_HEAD556			32			N/A			SMBAT/BRC8 BLOCK2  Voltage						N/A		SMBAT/BRC8 Block 2 Spannung	 				N/A
600       	ID_HEAD557			32			N/A			SMBAT/BRC8 BLOCK3  Voltage						N/A		SMBAT/BRC8 Block 3 Spannung	 				N/A
601       	ID_HEAD558			32			N/A			SMBAT/BRC8 BLOCK4  Voltage						N/A		SMBAT/BRC8 Block 4 Spannung	 				N/A
602       	ID_HEAD559			32			N/A			SMBAT/BRC8 BLOCK5  Voltage						N/A		SMBAT/BRC8 Block 5 Spannung					N/A
603       	ID_HEAD560			32			N/A			SMBAT/BRC8 BLOCK6  Voltage						N/A		SMBAT/BRC8 Block 6 Spannung	 				N/A
604       	ID_HEAD561			32			N/A			SMBAT/BRC8 BLOCK7  Voltage						N/A		SMBAT/BRC8 Block 7 Spannung					N/A
605       	ID_HEAD562			32			N/A			SMBAT/BRC8 BLOCK8  Voltage						N/A		SMBAT/BRC8 Block 8 Spannung	 				N/A
606       	ID_HEAD563			32			N/A			SMBAT/BRC8 BLOCK9  Voltage						N/A		SMBAT/BRC8 Block 9 Spannung	 				N/A
607       	ID_HEAD564			32			N/A			SMBAT/BRC8 BLOCK10 Voltage						N/A		SMBAT/BRC8 Block 10 Spannung					N/A
608       	ID_HEAD565			32			N/A			SMBAT/BRC8 BLOCK11 Voltage						N/A		SMBAT/BRC8 Block 11 Spannung					N/A
609       	ID_HEAD566			32			N/A			SMBAT/BRC8 BLOCK12 Voltage						N/A		SMBAT/BRC8 Block 12 Spannung					N/A
610       	ID_HEAD567			32			N/A			SMBAT/BRC8 BLOCK13 Voltage						N/A		SMBAT/BRC8 Block 13 Spannung					N/A
611       	ID_HEAD568			32			N/A			SMBAT/BRC8 BLOCK14 Voltage						N/A		SMBAT/BRC8 Block 14 Spannung					N/A
612       	ID_HEAD569			32			N/A			SMBAT/BRC8 BLOCK15 Voltage						N/A		SMBAT/BRC8 Block 15 Spannung					N/A
613       	ID_HEAD570			32			N/A			SMBAT/BRC8 BLOCK16 Voltage						N/A		SMBAT/BRC8 Block 16 Spannung					N/A
614       	ID_HEAD571			32			N/A			SMBAT/BRC8 BLOCK17 Voltage						N/A		SMBAT/BRC8 Block 17 Spannung					N/A
615       	ID_HEAD572			32			N/A			SMBAT/BRC8 BLOCK18 Voltage						N/A		SMBAT/BRC8 Block 18 Spannung					N/A
616       	ID_HEAD573			32			N/A			SMBAT/BRC8 BLOCK19 Voltage						N/A		SMBAT/BRC8 Block 19 Spannung					N/A
617       	ID_HEAD574			32			N/A			SMBAT/BRC8 BLOCK20 Voltage						N/A		SMBAT/BRC8 Block 20 Spannung					N/A
618       	ID_HEAD575			32			N/A			SMBAT/BRC8 BLOCK21 Voltage						N/A		SMBAT/BRC8 Block 21 Spannung					N/A
619       	ID_HEAD576			32			N/A			SMBAT/BRC8 BLOCK22 Voltage						N/A		SMBAT/BRC8 Block 22 Spannung					N/A
620       	ID_HEAD577			32			N/A			SMBAT/BRC8 BLOCK23 Voltage						N/A		SMBAT/BRC8 Block 23 Spannung					N/A
621       	ID_HEAD578			32			N/A			SMBAT/BRC8 BLOCK24 Voltage						N/A		SMBAT/BRC8 Block 24 Spannung					N/A
622       	ID_HEAD579			32			N/A			SMBAT/BRC9 BLOCK1  Voltage						N/A		SMBAT/BRC9 Block 1 Spannung	 				N/A
623       	ID_HEAD580			32			N/A			SMBAT/BRC9 BLOCK2  Voltage						N/A		SMBAT/BRC9 Block 2 Spannung 				N/A
624       	ID_HEAD581			32			N/A			SMBAT/BRC9 BLOCK3  Voltage						N/A		SMBAT/BRC9 Block 3 Spannung 				N/A
625       	ID_HEAD582			32			N/A			SMBAT/BRC9 BLOCK4  Voltage						N/A		SMBAT/BRC9 Block 4 Spannung				N/A
626       	ID_HEAD583			32			N/A			SMBAT/BRC9 BLOCK5  Voltage						N/A		SMBAT/BRC9 Block 5 Spannung				N/A
627       	ID_HEAD584			32			N/A			SMBAT/BRC9 BLOCK6  Voltage						N/A		SMBAT/BRC9 Block 6 Spannung 				N/A
628       	ID_HEAD585			32			N/A			SMBAT/BRC9 BLOCK7  Voltage						N/A		SMBAT/BRC9 Block 7 Spannung				N/A
629       	ID_HEAD586			32			N/A			SMBAT/BRC9 BLOCK8  Voltage						N/A		SMBAT/BRC9 Block 8 Spannung				N/A
630       	ID_HEAD587			32			N/A			SMBAT/BRC9 BLOCK9  Voltage						N/A		SMBAT/BRC9 Block 9 Spannung 				N/A
631       	ID_HEAD588			32			N/A			SMBAT/BRC9 BLOCK10 Voltage						N/A		SMBAT/BRC9 Block 10 Spannung				N/A
632       	ID_HEAD589			32			N/A			SMBAT/BRC9 BLOCK11 Voltage						N/A		SMBAT/BRC9 Block 11 Spannung				N/A
633       	ID_HEAD590			32			N/A			SMBAT/BRC9 BLOCK12 Voltage						N/A		SMBAT/BRC9 Block 12 Spannung				N/A
634       	ID_HEAD591			32			N/A			SMBAT/BRC9 BLOCK13 Voltage						N/A		SMBAT/BRC9 Block 13 Spannung				N/A
635       	ID_HEAD592			32			N/A			SMBAT/BRC9 BLOCK14 Voltage						N/A		SMBAT/BRC9 Block 14 Spannung				N/A
636       	ID_HEAD593			32			N/A			SMBAT/BRC9 BLOCK15 Voltage						N/A		SMBAT/BRC9 Block 15 Spannung				N/A
637       	ID_HEAD594			32			N/A			SMBAT/BRC9 BLOCK16 Voltage						N/A		SMBAT/BRC9 Block 16 Spannung				N/A
638       	ID_HEAD595			32			N/A			SMBAT/BRC9 BLOCK17 Voltage						N/A		SMBAT/BRC9 Block 17 Spannung				N/A
639       	ID_HEAD596			32			N/A			SMBAT/BRC9 BLOCK18 Voltage						N/A		SMBAT/BRC9 Block 18 Spannung				N/A
640       	ID_HEAD597			32			N/A			SMBAT/BRC9 BLOCK19 Voltage						N/A		SMBAT/BRC9 Block 19 Spannung				N/A
641       	ID_HEAD598			32			N/A			SMBAT/BRC9 BLOCK20 Voltage						N/A		SMBAT/BRC9 Block 20 Spannung				N/A
642       	ID_HEAD599			32			N/A			SMBAT/BRC9 BLOCK21 Voltage						N/A		SMBAT/BRC9 Block 21 Spannung				N/A
643       	ID_HEAD600			32			N/A			SMBAT/BRC9 BLOCK22 Voltage						N/A		SMBAT/BRC9 Block 22 Spannung				N/A
644       	ID_HEAD601			32			N/A			SMBAT/BRC9 BLOCK23 Voltage						N/A		SMBAT/BRC9 Block 23 Spannung				N/A
645       	ID_HEAD602			32			N/A			SMBAT/BRC9 BLOCK24 Voltage						N/A		SMBAT/BRC9 Block 24 Spannung				N/A
646       	ID_HEAD603			32			N/A			SMBAT/BRC10 BLOCK1  Voltage						N/A		SMBAT/BRC10 Block 1 Spannung 				N/A
647       	ID_HEAD604			32			N/A			SMBAT/BRC10 BLOCK2  Voltage						N/A		SMBAT/BRC10 Block 2 Spannung 				N/A
648       	ID_HEAD605			32			N/A			SMBAT/BRC10 BLOCK3  Voltage						N/A		SMBAT/BRC10 Block 3 Spannung  				N/A
649       	ID_HEAD606			32			N/A			SMBAT/BRC10 BLOCK4  Voltage						N/A		SMBAT/BRC10 Block 4 Spannung  				N/A
650       	ID_HEAD607			32			N/A			SMBAT/BRC10 BLOCK5  Voltage						N/A		SMBAT/BRC10 Block 5 Spannung  				N/A
651       	ID_HEAD608			32			N/A			SMBAT/BRC10 BLOCK6  Voltage						N/A		SMBAT/BRC10 Block 6 Spannung 				N/A
652       	ID_HEAD609			32			N/A			SMBAT/BRC10 BLOCK7  Voltage						N/A		SMBAT/BRC10 Block 7 Spannung  				N/A
653       	ID_HEAD610			32			N/A			SMBAT/BRC10 BLOCK8  Voltage						N/A		SMBAT/BRC10 Block 8 Spannung  				N/A
654       	ID_HEAD611			32			N/A			SMBAT/BRC10 BLOCK9  Voltage						N/A		SMBAT/BRC10 Block 9 Spannung 				N/A
655       	ID_HEAD612			32			N/A			SMBAT/BRC10 BLOCK10 Voltage						N/A		SMBAT/BRC10 Block 10 Spannung 				N/A
656       	ID_HEAD613			32			N/A			SMBAT/BRC10 BLOCK11 Voltage						N/A		SMBAT/BRC10 Block 11 Spannung 				N/A
657       	ID_HEAD614			32			N/A			SMBAT/BRC10 BLOCK12 Voltage						N/A		SMBAT/BRC10 Block 12 Spannung 				N/A
658       	ID_HEAD615			32			N/A			SMBAT/BRC10 BLOCK13 Voltage						N/A		SMBAT/BRC10 Block 13 Spannung 				N/A
659       	ID_HEAD616			32			N/A			SMBAT/BRC10 BLOCK14 Voltage						N/A		SMBAT/BRC10 Block 14 Spannung 				N/A
660       	ID_HEAD617			32			N/A			SMBAT/BRC10 BLOCK15 Voltage						N/A		SMBAT/BRC10 Block 15 Spannung 				N/A
661       	ID_HEAD618			32			N/A			SMBAT/BRC10 BLOCK16 Voltage						N/A		SMBAT/BRC10 Block 16 Spannung 				N/A
662       	ID_HEAD619			32			N/A			SMBAT/BRC10 BLOCK17 Voltage						N/A		SMBAT/BRC10 Block 17 Spannung 				N/A
663       	ID_HEAD620			32			N/A			SMBAT/BRC10 BLOCK18 Voltage						N/A		SMBAT/BRC10 Block 18 Spannung 				N/A
664       	ID_HEAD621			32			N/A			SMBAT/BRC10 BLOCK19 Voltage						N/A		SMBAT/BRC10 Block 19 Spannung 			N/A
665       	ID_HEAD622			32			N/A			SMBAT/BRC10 BLOCK20 Voltage						N/A		SMBAT/BRC10 Block 20 Spannung 				N/A
666       	ID_HEAD623			32			N/A			SMBAT/BRC10 BLOCK21 Voltage						N/A		SMBAT/BRC10 Block 21 Spannung 			N/A
667       	ID_HEAD624			32			N/A			SMBAT/BRC10 BLOCK22 Voltage						N/A		SMBAT/BRC10 Block 22 Spannung 			N/A
668       	ID_HEAD625			32			N/A			SMBAT/BRC10 BLOCK23 Voltage						N/A		SMBAT/BRC10 Block 24 Spannung 			N/A
669       	ID_HEAD626			32			N/A			SMBAT/BRC10 BLOCK24 Voltage						N/A		Tension Bloc24 SMBAT/BRC10				N/A
670       	ID_HEAD627			32			N/A			SMBAT/BRC11 BLOCK1  Voltage						N/A		SMBAT/BRC11 Block 1 Spannung  				N/A
671       	ID_HEAD628			32			N/A			SMBAT/BRC11 BLOCK2  Voltage						N/A		SMBAT/BRC11 Block 2 Spannung  				N/A
672       	ID_HEAD629			32			N/A			SMBAT/BRC11 BLOCK3  Voltage						N/A		SMBAT/BRC11 Block 3 Spannung  				N/A
673       	ID_HEAD630			32			N/A			SMBAT/BRC11 BLOCK4  Voltage						N/A		SMBAT/BRC11 Block 4 Spannung 				N/A
674       	ID_HEAD631			32			N/A			SMBAT/BRC11 BLOCK5  Voltage						N/A		SMBAT/BRC11 Block 5 Spannung  				N/A
675       	ID_HEAD632			32			N/A			SMBAT/BRC11 BLOCK6  Voltage						N/A		SMBAT/BRC11 Block 6 Spannung 				N/A
676       	ID_HEAD633			32			N/A			SMBAT/BRC11 BLOCK7  Voltage						N/A		SMBAT/BRC11 Block 7 Spannung 				N/A
677       	ID_HEAD634			32			N/A			SMBAT/BRC11 BLOCK8  Voltage						N/A		SMBAT/BRC11 Block 8 Spannung  				N/A
678       	ID_HEAD635			32			N/A			SMBAT/BRC11 BLOCK9  Voltage						N/A		SMBAT/BRC11 Block 9 Spannung 				N/A
679       	ID_HEAD636			32			N/A			SMBAT/BRC11 BLOCK10 Voltage						N/A		SMBAT/BRC11 Block 10 Spannung 				N/A
680       	ID_HEAD637			32			N/A			SMBAT/BRC11 BLOCK11 Voltage						N/A		SMBAT/BRC11 Block 11 Spannung 				N/A
681       	ID_HEAD638			32			N/A			SMBAT/BRC11 BLOCK12 Voltage						N/A		SMBAT/BRC11 Block 12 Spannung 			N/A
682       	ID_HEAD639			32			N/A			SMBAT/BRC11 BLOCK13 Voltage						N/A		SMBAT/BRC11 Block 13 Spannung 				N/A
683       	ID_HEAD640			32			N/A			SMBAT/BRC11 BLOCK14 Voltage						N/A		SMBAT/BRC11 Block 14 Spannung 			N/A
684       	ID_HEAD641			32			N/A			SMBAT/BRC11 BLOCK15 Voltage						N/A		SMBAT/BRC11 Block 15 Spannung 				N/A
685       	ID_HEAD642			32			N/A			SMBAT/BRC11 BLOCK16 Voltage						N/A		SMBAT/BRC11 Block 16 Spannung 				N/A
686       	ID_HEAD643			32			N/A			SMBAT/BRC11 BLOCK17 Voltage						N/A		SMBAT/BRC11 Block 17 Spannung 				N/A
687       	ID_HEAD644			32			N/A			SMBAT/BRC11 BLOCK18 Voltage						N/A		SMBAT/BRC11 Block 18 Spannung 				N/A
688       	ID_HEAD645			32			N/A			SMBAT/BRC11 BLOCK19 Voltage						N/A		SMBAT/BRC11 Block 19 Spannung 			N/A
689       	ID_HEAD646			32			N/A			SMBAT/BRC11 BLOCK20 Voltage						N/A		SMBAT/BRC11 Block 20 Spannung 				N/A
690       	ID_HEAD647			32			N/A			SMBAT/BRC11 BLOCK21 Voltage						N/A		SMBAT/BRC11 Block 21 Spannung 				N/A
691       	ID_HEAD648			32			N/A			SMBAT/BRC11 BLOCK22 Voltage						N/A		SMBAT/BRC11 Block 22 Spannung 				N/A
692       	ID_HEAD649			32			N/A			SMBAT/BRC11 BLOCK23 Voltage						N/A		SMBAT/BRC11 Block 23 Spannung 			N/A
693       	ID_HEAD650			32			N/A			SMBAT/BRC11 BLOCK24 Voltage						N/A		SMBAT/BRC11 Block 24 Spannung 			N/A
694       	ID_HEAD651			32			N/A			SMBAT/BRC12 BLOCK1  Voltage						N/A		SMBAT/BRC12 Block 1 Spannung  				N/A
695       	ID_HEAD652			32			N/A			SMBAT/BRC12 BLOCK2  Voltage						N/A	  SMBAT/BRC12 Block 2 Spannung  				N/A
696       	ID_HEAD653			32			N/A			SMBAT/BRC12 BLOCK3  Voltage						N/A		SMBAT/BRC12 Block 3 Spannung  				N/A
697       	ID_HEAD654			32			N/A			SMBAT/BRC12 BLOCK4  Voltage						N/A		SMBAT/BRC12 Block 4 Spannung 				N/A
698       	ID_HEAD655			32			N/A			SMBAT/BRC12 BLOCK5  Voltage						N/A		SMBAT/BRC12 Block 5 Spannung 				N/A
699       	ID_HEAD656			32			N/A			SMBAT/BRC12 BLOCK6  Voltage						N/A		SMBAT/BRC12 Block 6 Spannung  				N/A
700       	ID_HEAD657			32			N/A			SMBAT/BRC12 BLOCK7  Voltage						N/A		SMBAT/BRC12 Block 7 Spannung  				N/A
701       	ID_HEAD658			32			N/A			SMBAT/BRC12 BLOCK8  Voltage						N/A		SMBAT/BRC12 Block 8 Spannung  				N/A
702       	ID_HEAD659			32			N/A			SMBAT/BRC12 BLOCK9  Voltage						N/A		SMBAT/BRC12 Block 9 Spannung  				N/A
703       	ID_HEAD660			32			N/A			SMBAT/BRC12 BLOCK10 Voltage						N/A		SMBAT/BRC12 Block 10 Spannung 				N/A
704       	ID_HEAD661			32			N/A			SMBAT/BRC12 BLOCK11 Voltage						N/A		SMBAT/BRC12 Block 11 Spannung 				N/A
705       	ID_HEAD662			32			N/A			SMBAT/BRC12 BLOCK12 Voltage						N/A		SMBAT/BRC12 Block 12 Spannung 				N/A
706       	ID_HEAD663			32			N/A			SMBAT/BRC12 BLOCK13 Voltage						N/A		SMBAT/BRC12 Block 13 Spannung 			N/A
707       	ID_HEAD664			32			N/A			SMBAT/BRC12 BLOCK14 Voltage						N/A		SMBAT/BRC12 Block 14 Spannung 			N/A
708       	ID_HEAD665			32			N/A			SMBAT/BRC12 BLOCK15 Voltage						N/A		SMBAT/BRC12 Block 15 Spannung 				N/A
709       	ID_HEAD666			32			N/A			SMBAT/BRC12 BLOCK16 Voltage						N/A		SMBAT/BRC12 Block 16 Spannung 				N/A
710       	ID_HEAD667			32			N/A			SMBAT/BRC12 BLOCK17 Voltage						N/A		SMBAT/BRC12 Block 17 Spannung 				N/A
711       	ID_HEAD668			32			N/A			SMBAT/BRC12 BLOCK18 Voltage						N/A		SMBAT/BRC12 Block 18 Spannung 			N/A
712       	ID_HEAD669			32			N/A			SMBAT/BRC12 BLOCK19 Voltage						N/A		SMBAT/BRC12 Block 19 Spannung 				N/A
713       	ID_HEAD670			32			N/A			SMBAT/BRC12 BLOCK20 Voltage						N/A		SMBAT/BRC12 Block 20 Spannung 				N/A
714       	ID_HEAD671			32			N/A			SMBAT/BRC12 BLOCK21 Voltage						N/A		SMBAT/BRC12 Block 21 Spannung 				N/A
715       	ID_HEAD672			32			N/A			SMBAT/BRC12 BLOCK22 Voltage						N/A		SMBAT/BRC12 Block 22 Spannung 				N/A
716       	ID_HEAD673			32			N/A			SMBAT/BRC12 BLOCK23 Voltage						N/A		SMBAT/BRC12 Block 23 Spannung 				N/A
717       	ID_HEAD674			32			N/A			SMBAT/BRC12 BLOCK24 Voltage						N/A		SMBAT/BRC12 Block 24 Spannung 			N/A
718       	ID_HEAD675			32			N/A			SMBAT/BRC13 BLOCK1  Voltage						N/A		SMBAT/BRC13 Block 1 Spannung 				N/A
719       	ID_HEAD676			32			N/A			SMBAT/BRC13 BLOCK2  Voltage						N/A		SMBAT/BRC13 Block 2 Spannung 				N/A
720       	ID_HEAD677			32			N/A			SMBAT/BRC13 BLOCK3  Voltage						N/A		SMBAT/BRC13 Block 3 Spannung  				N/A
721       	ID_HEAD678			32			N/A			SMBAT/BRC13 BLOCK4  Voltage						N/A		SMBAT/BRC13 Block 4 Spannung 				N/A
722       	ID_HEAD679			32			N/A			SMBAT/BRC13 BLOCK5  Voltage						N/A		SMBAT/BRC13 Block 5 Spannung  				N/A
723       	ID_HEAD680			32			N/A			SMBAT/BRC13 BLOCK6  Voltage						N/A		SMBAT/BRC13 Block 6 Spannung 				N/A
724       	ID_HEAD681			32			N/A			SMBAT/BRC13 BLOCK7  Voltage						N/A		SMBAT/BRC13 Block 7 Spannung 				N/A
725       	ID_HEAD682			32			N/A			SMBAT/BRC13 BLOCK8  Voltage						N/A		SMBAT/BRC13 Block 8 Spannung 				N/A
726       	ID_HEAD683			32			N/A			SMBAT/BRC13 BLOCK9  Voltage						N/A		SMBAT/BRC13 Block 9 Spannung  				N/A
727       	ID_HEAD684			32			N/A			SMBAT/BRC13 BLOCK10 Voltage						N/A		SMBAT/BRC13 Block 10 Spannung 				N/A
728       	ID_HEAD685			32			N/A			SMBAT/BRC13 BLOCK11 Voltage						N/A	  SMBAT/BRC13 Block 11 Spannung 			N/A
729       	ID_HEAD686			32			N/A			SMBAT/BRC13 BLOCK12 Voltage						N/A		SMBAT/BRC13 Block 12 Spannung 				N/A
730       	ID_HEAD687			32			N/A			SMBAT/BRC13 BLOCK13 Voltage						N/A		SMBAT/BRC13 Block 13 Spannung 			N/A
731       	ID_HEAD688			32			N/A			SMBAT/BRC13 BLOCK14 Voltage						N/A		SMBAT/BRC13 Block 14 Spannung 			N/A
732       	ID_HEAD689			32			N/A			SMBAT/BRC13 BLOCK15 Voltage						N/A		SMBAT/BRC13 Block 15 Spannung 				N/A
733       	ID_HEAD690			32			N/A			SMBAT/BRC13 BLOCK16 Voltage						N/A		SMBAT/BRC13 Block 16 Spannung 				N/A
734       	ID_HEAD691			32			N/A			SMBAT/BRC13 BLOCK17 Voltage						N/A		SMBAT/BRC13 Block 17 Spannung 				N/A
735       	ID_HEAD692			32			N/A			SMBAT/BRC13 BLOCK18 Voltage						N/A		SMBAT/BRC13 Block 18 Spannung 				N/A
736       	ID_HEAD693			32			N/A			SMBAT/BRC13 BLOCK19 Voltage						N/A		SMBAT/BRC13 Block 19 Spannung 				N/A
737       	ID_HEAD694			32			N/A			SMBAT/BRC13 BLOCK20 Voltage						N/A		SMBAT/BRC13 Block 20 Spannung 				N/A
738       	ID_HEAD695			32			N/A			SMBAT/BRC13 BLOCK21 Voltage						N/A		SMBAT/BRC13 Block 21 Spannung 				N/A
739       	ID_HEAD696			32			N/A			SMBAT/BRC13 BLOCK22 Voltage						N/A		SMBAT/BRC13 Block 22 Spannung 				N/A
740       	ID_HEAD697			32			N/A			SMBAT/BRC13 BLOCK23 Voltage						N/A		SMBAT/BRC13 Block 23 Spannung 				N/A
741       	ID_HEAD698			32			N/A			SMBAT/BRC13 BLOCK24 Voltage						N/A		SMBAT/BRC13 Block 24 Spannung 				N/A
742       	ID_HEAD699			32			N/A			SMBAT/BRC14 BLOCK1  Voltage						N/A		SMBAT/BRC14 Block 1 Spannung  				N/A
743       	ID_HEAD700			32			N/A			SMBAT/BRC14 BLOCK2  Voltage						N/A		SMBAT/BRC14 Block 2 Spannung  				N/A
744       	ID_HEAD701			32			N/A			SMBAT/BRC14 BLOCK3  Voltage						N/A		SMBAT/BRC14 Block 3 Spannung 				N/A
745       	ID_HEAD702			32			N/A			SMBAT/BRC14 BLOCK4  Voltage						N/A		SMBAT/BRC14 Block 4 Spannung  				N/A
746       	ID_HEAD703			32			N/A			SMBAT/BRC14 BLOCK5  Voltage						N/A		SMBAT/BRC14 Block 5 Spannung 				N/A
747       	ID_HEAD704			32			N/A			SMBAT/BRC14 BLOCK6  Voltage						N/A		SMBAT/BRC14 Block 6 Spannung  				N/A
748       	ID_HEAD705			32			N/A			SMBAT/BRC14 BLOCK7  Voltage						N/A		SMBAT/BRC14 Block 7 Spannung  				N/A
749       	ID_HEAD706			32			N/A			SMBAT/BRC14 BLOCK8  Voltage						N/A		SMBAT/BRC14 Block 8 Spannung  				N/A
750       	ID_HEAD707			32			N/A			SMBAT/BRC14 BLOCK9  Voltage						N/A		SMBAT/BRC14 Block 9 Spannung  				N/A
751       	ID_HEAD708			32			N/A			SMBAT/BRC14 BLOCK10 Voltage						N/A		SMBAT/BRC14 Block 10 Spannung 			N/A
752       	ID_HEAD709			32			N/A			SMBAT/BRC14 BLOCK11 Voltage						N/A		SMBAT/BRC14 Block 11 Spannung 				N/A
753       	ID_HEAD710			32			N/A			SMBAT/BRC14 BLOCK12 Voltage						N/A		SMBAT/BRC14 Block 12 Spannung 				N/A
754       	ID_HEAD711			32			N/A			SMBAT/BRC14 BLOCK13 Voltage						N/A		SMBAT/BRC14 Block 13 Spannung 				N/A
755       	ID_HEAD712			32			N/A			SMBAT/BRC14 BLOCK14 Voltage						N/A		SMBAT/BRC14 Block 14 Spannung 				N/A
756       	ID_HEAD713			32			N/A			SMBAT/BRC14 BLOCK15 Voltage						N/A		SMBAT/BRC14 Block 15 Spannung 				N/A
757       	ID_HEAD714			32			N/A			SMBAT/BRC14 BLOCK16 Voltage						N/A		SMBAT/BRC14 Block 16 Spannung 			N/A
758       	ID_HEAD715			32			N/A			SMBAT/BRC14 BLOCK17 Voltage						N/A	  SMBAT/BRC14 Block 17 Spannung 			N/A
759       	ID_HEAD716			32			N/A			SMBAT/BRC14 BLOCK18 Voltage						N/A		SMBAT/BRC14 Block 18 Spannung 				N/A
760       	ID_HEAD717			32			N/A			SMBAT/BRC14 BLOCK19 Voltage						N/A		SMBAT/BRC14 Block 19 Spannung 				N/A
761       	ID_HEAD718			32			N/A			SMBAT/BRC14 BLOCK20 Voltage						N/A		SMBAT/BRC14 Block 20 Spannung 			N/A
762       	ID_HEAD719			32			N/A			SMBAT/BRC14 BLOCK21 Voltage						N/A		SMBAT/BRC14 Block 21 Spannung 				N/A
763       	ID_HEAD720			32			N/A			SMBAT/BRC14 BLOCK22 Voltage						N/A		SMBAT/BRC14 Block 22 Spannung 				N/A
764       	ID_HEAD721			32			N/A			SMBAT/BRC14 BLOCK23 Voltage						N/A		SMBAT/BRC14 Block 23 Spannung 				N/A
765       	ID_HEAD722			32			N/A			SMBAT/BRC14 BLOCK24 Voltage						N/A		SMBAT/BRC14 Block 24 Spannung 			N/A
766       	ID_HEAD723			32			N/A			SMBAT/BRC15 BLOCK1  Voltage						N/A		SMBAT/BRC15 Block 1 Spannung  				N/A
767       	ID_HEAD724			32			N/A			SMBAT/BRC15 BLOCK2  Voltage						N/A		SMBAT/BRC15 Block 2 Spannung 				N/A
768       	ID_HEAD725			32			N/A			SMBAT/BRC15 BLOCK3  Voltage						N/A		SMBAT/BRC15 Block 3 Spannung				N/A
769       	ID_HEAD726			32			N/A			SMBAT/BRC15 BLOCK4  Voltage						N/A		SMBAT/BRC15 Block 4 Spannung 				N/A
770       	ID_HEAD727			32			N/A			SMBAT/BRC15 BLOCK5  Voltage						N/A		SMBAT/BRC15 Block 5 Spannung 				N/A
771       	ID_HEAD728			32			N/A			SMBAT/BRC15 BLOCK6  Voltage						N/A		SMBAT/BRC15 Block 6 Spannung 				N/A
772       	ID_HEAD729			32			N/A			SMBAT/BRC15 BLOCK7  Voltage						N/A		SMBAT/BRC15 Block 7 Spannung				N/A
773       	ID_HEAD730			32			N/A			SMBAT/BRC15 BLOCK8  Voltage						N/A		SMBAT/BRC15 Block 8 Spannung				N/A
774       	ID_HEAD731			32			N/A			SMBAT/BRC15 BLOCK9  Voltage						N/A		SMBAT/BRC15 Block 9 Spannung 				N/A
775       	ID_HEAD732			32			N/A			SMBAT/BRC15 BLOCK10 Voltage						N/A		SMBAT/BRC15 Block 10 Spannung				N/A
776       	ID_HEAD733			32			N/A			SMBAT/BRC15 BLOCK11 Voltage						N/A		SMBAT/BRC15 Block 11 Spannung				N/A
777       	ID_HEAD734			32			N/A			SMBAT/BRC15 BLOCK12 Voltage						N/A		SMBAT/BRC15 Block 12 Spannung				N/A
778       	ID_HEAD735			32			N/A			SMBAT/BRC15 BLOCK13 Voltage						N/A		SMBAT/BRC15 Block 13 Spannung				N/A
779       	ID_HEAD736			32			N/A			SMBAT/BRC15 BLOCK14 Voltage						N/A		SMBAT/BRC15 Block 14 Spannung				N/A
780       	ID_HEAD737			32			N/A			SMBAT/BRC15 BLOCK15 Voltage						N/A		SMBAT/BRC15 Block 15 Spannung				N/A
781       	ID_HEAD738			32			N/A			SMBAT/BRC15 BLOCK16 Voltage						N/A		SMBAT/BRC15 Block 16 Spannung				N/A
782       	ID_HEAD739			32			N/A			SMBAT/BRC15 BLOCK17 Voltage						N/A		SMBAT/BRC15 Block 17 Spannung				N/A
783       	ID_HEAD740			32			N/A			SMBAT/BRC15 BLOCK18 Voltage						N/A		SMBAT/BRC15 Block 18 Spannung				N/A
784       	ID_HEAD741			32			N/A			SMBAT/BRC15 BLOCK19 Voltage						N/A		SMBAT/BRC15 Block 19 Spannung				N/A
785       	ID_HEAD742			32			N/A			SMBAT/BRC15 BLOCK20 Voltage						N/A		SMBAT/BRC15 Block 20 Spannung				N/A
786       	ID_HEAD743			32			N/A			SMBAT/BRC15 BLOCK21 Voltage						N/A		SMBAT/BRC15 Block 21 Spannung			N/A
787       	ID_HEAD744			32			N/A			SMBAT/BRC15 BLOCK22 Voltage						N/A		SMBAT/BRC15 Block 22 Spannung				N/A
788       	ID_HEAD745			32			N/A			SMBAT/BRC15 BLOCK23 Voltage						N/A		SMBAT/BRC15 Block 23 Spannung			N/A
789       	ID_HEAD746			32			N/A			SMBAT/BRC15 BLOCK24 Voltage						N/A	  SMBAT/BRC15 Block 24 Spannung				N/A
790       	ID_HEAD747			32			N/A			SMBAT/BRC16 BLOCK1  Voltage						N/A		SMBAT/BRC16 Block 1 Spannung				N/A
791       	ID_HEAD748			32			N/A			SMBAT/BRC16 BLOCK2  Voltage						N/A		SMBAT/BRC16 Block 2 Spannung 				N/A
792       	ID_HEAD749			32			N/A			SMBAT/BRC16 BLOCK3  Voltage						N/A		SMBAT/BRC16 Block 3 Spannung 				N/A
793       	ID_HEAD750			32			N/A			SMBAT/BRC16 BLOCK4  Voltage						N/A		SMBAT/BRC16 Block 4 Spannung 				N/A
794       	ID_HEAD751			32			N/A			SMBAT/BRC16 BLOCK5  Voltage						N/A		SMBAT/BRC16 Block 5 Spannung				N/A
795       	ID_HEAD752			32			N/A			SMBAT/BRC16 BLOCK6  Voltage						N/A		SMBAT/BRC16 Block 6 Spannung 				N/A
796       	ID_HEAD753			32			N/A			SMBAT/BRC16 BLOCK7  Voltage						N/A	  SMBAT/BRC16 Block 7 Spannung 				N/A
797       	ID_HEAD754			32			N/A			SMBAT/BRC16 BLOCK8  Voltage						N/A		SMBAT/BRC16 Block 8 Spannung 				N/A
798       	ID_HEAD755			32			N/A			SMBAT/BRC16 BLOCK9  Voltage						N/A		SMBAT/BRC16 Block 9 Spannung 				N/A
799       	ID_HEAD756			32			N/A			SMBAT/BRC16 BLOCK10 Voltage						N/A		SMBAT/BRC16 Block 10 Spannung			N/A
800       	ID_HEAD757			32			N/A			SMBAT/BRC16 BLOCK11 Voltage						N/A		SMBAT/BRC16 Block 11 Spannung				N/A
801       	ID_HEAD758			32			N/A			SMBAT/BRC16 BLOCK12 Voltage						N/A		SMBAT/BRC16 Block 12 Spannung				N/A
802       	ID_HEAD759			32			N/A			SMBAT/BRC16 BLOCK13 Voltage						N/A		SMBAT/BRC16 Block 13 Spannung				N/A
803       	ID_HEAD760			32			N/A			SMBAT/BRC16 BLOCK14 Voltage						N/A		SMBAT/BRC16 Block 14 Spannung				N/A
804       	ID_HEAD761			32			N/A			SMBAT/BRC16 BLOCK15 Voltage						N/A		SMBAT/BRC16 Block 15 Spannung			N/A
805       	ID_HEAD762			32			N/A			SMBAT/BRC16 BLOCK16 Voltage						N/A		SMBAT/BRC16 Block 16 Spannung				N/A
806       	ID_HEAD763			32			N/A			SMBAT/BRC16 BLOCK17 Voltage						N/A		SMBAT/BRC16 Block 17 Spannung				N/A
807       	ID_HEAD764			32			N/A			SMBAT/BRC16 BLOCK18 Voltage						N/A		SMBAT/BRC16 Block 18 Spannung				N/A
808       	ID_HEAD765			32			N/A			SMBAT/BRC16 BLOCK19 Voltage						N/A		SMBAT/BRC16 Block 19 Spannung			N/A
809       	ID_HEAD766			32			N/A			SMBAT/BRC16 BLOCK20 Voltage						N/A		SMBAT/BRC16 Block 20 Spannung				N/A
810       	ID_HEAD767			32			N/A			SMBAT/BRC16 BLOCK21 Voltage						N/A		SMBAT/BRC16 Block 21 Spannung				N/A
811       	ID_HEAD768			32			N/A			SMBAT/BRC16 BLOCK22 Voltage						N/A		SMBAT/BRC16 Block 22 Spannung				N/A
812       	ID_HEAD769			32			N/A			SMBAT/BRC16 BLOCK23 Voltage						N/A		SMBAT/BRC16 Block 23 Spannung				N/A
813       	ID_HEAD770			32			N/A			SMBAT/BRC16 BLOCK24 Voltage						N/A		SMBAT/BRC16 Block 24 Spannung				N/A
814       	ID_HEAD771			32			N/A			SMBAT/BRC17 BLOCK1  Voltage						N/A		SMBAT/BRC17 Block 1	Spannung			N/A
815       	ID_HEAD772			32			N/A			SMBAT/BRC17 BLOCK2  Voltage						N/A		SMBAT/BRC17 Block 2 Spannung				N/A
816       	ID_HEAD773			32			N/A			SMBAT/BRC17 BLOCK3  Voltage						N/A		SMBAT/BRC17 Block 3 Spannung				N/A
817       	ID_HEAD774			32			N/A			SMBAT/BRC17 BLOCK4  Voltage						N/A		SMBAT/BRC17 Block 4 Spannung				N/A
818       	ID_HEAD775			32			N/A			SMBAT/BRC17 BLOCK5  Voltage						N/A		SMBAT/BRC17 Block 5	Spannung			N/A
819       	ID_HEAD776			32			N/A			SMBAT/BRC17 BLOCK6  Voltage						N/A		SMBAT/BRC17 Block 6 Spannung				N/A
820       	ID_HEAD777			32			N/A			SMBAT/BRC17 BLOCK7  Voltage						N/A		SMBAT/BRC17 Block 7	Spannung			N/A
821       	ID_HEAD778			32			N/A			SMBAT/BRC17 BLOCK8  Voltage						N/A		SMBAT/BRC17 Block 8 Spannung				N/A
822       	ID_HEAD779			32			N/A			SMBAT/BRC17 BLOCK9  Voltage						N/A		SMBAT/BRC17 Block 9	Spannung			N/A
823       	ID_HEAD780			32			N/A			SMBAT/BRC17 BLOCK10 Voltage						N/A		SMBAT/BRC17 Block 10 Spannung			N/A
824       	ID_HEAD781			32			N/A			SMBAT/BRC17 BLOCK11 Voltage						N/A		SMBAT/BRC17 Block 11 Spannung				N/A
825       	ID_HEAD782			32			N/A			SMBAT/BRC17 BLOCK12 Voltage						N/A		SMBAT/BRC17 Block 12 Spannung			N/A
826       	ID_HEAD783			32			N/A			SMBAT/BRC17 BLOCK13 Voltage						N/A		SMBAT/BRC17 Block 13 Spannung				N/A
827       	ID_HEAD784			32			N/A			SMBAT/BRC17 BLOCK14 Voltage						N/A		SMBAT/BRC17 Block 14 Spannung				N/A
828       	ID_HEAD785			32			N/A			SMBAT/BRC17 BLOCK15 Voltage						N/A		SMBAT/BRC17 Block 15 Spannung				N/A
829       	ID_HEAD786			32			N/A			SMBAT/BRC17 BLOCK16 Voltage						N/A		SMBAT/BRC17 Block 16 Spannung				N/A
830       	ID_HEAD787			32			N/A			SMBAT/BRC17 BLOCK17 Voltage						N/A		SMBAT/BRC17 Block 17 Spannung				N/A
831       	ID_HEAD788			32			N/A			SMBAT/BRC17 BLOCK18 Voltage						N/A		SMBAT/BRC17 Block 18 Spannung				N/A
832       	ID_HEAD789			32			N/A			SMBAT/BRC17 BLOCK19 Voltage						N/A		SMBAT/BRC17 Block 19 Spannung				N/A
833       	ID_HEAD790			32			N/A			SMBAT/BRC17 BLOCK20 Voltage						N/A		SMBAT/BRC17 Block 20 Spannung			N/A
834       	ID_HEAD791			32			N/A			SMBAT/BRC17 BLOCK21 Voltage						N/A		SMBAT/BRC17 Block 21 Spannung				N/A
835       	ID_HEAD792			32			N/A			SMBAT/BRC17 BLOCK22 Voltage						N/A		SMBAT/BRC17 Block 22 Spannung				N/A
836       	ID_HEAD793			32			N/A			SMBAT/BRC17 BLOCK23 Voltage						N/A		SMBAT/BRC17 Block 23 Spannung				N/A
837       	ID_HEAD794			32			N/A			SMBAT/BRC17 BLOCK24 Voltage						N/A		SMBAT/BRC17 Block 24 Spannung				N/A
838       	ID_HEAD795			32			N/A			SMBAT/BRC18 BLOCK1  Voltage						N/A		SMBAT/BRC18 Block 1 Spannung 				N/A
839       	ID_HEAD796			32			N/A			SMBAT/BRC18 BLOCK2  Voltage						N/A		SMBAT/BRC18 Block 2 Spannung  				N/A
840       	ID_HEAD797			32			N/A			SMBAT/BRC18 BLOCK3  Voltage						N/A		SMBAT/BRC18 Block 3 Spannung 				N/A
841       	ID_HEAD798			32			N/A			SMBAT/BRC18 BLOCK4  Voltage						N/A		SMBAT/BRC18 Block 4 Spannung				N/A
842       	ID_HEAD799			32			N/A			SMBAT/BRC18 BLOCK5  Voltage						N/A		SMBAT/BRC18 Block 5 Spannung 				N/A
843       	ID_HEAD800			32			N/A			SMBAT/BRC18 BLOCK6  Voltage						N/A		SMBAT/BRC18 Block 6 Spannung 				N/A
844       	ID_HEAD801			32			N/A			SMBAT/BRC18 BLOCK7  Voltage						N/A		SMBAT/BRC18 Block 7 Spannung 				N/A
845       	ID_HEAD802			32			N/A			SMBAT/BRC18 BLOCK8  Voltage						N/A		SMBAT/BRC18 Block 8 Spannung 				N/A
846       	ID_HEAD803			32			N/A			SMBAT/BRC18 BLOCK9  Voltage						N/A		SMBAT/BRC18 Block 9 Spannung 				N/A
847       	ID_HEAD804			32			N/A			SMBAT/BRC18 BLOCK10 Voltage						N/A		SMBAT/BRC18 Block 10 Spannung				N/A
848       	ID_HEAD805			32			N/A			SMBAT/BRC18 BLOCK11 Voltage						N/A		SMBAT/BRC18 Block 11 Spannung				N/A
849       	ID_HEAD806			32			N/A			SMBAT/BRC18 BLOCK12 Voltage						N/A		SMBAT/BRC18 Block 12 Spannung				N/A
850       	ID_HEAD807			32			N/A			SMBAT/BRC18 BLOCK13 Voltage						N/A		SMBAT/BRC18 Block 13 Spannung				N/A
851       	ID_HEAD808			32			N/A			SMBAT/BRC18 BLOCK14 Voltage						N/A		SMBAT/BRC18 Block 14 Spannung				N/A
852       	ID_HEAD809			32			N/A			SMBAT/BRC18 BLOCK15 Voltage						N/A		SMBAT/BRC18 Block 15 Spannung				N/A
853       	ID_HEAD810			32			N/A			SMBAT/BRC18 BLOCK16 Voltage						N/A		SMBAT/BRC18 Block 16 Spannung				N/A
854       	ID_HEAD811			32			N/A			SMBAT/BRC18 BLOCK17 Voltage						N/A		SMBAT/BRC18 Block 17 Spannung				N/A
855       	ID_HEAD812			32			N/A			SMBAT/BRC18 BLOCK18 Voltage						N/A		SMBAT/BRC18 Block 18 Spannung				N/A
856       	ID_HEAD813			32			N/A			SMBAT/BRC18 BLOCK19 Voltage						N/A		SMBAT/BRC18 Block 19 Spannung				N/A
857       	ID_HEAD814			32			N/A			SMBAT/BRC18 BLOCK20 Voltage						N/A		SMBAT/BRC18 Block 20 Spannung			N/A
858       	ID_HEAD815			32			N/A			SMBAT/BRC18 BLOCK21 Voltage						N/A		SMBAT/BRC18 Block 21 Spannung				N/A
859       	ID_HEAD816			32			N/A			SMBAT/BRC18 BLOCK22 Voltage						N/A		SMBAT/BRC18 Block 22 Spannung				N/A
860       	ID_HEAD817			32			N/A			SMBAT/BRC18 BLOCK23 Voltage						N/A		SMBAT/BRC18 Block 23 Spannung				N/A
861       	ID_HEAD818			32			N/A			SMBAT/BRC18 BLOCK24 Voltage						N/A		SMBAT/BRC18 Block 24 Spannung				N/A
862       	ID_HEAD819			32			N/A			SMBAT/BRC19 BLOCK1  Voltage						N/A		SMBAT/BRC19 Block 1 Spannung 				N/A
863       	ID_HEAD820			32			N/A			SMBAT/BRC19 BLOCK2  Voltage						N/A		SMBAT/BRC19 Block 2 Spannung				N/A
864       	ID_HEAD821			32			N/A			SMBAT/BRC19 BLOCK3  Voltage						N/A		SMBAT/BRC19 Block 3 Spannung				N/A
865       	ID_HEAD822			32			N/A			SMBAT/BRC19 BLOCK4  Voltage						N/A		SMBAT/BRC19 Block 4 Spannung 				N/A
866       	ID_HEAD823			32			N/A			SMBAT/BRC19 BLOCK5  Voltage						N/A		SMBAT/BRC19 Block 5 Spannung 				N/A
867       	ID_HEAD824			32			N/A			SMBAT/BRC19 BLOCK6  Voltage						N/A		SMBAT/BRC19 Block 6 Spannung 				N/A
868       	ID_HEAD825			32			N/A			SMBAT/BRC19 BLOCK7  Voltage						N/A		SMBAT/BRC19 Block 7 Spannung				N/A
869       	ID_HEAD826			32			N/A			SMBAT/BRC19 BLOCK8  Voltage						N/A		SMBAT/BRC19 Block 8 Spannung 				N/A
870       	ID_HEAD827			32			N/A			SMBAT/BRC19 BLOCK9  Voltage						N/A		SMBAT/BRC19 Block 9 Spannung				N/A
871       	ID_HEAD828			32			N/A			SMBAT/BRC19 BLOCK10 Voltage						N/A		SMBAT/BRC19 Block 10 Spannung				N/A
872       	ID_HEAD829			32			N/A			SMBAT/BRC19 BLOCK11 Voltage						N/A		SMBAT/BRC19 Block 11 Spannung				N/A
873       	ID_HEAD830			32			N/A			SMBAT/BRC19 BLOCK12 Voltage						N/A		SMBAT/BRC19 Block 12 Spannung				N/A
874       	ID_HEAD831			32			N/A			SMBAT/BRC19 BLOCK13 Voltage						N/A		SMBAT/BRC19 Block 13 Spannung				N/A
875       	ID_HEAD832			32			N/A			SMBAT/BRC19 BLOCK14 Voltage						N/A		SMBAT/BRC19 Block 14 Spannung				N/A
876       	ID_HEAD833			32			N/A			SMBAT/BRC19 BLOCK15 Voltage						N/A		SMBAT/BRC19 Block 15 Spannung				N/A
877       	ID_HEAD834			32			N/A			SMBAT/BRC19 BLOCK16 Voltage						N/A		SMBAT/BRC19 Block 16 Spannung				N/A
878       	ID_HEAD835			32			N/A			SMBAT/BRC19 BLOCK17 Voltage						N/A		SMBAT/BRC19 Block 17 Spannung				N/A
879       	ID_HEAD836			32			N/A			SMBAT/BRC19 BLOCK18 Voltage						N/A		SMBAT/BRC19 Block 18 Spannung				N/A
880       	ID_HEAD837			32			N/A			SMBAT/BRC19 BLOCK19 Voltage						N/A		SMBAT/BRC19 Block 19 Spannung				N/A
881       	ID_HEAD838			32			N/A			SMBAT/BRC19 BLOCK20 Voltage						N/A		SMBAT/BRC19 Block 20 Spannung			N/A
882       	ID_HEAD839			32			N/A			SMBAT/BRC19 BLOCK21 Voltage						N/A		SMBAT/BRC19 Block 21 Spannung				N/A
883       	ID_HEAD840			32			N/A			SMBAT/BRC19 BLOCK22 Voltage						N/A		SMBAT/BRC19 Block 22 Spannung				N/A
884       	ID_HEAD841			32			N/A			SMBAT/BRC19 BLOCK23 Voltage						N/A		SMBAT/BRC19 Block 23 Spannung				N/A
885       	ID_HEAD842			32			N/A			SMBAT/BRC19 BLOCK24 Voltage						N/A		SMBAT/BRC19 Block 24 Spannung				N/A
886       	ID_HEAD843			32			N/A			SMBAT/BRC20 BLOCK1  Voltage						N/A		SMBAT/BRC20 Block 1 Spannung 				N/A
887       	ID_HEAD844			32			N/A			SMBAT/BRC20 BLOCK2  Voltage						N/A		SMBAT/BRC20 Block 2 Spannung 				N/A
888       	ID_HEAD845			32			N/A			SMBAT/BRC20 BLOCK3  Voltage						N/A		SMBAT/BRC20 Block 3 Spannung  				N/A
889       	ID_HEAD846			32			N/A			SMBAT/BRC20 BLOCK4  Voltage						N/A		SMBAT/BRC20 Block 4 Spannung 				N/A
890       	ID_HEAD847			32			N/A			SMBAT/BRC20 BLOCK5  Voltage						N/A		SMBAT/BRC20 Block 5 Spannung 				N/A
891       	ID_HEAD848			32			N/A			SMBAT/BRC20 BLOCK6  Voltage						N/A		SMBAT/BRC20 Block 6 Spannung  				N/A
892       	ID_HEAD849			32			N/A			SMBAT/BRC20 BLOCK7  Voltage						N/A		SMBAT/BRC20 Block 7 Spannung  				N/A
893       	ID_HEAD850			32			N/A			SMBAT/BRC20 BLOCK8  Voltage						N/A		SMBAT/BRC20 Block 8 Spannung 				N/A
894       	ID_HEAD851			32			N/A			SMBAT/BRC20 BLOCK9  Voltage						N/A		SMBAT/BRC20 Block 9 Spannung  				N/A
895       	ID_HEAD852			32			N/A			SMBAT/BRC20 BLOCK10 Voltage						N/A		SMBAT/BRC20 Block 10 Spannung 				N/A
896       	ID_HEAD853			32			N/A			SMBAT/BRC20 BLOCK11 Voltage						N/A		SMBAT/BRC20 Block 11 Spannung 			N/A
897       	ID_HEAD854			32			N/A			SMBAT/BRC20 BLOCK12 Voltage						N/A		SMBAT/BRC20 Block 12 Spannung 				N/A
898       	ID_HEAD855			32			N/A			SMBAT/BRC20 BLOCK13 Voltage						N/A		SMBAT/BRC20 Block 13 Spannung 			N/A
899       	ID_HEAD856			32			N/A			SMBAT/BRC20 BLOCK14 Voltage						N/A		SMBAT/BRC20 Block 14 Spannung 				N/A
900       	ID_HEAD857			32			N/A			SMBAT/BRC20 BLOCK15 Voltage						N/A		SMBAT/BRC20 Block 15 Spannung 				N/A
901       	ID_HEAD858			32			N/A			SMBAT/BRC20 BLOCK16 Voltage						N/A		SMBAT/BRC20 Block 16 Spannung 				N/A
902       	ID_HEAD859			32			N/A			SMBAT/BRC20 BLOCK17 Voltage						N/A		SMBAT/BRC20 Block 17 Spannung 				N/A
903       	ID_HEAD860			32			N/A			SMBAT/BRC20 BLOCK18 Voltage						N/A		SMBAT/BRC20 Block 18 Spannung 				N/A
904       	ID_HEAD861			32			N/A			SMBAT/BRC20 BLOCK19 Voltage						N/A		SMBAT/BRC20 Block 19 Spannung 			N/A
905       	ID_HEAD862			32			N/A			SMBAT/BRC20 BLOCK20 Voltage						N/A		SMBAT/BRC20 Block 20 Spannung 				N/A
906       	ID_HEAD863			32			N/A			SMBAT/BRC20 BLOCK21 Voltage						N/A		SMBAT/BRC20 Block 21 Spannung 			N/A
907       	ID_HEAD864			32			N/A			SMBAT/BRC20 BLOCK22 Voltage						N/A		SMBAT/BRC20 Block 22 Spannung 				N/A
908       	ID_HEAD865			32			N/A			SMBAT/BRC20 BLOCK23 Voltage						N/A		SMBAT/BRC20 Block 23 Spannung 			N/A
909       	ID_HEAD866			32			N/A			SMBAT/BRC20 BLOCK24 Voltage						N/A		SMBAT/BRC20 Block 24 Spannung 				N/A


[p35_status_switch.htm:Number]
3

[p35_status_switch.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE				ABBR_IN_LOCALE
1		ID_AUTO_POP		16			N/A			Auto Popup				N/A		Auto Popup				N/A
2		ID_AUTO_POP_1		16			N/A			Auto Popup				N/A		Auto Popup				N/A
3		ID_DISABLE_AUTO_POP	32			N/A			Disable Auto Popup			N/A		Deaktiviere Auto Popup			N/A

[p36_clear_data.htm:Number]
18

[p36_clear_data.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN	FULL_IN_LOCALE					ABBR_IN_LOCALE
1		ID_ERROR0		64			N/A			Failed to clear data.				N/A		Fehler, Daten nicht gelöscht!			N/A
2		ID_ERROR1		64			N/A			was cleared.					N/A		Gelöscht!					N/A
3		ID_ERROR2		64			N/A			Unknown error.					N/A		Unbekannter Fehler!				N/A
4		ID_ERROR3		128			N/A			Failure, you are not authorized.		N/A		Fehler, Sie sind nicht berechtigt!		N/A
5		ID_ERROR4		64			N/A			Failed to communicate with the controller.	N/A		Kommunikationsfehler mit dem Kontroller		N/A
6		ID_ERROR5		64			N/A			Failure, Controller is hardware protected.	N/A		Fehler: Kontroller ist HW geschützt!		N/A
7		ID_HISTORY_ALARM	64			N/A			Alarm History					N/A		Alarm Historie					N/A
8		ID_HISTORY_DATA		64			N/A			Data History					N/A		Daten Historie					N/A
9		ID_HISTORY_STATDATA	64			N/A			Data Statistics					N/A		Datenstatistiken				N/A
10		ID_HISTORY_CONTROL	64			N/A			Control Command Log				N/A		Kontrollbefehle Protokoll			N/A
11		ID_HISTORY_BATTERY	64			N/A			Battery Test Log				N/A		Batterietest Protokoll				N/A
12		ID_HISTORY_PARAM	64			N/A			Runtime Persistent Data				N/A		Ständige Laufzeitdaten				N/A
13		ID_HISTORY_SCUP_RUNNING	32			N/A			System Runtime Log				N/A		Systemlaufzeit Protokoll			N/A
14		ID_HEAD			32			N/A			Clear						N/A		Löschen						N/A
15		ID_TIPS			32			N/A			Clear						N/A		Löschen						N/A
16		ID_CLEAR		32			N/A			Clear						N/A		Löschen						N/A
17		ID_TIPS1		64			N/A			Are you sure you want to clear the data?	N/A		Sind Sie sicher die Daten löschen zu wollen?	N/A
18		ID_HISTORY_DISEL_TEST	64			N/A			Diesel Test Log					N/A		Dieseltest Protokoll				N/A

[p37_edit_config_file.htm:Number]
0

[p37_edit_config_file.htm.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE


[p38_title_config_file.htm:Number]
3

[p38_title_config_file.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_PLC			16			N/A			PLC Configuration			N/A		PLC Konfiguration	N/A
2		ID_ALARM_REG		32			N/A			ALARM_REG CFG				N/A		ALARM_REG KONFG.	N/A
3		ID_AlARM		32			N/A			Alarm Suppression			N/A		Alarmunterdrückung	N/A

[p39_edit_config_plc.htm:Number]
51

[p39_edit_config_plc.htm]
#Sequence ID	RES_ID				MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN	FULL_IN_LOCALE					ABBR_IN_LOCALE
1		ID_Operator			16			N/A			Operator					N/A		Operator					N/A
2		ID_Input1			16			N/A			Input 1						N/A		Eingang 1					N/A
3		ID_Input2			16			N/A			Input 2						N/A		Eingang 2					N/A
4		ID_Param1			16			N/A			Parameter 1					N/A		Parameter 1					N/A
5		ID_Param2			16			N/A			Parameter 2					N/A		Parameter 2					N/A
6		ID_Output			16			N/A			Output						N/A		Ausgang						N/A
7		ID_EquipName/Register1		32			N/A			Equipment Name/Register				N/A		Ausstattung Name/Register			N/A
8		ID_SignalType1			16			N/A			Signal Type					N/A		Signaltyp					N/A
9		ID_SignalName1			16			N/A			Signal Name					N/A		Signalname					N/A
10		ID_EquipName/Register2		32			N/A			Equipment Name/Register				N/A		Ausstattung Name/Register			N/A
11		ID_SignalType2			16			N/A			Signal Type					N/A		Signaltyp					N/A
12		ID_SignalName2			16			N/A			Signal Name					N/A		Signalname					N/A
13		ID_EquipName/Register3		32			N/A			Equipment Name/Register				N/A		Ausstattung Name/Register			N/A
14		ID_SignalType3			16			N/A			Signal Type					N/A		Signaltyp					N/A
15		ID_SignalName3			16			N/A			Signal Name					N/A		Signalname					N/A
16		ID_ADD				16			N/A			Add						N/A		Hinzufügen					N/A
17		ID_Delete			16			N/A			Delete						N/A		Löschen						N/A
18		ID_Sampling			16			N/A			Sampling					N/A		Probe						N/A
19		ID_Control			16			N/A			Control						N/A		Kontrolle					N/A
20		ID_Setting			16			N/A			Settings					N/A		Einstellungen					N/A
21		ID_Alarm			16			N/A			Alarm						N/A		Alarm						N/A
22		ID_ERROR5			64			N/A			PLC configuration modified error		N/A		Fehler in der PLC Änderung			N/A
23		ID_ERROR6			128			N/A			PLC configuration modified successfully,\nTo go into effect, restart the Controller!	N/A	PLC Konfiguration erfolgreich geändert.\nDen Kontroller neu starten um die Änderungen zu aktivieren!	N/A
24		ID_CONFIRM_1			64			N/A			Are you sure to delete?				N/A		Sind Sie sicher zu löschen?			N/A
25		ID_ERROR0			64			N/A			PLC configuration file error.			N/A		PLC Konfigurationsdateifehler!			N/A
26		ID_ERROR1			128			N/A			Unknow error.					N/A		Unbekannter Fehler!				N/A
27		ID_INFO1			64			N/A			SYMBOL INFORMATION				N/A		SYMBOLERKLÄRUNG					N/A
28		ID_INFO2			64			N/A			1:R, which defines a Register.			N/A		1:R, definiert ein Register.			N/A
29		ID_INFO3			128			N/A			Usage: R(Register_ID); 0 = &lt; Register_ID &lt;= 99	N/A	Gebrauch: R(Register_ID); 0 = &lt; Register_ID &lt;= 99	N/A
30		ID_INFO4			64			N/A			2:P, which defines a Parameter.			N/A		2:P, definiert einen Parameter			N/A
31		ID_INFO5			64			N/A			Usage: P(The Value)				N/A		Gebrauch: P(Wert)				N/A
32		ID_INFO6			128			N/A			3:SET, which represents SET command.		N/A		3:SET, ist der "Übernahme" Befehl		N/A
33		ID_INFO7			128			N/A			Usage: SET _ _ Parameter1 _ Output		N/A		Gebrauch: SET _ _ Parameter1 _ Ausgang		N/A
34		ID_INFO8			128			N/A			4:AND, which represents AND command.		N/A		4:AND, definiert den UND Befehl			N/A
35		ID_INFO9			128			N/A			Usage: AND Input1 Input2 _ _ Output		N/A		Gebrauch: AND Eingang1 Eingang2 _ _ Ausgang	N/A
36		ID_INFO10			128			N/A			5:OR, which represents OR command.		N/A		5:OR, definiert den ODER Befehl			N/A
37		ID_INFO11			128			N/A			Usage: OR Input1 Input2 _ _ Output		N/A		Gebrauch: OR Eingang1 Eingang2 _ _ Ausgang	N/A
38		ID_INFO12			128			N/A			6:NOT, which represents NOT command.		N/A		6:NOT, definiert den NICHT Befehl		N/A
39		ID_INFO13			128			N/A			Usage: NOT Input1 _ _ _ Output			N/A		Gebrauch: NOT Eingang1 _ _ _ Ausgang		N/A
40		ID_INFO14			128			N/A			7:XOR, which represents XOR command.		N/A		7:XOR, definiert den XOR Befehl			N/A
41		ID_INFO15			128			N/A			Usage: XOR Input1 Input2 _ _ Output		N/A		Gebrauch: XOR Eingang1 Eingang2 _ _ Ausgang	N/A
42		ID_INFO16			128			N/A			8:GT, which represents Greater Than command.	N/A		8:GT, definiert den GRÖSSER ALS Befehl		N/A
43		ID_INFO17			128			N/A			Usage: GT Input1 _ Parameter1 Parameter2 Output	N/A		Gebrauch: GT Eingang1 _ Parameter1 Parameter2 Ausgang	N/A
44		ID_INFO18			128			N/A			9:LT, which represents Less Than command.	N/A		9:LT, definiert den KLEINER ALS Befehl		N/A
45		ID_INFO19			128			N/A			Usage: LT Input1 _ Parameter1 Parameter2 Output	N/A		Gebrauch: LT Eingang1 _ Parameter1 Parameter2 Ausgang	N/A
46		ID_INFO20			128			N/A			10:DS, which represents Delay command.		N/A		10:DS, definiert die Verzögerungszeit		N/A
47		ID_INFO21			128			N/A			Usage: DS Input1 _ Parameter1 _ Output		N/A		Gebrauch: DS Eingang1 _ Parameter1 _ Ausgang	N/A
48		ID_INFO22			32			N/A			LIMITATION					N/A		EINSCHRÄNKUNG					N/A
49		ID_INFO23			256			N/A			All Output signals must be of enum type, and not an alarm signal.	N/A	Alle Ausgangssignale müssen enum Typen sein und nicht Alarmsignale	N/A
50		ID_INFO24			128			N/A			LT and GT's Input1 values must be of type Float, Unsigned or Long.	N/A	Alle LT und GT Eingang1 Werte müssen Float Typen sein, ohne Vorzeichen oder lang	N/A
51		ID_Delete2			16			N/A			Delete						N/A		Löschen						N/A




[p40_cfg_plc_Popup.htm:Number]
67

[p40_cfg_plc_Popup.htm]
#Sequence ID	RES_ID				MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_TITLE			32			N/A			PLC CONFIG			N/A		PLC KONFIG		N/A
2		ID_Operator			16			N/A			Operator			N/A		Operator		N/A
3		ID_Input1			16			N/A			Input 1				N/A		Eingang 1		N/A
4		ID_Input2			16			N/A			Input 2				N/A		Eingang 2		N/A
5		ID_Param1			16			N/A			Parameter 1			N/A		Parameter 1		N/A
6		ID_Param2			16			N/A			Parameter 2			N/A		Parameter 1		N/A
7		ID_Output			16			N/A			Output				N/A		Ausgang			N/A
8		ID_Signal1			16			N/A			Signal				N/A		Signal			N/A
9		ID_Register1			16			N/A			Register			N/A		Register		N/A
10		ID_Signal2			16			N/A			Signal				N/A		Signal			N/A
11		ID_Register2			16			N/A			Register			N/A		Register		N/A
12		ID_Signal3			16			N/A			Signal				N/A		Signal			N/A
13		ID_Register3			16			N/A			Register			N/A		Register		N/A
14		ID_EquipName/Register1		32			N/A			Equipment Name/Register		N/A		Ausstattung Name/Register	N/A
15		ID_SignalType1			16			N/A			Signal Type			N/A		Signaltyp		N/A
16		ID_SignalName1			16			N/A			Signal Name			N/A		Signalname		N/A
17		ID_EquipName/Register2		32			N/A			Equipment Name/Register		N/A		Ausstattung Name/Register	N/A
18		ID_SignalType2			16			N/A			Signal Type			N/A		Signaltyp		N/A
19		ID_SignalName2			16			N/A			Signal Name			N/A		Signalname		N/A
20		ID_EquipName/Register3		32			N/A			Equipment Name/Register		N/A		Ausstattung Name/Register	N/A
21		ID_SignalType3			16			N/A			Signal Type			N/A		Signaltyp		N/A
22		ID_SignalName3			16			N/A			Signal Name			N/A		Signalname		N/A
23		ID_Sampling1			16			N/A			Sampling			N/A		Probe			N/A
24		ID_Control1			16			N/A			Control				N/A		Kontrolle		N/A
25		ID_Setting1			16			N/A			Setting				N/A		Einstellung		N/A
26		ID_Alarm1			16			N/A			Alarm				N/A		Alarm			N/A
27		ID_Sampling2			16			N/A			Sampling			N/A		Probe			N/A
28		ID_Control2			16			N/A			Control				N/A		Kontrolle		N/A
29		ID_Setting2			16			N/A			Setting				N/A		Einstellung		N/A
30		ID_Alarm2			16			N/A			Alarm				N/A		Alarm			N/A
31		ID_Sampling3			16			N/A			Sampling			N/A		Probe			N/A
32		ID_Control3			16			N/A			Control				N/A		Kontrolle		N/A
33		ID_Setting3			16			N/A			Setting				N/A		Einstellung		N/A
34		ID_ADD				16			N/A			Add				N/A		Hinzufügen		N/A
35		ID_CANCEL			16			N/A			Cancel				N/A		Entfernen		N/A
36		ID_OUTPUT_ERROR1		32			N/A			Output error			N/A		Ausgangsfehler		N/A
37		ID_PARAM1_ERROR			32			N/A			Parameter 1 error		N/A		Fehler Parameter 1	N/A
38		ID_OUTPUT_ERROR2		32			N/A			Output error			N/A		Ausgangsfehler		N/A
39		ID_ALL_ERROR1			32			N/A			Input/Output error		N/A		Ein/Ausgangsfehler	N/A
40		ID_INPUT1_ERROR1		32			N/A			Input 1 error			N/A		Fehler Eingang 1	N/A
41		ID_INPUT2_ERROR1		32			N/A			Input 2 error			N/A		Fehler Eingang 2	N/A
42		ID_OUTPUT_ERROR3		32			N/A			Output error			N/A		Ausgangsfehler		N/A
43		ID_ALL_ERROR2			32			N/A			Input/Output error		N/A		Ein/Ausgangsfehler	N/A
44		ID_INPUT1_ERROR2		32			N/A			Input 1 error			N/A		Fehler Eingang 1	N/A
45		ID_INPUT2_ERROR2		32			N/A			Input 2 error			N/A		Fehler Eingang 2	N/A
46		ID_OUTPUT_ERROR4		32			N/A			Output error			N/A		Ausgangsfehler		N/A
47		ID_ALL_ERROR3			32			N/A			Input/Output error		N/A		Ein/Ausgangsfehler	N/A
48		ID_INPUT1_ERROR3		32			N/A			Input 1 error			N/A		Fehler Eingang 1	N/A
49		ID_OUTPUT_ERROR5		32			N/A			Output error			N/A		Ausgangsfehler		N/A
50		ID_ALL_ERROR4			32			N/A			Input/Output error		N/A		Ein/Ausgangsfehler	N/A
51		ID_INPUT1_ERROR4		32			N/A			Input 1 error			N/A		Fehler Eingang 1	N/A
52		ID_INPUT2_ERROR3		32			N/A			Input 2 error			N/A		Fehler Eingang 2	N/A
53		ID_OUTPUT_ERROR6		32			N/A			Output error			N/A		Ausgangsfehler		N/A
54		ID_ALL_ERROR5			32			N/A			Input/Output error		N/A		Ein/Ausgangsfehler	N/A
55		ID_INPUT1_ERROR5		32			N/A			Input 1 error			N/A		Fehler Eingang 1	N/A
56		ID_OUTPUT_ERROR7		32			N/A			Output error			N/A		Ausgangsfehler		N/A
57		ID_PARAM1_ERROR2		32			N/A			Parameter 1 error		N/A		Fehler Parameter 1	N/A
58		ID_PARAM2_ERROR1		32			N/A			Parameter 2 error		N/A		Fehler Parameter 2	N/A
59		ID_ALL_ERROR6			32			N/A			Input/Output error		N/A		Ein/Ausgangsfehler	N/A
60		ID_INPUT1_ERROR6		32			N/A			Input 1 error			N/A		Fehler Eingang 1	N/A
61		ID_OUTPUT_ERROR8		32			N/A			Output error			N/A		Ausgangsfehler		N/A
62		ID_PARAM1_ERROR3		32			N/A			Parameter 1 error		N/A		Fehler Parameter 1	N/A
63		ID_PARAM2_ERROR2		32			N/A			Parameter 2 error		N/A		Fehler Parameter 2	N/A
64		ID_ALL_ERROR7			32			N/A			Input/Output error		N/A		Ein/Ausgangsfehler	N/A
65		ID_INPUT1_ERROR7		32			N/A			Input 1 error			N/A		Fehler Eingang 1	N/A
66		ID_OUTPUT_ERROR9		32			N/A			Output error			N/A		Ausgangsfehler		N/A
67		ID_PARAM1_ERROR4		32			N/A			Parameter 1 error		N/A		Fehler Parameter 1	N/A


[p41_edit_config_alarmReg.htm:Number]
14


[p41_edit_config_alarmReg.htm]
1		ID_TITLE1			32			N/A			Please Select Equipment:		N/A		Bitte Gerät/Modul auswählen		N/A
2		ID_Choice			32			N/A			Please Select				N/A		Bitte wählen				N/A
3		ID_Choice2			32			N/A			Please Select				N/A		Bitte wählen				N/A
4		ID_TITLE2			32			N/A			Alarm Relay Configuration		N/A		Alarmrelais Konfiguration		N/A
5		ID_STDEQUIPNAME			32			N/A			Standard Equipment Name			N/A		Standard Geräte/Modul Name		N/A
6		ID_AlarmID			32			N/A			Alarm Signal ID				N/A		Alarmsignal ID				N/A
7		ID_AlarmName			32			N/A			Alarm Signal Name			N/A		Alarmsignal Name			N/A
8		ID_AlarmReg			32			N/A			Alarm Relay Number			N/A		Alarmrelais Nummer			N/A
9		ID_Reg				32			N/A			New Relay Number			N/A		Neue Relaisnummer			N/A
10		ID_Edit				16			N/A			Modify					N/A		Ändern					N/A
11		ID_ERROR5			64			N/A			Alarm relay modified error		N/A		Fehler bei Alarmrelais Änderung		N/A
12		ID_ERROR6			64			N/A			Alarm relay modified successfully.	N/A		Änderung Alarmrelais erfolgreich	N/A
13		ID_ERROR4			64			N/A			Failure, you are not authorized.	N/A		Fehler, Sie sind nicht berechtigt!	N/A
14		ID_Edit				16			N/A			Modify					N/A		Ändern					N/A





[p42_edit_config_alarm.htm:Number]
19

[p42_edit_config_alarm.htm]
1		ID_Edit				16			N/A			Modify							N/A		Ändern				N/A
2		ID_TITLE1			32			N/A			Please Select Equipment:				N/A		Bitte Gerät/Modul auswählen	N/A
3		ID_TITLE2			32			N/A			Alarm Suppression					N/A		Alarmunterdrückung		N/A
4		ID_AlarmID			32			N/A			Alarm Signal ID						N/A		Alarmsignal ID			N/A
5		ID_AlarmName			32			N/A			Alarm Signal Name					N/A		Alarmsignal Name		N/A
6		ID_AlarmSuppress		32			N/A			Alarm Suppressing Expression				N/A		Darstellung Alarmunterdrückung	N/A
7		ID_TITLE3			64			N/A			Number of signals in alarm suppression expression:	N/A		Anzahl Signale in der Darstellung Alarmunterdrückung:	N/A
8		ID_Choice			32			N/A			Please Select						N/A		Bitte wählen			N/A
9		ID_TITLE4			32			N/A			Alarm Suppression Expression:				N/A		Darstellung Alarmunterdrückung	N/A
10		ID_Submit			16			N/A			Submit							N/A		Übermitteln			N/A
11		ID_Cancel			16			N/A			Cancel							N/A		Abbruch				N/A
12		ID_STDEQUIPNAME			32			N/A			Standard Equipment Name					N/A		Standard Geräte/Modul Name	N/A
13		ID_Choice2			32			N/A			Please Select						N/A		Bitte wählen			N/A
14		ID_ERROR5			64			N/A			Alarm suppression expression modified error		N/A		Änderungsfehler Darstellung Alarmunterdrückung		N/A
15		ID_ERROR6			128			N/A			Alarm suppression expression modified successfully,\nto go into effect,restart the system.	N/A	Änderung Darstellung Alarmunterdrückung erfolgreich.\nDen Kontroller neu starten um die Änderungen zu aktivieren!		N/A
16		ID_OPERATOR_ERROR1		32			N/A			Operator error						N/A		Fehler Operator			N/A
17		ID_OPERATOR_ERROR2		32			N/A			Operator error						N/A		Fehler Operator			N/A
18		ID_SELF_ERROR			64			N/A			Do not suppress the signal with same signal		N/A		Signal nicht mit gleichem Signal unterdrücken	N/A
19		ID_ERROR4			64			N/A			Failure, you are not authorized.			N/A		Fehler, Sie sind nicht berechtig!		N/A




[p43_ydn_config.htm:Number]
41


[p43_ydn_config.htm]
1		ID_ERROR0		64			N/A			Success.						N/A			Erfolgreich					N/A
2		ID_ERROR1		64			N/A			Failure.						N/A			Fehler!						N/A
3		ID_ERROR2		64			N/A			Failure, YDN23 Service has exited.			N/A			Fehler, YDN23 Service beendet			N/A
4		ID_ERROR3		64			N/A			Failure, invalid parameter.				N/A			Fehler, ungültiger Parameter!			N/A
5		ID_ERROR4		64			N/A			Failure, invalid  data.					N/A			Fehler, ungültige Daten!			N/A
6		ID_ERROR5		64			N/A			Controller is hardware protected, can't be modified.	N/A			Kontroller ist HW geschützt und kann nicht verändert werden	N/A
7		ID_ERROR6		64			N/A			service is busy, cannot change configuration now.	N/A			Service ist in Gebrauch und kann jetzt nicht geändert werden	N/A
8		ID_ERROR7		64			N/A			non-shared port has already been occupied.		N/A			Nicht freigegebener Port wurde bereits belegt	N/A
9		ID_ERROR8		64			N/A			Failure, you are not authorized.			N/A			Fehler, Sie sind nicht berechtigt!		N/A
10		ID_YDN_HEAD		64			N/A			Background Protocol Configuration			N/A			Hintergrund Protokoll Konfiguration		N/A
11		ID_PROTOCOL_TYPE	32			N/A			Protocol Type						N/A			Protokoll Typ					N/A
12		ID_PROTOCOL_MEDIA	32			N/A			Port Type						N/A			Port Typ					N/A
13		ID_REPORT_IN_USER	32			N/A			Alarm Report						N/A			Alarm Bericht					N/A
14		ID_MAX_ALARM_REPORT	32			N/A			Max Alarm Report Attempts				N/A			Max Alarm Berichtsversuch			N/A
15		ID_RANGE_FROM		32			N/A			Range							N/A			Bereich						N/A
16		ID_CALL_ELAPSE_TIME	32			N/A			Call Elapse Time					N/A			Anrufverzögerungszeit				N/A
17		ID_RANGE_FROM		32			N/A			Range							N/A			Bereich						N/A
18		ID_MAIN_REPORT_PHONE	32			N/A			First Report Phone Number				N/A			Telefonnummer 1					N/A
19		ID_SECOND_REPORT_PHONE	32			N/A			Second Report Phone Number				N/A			Telefonnummer 2					N/A
20		ID_CALLBACK_PHONE	32			N/A			Third Report Phone Number				N/A			Telefonnummer 3					N/A
21		ID_COMMON_PARAM		64			N/A			Port Parameter						N/A			Port Einstellungen				N/A
22		ID_MODIFY		32			N/A			Modify							N/A			Ändern						N/A
23		ID_PROTOCOL0		16			N/A			YDN23							N/A			YDN23						N/A
24		ID_PROTOCOL1		16			N/A			RSOC							N/A			RSOC						N/A
25		ID_PROTOCOL2		16			N/A			SOC/TPE							N/A			SOC/TPE						N/A
26		ID_MEDIA0		16			N/A			RS-232							N/A			RS-232						N/A
27		ID_MEDIA1		16			N/A			Modem							N/A			Modem						N/A
28		ID_MEDIA2		16			N/A			Ethernet						N/A			Ethernet					N/A
29		ID_TIPS8		128			N/A			Max alarm report attempts is incorrect.			N/A			Max. Alarm Berichtsversuche nicht korrekt	N/A
30		ID_TIPS9		128			N/A			Max call elapse time is incorrect.			N/A			Max. Anrufverzögerungszeit nicht korrekt	N/A
31		ID_TIPS10		128			N/A			Main report phone number is incorrect.			N/A			Telefonnummer 1 ist nicht korrekt		N/A
32		ID_TIPS11		128			N/A			Second report phone number is incorrect.		N/A			Telefonnummer 2 ist nicht korrekt		N/A
33		ID_TIPS12		128			N/A			Callback report phone number is incorrect.		N/A			Rückruftelefonnummer ist nicht korrekt		N/A
34		ID_TIPS21		64			N/A			Max alarm report attempts is incorrect			N/A			Max. Alarm Berichtsversuche nicht korrekt	N/A
35		ID_TIPS22		64			N/A			Max alarm report attempts is incorrect			N/A			Max. Alarm Berichtsversuche nicht korrekt	N/A
36		ID_TIPS23		64			N/A			input error						N/A			Eingabefehler					N/A
37		ID_TIPS24		64			N/A			The port is incorrect					N/A			Falscher Port					N/A
38		ID_CCID			16			N/A			Own Address						N/A			Eigene Adresse					N/A
39		ID_RANGE_FROM		32			N/A			Range							N/A			Bereich						N/A
40		ID_TIPS6		128			N/A			Address is incorrect, enter a number please.		N/A			Falsche Adresse, bitte eine Nummer eingeben!	N/A
41		ID_NO_PROTOCOL_TIPS	128			N/A			Please enter protocol.					N/A			Bitte Protkoll eingeben!			N/A







[p47_web_title.htm:Number]
0

[p47_web_title.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[p78_get_setting_param.htm:Number]
8

[p78_get_setting_param.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN										ABBR_IN_EN	FULL_IN_LOCALE							ABBR_IN_LOCALE
1		ID_HEAD			32			N/A			Get Parameter Settings									N/A		Parameter Einstellungen abrufen					N/A
2		ID_CLOSE_ACU		32			N/A			Get Parameter Settings									N/A		Parameter Einstellungen abrufen					N/A
3		ID_ERROR0		32			N/A			Unknown error.										N/A		Unbekannter Fehler!						N/A
4		ID_ERROR1		128			N/A			Retrieve of paramater settings was successfull.						N/A		Abrufen der Parameter Einstellungen war erfolgreich		N/A
5		ID_ERROR2		64			N/A			Failed to retrieve paramater settings.							N/A		Abruf der Parameter Einstellungen fehlgeschlagen!		N/A
6		ID_ERROR3		64			N/A			You are not authorized to close the controller!.					N/A		Sie sind nicht berechtigt den Kontroller herunterzufahren	N/A
7		ID_ERROR4		64			N/A			Failed to communicate with the controller.						N/A		Kommunikationsfehler mit dem Kontroller				N/A
8		ID_TIPS			128			N/A			Through this function, user can retrive current parameter settings from the controller.	N/A		Durch diese Funktion kann der Benutzer die aktuellen Parametereinstellungen abrufen.		N/A

[p79_site_map.htm:Number]
29

[p79_site_map.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE				ABBR_IN_LOCALE
1		ID_SITE_MAP		16			N/A			Site Map				N/A		Site Map				N/A
2		ID_SITE_MAP		16			N/A			Site Map				N/A		Site Map				N/A
3		ID_MODIFY_SITE_INFO	32			N/A			Modify Site Information			N/A		Site Informationen ändern		N/A
4		ID_MODIFY_DEVICE_INFO	32			N/A			Modify Device Information		N/A		Geräte Informationen ändern		N/A
5		ID_MODIFY_ALARM_INFO	32			N/A			Modify Alarm Information		N/A		Alarm Informationen ändern		N/A
6		ID_DEVICE_EXPLORE	16			N/A			DEVICES					N/A		GERÄTE/MODULE				N/A
7		ID_SYSTEM		16			N/A			SETTINGS				N/A		EINSTELLUNGEN				N/A
8		ID_NETWORK_SETTING	32			N/A			Network Configuration			N/A		Netzwerkkonfiguration			N/A
9		ID_NMS_SETTING		16			N/A			NMS Configuration			N/A		NMS Konfiguration			N/A
10		ID_ESR_SETTING		16			N/A			MC Configuration			N/A		MC Konfiguration			N/A
11		ID_USER			64			N/A			User Information Configuration		N/A		Benutzer Informationskonfiguration	N/A
12		ID_MAINTENANCE		16			N/A			MAINTENANCE				N/A		WARTUNG					N/A
13		ID_FILE_MANAGE		32			N/A			Download				N/A		Download				N/A
14		ID_MODIFY_CFG		64			N/A			Modify Configuration Online		N/A		Konfiguration Online ändern		N/A
15		ID_TIME_CFG		64			N/A			Time Synchronization			N/A		Zeitsynchronisation			N/A
16		ID_QUERY		16			N/A			QUERY					N/A		ABFRAGE					N/A
17		ID_ALARM		32			N/A			ALARMS					N/A		ALARME					N/A
18		ID_ACTIVE_ALARM		32			N/A			Active Alarms				N/A		Aktive Alarme				N/A
19		ID_HISTORY_ALARM	32			N/A			Alarm History				N/A		ALarm Historie				N/A
20		ID_QUERY_HIS_DATA	32			N/A			Data History				N/A		Daten Historie				N/A
21		ID_QUERY_LOG_DATA	32			N/A			Data Log				N/A		Datenprotokoll				N/A
22		ID_QUERY_BATT_DATA	32			N/A			Battery Test Data			N/A		Batterietestdaten			N/A
23		ID_CLEAR_DATA		32			N/A			Clear Data				N/A		Daten löschen				N/A
24		ID_EDIT_CONFIGFILE	32			N/A			Edit Configuration File			N/A		Konfigurationsdatei ändern		N/A
25		ID_CONFIG_ALARMSUPEXP	32			N/A			Alarm Suppression Configuration		N/A		Konfiguration Alarmunterdrückung	N/A
26		ID_CONFIG_ALARMRELAY	32			N/A			Alarm Relay Configuration		N/A		Alarmrelais Konfiguration		N/A
27		ID_CONFIG_PLC		32			N/A			PLC Configuration			N/A		PLC Konfiguration			N/A
28		ID_YDN_SETTING		32			N/A			HLMS Configuration			N/A		HLMS Konfiguration			N/A
29		ID_NMSV3_SETTING	16			N/A			NMSV3 Configuration			N/A		NMSV3 Konfiguration			N/A




[p80_status_view.htm:Number]
1

[p80_status_view.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_LEAVE		8			N/A			Logout				N/A		Ausloggen			N/A

[p81_user_def_page.htm:Number]
28


[p81_user_def_page.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN	FULL_IN_LOCALE					ABBR_IN_LOCALE
1		ID_INDEX	16				N/A			Equipment Name					N/A		Geräte/Modul Name				N/A
2		ID_SIGNAL_NAME	16				N/A			Signal Name					N/A		Signalname					N/A
3		ID_SIGNAL_VALUE	16				N/A			Value						N/A		Wert						N/A
4		ID_SIGNAL_UNIT	16				N/A			Unit						N/A		Einheit						N/A
5		ID_SAMPLE_TIME	16				N/A			Time						N/A		Zeit						N/A
6		ID_SET_VALUE	16				N/A			Set Value					N/A		Neuer Wert					N/A
7		ID_SET		16				N/A			Set						N/A		Setzen						N/A
8		ID_ERROR0	32				N/A			Failure.					N/A		Fehler!						N/A
9		ID_ERROR1	32				N/A			Success.					N/A		Erfolgreich					N/A
10		ID_ERROR2	64				N/A			Failure, conflict in settings.			N/A		Fehler, Konflikt in den Einstellungen		N/A
11		ID_ERROR3	32				N/A			Failure, you are not authorized.		N/A		Fehler, Sie sind nicht berechtigt!		N/A
12		ID_ERROR4	64				N/A			No information to send.				N/A		Keine Informationen zum Senden			N/A
13		ID_ERROR5	128				N/A			Failure, Controller is hardware protected	N/A		Fehler, Kontroller ist HW geschützt!		N/A
14		ID_SET_TYPE	16				N/A			Set						N/A		Setzen						N/A
15		ID_SHOW_TIPS0	64				N/A			Greater than the maximum value:			N/A		Größer als der Maximalwert			N/A
16		ID_SHOW_TIPS1	64				N/A			Less than the minimum value:			N/A		Kleiner als der Minimalwert			N/A
17		ID_SHOW_TIPS2	64				N/A			Can't be empty.					N/A		Kann nicht leer sein				N/A
18		ID_SHOW_TIPS3	64				N/A			Enter a number please.				N/A		Bitte eine Nummer eingeben			N/A
19		ID_SHOW_TIPS4	64				N/A			The control value is equal to the last value.	N/A		Der Wert ist gleich dem letzten Wert		N/A
20		ID_SHOW_TIPS5	64				N/A			Failure, you are not authorized.		N/A		Fehler, Sie sind nicht berechtigt!		N/A
21		ID_TIPS1	64				N/A			Send modifications				N/A		Änderungen senden				N/A
22		ID_SAMPLER	16				N/A			Status						N/A		Status						N/A
23		ID_CHANNEL	16				N/A			Channel						N/A		Kanal						N/A
24		ID_MONTH_ERROR	32				N/A			Incorrect month.				N/A		Falscher Monat					N/A
25		ID_DAY_ERROR	32				N/A			Incorrect day.					N/A		Falscher Tag					N/A
26		ID_HOUR_ERROR	32				N/A			Incorrect hour.					N/A		Falsche Stunde					N/A
27		ID_FORMAT_ERROR	64				N/A			Incorrect format.				N/A		Falsches Format					N/A
28		ID_MENU_PAGE	32				N/A			/cgi-bin/eng/					N/A		/cgi-bin/loc/					N/A



[alai_tree.js:Number]
0

[alai_tree.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE


[j09_alai_tree_help.js:Number]
0

[j09_alai_tree_help.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE


[p44_cfg_powersplite.htm:Number]
36

[p44_cfg_powersplite.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN	FULL_IN_LOCALE			ABBR_IN_LOCALE
1		ID_Master		32		N/A			Master				N/A		Master				N/A
2		ID_Slave		32		N/A			Slave				N/A		Slave				N/A
3		ID_CFGMode		32		N/A			Modify				N/A		Ändern				N/A
4		ID_TITLE2		32		N/A			PowerSplit Configuration	N/A		PowerSplit Konfiguration	N/A
5		ID_PSSigName		32		N/A			Signal Name			N/A		Signalname			N/A
6		ID_EquipName		32		N/A			Equipment Name			N/A		Geräte/Modul Name		N/A
7		ID_SigType		32		N/A			Signal Type			N/A		Signaltyp			N/A
8		ID_SigName		32		N/A			Signal Name			N/A		Signalname			N/A
9		ID_TITLE3		32		N/A			PowerSplit Configuration	N/A		PowerSplit Konfiguration	N/A
10		ID_PSSigName1		32		N/A			Signal Name			N/A		Signalname			N/A
11		ID_EquipName1		32		N/A			Equipment Name			N/A		Geräte/Modul Name		N/A
12		ID_SigType1		32		N/A			Signal Type			N/A		Signaltyp			N/A
13		ID_SigName1		32		N/A			Signal Name			N/A		Signalname			N/A
14		ID_CFGSubmit		32		N/A			Submit				N/A		Übermitteln			N/A
15		ID_CFGCancel		32		N/A			Cancel				N/A		Abbruch				N/A
16		ID_PS_MODE		32		N/A			PowerSplit Mode			N/A		PowerSplit Modus		N/A
17		ID_EQUIP_ERROR		32		N/A			Equipment Name Error		N/A		Fehler Geräte/Modul Name	N/A
18		ID_TYPE_ERROR		32		N/A			Signal Type Error		N/A		Fehler Signaltyp		N/A
19		ID_SIG_ERROR		32		N/A			Signal Name Error		N/A		Fehler Signalname		N/A
20		ID_Sampling		32		N/A			Sampling			N/A		Probe				N/A
21		ID_Control		32		N/A			Control				N/A		Kontrolle			N/A
22		ID_Setting		32		N/A			Setting				N/A		Einstellungen			N/A
23		ID_Sampling1		32		N/A			Sampling			N/A		Probe				N/A
24		ID_Control1		32		N/A			Control				N/A		Kontrolle			N/A
25		ID_Setting1		32		N/A			Setting				N/A		Einstellungen			N/A
26		ID_Alarm		32		N/A			Alarm				N/A		Alarm				N/A
27		ID_Alarm1		32		N/A			Alarm				N/A		Alarm				N/A
28		ID_EDIT			32		N/A			Edit				N/A		Ändern				N/A
29		ID_ERROR5		32		N/A			PowerSplit modified error	N/A		Fehler PowerSplit Änderung	N/A
30		ID_ERROR6		128		N/A			PowerSplit modified successfully.\nTo go into effect, restart the system.	N/A	PowerSplit erfolgreich geändert.\nDamit die Einstellungen aktiviert werden, starten Sie das System neu!	N/A
31		ID_ERROR0		32		N/A			Configuration file error.	N/A		Fehler Konfigurationsdatei!	N/A
32		ID_ERROR1		32		N/A			Unknown error.			N/A		Unbekannter Fehler!		N/A
33		ID_EXPLAIN		128		N/A			Other PowerSplit setting signals can be found here.	N/A	Anderen PowerSplit Signaleinstellungen können hier eingesehen werden.	N/A
34		ID_DIR			32		N/A			./eng				N/A		./loc				N/A
35		ID_CONFIRM_1		64		N/A			Are you sure?			N/A		Sind Sie sicher			N/A
36		ID_SYSTEM		32		N/A			Power System			N/A		DC-System			N/A
37		ID_NO_SIGNAL_ALERT	64		N/A			No such signal.			N/A		Kein solches Signal		N/A

[p77_auto_config.htm:Number]
9

[p77_auto_config.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN							ABBR_IN_EN	FULL_IN_LOCALE			ABBR_IN_LOCALE
1		ID_HEAD			32			N/A			Auto Configuration						N/A		Auto Konfiguration		N/A
2		ID_CLOSE_ACU		32			N/A			Auto Configuration						N/A		Auto Konfiguration		N/A
3		ID_ERROR0		32			N/A			Unknown error.							N/A		Unbekannter Fehler!		N/A
4		ID_ERROR1		64			N/A			Auto Config has started. Please wait a while.			N/A		Auto Konfiguration gestartet. Bitte warten Sie...!		N/A
5		ID_ERROR2		64			N/A			Failed to retreive.						N/A		Fehler beim Abrufen		N/A
6		ID_ERROR3		64			N/A			You do not have authority to stop the controller.		N/A		Sie haben nicht die Berechtigung den Kontroller zu stoppen!			N/A
7		ID_ERROR4		64			N/A			Failed to communicate with the controller.			N/A		Kommunikationsfehler mit dem Kontroller		N/A
8		ID_AUTO_CONFIG		128			N/A			The controller will run auto configuration, and web browser will be closed. Please wait a moment(About 2-5 minute).	N/A	Auto Konfiguration aktiv. Der WebBrowser wird geschlossen. Bitte warten Sie (ca. 2-5 Minuten)!		N/A
9		ID_TIPS			64			N/A			Through this function, you can run the auto configuration.	N/A		Durch diese Funktion wird Auto Konfiguration gestartet		N/A

[copyright.htm:Number]
0

[copyright.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE


[global.css:Number]
0

[global.css]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE


[header.htm:Number]
3

[header.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_SITE		16			N/A			Site		N/A		Site			N/A
2		ID_LOGIN	16			N/A			./eng/		N/A		./loc/			N/A
3		ID_LOGOUT	16			N/A			LOGOUT		N/A		Ausloggen		N/A


[p01_home.htm:Number]
0

[p01_home.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE


[p01_home_index.htm:Number]
12

[p01_home_index.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_EVENT_STATUS		32			N/A			Event Status		N/A		Ereignis-Status		N/A
2		ID_OUT_VOLTAGE		32			N/A			Output Voltage		N/A		Ausgangsspannung	N/A
3		ID_OUT_CURRENT		32			N/A			Output Current		N/A		Ausgangsstrom		N/A
4		ID_BATT_STATUS		32			N/A			Battery Status		N/A		Batteriestatus		N/A
5		ID_AMB_TEMP		32			N/A			Ambient Temp		N/A		Umgebungstemperatur	N/A
6		ID_LOAD_TREND		32			N/A			Load Trend		N/A		Verbrauchtrend		N/A
7		ID_TIME			32			N/A			Time			N/A		Zeit			N/A
8		ID_PEAK_CURRENT		32			N/A			Peak Current		N/A		Spitzenstrom		N/A
9		ID_AVERAGE_CURRENT	34			N/A			Daily Peak Average	N/A		Täglicher Topstrom (Durchschnitt)	N/A
10		ID_R			32			N/A			A			N/A		A			N/A
11		ID_S			32			N/A			B			N/A		B			N/A
12		ID_T			32			N/A			C			N/A		C			N/A


[p01_home_title.htm:Number]
1

[p01_home_title.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_SYS_STATUS	32			N/A			System Status	N/A		Systemstatus		N/A


[p_main_menu.html:Number]
0

[p_main_menu.html]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[excanvas.js:Number]
0

[excanvas.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[jquery.emsMeter.js:Number]
0

[jquery.emsMeter.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[jquery.emsplot.js:Number]
1

[jquery.emsplot.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_NO_DATA	64			N/A			There is no data to show!	N/A		Keine Daten zum Anzeigen		N/A

[jquery.emsplot.style.css:Number]
0

[jquery.emsplot.style.css]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[jquery.emsThermometer.js:Number]
0

[jquery.emsThermometer.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[jquery.min.js:Number]
0

[jquery.min.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[line_data.htm:Number]
1

[line_data.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_NO_DATA	64			N/A			There is no data to show!	N/A		Keine Daten zum Anzeigen		N/A
[p12_nmsv3_config.htm:Number]
50

[p12_nmsv3_config.htm]
#Sequence ID	RES_ID				MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN											ABBR_IN_EN		FULL_IN_LOCALE					ABBR_IN_LOCALE
1		ID_ERROR0			16			N/A			Unknown error.											N/A			Unbekannter Fehler					N/A
2		ID_ERROR1			64			N/A			Failed. The NMS already exists.									N/A			Fehler! NMS besteht bereits.			N/A
3		ID_ERROR2			32			N/A			Successful											N/A			Erfolgreich.						N/A
4		ID_ERROR3			64			N/A			Failed. Incomplete information.									N/A		Fehler! Unvollständige Eingabe.				N/A
5		ID_ERROR4			64			N/A			Failed. No authority.									   	N/A			Fehler! Keine Berechtigung.					N/A
6		ID_ERROR5			64			N/A			Cannot be modified. Controller is hardware protected.						N/A			Kann nicht geändert werden. Kontroller ist Hardware geschützt.		N/A
7		ID_ERROR6			64			N/A			Failed. Maximum number exceeded.								N/A			Fehler! Maximale Anzahl überschritten.				N/A
8		ID_NMS_HEAD1			32			N/A			NMSV3 Configuration										N/A			NMSV3 Konfiguration						N/A
9		ID_NMS_HEAD2			32			N/A			Current NMS											N/A			Aktuelle NMS						N/A
10		ID_NMS_IP			16			N/A			NMS IP												N/A			NMS IP						N/A
11		ID_NMS_AUTHORITY		16			N/A			Authority											N/A			Berechtigung						N/A
12		ID_NMS_TRAP			32			N/A			Accepted Trap Level										N/A			Akzeptierter Trap Level					N/A
13		ID_NMS_IP			16			N/A			NMS IP												N/A			NMS IP						N/A
14		ID_NMS_AUTHORITY		16			N/A			Authority											N/A			Berechtigung						N/A
15		ID_NMS_TRAP			32			N/A			Accepted Trap Level										N/A			Akzeptierter Trap Level					N/A
16		ID_NMS_ADD			16			N/A			Add New NMS											N/A			Hinzufügen neue NMS						N/A
17		ID_NMS_MODIFY			32			N/A			Modify NMS											N/A			Ändern NMS						N/A
18		ID_NMS_DELETE			32			N/A			Delete NMS											N/A			Löschen NMS						N/A
19		ID_NMS_PUBLIC			32			N/A			Public Community										N/A			Public Community					N/A
20		ID_NMS_PRIVATE			32			N/A			Private Community										N/A			Private Community					N/A
21		ID_NMS_LEVEL0			16			N/A			Not Used											N/A			Nicht benutzt						N/A
22		ID_NMS_LEVEL1			16			N/A			No Access											N/A			Kein Zugriff					N/A
23		ID_NMS_LEVEL2			32			N/A			Query Authority											N/A		Anzeigeberechtigung					N/A
24		ID_NMS_LEVEL3			32			N/A			Control Authority										N/A			Kontrollberechtigung					N/A
25		ID_NMS_LEVEL4			32			N/A			Administrator											N/A			Administrator					N/A
26		ID_NMS_TRAP_LEVEL0		16			N/A			NoAuthNoPriv											N/A			NoAuthNoPriv						N/A
27		ID_NMS_TRAP_LEVEL1		16			N/A			AuthNoPriv											N/A			AuthNoPriv					N/A
28		ID_NMS_TRAP_LEVEL2		16			N/A			AuthPriv											N/A			AuthPriv					N/A
29		ID_NMS_TRAP_LEVEL3		16			N/A			Critical Alarms											N/A			Kritische Alarme					N/A
30		ID_NMS_TRAP_LEVEL4		16			N/A			No Trap											N/A			Kein Trap					N/A
31		ID_TIPS0			128			N/A			Incorrect IP address of NMS. \nShould be in format 'nnn.nnn.nnn.nnn'. \nExample 10.76.8.29	N/A			Nicht korrekte IP Adresse des NMS. \nMuss im Format 'nnn.nnn.nnn.nnn'. \nBeispiel 
32		ID_TIPS1			128			N/A			Priv Password DES or Auth Password MD5 cannot be empty. Please try again.			N/A			Priv Passwort DES oder Auth Passwort MD5 darf nicht leer sein. Bitte nochmals versuchen.		N/A
33		ID_TIPS2			128			N/A			Already exists. Please try again.								N/A			Besteht bereits. Bitte nochmals versuchen.			N/A
34		ID_TIPS3			128			N/A			Does not exist. Cannot be modified. Please try again.					N/A			Existiert nicht. Kann nicht geändert werden. Bitte nochmals versuchen.			N/A
35		ID_TIPS4			128			N/A			Please select one or more NMS before clicking this button.					N/A			Bitte wählen sie eine oder mehrere NMS bevor sie diesen Knopf betätigen.					N/A
36		ID_TIPS5			128			N/A			NMS Info Configuration										N/A			NMS Konfigurationsinformation				N/A
41		ID_TIPS6			128			N/A			User name can't be null										N/A			Benutzername kann nicht Null sein					N/A
42		ID_NMS_USERNAME			128			N/A			User Name											N/A			Benutzername					N/A
43		ID_NMS_TRAP_IP			128			N/A			Trap IP Address											N/A			Trap IP Adresse					N/A
44		ID_NMS_TRAP_LEVEL		128			N/A			Trap Security Level										N/A			Trap Security Level					N/A
45		ID_NMS_DES			128			N/A			Priv Password DES										N/A			Priv Passwort DES				N/A
46		ID_NMS_MD5			128			N/A			Auth Password MD5										N/A			Auth Passwort MD5					N/A
47		ID_NMS_ENGINID			128			N/A			Trap Engine ID											N/A		Trap Engine ID					N/A
48		ID_NMS_USERNAME_DISPLAY		128			N/A			User Name											N/A			Benutzername					N/A
49		ID_TRAP_IP_DISPLAY		128			N/A			Trap IP Address											N/A			Trap IP Adresse					N/A
50		ID_NMS_DES_DISPLAY		128			N/A			Priv Password DES										N/A			Priv Passwort DES				N/A
51		ID_NMS_MD5_DISPLAY		128			N/A			Auth Password MD5										N/A			Auth Passwort MD5					N/A
52		ID_NMS_TRAP_ENGINE		128			N/A			Trap Engine ID											N/A			Trap Engine ID					N/A
53		ID_NMS_TRAP_LEVE_DISPLAY	128			N/A			Trap Security Level										N/A		Trap Security Level					N/A
54		ID_ERROR7			128			N/A			Do not support SNMPV3										N/A			SNMP v.3 wird nicht unterstützt					N/A

