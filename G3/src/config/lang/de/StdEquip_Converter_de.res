﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Converter				Converter		Converter				Converter
2		32			15			Output Voltage				Output Voltage		Ausgangsspannung			Ausg.Spannung
3		32			15			Actual Current				Actual Current		Ausgangsstrom				Ausgangsstrom
4		32			15			Temperature				Temperature		Temperatur				Temperatur
5		32			15			Converter High SN			Conv High SN		SNR hoch Converter			SNR hoch Conv
6		32			15			Converter SN				Conv SN			SNR Converter				SNR Conv
7		32			15			Total Running Time			Total Run Time		Gesamte Laufzeit			Ges. Laufzeit
8		32			15			Converter ID Overlap			Conv ID Overlap		Converter ID vorhanden			CV ID vorhanden
9		32			15			Converter Identification Status		Conv Id Status		Converter ID.Status			CV ID.Status
10		32			15			Fan Full Speed Status			Fan Full Speed		Lüfter Max. Drehzahl			Fan Max.Drehz.
11		32			15			EEPROM Failure Status			EEPROM Failure		EEPROM Fehler Status			EEPROM F.-Stat.
12		32			15			Thermal Shutdown Status			Therm Shutdown		Übertemp.-Status			Übertemp.Stat.
13		32			15			Input Low Voltage Status		Input Low Volt		Eingangsspannung niedrig		U Eing. niedrig
14		32			15			High Ambient Temperature Status		High Amb Temp		Umgebungstemp. zu hoch			Umg.temp.z.hoch
15		32			15			WALK-In Enabled Status			WALK-In Enabled		WALK-In Status aktiviert		WALK-In aktiv
16		32			15			On/Off Status				On/Off			Ein/Aus Status				EIN/AUS
17		32			15			Stopped Status				Stopped			Stopp Status				STOPP Status
18		32			15			Power Limited for Temperature		PowerLim(temp)		Paus. Begrenzung Temp.			Paus.Begrenzung
19		32			15			Over Voltage Status(DC)			Over Volt(DC)		Überspannung (DC)			Überspann.(DC)
20		32			15			Fan Failure Status			Fan Failure		Lüfterfehler				Lüfterfehler
21		32			15			Converter Failure Status		Conv Fail		Converterfehler				Conv Fehler
22		32			15			Barcode 1				Barcode 1		Barcode 1				Barcode 1
23		32			15			Barcode 2				Barcode 2		Barcode 2				Barcode 2
24		32			15			Barcode 3				Barcode 3		Barcode 3				Barcode 3
25		32			15			Barcode 4				Barcode 4		Barcode 4				Barcode 4
26		32			15			Emergency Stop/Shutdown Status		EmStop/Shutdown		NOTAUS Status				NOTAUS Status
27		32			15			Communication Status			Comm Status		Kommunikationsstatus			Komm.-Status
28		32			15			Existence State				Existence State		Modul Erfassung				Erfassung
29		32			15			DC On/Off Control			DC On/Off Ctrl		DC Ein/Aus Kontrolle			DC Ein/Aus
30		32			15			Over Volt Reset				Over Volt Reset		Überspannungsreset			Über V reset
31		32			15			LED Control				LED Control		LED Kontrolle				LED Kontrolle
32		32			15			Converter Reset				Conv Reset		Converter Neustart			Conv Neustart
33		32			15			AC Input Failure			AC Failure		AC Fehler				AC Fehler
34		32			15			Over Voltage				Over Voltage		Überspannung				Überspannung
37		32			15			Current Limit				Current Limit		Strombegrenzung				Strombegrenzung
39		32			15			Normal					Normal			Normal					Normal
40		32			15			Limited					Limited			Begrenzt				Begrenzt
45		32			15			Normal					Normal			Normal					Normal
46		32			15			Full					Full			n.begrenzt				n.begrenzt
47		32			15			Disabled				Disabled		Deaktiviert				Deaktiviert
48		32			15			Enabled					Enabled			Aktiviert				Aktiviert
49		32			15			On					On			Ein					Ein
50		32			15			Off					Off			Aus					Aus
51		32			15			Normal					Normal			Normal					Normal
52		32			15			Failure					Failure			Fehler					Fehler
53		32			15			Normal					Normal			Normal					Normal
54		32			15			Over Temperature			Over Temp		Übertemperatur				Übertemp.
55		32			15			Normal					Normal			Normal					Normal
56		32			15			Fault					Fault			Fehler					Fehler
57		32			15			Normal					Normal			Normal					Normal
58		32			15			Protected				Protected		Geschützt				Geschützt
59		32			15			Normal					Normal			Normal					Normal
60		32			15			Failure					Failure			Fehler					Fehler
61		32			15			Normal					Normal			Normal					Normal
62		32			15			Alarm					Alarm			Alarm					Alarm
63		32			15			Normal					Normal			Normal					Normal
64		32			15			Failure					Failure			Fehler					Fehler
65		32			15			Off					Off			Aus					Aus
66		32			15			On					On			Ein					Ein
67		32			15			Off					Off			Aus					Aus
68		32			15			On					On			Ein					Ein
69		32			15			Flash					Flash			Blinken					Blinken
70		32			15			Cancel					Cancel			Abstellen				Abstellen
71		32			15			Off					Off			Aus					Aus
72		32			15			Reset					Reset			Rücksetzen				Rücksetzen
73		32			15			Open Converter				On			Converter Ein				Conv Ein
74		32			15			Close Converter				Off			Converter Aus				Conv Aus
75		32			15			Off					Off			Aus					Aus
76		32			15			LED Control				Flash			LED Kontrolle				Blinken
77		32			15			Converter Reset				Conv Reset		Converter Reset				Conv Reset
80		32			15			Communication Failure			Comm Failure		Kommunikationsfehler			Komm.-fehler
84		32			15			Converter High SN			Conv High SN		SNR hoch Converter			SNR CV hoch
85		32			15			Converter Version			Conv Version		Converterversion			Conv Version
86		32			15			Converter Part Number			Conv Part NO.		Converter Art.-Nummer			Conv Art.-Nr.
87		32			15			Sharing Current State			Sharing Curr		Stromverteilung Status			Stromverteilung
88		32			15			Sharing Current Alarm			SharingCurr Alm		Stromverteilung Alarm			Stromvert.Alarm
89		32			15			HVSD Alarm				HVSD Alarm		Übersp.-Abschaltung			Übersp.Abschal
90		32			15			Normal					Normal			Normal					Normal
91		32			15			Over Voltage				Over Voltage		Überspannung				Überspannung
92		32			15			Line AB Voltage				Line AB Volt		Phasenspannung L1-L2			V Phase L1-L2
93		32			15			Line BC Voltage				Line BC Volt		Phasenspannung L2-L3			V Phase L2-L3
94		32			15			Line CA Voltage				Line CA Volt		Phasenspannung L3-L1			V Phase L3-L1
95		32			15			Low Voltage				Low Voltage		Unterspannung				Unterspannung
96		32			15			AC Under Voltage Protection		U-Volt Protect		AC-Unterspannungsschutz			U-Volt Schutz
97		32			15			Converter Position			Converter Pos		Converterposition			Conv Position
98		32			15			DC Output Shut Off			DC Output Off		DC-Ausgang Aus				DC-Ausg. Aus
99		32			15			Converter Phase				Conv Phase		Converter Phase				Conv Phase
100		32			15			A					A			L1					L1
101		32			15			B					B			L2					L2
102		32			15			C					C			L3					L3
103		32			15			Severe Sharing CurrAlarm		SevereSharCurr		Stromverteilung Alarm			Stromvert.Alarm
104		32			15			Barcode 1				Barcode 1		Barcode 1				Barcode 1
105		32			15			Barcode 2				Barcode 2		Barcode 2				Barcode 2
106		32			15			Barcode 3				Barcode 3		Barcode 3				Barcode 3
107		32			15			Barcode 4				Barcode 4		Barcode 4				Barcode 4
108		32			15			Converter Failure			Conv Failure		Converterfehler				Conv Fehler
109		32			15			No					No			Nein					Nein
110		32			15			Yes					Yes			Ja					Ja
111		32			15			Existence State				Existence ST		Modul Erfassung				Erfassung
112		32			15			Converter Failure			Converter Fail		Converterfehler				Conv Fehler
113		32			15			Comm OK					Comm OK			Kommunikation O.K.			Komm. O.K.
114		32			15			All Not Responding			All Not Resp		Keine Kommunikation			Keine Kommunik.
115		32			15			Not Responding				Not Responding		Keine Antwort				Keine Antwort
116		32			15			Valid Rated Current			Rated Current		Nennstrom				Nennstrom
117		32			15			Efficiency				Efficiency		Wirkungsgrad				Wirkungsgrad
118		32			15			Input Rated Voltage			Input RatedVolt		Nenneingangsspannung			Nenn.Eing.Span.
119		32			15			Output Rated Voltage			OutputRatedVolt		Nennausgangsspannung			Nenn.Ausg.Span.
120		32			15			LT 93					LT 93			Kleiner als 93				Kleiner93
121		32			15			GT 93					GT 93			Größer als93				Grösser93
122		32			15			GT 95					GT 95			Größer als95				Grösser95
123		32			15			GT 96					GT 96			Größer als96				Grösser96
124		32			15			GT 97					GT 97			Größer als97				Grösser97
125		32			15			GT 98					GT 98			Größer als98				Grösser98
126		32			15			GT 99					GT 99			Größer als99				Grösser99
276		32			15			Emergency Stop/Shutdown			EmStop/Shutdown		NOTAUS Funktion				NOTAUS Funkt.
277		32			15			Fan Failure				Fan Failure		Lüfterfehler				Lüfterfehler
278		32			15			Low Input Voltage			Low Input Volt		Niedrige Eingangsspannung		niedrig U ein
279		32			15			Set Converter ID			Set Conv ID		Setzte Wechselrichter ID		Setze WR ID
280		32			15			EEPROM Fail				EEPROM Fail		EEPROM Fehler				EEPROM Fehler
281		32			15			Thermal Shutdown			Thermal SD		Übertemperaturabschaltung		T Abschaltung
282		32			15			High Temperature			High Temp		Hohe Temperatur				Hohe Temperatur
283		32			15			Thermal Power Limit			Therm Power Lmt		Übertemperaturbegrenzung		T Begrenzung
284		32			15			Fan Fail				Fan Fail		Lüfterfehler				Lüfterfehler
285		32			15			Converter Fail				Converter Fail		Converterfehler				Conv Fehler
286		32			15			Mod ID Overlap				Mod ID Overlap		Modul ID vorhanden			doppelte ID
287		32			15			Low Input Volt				Low Input Volt		Niedrige Eingangsspannung		Niedr. U ein
288		32			15			Undervoltage				Undervoltage		Unterspannung				Unterspannung
289		32			15			Overvoltage				Overvoltage		Überspannung				Überspannung
290		32			15			Overcurrent				Overcurrent		Überstrom				Überstrom
291		32			15			GT 94					GT 94			Grösser94				Grösser94
292		32			15			Under Voltage(24V)			Under Volt(24V)		Unterspannung (24V)			Unterspan.
293		32			15			Over Voltage(24V)			Over Volt(24V)		Überspannung (24V)			Überspan.
294		32			15			Input Voltage			Input Voltage		Eingangsspannung			Eingangsspannung