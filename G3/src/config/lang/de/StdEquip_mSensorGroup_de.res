﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			mSensor Group		mSensor Group					mSensor-Gruppe		mSensor-Gruppe				
																					
4		32			15			Normal				Normal							Normal				Normal						
5		32			15			Fail				Fail							Ausfallen			Ausfallen						
6		32			15			Yes					Yes								Ja				Ja							
7		32			15			Existence State		Existence State					Existenzstaat		Existenzstaat				
8		32			15			Existent			Existent						Existieren			Existieren					
9		32			15			Not Existent		Not Existent					Nicht Existent		Nicht Existent				
10		32			15			Number of mSensor	Num of mSensor					Anzahl mSensor		Anzahl mSensor				
11		32			15			All mSensor Comm Fail			AllmSenCommFail		Alle mSensor-Comm-Fehler		AllemSenComFehl	
12		32			15			Interval of Impedance Test		ImpedTestInterv		Intervall von Impedanztest		IntervVonImpeda	
13		32			15			Hour of Impedance Test			ImpedTestHour		Test Stunde Impedanz			TestStundeImped	
14		32			15			Time of Last Impedance Test		LastTestTime		Zeit der letzten Impedanztest	Letzte Testzeit	
