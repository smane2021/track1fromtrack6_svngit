﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			EIB Battery				EIB Battery		EIB Batterie				EIB Batterie
2		32			15			Battery Current				Batt Current		Batteriestrom				I Batterie
3		32			15			Battery Voltage				Batt Voltage		Batteriespannung			U Batterie
4		32			15			Battery Capacity (Ah)			Batt Cap(Ah)		Batteriekapazität (Ah)			Batt.Kapaz.(Ah)
5		32			15			Battery Capacity (%)			Batt Cap(%)		Batteriekapazität (%)			Batt.Kapaz.(%)
6		32			15			Current Limit Exceeded			Curr Lmt Exceed		Strombegr.überschritten			I Begr.übersch
7		32			15			Over Battery Current			Over Current		Überstrom				Überstrom
8		32			15			Low Capacity				Low Capacity		Kapazität niedrig			niedrige Kapaz.
9		32			15			Yes					Yes			Ja					Ja
10		32			15			No					No			Nein					Nein
26		32			15			State					State			Status					Status
27		32			15			Battery Block 1 Voltage			Batt B1 Volt		Spann.Batterieblock 1			U Batt.block 1
28		32			15			Battery Block 2 Voltage			Batt B2 Volt		Spann.Batterieblock 2			U Batt.block 2
29		32			15			Battery Block 3 Voltage			Batt B3 Volt		Spann.Batterieblock 3			U Batt.block 3
30		32			15			Battery Block 4 Voltage			Batt B4 Volt		Spann.Batterieblock 4			U Batt.block 4
31		32			15			Battery Block 5 Voltage			Batt B5 Volt		Spann.Batterieblock 5			U Batt.block 5
32		32			15			Battery Block 6 Voltage			Batt B6 Volt		Spann.Batterieblock 6			U Batt.block 6
33		32			15			Battery Block 7 Voltage			Batt B7 Volt		Spann.Batterieblock 7			U Batt.block 7
34		32			15			Battery Block 8 Voltage			Batt B8 Volt		Spann.Batterieblock 8			U Batt.block 8
35		32			15			Battery Management			Batt Manage		Batteriemanagement			Batt.Management
36		32			15			Enable					Enable			Ja					Ja
37		32			15			Disable					Disable			Nein					Nein
38		32			15			Failure					Failure			Fehler					Fehler
39		32			15			Shunt Full Current			Shunt Current		Nennstrom Shunt				I Nenn Shunt
40		32			15			Shunt Full Voltage			Shunt Voltage		Nennspannung Shunt			U Nenn Shunt
41		32			15			On					On			An					An
42		32			15			Off					Off			Aus					Aus
43		32			15			Failure					Failure			Fehler					Fehler
44		32			15			Used Temperature Sensor			Used Sensor		Angeschl.Temp.Sensor			Temp.Sensor
87		32			15			None					None			Keiner					Keiner
91		32			15			Temperature Sensor 1			Temp Sensor 1		Temperatur Sensor 1			Temp.Sensor 1
92		32			15			Temperature Sensor 2			Temp Sensor 2		Temperatur Sensor 2			Temp.Sensor 2
93		32			15			Temperature Sensor 3			Temp Sensor 3		Temperatur Sensor 3			Temp.Sensor 3
94		32			15			Temperature Sensor 4			Temp Sensor 4		Temperatur Sensor 4			Temp.Sensor 4
95		32			15			Temperature Sensor 5			Temp Sensor 5		Temperatur Sensor 5			Temp.Sensor 5
96		32			15			Rated Capacity				Rated Capacity		Nennkapazität C10			Nennkapaz. C10
97		32			15			Battery Temperature			Battery Temp		Batterietemperatur			Batt.-Temp.
98		32			15			Battery Temperature Sensor		BattTempSensor		Batterietemp.Sensor			Batt.Temp.Sens.
99		32			15			None					None			Keiner					Keiner
100		32			15			Temperature 1				Temp 1			Temperatur 1				Temp. 1
101		32			15			Temperature 2				Temp 2			Temperatur 2				Temp. 2
102		32			15			Temperature 3				Temp 3			Temperatur 3				Temp. 3
103		32			15			Temperature 4				Temp 4			Temperatur 4				Temp. 4
104		32			15			Temperature 5				Temp 5			Temperatur 5				Temp. 5
105		32			15			Temperature 6				Temp 6			Temperatur 6				Temp. 6
106		32			15			Temperature 7				Temp 7			Temperatur 7				Temp. 7
107		32			15			Temperature 8				Temp 8			Temperatur 8				Temp. 8
108		32			15			Temperature 9				Temp 9			Temperatur 9				Temp. 9
109		32			15			Temperature 10				Temp 10			Temperatur 10				Temp. 10
110		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		Alm für Battstrom-Ungleich	Batstrom-Unglei
111		32			15			EIB1 Battery2				EIB1 Battery2				EIB1 Batterie2				EIB1 Batt2
112		32			15			EIB1 Battery3				EIB1 Battery3				EIB1 Batterie3				EIB1 Batt3
113		32			15			EIB2 Battery1				EIB2 Battery1				EIB2 Batterie1				EIB2 Batt1
114		32			15			EIB2 Battery2				EIB2 Battery2				EIB2 Batterie2				EIB2 Batt2
115		32			15			EIB2 Battery3				EIB2 Battery3				EIB2 Batterie3				EIB2 Batt3
116		32			15			EIB3 Battery1				EIB3 Battery1				EIB3 Batterie1				EIB3 Batt1
117		32			15			EIB3 Battery2				EIB3 Battery2				EIB3 Batterie2				EIB3 Batt2
118		32			15			EIB3 Battery3				EIB3 Battery3				EIB3 Batterie3				EIB3 Batt3
119		32			15			EIB4 Battery1				EIB4 Battery1				EIB4 Batterie1				EIB4 Batt1
120		32			15			EIB4 Battery2				EIB4 Battery2				EIB4 Batterie2				EIB4 Batt2
121		32			15			EIB4 Battery3				EIB4 Battery3				EIB4 Batterie3				EIB4 Batt3
122		32			15			EIB1 Battery1				EIB1 Battery1				EIB1 Batterie1				EIB1 Batt1

150		32			15		Battery 1					Batt 1		 Batterie 1					Batt 1
151		32			15		Battery 2					Batt 2		 Batterie 2					Batt 2
152		32			15		Battery 3					Batt 3		 Batterie 3					Batt 3
