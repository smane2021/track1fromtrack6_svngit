﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			LVD Unit				LVD			LVD Einheit				LVD
2		32			15			Large DU LVD				Large DU LVD		LVD Grosser Verteiler			LVD Gr.Verteil.
11		32			15			Connected				Connected		Angeschlossen				angeschlossen
12		32			15			Disconnected				Disconnected		nicht angeschlossen			n.angeschlossen
13		32			15			No					No			Nein					Nein
14		32			15			Yes					Yes			Ja					Ja
21		32			15			LVD1 Status				LVD1 Status		Status LVD1				Status LVD1
22		32			15			LVD2 Status				LVD2 Status		Status LVD2				Status LVD2
23		32			15			LVD1 Disconnected			LVD1 Discon		LVD1 n.angeschlossen			LVD1 n.angeschl
24		32			15			LVD2 Disconnected			LVD2 Discon		LVD2 n.angeschlossen			LVD2 n.angeschl
25		32			15			Communication Failure			Comm Failure		Kommunikationsfehler			Komm.-fehler
26		32			15			State					State			Status					Status
27		32			15			LVD1 Control				LVD1 Control		LVD1 Einstellungen			Einstell. LVD1
28		32			15			LVD2 Control				LVD2 Control		LVD2 Einstellungen			Einstell. LVD2
31		32			15			LVD1 Enabled				LVD1 Enabled		LVD1 aktiviert				LVD1 aktiviert
32		32			15			LVD1 Mode				LVD1 Mode		LVD1 Modus				LVD1 Modus
33		32			15			LVD1 Voltage				LVD1 Voltage		LVD1 Spannung				LVD1 Spannung
34		32			15			LVD1 Reconnect Voltage			LVD1 Recon Volt		LVD1 U Wiedereinschalten		LVD1 V reconn.
35		32			15			LVD1 Reconnect Delay			LVD1 Recon Del		LVD1 Wiedereinsch.verzögerung		LVD1 recondelay
36		32			15			LVD1 Time				LVD1 Time		LVD1 Zeit				LVD1 Zeit
37		32			15			LVD1 Dependency				LVD1 Dependency		LVD1 Abhängigkeit			LVD1 Abhängig.
41		32			15			LVD2 Enabled				LVD2 Enabled		LVD2 aktiviert				LVD2 aktiviert
42		32			15			LVD2 Mode				LVD2 Mode		LVD2 Modus				LVD2 Modus
43		32			15			LVD2 Voltage				LVD2 Voltage		LVD2 Spannung				LVD2 Spannung
44		32			15			LVR2 Reconnect Voltage			LVR2 ReconnVolt		LVD2 U Wiedereinschalten		LVD2 V reconn.
45		32			15			LVD2 Reconnect Delay			LVD2 Recon Del		LVD2 Wiedereinsch.verzögerung		LVD2 recondelay
46		32			15			LVD2 Time				LVD2 Time		LVD2 Zeit				LVD2 Zeit
47		32			15			LVD2 Dependency				LVD2 Dependency		LVD2 Abhängigkeit			LVD2 Abhängig.
51		32			15			Disabled				Disabled		Deaktiviert				Deaktiviert
52		32			15			Enabled					Enabled			Aktiviert				Aktiviert
53		32			15			Voltage					Voltage			Spannung				Spannung
54		32			15			Time					Time			Zeit					Zeit
55		32			15			None					None			Kein					Kein
56		32			15			LVD1					LVD1			LVD1					LVD1
57		32			15			LVD2					LVD2			LVD2					LVD2
103		32			15			HTD1 Enable				HTD1 Enable		HTD1 aktiviert				HTD1 aktiviert
104		32			15			HTD2 Enable				HTD2 Enable		HTD2 aktiviert				HTD2 aktiviert
105		32			15			Battery LVD				Batt LVD		Batterie-LVD				Batt.-LVD
110		32			15			Commnication Interrupt			Comm Interrupt		Kommunikationsunterbrechung		Komm.Unterbrech
111		32			15			Interrupt Times				Interrupt Times		Anzahl Unterbrechungen			Unterbrechungen
116		32			15			LVD1 Contactor Failure			LVD1 Failure		LVD1 Fehler				LVD1 Fehler
117		32			15			LVD2 Contactor Failure			LVD2 Failure		LVD2 Fehler				LVD2 Fehler
118		32			15			DCD No.					DCD No.			Nummer DC-Verteiler			Nr.DC-Verteiler
