﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			SM Temp Group				SMTemp Group		Groupe SMTemp				Groupe SMTemp
2		32			15			SM Temp Number				SMTemp Num		Numéro de SMTemp			Num. SMTemp
3		32			15			Interrupt State				Interr State		Interruption				Interruption
4		32			15			Existence State				Existence State		Détection				Détection
5		32			15			Existent				Existent		Présence				Présence
6		32			15			Non Existent				Non Existent		Absent					Absent
11		32			15			SM Temp Lost				SMTemp Lost		Perte SMTemp perdido			Perte SMTemp
12		32			15			Last Number of SMTemp			Last SMTemp No.		Dernier numéro de SMTemp		Dernier N SMTem
13		32			15			Clear SMTemp Lost Alarm			Clr SMTemp Lost		Effacement Alarme Perte SMTemp		Eff Al Pert SMT
14		32			15			Clear					Clear			Effacement				Effacement
