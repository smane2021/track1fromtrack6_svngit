﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Converter Group				Conv Group		Groupe Convertisseurs			Groupe Convert.
2		32			15			Total Current				Tot Conv Curr		Courant Total				Courant Conv
3		32			15			Average Voltage				Conv Voltage		Tension Moyenne				Tension Conv
4		32			15			System Capacity Used			Sys Cap Used		Pourcentage de Charge			Pourcent Charge
5		32			15			Maximum Used Capacity			Max Used Cap		Pourcentage Charge Max			Pourc.Charge Max
6		32			15			Minimum Used Capacity			Min Used Cap		Pourcentage Charge Min			Pourc.Charge Min
7		32			15			Communicating Converters		No. Comm Conv		Nombre Convertisseur en communication	Nb Convert Com.
8		32			15			Valid Converters			Valid Conv		Convertisseur Valide			Conv. Valide
9		32			15			Number of Converters			No. Converters		Numéro du Convertisseur			Numéro Conv
10		32			15			Converter AC Failure State		AC-Fail State		Etat Défaut AC convertisseurs		Etat Déf AC Conv
11		32			15			Multi-Converters Failure Status		Multi-Conv Fail		Défaut Multiple Convertisseur		Déf +1 Convert
12		32			15			Converter Current Limit			Current Limit		Limitation Courrant Convertisseur	Limit I Convert
13		32			15			Converters Trim				Conv Trim		Limitation Convertisseurs		Limit Convert
14		32			15			DC On/Off Control			DC On/Off Ctrl		Controle DC				Controle DC
15		32			15			AC On/Off Control			AC On/Off Ctrl		Controle AC				Controle AC
16		32			15			Converters LEDs Control			LEDs Control		Controle LEDs				Controle LEDs
17		32			15			Fan Speed Control			Fan Speed Ctrl		Controle ventilateurs			Controle Ventl
18		32			15			Rated Voltage				Rated Voltage		Tension Convertisseurs			V Convert
19		32			15			Rated Current				Rated Current		Courant Convertisseurs			I Convert
20		32			15			High Voltage Limit			Hi-Volt Limit		Limite Tension Haute			Limite V Haute
21		32			15			Low Voltage Limit			Lo-Volt Limit		Limite Tension Basse			Limite V Basse
22		32			15			High Temperature Limit			Hi-Temp Limit		Limite Température Haute		Limit Temp Haut
24		32			15			Restart Time on Over Voltage		OverVRestartT		Tempo Démarrage Surtension		Tempo Dém Sur U
25		32			15			Walk-in Time				Walk-in Time		Temporisation Démarrage lent		Tempo Dém lent
26		32			15			Walk-in					Walk-in			Active Démarrage Lent			Act Dém Lent
27		32			15			Min Redundancy				Min Redundancy		Redondance Minimum			Redond Min
28		32			15			Max Redundancy				Max Redundancy		Redondance Maximum			Redond Max
29		32			15			Switch Off Delay			SwitchOff Delay		Retard à l'arrêt			Retard à l'arrêt
30		32			15			Cycle Period				Cyc Period		Périodicitée du Cycle			Périod Cycle
31		32			15			Cycle Activation Time			Cyc Act Time		Durée du Cycle				Durée du Cycle
32		32			15			Rectifier AC Failure			Rect AC Fail		Défaut AC Redresseur			Déf AC Red
33		32			15			Multi-Rectifiers Failure		Multi-rect Fail		Défaut Multi Redresseur			Défaut +1 Red
36		32			15			Normal					Normal			Normal					Normal
37		32			15			Failure					Failure			Défaut					Défaut
38		32			15			Switch Off All				Switch Off All		Tous redresseurs à l'Arrêt		Tous red Arrêt
39		32			15			Switch On All				Switch On All		Tous redresseurs en Marche		Tous red Marche
42		32			15			All Flash				All Flash		Clignotement				Clignotement
43		32			15			Stop Flash				Stop Flash		Fixe					Fixe
44		32			15			Full Speed				Full Speed		Vitesse maximum				Vitesse max
45		32			15			Automatic Speed				Auto Speed		Vitesse Automatique			Vitesse Auto
46		32			32			Current Limit Control			Curr-Limit Ctl		Controle limite de courant		Control limit I
47		32			32			Full Capacity Control			Full-Cap Ctl		Puissance maximum			Puissance max
54		32			15			Disabled				Disabled		Dés actif				Dés actif
55		32			15			Enabled					Enabled			Actif					Actif
68		32			15			System ECO				System ECO		Mis en attente				Mis en attente
72		32			15			Turn-on at AC Over-voltage		Turn-on ACOverV		Marche Forcée Surtension AC		Marche Forcée Surtension AC
73		32			15			No					No			Non					Non
74		32			15			Yes					Yes			Oui					Oui
77		32			15			Pre-CurrLmt On Switch-on Enb		Pre-CurrLimit		Pre Limitation				Pre Limitation
78		32			15			Rectifier Power type			Rect Power type		Type Redresseur				Type Red
79		32			15			Double Supply				Double Supply		Double Entrée				Double Entrée
80		32			15			Single Supply				Single Supply		Simple Entrée				Simple Entrée
81		32			15			Last Rectifiers Quantity		Last Rect Qty		Dernière Quantité Redresseurs		Dernière Quant Red
82		32			15			Converter Lost				Converter Lost		Perte Convertisseur			Perte Convert
83		32			15			Rectifier Lost				Rectifier Lost		Perte Redresseur			Perte Red
84		32			15			Clear Converter Lost Alarm		Clear Conv Lost		Reset Perte Convertisseur		Reset Perte Convertisseur
85		32			15			Clear					Clear			Effacement				Effacement
86		32			15			Confirm Converters Position		Confirm Pos		Confirmation Position Convertisseur	Confirm Pos Conv
87		32			15			Confirm					Confirm			Confirmation				Confirmation
88		32			15			Best Operating Point			Best Point		Meilleur Point				Meilleur Point
89		32			15			Rectifier Redundancy			Rect Redundancy		Redondance Convertisseur		Redond.Convert.
90		32			15			Load Fluctuation Range			Fluct Range		Pourcentage Equilibrage			% Equilibrage
91		32			15			System Energy Saving Point		EngySave Point		Meilleur Point de fonctionnement	Meil Point Fonct
92		32			15			Emergency Stop Function			E-Stop Function		Fonction E-Stop				Fonction E-Stop
93		32			15			AC Phases				AC Phases		Nb Phases				Nb Phases
94		32			15			Single Phase				Single Phase		Entrée Monophasé			Entrée 1 Phase
95		32			15			Three Phases				Three Phases		Entrée Tri-Phasee			Entrée 3 Phases
96		32			15			Input Current Limit			InputCurrLimit		Limit Courant AC			Limit I AC
97		32			15			Double Supply				Double Supply		Double Entrée AC			2 Entrées AC
98		32			15			Single Supply				Single Supply		Simple Entrée AC			1 Entrée AC
99		32			15			Small Supply				Small Supply		Petite Puissance			Petite Puissanc
100		32			15			Restart on Overvoltage Enabled		DCOverVRestart		Redémarrage Redresseur sur Surtention DC	Dem Red Surt DC
101		32			15			Sequence Start Interval			Start Interval		Demarrage Sequentiel			Dem. Sequentiel
102		32			15			Rated Voltage				Rated Voltage		Tension de repli			Tension repli
103		32			15			Rated Current				Rated Current		Courant max de repli			Imax repli
104		32			15			All Converters Comm Fail		AllConvCommFail		Pas de Réponse Convertisseur		Pas de Rép.Con.
105		32			15			Inactive				Inactive		Désactive				Désactive
106		32			15			Active					Active			Active					Active
107		32			15			Rectifier Redundancy Active		Redund Active		Activation ECO Mode			Activ ECO Mode
108		32			15			Existence State				Existence State		Détection				Détection
109		32			15			Existent				Existent		Présent					Présent
110		32			15			Non-Existent				Non-Existent		Non Présent				Non Présent
111		32			15			Average Current				Average Current		Courant Moyen				Courant Moyen
112		32			15			Default Current Limit			Current Limit		Limitation Courant			Limit.Courant
113		32			15			Default Output Voltage			Output Voltage		Défaut Tension de sortie		Déf. U sortie
114		32			15			Under Voltage				Under Voltage		Sous Tension				Sous Tension
115		32			15			Over Voltage				Over Voltage		Sur Tension				Sur Tension
116		32			15			Over Current				Over Current		Sur Courant				Sur Courant
117		32			15			Average Voltage				Average Voltage		Tension Moyenne				U Moyenne
118		32			15			HVSD Limit				HVSD Limit		Limite HVSD				Limite HVSD
119		32			15			Defualt HVSD Pst			Defualt HVSD Pst	Défaut HVSD Pst				Déf. HVSD Pst
120		32			15			Clear Converter Comm Fail		ClrConvCommFail		Reset Défaut Communication		Reset Déf.Comm.
121		32			15			HVSD					HVSD			HVSD					HVSD
135		32			15			Current Limit Point			Curr Limit Pt		Limitation Courant Point		Limit.CourantPt
136		32			15			Current Limit				Current Limit		Limitation Courant			Limit.Courant
137		32			15			Maximum Current Limit Value		Max Curr Limit		Limitation Courant Max			Limit.Cour.Max
138		32			15			Default Current Limit Point		Def Curr Lmt Pt		Défaut Limitation Courant		Def.Limit.Cour.
139		32			15			Minimum Current Limit Value		Min Curr Limit		Limitation Courant Min			Limit.Cour.Min
290		32			15			Over Current				Over Current		Sur Courant				Sur Courant
291		32			15			Over Voltage				Over Voltage		Sur Tension				Sur Tension
292		32			15			Under Voltage				Under Voltage		Tension basse				Tension basse
293		32			15			Clear All Converters Comm Fail		ClrAllConvCommF		Reset Tous Défaut Communication		Reset Déf.Comm.
294		32			15			All Converters Comm Status		All Conv Status		Etat com. tous Convertisseurs		Etat com.Convert
295		32			15			Converter Trim(24V)			Conv Trim(24V)		Conv Trim(24V)				Conv Trim(24V)
296		32			15			Last Conv Qty(Internal Use)		LastConvQty(P)		Dernier nombre convertissseur		Dernier Nb Conv
297		32			15			Def Currt Lmt Pt(Internal Use)		DefCurrLmtPt(P)		Defaut limitation courant		Def Lim Courant
298		32			15			Converter Protect			Conv Protect		Protection convertisseur		Prot Conv
299		32			15			Input Rated Voltage			Input RatedVolt		Tension d'entrée nominale		TensioEntreeNom
300		32			15			Total Rated Current			Total RatedCurr		Courant d'entrée nominale		CouranEntreeNom
301		32			15			Converter Type				Conv Type		Type de convertisseur			Type de CV
302		32			15			24-48V Conv				24-48V Conv		Convertisseur 24-48V			Convert 24-48V
303		32			15			48-24V Conv				48-24V Conv		Convertisseur 48-24V			Convert 48-24V
304		32			15			400-48V Conv				400-48V Conv		Convertisseur 400-48V			Convert 400-48V
305		32			15			Total Output Power			Output Power		Puissance de sortie totale		W - TotalSortie
306		32			15			Reset Converter IDs			Reset Conv IDs		Reset Convertisseur IDs			Reset Conv IDs
307		32			15			Input Voltage			Input Voltage		Tension dentre			Tension dentre
