﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		Tension Système				Tension Système
2		32			15			Current 1				Current 1		Corriente 1				Corriente 1
3		32			15			Current 2				Current 2		Corriente 2				Corriente 2
4		32			15			Current 3				Current 3		Corriente 3				Corriente 3
5		32			15			Current 4				Current 4		Corriente 4				Corriente 4
6		32			15			Fuse 1					Fuse 1			Fusible 1				Fusible 1
7		32			15			Fuse 2					Fuse 2			Fusible 2				Fusible 2
8		32			15			Fuse 3					Fuse 3			Fusible 3				Fusible 3
9		32			15			Fuse 4					Fuse 4			Fusible 4				Fusible 4
10		32			15			Fuse 5					Fuse 5			Fusible 5				Fusible 5
11		32			15			Fuse 6					Fuse 6			Fusible 6				Fusible 6
12		32			15			Fuse 7					Fuse 7			Fusible 7				Fusible 7
13		32			15			Fuse 8					Fuse 8			Fusible 8				Fusible 8
14		32			15			Fuse 9					Fuse 9			Fusible 9				Fusible 9
15		32			15			Fuse 10					Fuse 10			Fusible 10				Fusible 10
16		32			15			Fuse 11					Fuse 11			Fusible 11				Fusible 11
17		32			15			Fuse 12					Fuse 12			Fusible 12				Fusible 12
18		32			15			Fuse 13					Fuse 13			Fusible 13				Fusible 13
19		32			15			Fuse 14					Fuse 14			Fusible 14				Fusible 14
20		32			15			Fuse 15					Fuse 15			Fusible 15				Fusible 15
21		32			15			Fuse 16					Fuse 16			Fusible 16				Fusible 16
22		32			15			Run Time				Run Time		TTemps de Fonctionnement		Temps Fonct.
23		32			15			LVD1 Control				LVD1 Control		Control LVD1				Control LVD1
24		32			15			LVD2 Control				LVD2 Control		Control LVD2				Control LVD2
25		32			15			LVD1 Voltage				LVD1 Voltage		Tension LVD1				Tension LVD1
26		32			15			LVR1 Voltage				LVR1 Voltage		Tension LVR1				Tension LVR1
27		32			15			LVD2 Voltage				LVD2 Voltage		Tension LVD2				Tension LVD2
28		32			15			LVR2 Voltage				LVR2 Voltage		Tension LVR2				Tension LVR2
29		32			15			On					On			Conectado				Conectado
30		32			15			Off					Off			Desconectado				Desconectado
31		32			15			Normal					Normal			Normal					Normal
32		32			15			Error					Error			Error					Error
33		32			15			On					On			Conectado				Conectado
34		32			15			Fuse 1 Alarm				Fuse1 Alarm		Alarma Fusible 1			Alarma fus1
35		32			15			Fuse 2 Alarm				Fuse2 Alarm		Alarma Fusible 2			Alarma fus2
36		32			15			Fuse 3 Alarm				Fuse3 Alarm		Alarma Fusible 3			Alarma fus3
37		32			15			Fuse 4 Alarm				Fuse4 Alarm		Alarma Fusible 4			Alarma fus4
38		32			15			Fuse 5 Alarm				Fuse5 Alarm		Alarma Fusible 5			Alarma fus5
39		32			15			Fuse 6 Alarm				Fuse6 Alarm		Alarma Fusible 6			Alarma fus6
40		32			15			Fuse 7 Alarm				Fuse7 Alarm		Alarma Fusible 7			Alarma fus7
41		32			15			Fuse 8 Alarm				Fuse8 Alarm		Alarma Fusible 8			Alarma fus8
42		32			15			Fuse 9 Alarm				Fuse9 Alarm		Alarma Fusible 9			Alarma fus9
43		32			15			Fuse 10 Alarm				Fuse10 Alarm		Alarma Fusible 10			Alarma fus10
44		32			15			Fuse 11 Alarm				Fuse11 Alarm		Alarma Fusible 11			Alarma fus11
45		32			15			Fuse 12 Alarm				Fuse12 Alarm		Alarma Fusible 12			Alarma fus12
46		32			15			Fuse 13 Alarm				Fuse13 Alarm		Alarma Fusible 13			Alarma fus13
47		32			15			Fuse 14 Alarm				Fuse14 Alarm		Alarma Fusible 14			Alarma fus14
48		32			15			Fuse 15 Alarm				Fuse15 Alarm		Alarma Fusible 15			Alarma fus15
49		32			15			Fuse 16 Alarm				Fuse16 Alarm		Alarma Fusible 16			Alarma fus16
50		32			15			HW Test Alarm				HW Test Alarm		Alarma prueba HW			Alarma test HW
51	32			15			SMDUP3					SMDUP3			SMDUP3				SMDUP3
52		32			15			Battery Fuse 1 Voltage			BATT Fuse1 Volt		Tension fusible Batterie 1		Tens fus bat1
53		32			15			Battery Fuse 2 Voltage			BATT Fuse2 Volt		Tension fusible Batterie 2		Tens fus bat2
54		32			15			Battery Fuse 3 Voltage			BATT Fuse3 Volt		Tension fusible Batterie 3		Tens fus bat3
55		32			15			Battery Fuse 4 Voltage			BATT Fuse4 Volt		Tension fusible Batterie 4		Tens fus bat4
56		32			15			Battery Fuse 1 Status			BATT Fuse1 Stat		Estado fusible Batterie 1		Estado fus bat1
57		32			15			Battery Fuse 2 Status			BATT Fuse2 Stat		Estado fusible Batterie 2		Estado fus bat2
58		32			15			Battery Fuse 3 Status			BATT Fuse3 Stat		Estado fusible Batterie 3		Estado fus bat3
59		32			15			Battery Fuse 4 Status			BATT Fuse4 Stat		Estado fusible Batterie 4		Estado fus bat4
60		32			15			On					On			Conectado				Conectado
61		32			15			Off					Off			Desconectado				Desconectado
62		32			15			Battery Fuse 1 Alarm			Batt Fuse1 Alarm	Alarma fusible Batterie 1		Alarma fus1 bat
63		32			15			Battery Fuse 2 Alarm			Batt Fuse2 Alarm	Alarma fusible Batterie 2		Alarma fus2 bat
64		32			15			Battery Fuse 3 Alarm			Batt Fuse3 Alarm	Alarma fusible Batterie 3		Alarma fus3 bat
65		32			15			Battery Fuse 4 Alarm			Batt Fuse4 Alarm	Alarma fusible Batterie 4		Alarma fus4 bat
66		32			15			Total Load Current			Tot Load Curr		Corriente total carga			Total carga
67		32			15			Over Load Current Limit			Over Curr Lim		Valor de sobrecorriente			Sobrecorriente
68		32			15			Over Current				Over Current		Sobrecorriente				Sobrecorriente
69		32			15			LVD1 Enabled				LVD1 Enabled		LVD1 habilitado				LVD1 habilitado
70		32			15			LVD1 Mode				LVD1 Mode		Modo LVD1				Modo LVD1
71		32			15			LVD1 Reconnect Delay			LVD1Recon Delay		Retardo recon LVD1			RetarRecon LVD1
72		32			15			LVD2 Enabled				LVD2 Enabled		LVD2 habilitado				LVD2 habilitado
73		32			15			LVD2 Mode				LVD2 Mode		Modo LVD2				Modo LVD2
74		32			15			LVD2 Reconnect Delay			LVD2Recon Delay		Retardo recon LVD2			RetarRecon LVD2
75		32			15			LVD1 Status				LVD1 Status		Estado LVD1				Estado LVD1
76		32			15			LVD2 Status				LVD2 Status		Estado LVD2				Estado LVD2
77		32			15			Disabled				Disabled		Deshabilitado				Deshabilitado
78		32			15			Enabled					Enabled			Habilitado				Habilitado
79		32			15			By Voltage				By Volt			Por Tension				Por Tension
80		32			15			By Time					By Time			Por tiempo				Por tiempo
81		32			15			Bus Bar Volt Alarm			Bus Bar Alarm		Alarma Barra Dist			Alarma bus Dist
82		32			15			Normal					Normal			Normal					Normal
83		32			15			Low					Low			Bajo					Bajo
84		32			15			High					High			Alto					Alto
85		32			15			Low Voltage				Low Voltage		Baja Tension				Baja Tension
86		32			15			High Voltage				High Voltage		Alta Tension				Alta Tension
87		32			15			Shunt 1 Current Alarm			Shunt1 Alarm		Alarma corriente shunt1			Alarma shunt1
88		32			15			Shunt 2 Current Alarm			Shunt2 Alarm		Alarma corriente shunt2			Alarma shunt2
89		32			15			Shunt 3 Current Alarm			Shunt3 Alarm		Alarma corriente shunt3			Alarma shunt3
90		32			15			Shunt 4 Current Alarm			Shunt4 Alarm		Alarma corriente shunt4			Alarma shunt4
91		32			15			Shunt 1 Over Current			Shunt1 Over Cur		Sobrecorriente en shunt1		Sobrecor shunt1
92		32			15			Shunt 2 Over Current			Shunt2 Over Cur		Sobrecorriente en shunt2		Sobrecor shunt2
93		32			15			Shunt 3 Over Current			Shunt3 Over Cur		Sobrecorriente en shunt3		Sobrecor shunt3
94		32			15			Shunt 4 Over Current			Shunt4 Over Cur		Sobrecorriente en shunt4		Sobrecor shunt4
95		32			15			Interrupt Times				Interrupt Times		Interrupciones				Interrupciones
96		32			15			Existent				Existent		Existente				Existente
97		32			15			Non-Existent				Non-Existent		Inexistente				Inexistente
98		32			15			Very Low				Very Low		Muy bajo				Muy bajo
99		32			15			Very High				Very High		Muy alto				Muy alto
100		32			15			Switch					Switch			Interruptor				Interruptor
101		32			15			LVD1 Failure				LVD1 Failure		Fallo LVD1				Fallo LVD1
102		32			15			LVD2 Failure				LVD2 Failure		Fallo LVD2				Fallo LVD2
103		32			15			HTD1 Enable				HTD1 Enable		Habilitar HTD1				Habilitar HTD1
104		32			15			HTD2 Enable				HTD2 Enable		Habilitar HTD2				Habilitar HTD2
105		32			15			Battery LVD				Battery LVD		LVD de Batterie				LVD de Batterie
106		32			15			No Battery				No Battery		Sin Batterie				Sin Batterie
107		32			15			LVD1					LVD1			LVD1					LVD1
108		32			15			LVD2					LVD2			LVD2					LVD2
109		32			15			Battery Always On			BattAlwaysOn		Batterie siempre conectada		Siempre con Bat
110		32			15			Barcode					Barcode			Barcode					Barcode
111		32			15			DC Overvoltage				DC Overvolt		SobreTension CC				SobreTension CC
112		32			15			DC Undervoltage				DC Undervolt		SubTension CC				SubTension CC
113		32			15			Overcurrent 1				Overcurr 1		Sobrecorriente 1			Sobrecorrien 1
114		32			15			Overcurrent 2				Overcurr 2		Sobrecorriente 2			Sobrecorrien 2
115		32			15			Overcurrent 3				Overcurr 3		Sobrecorriente 3			Sobrecorrien 3
116		32			15			Overcurrent 4				Overcurr 4		Sobrecorriente 4			Sobrecorrien 4
117		32			15			Existence State				Existence State		Existence State				Existence State
118		32			15			Commnication Interrupt			Comm Interrupt		Comm Interrupt				Comm Interrupt
119		32			15			Bus Voltage Status			Bus Status		Estado Bus Tension			Estado Bus V
120		32			15			Communication OK			Comm OK			Comm OK					Comm OK
121		32			15			None is Responding			None Responding		Ninguno Responde			No Responden
122		32			15			No Response				No Response		No responde				No responde
123		32			15			Rated Capacity				Rated Capacity		Capacidad Estimada			Capacidad Est
124		32			15			Current 5				Current 5		Corriente 5				Corriente 5
125		32			15			Current 6				Current 6		Corriente 6				Corriente 6
126		32			15			Current 7				Current 7		Corriente 7				Corriente 7
127		32			15			Current 8				Current 8		Corriente 8				Corriente 8
128		32			15			Current 9				Current 9		Corriente 9				Corriente 9
129		32			15			Current 10				Current 10		Corriente 10				Corriente 10
130		32			15			Current 11				Current 11		Corriente 11				Corriente 11
131		32			15			Current 12				Current 12		Corriente 12				Corriente 12
132		32			15			Current 13				Current 13		Corriente 13				Corriente 13
133		32			15			Current 14				Current 14		Corriente 14				Corriente 14
134		32			15			Current 15				Current 15		Corriente 15				Corriente 15
135		32			15			Current 16				Current 16		Corriente 16				Corriente 16
136		32			15			Current 17				Current 17		Corriente 17				Corriente 17
137		32			15			Current 18				Current 18		Corriente 18				Corriente 18
138		32			15			Current 19				Current 19		Corriente 19				Corriente 19
139		32			15			Current 20				Current 20		Corriente 20				Corriente 20
140		32			15			Current 21				Current 21		Corriente 21				Corriente 21
141		32			15			Current 22				Current 22		Corriente 22				Corriente 22
142		32			15			Current 23				Current 23		Corriente 23				Corriente 23
143		32			15			Current 24				Current 24		Corriente 24				Corriente 24
144		32			15			Current 25				Current 25		Corriente 25				Corriente 25
145		32			15			Voltage 1				Voltage 1		Tension 1				Tension 1
146		32			15			Voltage 2				Voltage 2		Tension 2				Tension 2
147		32			15			Voltage 3				Voltage 3		Tension 3				Tension 3
148		32			15			Voltage 4				Voltage 4		Tension 4				Tension 4
149		32			15			Voltage 5				Voltage 5		Tension 5				Tension 5
150		32			15			Voltage 6				Voltage 6		Tension 6				Tension 6
151		32			15			Voltage 7				Voltage 7		Tension 7				Tension 7
152		32			15			Voltage 8				Voltage 8		Tension 8				Tension 8
153		32			15			Voltage 9				Voltage 9		Tension 9				Tension 9
154		32			15			Voltage 10				Voltage 10		Tension 10				Tension 10
155		32			15			Voltage 11				Voltage 11		Tension 11				Tension 11
156		32			15			Voltage 12				Voltage 12		Tension 12				Tension 12
157		32			15			Voltage 13				Voltage 13		Tension 13				Tension 13
158		32			15			Voltage 14				Voltage 14		Tension 14				Tension 14
159		32			15			Voltage 15				Voltage 15		Tension 15				Tension 15
160		32			15			Voltage 16				Voltage 16		Tension 16				Tension 16
161		32			15			Voltage 17				Voltage 17		Tension 17				Tension 17
162		32			15			Voltage 18				Voltage 18		Tension 18				Tension 18
163		32			15			Voltage 19				Voltage 19		Tension 19				Tension 19
164		32			15			Voltage 20				Voltage 20		Tension 20				Tension 20
165		32			15			Voltage 21				Voltage 21		Tension 21				Tension 21
166		32			15			Voltage 22				Voltage 22		Tension 22				Tension 22
167		32			15			Voltage 23				Voltage 23		Tension 23				Tension 23
168		32			15			Voltage 24				Voltage 24		Tension 24				Tension 24
169		32			15			Voltage 25				Voltage 25		Tension 25				Tension 25
170		32			15			High Current 1				Hi Current 1		Alta corriente 1			Alta corr 1
171		32			15			Very High Current 1			VHi Current 1		Courant 1 Très Elevé			I 1 Très Elevé
172		32			15			High Current 2				Hi Current 2		Alta corriente 2			Alta corr 2
173		32			15			Very High Current 2			VHi Current 2		Courant 2 Très Elevé			I 2 Très Elevé
174		32			15			High Current 3				Hi Current 3		Alta corriente 3			Alta corr 3
175		32			15			Very High Current 3			VHi Current 3		Courant 3 Très Elevé			I 3 Très Elevé
176		32			15			High Current 4				Hi Current 4		Alta corriente 4			Alta corr 4
177		32			15			Very High Current 4			VHi Current 4		Courant 4 Très Elevé			I 4 Très Elevé
178		32			15			High Current 5				Hi Current 5		Alta corriente 5			Alta corr 5
179		32			15			Very High Current 5			VHi Current 5		Courant 5 Très Elevé			I 5 Très Elevé
180		32			15			High Current 6				Hi Current 6		Alta corriente 6			Alta corr 6
181		32			15			Very High Current 6			VHi Current 6		Courant 6 Très Elevé			I 6 Très Elevé
182		32			15			High Current 7				Hi Current 7		Alta corriente 7			Alta corr 7
183		32			15			Very High Current 7			VHi Current 7		Courant 7 Très Elevé			I 7 Très Elevé
184		32			15			High Current 8				Hi Current 8		Alta corriente 8			Alta corr 8
185		32			15			Very High Current 8			VHi Current 8		Courant 8 Très Elevé			I 8 Très Elevé
186		32			15			High Current 9				Hi Current 9		Alta corriente 9			Alta corr 9
187		32			15			Very High Current 9			VHi Current 9		Courant 9 Très Elevé			I 9 Très Elevé
188		32			15			High Current 10				Hi Current 10		Alta corriente 10			Alta corr 10
189		32			15			Very High Current 10			VHi Current 10		Courant 10 Très Elevé			I 10 Très Elevé
190		32			15			High Current 11				Hi Current 11		Alta corriente 11			Alta corr 11
191		32			15			Very High Current 11			VHi Current 11		Courant 11 Très Elevé			I 11 Très Elevé
192		32			15			High Current 12				Hi Current 12		Alta corriente 12			Alta corr 12
193		32			15			Very High Current 12			VHi Current 12		Courant 12 Très Elevé			I 12 Très Elevé
194		32			15			High Current 13				Hi Current 13		Alta corriente 13			Alta corr 13
195		32			15			Very High Current 13			VHi Current 13		Courant 13 Très Elevé			I 13 Très Elevé
196		32			15			High Current 14				Hi Current 14		Alta corriente 14			Alta corr 14
197		32			15			Very High Current 14			VHi Current 14		Courant 14 Très Elevé			I 14 Très Elevé
198		32			15			High Current 15				Hi Current 15		Alta corriente 15			Alta corr 15
199		32			15			Very High Current 15			VHi Current 15		Courant 15 Très Elevé			I 15 Très Elevé
200		32			15			High Current 16				Hi Current 16		Alta corriente 16			Alta corr 16
201		32			15			Very High Current 16			VHi Current 16		Courant 16 Très Elevé			I 16 Très Elevé
202		32			15			High Current 17				Hi Current 17		Alta corriente 17			Alta corr 17
203		32			15			Very High Current 17			VHi Current 17		Courant 17 Très Elevé			I 17 Très Elevé
204		32			15			High Current 18				Hi Current 18		Alta corriente 18			Alta corr 18
205		32			15			Very High Current 18			VHi Current 18		Courant 18 Très Elevé			I 18 Très Elevé
206		32			15			High Current 19				Hi Current 19		Alta corriente 19			Alta corr 19
207		32			15			Very High Current 19			VHi Current 19		Courant 19 Très Elevé			I 19 Très Elevé
208		32			15			High Current 20				Hi Current 20		Alta corriente 20			Alta corr 20
209		32			15			Very High Current 20			VHi Current 20		Courant 20 Très Elevé			I 20 Très Elevé
210		32			15			High Current 21				Hi Current 21		Alta corriente 21			Alta corr 21
211		32			15			Very High Current 21			VHi Current 21		Courant 21 Très Elevé			I 21 Très Elevé
212		32			15			High Current 22				Hi Current 22		Alta corriente 22			Alta corr 22
213		32			15			Very High Current 22			VHi Current 22		Courant 22 Très Elevé			I 22 Très Elevé
214		32			15			High Current 23				Hi Current 23		Alta corriente 23			Alta corr 23
215		32			15			Very High Current 23			VHi Current 23		Courant 23 Très Elevé			I 23 Très Elevé
216		32			15			High Current 24				Hi Current 24		Alta corriente 24			Alta corr 24
217		32			15			Very High Current 24			VHi Current 24		Courant 24 Très Elevé			I 24 Très Elevé
218		32			15			High Current 25				Hi Current 25		Alta corriente 25			Alta corr 25
219		32			15			Very High Current 25			VHi Current 25		Courant 25 Très Elevé			I 25 Très Elevé
220		32			15			Current 1 High Limit			Curr 1 Hi Lim		Courant 1 Elevé				Courant 1 Elevé
221		32			15			Current 1 Very High Limit		Curr 1 VHi Lim		Courant 1 Très Elevé			I 1 Très Elevé
222		32			15			Current 2 High Limit			Curr 2 Hi Lim		Courant 2 Elevé				Courant 2 Elevé
223		32			15			Current 2 Very High Limit		Curr 2 VHi Lim		Courant 2 Très Elevé			I 2 Très Elevé
224		32			15			Current 3 High Limit			Curr 3 Hi Lim		Courant 3 Elevé				Courant 3 Elevé
225		32			15			Current 3 Very High Limit		Curr 3 VHi Lim		Courant 3 Très Elevé			I 3 Très Elevé
226		32			15			Current 4 High Limit			Curr 4 Hi Lim		Courant 4 Elevé				Courant 4 Elevé
227		32			15			Current 4 Very High Limit		Curr 4 VHi Lim		Courant 4 Très Elevé			I 4 Très Elevé
228		32			15			Current 5 High Limit			Curr 5 Hi Lim		Courant 5 Elevé				Courant 5 Elevé
229		32			15			Current 5 Very High Limit		Curr 5 VHi Lim		Courant 5 Très Elevé			I 5 Très Elevé
230		32			15			Current 6 High Limit			Curr 6 Hi Lim		Courant 6 Elevé				Courant 6 Elevé
231		32			15			Current 6 Very High Limit		Curr 6 VHi Lim		Courant 6 Très Elevé			I 6 Très Elevé
232		32			15			Current 7 High Limit			Curr 7 Hi Lim		Courant 7 Elevé				Courant 7 Elevé
233		32			15			Current 7 Very High Limit		Curr 7 VHi Lim		Courant 7 Très Elevé			I 7 Très Elevé
234		32			15			Current 8 High Limit			Curr 8 Hi Lim		Courant 8 Elevé				Courant 8 Elevé
235		32			15			Current 8 Very High Limit		Curr 8 VHi Lim		Courant 8 Très Elevé			I 8 Très Elevé
236		32			15			Current 9 High Limit			Curr 9 Hi Lim		Courant 9 Elevé				Courant 9 Elevé
237		32			15			Current 9 Very High Limit		Curr 9 VHi Lim		Courant 9 Très Elevé			I 9 Très Elevé
238		32			15			Current 10 High Limit			Curr 10 Hi Lim		Courant 10 Elevé			Courant 10 Elevé
239		32			15			Current 10 Very High Limit		Curr 10 VHi Lim		Courant 10 Très Elevé			I 10 Très Elevé
240		32			15			Current 11 High Limit			Curr 11 Hi Lim		Courant 11 Elevé			Courant 11 Elevé
241		32			15			Current 11 Very High Limit		Curr 11 VHi Lim		Courant 11 Très Elevé			I 11 Très Elevé
242		32			15			Current 12 High Limit			Curr 12 Hi Lim		Courant 12 Elevé			Courant 12 Elevé
243		32			15			Current 12 Very High Limit		Curr 12 VHi Lim		Courant 12 Très Elevé			I 12 Très Elevé
244		32			15			Current 13 High Limit			Curr 13 Hi Lim		Courant 13 Elevé			Courant 13 Elevé
245		32			15			Current 13 Very High Limit		Curr 13 VHi Lim		Courant 13 Très Elevé			I 13 Très Elevé
246		32			15			Current 14 High Limit			Curr 14 Hi Lim		Courant 14 Elevé			Courant 14 Elevé
247		32			15			Current 14 Very High Limit		Curr 14 VHi Lim		Courant 14 Très Elevé			I 14 Très Elevé
248		32			15			Current 15 High Limit			Curr 15 Hi Lim		Courant 15 Elevé			Courant 15 Elevé
249		32			15			Current 15 Very High Limit		Curr 15 VHi Lim		Courant 15 Très Elevé			I 15 Très Elevé
250		32			15			Current 16 High Limit			Curr 16 Hi Lim		Courant 16 Elevé			Courant 16 Elevé
251		32			15			Current 16 Very High Limit		Curr 16 VHi Lim		Courant 16 Très Elevé			I 16 Très Elevé
252		32			15			Current 17 High Limit			Curr 17 Hi Lim		Courant 17 Elevé			Courant 17 Elevé
253		32			15			Current 17 Very High Limit		Curr 17 VHi Lim		Courant 17 Très Elevé			I 17 Très Elevé
254		32			15			Current 18 High Limit			Curr 18 Hi Lim		Courant 18 Elevé			Courant 18 Elevé
255		32			15			Current 18 Very High Limit		Curr 18 VHi Lim		Courant 18 Très Elevé			I 18 Très Elevé
256		32			15			Current 19 High Limit			Curr 19 Hi Lim		Courant 19 Elevé			Courant 19 Elevé
257		32			15			Current 19 Very High Limit		Curr 19 VHi Lim		Courant 19 Très Elevé			I 19 Très Elevé
258		32			15			Current 20 High Limit			Curr 20 Hi Lim		Courant 20 Elevé			Courant 20 Elevé
259		32			15			Current 20 Very High Limit		Curr 20 VHi Lim		Courant 20 Très Elevé			I 20 Très Elevé
260		32			15			Current 21 High Limit			Curr 21 Hi Lim		Courant 21 Elevé			Courant 21 Elevé
261		32			15			Current 21 Very High Limit		Curr 21 VHi Lim		Courant 21 Très Elevé			I 21 Très Elevé
262		32			15			Current 22 High Limit			Curr 22 Hi Lim		Courant 22 Elevé			Courant 22 Elevé
263		32			15			Current 22 Very High Limit		Curr 22 VHi Lim		Courant 22 Très Elevé			I 22 Très Elevé
264		32			15			Current 23 High Limit			Curr 23 Hi Lim		Courant 23 Elevé			Courant 23 Elevé
265		32			15			Current 23 Very High Limit		Curr 23 VHi Lim		Courant 23 Très Elevé			I 23 Très Elevé
266		32			15			Current 24 High Limit			Curr 24 Hi Lim		Courant 24 Elevé			Courant 24 Elevé
267		32			15			Current 24 Very High Limit		Curr 24 VHi Lim		Courant 24 Très Elevé			I 24 Très Elevé
268		32			15			Current 25 High Limit			Curr 25 Hi Lim		Courant 25 Elevé			Courant 25 Elevé
269		32			15			Current 25 Very High Limit		Curr 25 VHi Lim		Courant 25 Très Elevé			I 25 Très Elevé
270		32			15			Current 1 Breaker Size			Curr 1 Brk Size		Calibre Protection Bat 1		Cal.Prot.Bat.1
271		32			15			Current 2 Breaker Size			Curr 2 Brk Size		Calibre Protection Bat 2		Cal.Prot.Bat.2
272		32			15			Current 3 Breaker Size			Curr 3 Brk Size		Calibre Protection Bat 3		Cal.Prot.Bat.3
273		32			15			Current 4 Breaker Size			Curr 4 Brk Size		Calibre Protection Bat 4		Cal.Prot.Bat.4
274		32			15			Current 5 Breaker Size			Curr 5 Brk Size		Calibre Protection Bat 5		Cal.Prot.Bat.5
275		32			15			Current 6 Breaker Size			Curr 6 Brk Size		Calibre Protection Bat 6		Cal.Prot.Bat.6
276		32			15			Current 7 Breaker Size			Curr 7 Brk Size		Calibre Protection Bat 7		Cal.Prot.Bat.7
277		32			15			Current 8 Breaker Size			Curr 8 Brk Size		Calibre Protection Bat 8		Cal.Prot.Bat.8
278		32			15			Current 9 Breaker Size			Curr 9 Brk Size		Calibre Protection Bat 9		Cal.Prot.Bat.9
279		32			15			Current 10 Breaker Size			Curr 10 Brk Size	Calibre Protection Bat 10		Cal.Prot.Bat.10
280		32			15			Current 11 Breaker Size			Curr 11 Brk Size	Calibre Protection Bat 11		Cal.Prot.Bat.11
281		32			15			Current 12 Breaker Size			Curr 12 Brk Size	Calibre Protection Bat 12		Cal.Prot.Bat.12
282		32			15			Current 13 Breaker Size			Curr 13 Brk Size	Calibre Protection Bat 13		Cal.Prot.Bat.13
283		32			15			Current 14 Breaker Size			Curr 14 Brk Size	Calibre Protection Bat 14		Cal.Prot.Bat.14
284		32			15			Current 15 Breaker Size			Curr 15 Brk Size	Calibre Protection Bat 15		Cal.Prot.Bat.15
285		32			15			Current 16 Breaker Size			Curr 16 Brk Size	Calibre Protection Bat 16		Cal.Prot.Bat.16
286		32			15			Current 17 Breaker Size			Curr 17 Brk Size	Calibre Protection Bat 17		Cal.Prot.Bat.17
287		32			15			Current 18 Breaker Size			Curr 18 Brk Size	Calibre Protection Bat 18		Cal.Prot.Bat.18
288		32			15			Current 19 Breaker Size			Curr 19 Brk Size	Calibre Protection Bat 19		Cal.Prot.Bat.19
289		32			15			Current 20 Breaker Size			Curr 20 Brk Size	Calibre Protection Bat 20		Cal.Prot.Bat.20
290		32			15			Current 21 Breaker Size			Curr 21 Brk Size	Calibre Protection Bat 21		Cal.Prot.Bat.21
291		32			15			Current 22 Breaker Size			Curr 22 Brk Size	Calibre Protection Bat 22		Cal.Prot.Bat.22
292		32			15			Current 23 Breaker Size			Curr 23 Brk Size	Calibre Protection Bat 23		Cal.Prot.Bat.23
293		32			15			Current 24 Breaker Size			Curr 24 Brk Size	Calibre Protection Bat 24		Cal.Prot.Bat.24
294		32			15			Current 25 Breaker Size			Curr 25 Brk Size	Calibre Protection Bat 25		Cal.Prot.Bat.25
295		32			15			Shunt 1 Voltage				Shunt1 Voltage		Tension Shunt 1				V Shunt 1
296		32			15			Shunt 1 Current				Shunt1 Current		Courant Shunt 1				I Shunt 1
297		32			15			Shunt 2 Voltage				Shunt2 Voltage		Tension Shunt 2				V Shunt 2
298		32			15			Shunt 2 Current				Shunt2 Current		Courant Shunt 2				I Shunt 2
299		32			15			Shunt 3 Voltage				Shunt3 Voltage		Tension Shunt 3				V Shunt 3
300		32			15			Shunt 3 Current				Shunt3 Current		Courant Shunt 3				I Shunt 3
301		32			15			Shunt 4 Voltage				Shunt4 Voltage		Tension Shunt 4				V Shunt 4
302		32			15			Shunt 4 Current				Shunt4 Current		Courant Shunt 4				I Shunt 4
303		32			15			Shunt 5 Voltage				Shunt5 Voltage		Tension Shunt 5				V Shunt 5
304		32			15			Shunt 5 Current				Shunt5 Current		Courant Shunt 5				I Shunt 5
305		32			15			Shunt 6 Voltage				Shunt6 Voltage		Tension Shunt 6				V Shunt 6
306		32			15			Shunt 6 Current				Shunt6 Current		Courant Shunt 6				I Shunt 6
307		32			15			Shunt 7 Voltage				Shunt7 Voltage		Tension Shunt 7				V Shunt 7
308		32			15			Shunt 7 Current				Shunt7 Current		Courant Shunt 7				I Shunt 7
309		32			15			Shunt 8 Voltage				Shunt8 Voltage		Tension Shunt 8				V Shunt 8
310		32			15			Shunt 8 Current				Shunt8 Current		Courant Shunt 8				I Shunt 8
311		32			15			Shunt 9 Voltage				Shunt9 Voltage		Tension Shunt 9				V Shunt 9
312		32			15			Shunt 9 Current				Shunt9 Current		Courant Shunt 9				I Shunt 9
313		32			15			Shunt 10 Voltage			Shunt10 Voltage		Tension Shunt 10			V Shunt 10
314		32			15			Shunt 10 Current			Shunt10 Current		Courant Shunt 10			I Shunt 10
315		32			15			Shunt 11 Voltage			Shunt11 Voltage		Tension Shunt 11			V Shunt 11
316		32			15			Shunt 11 Current			Shunt11 Current		Courant Shunt 11			I Shunt 11
317		32			15			Shunt 12 Voltage			Shunt12 Voltage		Tension Shunt 12			V Shunt 12
318		32			15			Shunt 12 Current			Shunt12 Current		Courant Shunt 12			I Shunt 12
319		32			15			Shunt 13 Voltage			Shunt13 Voltage		Tension Shunt 13			V Shunt 13
320		32			15			Shunt 13 Current			Shunt13 Current		Courant Shunt 13			I Shunt 13
321		32			15			Shunt 14 Voltage			Shunt14 Voltage		Tension Shunt 14			V Shunt 14
322		32			15			Shunt 14 Current			Shunt14 Current		Courant Shunt 14			I Shunt 14
323		32			15			Shunt 15 Voltage			Shunt15 Voltage		Tension Shunt 15			V Shunt 15
324		32			15			Shunt 15 Current			Shunt15 Current		Courant Shunt 15			I Shunt 15
325		32			15			Shunt 16 Voltage			Shunt16 Voltage		Tension Shunt 16			V Shunt 16
326		32			15			Shunt 16 Current			Shunt16 Current		Courant Shunt 16			I Shunt 16
327		32			15			Shunt 17 Voltage			Shunt17 Voltage		Tension Shunt 17			V Shunt 17
328		32			15			Shunt 17 Current			Shunt17 Current		Courant Shunt 17			I Shunt 17
329		32			15			Shunt 18 Voltage			Shunt18 Voltage		Tension Shunt 18			V Shunt 18
330		32			15			Shunt 18 Current			Shunt18 Current		Courant Shunt 18			I Shunt 18
331		32			15			Shunt 19 Voltage			Shunt19 Voltage		Tension Shunt 19			V Shunt 19
332		32			15			Shunt 19 Current			Shunt19 Current		Courant Shunt 19			I Shunt 19
333		32			15			Shunt 20 Voltage			Shunt20 Voltage		Tension Shunt 20			V Shunt 20
334		32			15			Shunt 20 Current			Shunt20 Current		Courant Shunt 20			I Shunt 20
335		32			15			Shunt 21 Voltage			Shunt21 Voltage		Tension Shunt 21			V Shunt 21
336		32			15			Shunt 21 Current			Shunt21 Current		Courant Shunt 21			I Shunt 21
337		32			15			Shunt 22 Voltage			Shunt22 Voltage		Tension Shunt 22			V Shunt 22
338		32			15			Shunt 22 Current			Shunt22 Current		Courant Shunt 22			I Shunt 22
339		32			15			Shunt 23 Voltage			Shunt23 Voltage		Tension Shunt 23			V Shunt 23
340		32			15			Shunt 23 Current			Shunt23 Current		Courant Shunt 23			I Shunt 23
341		32			15			Shunt 24 Voltage			Shunt24 Voltage		Tension Shunt 24			V Shunt 24
342		32			15			Shunt 24 Current			Shunt24 Current		Courant Shunt 24			I Shunt 24
343		32			15			Shunt 25 Voltage			Shunt25 Voltage		Tension Shunt 25			V Shunt 25
344		32			15			Shunt 25 Current			Shunt25 Current		Courant Shunt 25			I Shunt 25
345		32			15			Shunt Size Settable			Shunt Settable		Configuration Shunt			Config. Shunt
346		32			15			By Software				By Software		Par Software				Par Software
347		32			15			By Dip-Switch				By Dip-Switch		Par Dip switch				Par Dip switch
348		32			15			Not Supported				Not Supported		Pas Supporte				Pas Supporte
349		32			15			Shunt Coefficient Conflict		Shunt Conflict		Conflit Param Shunt			Conflit Shunt
350		32			15			Load1 Alarm Flag			Load1 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
351		32			15			Load2 Alarm Flag			Load2 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
352		32			15			Load3 Alarm Flag			Load3 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
353		32			15			Load4 Alarm Flag			Load4 Alarm Flag	Load4 Alarm Flag			Load4 Alarm Flag
354		32			15			Load5 Alarm Flag			Load5 Alarm Flag	Load5 Alarm Flag			Load5 Alarm Flag
355		32			15			Load6 Alarm Flag			Load6 Alarm Flag	Load6 Alarm Flag			Load6 Alarm Flag
356		32			15			Load7 Alarm Flag			Load7 Alarm Flag	Load7 Alarm Flag			Load7 Alarm Flag
357		32			15			Load8 Alarm Flag			Load8 Alarm Flag	Load8 Alarm Flag			Load8 Alarm Flag
358		32			15			Load9 Alarm Flag			Load9 Alarm Flag	Load9 Alarm Flag			Load9 Alarm Flag
359		32			15			Load10 Alarm Flag			Load10 Alarm Flag	Load10 Alarm Flag			Load10 Alarm Flag
360		32			15			Load11 Alarm Flag			Load11 Alarm Flag	Load11 Alarm Flag			Load11 Alarm Flag
361		32			15			Load12 Alarm Flag			Load12 Alarm Flag	Load12 Alarm Flag			Load12 Alarm Flag
362		32			15			Load13 Alarm Flag			Load13 Alarm Flag	Load13 Alarm Flag			Load13 Alarm Flag
363		32			15			Load14 Alarm Flag			Load14 Alarm Flag	Load14 Alarm Flag			Load14 Alarm Flag
364		32			15			Load15 Alarm Flag			Load15 Alarm Flag	Load15 Alarm Flag			Load15 Alarm Flag
365		32			15			Load16 Alarm Flag			Load16 Alarm Flag	Load16 Alarm Flag			Load16 Alarm Flag
366		32			15			Load17 Alarm Flag			Load17 Alarm Flag	Load17 Alarm Flag			Load17 Alarm Flag
367		32			15			Load18 Alarm Flag			Load18 Alarm Flag	Load18 Alarm Flag			Load18 Alarm Flag
368		32			15			Load19 Alarm Flag			Load19 Alarm Flag	Load19 Alarm Flag			Load19 Alarm Flag
369		32			15			Load20 Alarm Flag			Load20 Alarm Flag	Load20 Alarm Flag			Load20 Alarm Flag
370		32			15			Load21 Alarm Flag			Load21 Alarm Flag	Load21 Alarm Flag			Load21 Alarm Flag
371		32			15			Load22 Alarm Flag			Load22 Alarm Flag	Load22 Alarm Flag			Load22 Alarm Flag
372		32			15			Load23 Alarm Flag			Load23 Alarm Flag	Load23 Alarm Flag			Load23 Alarm Flag
373		32			15			Load24 Alarm Flag			Load24 Alarm Flag	Load24 Alarm Flag			Load24 Alarm Flag
374		32			15			Load25 Alarm Flag			Load25 Alarm Flag	Load25 Alarm Flag			Load25 Alarm Flag
400		32			15			Shunt 1						Shunt 1					Shunt 1						Shunt 1	
401		32			15			Shunt 2						Shunt 2					Shunt 2						Shunt 2	
402		32			15			Shunt 3						Shunt 3					Shunt 3						Shunt 3	
403		32			15			Shunt 4						Shunt 4					Shunt 4						Shunt 4	
404		32			15			Shunt 5						Shunt 5					Shunt 5						Shunt 5	
405		32			15			Shunt 6						Shunt 6					Shunt 6						Shunt 6	
406		32			15			Shunt 7						Shunt 7					Shunt 7						Shunt 7	
407		32			15			Shunt 8						Shunt 8					Shunt 8						Shunt 8	
408		32			15			Shunt 9						Shunt 9					Shunt 9						Shunt 9	
409		32			15			Shunt 10					Shunt 10				Shunt 10					Shunt 10
410		32			15			Shunt 11					Shunt 11				Shunt 11					Shunt 11
411		32			15			Shunt 12					Shunt 12				Shunt 12					Shunt 12
412		32			15			Shunt 13					Shunt 13				Shunt 13					Shunt 13
413		32			15			Shunt 14					Shunt 14				Shunt 14					Shunt 14
414		32			15			Shunt 15					Shunt 15				Shunt 15					Shunt 15
415		32			15			Shunt 16					Shunt 16				Shunt 16					Shunt 16
416		32			15			Shunt 17					Shunt 17				Shunt 17					Shunt 17
417		32			15			Shunt 18					Shunt 18				Shunt 18					Shunt 18
418		32			15			Shunt 19					Shunt 19				Shunt 19					Shunt 19
419		32			15			Shunt 20					Shunt 20				Shunt 20					Shunt 20
420		32			15			Shunt 21					Shunt 21				Shunt 21					Shunt 21
421		32			15			Shunt 22					Shunt 22				Shunt 22					Shunt 22
422		32			15			Shunt 23					Shunt 23				Shunt 23					Shunt 23
423		32			15			Shunt 24					Shunt 24				Shunt 24					Shunt 24
424		32			15			Shunt 25					Shunt 25				Shunt 25					Shunt 25
