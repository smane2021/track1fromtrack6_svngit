﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			EIB Battery			EIB Battery		EIB Batteries			EIB Batteries
2		32			15			Battery Current				Batt Current		Courant Batterie			I Batterie
3		32			15			Battery Voltage				Batt Voltage		Tension Batterie			V Batterie
4		32			15			Battery Rating (Ah)			Batt Rating(Ah)		évaluation Batterie (AH)		éval Bat (AH)
5		32			15			Battery Capacity (%)		Batt Cap(%)		Capacité Batterie (%)		Capa Bat (%)
6		32			15			Current Limit Exceeded			Curr Lmt Exceed		Depassement Courant Limitation		Depass.I Limit.
7		32			15			Over Battery current			Over Current		Sur Courant Batterie			Sur I Batt.
8		32			15			Low Capacity			Low Capacity		Capacité Basse 			Capa. Basse
9		32			15			Yes					Yes			Oui					Oui
10		32			15			No					No			Non					Non
26		32			15			State					State			Etat					Etat
27		32			15			Battery Block 1 Voltage			Batt B1 Volt		Tension Block 1				V Block 1
28		32			15			Battery Block 2 Voltage			Batt B2 Volt		Tension Block 2				V Block 2
29		32			15			Battery Block 3 Voltage			Batt B3 Volt		Tension Block 3				V Block 3
30		32			15			Battery Block 4 Voltage			Batt B4 Volt		Tension Block 4				V Block 4
31		32			15			Battery Block 5 Voltage			Batt B5 Volt		Tension Block 5				V Block 5
32		32			15			Battery Block 6 Voltage			Batt B6 Volt		Tension Block 6				V Block 6
33		32			15			Battery Block 7 Voltage			Batt B7 Volt		Tension Block 7				V Block 7
34		32			15			Battery Block 8 Voltage			Batt B8 Volt		Tension Block 8				V Block 8
35		32			15			Battery Management			Batt Management		Gestion Batterie			Gest. Bat
36		32			15			Enabled				Enabled			Actif				Actif
37		32			15			Disabled			Disabled		Inactif				Inactif	
38		32			15			Communication Fail			Comm Fail		Defaut de communication			Defaut COM
39		32			15			Shunt Full Current			Shunt Full Curr		Courant Shunt				I Shunt
40		32			15			Shunt Full Voltage			Shunt Full Volt		Tension Shunt				V Shunt
41		32			15			On					On			Marche					Marche
42		32			15			Off					Off			Off					Off
43		32			15			Communication Fail			Comm Fail		Defaut de communication			Defaut COM
44		32			15			Battery Temperature Probe	BattTempPrbNum		Température de la batterie Sonde		TempBatNomSonde
87		32			15			No					No			Non					Non
91		32			15			Temperature Sensor 1		Temp Sensor 1		Sonde Température 1		Sonde Temp.1
92		32			15			Temperature Sensor 2		Temp Sensor 2		Sonde Température 2		Sonde Temp.2
93		32			15			Temperature Sensor 3		Temp Sensor 3		Sonde Température 3		Sonde Temp.3
94		32			15			Temperature Sensor 4		Temp Sensor 4		Sonde Température 4		Sonde Temp.4
95		32			15			Temperature Sensor 5		Temp Sensor 5		Sonde Température 5		Sonde Temp.5
96		32			15			Rated Capacity			Rated Capacity		Capacité nominal C10		Capacité C10
97		32			15			Battery Temperature		Battery Temp		Température batterie		Temp. bat.
98		32			15			Battery Temperature Sensor		BattTempSensor		Capteur Temperature batterie		Capt.Temp. bat.
99		32			15			None					None			Aucun					Aucun
100		32			15			Temperature 1				Temp 1			Capteur Temp. 1				Capt.Temp. 1
101		32			15			Temperature 2				Temp 2			Capteur Temp. 2				Capt.Temp. 2
102		32			15			Temperature 3				Temp 3			Capteur Temp. 3				Capt.Temp. 3
103		32			15			Temperature 4				Temp 4			Capteur Temp. 4				Capt.Temp. 4
104		32			15			Temperature 5				Temp 5			Capteur Temp. 5				Capt.Temp. 5
105		32			15			Temperature 6				Temp 6			Capteur Temp. 6				Capt.Temp. 6
106		32			15			Temperature 7				Temp 7			Capteur Temp. 7				Capt.Temp. 7
107		32			15			Temperature 8				Temp 8			Capteur Temp. 8				Capt.Temp. 8
108		32			15			Temperature 9				Temp 9			Capteur Temp. 9				Capt.Temp. 9
109		32			15			Temperature 10				Temp 10			Capteur Temp. 10			Capt.Temp. 10
110		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		CourAlm DéséquilibreBatt		CourAlmDéséqBat	
111		32			15			EIB1 Battery2				EIB1 Battery2				EIB1 Batterie2				EIB1 Batterie2
112		32			15			EIB1 Battery3				EIB1 Battery3				EIB1 Batterie3				EIB1 Batterie3
113		32			15			EIB2 Battery1				EIB2 Battery1				EIB2 Batterie1				EIB2 Batterie1
114		32			15			EIB2 Battery2				EIB2 Battery2				EIB2 Batterie2				EIB2 Batterie2
115		32			15			EIB2 Battery3				EIB2 Battery3				EIB2 Batterie3				EIB2 Batterie3
116		32			15			EIB3 Battery1				EIB3 Battery1				EIB3 Batterie1				EIB3 Batterie1
117		32			15			EIB3 Battery2				EIB3 Battery2				EIB3 Batterie2				EIB3 Batterie2
118		32			15			EIB3 Battery3				EIB3 Battery3				EIB3 Batterie3				EIB3 Batterie3
119		32			15			EIB4 Battery1				EIB4 Battery1				EIB4 Batterie1				EIB4 Batterie1
120		32			15			EIB4 Battery2				EIB4 Battery2				EIB4 Batterie2				EIB4 Batterie2
121		32			15			EIB4 Battery3				EIB4 Battery3				EIB4 Batterie3				EIB4 Batterie3
122		32			15			EIB1 Battery1				EIB1 Battery1				EIB1 Batterie1				EIB1 Batterie1

150		32			15		Battery 1					Batt 1		Batterie 1					Batt 1
151		32			15		Battery 2					Batt 2		Batterie 2					Batt 2
152		32			15		Battery 3					Batt 3		Batterie 3					Batt 3
