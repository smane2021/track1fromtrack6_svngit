﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			16			Power System				Power System		Système					Système
2		32			15			System Output Voltage			Output Voltage		Tension système				Tension sys.
3		32			15			System Output Current			Output Current		Charge système				Charge sys.
4		32			15			System Output Power			Output Power		Puissance système			Puis W sys.
5		32			15			Total Power Consumption			Power Consump		Puissance consommée			Puis VA sys.
6		32			15			Power Peak in 24 Hours			Power Peak		Pique Puis. des dernières 24h		Pique W
7		32			15			Average Power in 24 Hours		Avg power		PU moyen. des dernières 24h		Moy.puissance
8		32			15			Hardware Write-protect Switch		Hardware Switch		Protection écriture			Protect. écrit.
9		32			15			Ambient Temperature			Ambient Temp		Température ambiante			Temp ambiante
10		32			15			Number of SM-DUs			No of SM-DUs		Numéro de SMDUs				Num SMDUs
18		32			15			Relay Output 1				Relay Output 1		Sortie Relais 1				Sortie RL 1
19		32			15			Relay Output 2				Relay Output 2		Sortie Relais 2				Sortie RL 2
20		32			15			Relay Output 3				Relay Output 3		Sortie Relais 3				Sortie RL 3
21		32			15			Relay Output 4				Relay Output 4		Sortie Relais 4				Sortie RL 4
22		32			15			Relay Output 5				Relay Output 5		Sortie Relais 5				Sortie RL 5
23		32			15			Relay Output 6				Relay Output 6		Sortie Relais 6				Sortie RL 6
24		32			15			Relay Output 7				Relay Output 7		Sortie Relais 7				Sortie RL 7
25		32			15			Relay Output 8				Relay Output 8		Sortie Relais 8				Sortie RL 8
27		32			15			Undervoltage 1 Level			Undervoltage 1		Tension DC basse			V DC bas
28		32			15			Undervoltage 2 Level			Undervoltage 2		Tension DC très basse			V DC très bas
29		32			15			Overvoltage 1 Level			Overvoltage 1		Tension DC haute			V DC haut
31		32			15			Temperature 1 High Limit		Temp 1 High		Limite Température 1 haute		Lim Temp1 haute
32		32			15			Temperature 1 Low Limit			Temp 1 Low		Limite Température 1 basse		Lim Temp1 basse
33		32			15			Auto/Man State				Auto/Man State		Etat Aut/Man				Etat Aut/Man
34		32			15			Outgoing Alarms Blocked			Alarms Blocked		Blocage report des alarmes		Bloc Report alm
35		32			15			Supervision Unit Internal Fault		Internal Fault		Défaut Contrôleur			Déf Contrôleur
36		32			15			CAN Communication Failure		CAN Comm Fault		Défaut Communication CAN		Défaut com CAN
37		32			15			Mains Failure				Mains Failure		Absence secteur				Abs secteur
38		32			15			Undervoltage 1				Undervolt 1		Tension DC basse			V DC bas
39		32			15			Undervoltage 2				Undervolt 2		Tension DC très basse			V DC très bas
40		32			15			Overvoltage 1				Overvolt 1		Tension DC haute			V DC haut
41		32			15			High Temperature 1			High Temp 1		Température 1 haute			Temp 1 haute
42		32			15			Low Temperature 1			Low Temp 1		Température 1 basse			Temp 1 basse
43		32			15			Temperature 1 Sensor Fault		T1 Sensor Fault		Défaut capteur temp.			Défaut CapTemp
44		32			15			Outgoing Alarms Blocked			Alarms Blocked		Blocage report des alarmes		Bloc Report alm
45		32			15			Maintenance Alarm			MaintenanceAlrm		Alarme maintenance			AlrMaintenance
46		32			15			Not Protected				Not protected		Pas de protection			Pas de protect.
47		32			15			Protected				Protected		Protection				Protection
48		32			15			Normal					Normal			Normal					Normal
49		32			15			Fault					Fault			Défaut					Défaut
50		32			15			Off					Off			Inactive				Inactive
51		32			15			On					On			Active					Active
52		32			15			Off					Off			Inactive				Inactive
53		32			15			On					On			Active					Active
54		32			15			Off					Off			Inactive				Inactive
55		32			15			On					On			Active					Active
56		32			15			Off					Off			Inactive				Inactive
57		32			15			On					On			Active					Active
58		32			15			Off					Off			Inactive				Inactive
59		32			15			On					On			Active					Active
60		32			15			Off					Off			Inactive				Inactive
61		32			15			On					On			Active					Active
62		32			15			Off					Off			Inactive				Inactive
63		32			15			On					On			Active					Active
64		32			15			Open					Open			Ouvert					Ouvert
65		32			15			Closed					Closed			Fermé					Fermé
66		32			15			Open					Open			Ouvert					Ouvert
67		32			15			Closed					Closed			Fermé					Fermé
78		32			15			Off					Off			Inactive				Inactive
79		32			15			On					On			Active					Active
80		32			15			Auto					Auto			Auto					Auto
81		32			15			Manual					Manual			Manuel					Manuel
82		32			15			Normal					Normal			Normal					Normal
83		32			15			Blocked					Blocked			Bloqué					Bloqué
85		32			15			Set to Auto Mode			To Auto Mode		Mode Automatique			Mode AUTO
86		32			15			Set to Manual Mode			To Manual Mode		Mode Manuel				Mode Manuel
87		32			15			Power Rate Level			PowerRate Level		Limite de Puissance			Limit de Puis.
88		32			15			Current Power Peak Limit		Power Limit		Limite de courrant			Limit I
89		32			15			Peak					Peak			Pique					Pique
90		32			15			High					High			Haut					Haut
91		32			15			Flat					Flat			Constant				Constant
92		32			15			Low					Low			Bas					Bas
93		32			15			Lower Consumption			Lower Consump		Conso. basse activée			Conso basse
94		32			15			Power Peak Savings			Pow Peak Save		Limitation de puissance active		Limit P active
95		32			15			Disable					Disable			Inactif					Inactif
96		32			15			Enable					Enable			Actif					Actif
97		32			15			Disable					Disable			Inactif					Inactif
98		32			15			Enable					Enable			Actif					Actif
99		32			15			Over Maximum Power			Over Power		Sur puissance				Sur puissance
100		32			15			Normal					Normal			Normal					Normal
101		32			15			Alarm					Alarm			Alarme					Alarme
102		32			15			Over Maximum Power Alarm		Over Power		Sur puissance				Sur Puis.
104		32			15			System Alarm Status			Alarm Status		Etat système				Etat système
105		32			15			No Alarms				No Alarms		Aucune alarme				0 alarme
106		32			15			Observation Alarm			Observation		Observation				Observation
107		32			15			Major Alarm				Major			Mineur					Mineur
108		32			15			Critical Alarm				Critical		Majeur					Majeur
109		32			15			Maintenance Run Time			Mtn Run Time		Tps fct depuis dernière maint.		Maintenance Fct
110		32			15			Maintenance Interval			Mtn Interval		Limite prochaine maintenance		Limite maint
111		32			15			Maintenance Alarm Time			Mtn Alarm Time		Prochaine maintenance			Prochaine maint
112	32		15		Maintenance Time Out		Mtn Time Out		Durée Maintenance Ecoulée	Duré Mtn Ecoulé
113		32			15			No					No			Non					Non
114		32			15			Yes					Yes			Oui					Oui
115		32			15			Power Split Mode			Split Mode		Mode puissance				Mode puissance
116	32		15		Slave Current Limit		Slave Cur Lmt		Esclave:Limitation de courant	Escl:Limit. courant
117		32			15			Slave Delta Voltage			Slave Delta vol		Esclave: Ecart de tension		Escl: Ecart U
118		32			15			Slave Proportional Coefficient		Slave P Coef		Esclave:Proportionnel P			Escl:Proport P
119		32			15			Slave Integral Time			Slave I Time		Esclave:Durée Intégrale			Escl:Durée Inté
120		32			15			Master Mode				Master			Mode maître				Mode maître
121		32			15			Slave Mode				Slave			Mode esclave				Mode esclave
122		32			15			MPCL Control Step			MPCL Ctl Step		MPCL commande				MPCL commande
123		32			15			MPCL Control Threshold			MPCL Ctl Thres		MPCL seuil				MPCL seuil
124		32			15			MPCL Battery Discharge Enabled		MPCL BatDisch		MPCL décharge actif			MPCL décharge
125		32			15			MPCL Diesel Control Enabled		MPCL DieselCtl		MPCL diesel actif			MPCL diesel
126		32			15			Disabled				Disabled		Inactif					Inactif
127		32			15			Enabled					Enabled			Actif					Actif
128		32			15			Disabled				Disabled		Inactif					Inactif
129		32			15			Enabled					Enabled			Actif					Actif
130		32			15			System Time				System Time		Heure système				Heure système
131		32			15			LCD Language				LCD Language		LCD langue				LCD langue
132		32			15			Audible Alarm				Audible Alarm		Activation Buzzer			Activ.Buzzer
133		32			15			English					English			Anglais					Anglais
134		32			15			French					French			Français				Français
135		32			15			On					On			Marche					Marche
136		32			15			Off					Off			Arrêt					Arrêt
137		32			15			3 Minutes				3 Min			3 Min					3 Min
138		32			15			10 Minutes				10 Min			10 Min					10 Min
139		32			15			1 Hour					1 Hour			1 Heure					1 Heure
140		32			15			4 Hours					4 Hour			4 Heure					4 Heure
141		32			15			Reset Maintenance Run Time		Reset Run Time		RAZ date				RAZ date
142		32			15			No					No			Non					Non
143		32			15			Yes					Yes			Oui					Oui
144		32			15			Alarm					Alarm			Alarme					Alarme
145		32			15			ACU+ HW Status				HW Status		Etat ACU+				Etat ACU+
146		32			15			Normal					Normal			Normale					Normale
147		32			15			Configuration Error			Config Error		Erreur configuration			Err. Conf.
148		32			15			Flash Fault				Flash Fault		Défaut flash				Défaut flash
150		32			15			LCD Time Zone				Time Zone		Decalage horaire			Decal. Horaire
151		32			15			Time Sync Main Server			Time Sync Svr		Synchro. horaire			Syn. horaire
152		32			15			Time Sync Backup Server			Backup Sync Svr		Synchro Horaire sur Server		Synch H Server
153		32			15			Time Sync Interval			Sync Interval		Synchro interval			Synchro inter.
155		32			15			Reset SCU				Reset SCU		SCU Reset				SCU Reset
156		32			15			Running Configuration Type		Config Type		Type configuration			Config type
157		32			15			Normal Configuration			Normal Config		Config. normale				Config. norm.
158		32			15			Backup Configuration			Backup Config		Restauration config.			Restor Config
159		32			15			Default Configuration			Default Config		Défaut Config.				Défaut Config
160		32			15			Configuration Error from Backup		Config Error 1		Err config 1				Err config 1
161		32			15			Configuration Errorfrom Default		Config Error 2		Err config 2				Err config 2
162		32			15			Control System Alarm LED		Ctrl Sys Alarm		Système alarme led			Alarme led
163	32		15		Abnormal Load Current		Ab Load Curr		Courant Utilisation anormal	Err. I util
164		32			15			Normal					Normal			Normal					Normal
165		32			15			Abnormal				Abnormal		Anormal					Anormal
167		32			15			Main Switch Block Condition		MS Block Cond		Switch bloqué				Switch bloqué
168		32			15			No					No			Non					Non
169		32			15			Yes					Yes			Oui					Oui
170		32			15			Total Run Time				Total Run Time		Temps total de fonc.			Temps total fct
171		32			15			Total Number of Alarms			Total No. Alms		Total alarme				Total alarme
172		32			15			Total Number of OA			Total No. OA		Total OA nbre				Total OA
173		32			15			Total Number of MA			Total No. MA		Total MA				Total MA
174		32			15			Total Number of CA			Total No. CA		Total CA				Total CA
175		32			15			SPD Fault				SPD Fault		Défaut Protection Départ DC		Déf Prot DC
176		32			15			Overvoltage 2 Level			Overvoltage 2		Tension DC très haute			V DC très haute
177		32			15			Overvoltage 2				Overvoltage 2		Tension DC très haute			V DC très haute
178		32			15			Regulation Voltage			Regu Voltage		Inibition Regulation Tension		Inib Reg Vs
179		32			15			Regulation Voltage Range		Regu Volt Range		Niveau Regulation Tension		Niv Reg. Vs
180		32			15			Keypad Sound				Keypad Sound		Bip Clavier				Bip Clavier
181		32			15			On					On			Marche					Marche
182		32			15			Off					Off			Arrêt					Arrêt
183		32			15			Load Shunt Full Current			Load Shunt Curr		Courant Shunt Util			I Shunt Util
184		32			15			Load Shunt Full Voltage			Load Shunt Volt		Tension Shunt Util			V Shunt Util
185		32			15			Battery 1 Shunt Full Current		Bat1 Shunt Curr		Courant Shunt Bat 1			I Shunt Bat 1
186		32			15			Battery 1 Shunt Full Voltage		Bat1 Shunt Volt		Tension Shunt Bat 1			V Shunt Bat 1
187		32			15			Battery 2 Shunt Full Current		Bat2 Shunt Curr		Courant Shunt Bat 2			I Shunt Bat 2
188		32			15			Battery 2 Shunt Full Voltage		Bat2 Shunt Volt		Tension Shunt Bat 2			V Shunt Bat 2
219	32		15		DO6				DO6			Sortie 6			STOR 6
220	32		15		DO7				DO7			STOR 7				STOR 7
221	32		15		DO8				DO8			STOR 8				STOR 8
222		32			15			RS232 Baudrate				RS232 Baudrate		RS232 Baudrate				RS232 Baudrate
223		32			15			Local Address				Local Addr		Local Address				Local Addr
224		32			15			Callback Number				Callback Num		Callback Num				Callback Num
225		32			15			Interval Time of Callback		Interval		Interval Time of Callback		Interval
227	32		15		DC Data Flag (On-Off)		DC DataFlag 1		DC Data Flag(on-off)		DCDataFlag1
228	32		15		DC Data Flag (Alarm)		DC DataFlag 2		DC Data Flag (Alarm)		DC DataFlag 2
229		32			15			Relay Report Method			RelayReport Way		Type Repport Alarme			Type Rep. Alar.
230		32			15			Fixed					Fixed			Standard				Standard
231		32			15			User Defined				User Defined		Configuration utilisateur		Config util
232	32		15		Rectifier Data Flag (Alarm)	RectDataFlag2		Redresseur Data Flag(Alarm)	Red Data Flag2
233		32			15			Master Controlled			Master Ctrlled		Maitre					Maitre
234		32			15			Slave Controlled			Slave Ctrlled		Esclave					Esclave
236		32			15			Over Load Alarm Level			Over Load Lvl		Niveau de surcharge			Niveau surcharg
237		32			15			Overload				Overload		Surcharge				Surcharge
238	32		15		System Data Flag (On-Off)	SysData Flag 1		System Data Flag (On-Off)	SysData Flag 1
239	32		15		System Data Flag (Alarm)	SysData Flag 2		System Data Flag (Alarm)	SysData Flag 2
240		32			15			French Language				French			Langue Française			Français
241		32			15			Imbalance Protection			Imb Protect		Protection Déséquilibrage actif		Prot Désé actif
242		32			15			Enable					Enable			Marche					Marche
243		32			15			Disable					Disable			Arrêt					Arrêt
244		32			15			Buzzer Control				Buzzer Control		Alarme sonore				Alarme sonore
245		32			15			Normal					Normal			Normal					Normal
246		32			15			Disable					Disable			Arrêt					Arrêt
247		32			15			Ambient Temperature Alarm		Amb Temp Alarm		Alarme température ambiante		Alarm Temp
248		32			15			Normal					Normal			Normal					Normal
249		32			15			Low					Low			Bas					Bas
250		32			15			High					High			Haut					Haut
251		32			15			Reallocate Rectifier Address		Realloc Addr		Réatribution adresse redresseurs	Réatrib adresse
252		32			15			Normal					Normal			Normal					Normal
253		32			15			Realloc Addr				Realloc Addr		Réatribution adresse			Réatrib adresse
254		32			15			Test State Set				Test State Set		Test State Set				Test State Set
255		32			15			Normal					Normal			Normal					Normal
256	32		15		Test State			Test State		Etat Test			Etat Test
257		32			15			Minification for Test			Minification		Minification for Test			Minification
258		32			15			Digital Input 1				DI 1			Entree Digital 1			DI 1
259		32			15			Digital Input 2				DI 2			Digital Input 2				DI 2
260		32			15			Digital Input 3				DI 3			Digital Input 3				DI 3
261		32			15			Digital Input 4				DI 4			Digital Input 4				DI 4
262		32			15			System Type Value			Sys Type Value		System Type Value			Sys Type Value
263		32			15			AC/DC Board Type			AC/DCBoardType		Type module AC/DC			Type mod AC/DC
264		32			15			B14C3U1					B14C3U1			B14C3U1					B14C3U1
265		32			15			Large DU				Large DU		Large DU				Large DU
266	32		15		SPD				SPD			Parafoudre			Parafoudre
267		32			15			Temperature 1				Temp 1			Température 1				Temp1
268		32			15			Temperature 2				Temp 2			Température 2				Temp2
269		32			15			Temperature 3				Temp 3			Température 3				Temp3
270		32			15			Temperature 4				Temp 4			Température 4				Temp4
271		32			15			Temperature 5				Temp 5			Température 5				Temp5
272		32			15			Auto Mode				Auto Mode		Mode Auto				Mode Auto
273		32			15			EMEA					EMEA			EMEA					EMEA
274		32			15			Other					Other			Normal					Normal
275		32			15			Float Voltage				Float Volt		Tension floating			V floating
276	32		15		Emergency Stop/Shutdown		Emerg Stop		Arrêt d´urgence		Arrêt d´urgence
277		32			15			Shunt 1 Full Current			Shunt1 Current		Courant Shunt 1				I Shunt 1
278		32			15			Shunt 2 Full Current			Shunt2 Current		Courant Shunt 2				I Shunt 2
279		32			15			Shunt 3 Full Current			Shunt3 Current		Courant Shunt 3				I Shunt 3
280		32			15			Shunt 1 Full Voltage			Shunt1 Voltage		Tension Shunt 1				V Shunt 1
281		32			15			Shunt 2 Full Voltage			Shunt2 Voltage		Tension Shunt 2				V Shunt 2
282		32			15			Shunt 3 Full Voltage			Shunt3 Voltage		Tension Shunt 3				V Shunt 3
283		32			15			Temperature 1				Temp 1			Sonde Temp 1				Sonde Temp1
284		32			15			Temperature 2				Temp 2			Sonde Temp 2				Sonde Temp2
285		32			15			Temperature 3				Temp 3			Sonde Temp 3				Sonde Temp3
286		32			15			Temperature 4				Temp 4			Sonde Temp 4				Sonde Temp4
287		32			15			Temperature 5				Temp 5			Sonde Temp 5				Sonde Temp5
288		32			15			Very High Temperature 1 Limit		VeryHighTemp1		Temp 1 Très haute			Temp 1 T.Haute
289		32			15			Very High Temperature 2 Limit		VeryHighTemp2		Temp 2 Très haute			Temp 2 T.Haute
290		32			15			Very High Temperature 3 Limit		VeryHighTemp3		Temp 3 Très haute			Temp 3 T.Haute
291		32			15			Very High Temperature 4 Limit		VeryHighTemp4		Temp 4 Très haute			Temp 4 T.Haute
292		32			15			Very High Temperature 5 Limit		VeryHighTemp5		Temp 5 Très haute			Temp 5 T.Haute
293		32			15			High Temperature 2 Limit		High Temp2		Température 2 Haute			Temp 2 Haute
294		32			15			High Temperature 3 Limit		High Temp3		Température 3 Haute			Temp 3 Haute
295		32			15			High Temperature 4 Limit		High Temp4		Température 4 Haute			Temp 4 Haute
296		32			15			High Temperature 5 Limit		High Temp5		Température 5 Haute			Temp 5 Haute
297		32			15			Low Temperature 2 Limit			Low Temp2		Température2 Basse			Temp2 Basse
298		32			15			Low Temperature 3 Limit			Low Temp3		Température3 Basse			Temp3 Basse
299		32			15			Low Temperature 4 Limit			Low Temp4		Température4 Basse			Temp4 Basse
300		32			15			Low Temperature 5 Limit			Low Temp5		Température5 Basse			Temp5 Basse
301		32			15			Temperature 1 not Used			Temp1 not Used		Sonde Tempér.1 non utilisée		Temp1 non util.
302		32			15			Temperature 2 not Used			Temp2 not Used		Sonde Tempér.2 non utilisée		Temp2 non util.
303		32			15			Temperature 3 not Used			Temp3 not Used		Sonde Tempér.3 non utilisée		Temp3 non util.
304		32			15			Temperature 4 not Used			Temp4 not Used		Sonde Tempér.4 non utilisée		Temp4 non util.
305		32			15			Temperature 5 not Used			Temp5 not Used		Sonde Tempér.5 non utilisée		Temp5 non util.
306		32			15			High Temperature 2			High Temp 2		Temp 2 Haute				Temp 2 Haute
307		32			15			Low Temperature 2			Low Temp 2		Température2 Basse			Temp2 Basse
308		32			15			Temperature 2 Sensor Fault		T2 Sensor Fault		Défaut capteur Température2		Déf capt Temp2
309		32			15			High Temperature 3			High Temp 3		Température 3 Haute			Temp 3 Haute
310		32			15			Low Temperature 3			Low Temp 3		Température3 Basse			Temp3 Basse
311		32			15			Temperature 3 Sensor Fault		T3 Sensor Fault		Défaut capteur Température3		Déf capt Temp3
312		32			15			High Temperature 4			High Temp 4		Température 4 Haute			Temp 4 Haute
313		32			15			Low Temperature 4			Low Temp 4		Température4 Basse			Temp4 Basse
314		32			15			Temperature 4 Sensor Fault		T4 Sensor Fault		Défaut capteur Température4		Déf capt Temp4
315		32			15			High Temperature 5			High Temp 5		Température 5 Haute			Temp 5 Haute
316		32			15			Low Temperature 5			Low Temp 5		Température5 Basse			Temp5 Basse
317		32			15			Temperature 5 Sensor Fault		T5 Sensor Fault		Défaut capteur Température5		Déf capt Temp5
318		32			15			Very High Temperature 1			Very High Temp1		Température 1 Très haute		Temp 1 T.Haute
319		32			15			Very High Temperature 2			Very High Temp2		Température 2 Très haute		Temp 2 T.Haute
320		32			15			Very High Temperature 3			Very High Temp3		Température 3 Très haute		Temp 3 T.Haute
321		32			15			Very High Temperature 4			Very High Temp4		Température 4 Très haute		Temp 4 T.Haute
322		32			15			Very High Temperature 5			Very High Temp5		Température 5 Très haute		Temp 5 T.Haute
323		32			15			Energy Saving Status			Energy Saving Status	Etat MODE ECO				Etat MODE ECO
324		32			15			Energy Saving				Energy Saving		MODE ECO actif				MODE ECO actif
325		32			15			Internal Use(I2C)			Internal Use		Internal Use(I2C)			Internal Use
326		32			15			Disable					Disable			Arrêt					Arrêt
327		32			15			Emergency Stop				EStop			Arrêt d'urgence				Arrêt d'urgence
328		32			15			Emeregency Shutdown			EShutdown		Arrêt d'urgence				Arrêt d'urgence
329		32			15			Ambient					Ambient			Environment				Environment
330		32			15			Battery					Battery			Batterie				Batterie
331		32			15			Temperature 6				Temp 6			Température6				Temp6
332		32			15			Temperature 7				Temp 7			Température7				Temp7
333		32			15			Temperature 6				Temp 6			Sonde Temp 6				Sonde Temp6
334		32			15			Temperature 7				Temp 7			Sonde Temp 7				Sonde Temp7
335		32			15			Very High Temperature 6 Limit		VeryHighTemp6		Température 6 Très haute		Temp 6 T.Haute
336		32			15			Very High Temperature 7 Limit		VeryHighTemp7		Température 7 Très haute		Temp 7 T.Haute
337		32			15			High Temperature 6 Limit		High Temp 6		Température 6 haute			Temp 6 Haute 
338		32			15			High Temperature 7 Limit		High Temp 7		Température 7 haute			Temp 7 Haute 
339		32			15			Low Temperature 6 Limit			Low Temp 6		Température6 Basse			Temp6 Basse
340		32			15			Low Temperature 7 Limit			Low Temp 7		Température7 Basse			Temp7 Basse
341		32			15			EIB Temperature 1 not Used		EIB T1 not Used		EIB Sonde Tempér.1 non utilisée		EIB T1 non util
342		32			15			EIB Temperature 2 not Used		EIB T2 not Used		EIB Sonde Tempér.2 non utilisée		EIB T2 non util
343		32			15			High Temperature 6			High Temp 6		Température 6 haute 			Temp 6 Haute 
344		32			15			Low Temperature 6			Low Temp 6		Température6 Basse			Temp6 Basse
345		32			15			Temperature 6 Sensor Fault		T6 Sensor Fault		Défaut capteur Température6		Déf capt Temp6
346		32			15			High Temperature 7			High Temp 7		Température 7 haute 			Temp 7 Haute 
347		32			15			Low Temperature 7			Low Temp 7		Température7 Basse			Temp7 Basse
348		32			15			Temperature7 Sensor Fault		T7 Sensor Fault		Défaut capteur Température7		Déf capt Temp7
349		32			15			Very High Temperature 6			Very High Temp6		Température 6 Très haute		Temp 6 T.Haute
350		32			15			Very High Temperature 7			Very High Temp7		Température 7 Très haute		Temp 7 T.Haute
501		32			15			Manual Mode				Manual Mode		Mode Manuel				Mode Manuel
1111		32			15			System Temperature 1			System Temp 1		Système Température 1			Sys Temp1
1131	32		15		System Temperature 1		System Temp 1		Système Température 1		Sys Temp1
1132		32			15			Very High Battery Temp 1 Limit		VHighBattTemp1		Température 1 très haute		Temp 1 Très haut
1133		32			15			High Battery Temp 1 Limit		Hi Batt Temp 1		Température 1 haute			Temp 1 haute
1134		32			15			Low Battery Temp 1 Limit		Lo Batt Temp 1		Température 1 Basse			Temp 1 Basse
1141		32			15			System Temperature 1 not Used		Sys T1 not Used		Tempér.1 système non utilisée		T1 Sys non util
1142		32			15			System Temperature 1 Sensor Fault	Sys T1 Sens Flt		Défaut capteur Système Temp1		Déf capt Temp1
1143		32			15			Very High Battery Temperature 1		VHighBattTemp1		Température 1 Très haute		Temp 1 T.Haute
1144		32			15			High Battery Temperature 1		Hi Batt Temp 1		Température 1 haute			Temp 1 Haute
1145		32			15			Low Battery Temperature 1		Lo Batt Temp 1		Température 1 Basse			Temp1 Basse
1211		32			15			System Temperature 2			System Temp 2		Système Température 2			Sys Temp2
1231	32		15		System Temperature 2		System Temp 2			Système Température 2			Sys Temp2
1232		32			15			Very High Battery Temp 2 Limit		VHighBattTemp2		Température 2 très haute		Temp 2 Très haut
1233		32			15			High Battery Temp 2 Limit		Hi Batt Temp 2		Température 2 haute			Temp 2 haute
1234		32			15			Low Battery Temp 2 Limit		Lo Batt Temp 2		Température 2 Basse			Temp 2 Basse
1241		32			15			System Temperature 2 not Used		Sys T2 not Used		Tempér.2 système non utilisée		T2 Sys non util
1242		32			15			System Temperature 2 Sensor Fault	Sys T2 Sens Flt		Défaut capteur Système Temp3		Déf capt Temp3
1243		32			15			Very High Battery Temperature 2		VHighBattTemp2		Température 2 très haute		Temp 2 Très haut
1244		32			15			High Battery Temperature 2		Hi Batt Temp 2		Température 2 haute			Temp 2 haute
1245		32			15			Low Battery Temperature 2		Lo Batt Temp 2		Température 2 Basse			Temp 2 Basse
1311		32			15			System Temperature 3			System Temp 3		Système Température 3			Sys Temp3
1331	32		15		System Temperature 3		System Temp 3		Système Température 3				Sys Temp3
1332		32			15			Very High Battery Temp 3 Limit		VHighBattTemp3		Température 3 très haute		Temp 3 Très haut
1333		32			15			High Battery Temp 3 Limit		Hi Batt Temp 3		Température 3 haute			Temp 3 haute
1334		32			15			Low Battery Temp 3 Limit		Lo Batt Temp 3		Température 3 Basse			Temp 3 Basse
1341		32			15			System Temperature 3 not Used		Sys T3 not Used		Tempér.3 système non utilisée		T3 Sys non util
1342		32			15			System Temperature 3 Sensor Fault	Sys T3 Sens Flt		Défaut capteur Système Temp3		Déf capt Temp3
1343		32			15			Very High Battery Temperature 3		VHighBattTemp3		Température 3 très haute		Temp 3 Très haut
1344		32			15			High Battery Temperature 3		Hi Batt Temp 3		Température 3 haute			Temp 3 haute
1345		32			15			Low Battery Temperature 3		Lo Batt Temp 3		Température 3 Basse			Temp 3 Basse
1411		32			15			IB2 Temperature 1			IB2 Temp 1		IB2 Température 1			IB2 Temp1
1431		32			15			IB2 Temperature 1			IB2 Temp 1		Activation IB2 Température 1		Act IB2 Temp1
1432		32			15			Very High Battery Temp 4 Limit		VHighBattTemp4		Température 4 très haute		Temp 4 Très haut
1433		32			15			High Battery Temp 4 Limit		Hi Batt Temp 4		Température 4 haute			Temp 4 haute
1434		32			15			Low Battery Temp 4 Limit		Lo Batt Temp 4		Température 4 Basse			Temp 4 Basse
1441		32			15			IB2 Temperature 1 not Used		IB2 T1 not Used		Tempér.1 IB2 non utilisée		T1 IB2 non util
1442		32			15			IB2 Temperature 1 Sensor Fault		IB2 T1 Sens Flt		Défaut capteur IB2 Temp1		Déf capt IB2 T1
1443		32			15			Very High Battery Temperature 4		VHighBattTemp4		Température 4 très haute		Temp 4 Très haut
1444		32			15			High Battery Temperature 4		Hi Batt Temp 4		Température 4 haute			Temp 4 haute
1445		32			15			Low Battery Temperature 4		Lo Batt Temp 4		Température 4 Basse			Temp 4 Basse
1511		32			15			IB2 Temperature 2			IB2 Temp 2		IB2 Température 2			IB2 Temp2
1531		32			15			IB2 Temperature 2			IB2 Temp 2		Activation IB2 Température 2		Act IB2 Temp2
1532		32			15			Very High Battery Temp 5 Limit		VHighBattTemp5		Température 5 très haute		Temp 5 Très haut
1533		32			15			High Battery Temp 5 Limit		Hi Batt Temp 5		Température 5 haute			Temp 5 haute
1534		32			15			Low Battery Temp 5 Limit		Lo Batt Temp 5		Température 5 Basse			Temp 5 Basse
1541		32			15			IB2 Temperature 2 not Used		IB2 T2 not Used		Tempér.2 IB2 non utilisée		T2 IB2 non util
1542		32			15			IB2 Temperature 2 Sensor Fault		IB2 T2 Sens Flt		Défaut capteur IB2 Temp2		Déf capt IB2 T2
1543		32			15			Very High Battery Temperature 5		VHighBattTemp5		Température 5 très haute		Temp 5 Très haut
1544		32			15			High Battery Temperature 5		Hi Batt Temp 5		Température 5 haute			Temp 5 haute
1545		32			15			Low Battery Temperature 5		Lo Batt Temp 5		Température 5 Basse			Temp 5 Basse
1611		32			15			EIB Temperature 1			EIB Temp 1		EIB Température 1			EIB Temp1
1631		32			15			EIB Temperature 1			EIB Temp 1		Activation EIB Température 1		Act EIB Temp1
1632		32			15			Very High Battery Temp 6 Limit		VHighBattTemp6		Température 6 très haute		Temp 6 Très haut
1633		32			15			High Battery Temp 6 Limit		Hi Batt Temp 6		Température 6 haute			Temp 6 haute
1634		32			15			Low Battery Temp 6 Limit		Lo Batt Temp 6		Température 6 Basse			Temp 6 Basse
1641		32			15			EIB Temperature 1 not Used		EIB T1 not Used		Tempér.1 EIB non utilisée		T1 EIB non util
1642		32			15			EIB Temperature 1 Sensor Fault		EIB T1 Sens Flt		Défaut capteur EIB Temp1		Déf capt EIB T1
1643		32			15			Very High Battery Temperature 6		VHighBattTemp6		Température 6 très haute		Temp 6 Très haut
1644		32			15			High Battery Temperature 6		Hi Batt Temp 6		Température 6 haute			Temp 6 haute
1645		32			15			Low Battery Temperature 6		Lo Batt Temp 6		Température 6 Basse			Temp 6 Basse
1711		32			15			EIB Temperature 2			EIB Temp 2		EIB Température 2			EIB Temp2
1731		32			15			EIB Temperature 2			EIB Temp 2		Activation EIB Température 2		Act EIB Temp2
1732		32			15			Very High Battery Temp 7 Limit		VHighBattTemp7		Température 7 très haute		Temp 7 Très haut
1733		32			15			High Battery Temp 7 Limit		Hi Batt Temp 7		Température 8 haute			Temp 8 haute
1734		32			15			Low Battery Temp 7 Limit		Lo Batt Temp 7		Température 7 Basse			Temp 7 Basse
1741		32			15			EIB Temperature 2 not Used		EIB T2 not Used		Tempér.2 EIB non utilisée		T2 EIB non util
1742		32			15			EIB Temperature 2 Sensor Fault		EIB T2 Sens Flt		Défaut capteur IB2 Temp2		Déf capt IB2 T2
1743		32			15			Very High Battery Temperature 7		VHighBattTemp7		Température 7 très haute		Temp 7 Très haut
1744		32			15			High Battery Temperature 7		Hi Batt Temp 7		Température 8 haute			Temp 8 haute
1745		32			15			Low Battery Temperature 7		Lo Batt Temp 7		Température 7 Basse			Temp 7 Basse
1746		32			15			DHCP Failure				DHCP Failure		DHCP Failure				DHCP Failure
1747		32			15			Internal Use(CAN)			Internal Use		Internal Use(CAN)			Internal Use
1748		32			15			Internal Use(485)			Internal Use		Internal Use(485)			Internal Use
1749		32			15			Address of Secondary System		Addr Second		Adresse second système			Address(Slave)
1750		32			15			201					201			201					201
1751		32			15			202					202			202					202
1752		32			15			203					203			203					203
1753		32			15			Master/Slave Mode			Master Mode		Mode Maitre/Esclave			Mode Mait/Escl
1754		32			15			Master					Master			Maitre					Maitre
1755		32			15			Slave					Slave			Esclave					Esclave
1756		32			15			Alone					Alone			Mode seul				Mode seul
1757		32			15			SMBatTemp High Limit			SMBatTemHighLim		Température SMBAT haute			Temp SMBAT haut
1758		32			15			SMBatTemp Low Limit			SMBatTempLowLim		Température SMBAT Basse			Temp SMBAT Bass
1759		32			15			PLC Config Error			PLC Conf Error		Erreur Configuration PLC		Erreur Cfg PLC
1760		32			15			System Expansion			Sys Expansion		Mode de travail				Mode de travail
1761		32			15			Inactive				Inactive		Autonome				Autonome
1762		32			15			Primary					Primary			Maitre					Maitre
1763		32			15			Secondary				Secondary		Esclave					Esclave
1764		128			64			Mode Changed, Auto Config will be started!	Auto Config will be started!	Le mode à Changé! L'auto configuration va démarrer!	L'auto configuration va démarrer!
1765		32			15			Former Lang				Former Lang		Langue Précédente			Langue Précéd.
1766		32			15			Current Lang				Current Lang		Langue Actuelle				Langue Act.
1767		32			15			English					English			Anglais					Anglais
1768		32			15			French					French			Français				Français
1769		32			15			RS485 Communication Failure		485 Comm Fail		Défaut COM RS-485			Déf COM 485
1770		64			32			SMDU Sampler Mode			SMDU Sampler		Mode Mesure SMDU			Mesure SMDU
1771		32			15			CAN					CAN			CAN					CAN
1772		32			15			RS485					RS485			RS485					RS485
1773	32		15		To Auto Delay			To Auto Delay		Retour mode Auto		Retour Auto
1774		32			15			OBSERVATION SUMMARY			OA SUMMARY		Total Observation			Total Obser.
1775		32			15			MAJOR SUMMARY				MA SUMMARY		Total Mineure				Total Mineure
1776		32			15			CRITICAL SUMMARY			CA SUMMARY		Total Majeure				Total Majeure
1777		32			15			All Rectifiers Current			All Rects Curr		Courant Total Redresseur		I Total Red.
1778		32			15			Rectifier Group Lost Status		RectGroup Lost		Perte Groupe Redresseur			Perte Gr.Red.
1779		32			15			Reset Rectifier Lost Alarm		ResetRectLost		Reset Perte Redresseur			Reset Perte Red
1780		32			15			Last GroupRectifiers Num		GroupRect Number	Dernier Groupe Redresseur		Dernier Gr.Red.
1781		32			15			Rectifier Group Lost			Rect Group Lost		Perte Groupe Redresseur			Perte Gr.Red.
1782		32			15			High Ambient Temp 1 Limit		Hi Amb Temp 1		Limite Temp Ambiante1 Haute		Temp1 Amb Haute
1783		32			15			Low Ambient Temp 1 Limit		Lo Amb Temp 1		Limite Temp Ambiante1 Basse		Temp1 Amb Basse
1784		32			15			High Ambient Temp 2 Limit		Hi Amb Temp 2		Limite Temp Ambiante2 Haute		Temp2 Amb Haute
1785		32			15			Low Ambient Temp 2 Limit		Lo Amb Temp 2		Limite Temp Ambiante2 Basse		Temp2 Amb Basse
1786		32			15			High Ambient Temp 3 Limit		Hi Amb Temp 3		Limite Temp Ambiante3 Haute		Temp3 Amb Haute
1787		32			15			Low Ambient Temp 3 Limit		Lo Amb Temp 3		Limite Temp Ambiante3 Basse		Temp3 Amb Basse
1788		32			15			High Ambient Temp 4 Limit		Hi Amb Temp 4		Limite Temp Ambiante4 Haute		Temp4 Amb Haute
1789		32			15			Low Ambient Temp 4 Limit		Lo Amb Temp 4		Limite Temp Ambiante4 Basse		Temp4 Amb Basse
1790		32			15			High Ambient Temp 5 Limit		Hi Amb Temp 5		Limite Temp Ambiante5 Haute		Temp5 Amb Haute
1791		32			15			Low Ambient Temp 5 Limit		Lo Amb Temp 5		Limite Temp Ambiante5 Basse		Temp5 Amb Basse
1792		32			15			High Ambient Temp 6 Limit		Hi Amb Temp 6		Limite Temp Ambiante6 Haute		Temp6 Amb Haute
1793		32			15			Low Ambient Temp 6 Limit		Lo Amb Temp 6		Limite Temp Ambiante6 Basse		Temp6 Amb Basse
1794		32			15			High Ambient Temp 7 Limit		Hi Amb Temp 7		Limite Temp Ambiante7 Haute		Temp7 Amb Haute
1795		32			15			Low Ambient Temp 7 Limit		Lo Amb Temp 7		Limite Temp Ambiante7 Basse		Temp7 Amb Basse
1796		32			15			High Ambient Temperature 1		Hi Amb Temp 1		Temp Ambiante1 Haute			Temp Amb1 Haute
1797		32			15			Low Ambient Temperature 1		Lo Amb Temp 1		Temp Ambiante1 Basse			Temp Amb1 Basse
1798		32			15			High Ambient Temperature 2		Hi Amb Temp 2		Temp Ambiante2 Haute			Temp Amb2 Haute
1799		32			15			Low Ambient Temperature 2		Lo Amb Temp 2		Temp Ambiante2 Basse			Temp Amb2 Basse
1800		32			15			High Ambient Temperature 3		Hi Amb Temp 3		Temp Ambiante3 Haute			Temp Amb3 Haute
1801		32			15			Low Ambient Temperature 3		Lo Amb Temp 3		Temp Ambiante3 Basse			Temp Amb3 Basse
1802		32			15			High Ambient Temperature 4		Hi Amb Temp 4		Temp Ambiante4 Haute			Temp Amb4 Haute
1803		32			15			Low Ambient Temperature 4		Lo Amb Temp 4		Temp Ambiante4 Basse			Temp Amb4 Basse
1804		32			15			High Ambient Temperature 5		Hi Amb Temp 5		Temp Ambiante5 Haute			Temp Amb5 Haute
1805		32			15			Low Ambient Temperature 5		Lo Amb Temp 5		Temp Ambiante5 Basse			Temp Amb5 Basse
1806		32			15			High Ambient Temperature 6		Hi Amb Temp 6		Temp Ambiante6 Haute			Temp Amb6 Haute
1807		32			15			Low Ambient Temperature 6		Lo Amb Temp 6		Temp Ambiante6 Basse			Temp Amb6 Basse
1808		32			15			High Ambient Temperature 7		Hi Amb Temp 7		Temp Ambiante7 Haute			Temp Amb7 Haute
1809		32			15			Low Ambient Temperature 7		Lo Amb Temp 7		Temp Ambiante7 Basse			Temp Amb7 Basse
1810		32			15			Fast Sampler Flag			Fast SamplFlag		Fast Sampler Flag			Fast SamplFlag
1811		32			15			LVD Quantity				LVD Quantity		Nombre de Contacteur			Nombre de LVDs
1812		32			15			LCD Rotation				LCD Rotation		Rotation Ecran				Rotation LCD
1813		32			15			0 deg					0 deg			0 Degré					0 Degré
1814		32			15			90 deg					90 deg			90 Degrés				90 Degrés
1815		32			15			180 deg					180 deg			180 Degrés				180 Degrés
1816		32			15			270 deg					270 deg			270 Degrés				270 Degrés
1817	32		15		Overvoltage 1			Overvolt 1		Surtension 1			Surtension 1
1818	32		15		Overvoltage 2			Overvolt 2		Surtension 2			Surtension 2
1819	32		15		Undervoltage 1			Undervolt 1		Soustension 1			Soustension 1
1820	32		15		Undervoltage 2			Undervolt 2		Soustension 2			Soustension 2
1821	32		15		Overvoltage 1			Overvolt 1		Surtension 1 (24V)		Surtension1 24V
1822	32		15		Overvoltage 2			Overvolt 2		Surtension 2 (24V)		Surtension2 24V 
1823	32		15		Undervoltage 1			Undervolt 1		Soustension 1 (24V)		Soustens.1 24V
1824	32		15		Undervoltage 2			Undervolt 2		Soustension 2 (24V)		Soustens.2 24V
1825		32			15			Fail Safe Mode				Fail Safe		Défaut Mode Sécurité			Déf.Mode Sécu.
1826		32			15			Disable					Disable			Arrêt					Arrêt
1827		32			15			Enable					Enable			Marche					Marche
1828		32			15			Hybrid Mode				Hybrid Mode		Mode Hybride				Mode Hybride½
1829		32			15			Disable					Disable			Arrêt					Arrêt
1830		32			15			Capacity				Capacity		Capacité				Capacité
1831		32			15			Cyclic					Cyclic			Cycle					Cycle
1832		32			15			DG Run at High Temperature		DG Run Hi Temp		Fonction. GE à Température Haute	Fonct.GE Temp.H
1833		32			15			Used DG for Hybrid			Used DG			Utilisation GE pour Hybride		Util.GE Hybrid.
1834		32			15			DG1					DG1			Générateur 1				Générateur 1
1835		32			15			DG2					DG1			Générateur 2				Générateur 2
1836		32			15			Both					Both			Ensemble				Ensemble
1837		32			15			DI for Grid				DI for Grid		Entrée Pour Secteur			Entrée Secteur
1838		32			15			DI 1					DI 1			Entrée 1				Entrée 1
1839		32			15			DI 2					DI 2			Entrée 2				Entrée 2
1840		32			15			DI 3					DI 3			Entrée 3				Entrée 3
1841		32			15			DI 4					DI 4			Entrée 4				Entrée 4
1842		32			15			DI 5					DI 5			Entrée 5				Entrée 5
1843		32			15			DI 6					DI 6			Entrée 6				Entrée 6
1844		32			15			DI 7					DI 7			Entrée 7				Entrée 7
1845		32			15			DI 8					DI 8			Entrée 8				Entrée 8
1846		32			15			Depth of Discharge			DepthDisch		Profondeur de Décharge			Prof. Décharge
1847		32			15			Discharge Duration			Disch Duration		Durée Décharge				Durée Décharge
1848		32			15			Discharge Start time			Disch Start		Heure démarrage décharge		Heure Dém.Décha
1849		32			15			Diesel Run Over Temperature		DG Run OverTemp		Fonction. GE à Température Haute	Fonct.GE Temp.H
1850		32			15			DG1 is Running				DG1 is Running		GE 1 En Marche				GE 1 En Marche
1851		32			15			DG2 is Running				DG2 is Running		GE 2 En Marche				GE 2 En Marche
1852		32			15			Hybrid High Load Level			High Load Lvl		Charge Max Hybride			Charge Max Hybr
1853		32			15			Hybrid is High Load			High Load		Charge Haute Hybride			Char.Haut.Hybri
1854		32			15			DG Run Time at High Temperature		DG Run HiTemp		Fonction. GE à Température Haute	Fonct.GE Temp.H
1855		32			15			Equalising Start Time			EqualStartTime		Heure démarrage Egalisation		Heure Dém.Egali.
1856		32			15			DG1 Failure				DG1 Failure		Défaut GE1				Défaut GE1
1857		32			15			DG2 Failure				DG2 Failure		Défaut GE2				Défaut GE2
1858		32			15			Diesel Alarm Delay			DG Alarm Delay		Délais Alarme GE			Délais Alarm GE
1859		32			15			Grid is on				Grid is on		Secteur ON				Secteur ON
1860		32			15			PowerSplit Contactor Mode		Contactor Mode		Mode contacteur PowerSplit		Mode LVD PSplit
1861		32			15			Ambient Temperature			Amb Temp		Température Ambiante			Temp.Amb
1862		32			15			Ambient Temperature Sensor		Amb Temp Sensor		Capteur Temp.Ambiante			Capt. Temp.Amb.
1863		32			15			None					None			Aucun					Aucun
1864		32			15			Temperature 1				Temp 1			Capteur Temp.1				Capteur Temp. 1
1865		32			15			Temperature 2				Temp 2			Capteur Temp.2				Capteur Temp. 2
1866		32			15			Temperature 3				Temp 3			Capteur Temp.3				Capteur Temp. 3
1867		32			15			Temperature 4				Temp 4			Capteur Temp.4				Capteur Temp. 4
1868		32			15			Temperature 5				Temp 5			Capteur Temp.5				Capteur Temp. 5
1869		32			15			Temperature 6				Temp 6			Capteur Temp.6				Capteur Temp. 6
1870		32			15			Temperature 7				Temp 7			Capteur Temp.7				Capteur Temp.7
1871		32			15			Temperature 8				Temp 8			Capteur Temp.8				Capteur Temp.8
1872		32			15			Temperature 9				Temp 9			Capteur Temp.9				Capteur Temp.9
1873		32			15			Temperature 10				Temp 10			Capteur Temp.10				Capteur Temp.10
1874		32			15			High Ambient Temperature Level		High Amb Temp		Température Haute			Temp. Haute
1875		32			15			Low Ambient Temperature Level		Low Amb Temp		Température Basse			Temp. Basse
1876		32			15			High Ambient Temperature		High Temperature	Température Haute			Temp. Haute
1877		32			15			Low Ambient Temperature			Low Temperature		Température Basse			Temp. Basse
1878		32			15			Ambient Temp Sensor Fault		Amb Sensor Flt		Défaut Capteur Température		Déf.Cap.Temp.
1879		32			15			Digital Input 1				DI 1			Entrée 1				Entrée 1
1880		32			15			Digital Input 2				DI 2			Entrée 2				Entrée 2
1881		32			15			Digital Input 3				DI 3			Entrée 3				Entrée 3
1882		32			15			Digital Input 4				DI 4			Entrée 4				Entrée 4
1883		32			15			Digital Input 5				DI 5			Entrée 5				Entrée 5
1884		32			15			Digital Input 6				DI 6			Entrée 6				Entrée 6
1885		32			15			Digital Input 7				DI 7			Entrée 7				Entrée 7
1886		32			15			Digital Input 8				DI 8			Entrée 8				Entrée 8
1887		32			15			Open					Open			Ouvert					Ouvert
1888		32			15			Closed					Closed			Fermé					Fermé
1889		32			15			Relay Output 1				Relay Output 1		Sortie Relais 1				Sortie Relais 1
1890		32			15			Relay Output 2				Relay Output 2		Sortie Relais 2				Sortie Relais 2
1891		32			15			Relay Output 3				Relay Output 3		Sortie Relais 3				Sortie Relais 3
1892		32			15			Relay Output 4				Relay Output 4		Sortie Relais 4				Sortie Relais 4
1893		32			15			Relay Output 5				Relay Output 5		Sortie Relais 5				Sortie Relais 5
1894		32			15			Relay Output 6				Relay Output 6		Sortie Relais 6				Sortie Relais 6
1895		32			15			Relay Output 7				Relay Output 7		Sortie Relais 7				Sortie Relais 7
1896		32			15			Relay Output 8				Relay Output 8		Sortie Relais 8				Sortie Relais 8
1897		32			15			DI1 Alarm State				DI1 Alarm State		Etat Alarme Entrée 1			Etat Al.Entrée1
1898		32			15			DI2 Alarm State				DI2 Alarm State		Etat Alarme Entrée 2			Etat Al.Entrée2
1899		32			15			DI3 Alarm State				DI3 Alarm State		Etat Alarme Entrée 3			Etat Al.Entrée3
1900		32			15			DI4 Alarm State				DI4 Alarm State		Etat Alarme Entrée 4			Etat Al.Entrée4
1901		32			15			DI5 Alarm State				DI5 Alarm State		Etat Alarme Entrée 5			Etat Al.Entrée5
1902		32			15			DI6 Alarm State				DI6 Alarm State		Etat Alarme Entrée 6			Etat Al.Entrée6
1903		32			15			DI7 Alarm State				DI7 Alarm State		Etat Alarme Entrée 7			Etat Al.Entrée7
1904		32			15			DI8 Alarm State				DI8 Alarm State		Etat Alarme Entrée 8			Etat Al.Entrée8
1905		32			15			DI 1 Fault				DI 1 Fault		Alarme Entrée 1				Alarme Entrée 1
1906		32			15			DI 2 Fault				DI 2 Fault		Alarme Entrée 2				Alarme Entrée 2
1907		32			15			DI 3 Fault				DI 3 Fault		Alarme Entrée 3				Alarme Entrée 3
1908		32			15			DI 4 Fault				DI 4 Fault		Alarme Entrée 4				Alarme Entrée 4
1909		32			15			DI 5 Fault				DI 5 Fault		Alarme Entrée 5				Alarme Entrée 5
1910		32			15			DI 6 Fault				DI 6 Fault		Alarme Entrée 6				Alarme Entrée 6
1911		32			15			DI 7 Fault				DI 7 Fault		Alarme Entrée 7				Alarme Entrée 7
1912		32			15			DI 8 Fault				DI 8 Fault		Alarme Entrée 8				Alarme Entrée 8
1913		32			15			On					On			Marche					Marche
1914		32			15			Off					Off			Arrêt					Arrêt
1915		32			15			High					High			Niveau Haut				Niveau Haut
1916		32			15			Low					Low			Niveau Bas				Niveau Bas
1917		32			15			IB Number				IB Number		Nombre carte IB				Nbre carte IB
1918		32			15			IB Type					IB Type			Type carte IB				Type carte IB
1919		32			15			CSU Undervoltage 1			CSU UV1			CSU Sous-Tension 1			CSU Sous U 1
1920		32			15			CSU Undervoltage 2			CSU UV2			CSU Sous-Tension 2			CSU Sous U 2
1921		32			15			CSU Overvoltage				CSU OV			CSU Sur-Tension 2			CSU Sur U 2
1922		32			15			CSU Communication Fault			CSU Comm Fault		CSU Défaut Communication		CSU Def.Comm.
1923		32			15			CSU External Alarm 1			CSU Ext Al1		CSU Alarme Externe 1			CSU Al.Ext.1
1924		32			15			CSU External Alarm 2			CSU Ext Al2		CSU Alarme Externe 2			CSU Al.Ext.2
1925		32			15			CSU External Alarm 3			CSU Ext Al3		CSU Alarme Externe 3			CSU Al.Ext.3
1926		32			15			CSU External Alarm 4			CSU Ext Al4		CSU Alarme Externe 4			CSU Al.Ext.4
1927		32			15			CSU External Alarm 5			CSU Ext Al5		CSU Alarme Externe 5			CSU Al.Ext.5
1928		32			15			CSU External Alarm 6			CSU Ext Al6		CSU Alarme Externe 6			CSU Al.Ext.6
1929		32			15			CSU External Alarm 7			CSU Ext Al7		CSU Alarme Externe 7			CSU Al.Ext.7
1930		32			15			CSU External Alarm 8			CSU Ext Al8		CSU Alarme Externe 8			CSU Al.Ext.8
1931		32			15			CSU External Communication Fault	CSUExtComm		CSU External Défaut Communication	CSU Ext.Déf.Comm
1932		32			15			CSU Battery Current Limit Alarm		CSUBattCurrLim		CSU Batterie Alarme Limite Courant	CSUBat Alarm.Lim.I
1933		32			15			DCLCNumber				DCLCNumber		Nombre DCLC				Nombre DCLC
1934		32			15			BatLCNumber				BatLCNumber		Nombre BatLC				Nombre BatLC
1935		32			15			Very High Ambient Temperature		VHi Amb Temp		Très haute Température			Très haute Temp
1936		32			15			System Type				System Type		System Type				System Type
1937		32			15			Normal					Normal			Normal					Normal
1938		32			15			Test					Test			Test					Test
1939		32			15			Auto Mode				Auto Mode		Mode Automatique			Mode Auto.
1940		32			15			EMEA					EMEA			EMEA					EMEA
1941		32			15			Normal					Normal			Normal					Normal
1942		32			15			Bus Run Mode				Bus Run Mode		Bus Run Mode				Bus Run Mode
1943		32			15			NO					NO			NO					NO
1944		32			15			NC					NC			NC					NC
1945		32			15			Fail Safe Mode(Hybrid)			Fail Safe		Fail Safe Mode(Hybrid)			Fail Safe
1946		32			15			IB Communication Failure		IB Comm Fail		IB Défaut communication			IB Défaut Com
1947		32			15			Relay Testing				Relay Testing		Test relais				Test relais
1948		32			15			Testing Relay 1				Testing Relay 1		Test relais 1				Test relais 1
1949		32			15			Testing Relay 2				Testing Relay 2		Test relais 2				Test relais 2
1950		32			15			Testing Relay 3				Testing Relay 3		Test relais 3				Test relais 3
1951		32			15			Testing Relay 4				Testing Relay 4		Test relais 4				Test relais 4
1952		32			15			Testing Relay 5				Testing Relay 5		Test relais 5				Test relais 5
1953		32			15			Testing Relay 6				Testing Relay 6		Test relais 6				Test relais 6
1954		32			15			Testing Relay 7				Testing Relay 7		Test relais 7				Test relais 7
1955		32			15			Testing Relay 8				Testing Relay 8		Test relais 8				Test relais 8
1956		32			15			Relay Test				Relay Test		Test relais				Test relais
1957		32			15			Relay Test Time				Relay Test Time		Heure de test relais			Heure test rel.
1958		32			15			Manual					Manual			Manuel					Manuel
1959		32			15			Automatic				Automatic		Automatique				Automatique
1960		32			15			System Temperature 1			Sys Temp1		Temp1 (ACU)				Temp1 (ACU)
1961		32			15			System Temperature 2			Sys Temp2		Temp2 (ACU)				Temp2 (ACU)
1962		32			15			System Temperature 3			Sys Temp3		Temp3 (OB)				Temp3 (OB)
1963		32			15			IB2 Temperature 1			IB2 Temp1		IB2-Temp1				IB2-Temp1
1964		32			15			IB2 Temperature 2			IB2 Temp2		IB2-Temp2				IB2-Temp2
1965		32			15			EIB Temperature 1			EIB Temp1		EIB-Temp1				EIB-Temp1
1966		32			15			EIB Temperature 2			EIB Temp2		EIB-Temp2				EIB-Temp2
1967		32			15			SMTemp1 Temp1				SMTemp1 T1		SMTemp1 Temp1				SMTemp1 Temp1
1968		32			15			SMTemp1 Temp2				SMTemp1 T2		SMTemp1 Temp2				SMTemp1 Temp2
1969		32			15			SMTemp1 Temp3				SMTemp1 T3		SMTemp1 Temp3				SMTemp1 Temp3
1970		32			15			SMTemp1 Temp4				SMTemp1 T4		SMTemp1 Temp4				SMTemp1 Temp4
1971		32			15			SMTemp1 Temp5				SMTemp1 T5		SMTemp1 Temp5				SMTemp1 Temp5
1972		32			15			SMTemp1 Temp6				SMTemp1 T6		SMTemp1 Temp6				SMTemp1 Temp6
1973		32			15			SMTemp1 Temp7				SMTemp1 T7		SMTemp1 Temp7				SMTemp1 Temp7
1974		32			15			SMTemp1 Temp8				SMTemp1 T8		SMTemp1 Temp8				SMTemp1 Temp8
1975		32			15			SMTemp2 Temp1				SMTemp2 T1		SMTemp2 Temp1				SMTemp2 Temp1
1976		32			15			SMTemp2 Temp2				SMTemp2 T2		SMTemp2 Temp2				SMTemp2 Temp2
1977		32			15			SMTemp2 Temp3				SMTemp2 T3		SMTemp2 Temp3				SMTemp2 Temp3
1978		32			15			SMTemp2 Temp4				SMTemp2 T4		SMTemp2 Temp4				SMTemp2 Temp4
1979		32			15			SMTemp2 Temp5				SMTemp2 T5		SMTemp2 Temp5				SMTemp2 Temp5
1980		32			15			SMTemp2 Temp6				SMTemp2 T6		SMTemp2 Temp6				SMTemp2 Temp6
1981		32			15			SMTemp2 Temp7				SMTemp2 T7		SMTemp2 Temp7				SMTemp2 Temp7
1982		32			15			SMTemp2 Temp8				SMTemp2 T8		SMTemp2 Temp8				SMTemp2 Temp8
1983		32			15			SMTemp3 Temp1				SMTemp3 T1		SMTemp3 Temp1				SMTemp3 Temp1
1984		32			15			SMTemp3 Temp2				SMTemp3 T2		SMTemp3 Temp2				SMTemp3 Temp2
1985		32			15			SMTemp3 Temp3				SMTemp3 T3		SMTemp3 Temp3				SMTemp3 Temp3
1986		32			15			SMTemp3 Temp4				SMTemp3 T4		SMTemp3 Temp4				SMTemp3 Temp4
1987		32			15			SMTemp3 Temp5				SMTemp3 T5		SMTemp3 Temp5				SMTemp3 Temp5
1988		32			15			SMTemp3 Temp6				SMTemp3 T6		SMTemp3 Temp6				SMTemp3 Temp6
1989		32			15			SMTemp3 Temp7				SMTemp3 T7		SMTemp3 Temp7				SMTemp3 Temp7
1990		32			15			SMTemp3 Temp8				SMTemp3 T8		SMTemp3 Temp8				SMTemp3 Temp8
1991		32			15			SMTemp4 Temp1				SMTemp4 T1		SMTemp4 Temp1				SMTemp4 Temp1
1992		32			15			SMTemp4 Temp2				SMTemp4 T2		SMTemp4 Temp2				SMTemp4 Temp2
1993		32			15			SMTemp4 Temp3				SMTemp4 T3		SMTemp4 Temp3				SMTemp4 Temp3
1994		32			15			SMTemp4 Temp4				SMTemp4 T4		SMTemp4 Temp4				SMTemp4 Temp4
1995		32			15			SMTemp4 Temp5				SMTemp4 T5		SMTemp4 Temp5				SMTemp4 Temp5
1996		32			15			SMTemp4 Temp6				SMTemp4 T6		SMTemp4 Temp6				SMTemp4 Temp6
1997		32			15			SMTemp4 Temp7				SMTemp4 T7		SMTemp4 Temp7				SMTemp4 Temp7
1998		32			15			SMTemp4 Temp8				SMTemp4 T8		SMTemp4 Temp8				SMTemp4 Temp8
1999		32			15			SMTemp5 Temp1				SMTemp5 T1		SMTemp5 Temp1				SMTemp5 Temp1
2000		32			15			SMTemp5 Temp2				SMTemp5 T2		SMTemp5 Temp2				SMTemp5 Temp2
2001		32			15			SMTemp5 Temp3				SMTemp5 T3		SMTemp5 Temp3				SMTemp5 Temp3
2002		32			15			SMTemp5 Temp4				SMTemp5 T4		SMTemp5 Temp4				SMTemp5 Temp4
2003		32			15			SMTemp5 Temp5				SMTemp5 T5		SMTemp5 Temp5				SMTemp5 Temp5
2004		32			15			SMTemp5 Temp6				SMTemp5 T6		SMTemp5 Temp6				SMTemp5 Temp6
2005		32			15			SMTemp5 Temp7				SMTemp5 T7		SMTemp5 Temp7				SMTemp5 Temp7
2006		32			15			SMTemp5 Temp8				SMTemp5 T8		SMTemp5 Temp8				SMTemp5 Temp8
2007		32			15			SMTemp6 Temp1				SMTemp6 T1		SMTemp6 Temp1				SMTemp6 Temp1
2008		32			15			SMTemp6 Temp2				SMTemp6 T2		SMTemp6 Temp2				SMTemp6 Temp2
2009		32			15			SMTemp6 Temp3				SMTemp6 T3		SMTemp6 Temp3				SMTemp6 Temp3
2010		32			15			SMTemp6 Temp4				SMTemp6 T4		SMTemp6 Temp4				SMTemp6 Temp4
2011		32			15			SMTemp6 Temp5				SMTemp6 T5		SMTemp6 Temp5				SMTemp6 Temp5
2012		32			15			SMTemp6 Temp6				SMTemp6 T6		SMTemp6 Temp6				SMTemp6 Temp6
2013		32			15			SMTemp6 Temp7				SMTemp6 T7		SMTemp6 Temp7				SMTemp6 Temp7
2014		32			15			SMTemp6 Temp8				SMTemp6 T8		SMTemp6 Temp8				SMTemp6 Temp8
2015		32			15			SMTemp7 Temp1				SMTemp7 T1		SMTemp7 Temp1				SMTemp7 Temp1
2016		32			15			SMTemp7 Temp2				SMTemp7 T2		SMTemp7 Temp2				SMTemp7 Temp2
2017		32			15			SMTemp7 Temp3				SMTemp7 T3		SMTemp7 Temp3				SMTemp7 Temp3
2018		32			15			SMTemp7 Temp4				SMTemp7 T4		SMTemp7 Temp4				SMTemp7 Temp4
2019		32			15			SMTemp7 Temp5				SMTemp7 T5		SMTemp7 Temp5				SMTemp7 Temp5
2020		32			15			SMTemp7 Temp6				SMTemp7 T6		SMTemp7 Temp6				SMTemp7 Temp6
2021		32			15			SMTemp7 Temp7				SMTemp7 T7		SMTemp7 Temp7				SMTemp7 Temp7
2022		32			15			SMTemp7 Temp8				SMTemp7 T8		SMTemp7 Temp8				SMTemp7 Temp8
2023		32			15			SMTemp8 Temp1				SMTemp8 T1		SMTemp8 Temp1				SMTemp8 Temp1
2024		32			15			SMTemp8 Temp2				SMTemp8 T2		SMTemp8 Temp2				SMTemp8 Temp2
2025		32			15			SMTemp8 Temp3				SMTemp8 T3		SMTemp8 Temp3				SMTemp8 Temp3
2026		32			15			SMTemp8 Temp4				SMTemp8 T4		SMTemp8 Temp4				SMTemp8 Temp4
2027		32			15			SMTemp8 Temp5				SMTemp8 T5		SMTemp8 Temp5				SMTemp8 Temp5
2028		32			15			SMTemp8 Temp6				SMTemp8 T6		SMTemp8 Temp6				SMTemp8 Temp6
2029		32			15			SMTemp8 Temp7				SMTemp8 T7		SMTemp8 Temp7				SMTemp8 Temp7
2030		32			15			SMTemp8 Temp8				SMTemp8 T8		SMTemp8 Temp8				SMTemp8 Temp8
2031		32			15			System Temperature 1 Very High		Sys T1 VHi		Température 1 Système Très haute	T1.Sys Très hau
2032		32			15			System Temperature 1 High		Sys T1 Hi		Température 1 Système Haute		T1.Sys Haute
2033		32			15			System Temperature 1 Low		Sys T1 Low		Température 1 Système Basse		T1.Sys Basse
2034		32			15			System Temperature 2 Very High		Sys T2 VHi		Température 2 Système Très haute	T2.Sys Très hau
2035		32			15			System Temperature 2 High		Sys T2 Hi		Température 2 Système Haute		T2.Sys Haute
2036		32			15			System Temperature 2 High		Sys T2 Hi		Température 2 Système Basse		T2.Sys Basse
2037		32			15			System Temperature 3 Very High		Sys T3 VHi		Température 3 Système Très haute	T3.Sys Très hau
2038		32			15			System Temperature 3 Low		Sys T3 Low		Température 3 Système Haute		T3.Sys Haute
2039		32			15			System Temperature 3 Low		Sys T3 Low		Température 3 Système Basse		T3.Sys Basse
2040		32			15			IB2 Temperature 1 Very High		IB2 T1 VHi		Température 1 IB2 Très haute		T1.IB2 Très hau
2041		32			15			IB2 Temperature 1 High			IB2 T1 Hi		Température 1 IB2 Haute			T1.IB2 Très Haute
2042		32			15			IB2 Temperature 1 Low			IB2 T1 Low		Température 1 IB2 Basse			T1.IB2 Basse
2043		32			15			IB2 Temperature 2 Very High		IB2 T2 VHi		Température 2 IB2 Très haute		T2.IB2 Très hau
2043		32			15			IB2 Temperature 2 Very High		IB2 T2 VHi		Température 2 IB2 Très haute		T2.IB2 Très hau
2044		32			15			IB2 Temperature 2 High			IB2 T2 Hi		Température 2 IB2 Haute			T2.IB2 Haute
2045		32			15			IB2 Temperature 2 Low			IB2 T2 Low		Température 2 IB2 Basse			T2.IB2 Basse
2046		32			15			EIB Temperature 1 Very High 		EIB T1 VHi		Température 1 EIB Très haute		T1.EIB Très hau
2047		32			15			EIB Temperature 1 High			EIB T1 Hi		Température 1 EIB Haute			T1.EIB Haute
2048		32			15			EIB Temperature 1 Low			EIB T1 Low		Température 1 EIB Basse			T1.EIB Basse
2049		32			15			EIB Temperature 2 Very High 		EIB T2 VHi		Température 2 EIB Très haute		T2.EIB Très hau
2050		32			15			EIB Temperature 2 High			EIB T2 Hi		Température 2 EIB Haute			T2.EIB Haute
2051		32			15			EIB Temperature 2 Low			EIB T2 Low		Température 2 EIB Basse			T2.EIB Basse
2052		32			15			SMTemp1 Temp1 High 2			SMTemp1 T1 Hi2		SMTemp1 Temp 1 Très haute		SMTemp1 T1 THau
2053		32			15			SMTemp1 Temp1 High 1			SMTemp1 T1 Hi1		SMTemp1 Temp 1 Haute			SMTemp1 T1 HauT
2054		32			15			SMTemp1 Temp1 Low			SMTemp1 T1 Low		SMTemp1 Temp 1 Basse			SMTemp1 T1 Bass
2055		32			15			SMTemp1 Temp2 High 2			SMTemp1 T2 Hi2		SMTemp1 Temp 2 Très haute		SMTemp1 T2 THau
2056		32			15			SMTemp1 Temp2 High 1			SMTemp1 T2 Hi1		SMTemp1 Temp 2 Haute			SMTemp1 T2 HauT
2057		32			15			SMTemp1 Temp2 Low			SMTemp1 T2 Low		SMTemp1 Temp 2 Basse			SMTemp1 T2 Bass
2058		32			15			SMTemp1 Temp3 High 2			SMTemp1 T3 Hi2		SMTemp1 Temp 3 Très haute		SMTemp1 T3 THau
2059		32			15			SMTemp1 Temp3 High 1			SMTemp1 T3 Hi1		SMTemp1 Temp 3 Haute			SMTemp1 T3 HauT
2060		32			15			SMTemp1 Temp3 Low			SMTemp1 T3 Low		SMTemp1 Temp 3 Basse			SMTemp1 T3 Bass
2061		32			15			SMTemp1 Temp4 High 2			SMTemp1 T4 Hi2		SMTemp1 Temp 4 Très haute		SMTemp1 T4 THau
2062		32			15			SMTemp1 Temp4 High 1			SMTemp1 T4 Hi1		SMTemp1 Temp 4 Haute			SMTemp1 T4 HauT
2063		32			15			SMTemp1 Temp4 Low			SMTemp1 T4 Low		SMTemp1 Temp 4 Basse			SMTemp1 T4 Bass
2064		32			15			SMTemp1 Temp5 High 2			SMTemp1 T5 Hi2		SMTemp1 Temp 5 Très haute		SMTemp1 T5 THau
2065		32			15			SMTemp1 Temp5 High 1			SMTemp1 T5 Hi1		SMTemp1 Temp 5 Haute			SMTemp1 T5 HauT
2066		32			15			SMTemp1 Temp5 Low			SMTemp1 T5 Low		SMTemp1 Temp 5 Basse			SMTemp1 T5 Bass
2067		32			15			SMTemp1 Temp6 High 2			SMTemp1 T6 Hi2		SMTemp1 Temp 6 Très haute		SMTemp1 T6 THau
2068		32			15			SMTemp1 Temp6 High 1			SMTemp1 T6 Hi1		SMTemp1 Temp 6 Haute			SMTemp1 T6 HauT
2069		32			15			SMTemp1 Temp6 Low			SMTemp1 T6 Low		SMTemp1 Temp 6 Basse			SMTemp1 T6 Bass
2070		32			15			SMTemp1 Temp7 High 2			SMTemp1 T7 Hi2		SMTemp1 Temp 7 Très haute		SMTemp1 T7 THau
2071		32			15			SMTemp1 Temp7 High 1			SMTemp1 T7 Hi1		SMTemp1 Temp 7 Haute			SMTemp1 T7 HauT
2072		32			15			SMTemp1 Temp7 Low			SMTemp1 T7 Low		SMTemp1 Temp 7 Basse			SMTemp1 T7 Bass
2073		32			15			SMTemp1 Temp8 High 2			SMTemp1 T8 Hi2		SMTemp1 Temp 8 Très haute		SMTemp1 T8 THau
2074		32			15			SMTemp1 Temp8 High 1			SMTemp1 T8 Hi1		SMTemp1 Temp 8 Haute			SMTemp1 T8 HauT
2075		32			15			SMTemp1 Temp8 Low			SMTemp1 T8 Low		SMTemp1 Temp 8 Basse			SMTemp1 T8 Bass
2076		32			15			SMTemp2 Temp1 High 2			SMTemp2 T1 Hi2		SMTemp2 Temp 1 Très haute		SMTemp2 T1 THau
2077		32			15			SMTemp2 Temp1 High 1			SMTemp2 T1 Hi1		SMTemp2 Temp 1 Haute			SMTemp2 T1 HauT
2078		32			15			SMTemp2 Temp1 Low			SMTemp2 T1 Low		SMTemp2 Temp 1 Basse			SMTemp2 T1 Bass
2079		32			15			SMTemp2 Temp2 High 2			SMTemp2 T2 Hi2		SMTemp2 Temp 2 Très haute		SMTemp2 T2 THau
2080		32			15			SMTemp2 Temp2 High 1			SMTemp2 T2 Hi1		SMTemp2 Temp 2 Haute			SMTemp2 T2 HauT
2081		32			15			SMTemp2 Temp2 Low			SMTemp2 T2 Low		SMTemp2 Temp 2 Basse			SMTemp2 T2 Bass
2082		32			15			SMTemp2 Temp3 High 2			SMTemp2 T3 Hi2		SMTemp2 Temp 3 Très haute		SMTemp2 T3 THau
2083		32			15			SMTemp2 Temp3 High 1			SMTemp2 T3 Hi1		SMTemp2 Temp 3 Haute			SMTemp2 T3 HauT
2084		32			15			SMTemp2 Temp3 Low			SMTemp2 T3 Low		SMTemp2 Temp 3 Basse			SMTemp2 T3 Bass
2085		32			15			SMTemp2 Temp4 High 2			SMTemp2 T4 Hi2		SMTemp2 Temp 4 Très haute		SMTemp2 T4 THau
2086		32			15			SMTemp2 Temp4 High 1			SMTemp2 T4 Hi1		SMTemp2 Temp 4 Haute			SMTemp2 T4 HauT
2087		32			15			SMTemp2 Temp4 Low			SMTemp2 T4 Low		SMTemp2 Temp 4 Basse			SMTemp2 T4 Bass
2088		32			15			SMTemp2 Temp5 High 2			SMTemp2 T5 Hi2		SMTemp2 Temp 5 Très haute		SMTemp2 T5 THau
2089		32			15			SMTemp2 Temp5 High 1			SMTemp2 T5 Hi1		SMTemp2 Temp 5 Haute			SMTemp2 T5 HauT
2090		32			15			SMTemp2 Temp5 Low			SMTemp2 T5 Low		SMTemp2 Temp 5 Basse			SMTemp2 T5 Bass
2091		32			15			SMTemp2 Temp6 High 2			SMTemp2 T6 Hi2		SMTemp2 Temp 6 Très haute		SMTemp2 T6 THau
2092		32			15			SMTemp2 Temp6 High 1			SMTemp2 T6 Hi1		SMTemp2 Temp 6 Haute			SMTemp2 T6 HauT
2093		32			15			SMTemp2 Temp6 Low			SMTemp2 T6 Low		SMTemp2 Temp 6 Basse			SMTemp2 T6 Bass
2094		32			15			SMTemp2 Temp7 High 2			SMTemp2 T7 Hi2		SMTemp2 Temp 7 Très haute		SMTemp2 T7 THau
2095		32			15			SMTemp2 Temp7 High 1			SMTemp2 T7 Hi1		SMTemp2 Temp 7 Haute			SMTemp2 T7 HauT
2096		32			15			SMTemp2 Temp7 Low			SMTemp2 T7 Low		SMTemp2 Temp 7 Basse			SMTemp2 T7 Bass
2097		32			15			SMTemp2 Temp8 High 2			SMTemp2 T8 Hi2		SMTemp2 Temp 8 Très haute		SMTemp2 T8 THau
2098		32			15			SMTemp2 Temp8 High 1			SMTemp2 T8 Hi1		SMTemp2 Temp 8 Haute			SMTemp2 T8 HauT
2099		32			15			SMTemp2 Temp8 Low			SMTemp2 T8 Low		SMTemp2 Temp 8 Basse			SMTemp2 T8 Bass
2100		32			15			SMTemp3 Temp1 High 2			SMTemp3 T1 Hi2		SMTemp3 Temp 1 Très haute		SMTemp3 T1 THau
2101		32			15			SMTemp3 Temp1 High 1			SMTemp3 T1 Hi1		SMTemp3 Temp 1 Haute			SMTemp3 T1 HauT
2102		32			15			SMTemp3 Temp1 Low			SMTemp3 T1 Low		SMTemp3 Temp 1 Basse			SMTemp3 T1 Bass
2103		32			15			SMTemp3 Temp2 High 2			SMTemp3 T2 Hi2		SMTemp3 Temp 2 Très haute		SMTemp3 T2 THau
2104		32			15			SMTemp3 Temp2 High 1			SMTemp3 T2 Hi1		SMTemp3 Temp 2 Haute			SMTemp3 T2 HauT
2105		32			15			SMTemp3 Temp2 Low			SMTemp3 T2 Low		SMTemp3 Temp 2 Basse			SMTemp3 T2 Bass
2106		32			15			SMTemp3 Temp3 High 2			SMTemp3 T3 Hi2		SMTemp3 Temp 3 Très haute		SMTemp3 T3 THau
2107		32			15			SMTemp3 Temp3 High 1			SMTemp3 T3 Hi1		SMTemp3 Temp 3 Haute			SMTemp3 T3 HauT
2108		32			15			SMTemp3 Temp3 Low			SMTemp3 T3 Low		SMTemp3 Temp 3 Basse			SMTemp3 T3 Bass
2109		32			15			SMTemp3 Temp4 High 2			SMTemp3 T4 Hi2		SMTemp3 Temp 4 Très haute		SMTemp3 T4 THau
2110		32			15			SMTemp3 Temp4 High 1			SMTemp3 T4 Hi1		SMTemp3 Temp 4 Haute			SMTemp3 T4 HauT
2111		32			15			SMTemp3 Temp4 Low			SMTemp3 T4 Low		SMTemp3 Temp 4 Basse			SMTemp3 T4 Bass
2112		32			15			SMTemp3 Temp5 High 2			SMTemp3 T5 Hi2		SMTemp3 Temp 5 Très haute		SMTemp3 T5 THau
2113		32			15			SMTemp3 Temp5 High 1			SMTemp3 T5 Hi1		SMTemp3 Temp 5 Haute			SMTemp3 T5 HauT
2114		32			15			SMTemp3 Temp5 Low			SMTemp3 T5 Low		SMTemp3 Temp 5 Basse			SMTemp3 T5 Bass
2115		32			15			SMTemp3 Temp6 High 2			SMTemp3 T6 Hi2		SMTemp3 Temp 6 Très haute		SMTemp3 T6 THau
2116		32			15			SMTemp3 Temp6 High 1			SMTemp3 T6 Hi1		SMTemp3 Temp 6 Haute			SMTemp3 T6 HauT
2117		32			15			SMTemp3 Temp6 Low			SMTemp3 T6 Low		SMTemp3 Temp 6 Basse			SMTemp3 T6 Bass
2118		32			15			SMTemp3 Temp7 High 2			SMTemp3 T7 Hi2		SMTemp3 Temp 7 Très haute		SMTemp3 T7 THau
2119		32			15			SMTemp3 Temp7 High 1			SMTemp3 T7 Hi1		SMTemp3 Temp 7 Haute			SMTemp3 T7 HauT
2120		32			15			SMTemp3 Temp7 Low			SMTemp3 T7 Low		SMTemp3 Temp 7 Basse			SMTemp3 T7 Bass
2121		32			15			SMTemp3 Temp8 High 2			SMTemp3 T8 Hi2		SMTemp3 Temp 8 Très haute		SMTemp3 T8 THau
2122		32			15			SMTemp3 Temp8 High 1			SMTemp3 T8 Hi1		SMTemp3 Temp 8 Haute			SMTemp3 T8 HauT
2123		32			15			SMTemp3 Temp8 Low			SMTemp3 T8 Low		SMTemp3 Temp 8 Basse			SMTemp3 T8 Bass
2124		32			15			SMTemp4 Temp1 High 2			SMTemp4 T1 Hi2		SMTemp4 Temp 1 Très haute		SMTemp4 T1 THau
2125		32			15			SMTemp4 Temp1 High 1			SMTemp4 T1 Hi1		SMTemp4 Temp 1 Haute			SMTemp4 T1 HauT
2126		32			15			SMTemp4 Temp1 Low			SMTemp4 T1 Low		SMTemp4 Temp 1 Basse			SMTemp4 T1 Bass
2127		32			15			SMTemp4 Temp2 High 2			SMTemp4 T2 Hi2		SMTemp4 Temp 2 Très haute		SMTemp4 T2 THau
2128		32			15			SMTemp4 Temp2 High 1			SMTemp4 T2 Hi1		SMTemp4 Temp 2 Haute			SMTemp4 T2 HauT
2129		32			15			SMTemp4 Temp2 Low			SMTemp4 T2 Low		SMTemp4 Temp 2 Basse			SMTemp4 T2 Bass
2130		32			15			SMTemp4 Temp3 High 2			SMTemp4 T3 Hi2		SMTemp4 Temp 3 Très haute		SMTemp4 T3 THau
2131		32			15			SMTemp4 Temp3 High 1			SMTemp4 T3 Hi1		SMTemp4 Temp 3 Haute			SMTemp4 T3 HauT
2132		32			15			SMTemp4 Temp3 Low			SMTemp4 T3 Low		SMTemp4 Temp 3 Basse			SMTemp4 T3 Bass
2133		32			15			SMTemp4 Temp4 High 2			SMTemp4 T4 Hi2		SMTemp4 Temp 4 Très haute		SMTemp4 T4 THau
2134		32			15			SMTemp4 Temp4 High 1			SMTemp4 T4 Hi1		SMTemp4 Temp 4 Haute			SMTemp4 T4 HauT
2135		32			15			SMTemp4 Temp4 Low			SMTemp4 T4 Low		SMTemp4 Temp 4 Basse			SMTemp4 T4 Bass
2136		32			15			SMTemp4 Temp5 High 2			SMTemp4 T5 Hi2		SMTemp4 Temp 5 Très haute		SMTemp4 T5 THau
2137		32			15			SMTemp4 Temp5 High 1			SMTemp4 T5 Hi1		SMTemp4 Temp 5 Haute			SMTemp4 T5 HauT
2138		32			15			SMTemp4 Temp5 Low			SMTemp4 T5 Low		SMTemp4 Temp 5 Basse			SMTemp4 T5 Bass
2139		32			15			SMTemp4 Temp6 High 2			SMTemp4 T6 Hi2		SMTemp4 Temp 6 Très haute		SMTemp4 T6 THau
2140		32			15			SMTemp4 Temp6 High 1			SMTemp4 T6 Hi1		SMTemp4 Temp 6 Haute			SMTemp4 T6 HauT
2141		32			15			SMTemp4 Temp6 Low			SMTemp4 T6 Low		SMTemp4 Temp 6 Basse			SMTemp4 T6 Bass
2142		32			15			SMTemp4 Temp7 High 2			SMTemp4 T7 Hi2		SMTemp4 Temp 7 Très haute		SMTemp4 T7 THau
2143		32			15			SMTemp4 Temp7 High 1			SMTemp4 T7 Hi1		SMTemp4 Temp 7 Haute			SMTemp4 T7 HauT
2144		32			15			SMTemp4 Temp7 Low			SMTemp4 T7 Low		SMTemp4 Temp 7 Basse			SMTemp4 T7 Bass
2145		32			15			SMTemp4 Temp8 High 2			SMTemp4 T8 Hi2		SMTemp4 Temp 8 Très haute		SMTemp4 T8 THau
2146		32			15			SMTemp4 Temp8 Hi1			SMTemp4 T8 Hi1		SMTemp4 Temp 8 Haute			SMTemp4 T8 HauT
2147		32			15			SMTemp4 Temp8 Low			SMTemp4 T8 Low		SMTemp4 Temp 8 Basse			SMTemp4 T8 Bass
2148		32			15			SMTemp5 Temp1 High 2			SMTemp5 T1 Hi2		SMTemp5 Temp 1 Très haute		SMTemp5 T1 THau
2149		32			15			SMTemp5 Temp1 High 1			SMTemp5 T1 Hi1		SMTemp5 Temp 1 Haute			SMTemp5 T1 HauT
2150		32			15			SMTemp5 Temp1 Low			SMTemp5 T1 Low		SMTemp5 Temp 1 Basse			SMTemp5 T1 Bass
2151		32			15			SMTemp5 Temp2 High 2			SMTemp5 T2 Hi2		SMTemp5 Temp 2 Très haute		SMTemp5 T2 THau
2152		32			15			SMTemp5 Temp2 High 1			SMTemp5 T2 Hi1		SMTemp5 Temp 2 Haute			SMTemp5 T2 HauT
2153		32			15			SMTemp5 Temp2 Low			SMTemp5 T2 Low		SMTemp5 Temp 2 Basse			SMTemp5 T2 Bass
2154		32			15			SMTemp5 Temp3 High 2			SMTemp5 T3 Hi2		SMTemp5 Temp 3 Très haute		SMTemp5 T3 THau
2155		32			15			SMTemp5 Temp3 High 1			SMTemp5 T3 Hi1		SMTemp5 Temp 3 Haute			SMTemp5 T3 HauT
2156		32			15			SMTemp5 Temp3 Low			SMTemp5 T3 Low		SMTemp5 Temp 3 Basse			SMTemp5 T3 Bass
2157		32			15			SMTemp5 Temp4 High 2			SMTemp5 T4 Hi2		SMTemp5 Temp 4 Très haute		SMTemp5 T4 THau
2158		32			15			SMTemp5 Temp4 High 1			SMTemp5 T4 Hi1		SMTemp5 Temp 4 Haute			SMTemp5 T4 HauT
2159		32			15			SMTemp5 Temp4 Low			SMTemp5 T4 Low		SMTemp5 Temp 4 Basse			SMTemp5 T4 Bass
2160		32			15			SMTemp5 Temp5 High 2			SMTemp5 T5 Hi2		SMTemp5 Temp 5 Très haute		SMTemp5 T5 THau
2161		32			15			SMTemp5 Temp5 High 1			SMTemp5 T5 Hi1		SMTemp5 Temp 5 Haute			SMTemp5 T5 HauT
2162		32			15			SMTemp5 Temp5 Low			SMTemp5 T5 Low		SMTemp5 Temp 5 Basse			SMTemp5 T5 Bass
2163		32			15			SMTemp5 Temp6 High 2			SMTemp5 T6 Hi2		SMTemp5 Temp 6 Très haute		SMTemp5 T6 THau
2164		32			15			SMTemp5 Temp6 High 1			SMTemp5 T6 Hi1		SMTemp5 Temp 6 Haute			SMTemp5 T6 HauT
2165		32			15			SMTemp5 Temp6 Low			SMTemp5 T6 Low		SMTemp5 Temp 6 Basse			SMTemp5 T6 Bass
2166		32			15			SMTemp5 Temp7 High 2			SMTemp5 T7 Hi2		SMTemp5 Temp 7 Très haute		SMTemp5 T7 THau
2167		32			15			SMTemp5 Temp7 High 1			SMTemp5 T7 Hi1		SMTemp5 Temp 7 Haute			SMTemp5 T7 HauT
2168		32			15			SMTemp5 Temp7 Low			SMTemp5 T7 Low		SMTemp5 Temp 7 Basse			SMTemp5 T7 Bass
2169		32			15			SMTemp5 Temp8 High 2			SMTemp5 T8 Hi2		SMTemp5 Temp 8 Très haute		SMTemp5 T8 THau
2170		32			15			SMTemp5 Temp8 High 1			SMTemp5 T8 Hi1		SMTemp5 Temp 8 Haute			SMTemp5 T8 HauT
2171		32			15			SMTemp5 Temp8 Low			SMTemp5 T8 Low		SMTemp5 Temp 8 Basse			SMTemp5 T8 Bass
2172		32			15			SMTemp6 Temp1 High 2			SMTemp6 T1 Hi2		SMTemp6 Temp 1 Très haute		SMTemp6 T1 THau
2173		32			15			SMTemp6 Temp1 High 1			SMTemp6 T1 Hi1		SMTemp6 Temp 1 Haute			SMTemp6 T1 HauT
2174		32			15			SMTemp6 Temp1 Low			SMTemp6 T1 Low		SMTemp6 Temp 1 Basse			SMTemp6 T1 Bass
2175		32			15			SMTemp6 Temp2 High 2			SMTemp6 T2 Hi2		SMTemp6 Temp 2 Très haute		SMTemp6 T2 THau
2176		32			15			SMTemp6 Temp2 High 1			SMTemp6 T2 Hi1		SMTemp6 Temp 2 Haute			SMTemp6 T2 HauT
2177		32			15			SMTemp6 Temp2 Low			SMTemp6 T2 Low		SMTemp6 Temp 2 Basse			SMTemp6 T2 Bass
2178		32			15			SMTemp6 Temp3 High 2			SMTemp6 T3 Hi2		SMTemp6 Temp 3 Très haute		SMTemp6 T3 THau
2179		32			15			SMTemp6 Temp3 High 1			SMTemp6 T3 Hi1		SMTemp6 Temp 3 Haute			SMTemp6 T3 HauT
2180		32			15			SMTemp6 Temp3 Low			SMTemp6 T3 Low		SMTemp6 Temp 3 Basse			SMTemp6 T3 Bass
2181		32			15			SMTemp6 Temp4 High 2			SMTemp6 T4 Hi2		SMTemp6 Temp 4 Très haute		SMTemp6 T4 THau
2182		32			15			SMTemp6 Temp4 High 1			SMTemp6 T4 Hi1		SMTemp6 Temp 4 Haute			SMTemp6 T4 HauT
2183		32			15			SMTemp6 Temp4 Low			SMTemp6 T4 Low		SMTemp6 Temp 4 Basse			SMTemp6 T4 Bass
2184		32			15			SMTemp6 Temp5 High 2			SMTemp6 T5 Hi2		SMTemp6 Temp 5 Très haute		SMTemp6 T5 THau
2185		32			15			SMTemp6 Temp5 High 1			SMTemp6 T5 Hi1		SMTemp6 Temp 5 Haute			SMTemp6 T5 HauT
2186		32			15			SMTemp6 Temp5 Low			SMTemp6 T5 Low		SMTemp6 Temp 5 Basse			SMTemp6 T5 Bass
2187		32			15			SMTemp6 Temp6 High 2			SMTemp6 T6 Hi2		SMTemp6 Temp 6 Très haute		SMTemp6 T6 THau
2188		32			15			SMTemp6 Temp6 High 1			SMTemp6 T6 Hi1		SMTemp6 Temp 6 Haute			SMTemp6 T6 HauT
2189		32			15			SMTemp6 Temp6 Low			SMTemp6 T6 Low		SMTemp6 Temp 6 Basse			SMTemp6 T6 Bass
2190		32			15			SMTemp6 Temp7 High 2			SMTemp6 T7 Hi2		SMTemp6 Temp 7 Très haute		SMTemp6 T7 THau
2191		32			15			SMTemp6 Temp7 High 1			SMTemp6 T7 Hi1		SMTemp6 Temp 7 Haute			SMTemp6 T7 HauT
2192		32			15			SMTemp6 Temp7 Low			SMTemp6 T7 Low		SMTemp6 Temp 7 Basse			SMTemp6 T7 Bass
2193		32			15			SMTemp6 Temp8 High 2			SMTemp6 T8 Hi2		SMTemp6 Temp 8 Très haute		SMTemp6 T8 THau
2194		32			15			SMTemp6 Temp8 High 1			SMTemp6 T8 Hi1		SMTemp6 Temp 8 Haute			SMTemp6 T8 HauT
2195		32			15			SMTemp6 Temp8 Low			SMTemp6 T8 Low		SMTemp6 Temp 8 Basse			SMTemp6 T8 Bass
2196		32			15			SMTemp7 Temp1 High 2			SMTemp7 T1 Hi2		SMTemp7 Temp 1 Très haute		SMTemp7 T1 THau
2197		32			15			SMTemp7 Temp1 High 1			SMTemp7 T1 Hi1		SMTemp7 Temp 1 Haute			SMTemp7 T1 HauT
2198		32			15			SMTemp7 Temp1 Low			SMTemp7 T1 Low		SMTemp7 Temp 1 Basse			SMTemp7 T1 Bass
2199		32			15			SMTemp7 Temp2 High 2			SMTemp7 T2 Hi2		SMTemp7 Temp 2 Très haute		SMTemp7 T2 THau
2200		32			15			SMTemp7 Temp2 High 1			SMTemp7 T2 Hi1		SMTemp7 Temp 2 Haute			SMTemp7 T2 HauT
2201		32			15			SMTemp7 Temp2 Low			SMTemp7 T2 Low		SMTemp7 Temp 2 Basse			SMTemp7 T2 Bass
2202		32			15			SMTemp7 Temp3 High 2			SMTemp7 T3 Hi2		SMTemp7 Temp 3 Très haute		SMTemp7 T3 THau
2203		32			15			SMTemp7 Temp3 High 1			SMTemp7 T3 Hi1		SMTemp7 Temp 3 Haute			SMTemp7 T3 HauT
2204		32			15			SMTemp7 Temp3 Low			SMTemp7 T3 Low		SMTemp7 Temp 3 Basse			SMTemp7 T3 Bass
2205		32			15			SMTemp7 Temp4 High 2			SMTemp7 T4 Hi2		SMTemp7 Temp 4 Très haute		SMTemp7 T4 THau
2206		32			15			SMTemp7 Temp4 High 1			SMTemp7 T4 Hi1		SMTemp7 Temp 4 Haute			SMTemp7 T4 HauT
2207		32			15			SMTemp7 Temp4 Low			SMTemp7 T4 Low		SMTemp7 Temp 4 Basse			SMTemp7 T4 Bass
2208		32			15			SMTemp7 Temp5 High 2			SMTemp7 T5 Hi2		SMTemp7 Temp 5 Très haute		SMTemp7 T5 THau
2209		32			15			SMTemp7 Temp5 High 1			SMTemp7 T5 Hi1		SMTemp7 Temp 5 Haute			SMTemp7 T5 HauT
2210		32			15			SMTemp7 Temp5 Low			SMTemp7 T5 Low		SMTemp7 Temp 5 Basse			SMTemp7 T5 Bass
2211		32			15			SMTemp7 Temp6 High 2			SMTemp7 T6 Hi2		SMTemp7 Temp 6 Très haute		SMTemp7 T6 THau
2212		32			15			SMTemp7 Temp6 High 1			SMTemp7 T6 Hi1		SMTemp7 Temp 6 Haute			SMTemp7 T6 HauT
2213		32			15			SMTemp7 Temp6 Low			SMTemp7 T6 Low		SMTemp7 Temp 6 Basse			SMTemp7 T6 Bass
2214		32			15			SMTemp7 Temp7 High 2			SMTemp7 T7 Hi2		SMTemp7 Temp 7 Très haute		SMTemp7 T7 THau
2215		32			15			SMTemp7 Temp7 High 1			SMTemp7 T7 Hi1		SMTemp7 Temp 7 Haute			SMTemp7 T7 HauT
2216		32			15			SMTemp7 Temp7 Low			SMTemp7 T7 Low		SMTemp7 Temp 7 Basse			SMTemp7 T7 Bass
2217		32			15			SMTemp7 Temp8 High 2			SMTemp7 T8 Hi2		SMTemp7 Temp 8 Très haute		SMTemp7 T8 THau
2218		32			15			SMTemp7 Temp8 High 1			SMTemp7 T8 Hi1		SMTemp7 Temp 8 Haute			SMTemp7 T8 HauT
2219		32			15			SMTemp7 Temp8 Low			SMTemp7 T8 Low		SMTemp7 Temp 8 Basse			SMTemp7 T8 Bass
2220		32			15			SMTemp8 Temp1 High 2			SMTemp8 T1 Hi2		SMTemp8 Temp 1 Très haute		SMTemp8 T1 THau
2221		32			15			SMTemp8 Temp1 High 1			SMTemp8 T1 Hi1		SMTemp8 Temp 1 Haute			SMTemp8 T1 HauT
2222		32			15			SMTemp8 Temp1 Low			SMTemp8 T1 Low		SMTemp8 Temp 1 Basse			SMTemp8 T1 Bass
2223		32			15			SMTemp8 Temp2 High 2			SMTemp8 T2 Hi2		SMTemp8 Temp 2 Très haute		SMTemp8 T2 THau
2224		32			15			SMTemp8 Temp2 High 1			SMTemp8 T2 Hi1		SMTemp8 Temp 2 Haute			SMTemp8 T2 HauT
2225		32			15			SMTemp8 Temp2 Low			SMTemp8 T2 Low		SMTemp8 Temp 2 Basse			SMTemp8 T2 Bass
2226		32			15			SMTemp8 Temp3 High 2			SMTemp8 T3 Hi2		SMTemp8 Temp 3 Très haute		SMTemp8 T3 THau
2227		32			15			SMTemp8 Temp3 High 1			SMTemp8 T3 Hi1		SMTemp8 Temp 3 Haute			SMTemp8 T3 HauT
2228		32			15			SMTemp8 Temp3 Low			SMTemp8 T3 Low		SMTemp8 Temp 3 Basse			SMTemp8 T3 Bass
2229		32			15			SMTemp8 Temp4 High 2			SMTemp8 T4 Hi2		SMTemp8 Temp 4 Très haute		SMTemp8 T4 THau
2230		32			15			SMTemp8 Temp4 High 1			SMTemp8 T4 Hi1		SMTemp8 Temp 4 Haute			SMTemp8 T4 HauT
2231		32			15			SMTemp8 Temp4 Low			SMTemp8 T4 Low		SMTemp8 Temp 4 Basse			SMTemp8 T4 Bass
2232		32			15			SMTemp8 Temp5 High 2			SMTemp8 T5 Hi2		SMTemp8 Temp 5 Très haute		SMTemp8 T5 THau
2233		32			15			SMTemp8 Temp5 High 1			SMTemp8 T5 Hi1		SMTemp8 Temp 5 Haute			SMTemp8 T5 HauT
2234		32			15			SMTemp8 Temp5 Low			SMTemp8 T5 Low		SMTemp8 Temp 5 Basse			SMTemp8 T5 Bass
2235		32			15			SMTemp8 Temp6 High 2			SMTemp8 T6 Hi2		SMTemp8 Temp 6 Très haute		SMTemp8 T6 THau
2236		32			15			SMTemp8 Temp6 High 1			SMTemp8 T6 Hi1		SMTemp8 Temp 6 Haute			SMTemp8 T6 HauT
2237		32			15			SMTemp8 Temp6 Low			SMTemp8 T6 Low		SMTemp8 Temp 6 Basse			SMTemp8 T6 Bass
2238		32			15			SMTemp8 Temp7 High 2			SMTemp8 T7 Hi2		SMTemp8 Temp 7 Très haute		SMTemp8 T7 THau
2239		32			15			SMTemp8 Temp7 High 1			SMTemp8 T7 Hi1		SMTemp8 Temp 7 Haute			SMTemp8 T7 HauT
2240		32			15			SMTemp8 Temp7 Low			SMTemp8 T7 Low		SMTemp8 Temp 7 Basse			SMTemp8 T7 Bass
2241		32			15			SMTemp8 Temp8 High 2			SMTemp8 T8 Hi2		SMTemp8 Temp 8 Très haute		SMTemp8 T8 THau
2242		32			15			SMTemp8 Temp8 High 1			SMTemp8 T8 Hi1		SMTemp8 Temp 8 Haute			SMTemp8 T8 HauT
2243		32			15			SMTemp8 Temp8 Low			SMTemp8 T8 Low		SMTemp8 Temp 8 Basse			SMTemp8 T8 Bass
2244		32			15			None					None			Aucun					Aucun
2245		32			15			High Load Level1			HighLoadLevel1		Niveau 1 Surcharge			Niveau 1 Surch
2246		32			15			High Load Level2			HighLoadLevel2		Niveau 2 Surcharge			Niveau 2 Surch
2247		32			15			Maximum					Maximum			Maximum					Maximum
2248		32			15			Average					Average			Moyenne					Moyenne
2249		32			15			Solar Mode				Solar Mode		Mode Solaire				Mode Solaire
2250		32			15			Running Way(For Solar)			Running Way		Mode de Fonctionnement			Mode Fonct.
2251		32			15			Disable					Disable			Désactivé				Désactivé
2252		32			15			RECT-SOLAR				RECT-SOLAR		REDRESSEUR+SOLAIRE			RED+SOLAIRE
2253		32			15			SOLAR					SOLAR			SOLAIRE					SOLAIRE
2254		32			15			RECT First				RECT First		REDRESSEUR Prioritaire			RED.Prio.
2255		32			15			SOLAR First				SOLAR First		SOLAIRE Prioritaire			SOLAIRE Prio.
2256		32			15			Please reboot after changing		Please reboot		SVP Rebooter après Modifications	SVP Rebooter
2257		32			15			CSU Failure				CSU Failure		Défaut Controleur			Déf.Controleur
2258		32			15			SSL and SNMPV3				SSL and SNMPV3		SSL et SNMPV3				SSL et SNMPV3
2259		32			15			Adjust Bus Input Voltage		Adjust In-Volt		Reglage tension d'entree		Regl Tension En
2260		32			15			Confirm Voltage Supplied		Confirm Voltage		Confirmation tension			Confirm Tension
2261		32			15			Converter Only				Converter Only		Convertisseur seul			Convertiss seul
2262		32			15			Net-Port Reset Interval			Reset Interval		Interval d'initialisation du port reseau	IntervInitIPort
2263		32			15			DC1 Load				DC1 Load		Charge DC1				Charge DC1
2264		32			15			DI9					DI9			ETOR9					ETOR9
2265		32			15			DI10					DI10			ETOR10					ETOR10
2266		32			15			DI11					DI11			ETOR11					ETOR11
2267		32			15			DI12					DI12			ETOR12					ETOR12
2268	32			15		Digital Input 9		DI 9				Entrée 9		Entrée 9
2269	32			15		Digital Input 10	DI 10				Entrée 10		Entrée 10
2270	32			15		Digital Input 11	DI 11				Entrée 11		Entrée 11
2271	32			15		Digital Input 12	DI 12				Entrée 12		Entrée 10
2272	32			15		DI9 Alarm State		DI9 Alm State			Etat alarme Entrée 9		Etat Al Entrée 9
2273	32			15		DI10 Alarm State	DI10 Alm State			Etat alarme Entrée 10		Etat Al Entrée 10
2274	32			15		DI11 Alarm State	DI11 Alm State			Etat alarme Entrée 11		Etat Al Entrée 11
2275	32			15		DI12 Alarm State	DI12 Alm State			Etat alarme Entrée 12		Etat Al Entrée 12
2276	32			15		DI9 Alarm		DI9 Alarm			Alarme Entrée 9			Alarme Entrée 9
2277	32			15		DI10 Alarm		DI10 Alarm			Alarme Entrée 10		Alarme Entrée 10
2278	32			15		DI11 Alarm		DI11 Alarm			Alarme Entrée 11		Alarme Entrée 11
2279	32			15		DI12 Alarm		DI12 Alarm			Alarme Entrée 12		Alarme Entrée 12
2280		32			15			None					None			Aucun					Aucun
2281		32			15			Exist					Exist			Existe					Existe
2282		32			15			IB01 State				IB01 State		Etat IB01				Etat IB01
2283		32			15			Relay Output 14				Relay Output 14		Sortie relai 14				Sortie relai 14
2284		32			15			Relay Output 15				Relay Output 15		Sortie relai 15				Sortie relai 15
2285		32			15			Relay Output 16				Relay Output 16		Sortie relai 16				Sortie relai 16
2286		32			15			Relay Output 17				Relay Output 17		Sortie relai 17				Sortie relai 17
2287		32			15			Time Display Format			Time Format		Format affichage heure			FormatAffHeur
2288		32			15			EMEA					EMEA			EMEA					EMEA
2289		32			15			NA					NA			NA					NA
2290		32			15			CN					CN			CN					CN
2291		32			15			Help					Help			Aide					Aide
2292		32			15			Current Protocol Type			Protocol		Type de protocole valide		TypProtocValide
2293		32			15			EEM					EEM			EEM					EEM
2294		32			15			YDN23					YDN23			YDN23					YDN23
2295		32			15			Modbus					ModBus			ModBus					ModBus
2296		32			15			SMS Alarm Level				SMS Alarm Level		Niveau alarme SMS			Niv Al SMS
2297		32			15			Disabled				Disabled		Invalide				Invalide
2298		32			15			Observation				Observation		Observation				Observation
2299		32			15			Major					Major			Majeure					Majeure
2300		32			15			Critical				Critical		Critique				Critique
2301		64			64			SPD is not connected to system or SPD is broken.	SPD is not connected to system or SPD is broken.	Parafoudre HS				Parafoudre HS
2302		32			15			Big Screen				Big Screen		Ecran large				Ecran large
2303		32			15			EMAIL Alarm Level			EMAIL Alarm Level	Niveau alarme EMAIL			Niv Al EMAIL
2304		32			15			HLMS Protocol Type			Protocol Type		HLMS protocol				HLMS protocol
2305		32			15			EEM					EEM			EEM					EEM
2306		32			15			YDN23					YDN23			YDN23					YDN23
2307		32			15			Modbus					Modbus			Modbus					Modbus
2308		32			15			24V Display State			24V Display		Etat Affichage 24V			Etat Affich 24V
2309		32			15			Syst Volt Level				Syst Volt Level		Tension système				Tension système
2310		32			15			48 V System				48 V System		Système 48Vdc				Système 48Vdc
2311		32			15			24 V System				24 V System		Système 24Vdc				Système 24Vdc
2312		32			15			Power Input Type			Power Input Type	Puissance d'entree			Puiss Entree
2313		32			15			AC Input				AC Input		Entree AC				Entree AC
2314		32			15			Diesel Input				Diesel Input		Entree GE				Entree GE
2315		32			15			2nd IB2 Temp1				2nd IB2 Temp1		Temp1 2eme IB2				Temp1 2eme IB2
2316		32			15			2nd IB2 Temp2				2nd IB2 Temp2		Temp2 2eme IB2				Temp2 2eme IB2
2317		32			15			EIB2 Temp1				EIB2 Temp1		Temp1 EIB2				Temp1 EIB2
2318		32			15			EIB2 Temp2				EIB2 Temp2		Temp2 EIB2				Temp2 EIB2
2319		32			15			2nd IB2 Temp1 High 2			2nd IB2 T1 Hi2		2eme IB2 Seuil haut 2 Temp 1		2eme IB2 Ht2 T1
2320		32			15			2nd IB2 Temp1 High 1			2nd IB2 T1 Hi1		2eme IB2 Seuil haut 1 Temp 1		2eme IB2 Ht1 T1
2321		32			15			2nd IB2 Temp1 Low			2nd IB2 T1 Low		2eme IB2 Seuil bas Temp 1		2eme IB2 Bas T1
2322		32			15			2nd IB2 Temp2 High 2			2nd IB2 T2 Hi2		2eme IB2 Seuil haut 2 Temp 2		2eme IB2 Ht2 T2
2323		32			15			2nd IB2 Temp2 High 1			2nd IB2 T2 Hi1		2eme IB2 Seuil haut 1 Temp 2		2eme IB2 Ht1 T2
2324		32			15			2nd IB2 Temp2 Low			2nd IB2 T2 Low		2eme IB2 Seuil bas Temp 2		2eme IB2 Bas T2
2325		32			15			EIB2 Temp1 High 2			EIB2 T1 Hi2		EIB2 Seuil haut 2 Temp 1		EIB2 Haut2 T1
2326		32			15			EIB2 Temp1 High 1			EIB2 T1 Hi1		EIB2 Seuil haut 1 Temp 1		EIB2 Haut1 T1
2327		32			15			EIB2 Temp1 Low				EIB2 T1 Low		EIB2 Seuil bas Temp 1			EIB2 Bas Temp1
2328		32			15			EIB2 Temp2 High 2			EIB2 T2 Hi2		EIB2 Seuil haut 2 Temp 2		EIB2 Haut2 T2
2329		32			15			EIB2 Temp2 High 1			EIB2 T2 Hi1		EIB2 Seuil haut 1 Temp 2		EIB2 Haut1 T2
2330		32			15			EIB2 Temp2 Low				EIB2 T2 Low		EIB2 Seuil bas Temp 2			EIB2 Bas Temp2
2331		32			15			Testing Relay 14			Testing Relay14		Test relai 14				Test relai 14
2332		32			15			Testing Relay 15			Testing Relay15		Test relai 15				Test relai 15
2333		32			15			Testing Relay 16			Testing Relay16		Test relai 16				Test relai 16
2334		32			15			Testing Relay 17			Testing Relay17		Test relai 17				Test relai 17
2335		32			15			Total Output Rated			Total Output Rated	% charge totale				% charge totale
2336		32			15			Load current capacity			Load capacity		Taux de charge				Taux de charge
2337		32			15			EES System Mode				EES System Mode		Système en mode EES			Syst en modeEES
2338		32			15			SMS Modem Fail				SMS Modem Fail		Defaut Modem SMS			Def Modem SMS
2339		32			15			SMDU-EIB Mode				SMDU-EIB Mode		Mode SMDU-EIB				Mode SMDU-EIB
2340		32			15			Load Switch				Load Switch		Commutateur de charge			CommutDe charge
2341		32			15			Manual State				Manual State		Mode Manuel				Mode Manuel
2342		32			15			Converter Voltage Level			Volt Level		Niv Tension Convertisseur		Niv Tension
2343		32			15			CB Threshold Value			Threshold Value		Val seuil disj				Val seuil
2344		32			15			CB Overload Value			Overload Value		Val seuil disj				Val surcharge
2345	32		15		SNMP Config Error		SNMP Config Err		Error Config SNMP		Error Cfg SNMP
2346		32			15			Cab X Num(Valid After Reset)		Cabinet X Num		Cab X Num(Valid After Reset)		Cabinet X Num
2347		32			15			Cab Y Num(Valid After Reset)		Cabinet Y Num		Cab Y Num(Valid After Reset)		Cabinet Y Num
2348		32			15			CB Threshold Power			Threshold Power		CB Threshold Power			Threshold Power
2349		32			15			CB Overload Power			Overload Power		CB Overload Power			Overload Power
2350		32			15			With FCUP				With FCUP		Avec FCUP				Avec FCUP
2351		32			15			FCUP Existence State			FCUP Exist State	Presence FCUP				Presence FCUP
2356		32			15			DHCP Enable				DHCP Enable		DHCP Activé				DHCP Activé
2357		32			15			DO1 Normal State  			DO1 Normal		DO1 Etat Normal				DO1 Normal
2358		32			15			DO2 Normal State  			DO2 Normal		DO2 Etat Normal				DO2 Normal
2359		32			15			DO3 Normal State  			DO3 Normal		DO3 Etat Normal				DO3 Normal
2360		32			15			DO4 Normal State  			DO4 Normal		DO4 Etat Normal				DO4 Normal
2361		32			15			DO5 Normal State  			DO5 Normal		DO5 Etat Normal				DO5 Normal
2362		32			15			DO6 Normal State  			DO6 Normal		DO6 Etat Normal				DO6 Normal
2363		32			15			DO7 Normal State  			DO7 Normal		DO7 Etat Normal				DO7 Normal
2364		32			15			DO8 Normal State  			DO8 Normal		DO8 Etat Normal				DO8 Normal
2365		32			15			Non-Energized				Non-Energized		Non-Energized				Non-Energized
2366		32			15			Energized				Energized		Energized					Energized
2367		32			15			DO14 Normal State  			DO14 Normal		DO14 Etat Normal				DO14 Normal
2368		32			15			DO15 Normal State  			DO15 Normal		DO15 Etat Normal				DO15 Normal
2369		32			15			DO16 Normal State  			DO16 Normal		DO16 Etat Normal				DO16 Normal
2370		32			15			DO17 Normal State  			DO17 Normal		DO17 Etat Normal				DO17 Normal
2371		32			15			SSH Enabled  			        SSH Enabled		SSH permettre				SSH permettre
2372		32			15			Disabled				Disabled		non activé				non activé			
2373		32			15			Enabled					Enabled			permettre				permettre
2374		32			15			Page Type For Login			Page Type		Page Type For Login			Page Type
2375		32			15			Standard				Standard		Standard				Standard
2376		32			15			For BT					For BT			For BT					For BT	
2377		32		15		Fiamm Battery		Fiamm Battery		Batterie Fiamm	Batterie Fiamm
2378		32			15			Correct Temperature1			Correct Temp1			Correct Temperature1			Correct Temp1
2379		32			15			Correct Temperature2			Correct Temp2			Correct Temperature2			Correct Temp2

2503		32			15			NTP Function Enable			NTPFuncEnable		NTP Enable				NTP Enable
2504		32			15			SW_Switch1			SW_Switch1			CommutateurSW1				CommutateurSW1	
2505		32			15			SW_Switch2			SW_Switch2			CommutateurSW2				CommutateurSW2	
2506		32			15			SW_Switch3			SW_Switch3			CommutateurSW3				CommutateurSW3	
2507		32			15			SW_Switch4			SW_Switch4			CommutateurSW4				CommutateurSW4	
2508		32			15			SW_Switch5			SW_Switch5			CommutateurSW5				CommutateurSW5	
2509		32			15			SW_Switch6			SW_Switch6			CommutateurSW6				CommutateurSW6	
2510		32			15			SW_Switch7			SW_Switch7			CommutateurSW7				CommutateurSW7	
2511		32			15			SW_Switch8			SW_Switch8			CommutateurSW8				CommutateurSW8	
2512		32			15			SW_Switch9			SW_Switch9			CommutateurSW9				CommutateurSW9	
2513		32			15			SW_Switch10			SW_Switch10			CommutateurSW10				CommutateurSW10	
