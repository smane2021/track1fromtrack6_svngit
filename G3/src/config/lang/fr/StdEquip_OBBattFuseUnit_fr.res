﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Fuse 1 Voltage				Fuse 1 Voltage		Tension Détec.Prot 1 		V Détec.Prot 1
2		32			15			Fuse 2 Voltage				Fuse 2 Voltage		Tension Détec.Prot 2 		V Détec.Prot 2
3		32			15			Fuse 3 Voltage				Fuse 3 Voltage		Tension Détec.Prot 3 		V Détec.Prot 3
4		32			15			Fuse 4 Voltage				Fuse 4 Voltage		Tension Détec.Prot 4 		V Détec.Prot 4
5		32			15			Fuse 1 Alarm				Fuse 1 Alarm		Alarme Protection 1		Alarme Prot.1
6		32			15			Fuse 2 Alarm				Fuse 2 Alarm		Alarme Protection 2		Alarme Prot.2
7		32			15			Fuse 3 Alarm				Fuse 3 Alarm		Alarme Protection 3		Alarme Prot.3
8		32			15			Fuse 4 Alarm				Fuse 4 Alarm		Alarme Protection 4		Alarme Prot.4
9		32			15			Battery Fuse				Battery Fuse		Protection Batterie		Prot.Batterie
10		32			15			On					On			Présent				Présent
11		32			15			Off					Off			Absent				Absent
12		32			15			Fuse 1 Status				Fuse 1 Status		Etat Protection 1		Etat Prot 1
13		32			15			Fuse 2 Status				Fuse 2 Status		Etat Protection 2		Etat Prot 2
14		32			15			Fuse 3 Status				Fuse 3 Status		Etat Protection 3		Etat Prot 3
15		32			15			Fuse 4 status				Fuse 4 Status		Etat Protection 4		Etat Prot 4
16		32			15			State					State			Etat				Etat
17		32			15			Failure					Failure			Défaut				Défaut
18		32			15			No					No			Non				Non
19		32			15			Yes					Yes			Oui				Oui
20		32			15			Battery Fuse Number			BattFuse Number		Numero Protection Batterie	Num Prot Bat
21		32			15			0					0			0				0
22		32			15			1					1			1				1
23		32			15			2					2			2				2
24		32			15			3					3			3				3
25		32			15			4					4			4				4
26	        32     			15             		5					5			5				5	
27	        32     			15             		6					6			6				6	
28		32			15			Fuse 5 Voltage				Fuse 5 Voltage		Tension fusible 5		Tension fusib 5
28	        32     			15             		Fuse 5 Voltage				Fuse 5 Voltage		Tension Détec.Prot 5		V Détec.Prot 5
29	        32     			15             		Fuse 6 Voltage				Fuse 6 Voltage		Tension Détec.Prot 6		V Détec.Prot 6
30	        32     			15             		Fuse 5 Alarm				Fuse 5 Alarm		Alarme Protection 5		Alarme Prot.5
31	        32     			15             		Fuse 6 Alarm				Fuse 6 Alarm		Alarme Protection 6		Alarme Prot.6
32	        32     			15             		Fuse 5 Status				Fuse 5 Status		Etat Protection 5	 	Etat Prot 5
33	        32     			15             		Fuse 6 Status				Fuse 6 Status		Etat Protection 6	 	Etat Prot 6
