﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Load Fuse 1				Load Fuse1		Protection Utilisation 1		Protec Util 1
2		32			15			Load Fuse 2				Load Fuse2		Protection Utilisation 2		Protec Util 2
3		32			15			Load Fuse 3				Load Fuse3		Protection Utilisation 3		Protec Util 3
4		32			15			Load Fuse 4				Load Fuse4		Protection Utilisation 4		Protec Util 4
5		32			15			Load Fuse 5				Load Fuse5		Protection Utilisation 5		Protec Util 5
6		32			15			Load Fuse 6				Load Fuse6		Protection Utilisation 6		Protec Util 6
7		32			15			Load Fuse 7				Load Fuse7		Protection Utilisation 7		Protec Util 7
8		32			15			Load Fuse 8				Load Fuse8		Protection Utilisation 8		Protec Util 8
9		32			15			Load Fuse 9				Load Fuse9		Protection Utilisation 9		Protec Util 9
10		32			15			Load Fuse 10				Load Fuse10		Protection Utilisation 10		Protec Util 10
11		32			15			Load Fuse 11				Load Fuse11		Protection Utilisation 11		Protec Util 11
12		32			15			Load Fuse 12				Load Fuse12		Protection Utilisation 12		Protec Util 12
13		32			15			Load Fuse 13				Load Fuse13		Protection Utilisation 13		Protec Util 13
14		32			15			Load Fuse 14				Load Fuse14		Protection Utilisation 14		Protec Util 14
15		32			15			Load Fuse 15				Load Fuse15		Protection Utilisation 15		Protec Util 15
16		32			15			Load Fuse 16				Load Fuse16		Protection Utilisation 16		Protec Util 16
17		32			15			SMDU1 DC Fuse				SMDU1 DC Fuse		SMDU1 DC Fuse				SMDU1 DC Fuse
18		32			15			State					State			Etat					Etat
19		32			15			Off					Off			Off					Off
20		32			15			On					On			On					On
21		32			15			Fuse 1 Alarm				Fuse 1 Alarm		Alarme Protec DC 1			Al Protec DC 1
22		32			15			Fuse 2 Alarm				Fuse 2 Alarm		Alarme Protec DC 2			Al Protec DC 2
23		32			15			Fuse 3 Alarm				Fuse 3 Alarm		Alarme Protec DC 3			Al Protec DC 3
24		32			15			Fuse 4 Alarm				Fuse 4 Alarm		Alarme Protec DC 4			Al Protec DC 4
25		32			15			Fuse 5 Alarm				Fuse 5 Alarm		Alarme Protec DC 5			Al Protec DC 5
26		32			15			Fuse 6 Alarm				Fuse 6 Alarm		Alarme Protec DC 6			Al Protec DC 6
27		32			15			Fuse 7 Alarm				Fuse 7 Alarm		Alarme Protec DC 7			Al Protec DC 7
28		32			15			Fuse 8 Alarm				Fuse 8 Alarm		Alarme Protec DC 8			Al Protec DC 8
29		32			15			Fuse 9 Alarm				Fuse 9 Alarm		Alarme Protec DC 9			Al Protec DC 9
30		32			15			Fuse 10 Alarm				Fuse 10 Alarm		Alarme Protec DC 10			Al Protec DC 10
31		32			15			Fuse 11 Alarm				Fuse 11 Alarm		Alarme Protec DC 11			Al Protec DC 11
32		32			15			Fuse 12 Alarm				Fuse 12 Alarm		Alarme Protec DC 12			Al Protec DC 12
33		32			15			Fuse 13 Alarm				Fuse 13 Alarm		Alarme Protec DC 13			Al Protec DC 13
34		32			15			Fuse 14 Alarm				Fuse 14 Alarm		Alarme Protec DC 14			Al Protec DC 14
35		32			15			Fuse 15 Alarm				Fuse 15 Alarm		Alarme Protec DC 15			Al Protec DC 15
36		32			15			Fuse 16 Alarm				Fuse 16 Alarm		Alarme Protec DC 16			Al Protec DC 16
37		32			15			Interrupt Times				Interrupt Times		Interrupt Times				Interrupt Times
38		32			15			Communication Interrupt			Comm Interrupt		Erreur Communication			Erreur Comm.
39		32			15			Load 1 Current				Load 1 Current		Courant Utilisation 1			I Util 1
40		32			15			Load 2 Current				Load 2 Current		Courant Utilisation 2			I Util 2
41		32			15			Load 3 Current				Load 3 Current		Courant Utilisation 3			I Util 3
42		32			15			Load 4 Current				Load 4 Current		Courant Utilisation 4			I Util 4
43		32			15			Load 5 Current				Load 5 Current		Courant Utilisation 5			I Util 5
