﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Fuse 1				Fuse 1			Protection 1		Protection 1
2		32			15			Fuse 2				Fuse 2			Protection 2		Protection 2
3		32			15			Fuse 3				Fuse 3			Protection 3		Protection 3
4		32			15			Fuse 4				Fuse 4			Protection 4		Protection 4
5		32			15			Fuse 5				Fuse 5			Protection 5		Protection 5
6		32			15			Fuse 6				Fuse 6			Protection 6		Protection 6
7		32			15			Fuse 7				Fuse 7			Protection 7		Protection 7
8		32			15			Fuse 8				Fuse 8			Protection 8		Protection 8
9		32			15			Fuse 9				Fuse 9			Protection 9		Protection 9
10		32			15			Auxillary Load Fuse		Aux Load Fuse		Protection Auxiliaire	Fus. Aux.
11		32			15			Fuse 1 Alarm			Fuse 1 Alarm		Alarme Protec DC 1	Al Protec DC1
12		32			15			Fuse 2 Alarm			Fuse 2 Alarm		Alarme Protec DC 2	Al Protec DC2
13		32			15			Fuse 3 Alarm			Fuse 3 Alarm		Alarme Protec DC 3	Al Protec DC3
14		32			15			Fuse 4 Alarm			Fuse 4 Alarm		Alarme Protec DC 4	Al Protec DC4
15		32			15			Fuse 5 Alarm			Fuse 5 Alarm		Alarme Protec DC 5	Al Protec DC5
16		32			15			Fuse 6 Alarm			Fuse 6 Alarm		Alarme Protec DC 6	Al Protec DC6
17		32			15			Fuse 7 Alarm			Fuse 7 Alarm		Alarme Protec DC 7	Al Protec DC7
18		32			15			Fuse 8 Alarm			Fuse 8 Alarm		Alarme Protec DC 8	Al Protec DC8
19		32			15			Fuse 9 Alarm			Fuse 9 Alarm		Alarme Protec DC 9	Al Protec DC9
20		32			15			Auxillary Load Alarm		AuxLoad Alarm		Alarme Protection Aux	Alarme Prot.Aux
21		32			15			On					On			Marche					Marche
22		32			15			Off					Off			Arrêt					Arrêt
23		32			15			DC Fuse Unit			DC Fuse Unit		Unite Protection DC	Unite Prot. DC
24		32			15			Fuse 1 Voltage				Fuse 1 Voltage		Tension Detec.Fuse 1			V Detec.Fuse 1
25		32			15			Fuse 2 Voltage				Fuse 2 Voltage		Tension Detec.Fuse 2			V Detec.Fuse 2
26		32			15			Fuse 3 Voltage				Fuse 3 Voltage		Tension Detec.Fuse 3			V Detec.Fuse 3
27		32			15			Fuse 4 Voltage				Fuse 4 Voltage		Tension Detec.Fuse 4			V Detec.Fuse 4
28		32			15			Fuse 5 Voltage				Fuse 5 Voltage		Tension Detec.Fuse 5			V Detec.Fuse 5
29		32			15			Fuse 6 Voltage				Fuse 6 Voltage		Tension Detec.Fuse 6			V Detec.Fuse 6
30		32			15			Fuse 7 Voltage				Fuse 7 Voltage		Tension Detec.Fuse 7			V Detec.Fuse 7
31		32			15			Fuse 8 Voltage				Fuse 8 Voltage		Tension Detec.Fuse 8			V Detec.Fuse 8
32		32			15			Fuse 9 Voltage				Fuse 9 Voltage		Tension Detec.Fuse 9			V Detec.Fuse 9
33		32			15			Fuse 10 Voltage				Fuse 10 Voltage		Tension Detec.Fuse 10			V Detec.Fuse 10
34		32			15			Fuse 10				Fuse 10			Protection 10		Protection 10
35		32			15			Fuse 10 Alarm 			Fuse 10 Alarm		Alarme Protec DC 10	Al Protec DC10
36		32			15			State				State			Etat			Etat
37		32			15			Failure				Failure			Defaut			Defaut
38		32			15			No				No			Non			Non
39		32			15			Yes				Yes			Oui			Oui
40	        32     			15             		Fuse 11				Fuse 11		    	Protection 11		Protection 11
41	        32     			15             		Fuse 12				Fuse 12		    	Protection 12		Protection 12
42	        32     			15             		Fuse 11 Alarm 			Fuse 11 Alarm		Alarme Protec DC 11	Al Protec DC11
43	        32     			15             		Fuse 12 Alarm 			Fuse 12 Alarm		Alarme Protec DC 12	Al Protec DC12
50		32			15			Load Shunt Voltage			Shunt Voltage		Tension Shunt Utilisation		V Shunt Util
51		32			15			Load Current				Load Curr		Courant Utilisation			I Util
52		32			15			Load Shunt Full Current			Load Shunt C		Courant Shunt Utilisation	I Shunt Util
53		32			15			Load Shunt Full Voltage			Load Shunt V		Tension Shunt				V Shunt
54		32			15			Load High Current Level			Load High C		Niveau Sur Courant			Niveau Sur I
55		32			15			Load High Current Alarm			Load High C A		Alarme Sur Courant			Al Sur I
56		32			15			SMDU Failure				SMDU Failure		Défaut SMDU				Défaut SMDU
