﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Rectifier Group				Rect Group		Groupe redresseur			Grp redresseur
2		32			15			Total Current				Tot Rect Curr		Courant total				Courant Red.
3		32			15			Average Voltage				Rect Voltage		Tension moyenne				Tension Red.
4		32			15			Used Capacity				Sys Cap Used		Taux de charge Red			Taux charge Red
5		32			15			Maximum Used Capacity			Max Used Cap		Max Taux de charge Red			Max Taux charge
6		32			15			Minimum Used Capacity			Min Used Cap		Min Taux de charge Red			Min Taux charge
7		32			15			Communicating Rectifiers		Comm Rect Num		Nbr redresseur en communication		Nbr Red en com.
8		32			15			Active Rectifiers			Active Rects		Redresseur valide			Red.valide
9		32			15			Installed Rectifiers			Inst Rects		Nombre de redresseurs			Nbre Red.
10		32			15			Rectifier AC Failure Status		AC Fail Status		Défaut AC redresseur			Défaut AC red
11		32			15			Multi-rectifier Failure Status		Multi-Rect Fail		Multi Redresseur en défaut		Multi Déf Red
12		32			15			Rectifer Current Limit			Current Limit		Limitation courant			Lim courant
13		32			15			Voltage Trim				Voltage Trim		Ajust. tension				Ajust. tension
14		32			15			DC On/Off Control			DC On/Off Ctrl		DC On/Off				DC On/Off
15		32			15			AC On/Off Control			AC On/Off Ctrl		AC On/Off				AC On/Off
16		32			15			Rectifiers LEDs Control			LEDs Control		Test leds				Test leds
17		32			15			Fan Speed Control			Fan Speed Ctrl		Vitesse max ventil			Vit. max ventil
18		32			15			Rated Voltage				Rated Voltage		Tension de repli			Tension repli
19		32			15			Rated Current				Rated Current		Courant max de repli			Imax repli
20		32			15			High Voltage Limit			Hi-Volt Limit		Seuil sur tension			seuil surU
21		32			15			Low Voltage Limit			Lo-Volt Limit		limite tension basse			lim tens. basse
22		32			15			High Temperature Limit			Hi-Temp Limit		limite temp. haute			lim temp. haute
24		32			15			Restart Time on Overvoltage		OverVRestartT		Timeout blocage sur-tension		TO blocage surU
25		32			15			WALK-In Time				WALK-In Time		Temporisation de démarrage		Tempo démarrage
26		32			15			WALK-In					WALK-In			Activation Tempo. Démarrage		Act.Tempo.Démar
27		32			15			Minimum Redundancy			Min Redundancy		Redondance min				Redondance min
28		32			15			Maximum Redundancy			Max Redundancy		Redondance max				Redondance max
29		32			15			Switch Off Delay			SwitchOff Delay		Délais Arrêt				Délais Arrêt
30		32			15			Cycle Period				Cyc Period		Cycle périodique			Cycle périodi.
31		32			15			Cycle Activation Time			Cyc Act Time		Durée cycle activation			Durée cycl acti
32		32			15			Rectifier AC Failure			Rect AC Fail		Défaut AC redresseur			Défaut AC red
33		32			15			Multi-Rectifiers Failure		Multi-rect Fail		Plus 1 redresseur en défaut		Défaut +1 Red
36		32			15			Normal					Normal			Normal					Normal
37		32			15			Failure					Failure			Défaut					Défaut
38		32			15			Switch Off All				Switch Off All		Tous sur arrêt				Arrêt Tous Red.
39		32			15			Switch On All				Switch On All		Tous en fonction			Tous en fonction
42		32			15			Flash All				Flash All		Clignotante				Clignotante
43		32			15			Stop Flash				Stop Flash		Fixe					Fixe
44		32			15			Full Speed				Full Speed		Vitesse maximum				Vitesse max
45		32			15			Automatic Speed				Auto Speed		Vitesse Automatique			Vitesse Auto
46		32			32			Current Limit Control			Curr Limit Ctrl		Control Limitation courant		Cont.Lim.I
47		32			32			Full Capacity Control			Full Cap Ctrl		Puisssance maximum			Puis. Maximum
54		32			15			Disabled				Disabled		Désactivé				Désactivé
55		32			15			Enabled					Enabled			Activé					Activé
68		32			15			ECO Mode				ECO Mode		Mode ECO				Mode ECO
72		32			15			Rectifier On at AC Overvoltage		AC Overvolt ON		Redresseur ON si Surtension AC		Red ON Surt AC
73		32			15			No					No			Non					Non
74		32			15			Yes					Yes			Oui					Oui
77		32			15			Pre-Current Limit Turn-On		Pre-Curr Limit		Pré-limitation au démarrage		Pré-limit Start
78		32			15			Rectifier Power type			Rect Power type		Type redresseur				Type red
79		32			15			Double Supply				Double Supply		Double Entree AC			2 Entrees AC
80		32			15			Single Supply				Single Supply		Simple Entree AC			1 Entree AC
81		32			15			Last Rectifiers Quantity		Last Rect Qty		Dernière Quantitée Redresseur		Dern Quant Red
82		32			15			Rectifier Lost				Rectifier Lost		Perte Redresseur			Perte Red
83		32			15			Rectifier Lost				Rectifier Lost		Perte Redresseur			Perte Red
84		32			15			Reset Rectifier Lost Alarm		Reset Rect Lost		Reset Perte Redresseur			Reset Perte Red
85		32			15			Reset					Reset			Reset					Reset
86		32			15			Confirm Rect Position/Phase		Confirm Pos/PH		Confirm Position Phase/Red		Confirm Posi Ph
87		32			15			Confirm					Confirm			Confirmation				Confirmation
88		32			15			Best Operating Point			Best Point		Meilleur Point de fonctionnement	Meil Point Fonct
89		32			15			Rectifier Redundancy			Rect Redundancy		Redondance redresseur			Redondance red
90		32			15			Load Fluctuation Range			Fluct Range		Pourcentage Equilibrage			% Equilibrage
91		32			15			System Energy Saving Point		EngySave Point		Meilleur Point de fonctionnement	Meil Point Fonct
92		32			15			E-Stop Function				E-Stop Function		Fonction E-Stop				Fonction E-Stop
93		32			15			AC Phases				AC Phases		Nb Phases				Nb Phases
94		32			15			Single Phase				Single Phase		Entrée Monophasé			Entrée 1 Phase
95		32			15			Three Phases				Three Phases		Entrée Tri-Phasee			Entrée 3 Phases
96		32			15			Input Current Limit			InputCurrLimit		Limit Courant AC			Limit I AC
97		32			15			Double Supply				Double Supply		Double Entrée AC			2 Entrées AC
98		32			15			Single Supply				Single Supply		Simple Entrée AC			1 Entrée AC
99		32			15			Small Supply				Small Supply		Petite Puissance			Petite Puissanc
100		32			15			Restart on Overvoltage Enabled		DCOverVRestart		Redémarrage Redresseur sur Surtention DC	Dem Red Surt DC
101		32			15			Sequence Start Interval				Start Interval			Démarrage Séquentiel		Dém. Séquentiel
102		32			15			Rated Voltage				Rated Voltage		Tension de repli			Tension repli
103		32			15			Rated Current				Rated Current		Courant max de repli			Imax repli
104		32			15			All Rectifiers Comm Fail		AllRectCommFail		Pas de Réponse Redresseurs		Pas de Rép.Red.
105		32			15			Inactive					Inactive			Désactivé			Désactivé
106		32			15			Active						Active				Activé				Activé
107		32			15			ECO Mode Active				ECO Mode Active		Activation ECO Mode			Activ ECO Mode
108		32			15			ECO Mode Oscillated			ECO Oscill		Oscill ECO Mode				Oscill ECO Mode
109		32			15			Reset ECO Mode Oscillated Alarm		Reset Oscill		Reset Oscill ECO Mode			Reset Oscill
110		32			15			High Voltage Limit(24V)			Hi-Volt Limit		Tension Max(24V)			V Max(24V)
111		32			15			Rectifiers Trim(24V)			Rect Trim		Tension Regul(24V)			V Regul(24V)
112		32			15			Rated Voltage(Internal Use)		Rated Voltage		Rated Voltage(Internal Use)		Rated Voltage
113		32			15			Rect Info Change(M/S Internal Use)	RectInfo Change		Information Redresseur Change		Info Red Change
114		32			15			MixHE Power				MixHE Power		Mix Type Redresseur			Mix Type Red
115		32			15			Derated					Derated			Réduction				Réduction
116		32			15			Non-derated				Non-derated		Pas de Réduction			Pas Réduction
117		32			15			Rectifier Drying Time			Rect Drying Time	Durée de déshumidification		Durée déshumidi
118		32			15			All Rectifiers On			All Rect On		Red en déshumidification		Red déshumidifi
119		32			15			Clear Rectifier Comm Fail Alarm		Clear Comm Fail		Reset Alarme Perte Redresseur		Reset Perte Red
120		32			15			HVSD					HVSD			Surtension DC Active			Sur U DC Active
121		32			15			HVSD Voltage Difference			HVSD Volt Diff		Dif U Surtension			Dif U SurU
122		32			15			Total Rated Current of Work On		Total Rated Cur		Courant Total				Courant Total
123		32			15			Diesel Power Limitation				Disl Pwr Limit			Limitation Puissance sur  GE 	Limit P sur GE
124		32			15			Diesel DI Input				Diesel DI Input		Entrée Signal GE			Entrée Signal GE
125		32			15			Diesel Power Limit Point Set		DislPwrLmt Set		Limite Puissance sur GE			Lim P sur GE
126		32			15			None					None			Aucune					Aucune
127		32			15			DI 1					DI 1			DI 1					DI 1
128		32			15			DI 2					DI 2			DI 2					DI 2
129		32			15			DI 3					DI 3			DI 3					DI 3
130		32			15			DI 4					DI 4			DI 4					DI 4
131		32			15			DI 5					DI 5			DI 5					DI 5
132		32			15			DI 6					DI 6			DI 6					DI 6
133		32			15			DI 7					DI 7			DI 7					DI 7
134		32			15			DI 8					DI 8			DI 8					DI 8
135		32			15			Current Limit Point			Curr Limit Pt		Limite Courant				Limite I
136		32			15			Current Limit				Curr Limit		Limite Courant Active			Limit I Act
137		32			15			Maximum Current Limit Value		Max Curr Limit		Courant Max Limit			Courant Max Limit
138		32			15			Default Current Limit Point		Def Curr Lmt Pt		Défaut Limite Courant			Def Limite I
139		32			15			Minimum Current Limit Value		Min Curr Limit		Limitation Courant Min			Limit.Cour.Min
140		32			15			AC Power Limit Mode			AC Power Lmt		Mode Limitation Puissance AC		Mode Lim.P.AC
141		32			15			A					A			A					A
142		32			15			B					B			B					B
143		32			15			Existence State				Existence State		Etat existant				Etat existant
144		32			15			Existent				Existent		Existant				Existant
145		32			15			Not Existent				Not Existent		Inexistant				Inexistant
146		32			15			Total Output Power			Output Power		Puissance de sortie totale		W Sortie totale
147		32			15			Total Slave Rated Current		Total RatedCurr		Courant total de l'esclave		I total esclave
148		32			15			Reset Rectifier IDs			Reset Rect IDs		Reset Redresseur IDs			Reset Redr IDs
149		32			15			HVSD Voltage Difference (24V)		HVSD Volt Diff		HVSD ecart tension (24V)		HVSD Volt Diff
150		32			15			Normal Update				Normal Update		Mise a jour standard			M a J standard
151		32			15			Force Update				Force Update		Mise a jour forcee			M à J forcee
152		32			15			Update OK Number			Update OK Num		Nombre de M a J OK			Nb Mise a J OK
153		32			15			Update State				Update State		Status de version			Status version
154		32			15			Updating				Updating		Mise a jour				Mise a jour
155		32			15			Normal Successful			Normal Success		Normale reussie				Normale reussie
156		32			15			Normal Failed					Normal Fail			Normale en echec		Normale enEchec
157		32			15			Force Successful			Force Success		Forcee reussie				Forcee reussie
158		32			15			Force Failed				Force Fail		Forcee en echec				Forcee en echec
159		32			15			Communication Time-Out			Comm Time-Out		Communication Time-Out			Comm Time-Out
160		32			15			Open File Failed			Open File Fail		Echec ouverture fichier			Echec ouv fich
161		32			15			Delay of LowRectCap Alarm		LowRectCapDelay				Retard Alm Low Rect Cap		RetardLowRedCap		
162		32			15			Maximum Redundacy 				MaxRedundacy 				Redondance maximale					Redondance Max	
163		32			15			Low Rectifier Capacity			Low Rect Cap				Cap Redresseur Faible			Cap RedFaible			
164		32			15			High Rectifier Capacity			High Rect Cap				Cap Redresseur Elevée		Cap RedElevée			

165		32			15			Efficiency Tracker			Effi Track				Traqueur d'efficacité			Traque Effic
166		32			15			Benchmark Rectifier			Benchmark Rect			Benchmark Rectifier			Benchmark Rect	
167		32			15			R48-3500e3					R48-3500e3				R48-3500e3					R48-3500e3		
168		32			15			R48-3200					R48-3200				R48-3200					R48-3200		
169		32			15			96%							96%						96%							96%				
170		32			15			95%							95%						95%							95%				
171		32			15			94%							94%						94%							94%				
172		32			15			93%							93%						93%							93%				
173		32			15			92%							92%						92%							92%				
174		32			15			91%							91%						91%							91%				
175		32			15			90%							90%						90%							90%				
176		32			15			Cost per kWh				Cost per kWh			Coût par kWh				Coût par kWh	
177		32			15			Currency					Currency				Devise					Devise		
178		32			15			USD							USD						USD							USD				
179		32			15			CNY							CNY						CNY							CNY				
180		32			15			EUR							EUR						EUR							EUR				
181		32			15			GBP							GBP						GBP							GBP				
182		32			15			RUB							RUB						RUB							RUB				
183		32			15			Percentage 98% Rectifiers	Percent98%Rect			98% Redresseurs				98% Redress
184		32			15			10%							10%						10%							10%				
185		32			15			20%							20%						20%							20%				
186		32			15			30%							30%						30%							30%				
187		32			15			40%							40%						40%							40%				
188		32			15			50%							50%						50%							50%				
189		32			15			60%							60%						60%							60%				
190		32			15			70%							70%						70%							70%				
191		32			15			80%							80%						80%							80%				
192		32			15			90%							90%						90%							90%				
193		32			15			100%						100%					100%						100%			
																					
200		32			15			Default Voltage			Default Voltage				Tension Défaut		Tension Défaut