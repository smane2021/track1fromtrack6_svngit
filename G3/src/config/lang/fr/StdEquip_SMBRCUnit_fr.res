﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		Tension jeu de barre			V jeu de barre
2		32			15			Load 1 Current				Load1 Current		Courant utilisation 1			Courant 1
3		32			15			Load 2 Current				Load2 Current		Courant utilisation 2			Courant 2
4		32			15			Load 3 Current				Load3 Current		Courant utilisation 3			Courant 3
5		32			15			Load 4 Current				Load4 Current		Courant utilisation 4			Courant 4
6		32			15			Load Fuse 1				Load Fuse1		Protection Utilisation 1		Prot.Util.1
7		32			15			Load Fuse 2				Load Fuse2		Protection Utilisation 2		Prot.Util.2
8		32			15			Load Fuse 3				Load Fuse3		Protection Utilisation 3		Prot.Util.3
9		32			15			Load Fuse 4				Load Fuse4		Protection Utilisation 4		Prot.Util.4
10		32			15			Load Fuse 5				Load Fuse5		Protection Utilisation 5		Prot.Util.5
11		32			15			Load Fuse 6				Load Fuse6		Protection Utilisation 6		Prot.Util.6
12		32			15			Load Fuse 7				Load Fuse7		Protection Utilisation 7		Prot.Util.7
13		32			15			Load Fuse 8				Load Fuse8		Protection Utilisation 8		Prot.Util.8
14		32			15			Load Fuse 9				Load Fuse9		Protection Utilisation 9		Prot.Util.9
15		32			15			Load Fuse 10				Load Fuse10		Protection Utilisation 10		Prot.Util.10
16		32			15			Load Fuse 11				Load Fuse11		Protection Utilisation 11		Prot.Util.11
17		32			15			Load Fuse 12				Load Fuse12		Protection Utilisation 12		Prot.Util.12
18		32			15			Load Fuse 13				Load Fuse13		Protection Utilisation 13		Prot.Util.13
19		32			15			Load Fuse 14				Load Fuse14		Protection Utilisation 14		Prot.Util.14
20		32			15			Load Fuse 15				Load Fuse15		Protection Utilisation 15		Prot.Util.15
21		32			15			Load Fuse 16				Load Fuse16		Protection Utilisation 16		Prot.Util.16
22		32			15			Run Time				Run Time		Durée fonctionnement			Durée fonction.
23		32			15			LVD1 Control				LVD1 Control		Control LVD1				Control LVD1
24		32			15			LVD2 Control				LVD2 Control		Control LVD2				Control LVD2
25		32			15			LVD1 Voltage				LVD1 Voltage		Tension LVD1				Tension LVD1
26		32			15			LVR1 Voltage				LVR1 Voltage		Tension LVR1				Tension LVR1
27		32			15			LVD2 Voltage				LVD2 Voltage		Tension LVD2				Tension LVD2
28		32			15			LVR2 voltage				LVR2 voltage		Tension LVR2				Tension LVR2
29		32			15			On					On			Connecté				Connecté
30		32			15			Off				Off			Déconnecté				Déconnecté
31		32			15			Normal					Normal			Normal					Normal
32		32			15			Error					Error			Erreur					Erreur
33		32			15			On					On			Connecté				Connecté
34		32			15			Fuse 1 Alarm			Fuse1 Alarm		Alarme Protection 1			Alarme Prot. 1
35		32			15			Fuse 2 Alarm			Fuse2 Alarm		Alarme Protection 2			Alarme Prot. 2
36		32			15			Fuse 3 Alarm			Fuse3 Alarm		Alarme Protection 3			Alarme Prot. 3
37		32			15			Fuse 4 Alarm			Fuse4 Alarm		Alarme Protection 4			Alarme Prot. 4
38		32			15			Fuse 5 Alarm			Fuse5 Alarm		Alarme Protection 5			Alarme Prot. 5
39		32			15			Fuse 6 Alarm			Fuse6 Alarm		Alarme Protection 6			Alarme Prot. 6
40		32			15			Fuse 7 Alarm			Fuse7 Alarm		Alarme Protection 7			Alarme Prot. 7
41		32			15			Fuse 8 Alarm			Fuse8 Alarm		Alarme Protection 8			Alarme Prot. 8
42		32			15			Fuse 9 Alarm			Fuse9 Alarm		Alarme Protection 9			Alarme Prot. 9
43		32			15			Fuse 10 Alarm			Fuse10 Alarm		Alarme Protection 10			Alarme Prot. 10
44		32			15			Fuse 11 Alarm			Fuse11 Alarm		Alarme Protection 11			Alarme Prot. 11
45		32			15			Fuse 12 Alarm			Fuse12 Alarm		Alarme Protection 12			Alarme Prot. 12
46		32			15			Fuse 13 Alarm			Fuse13 Alarm		Alarme Protection 13			Alarme Prot. 13
47		32			15			Fuse 14 Alarm			Fuse14 Alarm		Alarme Protection 14			Alarme Prot. 14
48		32			15			Fuse 15 Alarm			Fuse15 Alarm		Alarme Protection 15			Alarme Prot. 15
49		32			15			Alarm					Alarm			Alarme					Alarme
50		32			15			HW Test Alarm				HW Test Alarm		Alarme Test HW				Alarme test HW
51		32			15			SM-BRC Unit				SM-BRC Unit		Unitée SMBRC				Unitée SMBRC
52		32			15			Battery Fuse 1 Voltage		BATT Fuse1 Volt		V Protection Batterie 1			Tens Prot Bat1
53		32			15			Battery Fuse 2 Voltage		BATT Fuse2 Volt		V Protection Batterie 2			Tens Prot Bat2
54		32			15			Battery Fuse 3 Voltage		BATT Fuse3 Volt		V Protection Batterie 3			Tens Prot Bat3
55		32			15			Battery Fuse 4 Voltage		BATT Fuse4 Volt		V Protection Batterie 4			Tens Prot Bat4
56		32			15			Battery Fuse 1 Status			BATT Fuse1 Stat		Etat Protection Batterie 1		Etat Prot Bat1
57		32			15			Battery Fuse 2 Status			BATT Fuse2 Stat		Etat Protection Batterie 2		Etat Prot Bat2
58		32			15			Battery Fuse 3 Status			BATT Fuse3 Stat		Etat Protection Batterie 3		Etat Prot Bat3
59		32			15			Battery Fuse 4 Status			BATT Fuse4 Stat		Etat Protection Batterie 4		Etat Prot Bat4
60		32			15			On					On			Connecté				Connecté
61		32			15			Off					Off			DéConnecté				DéConnecté
62		32			15			Battery Fuse 1 Alarm		Bat Fuse1 Alarm		Alarme Protection Batterie 1		Alarm Prot Bat1
63		32			15			Battery Fuse 2 Alarm		Bat Fuse2 Alarm		Alarme Protection Batterie 2		Alarm Prot Bat2
64		32			15			Battery Fuse 3 Alarm		Bat Fuse3 Alarm		Alarme Protection Batterie 3		Alarm Prot Bat3
65		32			15			Battery Fuse 4 Alarm		Bat Fuse4 Alarm		Alarme Protection Batterie 4		Alarm Prot Bat4
66		32			15			Total Load Current			Tot Load Curr		Courant Total Utilisation		I Total Util.
67		32			15			Over Load Current Limit			Over Curr Lim		Valeur Sur Courant			Valeur Sur I
68		32			15			Over Current				Over Current		Sur Courant				Sur Courant
69		32			15			LVD1 Enabled				LVD1 Enabled		Activation LVD1				Activation LVD1
70		32			15			LVD1 Mode				LVD1 Mode		Mode LVD1				Mode LVD1
71		32			15			LVD1 Reconnect Delay			LVD1Recon Delay		Délais reconnexion LVD1			Délais rec.LVD1
72		32			15			LVD2 Enabled				LVD2 Enabled		Activation LVD2				Activation LVD2
73		32			15			LVD2 Mode				LVD2 Mode		Mode LVD2				Mode LVD2
74		32			15			LVD2 Reconnect Delay			LVD2Recon Delay		Délais reconnexion LVD2			Délais rec.LVD2
75		32			15			LVD1 Status				LVD1 Status		Etat LVD1				Etat LVD1
76		32			15			LVD2 Status				LVD2 Status		Etat LVD2				Etat LVD2
77		32			15			Disabled				Disabled		Désactivé				Désactivé
78		32			15			Enabled					Enabled			Activé					Activé
79		32			15			By Voltage				By Volt			Par Tension				Par Tension
80		32			15			By Time					By Time			Sur Durée				Sur Durée
81		32			15			Bus Bar Volt Alarm			Bus Bar Alarm		Alarme Bus Distribution			Alarme Bus Distrib.
82		32			15			Normal					Normal			Normal					Normal
83		32			15			Low					Low			Bas					Bas
84		32			15			High					High			Haut					Haut
85		32			15			Low Voltage				Low Voltage		Tension Basse				Tension Basse
86		32			15			High Voltage				High Voltage		Tension Haute				Tension Haute
87		32			15			Shunt 1 Current Alarm			Shunt1 Alarm		Alarme courant shunt1			Alarme I shunt1
88		32			15			Shunt 2 Current Alarm			Shunt2 Alarm		Alarme courant shunt2			Alarme I shunt2
89		32			15			Shunt 3 Current Alarm			Shunt3 Alarm		Alarme courant shunt3			Alarme I shunt3
90		32			15			Shunt 4 Current Alarm			Shunt4 Alarm		Alarme courant shunt4			Alarme I shunt4
91		32			15			Shunt 1 Over Current			Shunt1 Over Cur		Sur courant shunt1			Sur I shunt1
92		32			15			Shunt 2 Over Current			Shunt2 Over Cur		Sur courant shunt2			Sur I shunt2
93		32			15			Shunt 3 Over Current			Shunt3 Over Cur		Sur courant shunt3			Sur I shunt3
94		32			15			Shunt 4 Over Current			Shunt4 Over Cur		Sur courant shunt4			Sur I shunt4
95		32			15			Interrupt Times				Interrupt Times		Durée Interruption			Interruption
96		32			15			Existent				Existent		Présent					Présent
97		32			15			Non-Existent				Non-Existent		Absent					Absent
98		32			15			Very Low				Very Low		Très Bas				Très Bas
99		32			15			Very High				Very High		Très Haut				Très Haut
100		32			15			Switch					Switch			Interrupteur				Interrupteur
101		32			15			LVD1 Failure				LVD1 Failure		Défaut LVD1				Défaut LVD1
102		32			15			LVD2 Failure				LVD2 Failure		Défaut LVD2				Défaut LVD2
103		32			15			HTD1 Enable				HTD1 Enable		Activation HTD1				Activation HTD1
104		32			15			HTD2 Enable				HTD2 Enable		Activation HTD2				Activation HTD2
105		32			15			Battery LVD				Battery LVD		Contacteur Batterie			Contact.Batterie
106		32			15			No Battery				No Battery		Aucune batterie				Aucune Bat.
107		32			15			LVD1					LVD1			LVD1					LVD1
108		32			15			LVD2					LVD2			LVD2					LVD2
109		32			15			Battery Always On		BattAlwaysOn		Batterie Toujours connectée		Bat.Tjs connect
110		32			15			Barcode					Barcode			Code Barre				Code Barre
111		32			15			DC Overvoltage				DC Overvolt		Sur Tension DC				Sur Tension DC
112		32			15			DC Undervoltage				DC Undervolt		Sous Tension DC				Sous Tension DC
113		32			15			Overcurrent 1				Overcurr 1		Sur courant shunt1			Sur I shunt1
114		32			15			Overcurrent 2				Overcurr 2		Sur courant shunt2			Sur I shunt2
115		32			15			Overcurrent 3				Overcurr 3		Sur courant shunt3			Sur I shunt3
116		32			15			Overcurrent 4				Overcurr 4		Sur courant shunt4			Sur I shunt4
117		32			15			Existence State				Existence State		Détection				Détection
118		32			15			Communication Failure		Comm Fail		Défaut Communication 			Dé Comm.
119		32			15			Bus Voltage Status			Bus Status		Tension Bus				Tension Bus
120		32			15			Communication OK			Comm OK			Communication OK			Comm OK
121		32			15			All Communication Failure	All Comm Fail		Défaut Toute Communication 		Dé Toute Comm.
122		32			15			Communication Failure		Comm Fail		Défaut Communication 			Dé Comm.
123		32			15			Rated Capacity				Rated Capacity		Estimation Capacité			Estim. Capa.
150		32			15			Digital In Number			Digital In Num		Nombre Entées Digitale (DI)		NB DI
151		32			15			Digital Input 1				Digital Input1		DI 1					DI 1
152		32			15			Low					Low			Bas					Bas
153		32			15			High					High			Haut					Haut
154		32			15			Digital Input 2				Digital Input2		DI 2					DI 2
155		32			15			Digital Input 3				Digital Input3		DI 3					DI 3
156		32			15			Digital Input 4				Digital Input4		DI 4					DI 4
157		32			15			Digital Out Number			Digital Out Num		Nombre Sorties Digitale (DI)		NB DO
158		32			15			Digital Output 1			Digital Output1		DO 1					DO 1
159		32			15			Digital Output 2			Digital Output2		DO 2					DO 2
160		32			15			Digital Output 3			Digital Output3		DO 3					DO 3
161		32			15			Digital Output 4			Digital Output4		DO 4					DO 4
162		32			15			Operation State				Op State		Etat Fonctionnement			Etat Fonct.
163		32			15			Normal					Normal			Normal					Normal
164		32			15			Test					Test			Test					Test
165		32			15			Discharge				Discharge		Décharge				Décharge
166		32			15			Calibration				Calibration		Calibration				Calibration
167		32			15			Diagnostic				Diagnostic		Diagnostique				Diagnostique
168		32			15			Maintenance				Maintenance		Maintenance				Maintenance
169		32			15			Resistence Test Interval		Resist Test Int		Intervale Test Resistance		Inter.Test Res.
170		32			15			Ambient Temperature Value		Amb Temp Value		Température Ambiante			Temp. Ambiante
171		32			15			High Ambient Temperature		Hi Amb Temp		Température Ambiante Haute		Temp. Amb Haute
172		32			15			Configuration Number			Cfg Num			Numéro de configuration			Num Config
173		32			15			Number of Batteries			No. of Batt		Numéro de Batterie			Num Batterie
174		32			15			Unit Sequence Number			Unit Seq Num		Numéro de séquence Module		Num séq Module
175		32			15			Start Battery Sequence			Start Batt Seq		Démarrage Bat Séquence			Dém. Bat.Séq.
176		32			15			Low Ambient Temperature			Lo Amb Temp		Température ambiante Basse		Temp Amb Basse
177		34			15			Ambient Temperature Probe Failure	Amb Temp Fail		Défaut Capteur Temp Ambiante		DéfCapTempAmb
