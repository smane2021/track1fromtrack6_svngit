﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			AC Group				AC Group		Groupe AC				Groupe AC
2		32			15			Total Phase A Current			Phase A Curr		Courant total phase A			Cour. phase A
3		32			15			Total Phase B Current			Phase B Curr		Courant total phase B			Cour. phase B
4		32			15			Total Phase C Current			Phase C Curr		Courant total phase C			Cour. phase C
5		32			15			Total Phase A Power			Phase A Power		Puissance total phase A			Pui. phase A
6		32			15			Total Phase B Power			Phase B Power		Puissance total phase B			Pui. phase B
7		32			15			Total Phase C Power			Phase C Power		Puissance total phase C			Pui. phase C
8		32			15			AC Unit Type				AC Unit Type		Type Unit AC				Type Unit AC
9		32			15			AC Board				AC Board		Module AC				Module AC
10		32			15			No AC Board				No ACBoard		Sans carte AC				Sans AC
11		32			15			SM-AC					SM-AC			SM-AC					SM-AC
12		32			15			Mains Failure				Mains Failure		Manque Secteur				Manque Secteur
13		32			15			Existence State				Existence State		Module Présent				Module Présent
14		32			15			Existent				Existent		Présent					Présent
15		32			15			Non-Existent				Non-Existent		Absent					Absent
16		32			15			Total Input Current			Input Current		Courant d'entrée total			I Total entree
