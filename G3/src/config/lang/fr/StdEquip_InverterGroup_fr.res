﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Inverter Group				Invt Group			Groupe d'onduleurs				Invt Group
2		32			15			Total Current				Tot Invt Curr		Courant total				Courant total

4		32			15			Inverter Capacity Used			Sys Cap Used		Capacité de l'onduleur utilisée				Sys Cap Used
5		32			15			Maximum Capacity Used			Max Cap Used		Capacité maximale utilisée				Max Cap Used
6		32			15			Minimum Capacity Used			Min Cap Used		Capacité maximale utilisée				Max Cap Used
7		37			15			Total Inverters Communicating	Num Invts Comm			Nombre total d'onduleurs communiquant				Num Invts Comm
8		32			15			Valid Inverters					Valid Inverters			Onduleurs valides					Valid Inverters
9		32			15			Number of Inverters				Num of Invts			Nombre d'onduleurs					Num of Invts
10		32			15			Number of Phase1				Number of Phase1		Nombre de L1						Nombre de L1
11		32			15			Number of Phase2				Number of Phase2		Nombre de L2						Nombre de L2
12		32			15			Number of Phase3				Number of Phase3		Nombre de L3						Nombre de L3
13		32			15			Current of Phase1				Current of Phase1		Courant de L1						Courant de L1
14		32			15			Current of Phase2				Current of Phase2		Courant de L2						Courant deL2
15		32			15			Current of Phase3				Current of Phase3		Courant de L3						Courant de L3
16		32			15			Power_kW of Phase1				Power_kW of Phase1		Puissance de L1 en kW					Puissance L1kW
17		32			15			Power_kW of Phase2				Power_kW of Phase2		Puissance de L2 en kW					Puissance L2kW
18		32			15			Power_kW of Phase3				Power_kW of Phase3		Puissance de L3 en kW					Puissance L3kW
19		32			15			Power_kVA of Phase1				Power_kVA of Phase1		Puissance de L1 en kVA					Puissance L1KVA
20		32			15			Power_kVA of Phase2				Power_kVA of Phase2		Puissance de L2 en kVA					Puissance L2KVA
21		32			15			Power_kVA of Phase3				Power_kVA of Phase3		Puissance de L3 en kVA					Puissance L3KVA
22		32			15			Rated Current					Rated Current			Courant nominal					Courant nominal
23		32			15			Input Total Energy				Input Energy			Énergie d'entrée					Énergie d'entrée
24		32			15			Output Total Energy				Output Energy			Énergie de sortie					Énergie de sortie	
25		32			15			Inverter Sys Set ASYNC			Sys Set ASYNC			Onduleur Sys Set ASYNC				Sys Set ASYNC
26		32			15			Inverter AC Phase Abnormal		ACPhaseAbno				Phase CA de l'onduleur anormale				ACPhaseAbno
27		32			15			Inverter REPO					REPO					REPO						REPO
28		33			15			Inverter Output Freq ASYNC		OutputFreqASYNC			Fréq de sortie du variateur ASYNC				OutputFreqASYNC
29		32			18			Output On/Off			Output On/Off		Contrôle marche/arrêt de sortie				Output On/Off Ctrl
30		32			15			Inverters LED Control			Invt LED Ctrl			Contrôle LED des onduleurs				Invt LED Ctrl
31		37			15			Fan Speed				Fan Speed			Contrôle de la vitesse du ventilateur				Fan Speed Ctrl
32		32			15			Invt Work Mode					Invt Work Mode			Mode de travail Invt				En mode travail
33		32			17			Output Voltage Level			O/P Voltage Level	Niveau de tension de sortie				O/P Voltage Level
34		32			15			Output Frequency					Output Freq			Fréquence de sortie			Fréq de sortie
35		32			15			Source Ratio					Source Ratio			Ratio source					Ratio source
36		32			15			Normal					Normal			Ordinaire					Ordinaire
37		32			15			Fail					Fail			Échouer					Échouer
38		32			15			Switch Off All				Switch Off All		Désactiver tout				Désactiver tout 
39		32			15			Switch On All				Switch On All		Allumer tout				Allumer tout
42		32			15			All Flashing				All Flashing		Tout clignotant				Tout clignotant
43		32			15			Stop Flashing				Stop Flashing		Arrêtez de clignoter				Stop Flashing
44		32			15			Full Speed				Full Speed		Pleine vitesse					Pleine vitesse
45		32			15			Automatic Speed				Auto Speed		Vitesse automatique			Auto Speed
54		32			15			Disabled				Disabled		désactivé					désactivé
55		32			15			Enabled					Enabled			Activé					Activé
56		32			15			100V					100V			100V				100V
57		32			15			110V					110V			110V				110V
58		32			15			115V					115V			115V				115V
59		32			15			120V					120V			120V				120V
60		32			15			125V					125V			125V				125V
61		32			15			200V					200V			200V				200V
62		32			15			208V					208V			208V				208V
63		32			15			220V					220V			220V				220V
64		32			15			230V					230V			230V				230V
65		32			15			240V					240V			240V				240V
66		32			15			Auto					Auto			Auto				Auto
67		32			15			50Hz					50Hz			50Hz				50Hz
68		32			15			60Hz					60Hz			60Hz				60Hz
69		32			15			Input DC Current		Input DC Current		Courant CC d'entrée				Courant DC
70		32			15			Input AC Current		Input AC Current		Courant d'entrée CA				Courant AC
71		32			17			Inverter Work Status	InvtWorkStatus			Statut de travail de l'onduleur				Statut de travail
72		32			15			Off					Off						De							De
73		32			15			No					No						Non							Non
74		32			15			Yes					Yes						Oui							Oui
75		32			15			Part On				Part On					Part On						Part On
76		32			15			All On				All On					All On						All On

77		32			15			DC Low Voltage Off		DCLowVoltOff		DC basse tension désactivée			DCLowVoltOff
78		32			15			DC Low Voltage On		DCLowVoltOn		Basse tension CC activée			DCLowVoltOn
79		32			15			DC High Voltage Off	DCHiVoltOff		DC haute tension désactivé			DCHiVoltOff
80		32			15			DC High Voltage On	DCHighVoltOn		Haute tension CC activée			DCHighVoltOn
81		32			15			Last Inverters Quantity		Last Invts Qty		Dernière quantité d'onduleurs				Last Invts Qty
82		32			15			Inverter Lost				Inverter Lost		Onduleur perdu				Onduleur perdu
83		32			15			Inverter Lost				Inverter Lost		Onduleur perdu				Onduleur perdu
84		32			15			Clear Inverter Lost Alarm		Clear Invt Lost		Effacer l'alarme perdue			Clear Invt Lost
86		32			15			Confirm Inverter ID/Phase			Confirm ID/PH			Confirmer l'ID/Phase de l'onduleur			Confirm ID/PH
92		32			15			E-Stop Function				E-Stop Function		E-Stop fonction				E-Stop fonction
93		32			15			AC Phases				AC Phases		Phases AC				Phases AC
94		32			15			Single Phase				Single Phase		Monophasé					Monophasé
95		32			15			Three Phases				Three Phases		Trois phases					Trois phases
101		32			15			Sequence Start Interval			Start Interval		Intervalle de démarrage				Start Interval
104		64			15			All Inverters Communication Failure		AllInvtCommFail		Tous les échecs de comm			AllInvtCommFail
113		32			15			Invt Info Change (M/S Internal Use)	InvtInfo Change		Invt Info Change			InvtInfo Change
119		32			15			Clear Inverter Comm Fail Alarm		ClrInvtCommFail		Effacer l'alarme d'échec de comm			ClrInvtCommFail
126		32			15			None					None			Aucun					Aucun
143		32			16			Existence State				Existence State		État d'existence				État d'existence
144		32			15			Existent				Existent		Existant					Existant
145		32			15			Not Existent				Not Existent		Pas existant					Pas existant
146		32			15			Total Output Power			Output Power		Puissance de sortie				Puissance
147		32			15			Total Slave Rated Current		Total RatedCurr		Courant nominal esclave total				Courant nominal
148		32			15			Reset Inverter IDs			Reset Invt IDs		Réinitialiser les ID de l'onduleur				Reset Invt IDs
149		32			15			HVSD Voltage Difference (24V)		HVSD Volt Diff		Différence de tension HVSD(24V)				HVSD Volt Diff
150		32			15			Normal Update				Normal Update		Mise à jour normale				Normal Update
151		32			15			Force Update				Force Update		Forcer la mise à jour				Force Update
152		32			15			Update OK Number			Update OK Num		Mettre à jour le numéro OK				Update OK Num
153		32			15			Update State				Update State		État de mise à jour				Update State
154		32			15			Updating				Updating		Mise à jour					Mise à jour
155		32			15			Normal Successful			Normal Success		Succès normal				Succès normal
156		32			15			Normal Failed				Normal Fail		Échec normal				Échec normal
157		32			16			Force Successful			Force Success		Forcer le succès				Forcer le succès
158		32			15			Force Failed				Force Fail		Échec forcé				Échec forcé
159		32			15			Communication Time-Out			Comm Time-Out		Délai de communication				Délai de comm
160		32			15			Open File Failed			Open File Fail		Échec de l'ouverture du fichier				Open File Fail
161		32			15			AC mode						AC mode					Mode AC				Mode AC
162		32			15			DC mode						DC mode					Mode DC				Mode DC

#163		32			15			Safety mode					Safety mode				Mode de sécurité				Safety mode
#164		32			15			Idle  mode					Idle  mode				Mode inactif				Mode inactif
165		32			16			Max used capacity			Max used capacity		Capacité maximale utilisée				Cap max utilisée
166		32			16			Min used capacity			Min used capacity		Capacité minimale utilisée				Cap min utilisée
167		32			15			Average Voltage				Invt Voltage			Tension moyenne				Tension moyenne
168		32			15			Invt Redundancy Number		Invt Redu Num			Numéro de redondance Invt			Invt Redu Num
169		32			18			Inverter High Load			Inverter High Load		Onduleur haute charge				Invt Charge élevée
170		32			18			Rated Power					Rated Power				Puissance nominale				Puissance nominale

171		32			15			Clear Fault					Clear Fault				Effacer le défaut				Clear Fault
172		32			15			Reset Energy				Reset Energy			Réinitialiser l'énergie				Reset Energy

173		32			15			Syncronization Phase Failure			SynErrPhaType			Type de phase d'erreurs de sync			SynErrPhaType
174		32			15			Syncronization Voltage Failure			SynErrVoltType			Syn Err Volt Type			SynErrVoltType
175		32			15			Syncronization Frequency Failure			SynErrFreqLvl			Syn Err Freq Level			SynErrFreqLvl
176		32			15			Syncronization Mode Failure			SynErrWorkMode			Mode de travail Syn Err			SynErrWorkMode
177		32			15			Syn Err Low Volt TR			SynErrLVoltTR			Syn Err Low Volt TR			SynErrLVoltTR
178		32			15			Syn Err Low Volt CB			SynErrLVoltCB			Syn Err Low Volt CB			SynErrLVoltCB
179		32			15			Syn Err High Volt TR		SynErrHVoltTR			Syn Err High Volt TR			SynErrHVoltTR
180		32			15			Syn Err High Volt CB		SynErrHVoltCB			Syn Err High Volt CB			SynErrHVoltCB
181		32			15			Set Para To Invt			SetParaToInvt			Définir le para pour invt			SetParaToInvt
182		32			15			Get Para From Invt			GetParaFromInvt			Obtenez Para d'Invt			GetParaFromInvt
183		32			15			Power Used					Power Used				Puissance utilisée				Power Used
184		32			15			Input AC Voltage			Input AC Volt			Tension CA d'entrée 			Input AC Volt	
#185     32          15          Input AC Voltage Phase A       InpVoltPhA           Tension CA d'entrée A      InpVoltPhA A
185		32			16			Input AC Phase A			Input AC VolPhA			Entrée AC Phase A 			Entrée AC PhaseA
186		32			16			Input AC Phase B			Input AC VolPhB			Entrée AC Phase B 			Entrée AC PhaseB
187		32			16			Input AC Phase C			Input AC VolPhC			Entrée AC Phase C 			Entrée AC PhaseC		
188		32			15			Total output voltage			Total output V			Tension de sortie totale			Total output V
189		32			15			Power in kW					PowerkW				Puissance en kW				PuisskW
190		32			15			Power in kVA					PowerkVA				Puissance en kVA				PuisskVA
197		32			15			Maximum Power Capacity					Max Power Cap				Capacité de puissance maximale				Cappuissmax