﻿#
#  Locale language support:fr
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN					FULL_IN_LOCALE				ABBR_IN_LOCALE
1	32			15			PH1ModuleNum					PH1ModuleNum					PH1ModuleNom				PH1ModuleNom
2	32			15			PH1RedundAmount					PH1RedundAmount					PH1RedundQuant				PH1RedundQuant
3	32			15			PH2ModuleNum					PH2ModuleNum					PH2ModuleNom				PH2ModuleNom
4	32			15			PH2RedundAmount					PH2RedundAmount					PH2RedundQuant				PH2RedundQuant
5	32			15			PH3ModuleNum					PH3ModuleNum					PH3ModuleNom				PH3ModuleNom
6	32			15			PH3RedundAmount					PH3RedundAmount					PH3RedundQuant				PH3RedundQuant
7	32			15			PH4ModuleNum					PH4ModuleNum					PH4ModuleNom				PH4ModuleNom
8	32			15			PH4RedundAmount					PH4RedundAmount					PH4RedundQuant				PH4RedundQuant
9	32			15			PH5ModuleNum					PH5ModuleNum					PH5ModuleNom				PH5ModuleNom
10	32			15			PH5RedundAmount					PH5RedundAmount					PH5RedundQuant				PH5RedundQuant
11	32			15			PH6ModuleNum					PH6ModuleNum					PH6ModuleNom				PH6ModuleNom
12	32			15			PH6RedundAmount					PH6RedundAmount					PH6RedundQuant				PH6RedundQuant
13	32			15			PH7ModuleNum					PH7ModuleNum					PH7ModuleNom				PH7ModuleNom
14	32			15			PH7RedundAmount					PH7RedundAmount					PH7RedundQuant				PH7RedundQuant
15	32			15			PH8ModuleNum					PH8ModuleNum					PH8ModuleNom				PH8ModuleNom
16	32			15			PH8RedundAmount					PH8RedundAmount					PH8RedundQuant				PH8RedundQuant

17	32			15			PH1ModNumSeen					PH1ModNumSeen					PH1ModNomVu en				PH1ModNomVu en
18	32			15			PH2ModNumSeen					PH2ModNumSeen					PH2ModNomVu en				PH2ModNomVu en
19	32			15			PH3ModNumSeen					PH3ModNumSeen					PH3ModNomVu en				PH3ModNomVu en
20	32			15			PH4ModNumSeen					PH4ModNumSeen					PH4ModNomVu en				PH4ModNomVu en
21	32			15			PH5ModNumSeen					PH5ModNumSeen					PH5ModNomVu en				PH5ModNomVu en
22	32			15			PH6ModNumSeen					PH6ModNumSeen					PH6ModNomVu en				PH6ModNomVu en
23	32			15			PH7ModNumSeen					PH7ModNumSeen					PH7ModNomVu en				PH7ModNomVu en
24	32			15			PH8ModNumSeen					PH8ModNumSeen					PH8ModNomVu en				PH8ModNomVu en
							
25	32			15			ACG1ModNumSeen					ACG1ModNumSeen					ACG1ModNomVu en				ACG1ModNomVu en
26	32			15			ACG2ModNumSeen					ACG2ModNumSeen					ACG2ModNomVu en				ACG2ModNomVu en
27	32			15			ACG3ModNumSeen					ACG3ModNumSeen					ACG3ModNomVu en				ACG3ModNomVu en
28	32			15			ACG4ModNumSeen					ACG4ModNumSeen					ACG4ModNomVu en				ACG4ModNomVu en
																			
29	32			15			DCG1ModNumSeen					DCG1ModNumSeen					ACG1ModNomVu en				ACG1ModNomVu en
30	32			15			DCG2ModNumSeen					DCG2ModNumSeen					ACG2ModNomVu en				ACG2ModNomVu en
31	32			15			DCG3ModNumSeen					DCG3ModNumSeen					ACG3ModNomVu en				ACG3ModNomVu en
32	32			15			DCG4ModNumSeen					DCG4ModNumSeen					ACG4ModNomVu en				ACG4ModNomVu en
33	32			15			DCG5ModNumSeen					DCG5ModNumSeen					ACG5ModNomVu en				ACG5ModNomVu en
34	32			15			DCG6ModNumSeen					DCG6ModNumSeen					ACG6ModNomVu en				ACG6ModNomVu en
35	32			15			DCG7ModNumSeen					DCG7ModNumSeen					ACG7ModNomVu en				ACG7ModNomVu en
36	32			15			DCG8ModNumSeen					DCG8ModNumSeen					ACG8ModNomVu en				ACG8ModNomVu en

37	32			15			TotalAlm Num					TotalAlm Num					Nom Total Alm				Nom Total Alm

98	32			15			T2S Controller					T2S Controller					T2S Manette				T2S Manette
99	32			15			Communication Failure				Com Failure					Échec Communication			Échec Comm
100	32			15			Existence State					Existence State					Etat d'existence			Etat Existence

101	32			15			Fan Failure					Fan Failure					Panne du ventilateur			Panne Vent
102	32			15			Too Many Starts					Too Many Starts					Trop de démarrages			Trop Démarr
103	32			15			Overload Too Long				LongTOverload					Surcharge trop long			Surcharge Long
104	32			15			Out Of Sync					Out Of Sync					Désynchronisés				Désynchronisés
105	32			15			Temperature Too High				Temp Too High					Température trop élevée			TempTrop élevée
106	32			15			Communication Bus Failure			Com Bus Fail					Panne de communication Bus		Panne Comm Bus
107	32			15			Communication Bus Conflict			Com BusConflict					Conflit Bus Communication		ConflitBusComm
108	32			15			No Power Source					No Power					Pas Puissance				Pas Puissance	
109	32			15			Communication Bus Failure			Com Bus Fail					Panne de communication Bus		Panne Comm Bus	
110	32			15			Phase Not Ready					Phase Not Ready					Phase non prête				Phase Non Prête	
111	32			15			Inverter Mismatch				Inverter Mismatch				onduleur Mismatch			Ondule Mismatch	
112	32			15			Backfeed Error					Backfeed Error					Erreur de retour			Erreur Retour	
113	32			15			Com Bus Fail					Com Bus Fail					Panne Comm Bus				Panne Comm Bus
114	32			15			Com Bus Fail					Com Bus Fail					Panne Comm Bus				Panne Comm Bus
115	32			15			Overload Current				Overload Curr					Courant de surcharge			Cour Surcharge	
116	32			15			Communication Bus Mismatch			ComBusMismatch					Différence Bus Comm			Diff Bus Comm
117	32			15			Temperature Derating				Temp Derating					Température Déclassement		Temp Déclass	
118	32			15			Overload Power					Overload Power					surcharge Puissance			Surchar Puiss
119	32			15			Undervoltage Derating				Undervolt Derat					Déclassement Sous-tension		Déclass Sous-V	
120	32			15			Fan Failure					Fan Failure					Panne du ventilateur			Panne Vent
121	32			15			Remote Off					Remote Off					Remote Off				Remote Off	
122	32			15			Manually Off					Manually Off					Désactivé manuellement			Désact Manuel
123	32			15			Input AC Voltage Too Low			Input AC Too Low				EntrACTropFaibl				EntrACTropFaibl
124	32			15			Input AC Voltage Too High			Input AC Too High				EntrACTropHaute				EntrACTropHaute
125	32			15			Input AC Voltage Too Low			Input AC Too Low				EntrACTropFaibl				EntrACTropFaibl
126	32			15			Input AC Voltage Too High			Input AC Too High				EntrACTropHaute				EntrACTropHaute
127	32			15			Input AC Inconform				Input AC Inconform				EntrACInconform				EntrACInconform
128	32			15			Input AC Inconform				Input AC Inconform				EntrACInconform				EntrACInconform
129	32			15			Input AC Inconform				Input AC Inconform				EntrACInconform				EntrACInconform
130	32			15			Power Disabled					Power Disabled					Puissance désactivée			Puiss Désact
131	32			15			Input AC Inconformity				Input AC Inconform				Entrée AC Inconformity			EntrACInconform
132	32			15			Input AC THD Too High				Input AC THD High				Entrée AC THD trop élevée		EntrACTHDElevée
133	32			15			Output AC Not Synchronized			AC Out Of Sync					Sortie AC non synchronisée		AC Out Of Sync
134	32			15			Output AC Not Synchronized			AC Out Of Sync					Sortie AC non synchronisée		AC Out Of Sync
135	32			15			Inverters Not Synchronized			Out Of Sync					Onduleurs Non Synchronisé		Désynchronisés	
136	32			15			Synchronization Failure				Sync Failure					Echec de synchronisation		Echec Synchron
137	32			15			Input AC Voltage Too Low			Input AC Too Low				EntréeACFaible				EntréeACFaible
138	32			15			Input AC Voltage Too High			Input AC Too High				EntréeACHaute				EntréeACHaute
139	32			15			Input AC Frequency Too Low			Frequency Low					FréqFaible				FréqFaible
140	32			15			Input AC Frequency Too High			Frequency High					FréqHaute				FréqHaute
141	32			15			Input DC Voltage Too Low			Input DC Too Low				EntréeDCFaible				EntréeDCFaible
142	32			15			Input DC Voltage Too High			Input DC Too High				EntréeDCHaute				EntréeDCHaute
143	32			15			Input DC Voltage Too Low			Input DC Too Low				EntréeDCFaible				EntréeDCFaible
144	32			15			Input DC Voltage Too High			Input DC Too High				EntréeDCHaute				EntréeDCHaute
145	32			15			Input DC Voltage Too Low			Input DC Too Low				EntréeDCFaible				EntréeDCFaible
146	32			15			Input DC Voltage Too Low			Input DC Too Low				EntréeDCFaible				EntréeDCFaible
147	32			15			Input DC Voltage Too High			Input DC Too High				EntréeDCHaute				EntréeDCHaute
148	32			15			Digital Input 1 Failure				DI1 Failure					DI1 Échec				DI1 Échec	
149	32			15			Digital Input 2 Failure				DI2 Failure					DI2 Échec				DI2 Échec
150	32			15			Redundancy Lost					Redundancy Lost					Redondance perdue			Redond Perdue
151	32			15			Redundancy+1 Lost				Redund+1 Lost					Redondance+1 perdue			Redond+1 Perdue
152	32			15			System Overload					Sys Overload					Surcharge du système			Surcharge Sys	
153	32			15			Main Source Lost				Main Lost					Main Perdue				Main Perdue	
154	32			15			Secondary Source Lost				Secondary Lost					Secondaire perdu			Second Perdu
155	32			15			T2S Bus Failure					T2S Bus Fail					T2S Echec Bus				T2S Echec Bus
156	32			15			T2S Failure					T2S Fail					T2S Échouer				T2S Échouer	
157	32			15			Log Nearly Full					Log Full					Journal Complet				Journal Complet	
158	32			15			T2S Flash Error					T2S Flash Error					T2S FLASH Erreur			T2S FLASH Err	
159	32			15			Check Log File					Check Log File					Véri Fichier journal			VériFichJournal
160	32			15			Module Lost					Module Lost					Module Perdu				Module Perdu	

300	32			15			Device Number of Alarm 1			Dev Num Alm1					Dispositif Nombre d'alarme 1		DisposNomAlm 1
301	32			15			Type of Alarm 1					Type of Alm1					Type Alm 1				Type Alm 1
302	32			15			Device Number of Alarm 2			Dev Num Alm2					Dispositif Nombre d'alarme 2		DisposNomAlm 2
303	32			15			Type of Alarm 2					Type of Alm2					Type Alm 2				Type Alm 2
304	32			15			Device Number of Alarm 3			Dev Num Alm3					Dispositif Nombre d'alarme 3		DisposNomAlm 3
305	32			15			Type of Alarm 3					Type of Alm3					Type Alm 3				Type Alm 3
306	32			15			Device Number of Alarm 4			Dev Num Alm4					Dispositif Nombre d'alarme 4		DisposNomAlm 4
307	32			15			Type of Alarm 4					Type of Alm4					Type Alm 4				Type Alm 4
308	32			15			Device Number of Alarm 5			Dev Num Alm5					Dispositif Nombre d'alarme 5		DisposNomAlm 5
309	32			15			Type of Alarm 5					Type of Alm5					Type Alm 5				Type Alm 5

310	32			15			Device Number of Alarm 6			Dev Num Alm6					Dispositif Nombre d'alarme 6		DisposNomAlm 6
311	32			15			Type of Alarm 6					Type of Alm6					Type Alm 6				Type Alm 6
312	32			15			Device Number of Alarm 7			Dev Num Alm7					Dispositif Nombre d'alarme 7		DisposNomAlm 7
313	32			15			Type of Alarm 7					Type of Alm7					Type Alm 7				Type Alm 7
314	32			15			Device Number of Alarm 8			Dev Num Alm8					Dispositif Nombre d'alarme 8		DisposNomAlm 8
315	32			15			Type of Alarm 8					Type of Alm8					Type Alm 8				Type Alm 8
316	32			15			Device Number of Alarm 9			Dev Num Alm9					Dispositif Nombre d'alarme 9		DisposNomAlm 9
317	32			15			Type of Alarm 9					Type of Alm9					Type Alm 9				Type Alm 9
318	32			15			Device Number of Alarm 10			Dev Num Alm10					Dispositif Nombre d'alarme 10		DisposNomAlm 10
319	32			15			Type of Alarm 10				Type of Alm10					Type Alm 10				Type Alm 10

320	32			15			Device Number of Alarm 11			Dev Num Alm11					Dispositif Nombre d'alarme 11		DisposNomAlm 11
321	32			15			Type of Alarm 11				Type of Alm11					Type Alm 11				Type Alm 11
322	32			15			Device Number of Alarm 12			Dev Num Alm12					Dispositif Nombre d'alarme 12		DisposNomAlm 12
323	32			15			Type of Alarm 12				Type of Alm12					Type Alm 12				Type Alm 12
324	32			15			Device Number of Alarm 13			Dev Num Alm13					Dispositif Nombre d'alarme 13		DisposNomAlm 13
325	32			15			Type of Alarm 13				Type of Alm13					Type Alm 13				Type Alm 13
326	32			15			Device Number of Alarm 14			Dev Num Alm14					Dispositif Nombre d'alarme 14		DisposNomAlm 14
327	32			15			Type of Alarm 14				Type of Alm14					Type Alm 14				Type Alm 14
328	32			15			Device Number of Alarm 15			Dev Num Alm15					Dispositif Nombre d'alarme 15		DisposNomAlm 15
329	32			15			Type of Alarm 15				Type of Alm15					Type Alm 15				Type Alm 15
																			
330	32			15			Device Number of Alarm 16			Dev Num Alm16					Dispositif Nombre d'alarme 16		DisposNomAlm 16
331	32			15			Type of Alarm 16				Type of Alm16					Type Alm 16				Type Alm 16
332	32			15			Device Number of Alarm 17			Dev Num Alm17					Dispositif Nombre d'alarme 17		DisposNomAlm 17
333	32			15			Type of Alarm 17				Type of Alm17					Type Alm 17				Type Alm 17
334	32			15			Device Number of Alarm 18			Dev Num Alm18					Dispositif Nombre d'alarme 18		DisposNomAlm 18
335	32			15			Type of Alarm 18				Type of Alm18					Type Alm 18				Type Alm 18
336	32			15			Device Number of Alarm 19			Dev Num Alm19					Dispositif Nombre d'alarme 19		DisposNomAlm 19
337	32			15			Type of Alarm 19				Type of Alm19					Type Alm 19				Type Alm 19
338	32			15			Device Number of Alarm 20			Dev Num Alm20					Dispositif Nombre d'alarme 20		DisposNomAlm 20
339	32			15			Type of Alarm 20				Type of Alm20					Type Alm 20				Type Alm 20
																								
340	32			15			Device Number of Alarm 21			Dev Num Alm21					Dispositif Nombre d'alarme 21		DisposNomAlm 21
341	32			15			Type of Alarm 21				Type of Alm21					Type Alm 21				Type Alm 21
342	32			15			Device Number of Alarm 22			Dev Num Alm22					Dispositif Nombre d'alarme 22		DisposNomAlm 22
343	32			15			Type of Alarm 22				Type of Alm22					Type Alm 22				Type Alm 22
344	32			15			Device Number of Alarm 23			Dev Num Alm23					Dispositif Nombre d'alarme 23		DisposNomAlm 23
345	32			15			Type of Alarm 23				Type of Alm23					Type Alm 23				Type Alm 23
346	32			15			Device Number of Alarm 24			Dev Num Alm24					Dispositif Nombre d'alarme 24		DisposNomAlm 24
347	32			15			Type of Alarm 24				Type of Alm24					Type Alm 24				Type Alm 24
348	32			15			Device Number of Alarm 25			Dev Num Alm25					Dispositif Nombre d'alarme 25		DisposNomAlm 25
349	32			15			Type of Alarm 25				Type of Alm25					Type Alm 25				Type Alm 25
																			
350	32			15			Device Number of Alarm 26			Dev Num Alm26					Dispositif Nombre d'alarme 26		DisposNomAlm 26
351	32			15			Type of Alarm 26				Type of Alm26					Type Alm 26				Type Alm 26
352	32			15			Device Number of Alarm 27			Dev Num Alm27					Dispositif Nombre d'alarme 27		DisposNomAlm 27
353	32			15			Type of Alarm 27				Type of Alm27					Type Alm 27				Type Alm 27
354	32			15			Device Number of Alarm 28			Dev Num Alm28					Dispositif Nombre d'alarme 28		DisposNomAlm 28
355	32			15			Type of Alarm 28				Type of Alm28					Type Alm 28				Type Alm 28
356	32			15			Device Number of Alarm 29			Dev Num Alm29					Dispositif Nombre d'alarme 29		DisposNomAlm 29
357	32			15			Type of Alarm 29				Type of Alm29					Type Alm 29				Type Alm 29
358	32			15			Device Number of Alarm 30			Dev Num Alm30					Dispositif Nombre d'alarme 30		DisposNomAlm 30
359	32			15			Type of Alarm 30				Type of Alm30					Type Alm 30				Type Alm 30
																								
360	32			15			Device Number of Alarm 31			Dev Num Alm31					Dispositif Nombre d'alarme 31		DisposNomAlm 31
361	32			15			Type of Alarm 31				Type of Alm31					Type Alm 31				Type Alm 31
362	32			15			Device Number of Alarm 32			Dev Num Alm32					Dispositif Nombre d'alarme 32		DisposNomAlm 32
363	32			15			Type of Alarm 32				Type of Alm32					Type Alm 32				Type Alm 32
364	32			15			Device Number of Alarm 33			Dev Num Alm33					Dispositif Nombre d'alarme 33		DisposNomAlm 33
365	32			15			Type of Alarm 33				Type of Alm33					Type Alm 33				Type Alm 33
366	32			15			Device Number of Alarm 34			Dev Num Alm34					Dispositif Nombre d'alarme 34		DisposNomAlm 34
367	32			15			Type of Alarm 34				Type of Alm34					Type Alm 34				Type Alm 34
368	32			15			Device Number of Alarm 35			Dev Num Alm35					Dispositif Nombre d'alarme 35		DisposNomAlm 35
369	32			15			Type of Alarm 35				Type of Alm35					Type Alm 35				Type Alm 35
																			
370	32			15			Device Number of Alarm 36			Dev Num Alm36					Dispositif Nombre d'alarme 36		DisposNomAlm 36
371	32			15			Type of Alarm 36				Type of Alm36					Type Alm 36				Type Alm 36
372	32			15			Device Number of Alarm 37			Dev Num Alm37					Dispositif Nombre d'alarme 37		DisposNomAlm 37
373	32			15			Type of Alarm 37				Type of Alm37					Type Alm 37				Type Alm 37
374	32			15			Device Number of Alarm 38			Dev Num Alm38					Dispositif Nombre d'alarme 38		DisposNomAlm 38
375	32			15			Type of Alarm 38				Type of Alm38					Type Alm 38				Type Alm 38
376	32			15			Device Number of Alarm 39			Dev Num Alm39					Dispositif Nombre d'alarme 39		DisposNomAlm 39
377	32			15			Type of Alarm 39				Type of Alm39					Type Alm 39				Type Alm 39
378	32			15			Device Number of Alarm 40			Dev Num Alm40					Dispositif Nombre d'alarme 40		DisposNomAlm 40
379	32			15			Type of Alarm 40				Type of Alm40					Type Alm 40				Type Alm 40
																								
380	32			15			Device Number of Alarm 41			Dev Num Alm41					Dispositif Nombre d'alarme 41		DisposNomAlm 41
381	32			15			Type of Alarm 41				Type of Alm41					Type Alm 41				Type Alm 41
382	32			15			Device Number of Alarm 42			Dev Num Alm42					Dispositif Nombre d'alarme 42		DisposNomAlm 42
383	32			15			Type of Alarm 42				Type of Alm42					Type Alm 42				Type Alm 42
384	32			15			Device Number of Alarm 43			Dev Num Alm43					Dispositif Nombre d'alarme 43		DisposNomAlm 43
385	32			15			Type of Alarm 43				Type of Alm43					Type Alm 43				Type Alm 43
386	32			15			Device Number of Alarm 44			Dev Num Alm44					Dispositif Nombre d'alarme 44		DisposNomAlm 44
387	32			15			Type of Alarm 44				Type of Alm44					Type Alm 44				Type Alm 44
388	32			15			Device Number of Alarm 45			Dev Num Alm45					Dispositif Nombre d'alarme 45		DisposNomAlm 45
389	32			15			Type of Alarm 45				Type of Alm45					Type Alm 45				Type Alm 45
																			
390	32			15			Device Number of Alarm 46			Dev Num Alm46					Dispositif Nombre d'alarme 46		DisposNomAlm 46
391	32			15			Type of Alarm 46				Type of Alm46					Type Alm 46				Type Alm 46
392	32			15			Device Number of Alarm 47			Dev Num Alm47					Dispositif Nombre d'alarme 47		DisposNomAlm 47
393	32			15			Type of Alarm 47				Type of Alm47					Type Alm 47				Type Alm 47
394	32			15			Device Number of Alarm 48			Dev Num Alm48					Dispositif Nombre d'alarme 48		DisposNomAlm 48
395	32			15			Type of Alarm 48				Type of Alm48					Type Alm 48				Type Alm 48
396	32			15			Device Number of Alarm 49			Dev Num Alm49					Dispositif Nombre d'alarme 49		DisposNomAlm 49
397	32			15			Type of Alarm 49				Type of Alm49					Type Alm 49				Type Alm 49
398	32			15			Device Number of Alarm 50			Dev Num Alm50					Dispositif Nombre d'alarme 50		DisposNomAlm 50
399	32			15			Type of Alarm 50				Type of Alm50					Type Alm 50				Type Alm 50
