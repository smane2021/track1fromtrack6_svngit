﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Manual					Manual			Manuel					Manuel
2		32			15			Auto					Auto			Auto					Auto
3		32			15			Off					Off			Non					Non
4		32			15			On					On			Oui					Oui
5		32			15			No Input				No Input		Pas d'entrée				Pas d'entrée
6		32			15			Input 1					Input 1			Entrée 1				Entrée 1
7		32			15			Input 2					Input 2			Entrée 2				Entrée 2
8		32			15			Input 3					Input 3			Entrée 3				Entrée 3
9		32			15			No Input				No Input		Pas d'entrée				Pas d'entrée
10		32			15			Input					Input			Entrée					Entrée
11		32			15			Close					Close			Fermé					Fermé
12		32			15			Open					Open			Ouvert					Ouvert
13		32			15			Close					Close			Fermé					Fermé
14		32			15			Open					Open			Ouvert					Ouvert
15		32			15			Close					Close			Fermé					Fermé
16		32			15			Open					Open			Ouvert					Ouvert
17		32			15			Close					Close			Fermé					Fermé
18		32			15			Open					Open			Ouvert					Ouvert
19		32			15			Close					Close			Fermé					Fermé
20		32			15			Open					Open			Ouvert					Ouvert
21		32			15			Close					Close			Fermé					Fermé
22		32			15			Open					Open			Ouvert					Ouvert
23		32			15			Close					Close			Fermé					Fermé
24		32			15			Open					Open			Ouvert					Ouvert
25		32			15			Close					Close			Fermé					Fermé
26		32			15			Open					Open			Ouvert					Ouvert
27		32			15			1-Phase					1-Phase			Mono Phasé				Mono Phasé
28		32			15			3-Phase					3-Phase			Tri Phasé				Tri Phasé
29		32			15			No Measurement				No Measurement		Pas de mesure				Pas de mesure
30		32			15			1-Phase					1-Phase			Mono Phasé				Mono Phasé
31		32			15			3-Phase					3-Phase			Tri Phasé				Tri Phasé
32		32			15			Response				Response		Réponse					Réponse
33		32			15			Not Responding				Not Responding		Non Réponse				Non Réponse
34		32			15			AC Distribution				AC Distribution		Distribution AC				Distribution AC
35		32			15			Mains 1 Uab/Ua				1 Uab/Ua		Secteur 1 V phase 1			AC 1 V phase 1
36		32			15			Mains 1 Ubc/Ub				1 Ubc/Ub		Secteur 1 V phase 2			AC 1 V phase 2
37		32			15			Mains 1 Uca/Uc				1 Uca/Uc		Secteur 1 V phase 3			AC 1 V phase 3
38		32			15			Mains 2 Uab/Ua				2 Uab/Ua		Secteur 2 V phase 1			AC 2 V phase 1
39		32			15			Mains 2 Ubc/Ub				2 Ubc/Ub		Secteur 2 V phase 2			AC 2 V phase 2
40		32			15			Mains 2 Uca/Uc				2 Uca/Uc		Secteur 2 V phase 3			AC 2 V phase 3
41		32			15			Mains 3 Uab/Ua				3 Uab/Ua		Secteur 3 V phase 1			AC 3 V phase 1
42		32			15			Mains 3 Ubc/Ub				3 Ubc/Ub		Secteur 3 V phase 2			AC 3 V phase 2
43		32			15			Mains 3 Uca/Uc				3 Uca/Uc		Secteur 3 V phase 3			AC 3 V phase 3
53		32			15			Working Phase A Current			Phase A Curr		Courant Phase 1				Courant Phase 1
54		32			15			Working Phase B Current			Phase B Curr		Courant Phase 2				Courant Phase 2
55		32			15			Working Phase C Current			Phase C Curr		Courant Phase 3				Courant Phase 3
56		32			15			AC Input Frequency			AC Input Freq		Fréquence Entrée AC			Fréq. Entr CA
57		32			15			AC Input Switch Mode			AC Switch Mode		Mode commutateur entrée AC		Mode Commut. AC
58		32			15			Fault Lighting Status			Fault Lighting		Défaut parafoudre			Déf parafoudre
59		32			15			Mains 1 Input Status			1 Input Status		Etat entrée AC 1			Etat entrée AC1
60		32			15			Mains 2 Input Status			2 Input Status		Etat entrée AC 2			Etat entrée AC2
61		32			15			Mains 3 Input Status			3 Input Status		Etat entrée AC 3			Etat entrée AC3
62		32			15			AC Output 1 Status			Output 1 Status		Etat Sortie AC 1			Etat Sortie AC1
63		32			15			AC Output 2 Status			Output 2 Status		Etat Sortie AC 2			Etat Sortie AC2
64		32			15			AC Output 3 Status			Output 3 Status		Etat Sortie AC 3			Etat Sortie AC3
65		32			15			AC Output 4 Status			Output 4 Status		Etat Sortie AC 4			Etat Sortie AC4
66		32			15			AC Output 5 Status			Output 5 Status		Etat Sortie AC 5			Etat Sortie AC5
67		32			15			AC Output 6 Status			Output 6 Status		Etat Sortie AC 6			Etat Sortie AC6
68		32			15			AC Output 7 Status			Output 7 Status		Etat Sortie AC 7			Etat Sortie AC7
69		32			15			AC Output 8 Status			Output 8 Status		Etat Sortie AC 8			Etat Sortie AC8
70		32			15			AC Input Frequency High			Frequency High		Fréquence AC Haute			Fréq. AC Haute
71		32			15			AC Input Frequency Low			Frequency Low		Fréquence AC Basse			Fréq. AC Basse
72		32			15			AC Input MCCB Trip			Input MCCB Trip		Disjoncteur Entrée Ouvert 	DJ Entrée Ouver 
73		32			15			SPD Trip				SPD Trip		Défaut parafoudre			Déf Parafoudre
74		32			15			AC Output MCCB Trip			OutputMCCB Trip		Défaut Protect.Parafoudre		Déf.Prot.Para
75		32			15			AC Input 1 Failure			Input 1 Failure		Défaut entrée AC 1			Déf entrée AC1
76		32			15			AC Input 2 Failure			Input 2 Failure		Défaut entrée AC 2			Déf entrée AC2
77		32			15			AC Input 3 Failure			Input 3 Failure		Défaut entrée AC 3			Déf entrée AC3
78		32			15			Mains 1 Uab/Ua Low			M1 Uab/a UnderV		V Bas AC1 Ph1				V Bas AC1 Ph1
79		32			15			Mains 1 Ubc/Ub Low			M1 Ubc/b UnderV		V Bas AC1 Ph2				V Bas AC1 Ph2
80		32			15			Mains 1 Uca/Uc Low			M1 Uca/c UnderV		V Bas AC1 Ph3				V Bas AC1 Ph3
81		32			15			Mains 2 Uab/Ua Low			M2 Uab/a UnderV		V Bas AC2 Ph1				V Bas AC2 Ph1
82		32			15			Mains 2 Ubc/Ub Low			M2 Ubc/b UnderV		V Bas AC2 Ph2				V Bas AC2 Ph2
83		32			15			Mains 2 Uca/Uc Low			M2 Uca/c UnderV		V Bas AC2 Ph3				V Bas AC2 Ph3
84		32			15			Mains 3 Uab/Ua Low			M3 Uab/a UnderV		V Bas AC3 Ph1				V Bas AC3 Ph1
85		32			15			Mains 3 Ubc/Ub Low			M3 Ubc/b UnderV		V Bas AC3 Ph1				V Bas AC3 Ph1
86		32			15			Mains 3 Uca/Uc Low			M3 Uca/c UnderV		V Bas AC3 Ph2				V Bas AC3 Ph2
87		32			15			Mains 1 Uab/Ua High			M1 Uab/a OverV		V Haut AC1 Ph1				V Haut AC1 Ph1
88		32			15			Mains 1 Ubc/Ub High			M1 Ubc/b OverV		V Haut AC1 Ph2				V Haut AC1 Ph2
89		32			15			Mains 1 Uca/Uc High			M1 Uca/c OverV		V Haut AC1 Ph3				V Haut AC1 Ph3
90		32			15			Mains 2 Uab/Ua High			M2 Uab/a OverV		V Haut AC2 Ph1				V Haut AC2 Ph1
91		32			15			Mains 2 Ubc/Ub High			M2 Ubc/b OverV		V Haut AC2 Ph2				V Haut AC2 Ph2
92		32			15			Mains 2 Uca/Uc High			M2 Uca/c OverV		V Haut AC2 Ph3				V Haut AC2 Ph3
93		32			15			Mains 3 Uab/Ua High			M3 Uab/a OverV		V Haut AC3 Ph1				V Haut AC3 Ph1
93		32			15			Mains 3 Ubc/Ub High			M3 Ubc/b OverV		V Haut AC3 Ph2				V Haut AC3 Ph2
94		32			15			Mains 3 Uca/Uc High			M3 Uca/c OverV		V Haut AC3 Ph3				V Haut AC3 Ph3
95		32			15			Mains 1 Uab/Ua Failure			M1 Uab/a Fail		Défaut AC1 Ph1				Défaut AC1 Ph1
96		32			15			Mains 1 Ubc/Ub Failure			M1 Ubc/b Fail		Défaut AC1 Ph2				Défaut AC1 Ph2
97		32			15			Mains 1 Uca/Uc Failure			M1 Uca/c Fail		Défaut AC1 Ph3				Défaut AC1 Ph3
98		32			15			Mains 2 Uab/Ua Failure			M2 Uab/a Fail		Défaut AC2 Ph1				Défaut AC2 Ph1
99		32			15			Mains 2 Ubc/Ub Failure			M2 Ubc/b Fail		Défaut AC2 Ph2				Défaut AC2 Ph2
100		32			15			Mains 2 Uca/Uc Failure			M2 Uca/c Fail		Défaut AC2 Ph3				Défaut AC2 Ph3
101		32			15			Mains 3 Uab/Ua Failure			M3 Uab/a Fail		Défaut AC3 Ph1				Défaut AC3 Ph1
102		32			15			Mains 3 Ubc/Ub Failure			M3 Ubc/b Fail		Défaut AC3 Ph2				Défaut AC3 Ph2
103		32			15			Mains 3 Uca/Uc Failure			M3 Uca/c Fail		Défaut AC3 Ph3				Défaut AC3 Ph3
104		32			15			No Response				No Response		Pas de réponse				Pas de réponse
105		32			15			Overvoltage Limit			Overvolt Limit		Niveau surtension			Niveau sur U
106		32			15			Undervoltage Limit			Undervolt Limit		Niveau soustension			Niveau sous U
107		32			15			Phase Failure Voltage			Phase Fail Volt		Tension de Défaut			Tension Défaut
108		32			15			Overfrequency Limit			Overfreq Limit		Fréquence Maximum			Fréquence Max
109		32			15			Underfrequency Limit			Underfreq Limit		Fréquence Mimimum			Fréquence Mim
110		32			15			Current Transformer Coeff		Curr Trans Coef		Coefficient Transformateur		Coeff Transfo
111		32			15			Input Type				Input Type		Type entrée				Type entrée
112		32			15			Input Num				Input Num		Numéro Entrée				Numéro Entrée
113		32			15			Current Measurement			Curr Measure		Mesure courant				Mesure courant
114		32			15			Output Num				Output Num		Numéro de Sortie			Numéro de Sortie
115		32			15			Distribution Address			Distr Addr		Adresse Distribution			Adresse Distrib
116		32			15			Mains 1 Failure				Mains 1 Fail		Défaut AC1				Défaut AC1
117		32			15			Mains 2 Failure				Mains 2 Fail		Défaut AC2				Défaut AC2
118		32			15			Mains 3 Failure				Mains 3 Fail		Défaut AC3				Défaut AC3
119		32			15			Mains 1 Uab/Ua Failure			M1 Uab/a Fail		Défaut AC1 Ph1				Défaut AC1 Ph1
120		32			15			Mains 1 Ubc/Ub Failure			M1 Ubc/b Fail		Défaut AC1 Ph2				Défaut AC1 Ph2
121		32			15			Mains 1 Uca/Uc Failure			M1 Uca/c Fail		Défaut AC1 Ph3				Défaut AC1 Ph3
122		32			15			Mains 2 Uab/Ua Failure			M2 Uab/a Fail		Défaut AC2 Ph1				Défaut AC2 Ph1
123		32			15			Mains 2 Ubc/Ub Failure			M2 Ubc/b Fail		Défaut AC2 Ph2				Défaut AC2 Ph2
124		32			15			Mains 2 Uca/Uc Failure			M2 Uca/c Fail		Défaut AC2 Ph3				Défaut AC2 Ph3
125		32			15			Mains 3 Uab/Ua Failure			M3 Uab/a Fail		Défaut AC3 Ph1				Défaut AC3 Ph1
126		32			15			Mains 3 Ubc/Ub Failure			M3 Ubc/b Fail		Défaut AC3 Ph2				Défaut AC3 Ph2
127		32			15			Mains 3 Uca/Uc Failure			M3 Uca/c Fail		Défaut AC3 Ph3				Défaut AC3 Ph3
128		32			15			Overfrequency				Overfrequency		Fréquence AC Haute			Fréq. AC Haute
129		32			15			Underfrequency				Underfrequency		Fréquence AC Basse			Fréq. AC Basse
130		32			15			Mains 1 Uab/Ua UnderVoltage		M1 Uab/a UnderV		Sous U AC1 Ph 1				Sous U AC1 Ph1
131		32			15			Mains 1 Ubc/Ub UnderVoltage		M1 Ubc/b UnderV		Sous U AC1 Ph 2				Sous U AC1 Ph2
132		32			15			Mains 1 Uca/Uc UnderVoltage		M1 Uca/c UnderV		Sous U AC1 Ph 3				Sous U AC1 Ph3
133		32			15			Mains 2 Uab/Ua UnderVoltage		M2 Uab/a UnderV		Sous U AC2 Ph 1				Sous U AC2 Ph1
134		32			15			Mains 2 Ubc/Ub UnderVoltage		M2 Ubc/b UnderV		Sous U AC2 Ph 2				Sous U AC2 Ph2
135		32			15			Mains 2 Uca/Uc UnderVoltage		M2 Uca/c UnderV		Sous U AC2 Ph 3				Sous U AC2 Ph3
136		32			15			Mains 3 Uab/Ua UnderVoltage		M3 Uab/a UnderV		Sous U AC3 Ph 1				Sous U AC3 Ph1
137		32			15			Mains 3 Ubc/Ub UnderVoltage		M3 Ubc/b UnderV		Sous U AC3 Ph 2				Sous U AC3 Ph2
138		32			15			Mains 3 Uca/Uc UnderVoltage		M3 Uca/c UnderV		Sous U AC3 Ph 3				Sous U AC3 Ph3
139		32			15			Mains 1 Uab/Ua OverVoltage		M1 Uab/a OverV		Sur U AC1 Ph 1				Sur U AC1 Ph1
140		32			15			Mains 1 Ubc/Ub OverVoltage		M1 Ubc/b OverV		Sur U AC1 Ph 2				Sur U AC1 Ph2
141		32			15			Mains 1 Uca/Uc OverVoltage		M1 Uca/c OverV		Sur U AC1 Ph 3				Sur U AC1 Ph3
142		32			15			Mains 2 Uab/Ua OverVoltage		M2 Uab/a OverV		Sur U AC2 Ph 1				Sur U AC2 Ph1
143		32			15			Mains 2 Ubc/Ub OverVoltage		M2 Ubc/b OverV		Sur U AC2 Ph 2				Sur U AC2 Ph2
144		32			15			Mains 2 Uca/Uc OverVoltage		M2 Uca/c OverV		Sur U AC2 Ph 3				Sur U AC2 Ph3
145		32			15			Mains 3 Uab/Ua OverVoltage		M3 Uab/a OverV		Sur U AC3 Ph 1				Sur U AC3 Ph1
146		32			15			Mains 3 Ubc/Ub Over Voltage		M3 Ubc/b OverV		Sur U AC3 Ph 2				Sur U AC3 Ph2
147		32			15			Mains 3 Uca/Uc Over Voltage		M3 Uca/c OverV		Sur U AC3 Ph 3				Sur U AC3 Ph3
148		32			15			AC Input MCCB Trip			In-MCCB Trip		Défaut Disjoncteur Entrée 	Def DJ Entrée 
149		32			15			AC Output MCCB Trip			Out-MCCB Trip		Défaut Disjoncteur Sortie 	Def DJ Sortie
150		32			15			SPD Trip				SPD Trip		Défaut Parafoudre			Déf Parafoudre
169		32			15			No Response				No Response		Pas de réponse				Pas réponse
170		32			15			Mains Failure				Mains Failure		Défaut secteur				Défaut secteur
171		32			15			Large AC Distribution Unit		Large AC Dist		Module Distribution AC		Mod. Distrib AC
172		32			15			No Alarm				No Alarm		Aucune Alarme				Aucune Alarme
173		32			15			Overvoltage				Overvolt		Sous Tension				Sous Tension
174		32			15			Undervoltage				Undervolt		Sur Tension				Sur Tension
175		32			15			AC Phase Failure			AC Phase Fail		Défaut Phase AC				Déf. Phase AC
176		32			15			No Alarm				No Alarm		Aucune Alarme				Aucune Alarme
177		32			15			Overfrequency				Overfrequency		Fréquence Haute				Fréquence Haute
178		32			15			Underfrequency				Underfrequency		Fréquence Basse				Fréquence Basse
179		32			15			No Alarm				No Alarm		Aucune Alarme				Aucune Alarme
180		32			15			AC Overvoltage				AC Overvolt		Sous Tension				Sous Tension
181		32			15			AC Undervoltage				AC Undervolt		Sur Tension				Sur Tension
182		32			15			AC Phase Failure			AC Phase Fail		Défaut Phase AC				Déf. Phase AC
183		32			15			No Alarm				No Alarm		Aucune Alarme				Aucune Alarme
184		32			15			AC Overvoltage				AC Overvolt		Sous Tension				Sous Tension
185		32			15			AC Undervoltage				AC Undervolt		Sur Tension				Sur Tension
186		32			15			AC Phase Failure			AC Phase Fail		Défaut Phase AC				Déf. Phase AC
187		32			15			Mains 1 Uab/Ua Alarm			M1 Uab/a Alarm		Alarme U AC1 Ph 1			Al U AC1 Ph1
188		32			15			Mains 1 Ubc/Ub Alarm			M1 Ubc/b Alarm		Alarme U AC1 Ph 2			Al U AC1 Ph2
189		32			15			Mains 1 Uca/Uc Alarm			M1 Uca/c Alarm		Alarme U AC1 Ph 3			Al U AC1 Ph3
190		32			15			Frequency Alarm				Freq Alarm		Alarme Fréquence			Al. Fréquence
191		32			15			No Response				No Response		Aucune Réponse				Aucune Réponse
192		32			15			Normal					Normal			Normal					Normal
193		32			15			Failure					Failure			Défaut					Défaut
194		32			15			No Response				No Response		Aucune Réponse				Aucune Réponse
195		32			15			Mains Input No				Mains Input No		Nombre entrée AC			Nbre entrée AC
196		32			15			No. 1					No. 1			1 entrée				1 entrée
197		32			15			No. 2					No. 2			2 entrées				2 entrées
198		32			15			No. 3					No. 3			3 entrées				3 entrées
199		32			15			None					None			Aucune					Aucune
200		32			15			Emergency Light				Emergency Light		Arrêt d'urgence				Arrêt Urgence
201		32			15			Close					Close			Fermé				Fermé
202		32			15			Open					Open			Ouvert				Ouvert
203		32			15			Mains 2 Uab/Ua Alarm			M2 Uab/a Alarm		Alarme U AC2 Ph 1			Al U AC2 Ph1
204		32			15			Mains 2 Ubc/Ub Alarm			M2 Ubc/b Alarm		Alarme U AC2 Ph 2			Al U AC2 Ph2
205		32			15			Mains 2 Uca/Uc Alarm			M2 Uca/c Alarm		Alarme U AC2 Ph 3			Al U AC2 Ph3
206		32			15			Mains 3 Uab/Ua Alarm			M3 Uab/a Alarm		Alarme U AC3 Ph 1			Al U AC3 Ph1
207		32			15			Mains 3 Ubc/Ub Alarm			M3 Ubc/b Alarm		Alarme U AC3 Ph 2			Al U AC3 Ph2
208		32			15			Mains 3 Uca/Uc Alarm			M3 Uca/c Alarm		Alarme U AC3 Ph 3			Al U AC3 Ph3
209		32			15			Normal					Normal			Normal					Normal
210		32			15			Alarm					Alarm			Alarme					Alarme
211		32			15			AC Fuse Number				AC Fuse No.		Nombre départs AC			Nombre dép.AC
212		32			15			Existence State				Existence State		Détection				Détection
213		32			15			Existent				Existent		Présent					Présent
214		32			15			Non-Existent				Non-Existent		Non Présent				Non Présent
