﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			BMS				BMS		BMS				BMS
2	32			15			Rated Capacity				Rated Capacity		额定容量			额定容量
3	32			15			Communication Fail			Comm Fail		
通讯失败			Comm Fail
4	32			15			Existence State				Existence State		存在状态			存在状态
5	32			15			Existent				Existent		存在的					存在的
6	32			15			Not Existent				Not Existent		不存在				不存在
7	32			15			Battery Voltage				Batt Voltage		电池电压				Batt Voltage
8	32			15			Battery Current				Batt Current		电池电流			Batt Current
9	32			15			Battery Capacity (Ah)			Batt Cap(Ah)		电池容量 (Ah)			Batt Cap(Ah)
10	32			15			Battery Capacity (%)			Batt Cap (%)		电池容量 (%)			Batt Cap(%)
11	32			15			Bus Voltage				Bus Voltage		母线电压				母线电压
12	32			15			Battery Center Temperature(AVE)		Batt Temp(AVE)		电池中心温度（AVE）		Batt Temp(AVE)
13	32			15			Ambient Temperature			Ambient Temp		环境温度			Ambient Temp
29	32			15			Yes					Yes			是					是
30	32			15			No					No			没有					没有
31	32			15			Cell Over Voltage Alarm			CellOverVolt		电池过压报警		CellOverVolt
32	32			15			Cell Under Voltage Alarm		CellUnderVolt		电池欠压报警			CellUnderVolt voltSimple
33	32			15			Battery Over Voltage Alarm		Batt OverVolt		电池过压报警			Batt OverVolt
34	32			15			Battery Under Voltage Alarm		Batt UnderVolt		电池欠压报警			Batt UnderVolt
35	32			15			Charge Over Current Alarm		ChargeOverCurr		充电过电流警报		ChargeOverCurr
36	32			15			Discharge Over Current Alarm		DisCharOverCurr		放电过电流报警		DisCharOverCurr
37	32			15			High Battery Temperature Alarm		HighBattTemp		电池温度过高警报		HighBattTemp
38	32			15			Low Battery Temperature Alarm		LowBattTemp		电池温度过低警报	LowBattTemp
39	32			15			High Ambient Temperature Alarm		HighAmbientTemp		高环境温度警报	HighAmbientTemp
40	32			15			Low Ambient Temperature Alarm		LowAmbientTemp		低环境温度警报	LowAmbientTemp
41	32			15			Charge High Temperature Alarm		ChargeHighTemp		充电高温报警	ChargeHighTemp
42	32			15			Charge Low Temperature Alarm		ChargeLowTemp		充电低温警报	ChargeLowTemp
43	32			15			Discharge High Temperature Alarm	DisCharHighTemp		高温放电报警	DisCharHighTemp
44	32			15			Discharge Low Temperature Alarm		DisCharLowTemp		放电低温报警	DisCharLowTemp
45	32			15			Low Battery Capacity Alarm		LowBattCapacity		电池电量不足警报		LowBattCapacity
46	32			15			High Voltage Difference Alarm		HighVoltDiff		高电压差报警		HighVoltDiff
47	32			15			Cell Over Voltage Protect		Cell OverVProt		电池过压保护		Cell OverVProt
48	32			15			Battery Over Voltage Protect		Batt OverVProt		电池过压保护		Batt OverVProt
49	32			15			Cell Under Voltage Protect		Cell UnderVProt		电池欠压保护		Cell UnderVProt
50	32			15			Battery  Under Voltage Protect		Batt UnderVProt		电池欠压保护		Batt UnderVProt
51	32			15			Charge High Current Protect		ChargeHiCurProt		充电大电流保护 	ChargeHiCurProt
52	32			15			Charge Very High Current Protect	ChgVHiCurProt		充电极高电流保护 	ChgVHiCurProt
53	32			15			Discharge High Current Protect		ChargeHiCurProt		放电大电流保护 	ChargeHiCurProt
54	32			15			Discharge Very High Current Protect	DischVHiCurProt		放电极高电流保护		DischVHiCurProt
55	32			15			Short Circuit Protect			ShortCircProt		短路保护		ShortCircProt
56	32			15			Over Current Protect			OverCurrProt		过电流保护			OverCurrProt
57	32			15			Charge High Temperature Protect		CharHiTempProt		充电高温保护		CharHiTempProt
58	32			15			Charge Low Temperature Protect		CharLoTempProt		充电低温保护		CharLoTempProt
59	32			15			Discharge High Temp Protect		DischHiTempProt		放电高温保护	DischHiTempProt
60	32			15			Discharge Low Temp Protect		DischLoTempProt		放电低温保护	DischLoTempProt
61	32			15			Front End Sample Comm Fault		SampCommFault		前端样本通信故障		SampCommFault
62	32			15			Temp Sensor Disconnect Fault		TempSensDiscFlt		温度传感器断开故障	TempSensDiscFlt
63	32			15			Charge MOS Fault			ChargeMOSFault		充电MOS故障			ChargeMOSFault
64	32			15			Discharge MOS Fault			DischMOSFault		放电MOS故障			DischMOSFault
65	32			15			Charging				Charging		充电中				充电中
66	32			15			Discharging				Discharging		卸货				卸货
67	32			15			Charging MOS Breakover			CharMOSBrkover		充电MOS突破			CharMOSBrkover
68	32			15			Discharging MOS Breakover		DischMOSBrkover		释放MOS突破		DischMOSBrkover
69	32			15			Current Limit Activation		CurrLmtAct		限流激活		现行法令
70	32			15			SR status				SRstatus		SR状态				SR状态
71	32			15			Communication Address			CommAddress		通讯地址		
通讯地址



