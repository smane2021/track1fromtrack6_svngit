﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			IB Group		IB Group		Groupe Carte IB		Groupe Carte IB
2		32			15			Load Current				Load Current		Courant Utilisation			I Util.
3		32			15			DC Distribution				DC			Distribution DC				Distrib.DC
4		32			15			Load Shunt				Load Shunt		Shunt Utilisation			Shunt Util.
5		32			15			Disabled				Disabled		Inactif					Inactif
6		32			15			Enabled					Enabled			Actif					Actif
7		32			15			Existence State		Existence State		Présence Carte		Présent Carte
8		32			15			Existent		Existent		Présente		Présente
9		32			15			Non-Existent		Non-Existent		Non Présente		Non Présente
10		32			15			IB1					IB1			IB1					IB1
11		32			15			IB2					IB2			IB2					IB2
12		32			15			NA					NA			NA					NA
