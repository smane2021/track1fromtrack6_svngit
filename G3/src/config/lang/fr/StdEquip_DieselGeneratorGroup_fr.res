﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Date of Last Diesel Test		Last Test Date		Date du dernier test			Dernier test
2		32			15			Diesel Test In Progress			Diesel Test		Test en cours				Test en cours
3		32			15			Diesel Test Result			Test Result		Résultat du test			Résultat test
4		32			15			Start Diesel Test			Start Test		Démarrage du test			Démarrage test
5		32			15			Max duration for Diesel Test		Max Test Time		Temps maximum du test			Temps max test
6		32			15			Schedule Diesel Test Enabled		PlanTest Enable		Test électrogène planifié actif		Tst plan actif
7		32			15			Diesel Control Inhibit			CTRL Inhibit		Inhibition diesel			Inhi. diesel
8		32			15			Diesel Test In Progress			Diesel Test		Test en cours				Test en cours
9		32			15			Diesel Test Failure			Test Failure		Défaut test				Défaut test
10		32			15			no					no			Non					Non
11		32			15			yes					yes			Oui					Oui
12		32			15			Reset Diesel Test Error			Reset Test Err		RAZ défaut test				RAZ défaut test
13		32			15			Delay before New Test			New Test Delay		Délais nouveau test			Délais nou. test
14		32			15			Number of schedule Test per year	Test Number		Numero test				No test
15		32			15			Test 1 date				Test 1 date		Date test 1				Date test 1
16		32			15			Test 2 date				Test 2 date		Date test 2				Date test 2
17		32			15			Test 3 date				Test 3 date		Date test 3				Date test 3
18		32			15			Test 4 date				Test 4 date		Date test 4				Date test 4
19		32			15			Test 5 date				Test 5 date		Date test 5				Date test 5
20		32			15			Test 6 date				Test 6 date		Date test 6				Date test 6
21		32			15			Test 7 date				Test 7 date		Date test 7				Date test 7
22		32			15			Test 8 date				Test 8 date		Date test 8				Date test 8
23		32			15			Test 9 date				Test 9 date		Date test 9				Date test 9
24		32			15			Test 10 date				Test 10 date		Date test 10				Date test 10
25		32			15			Test 11 date				Test 11 date		Date test 11				Date test 11
26		32			15			Test 12 date				Test 12 date		Date test 12				Date test 12
27		32			15			Normal					Normal			Normal					Normal
28		32			15			Manual Stop				Manual Stop		Arrêt manuel				Arrêt manuel
29		32			15			Time is up				Time is up		Dépacement de temps			Dépace. temps
30		32			15			In Man State				In Man State		Etat manuel				Etat manuel
31		32			15			Low Battery Voltage			Low Batt Vol		Tension basse batterie			Tension bas batt
32		32			15			High Water Temperature			High Water Temp		Température eau haute			Temp. eau haute
33		32			15			Low Oil Pressure			Low Oil Press		Pression d huile basse			Pres. huile bas
34		32			15			Low Fuel Level				Low Fuel Level		Niveau bas fuel				Niv bas fuel
35		32			15			Diesel Failure				Diesel Failure		Défaut GrpElec				Défaut GrpElec
36		32			15			Diesel Generator Group			Diesel Group		Groupe électrogén			Grp électrogén
37		32			15			State					State			Etat					Etat
38		32			15			Existence State				Existence State		Module présent				Module présent
39		32			15			Existent				Existent		Présent					Présent
40		32			15			Non-Existent				Non-Existent		Non Présent				Non Présent
41		32			15			Total Input Current			Input Current		Courant d'entrée total			CouranEntrTotal
