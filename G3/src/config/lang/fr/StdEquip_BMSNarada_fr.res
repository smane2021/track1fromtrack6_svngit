﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			BMS				BMS		BMS				BMS
2	32			15			Rated Capacity				Rated Capacity		Capacité nominale			Cap nominale
3	32			15			Communication Fail			Comm Fail		Échec Communication			Échec Comm
4	32			15			Existence State				Existence State		État Existence				État Existence
5	32			15			Existent				Existent		Existent				Existent
6	32			15			Not Existent				Not Existent		Pas existant				Pas existant
7	32			15			Battery Voltage				Batt Voltage		Voltage Batterie			Volt Batt
8	32			15			Battery Current				Batt Current		Courant Batterie			Courant Batt
9	32			15			Battery Rating (Ah)			Batt Rating(Ah)		Évaluation batterie(Ah)			Évalu batt(Ah)
10	32			15			Battery Capacity (%)			Batt Cap (%)		Capacité batterie (%)			Cap Batt (%)
11	32			15			Bus Voltage				Bus Voltage		Tension Bus				Tension Bus
12	32			15			Battery Center Temperature(AVE)		Batt Temp(AVE)		Température Centre Batterie(AVE)	TempBat(AVE)
13	32			15			Ambient Temperature			Ambient Temp		Température Ambiante			Temp ambiante
29	32			15			Yes					Yes			Oui					Oui
30	32			15			No					No			Non					Non
31	32			15			Single Over Voltage Alarm		SingleOverVolt		Unique Surtension alarme		UniqSurtension
32	32			15			Single Under Voltage Alarm		SingleUnderVolt		Unique Soustension alarme		UniqSoustension
33	32			15			Whole Over Voltage Alarm		WholeOverVolt		Tout Surtension alarme		ToutSurtension
34	32			15			Whole Under Voltage Alarm		WholeUnderVolt		Tout Soustension alarme		ToutSoustension
35	32			15			Charge Over Current Alarm		ChargeOverCurr		Charge sur l'alarme Courant		ChargeSurCour
36	32			15			Discharge Over Current Alarm		DisCharOverCurr		Décharge sur l'alarme Courant		DéchargeSurCour
37	32			15			High Battery Temperature Alarm		HighBattTemp		Alarme haute température batterie	HauteTempBatt
38	32			15			Low Battery Temperature Alarm		LowBattTemp		Alarme basse température batterie	BasseTempBatt
39	32			15			High Ambient Temperature Alarm		HighAmbientTemp		Alarme haute température Ambiant	HauteTempAmb
40	32			15			Low Ambient Temperature Alarm		LowAmbientTemp		Alarme basse température Ambiant	BasseTempAmb
41	32			15			High PCB Temperature Alarm		HighPCBTemp		Haute température PCB alarme		HauteTempPCB
42	32			15			Low Battery Capacity Alarm		LowBattCapacity		Capacité batterie faible Alarme		CapBattBasse
43	32			15			High Voltage Difference Alarm		HighVoltDiff		Alarme haute différence tension		HauteDifTension
44	32			15			Single Over Voltage Protect		SingOverVProt		Unique Surtension Protéger		UniqSur V Prot
45	32			15			Single Under Voltage Protect		SingUnderVProt		Unique Soustension Protéger		UniqSous V Prot
46	32			15			Whole Over Voltage Protect		WholeOverVProt		Tout Surtension Protéger		ToutSur V Prot
47	32			15			Whole Under Voltage Protect		WholeUnderVProt		Tout Soustension Protéger		ToutSous V Prot
48	32			15			Short Circuit Protect			ShortCircProt		Court circuit Protéger			CourCircuitProt
49	32			15			Over Current Protect			OverCurrProt		Sur Court Protég			Sur Court Prot
50	32			15			Charge High Temperature Protect		CharHiTempProt		Charge haute Temp protège		ChargeHaTempPro
51	32			15			Charge Low Temperature Protect		CharLoTempProt		Charge Bas Temp protège			ChargeBaTempPro
52	32			15			Discharge High Temp Protect		DischHiTempProt		Décharge haute Temp protège		DéchaHaTempPro
53	32			15			Discharge Low Temp Protect		DischLoTempProt		Décharge Bas Temp protège		DéchaBaTempPro
54	32			15			Front End Sample Comm Fault		SampCommFault		Exemple Comm Fault Front End		ExempCommFaute
55	32			15			Temp Sensor Disconnect Fault		TempSensDiscFlt		Temp défaut capteur déconnexion		TempFauteCapDéc
56	32			15			Charging				Charging		Charger				Charger
57	32			15			Discharging				Discharging		Déchargement				Déchargement
58	32			15			Charging MOS Breakover			CharMOSBrkover		Charging MOS Breakover			CharMOSBrkover
59	32			15			Dischanging MOS Breakover		DischMOSBrkover		Déchanging MOS Breakover		DéchMOSBrkover
60	32			15			Communication Address			CommAddress		Adresse de communication		AdresseComm
61	32			15			Current Limit Activation		CurrLmtAct		Activlimitecourant			ActivLmtCour



