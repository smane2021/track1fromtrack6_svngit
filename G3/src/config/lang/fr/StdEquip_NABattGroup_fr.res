﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bridge Card				Bridge Card		Bridge Card				Bridge Card
2		32			15			Total Batt Curr				Total Batt Curr		Total Batt Curr				Total Batt Curr
3		32			15			Average Batt Volt			Average Batt Volt	Average Batt Volt			Average Batt Volt
4		32			15			Average Batt Temp			Average Batt Temp	Average Batt Temp			Average Batt Temp
5		32			15			Batt Lost				Batt Lost		Batt Lost				Batt Lost
6		32			15			Number of Installed			NumberOf Inst		Number of Installed			NumberOf Inst
7		32			15			Number of Disconncted			NumberOf Discon		Number of Disconncted			NumberOf Discon
8		32			15			Number of No Reply			NumberOf NoReply	Number of No Reply			NumberOf NoReply
9		32			15			Inventory Updating			Invent Update		Inventory Updating			Invent Update
10		32			15			BridgeCard Firmware Version		BdgCard FirmVer		BridgeCard Firmware Version		BdgCard FirmVer
11		32			15			Bridge Card Barcode 1			BdgCard Barcode1	Bridge Card Barcode 1			BdgCard Barcode1
12		32			15			Bridge Card Barcode 2			BdgCard Barcode2	Bridge Card Barcode 2			BdgCard Barcode2
13		32			15			Bridge Card Barcode 3			BdgCard Barcode3	Bridge Card Barcode 3			BdgCard Barcode3
14		32			15			Bridge Card Barcode 4			BdgCard Barcode4	Bridge Card Barcode 4			BdgCard Barcode4
50		32			15			All no response				All no response		All no response				All no response
51		32			15			Multi no response			Multil no response	Multi no response			Multil no response
52		32			15			Batt Lost				Batt Lost		Batt Lost				Batt Lost
99		32			15			Interrupt				Interrupt		Interrupt				Interrupt
100		32			15			Exist Stat				Exist Stat		Exist Stat				Exist Stat
101		32			15			Normal					Normal			Normal					Normal
102		32			15			Alarm					Alarm			Alarm					Alarm
103		32			15			Exist					Exist			Exist					Exist
104		32			15			No Exist				No Exist		No Exist				No Exist
