﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE			
1	32			15			Battery Current				Batt Current		Courant batterie		Courant Batt			
2	32			15			Battery Rating (Ah)     		Batt Rating(Ah)		Évaluation Batterie(Ah)		Évaluat Batt(Ah)			
3	32			15			Bus Voltage				Bus Voltage		Tension Bus		TensionBus		
5	32			15			Battery Over Current			Batt Over Curr		Batterie Surintensité		BatSurintensité		
6	32			15			Battery Capacity (%)			Batt Cap (%)		Capacité batterie(%)		Cap Batt(%)		
7	32			15			Battery Voltage				Batt Voltage		Voltage Batterie		Voltage Batt		
18	32			15			SoNick Battery				SoNick Batt		SoNick Batterie		SoNick Batt		
29	32			15			Yes					Yes			Oui			Oui
30	32			15			No					No			Non			Non
31	32			15			On					On			Sur			Sur
32	32			15			Off					Off			De			De
33	32			15			State					State			Etat			Etat
87	32			15			No					No			Non			Non
96	32			15			Rated Capacity				Rated Capacity		Capacité nominale	Cap nominale	
99	32			15			Battery Temperature(AVE)		Batt Temp(AVE)		Température Batterie	Temp Batt
100	32			15			Board Temperature			Board Temp		Conseil Temp		Conseil Temp
101	32			15			Tc Center Temperature			Tc Center Temp		Tc Temp centrale	TcTempCentrale
102	32			15			Tc Left Temperature			Tc Left Temp		Tc Température gauche		TcTempGauche
103	32			15			Tc Right Temperature			Tc Right Temp		Tc Température droit		TcTempDroit
114	32			15			Battery Communication Fail		Batt Comm Fail		Batterie Échec comm		BattÉchecComm
129	32			15			Low Ambient Temperature			Low Amb Temp		Temp ambiante basse		TempAmbBasse
130	32			15			High Ambient Temperature Warning	High Amb Temp W 	HauteTempAmbianteWarning	HauteTempAmb W	
131	32			15			High Ambient Temperature		High Amb Temp		HauteTempAmbiante		HauteTempAmb	
132	32			15			Low Battery Internal Temperature	Low Int Temp		Temp interne Batterie basse	TempIntBatBasse
133	32			15			High Batt Internal Temp Warning		Hi Int Temp W		HauteTempInterne Batterie Warning	HauteTempInt W	
134	32			15			High Batt Internal Temperature		Hi Bat Int Temp		HauteTempInterne Batterie	HauteTempInt	
135	32			15			Bus Voltage Below 40V			Bus V Below 40V		V Bus Bas 40V		V Bus Bas40V	
136	32			15			Bus Voltage Below 39V			Bus V Below 39V		V Bus Bas 39V		V Bus Bas39V	
137	32			15			Bus Voltage Above 60V			Bus V Above 60V		V Bus Sur 40V		V Bus Sur60V	
138	32			15			Bus Voltage Above 65V			Bus V Above 65V		V Bus Sur 39V		V Bus Sur65V	
139	32			15			High Discharge Current Warning		Hi Disch Curr W		CourDécharge élevé Warning	CourDéchélevé W
140	32			15			High Discharge Current			High Disch Curr		CourDécharge élevé	CourDéchélevé
141	32			15			Main Switch Error			Main Switch Err		Erreur commutateur principal		ErrComPrincipal
142	32			15			Fuse Blown				Fuse Blown		Fusible Soufflé			Fusible Soufflé
143	32			15			Heaters Failure				Heaters Failure		Echec chauffe-eau		EchecChauffe
144	32			15			Thermocouple Failure			Thermocple Fail		Echec thermocouple		EchecThermo
145	32			15			Voltage Measurement Circuit Fail	V M Cir Fail		ÉchecCircuitMesureTension	Échec Cir M V
146	32			15			Current Measurement Circuit Fail	C M Cir Fail		ÉchecCircuitMesureCourant	Échec Cir M C
147	32			15			BMS Hardware Failure			BMS Hdw Failure		BMS Erreur matérielle			BMS ErrMatéri
148	32			15			Hardware Protection Sys Active		HdW Protect Sys		Système Protection Matéri Actif		SysProtMatéri
149	32			15			High Heatsink Temperature 		Hi Heatsink Tmp		Haute température Dissipateur		HauteTempDissip
150	32			15			Battery Voltage Below 39V		Bat V Below 39V		V Bat Bas 39V		V Bat Bas 39V
151	32			15			Battery Voltage Below 38V		Bat V Below 38V		V Bat Bas 38V		V Bat Bas 38V
152	32			15			Battery Voltage Above 53.5V		Bat V Abv 53.5V		V Bat Sur 53.5V		V Bat Sur 53.5V
153	32			15			Battery Voltage Above 53.6V		Bat V Abv 53.6V		V Bat Sur 53.6V		V Bat Sur 53.6V
154	32			15			High Charge Current Warning		Hi Chrge Curr W		CourCharge élevé Warning	CourCh Elevé W
155	32			15			High Charge Current			Hi Charge Curr		CourCharge élevé	CourCh Elevé
156	32			15			High Discharge Current Warning		Hi Disch Curr W		CourDécharge élevé Warning	CourDéchélevé W
157	32			15			High Discharge Current			Hi Disch Curr		CourDécharge élevé	CourDéchélevé
158	32			15			Voltage Unbalance Warning		V unbalance W		Déséquilibre V Warning		Déséq V 
159	32			15			Voltages Unbalance			Volt Unbalance		Déséquilibre V		Déséq V W
160	32			15			Dc Bus Pwr Too Low for Charging		DC Low for Chrg		Dc Bus Pwr Too Low pourChargement		DcLowPourCharge
161	32			15			Charge Regulation Failure		Charge Reg Fail		Règlement prescrivant défaut		RèglPrescDéfaut
162	32			15			Capacity Below 12.5%			Cap Below 12.5%		Capacité Bas 12.5%		Cap Bas 12.5%
163	32			15			Thermocouples Mismatch			Tcples Mismatch		Incompatibilité thermocouples		Incompa Thermo
164	32			15			Heater Fuse Blown			Heater FA		Chauffe Fusible grillé		ChaufFusieGril
