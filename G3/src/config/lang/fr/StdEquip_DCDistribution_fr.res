﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			DC Distribution				DC			Distribution DC				Distrib DC
2		32			15			Output Voltage				Output Volt		Tension DC				Tension DC
3		32			15			Output Current				Output Curr		Courant de charge			I charge
4		32			15			SCU Load Shunt Enable			SCU L-S Enable		Activation shunt Util			Activ SH Util
5		32			15			Disabled				Disabled		Desactive				Desactive
6		32			15			Enabled					Enabled			Active					Active
7		32			15			Shunt Full Current			Shunt Current		Courant Shunt				Courant Shunt
8		32			15			Shunt Full Voltage			Shunt Voltage		Tension Shunt				Tension Shunt
9		32			15			Load Shunt Exists			Shunt Exists		Shunt Présent				Shunt Présent
10		32			15			Yes					Yes			Oui					Oui
11		32			15			No					No			Non					Non
12		32			15			Overvoltage 1				Overvolt 1		Tension DC haute			V DC haut
13		32			15			Overvoltage 2				Overvolt 2		Tension DC Tres haute			V DC Tres haut
14		32			15			Undervoltage 1				Undervolt 1		Tension DC Basse			V DC Basse
15		32			15			Undervoltage 2				Undervolt 2		Tension DC Tres Basse			V DC Tres Basse
16		32			15			Overvoltage 1(24V)			Overvoltage 1		Tension DC haute(24V)			V DC haut(24V)
17		32			15			Overvoltage 2(24V)			Overvoltage 2		Tension DC Tres haute(24V)		V DC Tres haut
18		32			15			Undervoltage 1(24V)			Undervoltage 1		Tension DC Basse(24V)			V DC Basse(24V)
19		32			15			Undervoltage 2(24V)			Undervoltage 2		Tension DC Tres Basse(24V)		V DC Tres Basse
20		32			15			Total Load Current			TotalLoadCurr		Corriente total carga			Total carga
21		32			15			Load Alarm Flag				Load Alarm Flag		Load Alarm Flag				Load Alarm Flag
22		32			15			Current High Current			Curr Hi				CourHaut  Cour		CourHautCour
23		32			15			Current Very High Current		Curr Very Hi		Cour très élevé Cour		CourTrèsElevé
500		32			15			Current Break Size				Curr1 Brk Size		Cour1 Cassez Taille				Cour1TailBrk		
501		32			15			Current High 1 Current Limit	Curr1 Hi1 Limit		Cour1 élevé 1 Lmt Cour				Cour1Elevé1Lmt		
502		32			15			Current High 2 Current Limit	Curr1 Hi2 Lmt		Cour1 élevé 2 Lmt Cour				Cour1Elevé2Lmt			
503		32			15			Current High 1 Curr				Curr Hi1Cur			CourHaut 1 Cour		CourHaut1Cour	
504		32			15			Current High 2 Curr				Curr Hi2Cur			CourHaut 2 Cour		CourHaut2Cour
