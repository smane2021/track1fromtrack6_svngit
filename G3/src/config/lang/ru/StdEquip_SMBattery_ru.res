﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Current				Batt Current		ТокАБ					ТокАБ
2		32			15			Battery Capacity			Batt Capacity		Емкость					Емкость
3		32			15			Battery Voltage				Batt Voltage		НапряжАБ				НапряжАБ
4		32			15			Ambient Temperature			Ambient Temp		ОкрТемп					ОкрТемп
5		32			15			Acid Temperature			Acid Temp		ТемпАБ					ТемпАБ
6		32			15			Total Time of Batt Temp GT 30deg.c	ToT GT 30deg.c		Total Time of Batt Temp GT 30deg.c	ToT GT 30deg.c
7		32			15			Total Time of Batt Temp LT 10deg.c	ToT LT 10deg.c		Total Time of Batt Temp LT 10deg.c	ToT LT 10deg.c
8		32			15			Battery Block 1 Voltage			Block 1 Volt		Блок1Напр				Блок1Напр
9		32			15			Battery Block 2 Voltage			Block 2 Volt		Блок2Напр				Блок2Напр
10		32			15			Battery Block 3 Voltage			Block 3 Volt		Блок3Напр				Блок3Напр
11		32			15			Battery Block 4 Voltage			Block 4 Volt		Блок4Напр				Блок4Напр
12		32			15			Battery Block 5 Voltage			Block 5 Volt		Блок5Напр				Блок5Напр
13		32			15			Battery Block 6 Voltage			Block 6 Volt		Блок6Напр				Блок6Напр
14		32			15			Battery Block 7 Voltage			Block 7 Volt		Блок7Напр				Блок7Напр
15		32			15			Battery Block 8 Voltage			Block 8 Volt		Блок8Напр				Блок8Напр
16		32			15			Battery Block 9 Voltage			Block 9 Volt		Блок9Напр				Блок9Напр
17		32			15			Battery Block 10 Voltage		Block 10 Volt		Блок10Напр				Блок10Напр
18		32			15			Battery Block 11 Voltage		Block 11 Volt		Блок11Напр				Блок11Напр
19		32			15			Battery Block 12 Voltage		Block 12 Volt		Блок12Напр				Блок12Напр
20		32			15			Battery Block 13 Voltage		Block 13 Volt		Блок13Напр				Блок13Напр
21		32			15			Battery Block 14 Voltage		Block 14 Volt		Блок14Напр				Блок14Напр
22		32			15			Battery Block 15 Voltage		Block 15 Volt		Блок15Напр				Блок15Напр
23		32			15			Battery Block 16 Voltage		Block 16 Volt		Блок16Напр				Блок16Напр
24		32			15			Battery Block 17 Voltage		Block 17 Volt		Блок17Напр				Блок17Напр
25		32			15			Battery Block 18 Voltage		Block 18 Volt		Блок18Напр				Блок18Напр
26		32			15			Battery Block 19 Voltage		Block 19 Volt		Блок19Напр				Блок19Напр
27		32			15			Battery Block 20 Voltage		Block 20 Volt		Блок20Напр				Блок20Напр
28		32			15			Battery Block 21 Voltage		Block 21 Volt		Блок21Напр				Блок21Напр
29		32			15			Battery Block 22 Voltage		Block 22 Volt		Блок22Напр				Блок22Напр
30		32			15			Battery Block 23 Voltage		Block 23 Volt		Блок23Напр				Блок23Напр
31		32			15			Battery Block 24 Voltage		Block 24 Volt		Блок24Напр				Блок24Напр
32		32			15			Battery Block 25 Voltage		Block 25 Volt		Блок25Напр				Блок25Напр
33		32			15			Shunt voltage				Shunt voltage		НапрШунта				НапрШунта
34		32			15			Battery Leakage				Batt Leakage		Утечка					Утечка
35		32			15			Low Acid Level				Low Acid Level		НизкУрКислоты				НизкУрКислоты
36		32			15			Battery disconnected			Batt disconnec		АБоткл					АБоткл
37		32			15			Battery high temperature		Batt high temp		ВысТемпАБ				ВысТемпАБ
38		32			15			Battery low temperature			Batt low temp		НизкТемпАБ				НизкТемпАБ
39		32			15			Block voltage difference		Block volt diff		РазлНапрБлоков				РазлНапрБлоков
40		32			15			Battery shunt size			Batt Shunt Size		НоминШунтаАБ				НоминШунтаАБ
41		32			15			Block voltage difference		Block Volt Diff		РазлНапрБлоков				РазлНапрБлоков
42		32			15			Battery high temp limit			High Temp Limit		ВысТемпАБпорог				ВысТемпАБпорог
43		32			15			Battery high temp limit hyst		High temp hyst		ВысТемпАБгист				ВысТемпАБгист
44		32			15			Battery low temp limit			Low Temp Limit		НизкТемпАБпорог				НизкТемпАБпорог
45		32			15			Battery low temp limit hyst		Low temp hyst		НизкТемпАБгист				НизкТемпАБгист
46		32			15			Exceed Current Limit			Over Curr Limit		АварВысТок				АварВысТок
47		32			15			Battery Leakage				Battery Leakage		Утечка					Утечка
48		32			15			Low Acid Level				Low Acid Level		НизкУрКислоты				НизкУрКислоты
49		32			15			Battery disconnected			Batt disconnec		АБоткл					АБоткл
50		32			15			Battery high temperature		Batt high temp		ВысТемпАБ				ВысТемпАБ
51		32			15			Battery low temperature			Batt low temp		НизкТемпАБ				НизкТемпАБ
52		32			15			Cell voltage difference			Cell volt diff		РазлНапрБлоков				РазлНапрБлоков
53		32			15			SM unit fail				SM unit fail		SM BATНеиспр				SM BAT Неиспр
54		32			15			Battery Disconnected			Batt disconnec		АБоткл					АБоткл
55		32			15			No					No			нет					нет
56		32			15			Yes					Yes			да					да
57		32			15			No					No			нет					нет
58		32			15			Yes					Yes			да					да
59		32			15			No					No			нет					нет
60		32			15			Yes					Yes			да					да
61		32			15			No					No			нет					нет
62		32			15			Yes					Yes			да					да
63		32			15			No					No			нет					нет
64		32			15			Yes					Yes			да					да
65		32			15			No					No			нет					нет
66		32			15			Yes					Yes			да					да
67		32			15			No					No			нет					нет
68		32			15			Yes					Yes			да					да
69		32			15			SM Battery				SM Batt			SM Batt					SM Batt
70		32			15			Over Battery current			Ov Batt current		ВысТокАБ				ВысТокАБ
71		32			15			Battery Capacity(%)			Batt Cap(%)		Емкость(%)				Емкость(%)
72		32			15			SM BAT fail				SM BAT fail		SM BAT Неиспр				SM BATНеиспр
73		32			15			no					no			нет					нет
74		32			15			yes					yes			да					да
75		32			15			AI 4					AI 4			AI 4					AI 4
76		32			15			AI 7					AI 7			AI 7					AI 7
77		32			15			DI 4					DI 4			DI 4					DI 4
78		32			15			DI 5					DI 5			DI 5					DI 5
79		32			15			DI 6					DI 6			DI 6					DI 6
80		32			15			DI 7					DI 7			DI 7					DI 7
81		32			15			DI 8					DI 8			DI 8					DI 8
82		32			15			Relay 1 status				Relay 1 status		реле1Сост				реле1Сост
83		32			15			Relay 2 status				Relay 2 status		реле2Сост				реле2Сост
84		32			15			no					no			нет					нет
85		32			15			yes					yes			да					да
86		32			15			no					no			нет					нет
87		32			15			yes					yes			да					да
88		32			15			no					no			нет					нет
89		32			15			yes					yes			да					да
90		32			15			no					no			нет					нет
91		32			15			yes					yes			да					да
92		32			15			no					no			нет					нет
93		32			15			yes					yes			да					да
94		32			15			Off					Off			выкл					выкл
95		32			15			On					On			вкл					вкл
96		32			15			Off					Off			выкл					выкл
97		32			15			On					On			вкл					вкл
98		32			15			Relay 1 on/off				Relay1 on/off		реле1вкл/выкл				реле1вкл/выкл
99		32			15			Relay 2 on/off				Relay2 on/off		реле2вкл/выкл				реле2вкл/выкл
100		32			15			AI 2					AI 2			AI 2					AI 2
101		32			15			Battery temperature sensor fault	T Sensor Fault		ТемпДатчНеиспр				ТемпДатчНеиспр
102		32			15			Low capacity				Low capacity		НизкЕмкость				НизкЕмкость
103		32			15			Existence State				Existence State		Наличие					Наличие
104		32			15			Existent				Existent		есть					есть
105		32			15			Not Existent				Not Existent		нет					нет
106		32			15			Battary Not Response			Not Response		НетОтвета				НетОтвета
107		32			15			Response				Response		Ответ					Ответ
108		32			15			No Response				No Response		НетОтвета				НетОтвета
109		32			15			Rated Capacity				Rated Capacity		НомЕмкость				НомЕмкость
110		32			15			Used by Batt Management			Batt Manage		УправлАБ				УправлАБ
111		32			15			SMBatTemp High Limit			SMBatTempHighLimit	SMБатВысТемпПорог			SMБатВысТемпПорог
112		32			15			SMBatTemp Low Limit			SMBatTempLowLimit	SMБатНизкТемпПорог			SMБатНизкТемпПорог
113		32			15			SMBatTemp enable			SMBatTemp enable	SMБатТемпДатч				SMБатТемпДатч
114		32			15			Battary No Response			Battary No Response	АБнетОтвета				АБнетОтвета
115		32			15			Battery Temp not used			Battery Temp not used	НетТемпДатч				НетТемпДатч
116		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		Battery Current Imbalance Alarm		BattCurrImbalan
