﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Phase A Voltage				R			Напряжение фазаА			R
2		32			15			Phase B Voltage				S			Напряжение фазаВ			S
3		32			15			Phase C Voltage				T			Напряжение фазаC			T
4		64			15			Line AB Voltage				R			Линейное напряж AB			R
5		64			15			Line BC Voltage				S			Линейное напряж BС			S
6		64			15			Line CA Voltage				T			Линейное напряж CA			T
7		32			15			Phase A Current				Phase A Curr		Ток фазаА				Ток ф.А
8		32			15			Phase B Current				Phase B Curr		Ток фазаВ				Ток ф.В
9		32			15			Phase C Current				Phase C Curr		Ток фазаС				Ток ф.С
10		32			15			AC Frequency				AC Frequency		Частота					Частота
11		32			15			Total Real Power			Total RealPower		Полная мощность				Полная мощн
12		64			15			Phase A Real Power			Real Power A		Полная мощность фА			Полн мощн фА
13		64			15			Phase B Real Power			Real Power B		Полная мощность фВ			Полн мощн фВ
14		64			15			Phase C Real Power			Real Power C		Полная мощность фС			Полн мощн фС
15		32			15			Total Reactive Power			Tot React Power		Реакт мощность				Реакт мощн
16		64			15			Phase A Reactive Power			React Power A		Реак мощность фА			Реак мощ А
17		64			15			Phase B Reactive Power			React Power B		Реак мощность фВ			Реак мощ В
18		64			15			Phase C Reactive Power			React Power C		Реак мощность фС			Реак мощ С
19		32			15			Total Apparent Power			Total App Power		Актив мощность				Актив мощн
20		32			15			Phase A Apparent Power			App Power A		Актив мощн фА				Акт мощн А
21		32			15			Phase B Apparent Power			App Power B		Актив мощн фВ				Акт мощн В
22		32			15			Phase C Apparent Power			App Power C		Актив мощн фС				Акт мощн С
23		32			15			Power Factor				Power Factor		Коэф мощности				Коэф мощн
24		32			15			Phase A Power Factor			Power Factor A		Коэф мощн фА				Коэф мощн А
25		32			15			Phase B Power Factor			Power Factor B		Коэф мощн фВ				Коэф мощн В
26		32			15			Phase C Power Factor			Power Factor C		Коэф мощн фС				Коэф мощн С
27		64			15			Phase A Current Crest Factor		Ia Crest Factor		Крест фактор Ia				Крест-фактор Ia
28		64			15			Phase B Current Crest Factor		Ib Crest Factor		Крест фактор Ib				Крест фактор Ib
29		64			15			Phase C Current Crest Factor		Ic Crest Factor		Крест фактор Ic				Крест фактор Ic
30		32			15			Phase A Current THD			Current THD A		THD ток фазаА				THD токА
31		32			15			Phase B Current THD			Current THD B		THD ток фазаВ				THD токВ
32		32			15			Phase C Current THD			Current THD C		THD ток фазаС				THD токС
33		64			15			Phase A Voltage THD			Voltage THD A		THD напряж фазаА			THD напрА
34		64			15			Phase B Voltage THD			Voltage THD B		THD напряж фазаВ			THD напрВ
35		64			15			Phase C Voltage THD			Voltage THD C		THD напряж фазаС			THD напрС
36		32			15			Total Real Energy			Tot Real Energy		Полная энергия				Полная энергия
37		32			15			Total Reactive Energy			Tot ReactEnergy		Реакт энергия				Реакт энергия
38		32			15			Total Apparent Energy			Tot App Energy		Актив энергия				Актив энергия
39		64			15			Ambient Temperature			Ambient Temp		Темп окр воздуха			Темп окруж
40		64			15			Nominal Line Voltage			Nominal L-Volt		Номин лин напряж			Номин лин напряж
41		64			15			Nominal Phase Voltage			Nominal PH-Volt		Номин фазное напр			Ном фазн напр
42		32			15			Nominal Frequency			Nomi Frequency		Ном частота				Ном частота
43		64			15			Mains Failure Alarm Limit 1		Mains Fail Alm1		Авария сети уровень 1			АварСетиУр1
44		64			15			Mains Failure Alarm Limit 2		Mains Fail Alm2		Авария сети уровень 2			АварСетиУр2
45		64			15			Voltage Alarm Threshold 1		Volt Alm Trld 1		АварНапряжПорог1			АварНапрУр1
46		64			15			Voltage Alarm Threshold 2		Volt Alm Trld 2		АварНапряжПорог2			АварНапрУр2
47		64			15			Frequency Alarm Threshold		Freq Alarm Trld		АварЧастотаПорог			АварЧастота
48		32			15			High Temperature Limit			High Temp Limit		ВысТемпЛимит				ВысТемпЛимит
49		32			15			Low Temperature Limit			Low Temp Limit		НизТемпЛимит				НизТемпЛимит
50		32			15			Supervision Fail			Supervision Fail	АварБлокУпр				АварБлокУпр
51		32			15			Line AB Over Voltage 1			L-AB Over Volt1		ABПеренапр1				АВПеренапр1
52		32			15			Line AB Over Voltage 2			L-AB Over Volt2		ABПеренапр2				ABПеренапр2
53		32			15			Line AB Under Voltage 1			L-AB UnderVolt1		ABНизкНапр1				ABНизкНапр1
54		32			15			Line AB Under Voltage 2			L-AB UnderVolt2		ABНизкНапр2				ABНизкНапр2
55		32			15			Line BC Over Voltage 1			L-BC Over Volt1		BCПеренапр1				BCПеренапр1
56		32			15			Line BC Over Voltage 2			L-BC Over Volt2		BCПеренапр2				BCПеренапр2
57		32			15			Line BC Under Voltage 1			L-BC UnderVolt1		BCНизкНапр1				BCНизкНапр1
58		32			15			Line BC Under Voltage 2			L-BC UnderVolt2		BCНизкНапр2				BCНизкНапр2
59		32			15			Line CA Over Voltage 1			L-CA Over Volt1		CAПеренапр1				CAПеренапр1
60		32			15			Line CA Over Voltage 2			L-CA Over Volt2		CAПеренапр2				CAПеренапр2
61		32			15			Line CA Under Voltage 1			L-CA UnderVolt1		CAНизкНапр1				CAНизкНапр
62		32			15			Line CA Under Voltage 2			L-CA UnderVolt2		CAНизкНапр2				CAНизкНапр2
63		32			15			Phase A Over Voltage 1			PH-A Over Volt1		фAПеренапр1				фAПеренапр1
64		32			15			Phase A Over Voltage 2			PH-A Over Volt2		фАПеренапр2				фАПеренапр2
65		32			15			Phase A Under Voltage 1			PH-A UnderVolt1		фАНизкНапр1				фАНизкНапр1
66		32			15			Phase A Under Voltage 2			PH-A UnderVolt2		фАНизкНапр2				фАНизкНапр2
67		32			15			Phase B Over Voltage 1			PH-B Over Volt1		фВПеренапр1				фВПеренапр1
68		32			15			Phase B Over Voltage 2			PH-B Over Volt2		фВПеренапр2				фВПеренапр2
69		32			15			Phase B Under Voltage 1			PH-B UnderVolt1		фВНизкНапр1				фВНизкНапр1
70		32			15			Phase B Under Voltage 2			PH-B UnderVolt2		фВНизкНапр2				фВНизкНапр2
71		32			15			Phase C Over Voltage 1			PH-C Over Volt1		фСПернапр1				фСПеренапр1
72		32			15			Phase C Over Voltage 2			PH-C Over Volt2		фCПеренапр2				фСПеренапр2
73		32			15			Phase C Under Voltage 1			PH-C UnderVolt1		фСНизкНапр1				фСНизкНапр1
74		32			15			Phase C Under Voltage 2			PH-C UnderVolt2		фСНизкНапр2				фСНизкНапр2
75		32			15			Mains Failure				Mains Failure		Потеря сети				Потеря сети
76		64			15			Severe Mains Failure			SevereMainsFail		СерьезнПотеряСети			СерьезнПотеряСети
77		32			15			High Frequency				High Frequency		ВысЧастота				ВысЧастота
78		32			15			Low Frequency				Low Frequency		НизкЧастота				НизкЧастота
79		32			15			High Temperature			High Temp		ВысокТемп				ВысокТемп
80		32			15			Low Temperature				Low Temperature		НизкТемп				НизкТемп
81		32			15			Rectifier AC				AC			АС Выпрямит				АС Выпрямит
82		32			15			Supervision Fail			Supervision Fail	АварБлокУпр				АварБлокУпр
83		32			15			No					No			Нет					Нет
84		32			15			Yes					Yes			Да					Да
85		64			15			Phase A Mains Failure Counter		A Mains Fail Cnt	фА СчетПотерьСети			фАСчетПотерьСети
86		64			15			Phase B Mains Failure Counter		B Mains Fail Cnt	фВСчетПотерьСети			фВСчетПотерьСети
87		64			15			Phase C Mains Failure Counter		C Mains Fail Cnt	фССчетПотерьСети			фССчетПотерьСети
88		64			15			Frequency Failure Counter		F Fail Cnt		СчетАварийпоЧастоте			СчетАварЧастоте
89		32			15			Reset Phase A Mains Fail Counter	Reset A FailCnt		фАСбросСчет				фАСбросСчет
90		32			15			Reset Phase B Mains Fail Counter	Reset B FailCnt		фВСбросСчет				фВСбросСчет
91		32			15			Reset Phase C Mains Fail Counter	Reset C FailCnt		фССбросСчет				фССбросСчет
92		64			15			Reset Frequency Counter			Reset F FailCnt		СбросСчетЧастот				СбросСчетЧастот
93		32			15			Current Alarm Threshold			Curr Alarm Limit	ТокACПорог				ТокACПорог
94		32			15			Phase A High Current			A High Current		фAПревТока				фАПревТока
95		32			15			Phase B High Current			B High Current		фВПревТока				фВПревТока
96		32			15			Phase C High Current			C High Current		фСПревТока				фСПревТока
97		32			15			Min Phase Voltage			Min Phase Volt		МинФазНапряж				МинФазНапряж
98		64			15			Max Phase Voltage			Max Phase Volt		МаксФазнНапряж¦				МаксФазнНапр
99		32			15			Raw Data 1				Raw Data 1		Raw Data1				Raw Data1
100		32			15			Raw Data 2				Raw Data 2		Raw Data2				Raw Data2
101		32			15			Raw Data 3				Raw Data 3		Raw Data3				Raw Data3
102		32			15			Ref voltage				Ref voltage		Ref voltage				Ref voltage
103		32			15			State					State			Статус					Статус
104		32			15			off					off			Выкл					выкл
105		32			15			on					on			вкл					вкл
106		32			15			High Phase Voltage			High Ph-Volt		ВысокНапряжФазы				ВысНапрФазы
107		32			15			Very High Phase Voltage			VHigh Ph-Volt		ОчВысНапрФазы				ОчВысНапрФазы
108		32			15			Low Phase Voltage			Low Ph-Volt		НизкНапрФазы				НизкНапрФазы
109		32			15			Very Low Phase Voltage			VLow Ph-Volt		ОчНизкНапрФазы				ОчНизкНапрФазы
110		64			15			All Rectifiers No Response		Rects No Resp		НетОтветаВыпрямит			НетОтветаВыпрям
