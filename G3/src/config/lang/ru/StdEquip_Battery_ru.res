﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Current				Batt Current		Ток Батареи				ТокБатареи
2		32			15			Battery Capacity(Ah)			Batt Cap(Ah)		Емкость(Ач)				Емкость(Aч)
3		32			15			Exceed Current Limit			Exceed Curr Lmt		ПревышенЛимитТока			ПревТокЛимит
4		32			15			Battery					Battery			Батарея					Батарея
5		32			15			Over Battery current			Over Current		ВысБатТок				ВысБатТок
6		32			15			Battery Capacity(%)			Batt Cap(%)		ЕМкость(%)				Емкость(%)
7		32			15			Battery Voltage				Batt Voltage		НапряжениеАБ				НапрАБ
8		32			15			Low capacity				Low capacity		НизкЕмкость				НизкЕмкость
9		32			15			Battery Route Failure			Fuse Failure		ОтклАвтоматАБ				ОтклАвтоматАБ
10		32			15			DC Distribution Seq Num			DC Distri Seq Num	ПоследНомер DCраспред			НомерDCраспред
11		32			15			Battery Over Voltage			Over Volt		ВысНапряжБатареи			ВысНапрАБ
12		32			15			Battery Under Voltage			Under Volt		НизкНапрБатареи				НизкНапрАБ
13		32			15			Battery Over Current			Over Curr		ВысТокБатареи				ВысТокАБ
14		32			15			Battery Fuse Failure			Fuse Failure		ОтклАвтоматАБ				ОтклАвтоматАБ
15		32			15			Battery Over Voltage			Over Volt		ВысНапряжБатареи			ВысНапрАБ
16		32			15			Battery Under Voltage			Under Volt		НизкНапрБатареи				НизкНапрАБ
17		32			15			Battery Over Current			Over Curr		ВысТокБатареи				ВысТокАБ
18		32			15			Battery					Battery			Батарея					Батарея
19		32			15			Batt Sensor Coeffi			Batt-Coeffi		КоэфБатДатчика				БатДатКоэф
20		32			15			Over Voltage Setpoint			Over Volt Point		ПорогВысНапр				ПорогВысНапр
21		32			15			Low Voltage Setpoint			Low Volt Point		ПорогНизкНапр				ПорогНизкНапр
22		32			15			Battery Not Response			Not Response		НетОтветаБатарея			НетОтветаБат
23		32			15			Response				Response		Ответ					Ответ
24		32			15			No Response				No Response		Нет ответа				Нет ответа
25		32			15			No Response				No Response		Нет ответа				Нет ответа
26		32			15			Shunt Full Current			Shunt Current		ТокШунта				ТокШунта
27		32			15			Shunt Full Voltage			Shunt Voltage		НапряжениеШунта				НапряжШунта
28		32			15			Used By Batt Management			Manage Enable		УправБатВкл				УправБатВкл
29		32			15			Yes					Yes			Да					да
30		32			15			No					No			Нет					Нет
31		32			15			On					On			вкл					вкл
32		32			15			Off					Off			выкл					выкл
33		32			15			State					State			Статус					Статус
44		32			15			Temp No. of Battery			Temp No. of Battery	ТемпNoБатареи				ТемпNoБатареи
87		32			15			No					No			нет					нет
91		32			15			Temp1					Temp1			Темп1					Темп1
92		32			15			Temp2					Temp2			Темп2					Темп2
93		32			15			Temp3					Temp3			Темп3					Темп3
94		32			15			Temp4					Temp4			Темп4					Темп4
95		32			15			Temp5					Temp5			Темп5					Темп5
96		32			15			Rated Capacity				Rated Capacity		Емкость					Емкость
97		32			15			Battery Temp				Battery Temp		ТемпБатареи				ТемпБатареи
98		32			15			Battery Temp Sensor			BattTempSensor		ДатчикТемпАБ				ДатчикТемпАБ
99		32			15			None					None			нет					нет
100		32			15			Temperature1				Temp1			Темп1					Темп1
101		32			15			Temperature2				Temp2			Темп2					Темп2
102		32			15			Temperature3				Temp3			Темп3					Темп3
103		32			15			Temperature4				Temp4			Темп4					Темп4
104		32			15			Temperature5				Temp5			Темп5					Темп5
105		32			15			Temperature6				Temp6			Темп6					Темп6
106		32			15			Temperature7				Temp7			Темп7					Темп7
107		32			15			Temperature8				Temp8			Темп8					Темп8
108		32			15			Temperature9				Temp9			Темп9					Темп9
109		32			15			Temperature10				Temp10			Темп10					Темп10
500	32			15			Current Break Size			Curr1 Brk Size				Current Break Size			Curr1 Brk Size			
501	32			15			Current High 1 Current Limit		Curr1 Hi1 Lmt		Current High 1 Current Limit		Curr1 Hi1 Lmt	
502	32			15			Current High 2 Current Limit		Curr1 Hi2 Lmt		Current High 2 Current Limit		Curr1 Hi2 Lmt	
503	32			15			Battery Current High 1 Curr		BattCurr Hi1Cur			Battery Current High 1 Curr		BattCurr Hi1Cur		
504	32			15			Battery Current High 2 Curr		BattCurr Hi2Cur			Battery Current High 2 Curr		BattCurr Hi2Cur		
505	32			15			Battery 1						Battery 1				Battery 1						Battery 1			
506	32			15			Battery 2							Battery 2			Battery 2							Battery 2		
