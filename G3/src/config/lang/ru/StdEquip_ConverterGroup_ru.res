﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Converter Group				Converter Grp		ГрупКонверт				ГрупКонверт
2		32			15			Total Current				Total Current		ОбщийТок				ОбщийТок
3		32			15			Voltage					Voltage			Напряж					Напряж
4		32			15			System Capacity Used			SysCap Used		МощСистемы				МощСистемы
5		32			15			Maximum Used Capacity			Max Used Cap		МаксМощн				МаксМощн
6		32			15			Minimum Used Capacity			Min Used Cap		МинМощн					МинМощн
7		32			15			Communicating Converters		Comm Conv Num		КоммунКонвНомер				КоммунКонвНомер
8		32			15			Valid Converters			Valid Converters	РабКонв					РабКонв
9		32			15			Converter Number			Conv Number		НомерКонв				НомерКонв
10		32			15			Converter AC Failure State		AC-fail State		НетСетиАС				НетСетиАС
11		32			15			Multi-Converters Failure Status		Multi-Conv Fail		НескКонвНеиспр				НескКонвНеиспр
12		32			15			Converter Current Limit			Current Limit		ТокЛимит				ТокЛимит
13		32			15			Converters Trim				Conv Trim		КонвЛимит				КонвЛимит
14		32			15			DC On/Off Control			DC On/Off Ctrl		DC Вкл/Выкл				DC Вкл/Выкл
15		32			15			AC On/Off Control			AC On/Off Ctrl		АC Вкл/Выкл				АC Вкл/Выкл
16		32			15			Converter LEDs Control			LEDs Control		Светодиод				Светодиод
17		32			15			Fan Speed Control			Fan Speed Ctrl		Вентилятор				Вентилятор
18		32			15			Rated Voltage				Rated Voltage		НомНапряж				НомНапряж
19		32			15			Rated Current				Rated Current		НомТок					НомТок
20		32			15			High Voltage Limit			Hi-Volt Limit		ВысНапрОгранич				ВысНапрОгранич
21		32			15			Low Voltage Limit			Lo-Volt Limit		НизкНапрОгранич				НизкНапрОгранич
22		32			15			High Temperature Limit			Hi-Temp Limit		ВысТемпОгран				ВысТемпОгран
24		64			15			Restart Time on Over Voltage		OverVRestartT		ВремяСтартаПриВысНапр			ВремяСтартаПриВысНапр
25		64			15			Walk-in Time				Walk-in Time		ВремяМягкогоСтарта			ВремяМягкогоСтарта
26		32			15			Walk-in Enabled				Walk-in Enabled		МягкСтартАктив				МягкСтартАктив
27		32			15			Min Redundancy				Min Redundancy		МинРезерв				МинРезерв
28		32			15			Max Redundancy				Max Redundancy		МаксРезерв				МаксРезерв
29		32			15			Switch Off Delay			SwitchOff Delay		ЗадержкаВыкл				ЗадержкаВыкл
30		32			15			Cycle Period				Cyc Period		Цикл					Цикл
31		32			15			Cycle Activation Time			Cyc Act Time		ЦиклВремя				ЦиклВремя
32		32			15			Converter AC Failure			Conv AC Fail		НетAC					НетАС
33		32			15			Multi-Converters Failure		Multi-conv Fail		НескКонвНеиспр				НескКонвНеиспр
36		32			15			Normal					Normal			Норма					Норма
37		32			15			Failure					Failure			Неиспр					Неиспр
38		32			15			Switch Off All				Switch Off All		ВыклВсе					ВыклВсе
39		32			15			Switch On All				Switch On All		ВклВсе					ВклВсе
42		32			15			All Twinkle				All Twinkle		ВсеМигают				ВсеМигают
43		32			15			Stop Twinkle				Stop Twinkle		СтопМигание				СтопМигание
44		32			15			Full Speed				Full Speed		ПолнаяСкорость				ПолнаяСкорость
45		32			15			Automatic Speed				Auto Speed		СкоростьАвто				СкоростьАвто
46		32			32			Current Limit Control			Curr-Limit Ctl		ТокЛимит				ТокЛимит
47		32			32			Full Capability Control			Full-cap Ctl		ПолныйКонтр				ПолныйКонтр
54		32			15			Disabled				Disabled		Выкл					Выкл
55		32			15			Enabled					Enabled			Вкл					Вкл
68		32			15			Standby Enabled				Standby Enabled		StandbyВкл				StandbyВкл
72		32			15			Turn-on when AC Over-voltage		Turn-on AcOverV		ВыклПриВысНапр				ВыклПриВысНапр
73		32			15			No					No			Выкл					Выкл
74		32			15			Yes					Yes			Вкл					Вкл
77		64			15			Pre-CurrLmt On Switch-on Enb		Pre-CurrLimit		ТокЛимитпередОткл			ОгранТока
78		32			15			Converter Power type			Conv Power type		ТипКонв					ТипКонв
79		32			15			Double Supply				Double Supply		ДваВхода				ДваВхода
80		32			15			Single Supply				Single Supply		ОдинВход				ОдинВход
81		32			15			Last Converters Quantity		Last Conv Qty		КолвоКонв				КолвоКонв
82		32			15			Converter Lost				Converter Lost		ПотеряКонв				ПотеряКонв
83		32			15			Converter Lost				Converter Lost		ПотеряКонв				ПотеряКонв
84		64			15			Clear Converter Lost Alarm		Clear Conv Lost		СбросАвПотеряКонв			СбросАвПотеряКонв
85		32			15			Clear					Clear			Сброс					Сброс
86		32			15			Confirm Converters Position		Confirm Pos		ПозицКонв				ПозицКонв
87		32			15			Confirm					Confirm			Подтвердить				Подтвердить
88		64			15			Best Operating Point			Best Point		ЛучшаяТочкаРаботы			ЛучшаяРабота
89		32			15			Converter Redundancy			Conv Redundancy		КонвРезерв				КонвРезерв
90		64			15			Load Fluctuation Range			Fluct Range		КолебаниеНагрузки			КолебНагр
91		64			15			System Energy Saving Point		EngySave Point		ТочкаСохрЭнергии			ТочкаСохрЭнергии
92		64			15			E-Stop Function				E-Stop Function		E-Stop функция				E-Stop функции
93		32			15			AC phases				AC phases		AC фазы					AC фазы
94		32			15			Single phase				Single phase		Одна фаза				Одна фаза
95		32			15			Three phases				Three phases		Три фазы				Три фазы
96		32			15			Input current limit			InputCurrLimit		ВходТокЛимит				ВходТокЛимит
97		32			15			Double Supply				Double Supply		ДваВхода				ДваВхода
98		32			15			Single Supply				Single Supply		ОдинВход				ОдинВход
99		32			15			Small Supply				Small Supply		SmallПитание				SmallПитание
100		64			15			Restart on Over Voltage Enabled		DCOverVRestart		ПерезагрузПриВысНапр			ПерезагрузПриВысНапр
101		32			15			Sequence Start Interval			Start Interval		ИнтервалЗапуска				ИнтервалЗапуска
102		32			15			Rated Voltage				Rated Voltage		НомНапряжение				НомНапряж
103		32			15			Rated Current				Rated Current		НомТок					НомТок
104		32			15			All Converters Comm Fail		AllConvCommFail		НетОтветаВсеВыпр			НетОтветаВсеВыпр
105		32			15			Inactive				Inactive		Выкл					Выкл
106		32			15			Active					Active			Вкл					Вкл
107		32			15			Converter Redundancy Active		Redund Active		КонвРезервАктив				КонвРезервАктив
108		32			15			Existence State				Existence State		Статус					Статус
109		32			15			Existent				Existent		Есть					Есть
110		32			15			Not Existent				Not Existent		нет					нет
111		32			15			Average Current				Average Current		Средний ток				Средний ток
112		32			15			Default Current Limit			Current Limit		ОгранТока				ОгранТока
113		32			15			Default Output Voltage			Output Voltage		ВыхНапряжПоумолч			ВыхНапряж
114		32			15			Under Voltage				Under Voltage		НизкНапряж				НизкНапряж
115		32			15			Over Voltage				Over Voltage		ВысНапряж				ВысНапряж
116		32			15			Over Current				Over Current		Перегрузка				Перегрузка
117		32			15			Average Voltage				Average Voltage		Среднее напр				Среднее напр
118		32			15			HVSD Limit				HVSD Limit		HVSDограничение				HVSDогранич
119		64			15			Default HVSD Pst			Default HVSD Pst	Порог HVSD по умолч			Порог HVSD по умолч
120		64			15			Clear Converter Comm Fail		ClrConvCommFail		СбросАварииНетСвязи			СбросАварНетСвязи
121		64			15			HVSD					HVSD			Откл по выс выходному напряж		ОтклВысВыхНапр
135		64			15			Current Limit Point			Curr Limit Pt		Точка оганичения тока			ТочкаОгранТока
136		64			15			Current Limit				Current Limit		Огранич выходного тока			ОгранВыхТока
137		64			15			Maximum Current Limit Value		Max Curr Limit		Макс значение выход тока		МаксВыхТок
138		64			15			Default Current Limit Point		Def Curr Lmt Pt		Заводская точка огранич тока		ЗаводТочкаОгран
139		64			15			Minimize Current Limit Value		Min Curr Limit		Миним значение выход тока		МинВыхТок
290		32			15			Over Current				Over Current		Перегрузка				Перегрузка
291		32			15			Over Voltage				Over Voltage		Выс напряж				Выс напряж
292		64			15			Under Voltage				Under Voltage		Низкое напряжение			НизНапряжение
293		64			15			Clear All Converters Comm Fail		ClrAllConvCommF		Сброс авар потери связи кон-ов		СбросПотерСвязи
294		64			15			All Conv Comm Status			All Conv Status		Статус связи конверторо			СтатусСвязКонв
295		64			15			Converter Trim(24V)			Conv Trim(24V)		Напряж регулировки конверторов		НапряжРегулКонв
296		64			15			Last Conv Qty(Internal Use)		LastConvQty(P)		Кол-во конвертеров (внешние)		КолКонв(внешн)
297		64			15			Def Currt Lmt Pt(Inter Use)		DefCurrLmtPt(P)		Завод точ огранич тока (внешние)	ЗаводТочкаОгран
298		64			15			Converter Protect			Conv Protect		Защита конвертора			ЗащитаКонверт
299		64			15			Input Rated Voltage			Input RatedVolt		Входное напряжение			ВыхНапряжение
300		32			15			Total Rated Current			Total RatedCurr		Полный ток				Полный ток
301		32			15			Converter Type				Conv Type		Тип конвертора				ТипКонверт
302		64			15			24-48V Conv				24-48V Conv		Конвертор 24 - 48 В			Конвертор24-48
303		64			15			48-24V Conv				48-24V Conv		Конвертор 48 - 24 В			Конвертор48-24
304		64			15			400-48V Conv				400-48V Conv		Конвертор 400 - 48 В			Конвертор400-48
305		64			15			Total Output Power			Output Power		Выходная мощность			ВыхМощность
306		64			15			Reset Converter IDs			Reset Conv IDs		Cброс ID конвертера			СбрсIDКонв
307		64			15			Input Voltage			Input Voltage		Входное напряжение			Входно напряжен
