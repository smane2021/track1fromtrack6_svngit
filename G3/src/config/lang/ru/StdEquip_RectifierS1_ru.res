﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			GroupI Rectifier			GroupI Rectifier	ГрIВыпрямит				ГрIВыпрямит
2		32			15			DC Status				DC Status		СтатусDC				СтатусDC
3		32			15			DC Output Voltage			DC Voltage		Напряжение DC				Напряжение DC
4		32			15			Origin Current				Origin Current		Ток					Ток
5		32			15			Temperature				Temperature		Темп					Темп
6		32			15			Used Capacity				Used Capacity		Нагрузка				Нагрузка
7		32			15			AC Input Voltage			AC Voltage		НапрВходАС				НапрВходАС
8		32			15			DC Output Current			DC Current		ТокDC					ТокDC
9		32			15			Rectifier SN				Rectifier SN		Серийный номер				Серийный номер
10		32			15			Total Running Time			Running Time		Наработка				Наработка
11		32			15			No Response Count			NoResponseCount		СчетНетОтвета				СчетНетОтвета
12		32			15			Derated by AC				Derated by AC		СнижМощнпоАС				СнижМощнпоАС
13		32			15			Derated by Temp				Derated by Temp		СнижМощпоТемп				СнижМощпоТемп
14		32			15			Derated					Derated			СнижМощн				СнижМощн
15		32			15			Full Fan Speed				Full Fan Speed		ПолнСкорость				ПолнСкорость
16		32			15			WALK-In Function			Walk-in Func		МягкПуск				МягкПуск
17		32			15			AC On/Off				AC On/Off		АС Вкл/Выкл				АС Вкл/Выкл
18		32			15			Current Limitation			Curr Limit		ОграничТок				ОгранТок
19		32			15			Voltage High Limit			Volt Hi-limit		ВысНапрПорог				ВысНапр
20		32			15			AC Input Status				AC Status		СтатусАС				СтатусАС
21		32			15			Rect Temperature High			Rect Temp High		ВысТемпВыпр				ВысТемпВыпр
22		32			15			Rectifier Fault				Rect Fault		ВыпрНеиспр				ВыпрНеиспр
23		32			15			Rectifier Protected			Rect Protected		ЗащВыпрям				ЗащВыпрям
24		32			15			Fan Failure				Fan Failure		Вент Неиспр				Вент Неиспр
25		32			15			Current Limit State			Curr-lmt State		ОгрТокаСтатус				ОгрТокаСтатус
26		32			15			EEPROM Failure				EEPROM Failure		EEPROMНеиспр				EEPROMНеиспр
27		32			15			DC On/Off Control			DC On/Off Ctrl		DC Вкл/Выкл Контр			DC Вкл/Выкл Контр
28		32			15			AC On/Off Control			AC On/Off Ctrl		АС Вкл/Выкл Контр			АС Вкл/Выкл Контр
29		32			15			LED Control				LED Control		Светодиоды				Светодиоды
30		32			15			Rectifier Reset				Rect Reset		СбросВыпрям				СбросВыпрям
31		32			15			AC Input Failure			AC Failure		НетВходаАС				НетВходаАС
34		32			15			Over Voltage				Over Voltage		ВысНапряж				ВысНапряж
37		32			15			Current Limit				Current limit		ОгранТока				ОгранТока
39		32			15			Normal					Normal			Норма					Норма
40		32			15			Limited					Limited			Огран					Огран
45		32			15			Normal					Normal			Норма					Норма
46		32			15			Full					Full			Полн					Полн
47		32			15			Disabled				Disabled		Неакт					Неакт
48		32			15			Enabled					Enabled			Актив					Актив
49		32			15			On					On			Вкл					Вкл
50		32			15			Off					Off			Выкл					Выкл
51		32			15			Normal					Normal			Норма					Норма
52		32			15			Failure					Failure			Неиспр					Неиспр
53		32			15			Normal					Normal			Норма					Норма
54		32			15			Over temperature			Over-temp		ВысТемп					ВысТем
55		32			15			Normal					Normal			Норма					Норма
56		32			15			Fault					Fault			Неиспр					Неиспр
57		32			15			Normal					Normal			Норма					Норма
58		32			15			Protected				Protected		Защита					Защита
59		32			15			Normal					Normal			Норма					Норма
60		32			15			Failure					Failure			Неиспр					Неиспр
61		32			15			Normal					Normal			Норма					Норма
62		32			15			Alarm					Alarm			Авария					Авария
63		32			15			Normal					Normal			Норма					Норма
64		32			15			Failure					Failure			Неиспр					Неиспр
65		32			15			Off					Off			Выкл					Выкл
66		32			15			On					On			Вкл					Вкл
67		32			15			Off					Off			Выкл					Выкл
68		32			15			On					On			Вкл					Вкл
69		32			15			Flash					Flash			Моргает					Моргает
70		32			15			Cancel					Cancel			Отмена					Отмена
71		32			15			Off					Off			Выкл					Выкл
72		32			15			Reset					Reset			Сброс					Сброс
73		32			15			Open Rectifier				On			Вкл					Вкл
74		32			15			Close Rectifier				Off			Выкл					Выкл
75		32			15			Off					Off			Выкл					Выкл
76		32			15			LED Control				Flash			Моргает					Моргает
77		32			15			Rectifier Reset				Rect Reset		СбросВыпрям				СбросВыпрям
80		32			15			Comm Fail				Comm Fail		НетСвязи				НетСвязи
84		32			15			Rectifier High SN			Rect High SN		ВысСерНомер				ВысСерНомер
85		32			15			Rectifier Version			Rect Version		Версия					Версия
86		32			15			Rectifier Part Number			Rect Part NO.		ПродКод					ПродКод
87		32			15			Sharing Current State			Sharing Curr		РазделТоков				РазделТоков
88		32			15			Sharing Current Alarm			SharingCurr Alm		АварРаздТоков				АварРаздТоков
89		32			15			Over Voltage				Over Voltage		ВысНапряж				ВысНапряж
90		32			15			Normal					Normal			Норма					Норма
91		32			15			Over Voltage				Over Voltage		ВысНапряж				ВысНапряж
92		32			15			Line AB Voltage				Line AB Volt		Напряж AB				Напряж AB
93		32			15			Line BC Voltage				Line BC Volt		Напряж BC				Напряж BC
94		32			15			Line CA Voltage				Line CA Volt		Напряж CA				Напряж CA
95		32			15			Low Voltage				Low Voltage		НизкНапр				НизкНапр
96		32			15			AC Under Voltage Protection		U-Volt Protect		дё©ИЗащНизкНапрЗащита			ЗащНизкНапрЗащита
97		32			15			Rectifier Position			Rect Position		ПозВыпрям				ПозВыпрям
98		32			15			DC Output Shut Off			DC Output Off		дё©ИВыхDCВыкл				ВыхDCВыкл
99		32			15			Rectifier Phase				Rect Phase		Фаза					Фаза
100		32			15			A					A			A					A
101		32			15			B					B			B					B
102		32			15			C					C			C					C
103		32			15			Severe Sharing CurrAlarm		SevereSharCurr		КритАварРаздТоков			КритАварРаздТоков
104		32			15			Bar Code1				Bar Code1		Bar Code1				Bar Code1
105		32			15			Bar Code2				Bar Code2		Bar Code2				Bar Code2
106		32			15			Bar Code3				Bar Code3		Bar Code3				Bar Code3
107		32			15			Bar Code4				Bar Code4		Bar Code4				Bar Code4
108		32			15			Rect failure				Rect failure		НеиспрВыпр				НеиспрВыпр
109		32			15			No					No			Нет					Нет
110		32			15			yes					yes			да					да
111		32			15			Existence State				Existence ST		Наличие					Наличие
113		32			15			Comm OK					Comm OK			СвязьОК					СвязьОК
114		32			15			All No Response				All No Response		НетОтветаВсе				НетОтветаВсе
115		32			15			No Response				No Response		дё©Им¬яTжпTо				дё©Им¬яTжпTо
116		32			15			Rated current				Rated current		НомТок					НомТок
117		32			15			Rated efficiency			Rated efficiency	НомКПД					НомКПД
118		32			15			LT 93					LT 93			LT93					LT93
119		32			15			GT 93					GT 93			GT93					GT93
120		32			15			GT 95					GT 95			GT95					GT95
121		32			15			GT 96					GT 96			GT96					GT96
122		32			15			GT 97					GT 97			GT97					GT97
123		32			15			GT 98					GT 98			GT98					GT98
124		32			15			GT 99					GT 99			GT99					GT99
125		32			15			Redundancy related alarm		Redundancy alarm	АварРезервир				АварРезервир
126		32			15			Rect HVSD status			HVSD status		СостояниеHVSD				СостояниеHVSD
276		32			15			EStop/EShutdown				EStop/EShutdown		EStop/EShutdown				EStop/EShutdown
