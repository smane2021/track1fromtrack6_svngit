﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Fuse 1					Fuse 1			Пред1					Пред1
2		32			15			Fuse 2					Fuse 2			Пред2					Пред2
3		32			15			Fuse 3					Fuse 3			Пред3					Пред3
4		32			15			Fuse 4					Fuse 4			Пред4					Пред4
5		32			15			Fuse 5					Fuse 5			Пред5					Пред5
6		32			15			Fuse 6					Fuse 6			Пред6					Пред6
7		32			15			Fuse 7					Fuse 7			Пред7					Пред7
8		32			15			Fuse 8					Fuse 8			Пред8					Пред8
9		32			15			Fuse 9					Fuse 9			Пред9					Пред9
10		32			15			Aux Load Fuse				Aux Load Fuse		ДопПред					ДопПред
11		32			15			Fuse 1 alarm				Fuse 1 alarm		Пред1Авар				Пред1Авар
12		32			15			Fuse 2 alarm				Fuse 2 alarm		Пред2Авар				Пред2Авар
13		32			15			Fuse 3 alarm				Fuse 3 alarm		Пред3Авар				Пред3Авар
14		32			15			Fuse 4 alarm				Fuse 4 alarm		Пред4Авар				Пред4Авар
15		32			15			Fuse 5 alarm				Fuse 5 alarm		Пред5Авар				Пред5Авар
16		32			15			Fuse 6 alarm				Fuse 6 alarm		Пред6Авар				Пред6Авар
17		32			15			Fuse 7 alarm				Fuse 7 alarm		Пред7Авар				Пред7Авар
18		32			15			Fuse 8 alarm				Fuse 8 alarm		Пред8Авар				Пред8Авар
19		32			15			Fuse 9 alarm				Fuse 9 alarm		Пред9Авар				Пред9Авар
20		32			15			Aux Load alarm				AuxLoad alarm		ДопПредАвар				ДопПредАвар
21		32			15			On					On			вкл					вкл
22		32			15			Off					Off			выкл					выкл
23		32			15			DC Fuse Unit				DC Fuse Unit		DCблокПред				DCблокПред
24		32			15			Fuse 1 Voltage				Fuse 1 Voltage		Пред1Напр				Пред1Напр
25		32			15			Fuse 2 Voltage				Fuse 2 Voltage		Пред2 Напр				Пред2 Напр
26		32			15			Fuse 3 Voltage				Fuse 3 Voltage		Пред3 Напр				Пред3 Напр
27		32			15			Fuse 4 Voltage				Fuse 4 Voltage		Пред4Напр				Пред4 Напр
28		32			15			Fuse 5 Voltage				Fuse 5 Voltage		Пред5 Напр				Пред5 Напр
29		32			15			Fuse 6 Voltage				Fuse 6 Voltage		Пред6 Напр				Пред6 Напр
30		32			15			Fuse 7 Voltage				Fuse 7 Voltage		Пред7 Напр				Пред7 Напр
31		32			15			Fuse 8 Voltage				Fuse 8 Voltage		Пред8 Напр				Пред8 Напр
32		32			15			Fuse 9 Voltage				Fuse 9 Voltage		Пред9 Напр				Пред9 Напр
33		32			15			Fuse 10 Voltage				Fuse 10 Voltage		Пред10 Напр				Пред10 Напр
34		32			15			Fuse 10					Fuse 10			Пред10					Пред10
35		32			15			Fuse 10 alarm				Fuse 10 alarm		Пред10Авар				Пред10Авар
36		32			15			State					State			статус					статус
37		32			15			Failure					Failure			Неиспр					Неиспр
38		32			15			No					No			нет					нет
39		32			15			Yes					Yes			да					да
40		32			15			Fuse 11					Fuse 11			Предохранитель 11			Предохр11
41		32			15			Fuse 12					Fuse 12			Предохранитель 12			Предохр12
42		32			15			Fuse 11 Alarm				Fuse 11 Alarm		Предохранитель 11 Авари			Предохр11Авария
43		32			15			Fuse 12 Alarm				Fuse 12 Alarm		Предохранитель 12 Авари			Предохр12Авария
