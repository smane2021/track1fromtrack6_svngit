﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Current				Batt Current		ТокАБ					ТокАБ
2		32			15			Battery Capacity(Ah)			Batt Cap(Ah)		Емкость(Ач)				Емкость(Ач)
3		32			15			Exceed Current Limit			Exceed Curr Lmt		ВысТокПорог				ВысТокПорог
4		32			15			Battery					Battery			Батарея					Батарея
5		32			15			Over Battery current			Over Current		ВысТок					ВысТок
6		32			15			Battery Capacity(%)			Batt Cap(%)		Емкость(%)				Емкость(%)
7		32			15			Battery Voltage				Batt Voltage		НапрАБ¦					НапрАБ
8		32			15			Low capacity				Low capacity		НизкЕмкость				НизкЕмкость
9		32			15			Battery Route Failure			Fuse Failure		БатАвтоматОткл				БатОткл
10		32			15			DC Distribution Seq Num			DC Distri Seq Num	DC Distribution Seq Num			DC Distri Seq Num
11		32			15			Battery Over Voltage			Over Volt		ПеренапряжАБ				ПеренапрАБ
12		32			15			Battery Under Voltage			Under Volt		НизкНапряжАБ				НизкНапрАБ
13		32			15			Battery Over Current			Over Curr		ВысТокАБ				ВысТокАБ
14		32			15			Battery Fuse Failure			Fuse Failure		АтоматАБоткл				БатОткл
15		32			15			Battery Over Voltage			Over Volt		ВысНапряАБ				ВысНапрАБ
16		32			15			Battery Under Voltage			Under Volt		НизкНапрАБ				НизкНапрАБ
17		32			15			Battery Over Current			Over Curr		ВысТокАБ				ВысТокАБ
18		32			15			LargeDU Battery				Ldu Battery		LargeDUАБ				LargeDUАБ
19		32			15			Batt Sensor Coeffi			Batt-Coeffi		КоэфДатчАБ				КоэфДатчАБ
20		32			15			Over Voltage Setpoint			Over Volt Point		ВысНапрПорог				ВысНапрПорог
21		32			15			Low Voltage Setpoint			Low Volt Point		НизкНапрПорог				НизкНапрПорог
22		32			15			Battary Not Response			Not Response		НетОтветаАБ				НетОтветаАБ
23		32			15			Response				Response		Ответ					Ответ
24		32			15			No Response				No Response		НетОтвета				НетОтвета
25		32			15			No Response				No Response		НетОтвета				НетОтвета
26		32			15			Existence State				Existence State		Наличие					Наличие
27		32			15			Existent				Existent		Есть					ЕСть
28		32			15			Not Existent				Not Existent		Нет					Нет
29		32			15			Rated Capacity				Rated Capacity		Емкость					Емкость
30		32			15			Used by Batt Management			Batt Manage		УправлениеАБ				УправлАБ
31		32			15			Yes					Yes			Да					Да
32		32			15			No					No			Нет					Нет
97		32			15			Battery Temp				Battery Temp		ТемпАБ					ТемпАБ
98		32			15			Battery Temp Sensor			BattTempSensor		ТемпДатчАБ				ТемпДатчАБ
99		32			15			None					None			нет					нет
100		32			15			Temperature1				Temp1			Темп1					Темп1
101		32			15			Temperature2				Temp2			Темп2					Темп2
102		32			15			Temperature3				Temp3			Темп3					Темп3
103		32			15			Temperature4				Temp4			Темп4					Темп4
104		32			15			Temperature5				Temp5			Темп5					Темп5
105		32			15			Temperature6				Temp6			Темп6					Темп6
106		32			15			Temperature7				Temp7			Темп7					Темп7
107		32			15			Temperature8				Temp8			Темп8					Темп8
108		32			15			Temperature9				Temp9			Темп9					Темп9
109		32			15			Temperature10				Temp10			Темп10					Темп10
110		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		Battery Current Imbalance Alarm		BattCurrImbalan
