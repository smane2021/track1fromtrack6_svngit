﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			BMS				BMS		BMS				BMS
2	32			15			Rated Capacity				Rated Capacity		номинальная мощность				номина мощно
3	32			15			Communication Fail			Comm Fail		Связь Сбой			Связь Сбой
4	32			15			Existence State				Existence State		Существование государство		Сущест госуд
5	32			15			Existent				Existent		существующий				существующий
6	32			15			Not Existent				Not Existent		не Существующее				не Существующее
7	32			15			Battery Voltage				Batt Voltage		Напряжение батареи				Напряж батареи
8	32			15			Battery Current				Batt Current		ток батареи				ток батареи
9	32			15			Battery Rating (Ah)			Batt Rating(Ah)		Батарея Рейтинг (Ah)			Бат Рейтинг(Ah)
10	32			15			Battery Capacity (%)			Batt Cap (%)		Емкость батареи (%)			Емкость бат(%)
11	32			15			Bus Voltage				Bus Voltage		Автобус Напряжение			Автобус Напр
12	32			15			Battery Center Temperature(AVE)		Batt Temp(AVE)		Темп батареи Центр(AVE)		Темпер бат(AVE)
13	32			15			Ambient Temperature			Ambient Temp		Температура окружающей		Темп окруж
29	32			15			Yes					Yes			да					да
30	32			15			No					No			нет					нет
31	32			15			Single Over Voltage Alarm		SingleOverVolt		Женат Будил Более Напр		ЖенатБолееНапр
32	32			15			Single Under Voltage Alarm		SingleUnderVolt		Женат Будил под напр		Женатподнапр
33	32			15			Whole Over Voltage Alarm		WholeOverVolt		все Будил Более Напр		всеБолееНапр
34	32			15			Whole Under Voltage Alarm		WholeUnderVolt		все Будил под напр		всеподнапр
35	32			15			Charge Over Current Alarm		ChargeOverCurr		Заряд Более ток Будильник		ЗарядБолееток
36	32			15			Discharge Over Current Alarm		DisCharOverCurr		разряд Более ток Будильник		разрядБолееток
37	32			15			High Battery Temperature Alarm		HighBattTemp		высокая Темп Буд батареи		высокаяТемпбат
38	32			15			Low Battery Temperature Alarm		LowBattTemp		низкий Темп Буд батареи		низкийТемпбат
39	32			15			High Ambient Temperature Alarm		HighAmbientTemp		высокая Темп Буд окружающий		высокаяТемпокр
40	32			15			Low Ambient Temperature Alarm		LowAmbientTemp		низкий Темп Буд окружающий		низкийТемпокр
41	32			15			High PCB Temperature Alarm		HighPCBTemp		высокая Темп Буд PCB		высокаяТемпPCB
42	32			15			Low Battery Capacity Alarm		LowBattCapacity		низкий мощность Буд батареи		низкиймощнбат
43	32			15			High Voltage Difference Alarm		HighVoltDiff		Высокое напр разница Будильник		Высокнапрраз
44	32			15			Single Over Voltage Protect		SingOverVProt		Женат Более Напрщизата		ЖенатБолНапрзащ
45	32			15			Single Under Voltage Protect		SingUnderVProt		Женат под Напрзащита		ЖенатподНапрзащ
46	32			15			Whole Over Voltage Protect		WholeOverVProt		все Более Напрзащита		всеБолееНапрзащ
47	32			15			Whole Under Voltage Protect		WholeUnderVProt		все под напрзащита		всеподнапрзащ
48	32			15			Short Circuit Protect			ShortCircProt		защита корот замык		защ корот замык
49	32			15			Over Current Protect			OverCurrProt		Более ток защита			Более ток защи
50	32			15			Charge High Temperature Protect		CharHiTempProt		Зарядка выс темп защита	Завыс темп защи
51	32			15			Charge Low Temperature Protect		CharLoTempProt		Зарядка низк темп защита	Занизктемпзащи
52	32			15			Discharge High Temp Protect		DischHiTempProt		разряд выс темп защита	развыс темп защи
53	32			15			Discharge Low Temp Protect		DischLoTempProt		разряд низк темп защита	разнизктемпзащи
54	32			15			Front End Sample Comm Fault		SampCommFault		Пере кон При Связь Сбой		При Связь Сбой
55	32			15			Temp Sensor Disconnect Fault		TempSensDiscFlt		Погода датчика Отк Диаг		ПогдатОткДиаг
56	32			15			Charging				Charging		зарядка				зарядка
57	32			15			Discharging				Discharging		разрядка				разрядка
58	32			15			Charging MOS Breakover			CharMOSBrkover		Зарядка MOS опрокидыв			Зар MOS опрокид
59	32			15			Discharging MOS Breakover		DischMOSBrkover		разрядка MOS опрокидыв			раз MOS опрокид
60	32			15			Communication Address			CommAddress		Связь Адрес			Связь Адрес
61	32			15			Current Limit Activation		CurrLmtAct		Огра тока акт		Огра тока акт



