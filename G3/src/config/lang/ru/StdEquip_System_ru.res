﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			16			Power System				Power System		Система					Система
2		32			15			System Voltage				System Voltage		Напряжение				Напряжение
3		32			15			System Load				System Load		Нагрузка				Нагрузка
4		32			15			System Power				System Power		Мощность				Мощность
5		32			15			Total Power Consumption			Power Consump		Потребление				Потребление
6		32			15			Power Peak In 24 Hours			Power Peak		24часПик¦				24часПик¦
7		32			15			Average Power in 24 Hours		Avg power		Среднее24ч				Среднее24ч
8		32			15			Hardware Write-protect Switch		Hardware Switch		HWЗащитаЗапись				HWЗащитаЗапись
9		32			15			Ambient Temperature			Ambient Temp		ОкрТемп					ОкрТемп
18		32			15			Relay Output 1				Relay Output 1		Реле1					Реле1
19		32			15			Relay Output 2				Relay Output 2		Реле2					Реле2
20		32			15			Relay Output 3				Relay Output 3		Реле3					Реле3
21		32			15			Relay Output 4				Relay Output 4		Реле4					Реле4
22		32			15			Relay Output 5				Relay Output 5		Реле5					Реле5
23		32			15			Relay Output 6				Relay Output 6		Реле6					Реле6
24		32			15			Relay Output 7				Relay Output 7		Реле7					Реле7
25		32			15			Relay Output 8				Relay Output 8		Реле8					Реле8
27		32			15			Under-voltage 1 Level			Low Voltage		НизкНапряжУр1				НизкНапряжУр1
28		32			15			Under-voltage 2 Level			Under Voltage		НизкНапряжУр1				НизкНапряжУр1
29		32			15			Over-voltage 1 Level			Over Voltage 1		ВысНапр1Ур1				ВысНапр1Ур1
31		32			15			Temperature 1 High 1			Temp 1 High 1		Темп1Выс				Темп1Выс
32		32			15			Temperature 1 Low 1			Temp 1 Low 1		Темп1Низк				Темп1Низк
33		32			15			Auto/Mannual State			Auto/Man State		Авто/Ручн				Авто/Ручн
34		32			15			Outgoing Alarm Blocked			Alarm Blocked		БлокирВыхАвар				БлокирВыхАвар
35		32			15			Supervision Unit Fault			SelfDetect Fail		БлокУпрНеиспр				БлокУпрНеиспр
36		32			15			CAN communication failure		CAN Comm Fail		CAN Неиспр				CAN Неиспр
37		32			15			Mains Failure				Mains Failure		НетСети					НетСети
38		32			15			Under-voltage				Under Vol		НизкНапряж				НизкНапряж
39		32			15			Very Under-voltage			Very Under Vol		ОчНизкНапр				ОчНизкНапр
40		32			15			Over-voltage				Over Voltage		ВысНапряж				ВысНапряж
41		32			15			High Temperature1			High Temp1		Темп1Выс				Темп1Выс
42		32			15			Low Temperature1			Low Temp1		Темп1Низк				Темп1Низк
43		32			15			Temperature1 Sensor Fault		T1 Sensor Fault		Темп1НеиспрДат				Темп1НеиспрДат
44		32			15			Outgoing Alarms Blocked			Alarm Blocked		БлокВыхАвар				БлокВыхАвар
45		32			15			Maintenance Time Limit Alarm		MaintenanceAlrm		ВремяТехОбслуживание			ВремяТехОбслуживание
46		32			15			Unprotected				Unprotected		БезЗащиты				БезЗащиты
47		32			15			Protected				Protected		защита					защита
48		32			15			Normal					Normal			Норма					Норма
49		32			15			Fault					Fault			Неиспр					Неиспр
50		32			15			Off					Off			Выкл					Выкл
51		32			15			On					On			Вкл					Вкл
52		32			15			Off					Off			Выкл					Выкл
53		32			15			On					On			Вкл					Вкл
54		32			15			Off					Off			Выкл					Выкл
55		32			15			On					On			Вкл					Вкл
56		32			15			Off					Off			Выкл					Выкл
57		32			15			On					On			Вкл					Вкл
58		32			15			Off					Off			Выкл					Выкл
59		32			15			On					On			Вкл					Вкл
60		32			15			Off					Off			Выкл					Выкл
61		32			15			On					On			Вкл					Вкл
62		32			15			Off					Off			Выкл					Выкл
63		32			15			On					On			Вкл					Вкл
64		32			15			Open					Open			Откр					Откр
65		32			15			Closed					Closed			Закр					Закр
66		32			15			Open					Open			Откр					Откр
67		32			15			Closed					Closed			Закр					Закр
78		32			15			Off					Off			Выкл					Выкл
79		32			15			On					On			Вкл					Вкл
80		32			15			Auto					Auto			авто					авто
81		32			15			Manual					Manual			Ручное					Ручное
82		32			15			Normal					Normal			Норма					Норма
83		32			15			Blocked					Blocked			Блокир					Блокир
85		32			15			Set to Auto Mode			To Auto Mode		АвтоРежим				АвтоРежим
86		32			15			Set to Manual Mode			To Manual Mode		РучнРежим				РучнРежим
87		32			15			Power rate level			PowerRate level		УрМощн					УрМощн
88		32			15			Current power peak limit		Power limit		Огранич мощности			Огран мощности
89		32			15			Peak					Peak			Пик					Пик
90		32			15			High					High			Выс					Выс
91		32			15			Flat					Flat			Ровно					Ровно
92		32			15			Low					Low			Низк					Низк
93		32			15			Lower consumption enable		Lower Consump		НизкПотреблАктив			НизкПотреблАктив
94		32			15			Power Peak Shaving			P Peak Shaving		СрезПиков				СрезПиков
95		32			15			Disable					Disable			Выкл					Выкл
96		32			15			Enable					Enable			Вкл					Вкл
97		32			15			Disable					Disable			Выкл					Выкл
98		32			15			Enable					Enable			Вкл					Вкл
99		32			15			Over Maximum Power			Over Power		Перегруз				Перегруз
100		32			15			Normal					Normal			Норма					Норма
101		32			15			Alarm					Alarm			Авар					Авар
102		32			15			Over Maximum Power Alarm		Over Power		ПерегрузАвар				ПерегрузАвар
104		32			15			System Alarm Status			Alarm Status		СтатусАварСистемы			СтатусАварСистемы
105		32			15			No Alarm				NoAlarm			НетАварий				НетАварий
106		32			15			Observation Alarm			Observation		ИнформАв				ИнформАв
107		32			15			Major Alarm				Major			ВажнаяАвар				ВажнаяАвар
108		32			15			Critical Alarm				Critical		КритАвар				КритАвар
109		32			15			Maintenance Time Run			Mtn Time Run		Время работы				Время работы
110		32			15			Maintain Cycle Time			Mtn Cycle Time		ЦиклТехОбслуж				ПериодТехОбслуж
111		32			15			Maintenance Time Delay			Mtn Time Delay		ДопЗадержкаТехОбсл			ДопЗадержкаТехОбсл
112		32			15			Maintenance Time Out			Mtn Time Out		ВремяТехОбслуживание			ВремяТехОбслуживание
113		32			15			No					No			Нет					Нет
114		32			15			Yes					Yes			Да					Да
115		64			15			Power Split Mode			Power Split		Разделение мощности			Разделение мощности
116		64			15			Current Limit Set Value			Slave Curr Lmt		значение выход тока			значение выход тока
117		32			15			Delta Voltage				Delta volt		Разница напряж				Разница напряж
118		64			15			Proportional Coefficient		ProportionCoeff		Proportional Coefficient		ProportionCoeff
119		32			15			Integral Time				Integral Time		Время					Время
120		32			15			Master Mode				Master			Режим Master				Режим Master
121		32			15			Slave Mode				Slave			Режим Slave				Режим Slave
122		64			15			MPCL Control Step			MPCL Ctl step		MPCL Control Step			MPCL Ctl step
123		64			15			MPCL Control threshold			MPCL CtlHister		MPCL настройка порога			MPCL настройка порога
124		64			15			MPCL Battery Discharge Enabled		MPCL BatDisch		MPCL батарея разряженна			MPCL батарея разряженна
125		64			15			MPCL Diesel Control Enabled		MPCL DieselCtl		MPCLуправление дизелем включено		MPCLуправление дизелем включено
126		32			15			Disable					Disable			Выкл					Выкл
127		32			15			Enable					Enabled			Актив					Актив
128		32			15			Disable					Disable			Выкл					Выкл
129		32			15			Enable					Enabled			Вкл					Вкл
130		32			15			System Time				System Time		ТипСистемы				ТипСистемы
131		32			15			LCD Language				LCD Language		ЯзыкДисплей				ЯзыкДисплей
132		32			15			Alarm Voice				LCD Alarm Voice		ЗвукСигнал				ЗвукСигнал
133		32			15			English					English			Англ					Англ
134		32			15			Russian					Russian			Русский					Русский
135		32			15			On					On			Вкл					Вкл
136		32			15			Off					Off			Выкл					Выкл
137		32			15			3 min					3 Min			3мин					3мин
138		32			15			10 min					10 Min			10мин					10мин
139		32			15			1 hour					1 Hour			1час					1час
140		32			15			4 hour					4 Hour			4час					4час
141		32			15			Clear Maintenance Run Time		Clr run time		СбросВремяТО				СбросВремяТО
142		32			15			No					No			Нет					Нет
143		32			15			Yes					Yes			Да					Да
144		32			15			Alarm					Alarm			Авар					Авар
145		32			15			HW Status				HW Status		HW Сост					ACU+HW Сост
146		32			15			Normal					Normal			Норма					Норма
147		32			15			Config Error				Config Error		КонфОшибка				КонфОшибка
148		32			15			Flash Fault				Flash Fault		МорганиеОшибка				МорганиеОшибка
150		32			15			LCD Time Zone				Time Zone		ЧасПояс					ЧасПояс
151		32			15			Time sync main time server		Time Sync Svr		ВремяСинхрВремени			ВремяСинхрВремени
152		32			15			Time sync backup time server		Backup Sync Svr		ВремяСинхрВремениРез			ВремяСинхрВремениРез
153		32			15			Time sync interval			Sync Interval		ИнтервалСинхр				ИнтервалСинхр
155		32			15			Reset SCU				Reset SCU		СбросSCU				СбросSCU
156		32			15			Running Config Type			Config Type		ТипКонфигур				ТипКонфигур
157		32			15			Normal Config				Normal Config		НормКонф				НормКонф
158		32			15			Backup Config				Backup Config		ЗапаснаяКонф				ЗапаснаяКонф
159		32			15			Default Config				Default Config		Поумолчанию				Поумолчанию
160		32			15			Config Error(backup config)		Config Error 1		КонфОшибка1				КонфОшибка1
161		32			15			Config Error(default config)		Config Error 2		КонфОшибка2				КонфОшибка2
162		32			15			Control System alarm LED		Ctrl Sys Alarm		КонтрАварИндик				КонтрАварИндик
163		32			15			Imbalance System Current		Imbalance Curr		РазбалансТок				РазбалансТок
164		32			15			Normal					Normal			Норма					Норма
165		32			15			Abnormal				Abnormal		Анормал					Анормал
167		32			15			MainSwitch Block Condition		MS block cond		MainSwitch Block Condition		MS block cond
168		32			15			No					No			Нет					Нет
169		32			15			Yes					Yes			Да					Да
170		32			15			Total Run Time				Total Run Time		Наработка				Наработка
171		32			15			Total Alarm Number			Total Alm Num		КолвоАварий				КолвоАварий
172		32			15			Total OA Number				Total OA Num		КолвоИнформСигн				КолвоИнформСигн
173		32			15			Total MA Number				Total MA Num		КолвоВажнАвар				КолвоВажнАвар
174		32			15			Total CA Number				Total CA Num		КолвоКритАвар				КолвоКритАвар
175		32			15			SPD Fault				SPD Fault		SPD Неиспр				SPD Неиспр
176		32			15			Over-voltage 2 Level			Over voltage 2		ВысНапряж2				ВысНапряж2
177		32			15			Very Over-voltage			Very Over Volt		ОчВысНапряжАвар				ОчВысНапряжАвар
178		32			15			Regulation voltage enabled		Regu Volt Enab		РегулНапрВкл				РегулНапрВкл
179		32			15			Regulation voltage range		Regu Volt range		УровРегулир				УровРегулир
180		32			15			Keypad Voice				Keypad Voice		ЗвукКлавиш				ЗвукКлавиш
181		32			15			On					On			Вкл					Вкл
182		32			15			Off					Off			Выкл					Выкл
183		32			15			Load Shunt Full Current			Load Shunt Curr		ТокШунта				ТокШунта
184		32			15			Load Shunt Full Voltage			Load Shunt Volt		T¦тьШунтНапр				НапрШунта
185		32			15			Bat 1 Shunt Full Current		Bat1 Shunt Curr		Бат1ШунтТок				Бат1ШунтТок
186		32			15			Bat 1 Shunt Full Voltage		Bat1 Shunt Volt		Бат1ШунтНапр				Бат1ШунтНапр
187		32			15			Bat 2 Shunt Full Current		Bat2 Shunt Curr		Бат2ШунтТок				Бат2ШунтТок
188		32			15			Bat 2 Shunt Full Voltage		Bat2 Shunt Volt		Бат2ШунтНапр				Бат2ШунтНапр
219		32			15			Relay 6					Relay 6			Реле 6					Реле 6
220		32			15			Relay 7					Relay 7			Реле 7					Реле 7
221		32			15			Relay 8					Relay 8			Реле 8					Реле 8
222		32			15			RS232 Baudrate				RS232 Baudrate		RS232 Скорость				RS232 Скорость
223		32			15			Local Address				Local Addr		Local Address				Local Addr
224		32			15			Callback Num				Callback Num		Callback Num				Callback Num
225		32			15			Interval Time of Callback		Interval		Interval Time of Callback		Интервал
227		32			15			DCDataFlage(on-off)			DCDataFlage1		DCDataFlage(on-off)			DCDataFlage1
228		32			15			DCDataFlage(Alarm)			DCDataFlage2		DCDataFlage(Alarm)			DCDataFlage2
229		32			15			Relay Reporting				Relay Reporting		Отчёт по реле				ОтчётПоРеле
230		32			15			Fixed					Fixed			Фиксирован				Фиксирован
231		32			15			User Defined				User Defined		УстПользов				УстПользов
232		32			15			RectDataFlage(Alarm)			RectDataFlage2		RectDataFlage(Alarm)			RectDataFlage2
233		32			15			Master Controlled			Master CTRLR		MasterРежим				MasterРежим
234		32			15			Slave Controlled			Slave CTRLR		SlaveРежим				SlaveРежим
236		32			15			Over Load Alarm Point			Over Load Point		ПерегрузПорог				ПерегрПорог
237		32			15			Over Load				Over Load		Перегруз				Перегруз
238		32			15			SystemDataFlage(on-off)			SysDataFlage1		SystemDataFlage(on-off)			SysDataFlage1
239		32			15			SystemDataFlage(Alarm)			SysDataFlage2		SystemDataFlage(Alarm)			SysDataFlage2
240		32			15			Spanish Language			Spanish			Испанский				Испанский
241		32			15			Imbalance Protected Enable		Imb Protect		РазбалансЗащитаВкл			РазбалЗащитВкл
242		32			15			Enable					Enable			Вкл					Вкл
243		32			15			Disable					Disable			Выкл					Выкл
244		32			15			Buzzer control				Buzzer control		Сирена					Сирена
245		32			15			Normal					Normal			Норма					Норма
246		32			15			Disable					Disable			Выкл					Выкл
247		32			15			Ambient Temp Alarm			Temp Alarm		ОкрТемпАвар				ОкрТемпАвар
248		32			15			Normal					Normal			Норма					Норма
249		32			15			Low					Low			Низк					Низк
250		32			15			High					High			Выс					Выс
251		32			15			Reallocate Rect Address			Realloc Addr		Reallocate Rect Address			Realloc Addr
252		32			15			Normal					Normal			Норма					Норма
253		32			15			Realloc Addr				Realloc Addr		Realloc Addr				Realloc Addr
254		32			15			Test State Set				Test State Set		Test State Set				Test State Set
255		32			15			Normal					Normal			Норма					Норма
256		32			15			Test State				Test State		ТестСтатус				ТестСтатус
257		32			15			Minification for Test			Minification		Minification for Test			Minification
258		32			15			Digital Input 1				DI 1			ЦифрВх1					ЦифрВх1
259		32			15			Digital Input 2				DI 2			ЦифрВх2					ЦифрВх2
260		32			15			Digital Input 3				DI 3			ЦифрВх3					ЦифрВх3
261		32			15			Digital Input 4				DI 4			ЦифрВх4					ЦифрВх4
262		32			15			System Type Value			Sys Type Value		ТипСистемы				ТипСистемы
263		32			15			AC/DC Board Type			AC/DCBoardType		АС/DC ТипПлаты				АС/DC ТипПлаты
264		32			15			B14C3U1					B14C3U1			B14C3U1					B14C3U1
265		32			15			Large DU				Large DU		Large DU				Large DU
266		32			15			SPD					SPD			SPD					SPD
267		32			15			Temp1					Temp1			Темп1					Темп1
268		32			15			Temp2					Temp2			Темп2					Темп2
269		32			15			Temp3					Temp3			Темп3					Темп3
270		32			15			Temp4					Temp4			Темп4					Темп4
271		32			15			Temp5					Temp5			Темп5					Темп5
272		32			15			Auto Mode				Auto Mode		Авто					Авто
273		32			15			EMEA					EMEA			EMEA					EMEA
274		32			15			Normal					Normal			Норма					Норма
275		32			15			Nominal Voltage				Nom Voltage		Напряжение				Напряжение
276		64			15			EStop/EShutdown				EStop/EShutdown		E-стоп/E-отключение			E-стоп/E-отключение
277		32			15			Shunt1 Full Current			Shunt1 Current		Шунт1Ток				Шунт1Ток
278		32			15			Shunt2 Full Current			Shunt2 Current		Шунт2Ток				Шунт2Ток
279		32			15			Shunt3 Full Current			Shunt3 Current		Шунт3Ток				Шунт3Ток
280		32			15			Shunt1 Full Voltage			Shunt1 Voltage		Шунт1Напряж				Шунт1Напряж
281		32			15			Shunt2 Full Voltage			Shunt2 Voltage		Шунт2Напряж				Шунт2Напряж
282		32			15			Shunt3 Full Voltage			Shunt3 Voltage		Шунт3Напряж				Шунт3Напряж
283		32			15			Temperature1 enable			Temp1 enable		Темп1Вкл				Темп1Вкл
284		32			15			Temperature2 enable			Temp2 enable		Темп2Вкл				Темп2Вкл
285		32			15			Temperature3 enable			Temp3 enable		Темп3Вкл				Темп3Вкл
286		32			15			Temperature4 enable			Temp4 enable		Темп4Вкл				Темп4Вкл
287		32			15			Temperature5 enable			Temp5 enable		Темп5Вкл				Темп5Вкл
288		32			15			Very High Temperature1 Limit		VeryHighTemp1		Темп1Порог				ОчВысТемп1Порог
289		32			15			Very High Temperature2 Limit		VeryHighTemp2		Темп2Порог				ОчВысТемп2Порог
290		32			15			Very High Temperature3 Limit		VeryHighTemp3		Темп3Порог				ОчВысТемп3Порог
291		32			15			Very High Temperature4 Limit		VeryHighTemp4		Темп4Порог				ОчВысТемп4Порог
292		32			15			Very High Temperature5 Limit		VeryHighTemp5		Темп5Порог				ОчВысТемп5Порог
293		32			15			High Temperature2 Limit			High Temp2		Темп2Выс				Темп2ВысПорог
294		32			15			High Temperature3 Limit			High Temp3		Темп3Выс				Темп3ВысПорог
295		32			15			High Temperature4 Limit			High Temp4		Темп4Выс				Темп4ВысПорог
296		32			15			High Temperature5 Limit			High Temp5		Темп5Выс				Темп5ВысПорог
297		32			15			Low Temperature2 Limit			Low Temp2		Темп2Низк				Темп2НизкПорог
298		32			15			Low Temperature3 Limit			Low Temp3		Темп3Низк				Темп3НизкПорог
299		32			15			Low Temperature4 Limit			Low Temp4		Темп4Низк				Темп4НизкПорог
300		32			15			Low Temperature5 Limit			Low Temp5		Темп5Низк				Темп5НизкПорог
301		32			15			Temperature1 not used			Temp1 not used		Темп1Нет				Темп1Нет
302		32			15			Temperature2 not used			Temp2 not used		Темп2Нет				Темп2Нет
303		32			15			Temperature3 not used			Temp3 not used		Темп3Нет				Темп3Нет
304		32			15			Temperature4 not used			Temp4 not used		Темп4Нет				Темп4Нет
305		32			15			Temperature5 not used			Temp5 not used		Темп5Нет				Темп5Нет
306		32			15			High Temperature2			High Temp2		Темп2Выс				Темп2Выс
307		32			15			Low Temperature2			Low Temp2		Темп2Низк				Темп2Низк
308		32			15			Temperature2 Sensor Fault		T2 Sensor Fault		Темп2НеиспрДат				Темп2НеиспрДат
309		32			15			High Temperature3			High Temp3		Темп3Выс				Темп3Выс
310		32			15			Low Temperature3			Low Temp3		Темп3Низк				Темп3Низк
311		32			15			Temperature3 Sensor Fault		T3 Sensor Fault		Темп3НеиспрДат				Темп3НеиспрДат
312		32			15			High Temperature4			High Temp4		Темп4Выс				Темп4Выс
313		32			15			Low Temperature4			Low Temp4		Темп4Низк				Темп4Низк
314		32			15			Temperature4 Sensor Fault		T4 Sensor Fault		Темп4НеиспрДат				Темп4НеиспрДат
315		32			15			High Temperature5			High Temp5		Темп5Выс				Темп5Выс
316		32			15			Low Temperature5			Low Temp5		Темп5Низк				Темп5Низк
317		32			15			Temperature5 Sensor Fault		T5 Sensor Fault		Темп5НеиспрДат				Темп5НеиспрДат
318		32			15			Very High Temperature1			Very High Temp1		Темп1ОчВыснб				Темп1ОчВыс
319		32			15			Very High Temperature2			Very High Temp2		Темп2ОчВыснб				Темп2ОчВыс
320		32			15			Very High Temperature3			Very High Temp3		Темп3ОчВыснб				Темп3ОчВыс
321		32			15			Very High Temperature4			Very High Temp4		Темп4ОчВыснб				Темп4ОчВыс
322		32			15			Very High Temperature5			Very High Temp5		Темп5ОчВыснб				Темп5ОчВыс
323		32			15			Energy Saving Status			Energy Saving Status	ЭКОрежим				ЭКОрежим
324		32			15			Energy Saving Enable			Energy Saving Enable	ЭКОрежимВкл				ЭКОрежимВкл
325		32			15			Internal Use(I2C)			Internal Use		Internal Use (I2C)			Internal Use
326		32			15			Disable					Disable			Выкл					Выкл
327		32			15			EStop					EStop			EStop					EStop
328		32			15			EShutdown				EShutdown		EShutdown				EShutdown
329		32			15			Ambient					Ambient			Внешний					Внешний
330		32			15			Battery					Battery			Батарея					Батарея
331		32			15			Temp6					Temp6			Темп6					Темп6
332		32			15			Temp7					Temp7			Темп7					Темп7
333		32			15			Temperature6 enable			Temp6 enable		Темп6Вкл				Темп6Вкл
334		32			15			Temperature7 enable			Temp7 enable		Темп7Вкл				Темп7Вкл
335		32			15			Very High Temperature6 Limit		VeryHighTemp6		Темп6ОчВысПорог				Темп6ОчВысПорог
336		32			15			Very High Temperature7 Limit		VeryHighTemp7		Темп7ОчВысПорог				Темп7ОчВысПорог
337		32			15			High Temperature6 Limit			High Temp6		Темп6Выс				Темп6Выс
338		32			15			High Temperature7 Limit			High Temp7		Темп7Выс				Темп7Выс
339		32			15			Low Temperature6 Limit			Low Temp6		Темп6Низк				Темп6Низк
340		32			15			Low Temperature7 Limit			Low Temp7		Темп7Низк				Темп7Низк
341		32			15			Temperature6 not used			Temp6 not used		Темп6Нет				Темп6Нет
342		32			15			Temperature7 not used			Temp7 not used		Темп7Нет				Темп7Нет
343		32			15			High Temperature6			High Temp6		Темп6Выс				Темп6Выс
344		32			15			Low Temperature6			Low Temp6		Темп6Низк				Темп6Низк
345		32			15			Temperature6 Sensor Fault		T6 Sensor Fault		Темп6НеиспрДат				Темп6НеиспрДат
346		32			15			High Temperature7			High Temp7		Темп7Выс				Темп7Выс
347		32			15			Low Temperature7			Low Temp7		Темп7Низк				Темп7Низк
348		32			15			Temperature7 Sensor Fault		T7 Sensor Fault		Темп7НеиспрДат				Темп7НеиспрДат
349		32			15			Very High Temperature6			Very High Temp6		Темп6ОчВыс				Темп6ОчВыс
350		32			15			Very High Temperature7			Very High Temp7		Темп7ОчВыс				Темп7ОчВыс
501		32			15			Manual Mode				Manual Mode		Ручной режим				Ручной режим
1111		32			15			Temperature 1				Temperature 1		Темп1					Темп1
1131		32			15			Temperature 1 enable			Temp 1 enable		Темп1Вкл				Темп1Вкл
1132		32			15			BTemperature 1 High 2			BTemp 1 High 2		Темп1ПорогБат				Темп1ПорогБат
1133		32			15			BTemperature 1 High 1			BTemp 1 High 1		Темп1ВысБат				Темп1ВысБат
1134		32			15			BTemperature 1 Low			BTemp 1 Low		Темп1НизкБат				Темп1НизкБат
1141		32			15			Temperature 1 not used			Temp 1 not used		Темп1Нет				Темп1Нет
1142		32			15			Temperature 1 Sensor Fault		T1 Sensor Fault		Темп1НеиспрДат				Темп1НеиспрДат
1143		32			15			BTemperature 1 High 2			BTemp 1 High 2		Темп1ОчВыснбБат				Темп1ОчВысБат
1144		32			15			BTemperature 1 High 1			BTemp 1 High 1		Темп1ВысБат				Темп1ВысБат
1145		32			15			BTemperature 1 Low			BTemp 1 Low		Темп1НизкБат				Темп1НизкБат
1211		32			15			Temperature 2				Temperature 2		Темп2					Темп2
1231		32			15			Temperature 2 enable			Temp 2 enable		Темп2Вкл				Темп2Вкл
1232		32			15			BTemperature 2 High 2			BTemp 2 High 2		Темп2ПорогБат				Темп2ПорогБат
1233		32			15			BTemperature 2 High 1			BTemp 2 High 1		Темп2ВысБат				Темп2ВысБат
1234		32			15			BTemperature 2 Low			BTemp 2 Low		Темп2НизкБат				Темп2НизкБат
1241		32			15			Temperature 2 not used			Temp 2 not used		Темп2Нет				Темп2Нет
1242		32			15			Temperature 2 Sensor Fault		T2 Sensor Fault		Темп2НеиспрДат				Темп2НеиспрДат
1243		32			15			BTemperature 2 High 2			BTemp 2 High 2		Темп2ОчВысБат				Темп2ОчВысБат
1244		32			15			BTemperature 2 High 1			BTemp 2 High 1		Темп2ВысБат				Темп2ВысБат
1245		32			15			BTemperature 2 Low			BTemp 2 Low		Темп2НизкБат				Темп2НизкБат
1311		32			15			Temperature 3				Temperature 3		Темп3					Темп3
1331		32			15			Temperature 3 enable			Temp 3 enable		Темп3Вкл				Темп3Вкл
1332		32			15			BTemperature 3 High 2			BTemp 3 High 2		Темп3ПорогБат				Темп3ПорогБат
1333		32			15			BTemperature 3 High 1			BTemp 3 High 1		Темп3ВысБат				Темп3ВысБат
1334		32			15			BTemperature 3 Low			BTemp 3 Low		Темп3НизкБат				Темп3НизкБат
1341		32			15			Temperature 3 not used			Temp 3 not used		Темп3Нет				Темп3Нет
1342		32			15			Temperature 3 Sensor Fault		T3 Sensor Fault		Темп3НеиспрДат				Темп3НеиспрДат
1343		32			15			BTemperature 3 High 2			BTemp 3 High 2		Темп3ОчВысБат				Темп3ОчВысБат
1344		32			15			BTemperature 3 High 1			BTemp 3 High 1		Темп3ВысБат				Темп3ВысБат
1345		32			15			BTemperature 3 Low			BTemp 3 Low		Темп3НизкБат				Темп3НизкБат
1411		32			15			Temperature 4				Temperature 4		Темп4					Темп4
1431		32			15			Temperature 4 enable			Temp 4 enable		Темп4Вкл				Темп4Вкл
1432		32			15			BTemperature 4 High 2			BTemp 4 High 2		Темп4ПорогБат				Темп4ПорогБат
1433		32			15			BTemperature 4 High 1			BTemp 4 High 1		Темп4ВысБат				Темп4ВысБат
1434		32			15			BTemperature 4 Low			BTemp 4 Low		Темп4НизкБат				Темп4НизкБат
1441		32			15			Temperature 4 not used			Temp 4 not used		Темп4Нет				Темп4Нет
1442		32			15			Temperature 4 Sensor Fault		T4 Sensor Fault		Темп4НеиспрДат				Темп4НеиспрДат
1443		32			15			BTemperature 4 High 2			BTemp 4 High 2		Темп4ОчВысБат				Темп4ОчВысБат
1444		32			15			BTemperature 4 High 1			BTemp 4 High 1		Темп4ВысБат				Темп4ВысБат
1445		32			15			BTemperature 4 Low			BTemp 4 Low		Темп4НизкБат				Темп4НизкБат
1511		32			15			Temperature 5				Temperature 5		Темп5					Темп5
1531		32			15			Temperature 5 enable			Temp 5 enable		Темп5Вкл				Темп5Вкл
1532		32			15			BTemperature 5 High 2			BTemp 5 High 2		Темп5ПорогБат				Темп5ПорогБат
1533		32			15			BTemperature 5 High 1			BTemp 5 High 1		Темп5ВысБат				Темп5ВысБат
1534		32			15			BTemperature 5 Low			BTemp 5 Low		Темп5НизкБат				Темп5НизкБат
1541		32			15			Temperature 5 not used			Temp 5 not used		Темп5Нет				Темп5Нет
1542		32			15			Temperature 5 Sensor Fault		T5 Sensor Fault		Темп5НеиспрДат				Темп5НеиспрДат
1543		32			15			BTemperature 5 High 2			BTemp 5 High 2		Темп5ОчВысБат				Темп5ОчВысБат
1544		32			15			BTemperature 5 High 1			BTemp 5 High 1		Темп5ВысБат				Темп5ВысБат
1545		32			15			BTemperature 5 Low			BTemp 5 Low		Темп5НизкБат				Темп5НизкБат
1611		32			15			Temperature 6				Temperature 6		Темп6					Темп6
1631		32			15			Temperature 6 enable			Temp 6 enable		Темп6Вкл				Темп6Вкл
1632		32			15			BTemperature 6 High 2			BTemp 6 High 2		Темп6ПорогБат				Темп6ПорогБат
1633		32			15			BTemperature 6 High 1			BTemp 6 High 1		Темп6ВысБат				Темп6ВысБат
1634		32			15			BTemperature 6 Low			BTemp 6 Low		Темп6НизкБат				Темп6НизкБат
1641		32			15			Temperature 6 not used			Temp 6 not used		Темп6Нет				Темп6Нет
1642		32			15			Temperature 6 Sensor Fault		T6 Sensor Fault		Темп6НеиспрДат				Темп6НеиспрДат
1643		32			15			BTemperature 6 High 2			BTemp 6 High 2		Темп6ОчВысБат				Темп6ОчВысБат
1644		32			15			BTemperature 6 High 1			BTemp 6 High 1		Темп6ВысБат				Темп6ВысБат
1645		32			15			BTemperature 6 Low			BTemp 6 Low		Темп6НизкБат				Темп6НизкБат
1711		32			15			Temperature 7				Temperature 7		Темп7					Темп7
1731		32			15			Temperature 7 enable			Temp 7 enable		Темп7Вкл				Темп7Вкл
1732		32			15			BTemperature 7 High 2			BTemp 7 High 2		Темп7ПорогБат				Темп7ПорогБат
1733		32			15			BTemperature 7 High 1			BTemp 7 High 1		Темп7ВысБат				Темп7ВысБат
1734		32			15			BTemperature 7 Low			BTemp 7 Low		Темп7НизкБат				Темп7НизкБат
1741		32			15			Temperature 7 not used			Temp 7 not used		Темп7Нет				Темп7Нет
1742		32			15			Temperature 7 Sensor Fault		T7 Sensor Fault		Темп7НеиспрДат				Темп7НеиспрДат
1743		32			15			BTemperature 7 High 2			BTemp 7 High 2		Темп7ОчВысБат				Темп7ОчВысБат
1744		32			15			BTemperature 7 High 1			BTemp 7 High 1		Темп7ВысБат				Темп7ВысБат
1745		32			15			BTemperature 7 Low			BTemp 7 Low		Темп7НизкБат				Темп7НизкБат
1746		32			15			DHCP Failure				DHCP Failure		DHCPНеиспр				DHCPНеиспр
1747		32			15			Internal Use(CAN)			Internal Use		Internal Use(CAN)			Internal Use
1748		32			15			Internal Use(485)			Internal Use		Internal Use (485)			Internal Use
1749		32			15			Address(Slave)				Address(Slave)		Адрес(Slave)				Адрес(Slave)
1750		32			15			201					201			201					201
1751		32			15			202					202			202					202
1752		32			15			203					203			203					203
1753		32			15			Master/Slave Mode			Master/Slave Mode	Master/Slave Реж			Mast/Slv Режим
1754		32			15			Master					Master			Master					Master
1755		32			15			Slave					Slave			Slave					Slave
1756		32			15			Alone					Alone			Один					Один
1757		32			15			SMBatTemp High Limit			SMBatTempHighLimit	SMБатВысТемпПорог			SMБатВысТемпПор
1758		32			15			SMBatTemp Low Limit			SMBatTempLowLimit	SMБатНизкТемпПорог			SMБатНизкТемпПо
1759		32			15			PLC Config Error			PLC Config Error	PLCКонфОшибка				PLCКонфОшибка
1760		32			15			Rectifier Expansion			Rect Expansion		РасширВыпрямителей			РасширВыпря
1761		32			15			Inactive				Inactive		Неактив					Неактив
1762		32			15			Primary					Primary			Первично				Первично
1763		32			15			Secondary				Secondary		Вторично				Вторично
1764		128			64			Mode Changed,Auto_Config will be started!	To Auto Config!		Mode Changed,Auto_Config will be started!	To Auto Config!
1765		32			15			Before Lang				Before Lang		ПредыдЯзык				ПредыдЯзык
1766		32			15			Current Lang				Current Lang		ТекущийЯзык				ТекущийЯзык
1767		32			15			eng					eng			Англ					Англ
1768		32			15			loc					loc			Рус					Рус
1769		32			15			485 communication failure		485 comm failure	485НетСсвязи				485НетСвязи
1770		64			32			SMDU Sampler Mode Change		SMDUSampChange		SMDU Sampler Mode Change		SMDUSampChange
1771		32			15			Can					Can			Can					Can
1772		32			15			485					485			485					485
1773		64			15			To Auto Delay				To Auto Delay		Задержка перехода в авто		Задержка в авто
1774		32			15			OBSERVATION SUMMARY			OA SUMMARY		ОА СВОДКА				ОА СВОДКА
1775		32			15			MAJOR SUMMARY				MA SUMMARY		МА СВОДКА				МА СВОДКА
1776		32			15			CRITICAL SUMMARY			CA SUMMARY		СА СВОДКА				СА СВОДКА
1777		32			15			All Rectifiers Current			All Rect Current	ТокВсехВыпрям				ТокВсехВыпрям
1778		32			15			RectifierGroup Lost Status		RectGroup Lost Status	ВыпрПотеряСтатус			ВыпрПотеряСтатус
1779		32			15			Reset RectifierGroup Lost alarm		ResetRectGLostAlarm	СбросАвПотеряВыпр			гЕЁЩПотеряГруппы
1780		32			15			RectifierGroup Number on last power-on	RectGroup Number	НомерГруппы				НомерГруппы
1781		32			15			RectifierGroup Lost			RectifierGroup Lost	ПотеряГруппы				ПотеряГруппы
1782		32			15			ETemperature 1 High 1			ETemp 1 High 1		ЕТемп1Выс1				ЕТемп1Выс1
1783		32			15			ETemperature 1 Low			ETemp 1 Low		ЕТемп1Низк				ЕТемп1Низк
1784		32			15			ETemperature 2 High 1			ETemp 2 High 1		ЕТемп2Выс1				ЕТемп2Выс1
1785		32			15			ETemperature 2 Low			ETemp 2 Low		ЕТемп2Низк				ЕТемп2Низк
1786		32			15			ETemperature 3 High 1			ETemp 3 High 1		ЕТемп3Выс1				ЕТемп3Выс1
1787		32			15			ETemperature 3 Low			ETemp 3 Low		ЕТемп3Низк				ЕТемп3Низк
1788		32			15			ETemperature 4 High 1			ETemp 4 High 1		ЕТемп4Выс1v				ЕТемп4Выс1
1789		32			15			ETemperature 4 Low			ETemp 4 Low		ЕТемп4Низк				ЕТемп4Низк
1790		32			15			ETemperature 5 High 1			ETemp 5 High 1		ЕТемп5Выс1				ЕТемп5Выс1
1791		32			15			ETemperature 5 Low			ETemp 5 Low		ЕТемп5Низк				ЕТемп5Низк
1792		32			15			ETemperature 6 High 1			ETemp 6 High 1		ЕТемп6Выс1				ЕТемп6Выс1
1793		32			15			ETemperature 6 Low			ETemp 6 Low		ЕТемп6Низк				ЕТемп6Низк
1794		32			15			ETemperature 7 High 1			ETemp 7 High 1		ЕТемп7Выс1				ЕТемп7Выс1
1795		32			15			ETemperature 7 Low			ETemp 7 Low		ЕТемп7Низк				ЕТемп7Низк
1796		32			15			ETemperature 1 High 1			ETemp 1 High 1		ЕТемп1Выс1				ЕТемп1Выс1
1797		32			15			ETemperature 1 Low			ETemp 1 Low		ЕТемп1Низк				ЕТемп1Низк
1798		32			15			ETemperature 2 High 1			ETemp 2 High 1		ЕТемп2Выс1				ЕТемп2Выс1
1799		32			15			ETemperature 2 Low			ETemp 2 Low		ЕТемп2Низк				ЕТемп2Низк
1800		32			15			ETemperature 3 High 1			ETemp 3 High 1		ЕТемп3Выс1				ЕТемп3Выс1
1801		32			15			ETemperature 3 Low			ETemp 3 Low		ЕТемп3Низк				ЕТемп3Низк
1802		32			15			ETemperature 4 High 1			ETemp 4 High 1		ЕТемп4Выс1				ЕТемп4Выс1
1803		32			15			ETemperature 4 Low			ETemp 4 Low		ЕТемп4Низк				ЕТемп4Низк
1804		32			15			ETemperature 5 High 1			ETemp 5 High 1		ЕТемп5Выс1				ЕТемп5Выс1
1805		32			15			ETemperature 5 Low			ETemp 5 Low		ЕТемп5Низк				ЕТемп5Низк
1806		32			15			ETemperature 6 High 1			ETemp 6 High 1		ЕТемп6Выс1				ЕТемп6Выс1
1807		32			15			ETemperature 6 Low			ETemp 6 Low		ЕТемп6Низк				ЕТемп6Низк
1808		32			15			ETemperature 7 High 1			ETemp 7 High 1		ЕТемп7Выс1				ЕТемп7Выс1
1809		32			15			ETemperature 7 Low			ETemp 7 Low		ЕТемп7Низк				ЕТемп7Низк
1810		32			15			Fast Sampler Flag			Fast Sampler Flag	Fast Sampler Flag			Fast Sampler Flag
1811		32			15			LVD Quantity				LVD Quantity		Колво LVD				КолвоLVD
1812		32			15			LCD Rotation				LCD Rotation		ПоворотЭкр				LCDповорот
1813		32			15			0 deg					0 deg			0град					0град
1814		32			15			90 deg					90 deg			90град					90град
1815		32			15			180 deg					180 deg			180град					180град
1816		32			15			270 deg					270 deg			270град					270град
1817		32			15			Over Voltage 1				Over Voltage 1		ВысНапр1				ВысНапр1
1818		32			15			Over Voltage 2				Over Voltage 2		ВысНапр2				ВысНапр2
1819		32			15			Under Voltage 1				Under Voltage 1		НизкНапряж1				НизкНапряж1
1820		32			15			Under Voltage 2				Under Voltage 2		НизкНапряж2				НизкНапряж2
1821		32			15			Over Voltage1(24V)			Over Voltage 1		ВысНапр(24V)				ВысНапр(24V)
1822		32			15			Over Voltage2(24V)			Over Voltage 2		ВысНапр(24V)				ВысНапр(24V)
1823		32			15			Under Voltage1(24V)			Under Voltage 1		НизкНапряж(24V)				НизкНапряж(24V)
1824		32			15			Under Voltage2(24V)			Under Voltage 2		НизкНапряж(24V)				НизкНапряж(24V)
1825		32			15			Fail Safe Mode				Fail Safe		Fail Safe Mode				Fail Safe Mode
1826		32			15			Disable					Disable			Выкл					Выкл
1827		32			15			Enable					Enable			Вкл					Вкл
1828		32			15			Hybrid Mode				Hybrid Mode		Гибрид					Гибрид
1829		32			15			Disable					Disable			Выкл					Выкл
1830		32			15			Capacity				Capacity		Емкость					Емкость
1831		32			15			Fixed Daily				Fixed Daily		Fixed Daily				Fixed Daily
1832		64			15			DG run at high temp			DG run overtemp		ДГ работает при высокой температуре	ДГвысокойтем
1833		32			15			Used DG for Hybrid			Used DG			ДГУ-Гибрид				ДГУ-Гибрид
1834		32			15			DG1					DG1			ДГУ1					ДГУ1
1835		32			15			DG2					DG1			ДГУ1					ДГУ2
1836		32			15			Both					Both			ДГУ1и2					ДГУ1и2
1837		32			15			DI for Grid				DI for Grid		ЦифрВх Сеть				ЦифрВх Сеть
1838		32			15			DI1					DI1			ЦифрВх1					ЦифрВх1
1839		32			15			DI2					DI2			ЦифрВх2					ЦифрВх2
1840		32			15			DI3					DI3			ЦифрВх3					ЦифрВх3
1841		32			15			DI4					DI4			ЦифрВх4					ЦифрВх4
1842		32			15			DI5					DI5			ЦифрВх5					ЦифрВх5
1843		32			15			DI6					DI6			ЦифрВх6					ЦифрВх6
1844		32			15			DI7					DI7			ЦифрВх7					ЦифрВх7
1845		32			15			DI8					DI8			ЦифрВх8					ЦифрВх8
1846		32			15			DOD					DOD			DOD					DOD
1847		32			15			Discharge duration			Disch duration		ДлитРазряда				ДлитРазряда
1848		32			15			Start discharge time			Start dish time		ВремяНачала				ВремяНачала
1849		32			15			Diesel Run Over Temp			DG Run OverTemp		ДГУВысТемп				ДГУВысТемп
1850		32			15			DG1 is running				DG1 is running		ДГУ1работа				ДГУ1работа
1851		32			15			DG2 is running				DG2 is running		ДГУ2работа				ДГУ2работа
1852		32			15			Hybrid High load setting		High load set		ГибридПерегрузНастр			ПерегрузНастр
1853		32			15			Hybrid is High Load			High Load		Перегрузка				Перегрузка
1854		80			15			DG run time at high temp		DG Run time		ДГ работает при высокой температуре	ДГпритемпературе
1855		64			15			Equalising Start time			Equal Start tim		Выравнивание времени начала		Выравреначала
1856		32			15			DG1 Failure				DG1 Failure		ДГУ1Неиспр				ДГУ1Неиспр
1857		32			15			DG2 Failure				DG1 Failure		ДГУ2Неиспр				ДГУ2Неиспр
1858		32			15			Diesel Alarm Delay			DG Alarm Delay		ДГУАварЗадержка				ДГУАварЗадержка
1859		32			15			Grid is on				Grid is on		Сеть					Сеть
1860		32			15			PowerSplit Contactor Mode		Contactor Mode		PowerSplitКонтактор			PSконтактор
1861		32			15			Ambient Temp				Amb Temp		ОкрТемп					ОкрТемп
1862		32			15			Ambient Temp Sensor			Amb Temp Sensor		ОкрТемпДатч				ОкрТемпДатч
1863		32			15			None					None			нет					нет
1864		32			15			Temperature1				Temp1			Темп1					Темп1
1865		32			15			Temperature2				Temp2			Темп2					Темп2
1866		32			15			Temperature3				Temp3			Темп3					Темп3
1867		32			15			Temperature4				Temp4			Темп4					Темп4
1868		32			15			Temperature5				Temp5			Темп5					Темп5
1869		32			15			Temperature6				Temp6			Темп6					Темп6
1870		32			15			Temperature7				Temp7			Темп7					Темп7
1871		32			15			Temperature8				Temp8			Темп8					Темп8
1872		32			15			Temperature9				Temp9			Темп9					Темп9
1873		32			15			Temperature10				Temp10			Темп10					Темп10
1874		32			15			High Ambient Temp Level			High Amb Temp		ОкрТемпВысУр1				ОкрТемпВысУр1
1875		32			15			Low Ambient Temp Level			Low Amb Temp		ОкрТемпНизкУр1				ОкрТемпНизкУр1
1876		32			15			High Amb Temperature			High Temperature	ОкрТемпВыс				ОкрТемпВыс
1877		32			15			Low Amb Temperature			Low Temperature		ОкрТемпНизк				ОкрТемпНизк
1878		32			15			Temperature Sensor Fault		Sensor Fault		ТемпНеиспрДат				ТемпНеиспрДат
1879		32			15			Digital Input 1				DI 1			ЦифрВх1					ЦифрВх1
1880		32			15			Digital Input 2				DI 2			ЦифрВх2					ЦифрВх2
1881		32			15			Digital Input 3				DI 3			ЦифрВх3					ЦифрВх3
1882		32			15			Digital Input 4				DI 4			ЦифрВх4					ЦифрВх4
1883		32			15			Digital Input 5				DI 5			ЦифрВх5					ЦифрВх5
1884		32			15			Digital Input 6				DI 6			ЦифрВх6					ЦифрВх6
1885		32			15			Digital Input 7				DI 7			ЦифрВх7					ЦифрВх7
1886		32			15			Digital Input 8				DI 8			ЦифрВх8					ЦифрВх8
1887		32			15			Open					Open			Откр					Откр
1888		32			15			Closed					Closed			Закр					Закр
1889		32			15			Relay Output 1				Relay Output 1		Реле1					Реле1
1890		32			15			Relay Output 2				Relay Output 2		Реле2					Реле2
1891		32			15			Relay Output 3				Relay Output 3		Реле3					Реле3
1892		32			15			Relay Output 4				Relay Output 4		Реле4					Реле4
1893		32			15			Relay Output 5				Relay Output 5		Реле5					Реле5
1894		32			15			Relay Output 6				Relay Output 6		Реле6					Реле6
1895		32			15			Relay Output 7				Relay Output 7		Реле7					Реле7
1896		32			15			Relay Output 8				Relay Output 8		Реле8					Реле8
1897		32			15			DI1 Alarm State				DI1 Alarm State		ЦифрВх1Статус				ЦифрВх1
1898		32			15			DI2 Alarm State				DI2 Alarm State		ЦифрВх2Статус				ЦифрВх2
1899		32			15			DI3 Alarm State				DI3 Alarm State		ЦифрВх3Статус				ЦифрВх3
1900		32			15			DI4 Alarm State				DI4 Alarm State		ЦифрВх4Статус				ЦифрВх4
1901		32			15			DI5 Alarm State				DI5 Alarm State		ЦифрВх5Статус				ЦифрВх5
1902		32			15			DI6 Alarm State				DI6 Alarm State		ЦифрВх6Статус				ЦифрВх6
1903		32			15			DI7 Alarm State				DI7 Alarm State		ЦифрВх7Статус				ЦифрВх7
1904		32			15			DI8 Alarm State				DI8 Alarm State		ЦифрВх8Статус				ЦифрВх8
1905		32			15			DI1 Alarm				DI1 Alarm		ЦифрВх1Авар				ЦиВх1
1906		32			15			DI2 Alarm				DI2 Alarm		ЦифрВх2Авар				ЦиВх2
1907		32			15			DI3 Alarm				DI3 Alarm		ЦифрВх3Авар				ЦиВх3
1908		32			15			DI4 Alarm				DI4 Alarm		ЦифрВх4Авар				ЦиВх4
1909		32			15			DI5 Alarm				DI5 Alarm		ЦифрВх5Авар				ЦиВх5
1910		32			15			DI6 Alarm				DI6 Alarm		ЦифрВх6Авар				ЦиВх6
1911		32			15			DI7 Alarm				DI7 Alarm		ЦифрВх7Авар				ЦиВх7
1912		32			15			DI8 Alarm				DI8 Alarm		ЦифрВх8Авар				ЦиВх8
1913		32			15			On					On			Вкл					Вкл
1914		32			15			Off					Off			Выкл					Выкл
1915		32			15			High					High			Выс					Выс
1916		32			15			Low					Low			Низк					Низк
1917		32			15			IB Num					IB Num			IB Num					IB Num
1918		32			15			IB Type					IB Type			IB Type					IB Type
1919		32			15			CSU_Undervoltage 1			Undervoltage 1		Undervoltage 1				Undervoltage 1
1920		32			15			CSU_Undervoltage 2			Undervoltage 2		Undervoltage 2				Undervoltage 2
1921		32			15			CSU_Overvoltage				Overvoltage		Overvoltage				Overvoltage
1922		32			15			CSU_Communication fault			Comm fault		Communication fault			Comm fault
1923		32			15			CSU_External alarm 1			Exter_alarm1		External alarm 1			Exter_alarm1
1924		32			15			CSU_External alarm 2			Exter_alarm2		External alarm 2			Exter_alarm2
1925		32			15			CSU_External alarm 3			Exter_alarm3		External alarm 3			Exter_alarm3
1926		32			15			CSU_External alarm 4			Exter_alarm4		External alarm 4			Exter_alarm4
1927		32			15			CSU_External alarm 5			Exter_alarm5		External alarm 5			Exter_alarm5
1928		32			15			CSU_External alarm 6			Exter_alarm6		External alarm 6			Exter_alarm6
1929		32			15			CSU_External alarm 7			Exter_alarm7		External alarm 7			Exter_alarm7
1930		32			15			CSU_External alarm 8			Exter_alarm8		External alarm 8			Exter_alarm8
1931		32			15			CSU_External comm fault			Ext_comm fault		External comm fault			Ext_comm fault
1932		32			15			CSU_Bat_curr limit alarm		Bat_curr limit		Bat_curr limit alarm			Bat_curr limit
1933		32			15			DCLCNumber				DCLCNumber		DCLCNumber				DCLCNumber
1934		32			15			BatLCNumber				BatLCNumber		BatLCNumber				BatLCNumber
1935		32			15			Ambient Temp High2			Amb Temp Hi2		Внешняя темпер высокая 2 уров		ВнешТемпВыс2
1936		32			15			System Type				System Type		Тип					Тип
1937		32			15			Normal					Normal			Норма					Норма
1938		32			15			Test					Test			Тест					Тест
1939		32			15			Auto Mode				Auto Mode		АвтоРежим				Авто
1940		32			15			EMEA					EMEA			EMEA					EMEA
1941		32			15			Normal					Normal			Норма					Норма
1942		32			15			Bus Run Mode				Bus Run Mode		вэоъткппдёй+				вэоъдёй+
1943		32			15			NO					NO			NO					NO
1944		32			15			NC					NC			NC					NC
1945		64			15			Fail Safe Mode(Hybrid)			Fail Safe		Отказ безопас режима (гибрид)		ОтказБезопРеж
1946		32			15			IB Communication Fail			IB Comm Fail		IB потеря связи				IBпотеряСвязи
1947		32			15			Relay Testing				Relay Testing		Тест реле				ТестРеле
1948		32			15			Testing Relay 1				Testing Relay1		Тест реле 1				ТестРеле1
1949		32			15			Testing Relay 2				Testing Relay2		Тест реле 2				ТестРеле2
1950		32			15			Testing Relay 3				Testing Relay3		Тест реле 3				ТестРеле3
1951		32			15			Testing Relay 4				Testing Relay4		Тест реле 4				ТестРеле4
1952		32			15			Testing Relay 5				Testing Relay5		Тест реле 5				ТестРеле5
1953		32			15			Testing Relay 6				Testing Relay6		Тест реле 6				ТестРеле6
1954		32			15			Testing Relay 7				Testing Relay7		Тест реле 7				ТестРеле7
1955		32			15			Testing Relay 8				Testing Relay8		Тест реле 8				ТестРеле8
1956		32			15			Relay Test				Relay Test		Тест реле				ТестРеле
1957		32			15			Relay Test Time				Relay Test Time		Время теста реле			ВремТестРеле
1958		32			15			Manual					Manual			вручную					вручную
1959		32			15			Automatic				Automatic		Автоматический				Автоматический
1960		32			15			System Temp1				System T1		Темп системы 1				ТемпСист1
1961		32			15			System Temp2				System T2		Темп системы 2				ТемпСист2
1962		32			15			System Temp3				System T3		Темп системы 3				ТемпСист3
1963		32			15			IB2 Temp1				IB2 T1			IB2 темп 1				IB2темп1
1964		32			15			IB2 Temp2				IB2 T2			IB2 темп 2				IB2темп2
1965		32			15			EIB Temp1				EIB T1			EIB темп 1				EIB темп1
1966		32			15			EIB Temp2				EIB T2			EIB темп 2				EIB темп2
1967		32			15			SMTemp1 Temp1				SMTemp1 T1		SM датчик 1 темп 1			SMдатч1темп1
1968		32			15			SMTemp1 Temp2				SMTemp1 T2		SM датчик 1 темп 2			SMдатч1темп2
1969		32			15			SMTemp1 Temp3				SMTemp1 T3		SM датчик 1 темп 3			SMдатч1темп3
1970		32			15			SMTemp1 Temp4				SMTemp1 T4		SM датчик 1 темп 4			SMдатч1темп4
1971		32			15			SMTemp1 Temp5				SMTemp1 T5		SM датчик 1 темп 5			SMдатч1темп5
1972		32			15			SMTemp1 Temp6				SMTemp1 T6		SM датчик 1 темп 6			SMдатч1темп6
1973		32			15			SMTemp1 Temp7				SMTemp1 T7		SM датчик 1 темп 7			SMдатч1темп7
1974		32			15			SMTemp1 Temp8				SMTemp1 T8		SM датчик 1 темп 8			SMдатч1темп8
1975		32			15			SMTemp2 Temp1				SMTemp2 T1		SM датчик 2 темп 1			Smдатч2темп1
1976		32			15			SMTemp2 Temp2				SMTemp2 T2		SM датчик 2 темп 2			Smдатч2темп2
1977		32			15			SMTemp2 Temp3				SMTemp2 T3		SM датчик 2 темп 3			Smдатч2темп3
1978		32			15			SMTemp2 Temp4				SMTemp2 T4		SM датчик 2 темп 4			Smдатч2темп4
1979		32			15			SMTemp2 Temp5				SMTemp2 T5		SM датчик 2 темп 5			Smдатч2темп5
1980		32			15			SMTemp2 Temp6				SMTemp2 T6		SM датчик 2 темп 6			Smдатч2темп6
1981		32			15			SMTemp2 Temp7				SMTemp2 T7		SM датчик 2 темп 7			Smдатч2темп7
1982		32			15			SMTemp2 Temp8				SMTemp2 T8		SM датчик 2 темп 8			Smдатч2темп8
1983		32			15			SMTemp3 Temp1				SMTemp3 T1		SM датчик 3 темп 1			Smдатч3темп1
1984		32			15			SMTemp3 Temp2				SMTemp3 T2		SM датчик 3 темп 2			Smдатч3темп2
1985		32			15			SMTemp3 Temp3				SMTemp3 T3		SM датчик 3 темп 3			Smдатч3темп3
1986		32			15			SMTemp3 Temp4				SMTemp3 T4		SM датчик 3 темп 4			Smдатч3темп4
1987		32			15			SMTemp3 Temp5				SMTemp3 T5		SM датчик 3 темп 5			Smдатч3темп5
1988		32			15			SMTemp3 Temp6				SMTemp3 T6		SM датчик 3 темп 6			Smдатч3темп6
1989		32			15			SMTemp3 Temp7				SMTemp3 T7		SM датчик 3 темп 7			Smдатч3темп7
1990		32			15			SMTemp3 Temp8				SMTemp3 T8		SM датчик 3 темп 8			Smдатч3темп8
1991		32			15			SMTemp4 Temp1				SMTemp4 T1		SM датчик 4 темп 1			Smдатч4темп1
1992		32			15			SMTemp4 Temp2				SMTemp4 T2		SM датчик 4 темп 2			Smдатч4темп2
1993		32			15			SMTemp4 Temp3				SMTemp4 T3		SM датчик 4 темп 3			Smдатч4темп3
1994		32			15			SMTemp4 Temp4				SMTemp4 T4		SM датчик 4 темп 4			Smдатч4темп4
1995		32			15			SMTemp4 Temp5				SMTemp4 T5		SM датчик 4 темп 5			Smдатч4темп5
1996		32			15			SMTemp4 Temp6				SMTemp4 T6		SM датчик 4 темп 6			Smдатч4темп6
1997		32			15			SMTemp4 Temp7				SMTemp4 T7		SM датчик 4 темп 7			Smдатч4темп7
1998		32			15			SMTemp4 Temp8				SMTemp4 T8		SM датчик 4 темп 8			Smдатч4темп8
1999		32			15			SMTemp5 Temp1				SMTemp5 T1		SM датчик 5 темп 1			Smдатч5темп1
2000		32			15			SMTemp5 Temp2				SMTemp5 T2		SM датчик 5 темп 2			Smдатч5темп2
2001		32			15			SMTemp5 Temp3				SMTemp5 T3		SM датчик 5 темп 3			Smдатч5темп3
2002		32			15			SMTemp5 Temp4				SMTemp5 T4		SM датчик 5 темп 4			Smдатч5темп4
2003		32			15			SMTemp5 Temp5				SMTemp5 T5		SM датчик 5 темп 5			Smдатч5темп5
2004		32			15			SMTemp5 Temp6				SMTemp5 T6		SM датчик 5 темп 6			Smдатч5темп6
2005		32			15			SMTemp5 Temp7				SMTemp5 T7		SM датчик 5 темп 7			Smдатч5темп7
2006		32			15			SMTemp5 Temp8				SMTemp5 T8		SM датчик 5 темп 8			Smдатч5темп8
2007		32			15			SMTemp6 Temp1				SMTemp6 T1		SM датчик 6 темп 1			Smдатч6темп1
2008		32			15			SMTemp6 Temp2				SMTemp6 T2		SM датчик 6 темп 2			Smдатч6темп2
2009		32			15			SMTemp6 Temp3				SMTemp6 T3		SM датчик 6 темп 3			Smдатч6темп3
2010		32			15			SMTemp6 Temp4				SMTemp6 T4		SM датчик 6 темп 4			Smдатч6темп4
2011		32			15			SMTemp6 Temp5				SMTemp6 T5		SM датчик 6 темп 5			Smдатч6темп5
2012		32			15			SMTemp6 Temp6				SMTemp6 T6		SM датчик 6 темп 6			Smдатч6темп6
2013		32			15			SMTemp6 Temp7				SMTemp6 T7		SM датчик 6 темп 7			Smдатч6темп7
2014		32			15			SMTemp6 Temp8				SMTemp6 T8		SM датчик 6 темп 8			Smдатч6темп8
2015		32			15			SMTemp7 Temp1				SMTemp7 T1		SM датчик 7 темп 1			Smдатч7темп1
2016		32			15			SMTemp7 Temp2				SMTemp7 T2		SM датчик 7 темп 2			Smдатч7темп2
2017		32			15			SMTemp7 Temp3				SMTemp7 T3		SM датчик 7 темп 3			Smдатч7темп3
2018		32			15			SMTemp7 Temp4				SMTemp7 T4		SM датчик 7 темп 4			Smдатч7темп4
2019		32			15			SMTemp7 Temp5				SMTemp7 T5		SM датчик 7 темп 5			Smдатч7темп5
2020		32			15			SMTemp7 Temp6				SMTemp7 T6		SM датчик 7 темп 6			Smдатч7темп6
2021		32			15			SMTemp7 Temp7				SMTemp7 T7		SM датчик 7 темп 7			Smдатч7темп7
2022		32			15			SMTemp7 Temp8				SMTemp7 T8		SM датчик 7 темп 8			Smдатч7темп8
2023		32			15			SMTemp8 Temp1				SMTemp8 T1		SM датчик 8 темп 1			Smдатч8темп1
2024		32			15			SMTemp8 Temp2				SMTemp8 T2		SM датчик 8 темп 2			Smдатч8темп2
2025		32			15			SMTemp8 Temp3				SMTemp8 T3		SM датчик 8 темп 3			Smдатч8темп3
2026		32			15			SMTemp8 Temp4				SMTemp8 T4		SM датчик 8 темп 4			Smдатч8темп4
2027		32			15			SMTemp8 Temp5				SMTemp8 T5		SM датчик 8 темп 5			Smдатч8темп5
2028		32			15			SMTemp8 Temp6				SMTemp8 T6		SM датчик 8 темп 6			Smдатч8темп6
2029		32			15			SMTemp8 Temp7				SMTemp8 T7		SM датчик 8 темп 7			Smдатч8темп7
2030		32			15			SMTemp8 Temp8				SMTemp8 T8		SM датчик 8 темп 8			Smдатч8темп8
2031		64			15			System Temp1 High 2			System T1 Hi2		Темпер системы 1 высокая уров 2		ТемпСист1высок2
2032		64			15			System Temp1 High 1			System T1 Hi1		Темпер системы 1 высокая уров 1		ТемпСист1высок1
2033		64			15			System Temp1 Low			System T1 Low		Темпер системы 1 низкая			ТемпСист1низк
2034		64			15			System Temp2 High 2			System T2 Hi2		Темпер системы 2 высокая уров 2		ТемпСист2высок2
2035		64			15			System Temp2 High 1			System T2 Hi1		Темпер системы 2 высокая уров 1		ТемпСист2высок1
2036		64			15			System Temp2 Low			System T2 Low		Темпер системы 2 низкая			ТемпСист2низк
2037		64			15			System Temp3 High 2			System T3 Hi2		Темпер системы 3 высокая уров 2		ТемпСист3высок2
2038		64			15			System Temp3 High 1			System T3 Hi1		Темпер системы 3 высокая уров 1		ТемпСист3высок1
2039		64			15			System Temp3 Low			System T3 Low		Темпер системы 3 низкая			ТемпСист3низк
2040		64			15			IB2 Temp1 High 2			IB2 T1 Hi2		IB2 температура 1 высокая уров 2	IBтемп1высок2
2041		64			15			IB2 Temp1 High 1			IB2 T1 Hi1		IB2 температура 1 высокая уров 1	IBтемп1высок1
2042		64			15			IB2 Temp1 Low				IB2 T1 Low		IB2 температура 1 низкая		IBтемп1низк
2043		64			15			IB2 Temp2 High 2			IB2 T2 Hi2		IB2 температура 2 высокая уров 2	Ibтемп2высок2
2044		64			15			IB2 Temp2 High 1			IB2 T2 Hi1		IB2 температура 2 высокая уров 1	Ibтемп2высок1
2045		64			15			IB2 Temp2 Low				IB2 T2 Low		IB2 температура 2 низкая		Ibтемп2низк
2046		64			15			EIB Temp1 High 2			EIB T1 Hi2		EIB температура 1 высокая уров 2	EIBтемп1высок2
2047		64			15			EIB Temp1 High 1			EIB T1 Hi1		EIB температура 1 высокая уров 1	EIBтемп1высок1
2048		64			15			EIB Temp1 Low				EIB T1 Low		EIB температура 1 низкая		EIBтемп1низк
2049		64			15			EIB Temp2 High 2			EIB T2 Hi2		EIB температура 2 высокая уров 2	EIBтемп2высок2
2050		64			15			EIB Temp2 High 1			EIB T2 Hi1		EIB температура 2 высокая уров 1	EIBтемп2высок1
2051		64			15			EIB Temp2 Low				EIB T2 Low		EIB температура 2 низкая		EIBтемп2низк
2052		64			15			SMTemp1 Temp1 High 2			SMTemp1 T1 Hi2		SM датч 1 темпер 1 высок уров 2		SM1темп1высок2
2053		64			15			SMTemp1 Temp1 High 1			SMTemp1 T1 Hi1		SM датч 1 темпер 1 высок уров 1		SM1темп1высок1
2054		64			15			SMTemp1 Temp1 Low			SMTemp1 T1 Low		SM датч 1 темпер 1 низкая		SM1темп1низк
2055		64			15			SMTemp1 Temp2 High 2			SMTemp1 T2 Hi2		SM датч 1 темпер 2 высок уров 2		SM1темп2высок2
2056		64			15			SMTemp1 Temp2 High 1			SMTemp1 T2 Hi1		SM датч 1 темпер 2 высок уров 1		SM1темп2высок1
2057		64			15			SMTemp1 Temp2 Low			SMTemp1 T2 Low		SM датч 1 темпер 2 низкая		SM1темп2низк
2058		64			15			SMTemp1 Temp3 High 2			SMTemp1 T3 Hi2		SM датч 1 темпер 3 высок уров 2		SM1темп3высок2
2059		64			15			SMTemp1 Temp3 High 1			SMTemp1 T3 Hi1		SM датч 1 темпер 3 высок уров 1		SM1темп3высок1
2060		64			15			SMTemp1 Temp3 Low			SMTemp1 T3 Low		SM датч 1 темпер 3 низкая		SM1темп3низк
2061		64			15			SMTemp1 Temp4 High 2			SMTemp1 T4 Hi2		SM датч 1 темпер 4 высок уров 2		SM1темп4высок2
2062		64			15			SMTemp1 Temp4 High 1			SMTemp1 T4 Hi1		SM датч 1 темпер 4 высок уров 1		SM1темп4высок1
2063		64			15			SMTemp1 Temp4 Low			SMTemp1 T4 Low		SM датч 1 темпер 4 низкая		SM1темп4низк
2064		64			15			SMTemp1 Temp5 High 2			SMTemp1 T5 Hi2		SM датч 1 темпер 5 высок уров 2		SM1темп5высок2
2065		64			15			SMTemp1 Temp5 High 1			SMTemp1 T5 Hi1		SM датч 1 темпер 5 высок уров 1		SM1темп5высок1
2066		64			15			SMTemp1 Temp5 Low			SMTemp1 T5 Low		SM датч 1 темпер 5 низкая		SM1темп5низк
2067		64			15			SMTemp1 Temp6 High 2			SMTemp1 T6 Hi2		SM датч 1 темпер 6 высок уров 2		SM1темп6высок2
2068		64			15			SMTemp1 Temp6 High 1			SMTemp1 T6 Hi1		SM датч 1 темпер 6 высок уров 1		SM1темп6высок1
2069		64			15			SMTemp1 Temp6 Low			SMTemp1 T6 Low		SM датч 1 темпер 6 низкая		SM1темп6низк
2070		64			15			SMTemp1 Temp7 High 2			SMTemp1 T7 Hi2		SM датч 1 темпер 7 высок уров 2		SM1темп7высок2
2071		64			15			SMTemp1 Temp7 High 1			SMTemp1 T7 Hi1		SM датч 1 темпер 7 высок уров 1		SM1темп7высок1
2072		64			15			SMTemp1 Temp7 Low			SMTemp1 T7 Low		SM датч 1 темпер 7 низкая		SM1темп7низк
2073		64			15			SMTemp1 Temp8 High 2			SMTemp1 T8 Hi2		SM датч 1 темпер 8 высок уров 2		SM1темп8высок2
2074		64			15			SMTemp1 Temp8 High 1			SMTemp1 T8 Hi1		SM датч 1 темпер 8 высок уров 1		SM1темп8высок1
2075		64			15			SMTemp1 Temp8 Low			SMTemp1 T8 Low		SM датч 1 темпер 8 низкая		SM1темп8низк
2076		64			15			SMTemp2 Temp1 High 2			SMTemp2 T1 Hi2		SM датч 2 темпер 1 высок уров 2		SM2темп1высок2
2077		64			15			SMTemp2 Temp1 High 1			SMTemp2 T1 Hi1		SM датч 2 темпер 1 высок уров 1		SM2темп1высок1
2078		64			15			SMTemp2 Temp1 Low			SMTemp2 T1 Low		SM датч 2 темпер 1 низкая		SM2темп1низк
2079		64			15			SMTemp2 Temp2 High 2			SMTemp2 T2 Hi2		SM датч 2 темпер 2 высок уров 2		SM2темп2высок2
2080		64			15			SMTemp2 Temp2 High 1			SMTemp2 T2 Hi1		SM датч 2 темпер 2 высок уров 1		SM2темп2высок1
2081		64			15			SMTemp2 Temp2 Low			SMTemp2 T2 Low		SM датч 2 темпер 2 низкая		SM2темп2низк
2082		64			15			SMTemp2 Temp3 High 2			SMTemp2 T3 Hi2		SM датч 2 темпер 3 высок уров 2		SM2темп3высок2
2083		64			15			SMTemp2 Temp3 High 1			SMTemp2 T3 Hi1		SM датч 2 темпер 3 высок уров 1		SM2темп3высок1
2084		64			15			SMTemp2 Temp3 Low			SMTemp2 T3 Low		SM датч 2 темпер 3 низкая		SM2темп3низк
2085		64			15			SMTemp2 Temp4 High 2			SMTemp2 T4 Hi2		SM датч 2 темпер 4 высок уров 2		SM2темп4высок2
2086		64			15			SMTemp2 Temp4 High 1			SMTemp2 T4 Hi1		SM датч 2 темпер 4 высок уров 1		SM2темп4высок1
2087		64			15			SMTemp2 Temp4 Low			SMTemp2 T4 Low		SM датч 2 темпер 4 низкая		SM2темп4низк
2088		64			15			SMTemp2 Temp5 High 2			SMTemp2 T5 Hi2		SM датч 2 темпер 5 высок уров 2		SM2темп5высок2
2089		64			15			SMTemp2 Temp5 High 1			SMTemp2 T5 Hi1		SM датч 2 темпер 5 высок уров 1		SM2темп5высок1
2090		64			15			SMTemp2 Temp5 Low			SMTemp2 T5 Low		SM датч 2 темпер 5 низкая		SM2темп5низк
2091		64			15			SMTemp2 Temp6 High 2			SMTemp2 T6 Hi2		SM датч 2 темпер 6 высок уров 2		SM2темп6высок2
2092		64			15			SMTemp2 Temp6 High 1			SMTemp2 T6 Hi1		SM датч 2 темпер 6 высок уров 1		SM2темп6высок1
2093		64			15			SMTemp2 Temp6 Low			SMTemp2 T6 Low		SM датч 2 темпер 6 низкая		SM2темп6низк
2094		64			15			SMTemp2 Temp7 High 2			SMTemp2 T7 Hi2		SM датч 2 темпер 7 высок уров 2		SM2темп7высок2
2095		64			15			SMTemp2 Temp7 High 1			SMTemp2 T7 Hi1		SM датч 2 темпер 7 высок уров 1		SM2темп7высок1
2096		64			15			SMTemp2 Temp7 Low			SMTemp2 T7 Low		SM датч 2 темпер 7 низкая		SM2темп7низк
2097		64			15			SMTemp2 Temp8 High 2			SMTemp2 T8 Hi2		SM датч 2 темпер 8 высок уров 2		SM2темп8высок2
2098		64			15			SMTemp2 Temp8 High 1			SMTemp2 T8 Hi1		SM датч 2 темпер 8 высок уров 1		SM2темп8высок1
2099		64			15			SMTemp2 Temp8 Low			SMTemp2 T8 Low		SM датч 2 темпер 8 низкая		SM2темп8низк
2100		64			15			SMTemp3 Temp1 High 2			SMTemp3 T1 Hi2		SM датч 3 темпер 1 высок уров 2		SM3темп1высок2
2101		64			15			SMTemp3 Temp1 High 1			SMTemp3 T1 Hi1		SM датч 3 темпер 1 высок уров 1		SM3темп1высок1
2102		64			15			SMTemp3 Temp1 Low			SMTemp3 T1 Low		SM датч 3 темпер 1 низкая		SM3темп1низк
2103		64			15			SMTemp3 Temp2 High 2			SMTemp3 T2 Hi2		SM датч 3 темпер 2 высок уров 2		SM3темп2высок2
2104		64			15			SMTemp3 Temp2 High 1			SMTemp3 T2 Hi1		SM датч 3 темпер 2 высок уров 1		SM3темп2высок1
2105		64			15			SMTemp3 Temp2 Low			SMTemp3 T2 Low		SM датч 3 темпер 2 низкая		SM3темп2низк
2106		64			15			SMTemp3 Temp3 High 2			SMTemp3 T3 Hi2		SM датч 3 темпер 2 высок уров 2		SM3темп2высок2
2107		64			15			SMTemp3 Temp3 High 1			SMTemp3 T3 Hi1		SM датч 3 темпер 3 высок уров 1		SM3темп3высок1
2108		64			15			SMTemp3 Temp3 Low			SMTemp3 T3 Low		SM датч 3 темпер 3 низкая		SM3темп3низк
2109		64			15			SMTemp3 Temp4 High 2			SMTemp3 T4 Hi2		SM датч 3 темпер 4 высок уров 2		SM3темп4высок2
2110		64			15			SMTemp3 Temp4 High 1			SMTemp3 T4 Hi1		SM датч 3 темпер 4 высок уров 1		SM3темп4высок1
2111		64			15			SMTemp3 Temp4 Low			SMTemp3 T4 Low		SM датч 3 темпер 4 низкая		SM3темп4низк
2112		64			15			SMTemp3 Temp5 High 2			SMTemp3 T5 Hi2		SM датч 3 темпер 4 низкая		SM3темп4низк
2113		64			15			SMTemp3 Temp5 High 1			SMTemp3 T5 Hi1		SM датч 3 темпер 5 высок уров 1		SM3темп5высок1
2114		64			15			SMTemp3 Temp5 Low			SMTemp3 T5 Low		SM датч 3 темпер 5 низкая		SM3темп5низк
2115		64			15			SMTemp3 Temp6 High 2			SMTemp3 T6 Hi2		SM датч 3 темпер 6 высок уров 2		SM3темп6высок2
2116		64			15			SMTemp3 Temp6 High 1			SMTemp3 T6 Hi1		SM датч 3 темпер 6 высок уров 1		SM3темп6высок1
2117		64			15			SMTemp3 Temp6 Low			SMTemp3 T6 Low		SM датч 3 темпер 6 низкая		SM3темп6низк
2118		64			15			SMTemp3 Temp7 High 2			SMTemp3 T7 Hi2		SM датч 3 темпер 7 высок уров 2		SM3темп7высок2
2119		64			15			SMTemp3 Temp7 High 1			SMTemp3 T7 Hi1		SM датч 3 темпер 7 высок уров 1		SM3темп7высок1
2120		64			15			SMTemp3 Temp7 Low			SMTemp3 T7 Low		SM датч 3 темпер 7 низкая		SM3темп7низк
2121		64			15			SMTemp3 Temp8 High 2			SMTemp3 T8 Hi2		SM датч 3 темпер 8 высок уров 2		SM3темп8высок2
2122		64			15			SMTemp3 Temp8 High 1			SMTemp3 T8 Hi1		SM датч 3 темпер 8 высок уров 1		SM3темп8высок1
2123		64			15			SMTemp3 Temp8 Low			SMTemp3 T8 Low		SM датч 3 темпер 8 низкая		SM3темп8низк
2124		64			15			SMTemp4 Temp1 High 2			SMTemp4 T1 Hi2		SM датч 4 темпер 1 высок уров 2		SM4темп1высок2
2125		64			15			SMTemp4 Temp1 High 1			SMTemp4 T1 Hi1		SM датч 4 темпер 1 высок уров 1		SM4темп1высок1
2126		64			15			SMTemp4 Temp1 Low			SMTemp4 T1 Low		SM датч 4 темпер 1 низкая		SM4темп1низк
2127		64			15			SMTemp4 Temp2 High 2			SMTemp4 T2 Hi2		SM датч 4 темпер 2 высок уров 2		SM4темп2высок2
2128		64			15			SMTemp4 Temp2 High 1			SMTemp4 T2 Hi1		SM датч 4 темпер 2 высок уров 1		SM4темп2высок1
2129		64			15			SMTemp4 Temp2 Low			SMTemp4 T2 Low		SM датч 4 темпер 2 низкая		SM4темп2низк
2130		64			15			SMTemp4 Temp3 High 2			SMTemp4 T3 Hi2		SM датч 4 темпер 3 высок уров 2		SM4темп3высок2
2131		64			15			SMTemp4 Temp3 High 1			SMTemp4 T3 Hi1		SM датч 4 темпер 3 высок уров 1		SM4темп3высок1
2132		64			15			SMTemp4 Temp3 Low			SMTemp4 T3 Low		SM датч 4 темпер 3 низкая		SM4темп3низк
2133		64			15			SMTemp4 Temp4 High 2			SMTemp4 T4 Hi2		SM датч 4 темпер 4 высок уров 2		SM4темп4высок2
2134		64			15			SMTemp4 Temp4 High 1			SMTemp4 T4 Hi1		SM датч 4 темпер 4 высок уров 1		SM4темп4высок1
2135		64			15			SMTemp4 Temp4 Low			SMTemp4 T4 Low		SM датч 4 темпер 4 низкая		SM4темп4низк
2136		64			15			SMTemp4 Temp5 High 2			SMTemp4 T5 Hi2		SM датч 4 темпер 5 высок уров 2		SM4темп5высок2
2137		64			15			SMTemp4 Temp5 High 1			SMTemp4 T5 Hi1		SM датч 4 темпер 5высок уров 1		SM4темп5высок1
2138		64			15			SMTemp4 Temp5 Low			SMTemp4 T5 Low		SM датч 4 темпер 5 низкая		SM4темп5низк
2139		64			15			SMTemp4 Temp6 High 2			SMTemp4 T6 Hi2		SM датч 4 темпер 6 высок уров 2		SM4темп6высок2
2140		64			15			SMTemp4 Temp6 High 1			SMTemp4 T6 Hi1		SM датч 4 темпер 6высок уров 1		SM4темп6высок1
2141		64			15			SMTemp4 Temp6 Low			SMTemp4 T6 Low		SM датч 4 темпер 6 низкая		SM4темп6низк
2142		64			15			SMTemp4 Temp7 High 2			SMTemp4 T7 Hi2		SM датч 4 темпер 7 высок уров 2		SM4темп7высок2
2143		64			15			SMTemp4 Temp7 High 1			SMTemp4 T7 Hi1		SM датч 4 темпер 7высок уров 1		SM4темп7высок1
2144		64			15			SMTemp4 Temp7 Low			SMTemp4 T7 Low		SM датч 4 темпер 7 низкая		SM4темп7низк
2145		64			15			SMTemp4 Temp8 High 2			SMTemp4 T8 Hi2		SM датч 4 темпер 8 высок уров 2		SM4темп8высок2
2146		64			15			SMTemp4 Temp8 Hi1			SMTemp4 T8 Hi1		SM датч 4 темпер 8высок уров 1		SM4темп8высок1
2147		64			15			SMTemp4 Temp8 Low			SMTemp4 T8 Low		SM датч 4 темпер 8 низкая		SM4темп8низк
2148		64			15			SMTemp5 Temp1 High 2			SMTemp5 T1 Hi2		SM датч 5 темпер 1 высок уров 2		SM5темп1высок2
2149		64			15			SMTemp5 Temp1 High 1			SMTemp5 T1 Hi1		SM датч 5 темпер 1высок уров 1		SM5темп1высок1
2150		64			15			SMTemp5 Temp1 Low			SMTemp5 T1 Low		SM датч 5 темпер 1 низкая		SM5темп1низк
2151		64			15			SMTemp5 Temp2 High 2			SMTemp5 T2 Hi2		SM датч 5 темпер 2 высок уров 2		SM5темп2высок2
2152		64			15			SMTemp5 Temp2 High 1			SMTemp5 T2 Hi1		SM датч 5 темпер 2высок уров 1		SM5темп2высок1
2153		64			15			SMTemp5 Temp2 Low			SMTemp5 T2 Low		SM датч 5 темпер 2 низкая		SM5темп2низк
2154		64			15			SMTemp5 Temp3 High 2			SMTemp5 T3 Hi2		SM датч 5 темпер 3 высок уров 2		SM5темп3высок2
2155		64			15			SMTemp5 Temp3 High 1			SMTemp5 T3 Hi1		SM датч 5 темпер 3высок уров 1		SM5темп3высок1
2156		64			15			SMTemp5 Temp3 Low			SMTemp5 T3 Low		SM датч 5 темпер 3 низкая		SM5темп3низк
2157		64			15			SMTemp5 Temp4 High 2			SMTemp5 T4 Hi2		SM датч 5 темпер 4 высок уров 2		SM5темп4высок2
2158		64			15			SMTemp5 Temp4 High 1			SMTemp5 T4 Hi1		SM датч 5 темпер 4высок уров 1		SM5темп4высок1
2159		64			15			SMTemp5 Temp4 Low			SMTemp5 T4 Low		SM датч 5 темпер 3 низкая		SM5темп3низк
2160		64			15			SMTemp5 Temp5 High 2			SMTemp5 T5 Hi2		SM датч 5 темпер 5 высок уров 2		SM5темп5высок2
2161		64			15			SMTemp5 Temp5 High 1			SMTemp5 T5 Hi1		SM датч 5 темпер 5высок уров 1		SM5темп5высок1
2162		64			15			SMTemp5 Temp5 Low			SMTemp5 T5 Low		SM датч 5 темпер 5 низкая		SM5темп5низк
2163		64			15			SMTemp5 Temp6 High 2			SMTemp5 T6 Hi2		SM датч 5 темпер 6 высок уров 2		SM5темп6высок2
2164		64			15			SMTemp5 Temp6 High 1			SMTemp5 T6 Hi1		SM датч 5 темпер 6высок уров 1		SM5темп6высок1
2165		64			15			SMTemp5 Temp6 Low			SMTemp5 T6 Low		SM датч 5 темпер 6 низкая		SM5темп6низк
2166		64			15			SMTemp5 Temp7 High 2			SMTemp5 T7 Hi2		SM датч 5 темпер 7 высок уров 2		SM5темп7высок2
2167		64			15			SMTemp5 Temp7 High 1			SMTemp5 T7 Hi1		SM датч 5 темпер 7высок уров 1		SM5темп7высок1
2168		64			15			SMTemp5 Temp7 Low			SMTemp5 T7 Low		SM датч 5 темпер 7 низкая		SM5темп7низк
2169		64			15			SMTemp5 Temp8 High 2			SMTemp5 T8 Hi2		SM датч 5 темпер 8 высок уров 2		SM5темп8высок2
2170		64			15			SMTemp5 Temp8 High 1			SMTemp5 T8 Hi1		SM датч 5 темпер 8высок уров 1		SM5темп8высок1
2171		64			15			SMTemp5 Temp8 Low			SMTemp5 T8 Low		SM датч 5 темпер 8 низкая		SM5темп8низк
2172		64			15			SMTemp6 Temp1 High 2			SMTemp6 T1 Hi2		SM датч 6 темпер 1 высок уров 2		SM6темп1высок2
2173		64			15			SMTemp6 Temp1 High 1			SMTemp6 T1 Hi1		SM датч 6 темпер 1высок уров 1		SM6темп1высок1
2174		64			15			SMTemp6 Temp1 Low			SMTemp6 T1 Low		SM датч 6 темпер 1 низкая		SM6темп1низк
2175		64			15			SMTemp6 Temp2 High 2			SMTemp6 T2 Hi2		SM датч 6 темпер 2 высок уров 2		SM6темп2высок2
2176		64			15			SMTemp6 Temp2 High 1			SMTemp6 T2 Hi1		SM датч 6 темпер 2высок уров 1		SM6темп2высок1
2177		64			15			SMTemp6 Temp2 Low			SMTemp6 T2 Low		SM датч 6 темпер 2 низкая		SM6темп2низк
2178		64			15			SMTemp6 Temp3 High 2			SMTemp6 T3 Hi2		SM датч 6 темпер 3 высок уров 2		SM6темп3высок2
2179		64			15			SMTemp6 Temp3 High 1			SMTemp6 T3 Hi1		SM датч 6 темпер 3высок уров 1		SM6темп3высок1
2180		64			15			SMTemp6 Temp3 Low			SMTemp6 T3 Low		SM датч 6 темпер 3 низкая		SM6темп3низк
2181		64			15			SMTemp6 Temp4 High 2			SMTemp6 T4 Hi2		SM датч 6 темпер 4 высок уров 2		SM6темп4высок2
2182		64			15			SMTemp6 Temp4 High 1			SMTemp6 T4 Hi1		SM датч 6 темпер 4высок уров 1		SM6темп4высок1
2183		64			15			SMTemp6 Temp4 Low			SMTemp6 T4 Low		SM датч 6 темпер 4 низкая		SM6темп4низк
2184		64			15			SMTemp6 Temp5 High 2			SMTemp6 T5 Hi2		SM датч 6 темпер 5 высок уров 2		SM6темп5высок2
2185		64			15			SMTemp6 Temp5 High 1			SMTemp6 T5 Hi1		SM датч 6 темпер 5высок уров 1		SM6темп5высок1
2186		64			15			SMTemp6 Temp5 Low			SMTemp6 T5 Low		SM датч 6 темпер 5 низкая		SM6темп5низк
2187		64			15			SMTemp6 Temp6 High 2			SMTemp6 T6 Hi2		SM датч 6 темпер 6 высок уров 2		SM6темп6высок2
2188		64			15			SMTemp6 Temp6 High 1			SMTemp6 T6 Hi1		SM датч 6 темпер 6высок уров 1		SM6темп6высок1
2189		64			15			SMTemp6 Temp6 Low			SMTemp6 T6 Low		SM датч 6 темпер 6 низкая		SM6темп6низк
2190		64			15			SMTemp6 Temp7 High 2			SMTemp6 T7 Hi2		SM датч 6 темпер 7 высок уров 2		SM6темп7высок2
2191		64			15			SMTemp6 Temp7 High 1			SMTemp6 T7 Hi1		SM датч 6 темпер 7высок уров 1		SM6темп7высок1
2192		64			15			SMTemp6 Temp7 Low			SMTemp6 T7 Low		SM датч 6 темпер 7 низкая		SM6темп7низк
2193		64			15			SMTemp6 Temp8 High 2			SMTemp6 T8 Hi2		SM датч 6 темпер 8 высок уров 2		SM6темп8высок2
2194		64			15			SMTemp6 Temp8 High 1			SMTemp6 T8 Hi1		SM датч 6 темпер 8высок уров 1		SM6темп8высок1
2195		64			15			SMTemp6 Temp8 Low			SMTemp6 T8 Low		SM датч 6 темпер 8 низкая		SM6темп8низк
2196		64			15			SMTemp7 Temp1 High 2			SMTemp7 T1 Hi2		SM датч 7 темпер 1 высок уров 2		SM7темп1высок2
2197		64			15			SMTemp7 Temp1 High 1			SMTemp7 T1 Hi1		SM датч 7 темпер 1высок уров 1		SM7темп1высок1
2198		64			15			SMTemp7 Temp1 Low			SMTemp7 T1 Low		SM датч 7 темпер 1 низкая		SM7темп1низк
2199		64			15			SMTemp7 Temp2 High 2			SMTemp7 T2 Hi2		SM датч 7 темпер 2 высок уров 2		SM7темп2высок2
2200		64			15			SMTemp7 Temp2 High 1			SMTemp7 T2 Hi1		SM датч 7 темпер 2высок уров 1		SM7темп2высок1
2201		64			15			SMTemp7 Temp2 Low			SMTemp7 T2 Low		SM датч 7 темпер 2 низкая		SM7темп2низк
2202		64			15			SMTemp7 Temp3 High 2			SMTemp7 T3 Hi2		SM датч 7 темпер 3высок уров 2		SM7темп3высок2
2203		64			15			SMTemp7 Temp3 High 1			SMTemp7 T3 Hi1		SM датч 7 темпер 3высок уров 1		SM7темп3высок1
2204		64			15			SMTemp7 Temp3 Low			SMTemp7 T3 Low		SM датч 7 темпер 3низкая		SM7темп3низк
2205		64			15			SMTemp7 Temp4 High 2			SMTemp7 T4 Hi2		SM датч 7 темпер 4высок уров 2		SM7темп4высок2
2206		64			15			SMTemp7 Temp4 High 1			SMTemp7 T4 Hi1		SM датч 7 темпер 4высок уров 1		SM7темп4высок1
2207		64			15			SMTemp7 Temp4 Low			SMTemp7 T4 Low		SM датч 7 темпер 4низкая		SM7темп4низк
2208		64			15			SMTemp7 Temp5 High 2			SMTemp7 T5 Hi2		SM датч 7 темпер 5высок уров 2		SM7темп5высок2
2209		64			15			SMTemp7 Temp5 High 1			SMTemp7 T5 Hi1		SM датч 7 темпер 5высок уров 1		SM7темп5высок1
2210		64			15			SMTemp7 Temp5 Low			SMTemp7 T5 Low		SM датч 7 темпер 5низкая		SM7темп5низк
2211		64			15			SMTemp7 Temp6 High 2			SMTemp7 T6 Hi2		SM датч 7 темпер 6высок уров 2		SM7темп6высок2
2212		64			15			SMTemp7 Temp6 High 1			SMTemp7 T6 Hi1		SM датч 7 темпер 6высок уров 1		SM7темп6высок1
2213		64			15			SMTemp7 Temp6 Low			SMTemp7 T6 Low		SM датч 7 темпер 6низкая		SM7темп6низк
2214		64			15			SMTemp7 Temp7 High 2			SMTemp7 T7 Hi2		SM датч 7 темпер 7высок уров 2		SM7темп7высок2
2215		64			15			SMTemp7 Temp7 High 1			SMTemp7 T7 Hi1		SM датч 7 темпер 7высок уров 1		SM7темп7высок1
2216		64			15			SMTemp7 Temp7 Low			SMTemp7 T7 Low		SM датч 7 темпер 7низкая		SM7темп7низк
2217		64			15			SMTemp7 Temp8 High 2			SMTemp7 T8 Hi2		SM датч 7 темпер 8высок уров 2		SM7темп8высок2
2218		64			15			SMTemp7 Temp8 High 1			SMTemp7 T8 Hi1		SM датч 7 темпер 8высок уров 1		SM7темп8высок1
2219		64			15			SMTemp7 Temp8 Low			SMTemp7 T8 Low		SM датч 7 темпер 8низкая		SM7темп8низк
2220		64			15			SMTemp8 Temp1 High 2			SMTemp8 T1 Hi2		SM датч 8 темпер 1высок уров 2		SM8темп1высок2
2221		64			15			SMTemp8 Temp1 High 1			SMTemp8 T1 Hi1		SM датч 8темпер 1высок уров 1		SM7темп1высок1
2222		64			15			SMTemp8 Temp1 Low			SMTemp8 T1 Low		SM датч 8 темпер 1низкая		SM8темп1низк
2223		64			15			SMTemp8 Temp2 High 2			SMTemp8 T2 Hi2		SM датч 8 темпер 2высок уров 2		SM8темп2высок2
2224		64			15			SMTemp8 Temp2 High 1			SMTemp8 T2 Hi1		SM датч 8темпер 2высок уров 1		SM7темп2высок1
2225		64			15			SMTemp8 Temp2 Low			SMTemp8 T2 Low		SM датч 8 темпер 2низкая		SM8темп2низк
2226		64			15			SMTemp8 Temp3 High 2			SMTemp8 T3 Hi2		SM датч 8 темпер 3высок уров 2		SM8темп3высок2
2227		64			15			SMTemp8 Temp3 High 1			SMTemp8 T3 Hi1		SM датч 8темпер 3высок уров 1		SM7темп3высок1
2228		64			15			SMTemp8 Temp3 Low			SMTemp8 T3 Low		SM датч 8 темпер 3низкая		SM8темп3низк
2229		64			15			SMTemp8 Temp4 High 2			SMTemp8 T4 Hi2		SM датч 8 темпер 4ысок уров 2		SM8темп4ысок2
2230		64			15			SMTemp8 Temp4 High 1			SMTemp8 T4 Hi1		SM датч 8темпер 4ысок уров 1		SM7темп4ысок1
2231		64			15			SMTemp8 Temp4 Low			SMTemp8 T4 Low		SM датч 8 темпер 4изкая			SM8темп4изк
2232		64			15			SMTemp8 Temp5 High 2			SMTemp8 T5 Hi2		SM датч 8 темпер 5ысок уров 2		SM8темп5ысок2
2233		64			15			SMTemp8 Temp5 High 1			SMTemp8 T5 Hi1		SM датч 8темпер 5ысок уров 1		SM7темп5ысок1
2234		64			15			SMTemp8 Temp5 Low			SMTemp8 T5 Low		SM датч 8 темпер 5изкая			SM8темп5изк
2235		64			15			SMTemp8 Temp6 High 2			SMTemp8 T6 Hi2		SM датч 8 темпер 6ысок уров 2		SM8темп6ысок2
2236		64			15			SMTemp8 Temp6 High 1			SMTemp8 T6 Hi1		SM датч 8темпер 6ысок уров 1		SM7темп6ысок1
2237		64			15			SMTemp8 Temp6 Low			SMTemp8 T6 Low		SM датч 8 темпер 6изкая			SM8темп6изк
2238		64			15			SMTemp8 Temp7 High 2			SMTemp8 T7 Hi2		SM датч 8 темпер 7ысок уров 2		SM8темп7ысок2
2239		64			15			SMTemp8 Temp7 High 1			SMTemp8 T7 Hi1		SM датч 8темпер 7ысок уров 1		SM7темп7ысок1
2240		64			15			SMTemp8 Temp7 Low			SMTemp8 T7 Low		SM датч 8 темпер 7изкая			SM8темп7изк
2241		64			15			SMTemp8 Temp8 High 2			SMTemp8 T8 Hi2		SM датч 8 темпер 8ысок уров 2		SM8темп8ысок2
2242		64			15			SMTemp8 Temp8 High 1			SMTemp8 T8 Hi1		SM датч 8темпер 8ысок уров 1		SM7темп8ысок1
2243		64			15			SMTemp8 Temp8 Low			SMTemp8 T8 Low		SM датч 8 темпер 8изкая			SM8темп8изк
2244		32			15			None					None			Нет					Нет
2245		32			15			High Load Level1			HighLoadLevel1		Высокая загрузка уровень 1		ВысокЗагрузка1
2246		32			15			High Load Level2			HighLoadLevel2		Высокая загрузка уровень 2		ВысокЗагрузка2
2247		32			15			Maximum					Maximum			Максимум				Максимум
2248		32			15			Average					Average			Средняя					Средняя
2249		32			15			Solar Mode				Solar Mode		Режим солнечных преобразоват		РежСолнПреобр
2250		32			15			Running Way(For Solar)			Running Way		Информ о работе (солнеч преобр)		Инф(СолнПреобр)
2251		32			15			Disabled				Disabled		Отключить				Отключить
2252		64			15			RECT-SOLAR				RECT-SOLAR		Выпрямитель-солнечн преобразов		Выпр-СолнПреобр
2253		64			15			SOLAR					SOLAR			Солнечн преобразов			СолнПреобр
2254		32			15			RECT First				RECT First		Начало работы от выпрямителей		РабОтВыпрям
2255		32			15			Solar First				Solar First		Начало работы от солн преобраз		РабОтСолнПреобр
2256		32			15			Please reboot after changing		Please reboot		Перезагруз для принятия парам		ПерзагрПараметр
2257		32			15			CSU Failure				CSU Failure		Неисправность CSU			НеиспрвнCSU
2258		32			15			SSL and SNMPV3				SSL and SNMPV3		SSL и SNMPV3				SSLиSNMPV3
2259		32			15			Adjust Bus Input Voltage		Adjust In-Volt		Регулировка вход напряжения		РегулВходНапр
2260		32			15			Confirm Voltage Supplied		Confirm Voltage		Подтвержд напряж вход сети		ПодтвНапрВхода
2261		64			15			Converter Only				Converter Only		Только преобразователь			ТолькоПреобраз
2262		32			15			Net-Port Reset Interval			Reset Interval		Частота сброса сетевого порта		ЧастСброса
2263		32			15			DC1 Load				DC1 Load		Нагрузка DC1				НагрузDC1
2264		32			15			DI9					DI9			ЦифВх 9					ЦифВх 9
2265		32			15			DI10					DI10			ЦифВх 10				ЦифВх 10
2266		32			15			DI11					DI11			ЦифВх 11				ЦифВх 11
2267		32			15			DI12					DI12			ЦифВх 12				ЦифВх 12
2268		32			15			Digital Input 9				DI 9			Цифровой вход 9				ЦифВх9
2269		32			15			Digital Input 10			DI 10			Цифровой вход 10			ЦифВх10
2270		32			15			Digital Input 11			DI 11			Цифровой вход 11			ЦифВх11
2271		32			15			Digital Input 12			DI 12			Цифровой вход 12			ЦифВх12
2272		32			15			DI9 Alarm State				DI9 Alm State		Цифров вход 9 состояние аварии		ЦифВх9авария
2273		32			15			DI10 Alarm State			DI10 Alm State		Цифров вход 10состояние аварии		ЦифВх10авария
2274		32			15			DI11 Alarm State			DI11 Alm State		Цифров вход 11состояние аварии		ЦифВх11авария
2275		32			15			DI12 Alarm State			DI12 Alm State		Цифров вход 12состояние аварии		ЦифВх12авария
2276		32			15			DI9 Alarm				DI9 Alarm		ЦифВх9авария				ЦифВх9
2277		32			15			DI10 Alarm				DI10 Alarm		ЦифВх10 авария				ЦифВх10
2278		32			15			DI11 Alarm				DI11 Alarm		ЦифВх11 авария				ЦифВх11
2279		32			15			DI12 Alarm				DI12 Alarm		ЦифВх12 авария				ЦифВх12
2280		32			15			None					None			нет					нет
2281		32			15			Exist					Exist			Присутствует				Присутствует
2282		32			15			IB01 State				IB01 State		IB01 состояние				IB01 статус
2283		32			15			Relay Output 14				Relay Output 14		Релейный выход 14			Рел Вых 14
2284		32			15			Relay Output 15				Relay Output 15		Релейный выход 15			Рел Вых 15
2285		32			15			Relay Output 16				Relay Output 16		Релейный выход 16			Рел Вых 16
2286		32			15			Relay Output 17				Relay Output 17		Релейный выход 17			Рел Вых 17
2287		32			15			Time Display Format			Time Format		Формат времени				Формат Время
2288		32			15			EMEA					EMEA			EMEA					EMEA
2289		32			15			NA					NA			NA					NA
2290		32			15			CN					CN			CN					CN
2291		32			15			Help					Help			Справка					Справка
2292		32			15			Current Protocol Type			Protocol		Тип протокола				протокол
2293		32			15			EEM					EEM			EEM					EEM
2294		32			15			YDN23					YDN23			YDN23					YDN23
2295		32			15			Modbus					ModBus			ModBus					ModBus
2296		32			15			SMS Alarm Level				SMS Alarm Level		SMS авария уровень			SMS авар уров
2297		32			15			Disabled				Disabled		запрещен				запрещен
2298		32			15			Observation				Observation		обзор					обзор
2299		32			15			Major					Major			тяжелый					тяж
2300		32			15			Critical				Critical		критический				критический
2301		64			64			SPD is not connected to system or SPD is broken.	SPD is not connected to system or SPD is broken.	SPD отсутствует				SPD нет
2302		32			15			Big Screen				Big Screen		Болльшой экран				Болльшой экран
2303		32			15			EMAIL Alarm Level			EMAIL Alarm Level	EMAIL авария уровень			EMAIL авария уровень
2304		32			15			HLMS Protocol Type			Protocol Type		HMLS протокол				HMLS проток
2305		32			15			EEM					EEM			EEM					EEM
2306		32			15			YDN23					YDN23			YDN23					YDN23
2307		32			15			Modbus					Modbus			Modbus					Modbus
2308		32			15			24V Display State			24V Display		24 вольт дисплей			24V дисплей
2309		64			15			Syst Volt Level				Syst Volt Level		Системное напряжение уровень		напряжение
2310		32			15			48 V System				48 V System		48 вольтовая система			48V система
2311		32			15			24 V System				24 V System		24 вольтовая система			24V система
2312		32			15			Power Input Type			Power Input Type	тип входной мощности			тип ввода
2313		32			15			AC Input				AC Input		Горсеть ввод				Горсеть ввод
2314		32			15			Diesel Input				Diesel Input		Дизель ввод				Дизель ввод
2315		32			15			2nd IB2 Temp1				2nd IB2 Temp1		IB2 термометр 1				IB2 терм 1
2316		32			15			2nd IB2 Temp2				2nd IB2 Temp2		IB2 термометр 2				IB2 терм 2
2317		32			15			EIB2 Temp1				EIB2 Temp1		EIB2 термометр 1			EIB2 терм 1
2318		32			15			EIB2 Temp2				EIB2 Temp2		EIB2 термометр 2			EIB2 терм 2
2319		32			15			2nd IB2 Temp1 High 2			2nd IB2 T1 Hi2		IB2 высокая2температура 1		IB2 высок2темп 1
2320		32			15			2nd IB2 Temp1 High 1			2nd IB2 T1 Hi1		IB2 высокая1температура 1		IB2 высок1темп 1
2321		32			15			2nd IB2 Temp1 Low			2nd IB2 T1 Low		IB2 низкая температура 1		IB2 низк темп 1
2322		32			15			2nd IB2 Temp2 High 2			2nd IB2 T2 Hi2		IB2 высокая2температура 2		IB2 высок2темп 2
2323		32			15			2nd IB2 Temp2 High 1			2nd IB2 T2 Hi1		IB2 высокая1температура 2		IB2 высок1темп 2
2324		32			15			2nd IB2 Temp2 Low			2nd IB2 T2 Low		IB2 низкая температура 2		IB2 низк темп 2
2325		32			15			EIB2 Temp1 High 2			EIB2 T1 Hi2		EIB2 высокая2температура 1		EIB2 высок2темп 1
2326		32			15			EIB2 Temp1 High 1			EIB2 T1 Hi1		EIB2 высокая1температура 1		EIB2 высок1темп 1
2327		32			15			EIB2 Temp1 Low				EIB2 T1 Low		EIB2 низкая температура 2		EIB2 низк темп 2
2328		32			15			EIB2 Temp2 High 2			EIB2 T2 Hi2		EIB2 высокая2температура 2		EIB2 высок2темп 2
2329		32			15			EIB2 Temp2 High 1			EIB2 T2 Hi1		EIB2 высокая1температура 2		EIB2 высок1темп 2
2330		32			15			EIB2 Temp2 Low				EIB2 T2 Low		EIB2 низкая температура 2		EIB2 низк темп 2
2331		32			15			Testing Relay 14			Testing Relay14		Тест реле 14				Тест реле 14
2332		32			15			Testing Relay 15			Testing Relay15		Тест реле 15				Тест реле 15
2333		32			15			Testing Relay 16			Testing Relay16		Тест реле 16				Тест реле 16
2334		32			15			Testing Relay 17			Testing Relay17		Тест реле 17				Тест реле 17
2335		32			15			Total Output Rated			Total Output Rated	Полная оценка выхода			ПолнОценкВых
2336		32			15			Load current capacity			Load capacity		Значение тока нагрузки			ЗначТокаНагр
2337		64			15			EES System Mode				EES System Mode		EES системный режим			EESCистРежим
2338		32			15			SMS Modem Fail				SMS Modem Fail		SMS модема сбой				SMSМодмСбой
2339		32			15			SMDU-EIB Mode				SMDU-EIB Mode		SMDU-EIB режим				SMDU-EIBРежм
2340		64			15			Load Switch				Load Switch		Переключататель нагрузки		ПереключатНагр
2341		32			15			Manual State				Manual State		Ручное состояние			РучнСост
2342		64			15			Converter Voltage Level			Volt Level		Преобразователь уровня напряжения	ПреобрУровНапр
2343		32			15			CB Threshold Value			Threshold Value		CB порог срабатывания			CBПорог
2344		32			15			CB Overload Value			Overload Value		СВ значение перегрузки			CBПерегруз
2345		64			15			SNMP Config Error			SNMP Config Error	SNMP ошибка конфигураци			SNMP ошибка конфигураци
2346		128			15			Cab X Num(Valid After Reset)		Cabinet X Num		Кабина X Нет (Действует После сброса)		Кабинет X Num
2347		128			15			Cab Y Num(Valid After Reset)		Cabinet Y Num		Кабина Y Нет (Действует После сброса)		Кабинет Y Num
2348		32			15			CB Threshold Power			Threshold Power		CB пороговая мощность			пороговая мощность
2349		32			15			CB Overload Power			Overload Power		CB от перегрузки Мощность			от перегрузки Мощность
2350		32			15			With FCUP				With FCUP		С FCUP				С FCUP
2351		32			15			FCUP Existence State			FCUP Exist State	FCUP существования государства			FCUP Существовать государство
2356		32			15			DHCP Enable				DHCP Enable		DHCP Включено							DHCP Включено
2357		64			15			DO1 Normal State  			DO1 Normal		DO1 Нормальное состояние				DO1 Нормальный
2358		64			15			DO2 Normal State  			DO2 Normal		DO2 Нормальное состояние				DO2 Нормальный
2359		64			15			DO3 Normal State  			DO3 Normal		DO3 Нормальное состояние				DO3 Нормальный
2360		64			15			DO4 Normal State  			DO4 Normal		DO4 Нормальное состояние				DO4 Нормальный
2361		64			15			DO5 Normal State  			DO5 Normal		DO5 Нормальное состояние				DO5 Нормальный
2362		64			15			DO6 Normal State  			DO6 Normal		DO6 Нормальное состояние				DO6 Нормальный
2363		64			15			DO7 Normal State  			DO7 Normal		DO7 Нормальное состояние				DO7 Нормальный
2364		64			15			DO8 Normal State  			DO8 Normal		DO8 Нормальное состояние				DO8 Нормальный
2365		32			15			Non-Energized				Non-Energized		Non-энергией			Non-энергией
2366		32			15			Energized				Energized		энергией					энергией
2367		64			15			DO14 Normal State  			DO14 Normal		DO14 Нормальное состояние				DO14 Нормальный
2368		64			15			DO15 Normal State  			DO15 Normal		DO15 Нормальное состояние				DO15 Нормальный
2369		64			15			DO16 Normal State  			DO16 Normal		DO16 Нормальное состояние				DO16 Нормальный
2370		64			15			DO17 Normal State  			DO17 Normal		DO17 Нормальное состояние				DO17 Нормальный
2371		32			15			SSH Enabled  			        SSH Enabled		SSH Включить					SSH Включить
2372		32			15			Disabled				Disabled		Не включен					Не включен
2373		32			15			Enabled					Enabled			Включить					Включить
2374		32			15			Page Type For Login			Page Type		Page Type For Login			Page Type
2375		32			15			Standard				Standard		Standard				Standard
2376		32			15			For BT					For BT			For BT					For BT	
2377		32		15		Fiamm Battery		Fiamm Battery		Батарея Fiamm	Батарея Fiamm
2378		32			15			Correct Temperature1			Correct Temp1			Correct Temperature1			Correct Temp1
2379		32			15			Correct Temperature2			Correct Temp2			Correct Temperature2			Correct Temp2

2503		32			15			NTP Function Enable			NTPFuncEnable		NTP Enable				NTP Enable
2504		32			15			SW_Switch1			SW_Switch1			SW_Switch1				SW_Switch1	
2505		32			15			SW_Switch2			SW_Switch2			SW_Switch2				SW_Switch2	
2506		32			15			SW_Switch3			SW_Switch3			SW_Switch3				SW_Switch3	
2507		32			15			SW_Switch4			SW_Switch4			SW_Switch4				SW_Switch4	
2508		32			15			SW_Switch5			SW_Switch5			SW_Switch5				SW_Switch5	
2509		32			15			SW_Switch6			SW_Switch6			SW_Switch6				SW_Switch6	
2510		32			15			SW_Switch7			SW_Switch7			SW_Switch7				SW_Switch7	
2511		32			15			SW_Switch8			SW_Switch8			SW_Switch8				SW_Switch8	
2512		32			15			SW_Switch9			SW_Switch9			SW_Switch9				SW_Switch9	
2513		32			15			SW_Switch10			SW_Switch10			SW_Switch10				SW_Switch10	
