﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Fuse 1					Fuse 1			Пред1					Пред1
2		32			15			Fuse 2					Fuse 2			Пред2					Пред2
3		32			15			Fuse 3					Fuse 3			Пред3					Пред3
4		32			15			Fuse 4					Fuse 4			Пред4					Пред4
5		32			15			Fuse 5					Fuse 5			Пред5					Пред5
6		32			15			Fuse 6					Fuse 6			Пред6					Пред6
7		32			15			Fuse 7					Fuse 7			Пред7					Пред7
8		32			15			Fuse 8					Fuse 8			Пред8					Пред8
9		32			15			Fuse 9					Fuse 9			Пред9					Пред9
10		32			15			Fuse 10					Fuse 10			Пред10					Пред10
11		32			15			Fuse 11					Fuse 11			Пред11					Пред11
12		32			15			Fuse 12					Fuse 12			Пред12					Пред12
13		32			15			Fuse 13					Fuse 13			Пред13					Пред13
14		32			15			Fuse 14					Fuse 14			Пред14					Пред14
15		32			15			Fuse 15					Fuse 15			Пред15					Пред15
16		32			15			Fuse 16					Fuse 16			Пред16					Пред16
17		32			15			SMDUP7 DC Fuse				SMDUP7 DC Fuse		SMDUP7 DCПред				SMDUP7 DCПред
18		32			15			State					State			Статус					Статус
19		32			15			Off					Off			Выкл					Выкл
20		32			15			On					On			Вкл					Вкл
21		32			15			Fuse 1 Alarm				DC Fuse 1 Alarm		Пред1Авар				Пред1Авар
22		32			15			Fuse 2 Alarm				DC Fuse 2 Alarm		Пред2Авар				Пред2Авар
23		32			15			Fuse 3 Alarm				DC Fuse 3 Alarm		Пред3Авар				Пред3Авар
24		32			15			Fuse 4 Alarm				DC Fuse 4 Alarm		Пред4Авар				Пред4Авар
25		32			15			Fuse 5 Alarm				DC Fuse 5 Alarm		Пред5Авар				Пред5Авар
26		32			15			Fuse 6 Alarm				DC Fuse 6 Alarm		Пред6Авар				Пред6Авар
27		32			15			Fuse 7 Alarm				DC Fuse 7 Alarm		Пред7Авар				Пред7Авар
28		32			15			Fuse 8 Alarm				DC Fuse 8 Alarm		Пред8Авар				Пред8Авар
29		32			15			Fuse 9 Alarm				DC Fuse 9 Alarm		Пред9Авар				Пред9Авар
30		32			15			Fuse 10 Alarm				DCFuse 10 Alarm		Пред10Авар				Пред10Авар
31		32			15			Fuse 11 Alarm				DCFuse 11 Alarm		Пред11Авар				Пред11Авар
32		32			15			Fuse 12 Alarm				DCFuse 12 Alarm		Пред12Авар				Пред12Авар
33		32			15			Fuse 13 Alarm				DCFuse 13 Alarm		Пред13Авар				Пред13Авар
34		32			15			Fuse 14 Alarm				DCFuse 14 Alarm		Пред14Авар				Пред14Авар
35		32			15			Fuse 15 Alarm				DCFuse 15 Alarm		Пред15Авар				Пред15Авар
36		32			15			Fuse 16 Alarm				DCFuse 16 Alarm		Пред16Авар				Пред16Авар
37		32			15			Interrupt Times				Interrupt Times		КолвоОбрывов				КолвоОбрывов
38		32			15			Commnication Interrupt			Comm Interrupt		ОбрывСвязи				ОбрывСвязи
39		32			15			Fuse 17					Fuse 17			Пред17					Пред17
40		32			15			Fuse 18					Fuse 18			Пред18					Пред18
41		32			15			Fuse 19					Fuse 19			Пред19					Пред19
42		32			15			Fuse 20					Fuse 20			Пред20					Пред20
43		32			15			Fuse 21					Fuse 21			Пред21					Пред21
44		32			15			Fuse 22					Fuse 22			Пред22					Пред22
45		32			15			Fuse 23					Fuse 23			Пред23					Пред23
46		32			15			Fuse 24					Fuse 24			Пред24					Пред24
47		32			15			Fuse 25					Fuse 25			Пред25					Пред25
48		32			15			Fuse 17 Alarm				DCFuse 17 Alarm		Пред17Авар				Пред17Авар
49		32			15			Fuse 18 Alarm				DCFuse 18 Alarm		Пред18Авар				Пред18Авар
50		32			15			Fuse 19 Alarm				DCFuse 19 Alarm		Пред19Авар				Пред19Авар
51		32			15			Fuse 20 Alarm				DCFuse 20 Alarm		Пред20Авар				Пред20Авар
52		32			15			Fuse 21 Alarm				DCFuse 21 Alarm		Пред21Авар				Пред21Авар
53		32			15			Fuse 22 Alarm				DCFuse 22 Alarm		Пред22Авар				Пред22Авар
54		32			15			Fuse 23 Alarm				DCFuse 23 Alarm		Пред23Авар				Пред23Авар
55		32			15			Fuse 24 Alarm				DCFuse 24 Alarm		Пред24Авар				Пред24Авар
56		32			15			Fuse 25 Alarm				DCFuse 25 Alarm		Пред25Авар				Пред25Авар
