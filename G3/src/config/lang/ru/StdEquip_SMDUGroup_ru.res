﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			SMDU Group				SMDU Group		SMDU группа				SMDU группа
2		32			15			Stand-to				Stand-to		Stand-to				Stand-to
3		32			15			Refresh					Refresh			Обновить				Обновить
4		32			15			Setting Refresh				Setting Refresh		НастрОбновл				НастрОбновл
5		32			15			E-Stop Function				E-Stop			E-Stop Функция				E-Stop
6		32			15			yes					yes			да					да
7		32			15			Existence State				Existence State		Наличие					наличие
8		32			15			Existent				Existent		Есть					ЕСть
9		32			15			Not Existent				Not Existent		нет					нет
10		32			15			SM_DUs Number				SM_DUs Num		SM-DUномер				SM-DUномер
11		32			15			SM_DU Config Changed			Cfg Changed		SMDUконфиг				SMDUконф
12		32			15			Not Changed				Not Changed		НеИзменен				НеИзменен
13		32			15			Changed					Changed			Изменен					Изменен
