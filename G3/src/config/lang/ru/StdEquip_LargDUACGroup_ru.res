﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			AC Distribution Num			AC Distri Num		АСраспредНомер				АСраспNo
2		32			15			LargeDU AC Distri Group			ACD Group		LargDU					РаспределАС
3		32			15			Over Voltage Limit			Over Volt Limit		ВысНапрПорог				ВысНапр
4		32			15			Under Voltage Limit			Under Volt Limit	НизкНапрПорог				НизкНапр
5		32			15			Phase Failure Voltage			Phase Fail Volt		ФазноеНапр				ФазнНапр
6		32			15			Over Frequency Limit			Over Freq Limit		ВысЧастотаПорог				ВысЧастота
7		32			15			Under Frequency Limit			Under Freq Limit	НизкЧастоаПорог				НизкЧастота
8		32			15			Mains Failure				Mains Failure		НетСети					НетСети
9		32			15			Noraml					Normal			Норма					Норма
10		32			15			Alarm					Alarm			Авария					Авария
11		32			15			Mains Failure				Mains Failure		НетСети					НетСети
12		32			15			Existence State				Existence State		Наличие					Наличие
13		32			15			Existent				Existent		Есть					Есть
14		32			15			Not Existent				Not Existent		Нет					Нет
