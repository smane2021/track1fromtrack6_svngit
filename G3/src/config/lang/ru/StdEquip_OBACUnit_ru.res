﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Phase A Voltage				Phase A Volt		ФазаAНапр				ФазаAНапр
2		32			15			Phase B Voltage				Phase B Volt		ФазаВНапр				ФазаВНапр
3		32			15			Phase C Voltage				Phase C Volt		ФазаСНапр				ФазаСНапр
4		32			15			Line AB Voltage				Line AB Volt		ABнапр					ABнапр
5		32			15			Line BC Voltage				Line BC Volt		BCнапр					BCнапр
6		32			15			Line CA Voltage				Line CA Volt		CAнапр					CAнапр
7		32			15			Phase A Current				Phase A Curr		ТокФазаА				ТокФазаА
8		32			15			Phase B Current				Phase B Curr		ТокФазаВ				ТокФазаВ
9		32			15			Phase C Current				Phase C Curr		ТокФазаС				ТокФазаС
10		32			15			Frequency				AC Frequency		Частота					Частота
11		32			15			Total Real Power			Total RealPower		ПолнМощн				ПолнМощн
12		32			15			Phase A Real Power			Real Power A		ПолнМощн.фА				ПолнМощн.фА
13		32			15			Phase B Real Power			Real Power B		ПолнМощн.фB				ПолнМощн.фВ
14		32			15			Phase C Real Power			Real Power C		ПолнМощн.фС				ПолнМощн.фС
15		32			15			Total Reactive Power			Tot React Power		РеактМощн				РектМощн
16		32			15			Phase A Reactive Power			React Power A		РеактМощн.фА				РеактМощн.фА
17		32			15			Phase B Reactive Power			React Power B		РеактМощн.фВ				РеактМощн.фВ
18		32			15			Phase C Reactive Power			React Power C		РеактМощн.фС				РеактМощн.фС
19		32			15			Total Apparent Power			Total App Power		АктивМощн				АктивМощн
20		32			15			Phase A Apparent Power			App Power A		АктивМощн.фА				АктивМощн.фА
21		32			15			Phase B Apparent Power			App Power B		АктивМощн.фВ				АктивМощн.фВ
22		32			15			Phase C Apparent Power			App Power C		АктивМощн.фС				АктивМощн.фС
23		32			15			Power Factor				Power Factor		КоэфМощн				КоэфМощн
24		32			15			Phase A Power Factor			Power Factor A		КоэфМощн.фА				КоэфМощн.фА
25		32			15			Phase B Power Factor			Power Factor B		КоэфМощн.фВ				КоэфМощн.фВ
26		32			15			Phase C Power Factor			Power Factor C		КоэфМощн.фС				КоэфМощн.фС
27		32			15			Phase A Current Crest Factor		Ia Crest Factor		IaКрестфакт				IaКрестФакт
28		32			15			Phase B Current Crest Factor		Ib Crest Factor		IbКрестФакт				IbКрестФакт
29		32			15			Phase C Current Crest Factor		Ic Crest Factor		IcКрестФакт				IcКрестФакт
30		32			15			Phase A Current THD			Current THD A		THDТок фА				THDТок фА
31		32			15			Phase B Current THD			Current THD B		THD Ток фВ				THD ТокфВ
32		32			15			Phase C Current THD			Current THD C		THD Ток фС				THD ТокфС
33		32			15			Phase A Voltage THD			Voltage THD A		фAНапрTHD				фAНапрTHD
34		32			15			Phase A Voltage THD			Voltage THD B		фBНапрTHD				фBНапрTHD
35		32			15			Phase A Voltage THD			Voltage THD C		фCНапрTHD				фCНапрTHD
36		32			15			Total Real Energy			Tot Real Energy		ПолнЭнергия				ПолнЭнерг
37		32			15			Total Reactive Energy			Tot ReactEnergy		РеактЭнергия				РеактЭнерг
38		32			15			Total Apparent Energy			Tot App Energy		АктивЭнергия				АктивЭнерг
39		32			15			Ambient Temperature			Ambient Temp		ОкрТемп					ОкрТемп
40		32			15			Nominal Line Voltage			Nominal L-Volt		НомЛинейнНапр				НомЛинНапр
41		32			15			Nominal Phase Voltage			Nominal PH-Volt		НомФазнНапр				НомФазнНапр
42		32			15			Nominal Frequency			Nomi Frequency		НомЧастота				НомЧастота
43		32			15			Mains Failure Alarm Threshold 1		Volt Threshold1		НетСетиПорог1				НетСетиПорог1
44		32			15			Mains Failure Alarm Threshold 2		Volt Threshold2		НетСетиПорог2				НетСетиПорог2
45		32			15			Voltage Alarm Threshold 1		Volt Alm Trld 1		НапрАварПорог1				НапрАварПорог1
46		32			15			Voltage Alarm Threshold 2		Volt Alm Trld 2		НапрАварПорог2				НапрАварПорог2
47		32			15			Frequency Alarm Threshold		Freq Alarm Trld		ЧастотаАварПорог			ЧастотаПорог
48		32			15			High Temperature Limit			High Temp Limit		ВысТемпЛимит				ВысТемпЛимит
49		32			15			Low Temperature Limit			Low Temp Limit		НизкТемпЛимит				НизкТемпЛимит
50		32			15			Supervision Fail			Supervision Fail	БлокУпрНеиспр				БлокУпрНеиспр
51		32			15			Line AB Over Voltage 1			L-AB Over Volt1		L-ABПеренапр1				L-ABПеренапр1
52		32			15			Line AB Over Voltage 2			L-AB Over Volt2		L-ABПеренапр2				L-ABПеренапр2
53		32			15			Line AB Under Voltage 1			L-AB UnderVolt1		L-ABНизкНапр1				L-ABНизкНапр1
54		32			15			Line AB Under Voltage 2			L-AB UnderVolt2		L-ABНизкНапр2				L-ABНизкНапр2
55		32			15			Line BC Over Voltage 1			L-BC Over Volt1		L-BCПеренапр1				L-BCПеренапр1
56		32			15			Line BC Over Voltage 2			L-BC Over Volt2		L-BCПеренапр2				L-BCПеренапр2
57		32			15			Line BC Under Voltage 1			L-BC UnderVolt1		L-BCНизкНапр1				L-BCНизкНапр1
58		32			15			Line BC Under Voltage 2			L-BC UnderVolt2		L-BCНизкНапр2				L-BCНизкНапр2
59		32			15			Line CA Over Voltage 1			L-CA Over Volt1		L-CAПеренапр1				L-CAПеренапр1
60		32			15			Line CA Over Voltage 2			L-CA Over Volt2		L-CAПеренапр2				L-CAПеренапр2
61		32			15			Line CA Under Voltage 1			L-CA UnderVolt1		L-CAНизкНапр1				L-CAНизкНапр1
62		32			15			Line CA Under Voltage 2			L-CA UnderVolt2		L-CAНизкНапр2				L-CAНизкНапр2
63		32			15			Phase A Over Voltage 1			PH-A Over Volt1		фAПеренапр1				фAПеренапр1
64		32			15			Phase A Over Voltage 2			PH-A Over Volt2		фAПеренапр2				фAПеренапр2
65		32			15			Phase A Under Voltage 1			PH-A UnderVolt1		фAНизкНапр1				AНизкНапр1
66		32			15			Phase A Under Voltage 2			PH-A UnderVolt2		фAНизкНапр2				фAНизкНапр2
67		32			15			Phase B Over Voltage 1			PH-B Over Volt1		BПеренапр1				BПеренапр1
68		32			15			Phase B Over Voltage 2			PH-B Over Volt2		фВПеренапр2				фВПеренапр2
69		32			15			Phase B Under Voltage 1			PH-B UnderVolt1		BНизкНапр1				BНизкНапр1
70		32			15			Phase B Under Voltage 2			PH-B UnderVolt2		фBНизкНапр2				фBНизкНапр2
71		32			15			Phase C Over Voltage 1			PH-C Over Volt1		фCПеренапр1				фCПеренапр1
72		32			15			Phase C Over Voltage 2			PH-C Over Volt2		фCПеренапр2				фCПеренапр2
73		32			15			Phase C Under Voltage 1			PH-C UnderVolt1		фCНизкНапр1				фCНизкНапр1
74		32			15			Phase C Under Voltage 2			PH-C UnderVolt2		фCНизкНапр2				фCНизкНапр2
75		32			15			Mains Failure				Mains Failure		НетСети					нетСети
76		32			15			Severe Mains Failure			SevereMainsFail		КритНетСети				КритНетСети
77		32			15			High Frequency				High Frequency		ВысЧастота				ВысЧастота
78		32			15			Low Frequency				Low Frequency		НизкЧастота				НизкЧастота
79		32			15			High Temperature			High Temp		ВысТемп					ВысТемп
80		32			15			Low Temperature				Low Temperature		НизкТемп				НизкТемп
81		32			15			OB AC Unit				OB AC Unit		OB ACблок				OB ACблок
82		32			15			Supervision Fail			Supervision Fail	БлокУпрНеиспр				БлокУпрНеиспр
83		32			15			no					No			нет					нет
84		32			15			yes					Yes			Да					Да
85		32			15			Phase A Mains Failure Counter		A Mains Fail Cnt	СчетНетФазыA				СчетНетФазыA
86		32			15			Phase B Mains Failure Counter		B Mains Fail Cnt	СчетНетФазыВ				СчетНетФазыВ
87		32			15			Phase C Mains Failure Counter		C Mains Fail Cnt	СчетНетФазыС				СчетНетФазыС
88		32			15			Frequency Failure Counter		F Fail Cnt		СчетЧастотаАвар				СчетАварЧастота
89		32			15			Reset Phase A Mains Fail Counter	Reset A FailCnt		СбросСчетНетФазыA			СбросСчетНетФазыA
90		32			15			Reset Phase B Mains Fail Counter	Reset B FailCnt		СбросСчетНетФазыВ			СбросСчетНетФазыВ
91		32			15			Reset Phase C Mains Fail Counter	Reset C FailCnt		СбросСчетНетФазыВ			СбросСчетНетФазыВ
92		32			15			Reset Frequency Counter			Reset F FailCnt		СбросСчетЧастота			СбросСчетЧастота
93		32			15			Current Alarm Threshold			Curr Alarm Limit	ТокАварПорог				ТокАварПорог
94		32			15			Phase A High Current			A High Current		ВысТок фА				ВысТок фА
95		32			15			Phase B High Current			B High Current		ВысТок фВ				ВысТок фВ
96		32			15			Phase C High Current			C High Current		ВысТок фС				ВысТок фС
97		32			15			State					State			Статус					Статус
98		32			15			off					off			Выкл					Выкл
99		32			15			on					on			вкл					вкл
100		32			15			State					State			Статус					Статус
101		32			15			Existent				Existent		есть					есть
102		32			15			Not Existent				Not Existent		нет					нет
103		32			15			AC Type					AC Type			ТипАС					ТипАС
104		32			15			None					None			нет					нет
105		32			15			Single Phase				Single Phase		1фаза					1фаза
106		32			15			Three Phases				Three Phases		3фазы					3фазы
107		32			15			Mains Failure(Single)			Mains Failure		НетСети(Single)				НетСети(Single)
108		32			15			Severe Mains Failure(Single)		SevereMainsFail		КритНетСети(Single)			КритНетСети
