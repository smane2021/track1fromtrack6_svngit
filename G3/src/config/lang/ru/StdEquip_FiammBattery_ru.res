﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE			
1	32			15			Battery Current				Batt Current		ток батареи		ток батареи			
2	32			15			Battery Rating (Ah)     		Batt Rating(Ah)		Батарея Рейтинг(Ah)	Бат Рейт(Ah)			
3	32			15			Bus Voltage				Bus Voltage		Автобус Напряжение		Авт Напр		
5	32			15			Battery Over Current			Batt Over Curr		Батарея сверхтока		Бат сверхтока		
6	32			15			Battery Capacity (%)			Batt Cap (%)		Емкость батареи(%)		Емк батареи(%)		
7	32			15			Battery Voltage				Batt Voltage		Напряжение батареи		Напр бат		
18	32			15			SoNick Battery				SoNick Batt		SoNickбатарея		SoNickбатарея		
29	32			15			Yes					Yes			да			да
30	32			15			No					No			нет			нет
31	32			15			On					On			на			на
32	32			15			Off					Off			от			от
33	32			15			State					State			состояние	состояние
87	32			15			No					No			нет			нет	
96	32			15			Rated Capacity				Rated Capacity		номин мощность		номин мощн	
99	32			15			Battery Temperature(AVE)		Batt Temp(AVE)		Темп батарея		Темп бат
100	32			15			Board Temperature			Board Temp		темп форума		темп форума
101	32			15			Tc Center Temperature			Tc Center Temp		Tc темп Центр		Tc темп Центр
102	32			15			Tc Left Temperature			Tc Left Temp		Tcлевые температуры		Tc левые темп
103	32			15			Tc Right Temperature			Tc Right Temp		Tcправый температуры		Tc правыйтемп
114	32			15			Battery Communication Fail		Batt Comm Fail		батарея связи Сбой		батсвязиСбой
129	32			15			Low Ambient Temperature			Low Amb Temp		Низкая Темп воздуха		НизкТемпвозд
130	32			15			High Ambient Temperature Warning	High Amb Temp W 	Высокая Темп воз Вним		ВысТемпвозВн		
131	32			15			High Ambient Temperature		High Amb Temp		Высокая Темп воз		ВысТемпвоз	
132	32			15			Low Battery Internal Temperature	Low Int Temp		Разряд бат вну темп		Разбатвнутемп
133	32			15			High Batt Internal Temp Warning		Hi Int Temp W		Выс Темп Вним Батт Вну		ВысТемВнБатВну	
134	32			15			High Batt Internal Temperature		Hi Bat Int Temp		Выс Темп Батт Вну		ВысТемБатВну	
135	32			15			Bus Voltage Below 40V			Bus V Below 40V		Автобус Напр Ниже40V		АвтНапрНиже40V	
136	32			15			Bus Voltage Below 39V			Bus V Below 39V		Автобус Напр Ниже39V		АвтНапрНиже39V	
137	32			15			Bus Voltage Above 60V			Bus V Above 60V		Автобус Напр выше60V		АвтНапрвыше60V	
138	32			15			Bus Voltage Above 65V			Bus V Above 65V		Автобус Напр выше65V		АвтНапрвыше65V	
139	32			15			High Discharge Current Warning		Hi Disch Curr W		Высокий раз ток Вни		ВыразтокВни
140	32			15			High Discharge Current			High Disch Curr		Высокий раз ток		Выразток
141	32			15			Main Switch Error			Main Switch Err		Основная ошибка перек		Осноошибкапере
142	32			15			Fuse Blown				Fuse Blown		Перегорел предох			Перег предох
143	32			15			Heaters Failure				Heaters Failure		обогреватели провал		обогр провал
144	32			15			Thermocouple Failure			Thermocple Fail		Термопара провал		Терм провал
145	32			15			Voltage Measurement Circuit Fail	V M Cir Fail		Измер напр цепи пр	Измнапрцепипр
146	32			15			Current Measurement Circuit Fail	C M Cir Fail		Измер тока цепи пр	Изметокцепипр
147	32			15			BMS Hardware Failure			BMS Hdw Failure		BMS Аппаратный сбой	BMS Аппа сбой
148	32			15			Hardware Protection Sys Active		HdW Protect Sys		Аппа Актив защита	АппАктивзащ
149	32			15			High Heatsink Temperature		Hi Heatsink Tmp		Высокие темп ради		Выстемпради
150	32			15			Battery Voltage Below 39V		Bat V Below 39V		Напр бат ниже39V		Напрбатниже39V
151	32			15			Battery Voltage Below 38V		Bat V Below 38V		Напр бат ниже38V		Напрбатниже38V
152	32			15			Battery Voltage Above 53.5V		Bat V Abv 53.5V		Напр бат выше53.5V	Напрбатвы53.5V
153	32			15			Battery Voltage Above 53.6V		Bat V Abv 53.6V		Напр бат выше53.6V	Напрбатвы53.6V
154	32			15			High Charge Current Warning		Hi Chrge Curr W		Высо ток заряда Вн		ВытокзарядаВн
155	32			15			High Charge Current			Hi Charge Curr		Высо ток заряда		Вытокзаряда
156	32			15			High Discharge Current Warning		Hi Disch Curr W		Высо ток разряд Вн		ВытокразрядВн
157	32			15			High Discharge Current			Hi Disch Curr		Высо ток разряд		Вытокразряд
158	32			15			Voltage Unbalance Warning		V unbalance W		Напр разб Вним		Напр разб Вним
159	32			15			Voltages Unbalance			Volt Unbalance		Напр разб		Напр разб
160	32			15			Dc Bus Pwr Too Low for Charging		DC Low for Chrg		Dc Авт Pwr Сли низ для зар		Dc Низ для зар
161	32			15			Charge Regulation Failure		Charge Reg Fail		Заряд регу провал		Заряд регу про
162	32			15			Capacity Below 12.5%			Cap Below 12.5%		Ниже Емкость12.5%		Ниже Емк12.5%
163	32			15			Thermocouples Mismatch			Tcples Mismatch		Термопары Несов		Терм Несовп
164	32			15			Heater Fuse Blown			Heater FA		Перег пред Обо		ПерегпредОбо
