﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Rectifier				Rectifier		Выпрямитель				Выпрямитель
2		32			15			DC Status				DC Status		DCстатус				DCстатус
3		32			15			DC Output Voltage			DC Voltage		НапряжDC				НапряжDC
4		32			15			Origin Current				Origin Current		Ток¦					Ток
5		32			15			Temperature				Temperature		Температура				Темп
6		32			15			Used Capacity				Used Capacity		Емкость					Емкость
7		32			15			AC Input Voltage			AC Voltage		НапрВходАС				НапрВходАС
8		32			15			DC Output Current			DC Current		ТокDC					ТокDC
9		32			15			Rectifier SN				Rectifier SN		СерНомер				СерНомер
10		32			15			Total Running Time			Running Time		Наработка				Наработка
11		32			15			No Response Count			NoResponseCount		СчетНетОтвета				СчетНетОтвета
12		32			15			Derated by AC				Derated by AC		СнижМощнПоАС				СнижМощнПоАС
13		32			15			Derated by Temp				Derated by Temp		СнижМощнПоТемп				СнижМощнПоТемп
14		32			15			Derated					Derated			СнижМощн				СнижМощн
15		32			15			Full Fan Speed				Full Fan Speed		ПолнСкорость				ПолнСкоростьВент
16		32			15			WALK-In Function			Walk-in Func		МягкийПуск				МягкийПуск
17		32			15			AC On/Off				AC On/Off		Вход AC Вкл/Выкл			AC Вкл/Выкл
18		32			15			Current Limitation			Curr Limit		ОгранТока				ОгранТока
19		32			15			Voltage High Limit			Volt Hi-limit		ВысНапрПорог				ВысНапрПорог
20		32			15			AC Input Status				AC Status		ВходАСстатус				ВходАСстатус
21		32			15			Rect Temperature High			Rect Temp High		ВысТемпВыпрям				ВысТемпВыпр
22		32			15			Rectifier Fault				Rect Fault		НеиспрВыпрям				НеиспрВыпр
23		32			15			Rectifier Protected			Rect Protected		ЗащитаВыпрям				ЗащитаВыпр
24		32			15			Fan Failure				Fan Failure		ВентНеиспр				ВентНеиспр
25		32			15			Current Limit State			Curr-lmt State		ОгранТокаСтатус				ОгранТокаСтатус
26		32			15			EEPROM Failure				EEPROM Failure		EEPROM Неиспр				EEPROM Неиспр
27		32			15			DC On/Off Control			DC On/Off Ctrl		DC Вкл/Выкл Контр			DC Вкл/Выкл Контр
28		32			15			AC On/Off Control			AC On/Off Ctrl		АC Вкл/Выкл Контр			АC Вкл/Выкл Контр
29		32			15			LED Control				LED Control		Светодиоды				Светодиоды
30		32			15			Rectifier Reset				Rect Reset		СбросВыпрям				СбросВыпрям
31		32			15			AC Input Failure			AC Failure		ВходАСнеиспр				ВходАСнеиспр
34		32			15			Over Voltage				Over Voltage		Перенапряж				Перенапряж
37		32			15			Current Limit				Current limit		ОгранТока				ОгранТока
39		32			15			Normal					Normal			Норма					Норма
40		32			15			Limited					Limited			огранич					огранич
45		32			15			Normal					Normal			норма					норма
46		32			15			Full					Full			полн					полн
47		32			15			Disabled				Disabled		выкл¬					выкл¬
48		32			15			Enabled					Enabled			вкл¬					вкл¬
49		32			15			On					On			вкл					вкл
50		32			15			Off					Off			выкл					выкл
51		32			15			Normal					Normal			норма					норма
52		32			15			Failure					Failure			Неиспр					Неиспр
53		32			15			Normal					Normal			норма					норма
54		32			15			Over temperature			Over-temp		ВысТемп					ВысТемп
55		32			15			Normal					Normal			норма					норма
56		32			15			Fault					Fault			Неиспр					Неиспр
57		32			15			Normal					Normal			норма					норма
58		32			15			Protected				Protected		Защита					Защита
59		32			15			Normal					Normal			норма					норма
60		32			15			Failure					Failure			Неиспр					Неиспр
61		32			15			Normal					Normal			норма					норма
62		32			15			Alarm					Alarm			Авария					Авария
63		32			15			Normal					Normal			норма					норма
64		32			15			Failure					Failure			Неиспр					Неиспр
65		32			15			Off					Off			выкл					выкл
66		32			15			On					On			вкл					вкл
67		32			15			Off					Off			выкл					выкл
68		32			15			On					On			вкл					вкл
69		32			15			Flash					Flash			Моргает					Моргает
70		32			15			Cancel					Cancel			Отмена					Отмена
71		32			15			Off					Off			выкл					выкл
72		32			15			Reset					Reset			Сброс					Сброс
73		32			15			Open Rectifier				On			вкл					вкл
74		32			15			Close Rectifier				Off			выкл					выкл
75		32			15			Off					Off			выкл					выкл
76		32			15			LED Control				Flash			Моргает					Моргает
77		32			15			Rectifier Reset				Rect Reset		ВыпрСброс				ВыпрСброс
80		32			15			Comm Fail				Comm Fail		нетСвязи				нетСвязи
84		32			15			Rectifier High SN			Rect High SN		ВысСерНомер				ВысСерНомер
85		32			15			Rectifier Version			Rect Version		Версия					Версия
86		32			15			Rectifier Part Number			Rect Part NO.		ПродуктКод				ПродуктКод
87		32			15			Sharing Current State			Sharing Curr		РазделТока				РазделТока
88		32			15			Sharing Current Alarm			SharingCurr Alm		РазделТокаАвар				РазделТокаАвар
89		32			15			Over Voltage				Over Voltage		Перенапряж				Перенапряж
90		32			15			Normal					Normal			норма					норма
91		32			15			Over Voltage				Over Voltage		Перенапряж				Перенапряж
92		32			15			Line AB Voltage				Line AB Volt		Напр AB					Напр AB
93		32			15			Line BC Voltage				Line BC Volt		Напр BC					Напр BC
94		32			15			Line CA Voltage				Line CA Volt		Напр CA					Напр CA
95		32			15			Low Voltage				Low Voltage		НизкНапр				НизкНапр
96		64			15			AC Under Voltage Protection		U-Volt Protect		дё©И+¦аВНизкНапрЗащита			+¦аВНизкНапрЗащита
97		32			15			Rectifier Position			Rect Position		ПозицВыпр				ПозицВыпр
98		32			15			DC Output Shut Off			DC Output Off		DCВыходОткл				DCВыходОткл
99		32			15			Rectifier Phase				Rect Phase		фазаВыпр				фазаВыпр
100		32			15			A					A			A					A
101		32			15			B					B			B					B
102		32			15			C					C			C					C
103		32			15			Severe Sharing CurrAlarm		SevereSharCurr		КритРазделТокАвар			КритРазделТокАвар
104		32			15			Bar Code1				Bar Code1		Bar Code1				Bar Code1
105		32			15			Bar Code2				Bar Code2		Bar Code2				Bar Code2
106		32			15			Bar Code3				Bar Code3		Bar Code3				Bar Code3
107		32			15			Bar Code4				Bar Code4		Bar Code4				Bar Code4
108		32			15			Rect failure				Rect failure		ВыпрНеиспр				ВыпрНеиспр
109		32			15			No					No			Норма					Норма
110		32			15			yes					yes			да					да
111		32			15			Existence State				Existence ST		Наличие					Наличие
113		32			15			Comm OK					Comm OK			СвязьОК					СвязьОК
114		32			15			All No Response				All No Response		НетОтветаВсе				НетОтветаВсе
115		32			15			No Response				No Response		нетОтвета				нетОтвета
116		32			15			Valid Rated Current			Rated Current		Выходной ток				ВыходТок
117		32			15			Rectifier Efficiency			Efficiency		КПД					КПД
118		32			15			LT 93					LT 93			LT93					LT93
119		32			15			GT 93					GT 93			GT93					GT93
120		32			15			GT 95					GT 95			GT95					GT95
121		32			15			GT 96					GT 96			GT96					GT96
122		32			15			GT 97					GT 97			GT97					GT97
123		32			15			GT 98					GT 98			GT98					GT98
124		32			15			GT 99					GT 99			GT99					GT99
125		32			15			Rect HVSD status			HVSD status		ВыпрHVSDстатус				ВыпрHVSDстатус
276		32			15			EStop/EShutdown				EStop/EShutdown		EStop/EShutdown				EStop/EShutdown
277		32			15			AC On (for EEM)				AC On(EEM)		Включить AC (для EEM)			ВклАС(EEM)
278		32			15			AC Off(for EEM)				AC Off(EEM)		Выключить AC (для EEM)			ВыклАС(ЕЕМ)
279		64			15			Rectifier Reset				Rect Reset(EEM)		Сброс выпрямителя			СбросВыпрям
280		64			15			Rectifier Communication Fail		Rect Comm Fail		Обрыв связи с выпрямителем		ПотерСвязиВыпр
