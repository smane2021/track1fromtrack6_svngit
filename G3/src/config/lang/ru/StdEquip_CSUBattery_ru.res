﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Current				Batt Current		ТокАБ					ТокАБ
2		32			15			Battery Capacity(Ah)			Batt Cap(Ah)		Емкость(Ач)				Емкость(Ач)
3		32			15			Exceed Current Limit			Exceed Curr Lmt		ПревышТокЛимит				ПревышТокЛимит
4		32			15			CSUBattery				CSUBattery		CSUБат					CSUБат
5		32			15			Over Battery current			Over Current		ВысТокАБ				ВысТокАБ
6		32			15			Battery Capacity(%)			Batt Cap(%)		Емкость(%)				Емкость(%)
7		32			15			Battery Voltage				Batt Voltage		НапряжБат				НапряжБат
8		32			15			Low capacity				Low capacity		НизкЕмкость				НизкЕмкость
9		32			15			CSU_Bat temp				CSU_Bat temp		CSU_БАтТемп				CSU_БатТемп
10		32			15			CSU_Battery failure			CSU_Batteryfail		CSU_БатНеиспр				CSU_БатНеиспр
11		32			15			Existent				Existent		есть					есть
12		32			15			Not Existent				Not Existent		нет					нет
28		32			15			Battery Management			Batt Management		УправленБат				УправлениеБат
29		32			15			Yes					Yes			Да					Да
30		32			15			No					No			Нет					нет
96		32			15			Rated Capacity				Rated Capacity		Емкость					Емкость
110		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		Battery Current Imbalance Alarm		BattCurrImbalan
