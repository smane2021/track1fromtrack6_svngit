﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Diesel Battery Voltage			Dsl Batt Volt		НапрБатДГУ				НапрБатДГУ
2		32			15			Diesel Running				Diesel Running		ДГУРабота				ДГУРАбота
3		32			15			Relay2 Status				Relay2 Status		Реле2Статус				Реле2Статус
4		32			15			Relay3 Status				Relay3 Status		Реле3Статус				Реле3Статус
5		32			15			Relay4 Status				Relay4 Status		Реле4Статус				Реле4Статус
6		32			15			Diesel Fail Status			Diesel Fail		ДГУНеиспр				ДГУНеиспр
7		32			15			Diesel Connected Status			Diesel Cnnected		ДГУПодкл				ДГУПодкл
8		32			15			Low Fuel Level Status			Low Fuel Level		НетТоплива				НетТоплива
9		32			15			High Water Temp Status			High Water Temp		ВысТемпВоды				ВысТемпВоды
10		32			15			Low Oil Pressure Status			Low Oil Pressure	НизкДавлМасла				НизкДавлМасла
11		32			15			Start Diesel				Start Diesel		СтартДГУ				СтартДГУ
12		32			15			Relay 2 on/off				Relay 2 on/off		Реле2 вкл/выкл				Реле2 вкл/выкл
13		32			15			Relay 3 on/off				Relay 3 on/off		Реле3 вкл/выкл				Реле3 вкл/выкл
14		32			15			Relay 4 on/off				Relay 4 on/off		Реле4 вкл/выкл				Реле4 вкл/выкл
15		32			15			Battery Voltage Limit			Batt Volt Limit		БатНапрЛимит				БатНапрЛимит
16		32			15			Relay 1 Pulse Time			RLY 1 Puls Time		Реле1ПерИмп				Реле1ПерИмп
17		32			15			Relay 2 Pulse Time			RLY 2 Puls Time		Реле2ПерИмп				Реле2ПерИмп
18		32			15			Relay 1 Pulse Time			RLY 1 Puls Time		Реле1ПерИмп				Реле1ПерИмп
19		32			15			Relay 2 Pulse Time			RLY 2 Puls Time		Реле2ПерИмп				Реле2ПерИмп
20		32			15			Low DC Voltage				Low DC Voltage		НизкНапрDC				НизкНапрDC
21		32			15			Diesel Supervision Fail			Supervision Fail	КонтролНеиспр				КонтролНеиспр
22		32			15			Diesel Generator Fail			Generator Fail		ДГУНеиспр				ДГУНеиспр
23		32			15			Diesel Generator Connected		Genrtor Cnected		ДГУПодкл				ДГУПодкл
24		32			15			Not running				Not running		НеРабот					НеРабот
25		32			15			Running					Running			Работа					Работа
26		32			15			off					off			выкл					выкл
27		32			15			on					on			вкл					вкл
28		32			15			off					off			выкл					выкл
29		32			15			on					on			вкл					вкл
30		32			15			off					off			выкл					выкл
31		32			15			on					on			вкл					вкл
32		32			15			no					no			нет					нет
33		32			15			yes					yes			да					да
34		32			15			no					no			нет					нет
35		32			15			yes					yes			да					да
36		32			15			off					off			выкл					выкл
37		32			15			on					on			вкл					вкл
38		32			15			off					off			выкл					выкл
39		32			15			on					on			вкл					вкл
40		32			15			off					off			выкл					выкл
41		32			15			on					on			вкл					вкл
42		32			15			Diesel Generator			Diesel Genrator		ДГУ					ДГУ
43		32			15			Mains Connected				Mains Connected		СетьПодкл				СетьПодкл
44		32			15			Diesel Shutdown				Diesel Shutdown		ВыклДГУ					ВыклДГУ
45		32			15			Low Fuel Level				Low Fuel Level		НетТоплива				НетТоплива
46		32			15			High Water Temperature			High Water Temp		ВысТемпВоды				ВысТемпВоды
47		32			15			Low Oil Pressure			Low Oil Press		НизкДавлМасла				НизкДавлМасла
48		32			15			Supervision Fail			SMAC Fail		SMACНеиспр				SMACНеиспр
49		32			15			Low Oil Pressure Alarm Status		Low Oil Press		НизкДавлМасла				НизкДавлМасла
50		32			15			Reset Low Oil Pressure Alarm		Clr OilPressArm		СбросАвНизкДавл				СбросАвНизкДавл
51		32			15			State					State			Статус					Статус
52		32			15			Existence State				Existence State		Наличие					Наличие
53		32			15			Existent				Existent		Есть					Есть
54		32			15			Not Existent				Not Existent		нет					нет
55		32			15			Total Run Time				Total Run Time		Время наработки				ВремяНаработ
56		32			15			Maintenance Time Limit			Mtn Time Limit		Установка времени обслуживания		УстанВремОбслуж
57		32			15			Clear Total Run Time			Clr Run Time		Сброс времени наработки			СбросВремНараб
58		32			15			Periodical Maintenance Required		Mtn Required		Периодичность обслуживания		ПериодОбслуж
59		32			15			Maintenance Countdown Timer		Mtn DownTimer		Таймер обрат отсчёта обслужив		ТаймерОтсчОбсл
