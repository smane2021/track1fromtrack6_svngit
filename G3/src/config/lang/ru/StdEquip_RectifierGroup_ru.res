﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Rectifier Group				Rect Group		Выпрямители				Выпрямители
2		32			15			Total Current				Total Current		Ток					Ток
3		32			15			Average Voltage				Average Voltage		СреднНапряж				СреднНапр
4		32			15			System Capacity Used			SysCap Used		МощнСистемы				МощнСистемы
5		32			15			Maximum Used Capacity			Max Used Cap		МаксМощн				МаксМощн
6		32			15			Minimum Used Capacity			Min Used Cap		МинМощн					МинМощн
7		64			15			Communicating Rectifiers		Comm Rect Num		КоммНомерВыпр				КоммНомерВыпр
8		32			15			Valid Rectifiers			Valid Rectifiers	Выпрямит в работе			Выпрям в работе
9		64			15			Rectifiers Number			Rect Number		Количество выпрям			Кол-во выпрям
10		32			15			Rectifier AC Failure State		AC-fail State		НетВходаАСВыпр				НетВходаАСВыпр
11		32			15			Multi-rectifiers Failure Status		Multi-Rect Fail		НетВходаАС				НетВходаАС
12		32			15			Rectifer Current Limit			Current Limit		ОгранТока				ОрганТока
13		32			15			Rectifiers Trim				Rect Trim		НастрВыпр				НастрВыпр
14		32			15			DC On/Off Control			DC On/Off Ctrl		DC Вкл/Выкл Контр			DC Вкл/Выкл Контр
15		32			15			AC On/Off Control			AC On/Off Ctrl		АC Вкл/Выкл Контр			АC Вкл/Выкл Контр
16		32			15			Rectifiers LEDs Control			LEDs Control		Светодиоды				Светодиоды
17		32			15			Fan Speed Control			Fan Speed Ctrl		ВентСкорость				ВентСкорость
18		32			15			Rated Voltage				Rated Voltage		НоминНапряж				НоминНапряж
19		32			15			Rated Current				Rated Current		НомТок					НомТок
20		32			15			High Voltage Limit			Hi-Volt Limit		ВысНапрПорог				ВыснапрПорог
21		32			15			Low Voltage Limit			Lo-Volt Limit		НизкНапрПорог				НизкНапрПорог
22		32			15			High Temperature Limit			Hi-Temp Limit		ВысТемпПорог				ВысТемпПорог
24		32			15			Restart Time on Over Voltage		OverVRestartT		ВремяПерезагрприВысНапр			ВремяВысНапр
25		32			15			Walk-in Time				Walk-in Time		Время старта				Время старта
26		32			15			Walk-in Enabled				Walk-in Enabled		Мягкий старт				Мягкий старт
27		32			15			Min Redundancy				Min Redundancy		МинРезерв				МинРезерв
28		32			15			Max Redundancy				Max Redundancy		МаксРезерв				МаксРезерв
29		64			15			Switch Off Delay			SwitchOff Delay		Задержка выключения			Задержка выключения
30		32			15			Cycle Period				Cyc Period		ПериодЦикл				ПериодЦикл
31		32			15			Cycle Activation Time			Cyc Act Time		ЦиклВремяАктив				ЦиклВремяАктив
32		32			15			Rectifier AC Failure			Rect AC Fail		НетВходаАСВыпрям			НетВходаАСВыпр
33		32			15			Multi-Rectifiers Failure		Multi-rect Fail		НескВыпрНеиспр				НескВыпрНеиспр
36		32			15			Normal					Normal			Норма					Норма
37		32			15			Failure					Failure			Неиспр					Неиспр
38		32			15			Switch Off All				Switch Off All		ВыклВсё					ВыклВсё
39		32			15			Switch On All				Switch On All		ВклВсё					ВклВсё
42		32			15			All Twinkle				All Twinkle		ВсеМоргают				ВсеМоргают
43		32			15			Stop Twinkle				Stop Twinkle		СтопМоргание				СтопМоргание
44		32			15			Full Speed				Full Speed		ПолнСкорость				ПолнСкор
45		32			15			Automatic Speed				Auto Speed		АвтоСкорость				АвтоСкор
46		32			32			Current Limit Control			Curr-Limit Ctl		ОгранТокаУправл				ОгрТокаУправл
47		32			32			Full Capability Control			Full-cap Ctl		ПолнУправл				ПолнУправ
54		32			15			Disabled				Disabled		Выкл					Выкл
55		32			15			Enabled					Enabled			вкл					вкл
68		32			15			ECO Mode				ECO Mode		ЭКО режим				ЭКО режим
72		32			15			Turn-on when AC Over-voltage		Turn-on AcOverV		ВклПриВысНапр				ВклПриВысНапр
73		32			15			No					No			нет					нет
74		32			15			Yes					Yes			да					да
77		32			15			Pre-CurrLmt On Switch-on Enb		Pre-CurrLimit		Pre-CurrLmt On Switch-on Enb		Pre-CurrLimit
78		32			15			Rectifier Power type			Rect Power type		ТипВходВыпрям				ТипВходВыпрям
79		32			15			Double Supply				Double Supply		2входа					2входа
80		32			15			Single Supply				Single Supply		1вход					1вход
81		32			15			Last Rectifiers Quantity		Last Rect Qty		КолвоВыпрямит				КолвоВыпрям
82		32			15			Rectifier Lost				Rectifier Lost		ПотеряВыпр				ПотеряВыпр
83		32			15			Rectifier Lost				Rectifier Lost		ПотеряВыпр				ПотеряВыпр
84		32			15			Clear Rectifier Lost Alarm		Clear Rect Lost		СбросАвПотеряВыпр			СбросАвПотеряВыпр
85		32			15			Clear					Clear			Сброс					Сброс
86		32			15			Confirm Rect Position/Phase		Confirm Posi/PH		Подтвержд Поз/фаза			Подт Поз/фаза
87		32			15			Confirm					Confirm			Подтвердить				Подтверд
88		32			15			Best Operating Point			Best Point		ЛучшаяРабота				ЛучшаяРабота
89		32			15			Rectifier Redundancy			Rect Redundancy		РезервВыпр				РезервВыпр
90		32			15			Load Fluctuation Range			Fluct Range		ИзменНагр				ИзменНагр
91		32			15			System Energy Saving Point		EngySave Point		Экономичность				Экономично
92		32			15			E-Stop Function				E-Stop Function		E-Stop функция				E-Stop Функ
93		32			15			AC phases				AC phases		АС фазы					АС фазы
94		32			15			Single phase				Single phase		1фаза					1фаза
95		32			15			Three phases				Three phases		3фазы					3фазы
96		32			15			Input current limit			InputCurrLimit		ОгранВходнТока				ОгранВходнТока
97		32			15			Double Supply				Double Supply		2входа					2входа
98		32			15			Single Supply				Single Supply		1вход					1вход
99		32			15			Small Supply				Small Supply		Small Supply				Small Supply
100		32			15			Restart on Over Voltage Enabled		DCOverVRestart		ПерезагрПриВысНапряж			ПерезагрПриВысНапряж
101		32			15			Sequence Start Interval			Start Interval		ИнтервалСтарта				ИнтервалСтарта
102		32			15			Rated Voltage				Rated Voltage		НомНапряж				НомНапряж
103		32			15			Rated Current				Rated Current		НомТок					НомТок
104		32			15			All Rectifiers No Response		Rects No Resp		НетОтветаВсеВыпрям			НетОтветаВсеВыпрям
105		32			15			Inactive				Inactive		Неактив					Неактив
106		32			15			Active					Active			Актив					Актив
107		32			15			Rectifier Redundancy Active		Redund Active		РезервирАктивно				РезервирАктив
108		32			15			Rectifier Redundancy Oscillated		Redund Oscill		Rectifier Redundancy Oscillated		Redund Oscill
109		32			15			ResetRedundancyOscillatedAlarm		Reset Oscill		СбросRedundancyOscillatedAlarm		СбросOscill
110		32			15			High Voltage Limit(24V)			Hi-Volt Limit		ВысНапрПорог(24V)			ВысНапрПорог
111		32			15			Rectifiers Trim(24V)			Rect Trim		Rectifiers Trim(24V)			Rect Trim
112		32			15			Rated Voltage(Internal Use)		Rated Voltage		НомНапряж(Internal Use)			НомНапряж
113		32			15			Rect Info Change(M/S Internal Use)	Rect Info Change	Rect Info Change(M/S Internal Use)	Rect Info Change
114		32			15			MixHE Power				MixHE Power		MixHE мощность				MixHE мощность
115		32			15			Derate					Derate			СнижМощн				СнижМощн
116		32			15			Non-derate				Non-derate		БезСнижМощн				БезСнижМощн
117		32			15			Rect Drying Time			Rect Drying Time	ВремяСушки				ВремяСушки
118		32			15			All Rect Is Drying			Rect Is Drying		Сушка					Сушка
119		32			15			Clear Rect Comm Fail Alarm		Clear Comm Fail		СвязьАвария				СвязьАвар
120		32			15			HVSD Enable				HVSD Enable		HVSD Актив				HVSD Актив
121		32			15			HVSD Voltage Difference			HVSD Volt Diff		HVSD РазнНапр				HVSDРазнНапр
122		32			15			Total Rated Current of Work On		Total Rated Cur		Total Rated Current of Work On		Total Rated Cur
123		32			15			Diesel Power Limit Enable		DislPwrLmt Enb		ДГУогранМощн				ДГУогранМощн
124		32			15			Diesel DI Input				Diesel DI Input		ДГУцифрВход				ДГУцифрВход
125		32			15			Diesel Power Limit Point Set		DislPwrLmt Set		ДГУогранМощнУст				ДГУогранМощнУст
126		32			15			None					None			нет					нет
127		32			15			DI 1					DI 1			ЦифрВх 1				ЦифрВх 1
128		32			15			DI 2					DI 2			ЦифрВх 2				ЦифрВх 2
129		32			15			DI 3					DI 3			ЦифрВх 3				ЦифрВх 3
130		32			15			DI 4					DI 4			ЦифрВх 4				ЦифрВх 4
131		32			15			DI 5					DI 5			ЦифрВх 5				ЦифрВх 5
132		32			15			DI 6					DI 6			ЦифрВх 6				ЦифрВх 6
133		32			15			DI 7					DI 7			ЦифрВх 7				ЦифрВх 7
134		32			15			DI 8					DI 8			ЦифрВх 8				ЦифрВх 8
135		32			15			Current Limit Point			Curr Limit Pt.		Порог огранич тока			Порог огранич тока
136		32			15			Current Limit				Current Limit		Порог огранич				Порог огранич
137		32			15			Max Current Limit Value			MaxCurrLimitVal		Макс ток Порог				Макс ток Порог
138		32			15			Default Current Limit Point		DeftCurrLimitPt		Порог по умолчанию			Порог по умолчанию
139		32			15			Minimum Current Limit Value		MinCurrLimitVal		Мин ток Порог				Мин ток Порог
140		32			15			AC Power Limit Mode			AC Power Lmt		Мощность АС режим ограничения		МощнАСрежОгран
141		32			15			A					A			A					A
142		32			15			B					B			B					B
143		32			15			Existence State				Existence State		Состояние установлен			СостУстановл
144		32			15			Existent				Existent		Установлено				Установлено
145		32			15			Not Existent				Not Existent		Не установлено				НеУстановлено
146		32			15			Total Output Power			Output Power		Суммарная выходная мощность		СумВыхМощн
147		32			15			Total Slave Rated Current		Total RatedCurr		Всего ведомого номинальный ток		ВсеВедомНомТок
148		32			15			Reset Rectifier IDs			Reset Rect IDs		Сброс ID выпрямителя			СбрсIDВыпрям
149		64			15			HVSD Voltage Difference (24V)		HVSD Volt Diff		Различается напряжение HVSD (24В))	РазлНапрHVSD(24В)
150		32			15			Normal Update				Normal Update		НормОбновл				НормОбновл
151		32			15			Force Update				Force Update		УскорОбновл				УскорОбновл
152		64			15			Сброс выпрямител			Update OK Number	Update OK Num				Обновление ОК колличества
153		64			15			Update State				Update State		Состояние обновления			СостОбновл
154		32			15			Updating				Updating		Обновление				Обновл
155		32			15			Normal Successful			Normal Success		Нормально удачно			НормУспех
156		32			15			Normal Failed				Normal Fail		Нормально не удачно			НормНеудача
157		32			15			Force Successful			Force Success		Ускоренное удачно			УскорУспех
158		32			15			Force Failed				Force Fail		Ускоренно не удачно			УскорНеудача
159		32			15			Communication Time-Out			Comm Time-Out		Связь прервана				СвязьПрерв
160		32			15			Open File Failed			Open File Fail		Открытие файла неудачна			ОткрФайлаНеудача
161		32			15			Delay of LowRectCap Alarm		LowRectCapDelay				Delay of LowRectCap Alarm			LowRectCapDelay		
162		32			15			Maximum Redundacy 				MaxRedundacy 				Maximum Redundacy 					MaxRedundacy 		
163		32			15			Low Rectifier Capacity			Low Rect Cap				Low Rectifier Capacity			Low Rect Cap			
164		32			15			High Rectifier Capacity			High Rect Cap				High Rectifier Capacity			High Rect Cap			

165		32			15			Efficiency Tracker			Effi Track				Efficiency Tracker			Effi Track		
166		32			15			Benchmark Rectifier			Benchmark Rect			Benchmark Rectifier			Benchmark Rect	
167		32			15			R48-3500e3					R48-3500e3				R48-3500e3					R48-3500e3		
168		32			15			R48-3200					R48-3200				R48-3200					R48-3200		
169		32			15			96%							96%						96%							96%				
170		32			15			95%							95%						95%							95%				
171		32			15			94%							94%						94%							94%				
172		32			15			93%							93%						93%							93%				
173		32			15			92%							92%						92%							92%				
174		32			15			91%							91%						91%							91%				
175		32			15			90%							90%						90%							90%				
176		32			15			Cost per kWh				Cost per kWh			Cost per kWh				Cost per kWh	
177		32			15			Currency					Currency				Currency					Currency		
178		32			15			USD							USD						USD							USD				
179		32			15			CNY							CNY						CNY							CNY				
180		32			15			EUR							EUR						EUR							EUR				
181		32			15			GBP							GBP						GBP							GBP				
182		32			15			RUB							RUB						RUB							RUB				
183		32			15			Percentage 98% Rectifiers	Percent98%Rect			Percentage 98% Rectifiers	Percent98%Rect	
184		32			15			10%							10%						10%							10%				
185		32			15			20%							20%						20%							20%				
186		32			15			30%							30%						30%							30%				
187		32			15			40%							40%						40%							40%				
188		32			15			50%							50%						50%							50%				
189		32			15			60%							60%						60%							60%				
190		32			15			70%							70%						70%							70%				
191		32			15			80%							80%						80%							80%				
192		32			15			90%							90%						90%							90%				
193		32			15			100%						100%					100%						100%			
																					
200		32			15			Default Voltage			Default Voltage				Default Voltage			Default Voltage		
