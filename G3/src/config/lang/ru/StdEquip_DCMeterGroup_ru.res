﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		64			15			DC Meter Group				DCMeter Group		Группа DC измерителей			ГрDCизмерит
2		64			15			DC Meter Number				DCMeter Num		Номер DC измерителя			НомерDCизмер
3		32			15			Communication Fail			Comm Fail		Потеря связи				ПотеряСвязи
4		64			15			Existence State				Existence		Состояние установлен			СостУстановл
5		32			15			Existent				Existent		Установлено				Установлено
6		32			15			Not Existent				Not Existent		Не установлено				НеУстановлено
11		64			15			DC Meter Lost				DCMeter Lost		Потеря DC измерителя			ПотеряDCизмер
12		64			15			DC Meter Num Last Time			LastDCMeter Num		Последнее время измерения DC		ВремяИзмерDC
13		64			15			Clear DC Meter Lost Alarm		ClrDCMeterLost		Сброс аварии потеля DC измерит		СбросПотерDCизм
14		32			15			Clear					Clear			Очистить				Очистить
15		64			15			Total Energy Consumption		TotalEnergy		Потребляемая мощность			ПотребМощн
