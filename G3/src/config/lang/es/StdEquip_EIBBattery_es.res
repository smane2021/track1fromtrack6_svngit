﻿#
# Locale language support: Spanish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			EIB Battery				EIB Battery		EIB Batería				EIB Batería
2		32			15			Battery Current				Batt Current		Corriente Batería			Corriente Bat
3		32			15			Battery Voltage				Batt Voltage		Tensión batería				Tensión batería
4		32			15			Battery Capacity (Ah)			Batt Cap(Ah)		Capacidad (Ah)				Capacidad (Ah)
5		32			15			Battery Capacity (%)			Batt Cap(%)		Capacidad batería (%)			Cap Bat (%)
6		32			15			Current Limit Exceeded			Curr Lmt Exceed		Limite corriente excedido		Lim corr pasado
7		32			15			Over Battery Current			Over Current		Sobrecorriente				Sobrecorriente
8		32			15			Low Capacity				Low Capacity		Baja capacidad				Baja capacidad
9		32			15			Yes					Yes			Sí					Sí
10		32			15			No					No			No					No
26		32			15			State					State			Estado					Estado
27		32			15			Battery Block 1 Voltage			Batt B1 Volt		Tensión elemento 1			Tensión Elem 1
28		32			15			Battery Block 2 Voltage			Batt B2 Volt		Tensión elemento 2			Tensión Elem 2
29		32			15			Battery Block 3 Voltage			Batt B3 Volt		Tensión elemento 3			Tensión Elem 3
30		32			15			Battery Block 4 Voltage			Batt B4 Volt		Tensión elemento 4			Tensión Elem 4
31		32			15			Battery Block 5 Voltage			Batt B5 Volt		Tensión elemento 5			Tensión Elem 5
32		32			15			Battery Block 6 Voltage			Batt B6 Volt		Tensión elemento 6			Tensión Elem 6
33		32			15			Battery Block 7 Voltage			Batt B7 Volt		Tensión elemento 7			Tensión Elem 7
34		32			15			Battery Block 8 Voltage			Batt B8 Volt		Tensión elemento 8			Tensión Elem 8
35		32			15			Battery Management			Batt Manage		Gestión Baterías			Gestión Bat
36		32			15			Enable					Enable			Sí					Sí
37		32			15			Disable					Disable			No					No
38		32			15			Failure					Failure			Fallo					Fallo
39		32			15			Shunt Full Current			Shunt Current		Corriente Shunt				Corr Shunt
40		32			15			Shunt Full Voltage			Shunt Voltage		Tensión Shunt				Tens Shunt
41		32			15			On					On			Conectado				Conectado
42		32			15			Off					Off			Apagado					Apagado
43		32			15			Failure					Failure			Fallo					Fallo
44		32			15			Used Temperature Sensor			Used Sensor		Sensor de temperatura			Sensor Temp
87		32			15			None					None			Ninguno					Ninguno
91		32			15			Temperature Sensor 1			Temp Sensor 1		Sensor temperatura 1			Sens Temp 1
92		32			15			Temperature Sensor 2			Temp Sensor 2		Sensor temperatura 2			Sens Temp 2
93		32			15			Temperature Sensor 3			Temp Sensor 3		Sensor temperatura 3			Sens Temp 3
94		32			15			Temperature Sensor 4			Temp Sensor 4		Sensor temperatura 4			Sens Temp 4
95		32			15			Temperature Sensor 5			Temp Sensor 5		Sensor temperatura 5			Sens Temp 5
96		32			15			Rated Capacity				Rated Capacity		Capacidad nominal C10			Capacidad C10
97		32			15			Battery Temperature			Battery Temp		Temperatura de Batería			Temp Batería
98		32			15			Battery Temperature Sensor		BattTempSensor		Sensor Temperatura Batería		Sensor Temp
99		32			15			None					None			Ninguno					Ninguno
100		32			15			Temperature 1				Temp 1			Temperatura 1				Temp 1
101		32			15			Temperature 2				Temp 2			Temperatura 2				Temp 2
102		32			15			Temperature 3				Temp 3			Temperatura 3				Temp 3
103		32			15			Temperature 4				Temp 4			Temperatura 4				Temp 4
104		32			15			Temperature 5				Temp 5			Temperatura 5				Temp 5
105		32			15			Temperature 6				Temp 6			Temperatura 6				Temp 6
106		32			15			Temperature 7				Temp 7			Temperatura 7				Temp 7
107		32			15			Temperature 8				Temp 8			Temperatura 8				Temp 8
108		32			15			Temperature 9				Temp 9			Temperatura 9				Temp 9
109		32			15			Temperature 10				Temp 10			Temperatura 10				Temp 10
110		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		Corr Bat Alm Dese		CorrBatAlmDese	
111		32			15			EIB1 Battery2				EIB1 Battery2				EIB1 Batería2				EIB1 Batería2
112		32			15			EIB1 Battery3				EIB1 Battery3				EIB1 Batería3				EIB1 Batería3
113		32			15			EIB2 Battery1				EIB2 Battery1				EIB2 Batería1				EIB2 Batería1
114		32			15			EIB2 Battery2				EIB2 Battery2				EIB2 Batería2				EIB2 Batería2
115		32			15			EIB2 Battery3				EIB2 Battery3				EIB2 Batería3				EIB2 Batería3
116		32			15			EIB3 Battery1				EIB3 Battery1				EIB3 Batería1				EIB3 Batería1
117		32			15			EIB3 Battery2				EIB3 Battery2				EIB3 Batería2				EIB3 Batería2
118		32			15			EIB3 Battery3				EIB3 Battery3				EIB3 Batería3				EIB3 Batería3
119		32			15			EIB4 Battery1				EIB4 Battery1				EIB4 Batería1				EIB4 Batería1
120		32			15			EIB4 Battery2				EIB4 Battery2				EIB4 Batería2				EIB4 Batería2
121		32			15			EIB4 Battery3				EIB4 Battery3				EIB4 Batería3				EIB4 Batería3
122		32			15			EIB1 Battery1				EIB1 Battery1				EIB1 Batería1				EIB1 Batería1

150		32			15		Battery 1					Batt 1		Batería 1					Bat 1
151		32			15		Battery 2					Batt 2		Batería 2					Bat 2
152		32			15		Battery 3					Batt 3		Batería 3					Bat 3
