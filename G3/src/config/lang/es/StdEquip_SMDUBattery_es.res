﻿#
# Locale language support: Spanish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Current				Batt Current		Corriente a batería			Corriente bat
2		32			15			Battery Capacity(Ah)			Batt Cap(Ah)		Capacidad batería			Capacidad bat
3		32			15			Current Limit Exceeded			Curr Lmt Exceed		Límite de corriente pasado		Lim corr pasado
4		32			15			Battery					Battery			Batería					Batería
5		32			15			Over Battery Current			Over Current		Sobrecorriente a batería		Sobrecorriente
6		32			15			Battery Capacity(%)			Batt Cap(%)		Capacidad batería (%)			Capacidad
7		32			15			Battery Voltage				Batt Voltage		Tensión batería				Tensión bat
8		32			15			Low Capacity				Low Capacity		Baja capacidad				Baja capacidad
9		32			15			On					On			Conectado				Conectado
10		32			15			Off					Off			Desconectado				Desconectado
11		32			15			Battery Fuse Voltage			Fuse Voltage		Tensión fusible de batería		Tensión fus bat
12		32			15			Battery Fuse Status			Fuse Status		Estado fusible bat			Estado fusible
13		32			15			Fuse Alarm				Fuse Alarm		Alarma de fusible			Alarma fusible
14		32			15			SMDU Battery				SMDU Battery		Batería Extensión (SMDU)		Batería Ext
15		32			15			State					State			Estado					Estado
16		32			15			Off					Off			Apagado					Apagado
17		32			15			On					On			Conectado				Conectado
18		32			15			Switch					Switch			Conmutador				Conmutador
19		32			15			Over Battery Current			Over Current		Sobrecorriente a batería		Sobrecorriente
20		32			15			Used by Battery Management		Manage Enable		Utilizada en Gestión Bat		En Gestión Bat
21		32			15			Yes					Yes			Sí					Sí	
22		32			15			No					No			No					No
23		32			15			Overvoltage Setpoint			OverVolt Point		Nivel de Sobretensión			Sobretensión
24		32			15			Low Voltage Setpoint			Low Volt Point		Nivel de Subtensión			Niv Subtensión
25		32			15			Battery Overvoltage			Overvoltage		Sobretensión Batería			Sobretensión
26		32			15			Battery Undervoltage			Undervoltage		Subtensión Batería			Subtensión Bat
27		32			15			Overcurrent				Overcurrent		Sobrecorriente				Sobrecorriente
28		32			15			Communication Interrupt			Comm Interrupt		Interrupción Comunicación		Interrup COM
29		32			15			Interrupt Times				Interrupt Times		Interrupciones				Interrupciones
44		32			15			Used Temperature Sensor			Used Sensor		Sensor Temperatura			Sensor Temp
87		32			15			None					None			Ninguno					Ninguno
91		32			15			Temperature Sensor 1			Temp Sens 1		Sensor temperatura 1			Sens Temp 1
92		32			15			Temperature Sensor 2			Temp Sens 2		Sensor temperatura 2			Sens Temp 2
93		32			15			Temperature Sensor 3			Temp Sens 3		Sensor temperatura 3			Sens Temp 3
94		32			15			Temperature Sensor 4			Temp Sens 4		Sensor temperatura 4			Sens Temp 4
95		32			15			Temperature Sensor 5			Temp Sens 5		Sensor temperatura 5			Sens Temp 5
96		32			15			Rated Capacity				Rated Capacity		Capacidad Nominal C10			Capacidad C10
97		32			15			Battery Temperature			Battery Temp		Temperatura Batería			Temperatura
98		32			15			Battery Temperature Sensor		BattTempSensor		Sensor Temperatura Batería		Sensor Temp Bat
99		32			15			None					None			No					No
100		32			15			Temperature 1				Temp 1			Temperatura 1				Temp 1
101		32			15			Temperature 2				Temp 2			Temperatura 2				Temp 2
102		32			15			Temperature 3				Temp 3			Temperatura 3				Temp 3
103		32			15			Temperature 4				Temp 4			Temperatura 4				Temp 4
104		32			15			Temperature 5				Temp 5			Temperatura 5				Temp 5
105		32			15			Temperature 6				Temp 6			Temperatura 6				Temp 6
106		32			15			Temperature 7				Temp 7			Temperatura 7				Temp 7
107		32			15			Temperature 8				Temp 8			Temperatura 8				Temp 8
108		32			15			Temperature 9				Temp 9			Temperatura 9				Temp 9
109		32			15			Temperature 10				Temp 10			Temperatura 10				Temp 10
110		32			15			SMDU1 Battery1			SMDU1 Battery1		SMDU1 Batería1			SMDU1 Batería1
111		32			15			SMDU1 Battery2			SMDU1 Battery2		SMDU1 Batería2			SMDU1 Batería2
112		32			15			SMDU1 Battery3			SMDU1 Battery3		SMDU1 Batería3			SMDU1 Batería3
113		32			15			SMDU1 Battery4			SMDU1 Battery4		SMDU1 Batería4			SMDU1 Batería4
114		32			15			SMDU1 Battery5			SMDU1 Battery5		SMDU1 Batería5			SMDU1 Batería5
115		32			15			SMDU2 Battery1			SMDU2 Battery1		SMDU2 Batería1			SMDU2 Batería1

116		32		15				Battery Current Imbalance Alarm		BattCurrImbalan		Corr Bat Alm Dese		CorrBatAlmDese

117		32			15			SMDU2 Battery3			SMDU2 Battery3		SMDU2 Batería3			SMDU2 Batería3
118		32			15			SMDU2 Battery4			SMDU2 Battery4		SMDU2 Batería4			SMDU2 Batería4
119		32			15			SMDU2 Battery5			SMDU2 Battery5		SMDU2 Batería5			SMDU2 Batería5
120		32			15			SMDU3 Battery1			SMDU3 Battery1		SMDU3 Batería1			SMDU3 Batería1
121		32			15			SMDU3 Battery2			SMDU3 Battery2		SMDU3 Batería2			SMDU3 Batería2
122		32			15			SMDU3 Battery3			SMDU3 Battery3		SMDU3 Batería3			SMDU3 Batería3
123		32			15			SMDU3 Battery4			SMDU3 Battery4		SMDU3 Batería4			SMDU3 Batería4
124		32			15			SMDU3 Battery5			SMDU3 Battery5		SMDU3 Batería5			SMDU3 Batería5
125		32			15			SMDU4 Battery1			SMDU4 Battery1		SMDU4 Batería1			SMDU4 Batería1
126		32			15			SMDU4 Battery2			SMDU4 Battery2		SMDU4 Batería2			SMDU4 Batería2
127		32			15			SMDU4 Battery3			SMDU4 Battery3		SMDU4 Batería3			SMDU4 Batería3
128		32			15			SMDU4 Battery4			SMDU4 Battery4		SMDU4 Batería4			SMDU4 Batería4
129		32			15			SMDU4 Battery5			SMDU4 Battery5		SMDU4 Batería5			SMDU4 Batería5
130		32			15			SMDU5 Battery1			SMDU5 Battery1		SMDU5 Batería1			SMDU5 Batería1
131		32			15			SMDU5 Battery2			SMDU5 Battery2		SMDU5 Batería2			SMDU5 Batería2
132		32			15			SMDU5 Battery3			SMDU5 Battery3		SMDU5 Batería3			SMDU5 Batería3
133		32			15			SMDU5 Battery4			SMDU5 Battery4		SMDU5 Batería4			SMDU5 Batería4
134		32			15			SMDU5 Battery5			SMDU5 Battery5		SMDU5 Batería5			SMDU5 Batería5
135		32			15			SMDU6 Battery1			SMDU6 Battery1		SMDU6 Batería1			SMDU6 Batería1
136		32			15			SMDU6 Battery2			SMDU6 Battery2		SMDU6 Batería2			SMDU6 Batería2
137		32			15			SMDU6 Battery3			SMDU6 Battery3		SMDU6 Batería3			SMDU6 Batería3
138		32			15			SMDU6 Battery4			SMDU6 Battery4		SMDU6 Batería4			SMDU6 Batería4
139		32			15			SMDU6 Battery5			SMDU6 Battery5		SMDU6 Batería5			SMDU6 Batería5
140		32			15			SMDU7 Battery1			SMDU7 Battery1		SMDU7 Batería1			SMDU7 Batería1
141		32			15			SMDU7 Battery2			SMDU7 Battery2		SMDU7 Batería2			SMDU7 Batería2
142		32			15			SMDU7 Battery3			SMDU7 Battery3		SMDU7 Batería3			SMDU7 Batería3
143		32			15			SMDU7 Battery4			SMDU7 Battery4		SMDU7 Batería4			SMDU7 Batería4
144		32			15			SMDU7 Battery5			SMDU7 Battery5		SMDU7 Batería5			SMDU7 Batería5
145		32			15			SMDU8 Battery1			SMDU8 Battery1		SMDU8 Batería1			SMDU8 Batería1
146		32			15			SMDU8 Battery2			SMDU8 Battery2		SMDU8 Batería2			SMDU8 Batería2
147		32			15			SMDU8 Battery3			SMDU8 Battery3		SMDU8 Batería3			SMDU8 Batería3
148		32			15			SMDU8 Battery4			SMDU8 Battery4		SMDU8 Batería4			SMDU8 Batería4
149		32			15			SMDU8 Battery5			SMDU8 Battery5		SMDU8 Batería5			SMDU8 Batería5
150		32			15			SMDU2 Battery2			SMDU2 Battery2		SMDU2 Batería2			SMDU2 Batería2


151		32			15			Battery 1			Batt 1			Batería 1			Bat 1
152		32			15			Battery 2			Batt 2			Batería 2			Bat 2
153		32			15			Battery 3			Batt 3			Batería 3			Bat 3
154		32			15			Battery 4			Batt 4			Batería 4			Bat 4
155		32			15			Battery 5			Batt 5			Batería 5			Bat 5
