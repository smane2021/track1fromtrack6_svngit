﻿#
# Locale language support: Spanish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Solar Converter Group			Solar ConvGrp		Grupo Convertidor Solar			Grpo Conv Solar
2		32			15			Total Current				Total Current		Corriente Total				Corriente Total
3		32			15			Average Voltage				Average Voltage		Tensión media				Tensión media
4		32			15			System Capacity Used			Sys Cap Used		Capacidad utilizada			Cap usada
5		32			15			Maximum Used Capacity			Max Cap Used		Máxima Capacidad utilizada		Max Cap Usada
6		32			15			Minimum Used Capacity			Min Cap Used		Mínima Capacidad utilizada		Min Cap Usada
7		36			15			Total Solar Converter Communicating	Num Solar Convs		Total Convertidores en comunicación	Num Conv Solar
8		32			15			Valid Solar Converter			Valid SolarConv		Convertidores válidos			ConvSol válidos
9		32			15			Number of Solar Converters		SolConv Num		Número de convertidores solares		Núm ConvSolar
11		34			15			Solar Converter(s) Failure Status	SolarConv Fail		Solar Converter(s) Failure Status	SolarConv Fail
12		32			15			Solar Converter Current Limit		SolConvCurrLim		Solar Converter Current Limit		SolConvCurrLim
13		32			15			Solar Converter Trim			Solar Conv Trim		Solar Converter Trim			Solar Conv Trim
14		32			15			DC On/Off Control			DC On/Off Ctrl		DC On/Off Control			DC On/Off Ctrl
16		32			15			Solar Converter LED Control		SolConv LEDCtrl		Solar Converter LED Control		SolConv LEDCtrl
17		32			15			Fan Speed Control			Fan Speed Ctrl		Fan Speed Control			Fan Speed Ctrl
18		32			15			Rated Voltage				Rated Voltage		Rated Voltage				Rated Voltage
19		32			15			Rated Current				Rated Current		Rated Current				Rated Current
20		32			15			High Voltage Shutdown Limit		HVSD Limit		High Voltage Shutdown Limit		HVSD Limit
21		32			15			Low Voltage Limit			Low Volt Limit		Low Voltage Limit			Low Volt Limit
22		32			15			High Temperature Limit			High Temp Limit		High Temperature Limit			High Temp Limit
24		32			15			HVSD Restart Time			HVSD Restart T		HVSD Restart Time			HVSD Restart T
25		32			15			Walk-In Time				Walk-In Time		Walk-In Time				Walk-In Time
26		32			15			Walk-In					Walk-In			Walk-In					Walk-In
27		32			15			Minimum Redundancy			Min Redundancy		Redundancia mínima			Redund Mín
28		32			15			Maximum Redundancy			Max Redundancy		Redundancia maxima			Redund Max
29		32			15			Turn Off Delay				Turn Off Delay		Desactivar Retraso			DesactRetraso
30		32			15			Cycle Period				Cycle Period		Periodo Ciclo				PeriodoCiclo
31		32			15			Cycle Activation Time			Cyc Active Time		CicloTiemActiv			CicloTiemActiv
33		32			15			Multiple Solar Converter Failure	MultSolConvFail		Fallo múltiple Convertidor		MultiFallo Conv
36		32			15			Normal					Normal			Normal					Normal
37		32			15			Failure					Failure			Fallo					Fallo
38		32			15			Switch Off All				Switch Off All		Apagar todo			Apagar todo
39		32			15			Switch On All				Switch On All		EncenderTodos		EncenderTodos
42		32			15			All Flashing				All Flashing		TodoBrillante		TodoBrillante
43		32			15			Stop Flashing				Stop Flashing		PararBrillante		PararBrillan
44		32			15			Full Speed				Full Speed		Full Speed				Full Speed
45		32			15			Automatic Speed				Auto Speed		Automatic Speed				Auto Speed
46		32			32			Current Limit Control			Curr Limit Ctrl		CtrlLmtCorr			CtrlLmtCorr
47		32			32			Full Capability Control			Full Cap Ctrl		ControlCapTotal		CtrlCapTotal	
54		32			15			Disabled				Disabled		No					No
55		32			15			Enabled					Enabled			Sí					Sí
68		32			15			ECO Mode				ECO Mode		ECO Modo				ECO Modo
73		32			15			No					No			No					No
74		32			15			Yes					Yes			Sí					Sí
77		32			15			Pre-CurrLimit Turn-On			Pre-Curr Limit		Límite Pre-Curr Encendido	LímPre-Curr
78		32			15			Solar Converter Power Type		SolConv PowType		Conv Energía solar Tipo		ConvEnerSolTipo
79		32			15			Double Supply				Double Supply		DobleSuministro			DobleSumini
80		32			15			Single Supply				Single Supply		SuministroUnico			SuminiUnico
81		32			15			Last Solar Converter Quantity		LastSolConvQty		Última ConverSolar Cant		ÚltConvSolCant
83		32			15			Solar Converter Lost			Solar Conv Lost		Convertidor Solar perdido		CvSolarPerdido
84		32			15			Clear Solar Converter Lost Alarm	Clear Conv Lost		BorrarConvSolarPerdidoAlm	BorrConvPerdi
85		32			15			Clear					Clear			Claro					Claro
86		32			15			Confirm Solar Converter ID		Confirm ID		Confirmar IDConvSolar		Confirmar ID
87		32			15			Confirm					Confirm			Confirmar				Confirmar
88		32			15			Best Operating Point			Best Oper Point		MejorPuntoOper			MejorPuntoOper
89		32			15			Solar Converter Redundancy		Solar Conv Red		Redundancia Conv Solar	RedundConvSola
90		32			15			Load Fluctuation Range			Fluct Range		Rango Fluctuación Carga		Rango Fluct
91		32			15			System Energy Saving Point		Energy Save Pt		Sistema Ahorro EnergPoint		AhorroEnergPt
92		32			15			E-Stop Function				E-Stop Function		Función E-Stop				Función E-Stop
94		32			15			Single Phase				Single Phase		Fase única				Fase única
95		32			15			Three Phases				Three Phases		Tres fases				Tres fases
96		32			15			Input Current Limit			Input Curr Lmt		LímCorrEntrada			LímCorrEntrada
97		32			15			Double Supply				Double Supply		Doble suministro		DobleSuminis
98		32			15			Single Supply				Single Supply		Suministro único		SuminisUnico
99		32			15			Small Supply				Small Supply		Pequeño suministro		PequeSuminis
100		32			15			Restart on HVSD				Restart on HVSD		Reiniciar en HVSD				Reinici en HVSD
101		32			15			Sequence Start Interval			Start Interval		Iniciar Intervalo			Iniciar Interv
102		32			15			Rated Voltage				Rated Voltage		Tensión nominal				Tensión nominal
103		32			15			Rated Current				Rated Current		Corr nominal				Corr nominal
104		32			15			All Solar Converters Comm Fail		SolarCv ComFail		All Solar Converters Comm Fail		SolarCv ComFail
105		32			15			Inactive				Inactive		No					No
106		32			15			Active					Active			Activo					Activo
112		32			15			Rated Voltage (Internal Use)		Rated Voltage		Rated Voltage (Internal Use)		Rated Voltage
113		32			15			Solar Convert InfoChg (Internal)	SolarInfoChange		Internal Use				Internal Use
114		32			15			MixHE Power				MixHE Power		MixHE Power				MixHE Power
115		32			15			Derated					Derated			Reducido				Reducido
116		32			15			Non-Derated				Non-Derated		No reducido				No Reducido
117		32			15			All Solar Converters On Time		SolarConvONTime		Todos ConvSolaresTiempo			ConvSolTiempo
118		32			15			All Solar Converters are On		AllSolarConvOn		Todos ConvSolEstán Activ			TodoConvSolActi
119		39			15			Clear Solar Converter Comm Fail Alarm	Clear Comm Fail		Claro Alarma solarConvFalloCom	Claro Fallo Com
120		32			15			HVSD					HVSD			HVSD					HVSD
121		32			15			HVSD Voltage Difference			HVSD Volt Diff		HVSD Diferencia Volt				HVSDDiferVolt
122		32			15			Total Rated Current			Total Rated Cur		Corriente nominal total		CorrNomTotal
123		32			15			Diesel Generator Power Limit		DG Pwr Lmt		Límite Potencia DG			LmtPotenciaDG
124		32			15			Diesel Generator Digital Input		Diesel DI Input		Gen Diesel Entrada DI		DieselEntradaDI
125		32			15			Diesel Gen Power Limit Point Set	DG Pwr Lmt Pt		Punto Ajuste LímDieselGenPower				DG Pwr Lmt Pt
126		32			15			None					None			No					No
140		32			15			Solar Converter Delta Voltage		SolConvDeltaVol		Solar Converter Delta Voltage		SolConvDeltaVol
141		32			15			Solar Status				Solar Status		Solar Status				Solar Status
142		32			15			Day					Day			Día					Día
143		32			15			Night					Night			Noche					Noche
144		32			15			Existence State				Existence State		Detección				Detección
145		32			15			Existent				Existent		Existe					Existe
146		32			15			Not Existent				Not Existent		No existe				No existe
147		32			15			Total Input Current			Input Current		Total Corriente Entrada			Corriente Ent
148		32			15			Total Input Power			Input Power		Total Potencia Entrada			Potencia Ent
149		32			15			Total Rated Current			Total Rated Cur		Corriente Total Estimada		Total Corr Est
150		32			15			Total Output Power			Output Power		Total Potencia Salida			Potencia Salida
151		32			15			Reset Solar Converter IDs		Reset Solar IDs		Reiniciar ID convertidores Solares	Res ID CV Solar
152		32			15			Clear Solar Converter Comm Fail		ClrSolarCommFail	Clear Solar Converter Comm Fail		ClrSolarCommFail
