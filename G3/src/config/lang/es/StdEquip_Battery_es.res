﻿#
# Locale language support: Spanish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Current					Current			Corriente Batería			Corriente Bat
2		32			15			Capacity (Ah)				Capacity(Ah)		Capacidad (Ah)				Capacidad (Ah)
3		32			15			Current Limit Exceeded			Curr Lmt Exceed		Limite corriente excedido		Lim corr pasado
4		32			15			Battery					Battery			Batería					Batería
5		32			15			Over Battery Current			Over Current		Sobrecorriente				Sobrecorriente
6		32			15			Capacity (%)				Capacity(%)		Capacidad batería (%)			Cap Bat (%)
7		32			15			Voltage					Voltage			Tensión batería				Tensión batería
8		32			15			Low Capacity				Low Capacity		Baja capacidad				Baja capacidad
9		32			15			Battery Fuse Failure			Fuse Failure		Fallo fusible batería			Fallo fus Bat
10		32			15			DC Distribution Seq Num			DC Distr Seq No		Num Seq de distribución			NSeq Distrib
11		32			15			Battery Overvoltage			Overvolt		Sobretensión				Sobretensión
12		32			15			Battery Undervoltage			Undervolt		Subtensión				Subtensión
13		32			15			Battery Overcurrent			Overcurr		Sobrecorriente				Sobrecorriente
14		32			15			Battery Fuse Failure			Fuse Failure		Fallo fusible batería			Fallo fus Bat
15		32			15			Battery Overvoltage			Overvolt		Sobretensión				Sobretensión
16		32			15			Battery Undervoltage			Undervolt		Subtensión				Subtensión
17		32			15			Battery Over Current			Over Curr		Sobrecorriente				Sobrecorriente
18		32			15			Battery					Battery			Batería					Batería
19		32			15			Batt Sensor Coeffi			Batt Coeff		Coeficiente sensor Bat			Coef Sens Bat
20		32			15			Over Voltage Setpoint			Over Volt Point		Nivel sobretensión			Sobretensión
21		32			15			Low Voltage Setpoint			Low Volt Point		Nivel subtensión			Subtensión
22		32			15			Communication Failure			Comm Fail		Batería no responde			Bat no resp
23		32			15			Communication OK			Comm OK			Respuesta				Respuesta
24		32			15			Communication Failure			Comm Fail		No responde				No responde
25		32			15			Communication Failure			Comm Fail		No responde				No responde
26		32			15			Shunt Full Current			Shunt Current		Corriente Shunt				Corr Shunt
27		32			15			Shunt Full Voltage			Shunt Voltage		Tensión Shunt				Tens Shunt
28		32			15			Used By Battery Management		Batt Manage		Utilizada en Gestión Bat		Incl Gestión
29		32			15			Yes					Yes			Sí					Sí
30		32			15			No					No			No					No
31		32			15			On					On			Conectado				Conectado
32		32			15			Off					Off			Apagado					Apagado
33		32			15			State					State			Estado					Estado
44		32			15			Used Temperature Sensor			Used Sensor		Sensor Temp utilizado			SensT usado
87		32			15			None					None			Ninguno					Ninguno
91		32			15			Temperature Sensor 1			Sensor 1		Sensor temperatura 1			Sens Temp 1
92		32			15			Temperature Sensor 2			Sensor 2		Sensor temperatura 2			Sens Temp 2
93		32			15			Temperature Sensor 3			Sensor 3		Sensor temperatura 3			Sens Temp 3
94		32			15			Temperature Sensor 4			Sensor 4		Sensor temperatura 4			Sens Temp 4
95		32			15			Temperature Sensor 5			Sensor 5		Sensor temperatura 5			Sens Temp 5
96		32			15			Rated Capacity				Rated Capacity		Capacidad nominal C10			Capacidad C10
97		32			15			Battery Temperature			Battery Temp		Temperatura de Batería			Temp Batería
98		32			15			Battery Temperature Sensor		BattTempSensor		Sensor Temperatura Batería		Sensor Temp
99		32			15			None					None			Ninguno					Ninguno
100		32			15			Temperature 1				Temp 1			Temperatura 1				Temp 1
101		32			15			Temperature 2				Temp 2			Temperatura 2				Temp 2
102		32			15			Temperature 3				Temp 3			Temperatura 3				Temp 3
103		32			15			Temperature 4				Temp 4			Temperatura 4				Temp 4
104		32			15			Temperature 5				Temp 5			Temperatura 5				Temp 5
105		32			15			Temperature 6				Temp 6			Temperatura 6				Temp 6
106		32			15			Temperature 7				Temp 7			Temperatura 7				Temp 7
107		32			15			Temperature 8				Temp 8			Temperatura 8				Temp 8
108		32			15			Temperature 9				Temp 9			Temperatura 9				Temp 9
109		32			15			Temperature 10				Temp 10			Temperatura 10				Temp 10
500	32			15			Current Break Size			Curr1 Brk Size				Corr1Tamaño ruptura				Corr1TamaRupt	
501	32			15			Current High 1 Current Limit		Curr1 Hi1 Lmt		Corr1Alta 1 LmtCorr				Corr1Alta1Lmt	
502	32			15			Current High 2 Current Limit		Curr1 Hi2 Lmt		Corr1Alta 2 LmtCorr				Corr1Alta2Lmt	
503	32			15			Battery Current High 1 Curr		BattCurr Hi1Cur			BateríaCorr1 Alta 1 Corr				BatCor1Alta1Cor		
504	32			15			Battery Current High 2 Curr		BattCurr Hi2Cur			BateríaCorr1 Alta 2 Corr				BatCor1Alta2Cor		
505	32			15			Battery 1						Battery 1				Batería 1						Batería 1		
506	32			15			Battery 2							Battery 2			Batería 2						Batería 2		
