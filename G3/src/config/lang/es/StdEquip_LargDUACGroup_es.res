﻿#
# Locale language support: Spanish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			AC Distribution Number			AC Distr No.		Número Distribución CA			Núm Distr CA
2		32			15			Large DU AC Distribution Group		LargDU AC Group		Grupo Distribución CA grande		Grupo DistCA-G
3		32			15			Overvoltage Limit			Overvolt Limit		Límite sobretensión			Lim Sobretens
4		32			15			Undervoltage Limit			Undervolt Limit		Nivel de subtensión			Lim Subtens
5		32			15			Phase Failure Voltage			Phase Fail Volt		Fallo tensión de fase			Fallo V fase
6		32			15			Overfrequency Limit			Overfreq Limit		Límite Alta frecuencia			Lim Alta Frec
7		32			15			Underfrequency Limit			Underfreq Limit		Límite Baja frecuencia			Lim Baja Frec
8		32			15			Mains Failure				Mains Failure		Fallo de Red				Fallo Red
9		32			15			Normal					Normal			Normal					Normal
10		32			15			Alarm					Alarm			Alarma					Alarma
11		32			15			Mains Failure				Mains Failure		Fallo de Red				Fallo Red
12		32			15			Existence State				Existence State		Detección				Detección
13		32			15			Existent				Existent		Existente				Existente
14		32			15			Non-Existent				Non-Existent		No existente				No existente
