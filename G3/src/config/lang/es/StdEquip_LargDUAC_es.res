﻿#
# Locale language support: Spanish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Manual					Manual			Manual					Manual
2		32			15			Auto					Auto			Auto					Auto
3		32			15			Off					Off			No					No
4		32			15			On					On			Sí					Sí
5		32			15			No Input				No Input		Sin entrada				Sin entrada
6		32			15			Input 1					Input 1			Entrada 1				Entrada 1
7		32			15			Input 2					Input 2			Entrada 2				Entrada 2
8		32			15			Input 3					Input 3			Entrada 3				Entrada 3
9		32			15			No Input				No Input		Sin entrada				Sin entrada
10		32			15			Input					Input			Entrada					Entrada
11		32			15			Close					Close			Cerrado					Cerrado
12		32			15			Open					Open			Abierto					Abierto
13		32			15			Close					Close			Cerrado					Cerrado
14		32			15			Open					Open			Abierto					Abierto
15		32			15			Close					Close			Cerrado					Cerrado
16		32			15			Open					Open			Abierto					Abierto
17		32			15			Close					Close			Cerrado					Cerrado
18		32			15			Open					Open			Abierto					Abierto
19		32			15			Close					Close			Cerrado					Cerrado
20		32			15			Open					Open			Abierto					Abierto
21		32			15			Close					Close			Cerrado					Cerrado
22		32			15			Open					Open			Abierto					Abierto
23		32			15			Close					Close			Cerrado					Cerrado
24		32			15			Open					Open			Abierto					Abierto
25		32			15			Close					Close			Cerrado					Cerrado
26		32			15			Open					Open			Abierto					Abierto
27		32			15			1-Phase					1-Phase			Monofásico				Monofásico
28		32			15			3-Phase					3-Phase			Trifásico				Trifásico
29		32			15			No Measurement				No Measurement		Ninguno					Ninguno
30		32			15			1-Phase					1-Phase			Monofásico				Monofásico
31		32			15			3-Phase					3-Phase			Trifásico				Trifásico
32		32			15			Response				Response		Respuesta				Respuesta
33		32			15			Not Responding				Not Responding		No responde				No responde
34		32			15			AC Distribution				AC Distribution		Distribución CA				Distribución CA
35		32			15			Mains 1 Uab/Ua				1 Uab/Ua		Red 1 Vrs/Vr				Red 1 Vrs/Vr
36		32			15			Mains 1 Ubc/Ub				1 Ubc/Ub		Red 1 Vst/Vs				Red 1 Vst/Vs
37		32			15			Mains 1 Uca/Uc				1 Uca/Uc		Red 1 Vrt/Vt				Red 1 Vrt/Vt
38		32			15			Mains 2 Uab/Ua				2 Uab/Ua		Red 2 Vrs/Vr				Red 2 Vrs/Vr
39		32			15			Mains 2 Ubc/Ub				2 Ubc/Ub		Red 2 Vst/Vs				Red 2 Vst/Vs
40		32			15			Mains 2 Uca/Uc				2 Uca/Uc		Red 2 Vrt/Vt				Red 2 Vrt/Vt
41		32			15			Mains 3 Uab/Ua				3 Uab/Ua		Red 3 Vrs/Vr				Red 3 Vrs/Vr
42		32			15			Mains 3 Ubc/Ub				3 Ubc/Ub		Red 3 Vst/Vs				Red 3 Vst/Vs
43		32			15			Mains 3 Uca/Uc				3 Uca/Uc		Red 3 Vrt/Vt				Red 3 Vrt/Vt
53		32			15			Working Phase A Current			Phase A Curr		Corriente Fase R			Corr Fase R
54		32			15			Working Phase B Current			Phase B Curr		Corriente Fase S			Corr Fase S
55		32			15			Working Phase C Current			Phase C Curr		Corriente Fase T			Corr Fase T
56		32			15			AC Input Frequency			AC Input Freq		Frecuencia Entrada CA			Frec Entr CA
57		32			15			AC Input Switch Mode			AC Switch Mode		Modo conmutador Entrada CA		Modo Entr CA
58		32			15			Fault Lighting Status			Fault Lighting		Fallo iluminación			Fallo ilumin
59		32			15			Mains 1 Input Status			1 Input Status		Estado Entrada Red 1			Estado Ent Red1
60		32			15			Mains 2 Input Status			2 Input Status		Estado Entrada Red 2			Estado Ent Red2
61		32			15			Mains 3 Input Status			3 Input Status		Estado Entrada Red 3			Estado Ent Red3
62		32			15			AC Output 1 Status			Output 1 Status		Estado Salida CA 1			Estado Sal CA 1
63		32			15			AC Output 2 Status			Output 2 Status		Estado Salida CA 2			Estado Sal CA 2
64		32			15			AC Output 3 Status			Output 3 Status		Estado Salida CA 3			Estado Sal CA 3
65		32			15			AC Output 4 Status			Output 4 Status		Estado Salida CA 4			Estado SAl CA 4
66		32			15			AC Output 5 Status			Output 5 Status		Estado Salida CA 5			Estado Sal CA 5
67		32			15			AC Output 6 Status			Output 6 Status		Estado Salida CA 6			Estado Sal CA 6
68		32			15			AC Output 7 Status			Output 7 Status		Estado Salida CA 7			Estado Sal CA 7
69		32			15			AC Output 8 Status			Output 8 Status		Estado Salida CA 8			Estado Sal CA 8
70		32			15			AC Input Frequency High			Frequency High		Alta frecuencia Entrada CA		Alta frecuencia
71		32			15			AC Input Frequency Low			Frequency Low		Baja frecuencia Entrada CA		Baja frecuencia
72		32			15			AC Input MCCB Trip			Input MCCB Trip		Entrada Trip MCCB CA			Ent Trip MCCB
73		32			15			SPD Trip				SPD Trip		Trip SPD				Trip SPD
74		32			15			AC Output MCCB Trip			OutputMCCB Trip		Salida Trip MCCB CA			Sal Trip MCCB
75		32			15			AC Input 1 Failure			Input 1 Failure		Fallo entrada CA 1			Fallo Ent1 CA
76		32			15			AC Input 2 Failure			Input 2 Failure		Fallo entrada CA 2			Fallo Ent2 CA
77		32			15			AC Input 3 Failure			Input 3 Failure		Fallo entrada CA 3			Fallo Ent3 CA
78		32			15			Mains 1 Uab/Ua Low			M1 Uab/a UnderV		Red 1 Baja Vrs/Vr			RD1 Baja Vrs/Vr
79		32			15			Mains 1 Ubc/Ub Low			M1 Ubc/b UnderV		Red 1 Baja Vst/Vs			RD1 Baja Vst/Vs
80		32			15			Mains 1 Uca/Uc Low			M1 Uca/c UnderV		Red 1 Baja Vrt/Vt			RD1 Baja Vrt/Vt
81		32			15			Mains 2 Uab/Ua Low			M2 Uab/a UnderV		Red 2 Baja Vrs/Vr			RD2 Baja Vrs/Vr
82		32			15			Mains 2 Ubc/Ub Low			M2 Ubc/b UnderV		Red 2 Baja Vst/Vs			RD2 Baja Vst/Vs
83		32			15			Mains 2 Uca/Uc Low			M2 Uca/c UnderV		Red 2 Baja Vrt/Vt			RD2 Baja Vrt/Vt
84		32			15			Mains 3 Uab/Ua Low			M3 Uab/a UnderV		Red 3 Baja Vrs/Vr			RD3 Baja Vrs/Vr
85		32			15			Mains 3 Ubc/Ub Low			M3 Ubc/b UnderV		Red 3 Baja Vst/Vs			RD3 Baja Vst/Vs
86		32			15			Mains 3 Uca/Uc Low			M3 Uca/c UnderV		Red 3 Baja Vrt/Vt			RD3 Baja Vrt/Vt
87		32			15			Mains 1 Uab/Ua High			M1 Uab/a OverV		Red 1 Alta Vrs/Vr			RD1 Alta Vrs/Vr
88		32			15			Mains 1 Ubc/Ub High			M1 Ubc/b OverV		Red 1 Alta Vst/Vs			RD1 Alta Vst/Vs
89		32			15			Mains 1 Uca/Uc High			M1 Uca/c OverV		Red 1 Alta Vrt/Vt			RD1 Alta Vrt/Vt
90		32			15			Mains 2 Uab/Ua High			M2 Uab/a OverV		Red 2 Alta Vrs/Vr			RD2 Alta Vrs/Vr
91		32			15			Mains 2 Ubc/Ub High			M2 Ubc/b OverV		Red 2 Alta Vst/Vs			RD2 Alta Vst/Vs
92		32			15			Mains 2 Uca/Uc High			M2 Uca/c OverV		Red 2 Alta Vrt/Vt			RD2 Alta Vrt/Vt
93		32			15			Mains 3 Uab/Ua High			M3 Uab/a OverV		Red 3 Alta Vrs/Vr			RD3 Alta Vrs/Vr
93		32			15			Mains 3 Ubc/Ub High			M3 Ubc/b OverV		Red 3 Alta Vst/Vs			RD3 Alta Vst/Vs
94		32			15			Mains 3 Uca/Uc High			M3 Uca/c OverV		Red 3 Alta Vrt/Vt			RD3 Alta Vrt/Vt
95		32			15			Mains 1 Uab/Ua Failure			M1 Uab/a Fail		Fallo Red1 Vrs/Vr			FalloRed1 RS/R
96		32			15			Mains 1 Ubc/Ub Failure			M1 Ubc/b Fail		Fallo Red1 Vst/Vs			FalloRed1 ST/S
97		32			15			Mains 1 Uca/Uc Failure			M1 Uca/c Fail		Fallo Red1 Vrt/Vt			FalloRed1 RT/T
98		32			15			Mains 2 Uab/Ua Failure			M2 Uab/a Fail		Fallo Red2 Vrs/Vr			FalloRed2 RS/R
99		32			15			Mains 2 Ubc/Ub Failure			M2 Ubc/b Fail		Fallo Red2 Vst/Vs			FalloRed2 ST/S
100		32			15			Mains 2 Uca/Uc Failure			M2 Uca/c Fail		Fallo Red2 Vrt/Vt			FalloRed2 RT/T
101		32			15			Mains 3 Uab/Ua Failure			M3 Uab/a Fail		Fallo Red3 Vrs/Vr			FalloRed3 RS/R
102		32			15			Mains 3 Ubc/Ub Failure			M3 Ubc/b Fail		Fallo Red3 Vst/Vs			FalloRed3 ST/S
103		32			15			Mains 3 Uca/Uc Failure			M3 Uca/c Fail		Fallo Red3 Vrt/Vt			FalloRed3 RT/T
104		32			15			No Response				No Response		No responde				No responde
105		32			15			Overvoltage Limit			Overvolt Limit		Nivel de sobretensión			Nivel Sobretens
106		32			15			Undervoltage Limit			Undervolt Limit		Nivel de subtensión			Nivel Subtens
107		32			15			Phase Failure Voltage			Phase Fail Volt		Tensión Fallo Fase			V Fallo Fase
108		32			15			Overfrequency Limit			Overfreq Limit		Límite de Alta Frecuencia		Lim Alta Frec
109		32			15			Underfrequency Limit			Underfreq Limit		Límite de Baja Frecuencia		Lim Baja Frec
110		32			15			Current Transformer Coeff		Curr Trans Coef		Coeficiente Transformador		Coef corriente
111		32			15			Input Type				Input Type		Tipo de Entrada				Tipo Entrada
112		32			15			Input Num				Input Num		Número de Entrada			Num Entrada
113		32			15			Current Measurement			Curr Measure		Medida de corriente			Medida Corr
114		32			15			Output Num				Output Num		Número de Salida			Núm Salida
115		32			15			Distribution Address			Distr Addr		Dirección de Distribución		Dir Distrib
116		32			15			Mains 1 Failure				Mains 1 Fail		Fallo de Red 1				Fallo Red 1
117		32			15			Mains 2 Failure				Mains 2 Fail		Fallo de Red 2				Fallo Red 2
118		32			15			Mains 3 Failure				Mains 3 Fail		Fallo de Red 3				Fallo Red 3
119		32			15			Mains 1 Uab/Ua Failure			M1 Uab/a Fail		Fallo Red1 Vrs/Vr			FalloRed1 RS/R
120		32			15			Mains 1 Ubc/Ub Failure			M1 Ubc/b Fail		Fallo Red1 Vst/Vs			FalloRed1 ST/S
121		32			15			Mains 1 Uca/Uc Failure			M1 Uca/c Fail		Fallo Red1 Vrt/Vt			FalloRed1 RT/T
122		32			15			Mains 2 Uab/Ua Failure			M2 Uab/a Fail		Fallo Red2 Vrs/Vr			FalloRed2 RS/R
123		32			15			Mains 2 Ubc/Ub Failure			M2 Ubc/b Fail		Fallo Red2 Vst/Vs			FalloRed2 ST/S
124		32			15			Mains 2 Uca/Uc Failure			M2 Uca/c Fail		Fallo Red2 Vrt/Vt			FalloRed2 RT/T
125		32			15			Mains 3 Uab/Ua Failure			M3 Uab/a Fail		Fallo Red3 Vrs/Vr			FalloRed3 RS/R
126		32			15			Mains 3 Ubc/Ub Failure			M3 Ubc/b Fail		Fallo Red3 Vst/Vs			FalloRed3 ST/S
127		32			15			Mains 3 Uca/Uc Failure			M3 Uca/c Fail		Fallo Red3 Vrt/Vt			FalloRed3 RT/T
128		32			15			Overfrequency				Overfrequency		Alta Frecuencia				Alta Frecuencia
129		32			15			Underfrequency				Underfrequency		Baja Frecuencia				Baja Frecuencia
130		32			15			Mains 1 Uab/Ua UnderVoltage		M1 Uab/a UnderV		Subtensión Red1 R-S/R			SubV Red1 RS/R
131		32			15			Mains 1 Ubc/Ub UnderVoltage		M1 Ubc/b UnderV		Subtensión Red1 S-T/S			SubV Red1 ST/S
132		32			15			Mains 1 Uca/Uc UnderVoltage		M1 Uca/c UnderV		Subtensión Red1 R-T/T			SubV Red1 RT/T
133		32			15			Mains 2 Uab/Ua UnderVoltage		M2 Uab/a UnderV		Subtensión Red2 R-S/R			SubV Red2 RS/R
134		32			15			Mains 2 Ubc/Ub UnderVoltage		M2 Ubc/b UnderV		Subtensión Red2 S-T/S			SubV Red2 ST/S
135		32			15			Mains 2 Uca/Uc UnderVoltage		M2 Uca/c UnderV		Subtensión Red2 R-T/T			SubV Red2 RT/T
136		32			15			Mains 3 Uab/Ua UnderVoltage		M3 Uab/a UnderV		Subtensión Red3 R-S/R			SubV Red3 RS/R
137		32			15			Mains 3 Ubc/Ub UnderVoltage		M3 Ubc/b UnderV		Subtensión Red3 S-T/S			SubV Red3 ST/S
138		32			15			Mains 3 Uca/Uc UnderVoltage		M3 Uca/c UnderV		Subtensión Red3 R-T/T			SubV Red3 RT/T
139		32			15			Mains 1 Uab/Ua OverVoltage		M1 Uab/a OverV		SObretensión Red1 R-S/R			SobrV Red1 RS/R
140		32			15			Mains 1 Ubc/Ub OverVoltage		M1 Ubc/b OverV		Sobretensión Red1 S-T/S			SobrV Red1 ST/S
141		32			15			Mains 1 Uca/Uc OverVoltage		M1 Uca/c OverV		Sobretensión Red1 R-T/T			SobrV Red1 RT/T
142		32			15			Mains 2 Uab/Ua OverVoltage		M2 Uab/a OverV		SObretensión Red2 R-S/R			SobrV Red2 RS/R
143		32			15			Mains 2 Ubc/Ub OverVoltage		M2 Ubc/b OverV		Sobretensión Red2 S-T/S			SobrV Red2 ST/S
144		32			15			Mains 2 Uca/Uc OverVoltage		M2 Uca/c OverV		Sobretensión Red2 R-T/T			SobrV Red2 RT/T
145		32			15			Mains 3 Uab/Ua OverVoltage		M3 Uab/a OverV		SObretensión Red3 R-S/R			SobrV Red3 RS/R
146		32			15			Mains 3 Ubc/Ub Over Voltage		M3 Ubc/b OverV		Sobretensión Red3 S-T/S			SobrV Red3 ST/S
147		32			15			Mains 3 Uca/Uc Over Voltage		M3 Uca/c OverV		Sobretensión Red3 R-T/T			SobrV Red3 RT/T
148		32			15			AC Input MCCB Trip			In-MCCB Trip		Entrada CA disparo MCCB			EntDisparo MCCB
149		32			15			AC Output MCCB Trip			Out-MCCB Trip		Salida CA disparo MCCB			SalDisparo MCCB
150		32			15			SPD Trip				SPD Trip		Disparo SPD				Disparo SPD
169		32			15			No Response				No Response		No responde				No responde
170		32			15			Mains Failure				Mains Failure		Fallo de Red				Fallo de Red
171		32			15			Large AC Distribution Unit		Large AC Dist		Unidad Distr CA grande			Distr CA grande
172		32			15			No Alarm				No Alarm		Sin alarmas				Sin alarmas
173		32			15			Overvoltage				Overvolt		Sobretensión				Sobretensión
174		32			15			Undervoltage				Undervolt		Subtensión				Subtensión
175		32			15			AC Phase Failure			AC Phase Fail		Fallo Fase CA				Fallo Fase CA
176		32			15			No Alarm				No Alarm		Sin alarmas				Sin Alarmas
177		32			15			Overfrequency				Overfrequency		Alta frecuencia				Alta frecuencia
178		32			15			Underfrequency				Underfrequency		Baja frecuencia				Baja frecuencia
179		32			15			No Alarm				No Alarm		Sin alarmas				Sin alarmas
180		32			15			AC Overvoltage				AC Overvolt		Sobretensión CA				Sobretensión CA
181		32			15			AC Undervoltage				AC Undervolt		Subtensión CA				Subtensión CA
182		32			15			AC Phase Failure			AC Phase Fail		Fallo Fase CA				Fallo Fase CA
183		32			15			No Alarm				No Alarm		Sin Alarmas				Sin Alarmas
184		32			15			AC Overvoltage				AC Overvolt		Sobretensión CA				Sobretensión CA
185		32			15			AC Undervoltage				AC Undervolt		Subtensión CA				Subtensión CA
186		32			15			AC Phase Failure			AC Phase Fail		Fallo Fase CA				Fallo Fase CA
187		32			15			Mains 1 Uab/Ua Alarm			M1 Uab/a Alarm		Alarma Red 1 Vrs/Vr			Alarm Red1 RS/R
188		32			15			Mains 1 Ubc/Ub Alarm			M1 Ubc/b Alarm		Alarma Red 1 Vst/Vs			Alarm Red1 ST/S
189		32			15			Mains 1 Uca/Uc Alarm			M1 Uca/c Alarm		Alarma Red 1 Vrt/Vt			Alarm Red1 RT/T
190		32			15			Frequency Alarm				Freq Alarm		Alarma Frecuencia			Alarma Frec
191		32			15			No Response				No Response		No responde				No responde
192		32			15			Normal					Normal			Normal					Normal
193		32			15			Failure					Failure			Fallo					Fallo
194		32			15			No Response				No Response		No responde				No responde
195		32			15			Mains Input No				Mains Input No		Núm Entrada Red				Núm Ent Red
196		32			15			No. 1					No. 1			N 1					N 1
197		32			15			No. 2					No. 2			N 2					N 2
198		32			15			No. 3					No. 3			N 3					N 3
199		32			15			None					None			Ninguna					Ninguna
200		32			15			Emergency Light				Emergency Light		Luz de emergencia			Luz emergencia
201		32			15			Close					Close			No					No
202		32			15			Open					Open			Sí					Sí
203		32			15			Mains 2 Uab/Ua Alarm			M2 Uab/a Alarm		Alarma Red 2 Vrs/Vr			Alarm Red2 RS/R
204		32			15			Mains 2 Ubc/Ub Alarm			M2 Ubc/b Alarm		Alarma Red 2 Vst/Vs			Alarm Red2 ST/S
205		32			15			Mains 2 Uca/Uc Alarm			M2 Uca/c Alarm		Alarma Red 2 Vrt/Vt			Alarm Red2 RT/T
206		32			15			Mains 3 Uab/Ua Alarm			M3 Uab/a Alarm		Alarma Red 3 Vrs/Vr			Alarm Red3 RS/R
207		32			15			Mains 3 Ubc/Ub Alarm			M3 Ubc/b Alarm		Alarma Red 3 Vst/Vs			Alarm Red3 ST/S
208		32			15			Mains 3 Uca/Uc Alarm			M3 Uca/c Alarm		Alarma Red 3 Vrt/Vt			Alarm Red3 RT/T
209		32			15			Normal					Normal			Normal					Normal
210		32			15			Alarm					Alarm			Alarma					Alarma
211		32			15			AC Fuse Number				AC Fuse No.		Núm de Fusible CA			Núm Fusible CA
212		32			15			Existence State				Existence State		Detección				Detección
213		32			15			Existent				Existent		Existente				Existente
214		32			15			Non-Existent				Non-Existent		No existente				No existente
