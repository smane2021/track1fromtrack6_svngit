﻿#
# Locale language support: Spanish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			LVD 3 Unit				LVD 3			LVD3					LVD3
11		32			15			Connected				Connected		Cerrado					Cerrado
12		32			15			Disconnected				Disconnected		Abierto					Abierto
13		32			15			No					No			No					No
14		32			15			Yes					Yes			Sí					Sí
21		32			15			LVD3 Status				LVD3 Status		Estado LVD3				Estado LVD3
22		32			15			LVD2 Status				LVD2 Status		Estado LVD2				Estado LVD2
23		32			15			LVD3 Failure				LVD3 Failure		Fallo LVD3				Fallo LVD3
24		32			15			LVD2 Failure				LVD2 Failure		Fallo LVD2				Fallo LVD2
25		32			15			Communication Failure			Comm Failure		Fallo comunicación			Fallo Com
26		32			15			State					State			Estado					Estado
27		32			15			LVD3 Control				LVD3 Control		Control LVD3				Control LVD3
28		32			15			LVD2 Control				LVD2 Control		Control LVD2				Control LVD2
31		32			15			LVD3					LVD3			Desconexión LVD3			Función LVD3
32		32			15			LVD3 Mode				LVD3 Mode		Modo LVD3				Modo LVD3
33		32			15			LVD3 Voltage				LVD3 Voltage		Tensión LVD3				Tensión LVD3
34		32			15			LVD3 Reconnect Voltage			LVD3 ReconnVolt		Tensión reconexión LVD3			Tens recon LVD3
35		32			15			LVD3 Reconnect Delay			LVD3 ReconDelay		Retardo reconexión LVD3			Retard recn LVD3
36		32			15			LVD3 Time				LVD3 Time		Tiempo LVD3				Tiempo LVD3
37		32			15			LVD3 Dependency				LVD3 Dependency		Dependencia LVD3			Depend LVD3
41		32			15			LVD2					LVD2			Desconexión LVD2			Función LVD2
42		32			15			LVD2 Mode				LVD2 Mode		Modo LVD2				Modo LVD2
43		32			15			LVD2 Voltage				LVD2 Voltage		Tensión LVD2				Tensión LVD2
44		32			15			LVR2 Reconnect Voltage			LVR2 ReconnVolt		Tensión reconexión LVD2			Tens recon LVD2
45		32			15			LVD2 Reconnect Delay			LVD2 ReconDelay		Retardo reconexión LVD2			Retard recn LVD2
46		32			15			LVD2 Time				LVD2 Time		Tiempo LVD2				Tiempo LVD2
47		32			15			LVD2 Dependency				LVD2 Dependency		Dependencia LVD2			Depend LVD2
51		32			15			Disabled				Disabled		Deshabilitada				Deshabilitada
52		32			15			Enabled					Enabled			Habilitada				Habilitada
53		32			15			By Voltage				By Voltage		Tensión					Tensión
54		32			15			By Time					By Time			Tiempo					Tiempo
55		32			15			None					None			No					No
56		32			15			LVD1					LVD1			LVD1					LVD1
57		32			15			LVD2					LVD2			LVD2					LVD2
103		32			15			High Temp Disconnect 3			HTD3			Habilitada HTD3				Habil HTD3
104		32			15			High Temp Disconnect 2			HTD2			Habilitada HTD2				Habil HTD2
105		32			15			Battery LVD				Batt LVD		LVD de batería				LVD batería
106		32			15			No Battery				No Batt			Sin Batería				Sin batería
107		32			15			LVD3					LVD3			LVD3					LVD3
108		32			15			LVD2					LVD2			LVD2					LVD2
109		32			15			Battery Always On			BattAlwaysOn		Batería siempre conectada		Siempre con Bat
110		32			15			LVD Contactor Type			LVD Type		Tipo contactor LVD			Tipo LVD
111		32			15			Bistable				Bistable		Biestable				Biestable
112		32			15			Monostable				Monostable		Monoestable				Monoestable
113		32			15			Monostable with Sampler			Mono with Samp		Estable con señal			Con Señal
116		32			15			LVD3 Disconnected			LVD3 Disconnect		LVD3 Abierto				LVD3 Abierto
117		32			15			LVD2 Disconnected			LVD2 Disconnect		LVD2 Abierto				LVD2 Abierto
118		32			15			LVD3 Monostable with Sampler		LVD3 Mono Sample	LVD3 estable con señal			LVD3 con señal
119		32			15			LVD2 Monostable with Sampler		LVD2 Mono Sample	LVD2 estable con señal			LVD2 con señal
125		32			15			State					State			Estado					Estado
126		32			15			LVD3 Voltage(24V)			LVD3 Voltage		Tensión LVD3(24V)			Tensión LVD3
127		32			15			LVD3 Reconnect Voltage(24V)		LVD3 ReconnVolt		Tensión reconexión LVD3(24V)		Tens recon LVD3
128		32			15			LVD2 Voltage(24V)			LVD2 Voltage		Tensión LVD2(24V)			Tensión LVD2
129		32			15			LVD2 Reconnect Voltage(24V)		LVD2 ReconnVolt		Tensión reconexión LVD2(24V)		Tens recon LVD2
130		32			15			LVD 3					LVD 3			LVD 3					LVD 3
200		32			15			Connect					Connect			Cerrado					Cerrado
201		32			15			Disconnect				Disconnect		Abierto					Abierto
