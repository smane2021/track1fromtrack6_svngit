﻿#
# Locale language support: Spanish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Phase L1 Voltage			L1			Tensión Fase R				R
2		32			15			Phase L2 Voltage			L2			Tensión Fase S				S
3		32			15			Phase L3 Voltage			L3			Tensión Fase T				T
4		32			15			Line Voltage L1-L2			L1-L2			Tension R-S				R-S
5		32			15			Line Voltage L2-L3			L2-L3			Tension S-T				S-T
6		32			15			Line Voltage L3-L1			L1-L3			Tension R-T				R-T
7		32			15			Phase L1 Current			Phase Curr L1		Corriente Fase R			Corriente R
8		32			15			Phase L2 Current			Phase Curr L2		Corriente Fase S			Corriente S
9		32			15			Phase L3 Current			Phase Curr L3		Corriente Fase T			Corriente T
10		32			15			Frequency				AC Frequency		Frecuencia CA				Frecuencia CA
11		32			15			Total Real Power			Total RealPower		Potencia real total			Potencia real
12		32			15			Phase L1 Real Power			Real Power L1		Potencia real fase R			Pot real R
13		32			15			Phase L2 Real Power			Real Power L2		Potencia real fase S			Pot real S
14		32			15			Phase L3 Real Power			Real Power L3		Potencia real fase T			Pot real T
15		32			15			Total Reactive Power			Tot React Power		Potencia reactiva total			Pot reactiva
16		32			15			Phase L1 Reactive Power			React Power L1		Potencia reactiva R			Pot reactiva R
17		32			15			Phase L2 Reactive Power			React Power L2		Potencia reactiva S			Pot reactiva S
18		32			15			Phase L3 Reactive Power			React Power L3		Potencia reactiva T			Pot reactiva T
19		32			15			Total Apparent Power			Total App Power		Potencia aparente total			Pot aparente
20		32			15			Phase L1 Apparent Power			App Power L1		Potencia aparente fase R		Pot aparente R
21		32			15			Phase L2 Apparent Power			App Power L2		Potencia aparente fase S		Pot aparente S
22		32			15			Phase L3 Apparent Power			App Power L3		Potencia aparente fase T		Pot aparente T
23		32			15			Power Factor				Power Factor		Factor de potencia			Factor potencia
24		32			15			Phase L1 Power Factor			Power Factor L1		Factor potencia fase R			Factor pot R
25		32			15			Phase L2 Power Factor			Power Factor L2		Factor potencia fase S			Factor pot S
26		32			15			Phase L3 Power Factor			Power Factor L3		Factor potencia fase T			Factor pot T
27		32			15			Phase L1 Current Crest Factor		L1 Crest Factor		Factor cresta corriente R		Fact cresta IR
28		32			15			Phase L2 Current Crest Factor		L2 Crest Factor		Factor cresta corriente S		Fact cresta IS
29		32			15			Phase L3 Current Crest Factor		L3 Crest Factor		Factor cresta corriente T		Fact cresta IT
30		32			15			Phase L1 Current THD			Current THD L1		THD corriente fase R			THD I fase R
31		32			15			Phase L2 Current THD			Current THD L2		THD corriente fase S			THD I fase S
32		32			15			Phase L3 Current THD			Current THD L3		THD corriente fase T			THD I fase T
33		32			15			Phase L1 Voltage THD			Voltage THD L1		THD tensión fase R			THD V fase R
34		32			15			Phase L2 Voltage THD			Voltage THD L2		THD tensión fase S			THD V fase S
35		32			15			Phase L3 Voltage THD			Voltage THD L3		THD tensión fase T			THD V fase T
36		32			15			Total Real Energy			Tot Real Energy		Energía Real total			Energia Real
37		32			15			Total Reactive Energy			Tot ReactEnergy		Energía reactiva total			Energia react
38		32			15			Total Apparent Energy			Tot App Energy		Energía aparente total			Energ Aparente
39		32			15			Ambient Temperature			Ambient Temp		Temperatura ambiente			Temp ambiente
40		32			15			Nominal Line Voltage			Nominal L-Volt		Tensión nominal sistema			Tension nominal
41		32			15			Nominal Phase Voltage			Nominal PH-Volt		Tensión nominal de fase			Tens nom fase
42		32			15			Nominal Frequency			Nom Frequency		Frecuencia nominal			Frecuencia nom
43		32			15			Mains Failure Alarm Threshold 1		MFA Threshold 1		Umbral alarma Fallo Red 1		Alarm FalloCA1
44		32			15			Mains Failure Alarm Threshold 2		MFA Threshold 2		Umbral alarma Fallo Red 2		Alarm FalloCA2
45		32			15			Voltage Alarm Threshold 1		Volt Alm Trld 1		Umbral alarma tensión 1			Umb alarma V 1
46		32			15			Voltage Alarm Threshold 2		Volt Alm Trld 2		Umbral alarma tensión 2			Umb alarma V 2
47		32			15			Frequency Alarm Threshold		Freq Alarm Trld		Umbral alarma frecuencia		Umb alm frec
48		32			15			High Temperature Limit			High Temp Limit		Límite Alta Temperatura			Lim alta temp
49		32			15			Low Temperature Limit			Low Temp Limit		Límite Baja Temperatura			Lim baja temp
50		32			15			Supervision Fail			Supervision Fail	Fallo supervisión			Fallo supervsn
51		32			15			High Line Voltage L1-L2			High L-Volt L1-L2		Alta tensión R-S			Alta tens R-S
52		32			15			Very High Line Voltage L1-L2		VHigh L-Volt L1-L2		Muy alta tensión R-S			Muy alta V R-S
53		32			15			Low Line Voltage L1-L2			Low L-Volt L1-L2		Baja tensión R-S			Baja tens R-S
54		32			15			Very Low Line Voltage L1-L2		VLow L-Volt L1-L2		Muy baja tensión R-S			Muy baja V R-S
55		32			15			High Line Voltage L2-L3			High L-Volt L2-L3		Alta tensión S-T			Alta tens S-T
56		32			15			Very High Line Voltage L2-L3		VHigh L-Volt L2-L3		Muy alta tensión S-T			Muy alta V S-T
57		32			15			Low Line Voltage L2-L3			Low L-Volt L2-L3		Baja tensión S-T			Baja tens S-T
58		32			15			Very Low Line Voltage L2-L3		VLow L-Volt L2-L3		Muy baja tensión S-T			Muy baja V S-T
59		32			15			High Line Voltage L3-L1			High L-Volt L3-L1		Alta tenisón R-T			Alta tens R-T
60		32			15			Very High Line Voltage L3-L1		VHigh L-Volt L3-L1		Muy alta tensión R-T			Muy alta V R-T
61		32			15			Low Line Voltage L3-L1			Low L-Volt L3-L1		Baja tensión R-T			Baja tens R-T
62		32			15			Very Low Line Voltage L3-L1		VLow L-Volt L3-L1		Muy baja tensión R-T			Muy baja V R-T
63		32			15			High Phase Voltage L1			High Ph-Volt L1		Alta tensión fase R			Alta tension R
64		32			15			Very High Phase Voltage L1		VHigh Ph-Volt L1		Muy alta tensión fase R			Muy alta tens R
65		32			15			Low Phase Voltage L1			Low Ph-Volt L1		Baja tensión fase R			Baja tension R
66		32			15			Very Low Phase Voltage L1		VLow Ph-Volt L1		Muy baja tensión fase R			Muy baja tens R
67		32			15			High Phase Voltage L2			High Ph-Volt L2		Alta tensión fase S			Alta tension S
68		32			15			Very High Phase Voltage L2		VHigh Ph-Volt L2		Muy alta tensión fase S			Muy alta tens S
69		32			15			Low Phase Voltage L2			Low Ph-Volt L2		Baja tensión fase S			Baja tension S
70		32			15			Very Low Phase Voltage L2		VLow Ph-Volt L2		Muy baja tensión S			Muy baja tens S
71		32			15			High Phase Voltage L3			High Ph-Volt L3		Alta tensión fase T			Alta tension T
72		32			15			Very High Phase Voltage L3		VHigh Ph-Volt L3		Muy alta tensión fase T			Muy alta tens T
73		32			15			Low Phase Voltage L3			Low Ph-Volt L3		Baja tensión fase T			Baja tension T
74		32			15			Very Low Phase Voltage L3		VLow Ph-Volt L3		Muy baja tensión fase T			Muy baja tens T
75		32			15			Mains Failure				Mains Failure		Fallo de Red				Fallo de Red
76		32			15			Severe Mains Failure			Severe Main Fail	Fallo de Red severo			Fallo Red sever
77		32			15			High Frequency				High Frequency		Alta frecuencia				Alta frecuencia
78		32			15			Low Frequency				Low Frequency		Baja frecuencia				Baja frecuencia
79		32			15			High Temperature			High Temp		Alta temperatura			Alta temp
80		32			15			Low Temperature				Low Temperature		Baja temperatura			Baja temp
81		32			15			Rectifier AC				AC			Alterna Rectificadores			CA Rectificador
82		32			15			Supervision Fail			Supervision Fail	Fallo supervisión			Fallo supervsn
83		32			15			No					No			No					No
84		32			15			Yes					Yes			Sí					Sí
85		32			15			Phase L1 Mains Failure Counter		L1 Mains Fail Cnt	Contador Fallos fase R			Cont fallos R
86		32			15			Phase L2 Mains Failure Counter		L2 Mains Fail Cnt	Contador Fallos fase S			Cont fallos S
87		32			15			Phase L3 Mains Failure Counter		L3 Mains Fail Cnt	Contador Fallos fase T			Cont fallos T
88		32			15			Frequency Failure Counter		F Fail Cnt		Contador fallos frecuencia		Cont fallosFrec
89		32			15			Reset Phase L1 Mains Fail Counter	Reset L1 FailCnt		Iniciar cont Fallos fase R		Inic fallos R
90		32			15			Reset Phase L2 Mains Fail Counter	Reset L2 FailCnt		Iniciar cont Fallos fase S		Inic fallos S
91		32			15			Reset Phase L3 Mains Fail Counter	Reset L3 FailCnt		Iniciar cont Fallos fase T		Inic fallos T
92		32			15			Reset Frequency Counter			Reset F FailCnt		Iniciar cont Fallos frecuencia		Inic fallosFrec
93		32			15			Current Alarm Threshold			Curr Alarm Limit	Umbral alarma de corriente		Umb alarm corr
94		32			15			Phase L1 High Current			L1 High Current		Alta corriente fase R			Alta I fase R
95		32			15			Phase L2 High Current			L2 High Current		Alta corriente fase S			Alta I fase S
96		32			15			Phase L3 High Current			L3 High Current		Alta corriente fase T			Alta I fase T
97		32			15			Min Phase Voltage			Min Phase Volt		Tensión mínima de fase			Tens min fase
98		32			15			Max Phase Voltage			Max Phase Volt		Tensión máxima de fase			Tens max fase
99		32			15			Raw Data 1				Raw Data 1		Datos en bruto 1			Datos brutos 1
100		32			15			Raw Data 2				Raw Data 2		Datos en bruto 2			Datos brutos 2
101		32			15			Raw Data 3				Raw Data 3		Datos en bruto 3			Datos brutos 3
102		32			15			Ref Voltage				Ref Voltage		Tensión de referencia			Tensión ref
103		32			15			State					State			Estado					Estado
104		32			15			Off					Off			Apagado					Apagado
105		32			15			On					on			Conectado				Conectado
106		32			15			High Phase Voltage			High Ph-Volt		Alta tensión de Fase			Alta V Fase
107		32			15			Very High Phase Voltage			VHigh Ph-Volt		Muy alta tensión de fase		Muy alta V Fase
108		32			15			Low Phase Voltage			Low Ph-Volt		Baja tensión de fase			Baja V Fase
109		32			15			Very Low Phase Voltage			VLow Ph-Volt		Muy baja tensión de fase		Muy baja V Fase
110		32			15			All Rectifiers Not Responding		Rects No Resp		Ningún rectificador responde		Rects No Resp
