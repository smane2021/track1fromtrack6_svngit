﻿#
# Locale language support: Spanish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			IPLU						IPLU					IPLU					IPLU
8		32			15			Battery Block 1 Voltage		Batt Blk1 Volt			Bloque1BatVoltaje		Bloque1BatVolt
9		32			15			Battery Block 2 Voltage		Batt Blk2 Volt			Bloque2BatVoltaje		Bloque2BatVolt
10		32			15			Battery Block 3 Voltage		Batt Blk3 Volt			Bloque3BatVoltaje		Bloque3BatVolt
11		32			15			Battery Block 4 Voltage		Batt Blk4 Volt			Bloque4BatVoltaje		Bloque4BatVolt
12		32			15			Battery Block 5 Voltage		Batt Blk5 Volt			Bloque5BatVoltaje		Bloque5BatVolt
13		32			15			Battery Block 6 Voltage		Batt Blk6 Volt			Bloque6BatVoltaje		Bloque6BatVolt
14		32			15			Battery Block 7 Voltage		Batt Blk7 Volt			Bloque7BatVoltaje		Bloque7BatVolt
15		32			15			Battery Block 8 Voltage		Batt Blk8 Volt			Bloque8BatVoltaje		Bloque8BatVolt
16		32			15			Battery Block 9 Voltage		Batt Blk9 Volt			Bloque9BatVoltaje		Bloque9BatVolt
17		32			15			Battery Block 10 Voltage	Batt Blk10 Volt			Bloque10BatVoltaje		Bloque10BatVolt
18		32			15			Battery Block 11 Voltage	Batt Blk11 Volt			Bloque11BatVoltaje		Bloque11BatVolt
19		32			15			Battery Block 12 Voltage	Batt Blk12 Volt			Bloque12BatVoltaje		Bloque12BatVolt
20		32			15			Battery Block 13 Voltage	Batt Blk13 Volt			Bloque13BatVoltaje		Bloque13BatVolt
21		32			15			Battery Block 14 Voltage	Batt Blk14 Volt			Bloque14BatVoltaje		Bloque14BatVolt
22		32			15			Battery Block 15 Voltage	Batt Blk15 Volt			Bloque15BatVoltaje		Bloque15BatVolt
23		32			15			Battery Block 16 Voltage	Batt Blk16 Volt			Bloque16BatVoltaje		Bloque16BatVolt
24		32			15			Battery Block 17 Voltage	Batt Blk17 Volt			Bloque17BatVoltaje		Bloque17BatVolt
25		32			15			Battery Block 18 Voltage	Batt Blk18 Volt			Bloque18BatVoltaje		Bloque18BatVolt
26		32			15			Battery Block 19 Voltage	Batt Blk19 Volt			Bloque19BatVoltaje		Bloque19BatVolt
27		32			15			Battery Block 20 Voltage	Batt Blk20 Volt			Bloque20BatVoltaje		Bloque20BatVolt
28		32			15			Battery Block 21 Voltage	Batt Blk21 Volt			Bloque21BatVoltaje		Bloque21BatVolt
29		32			15			Battery Block 22 Voltage	Batt Blk22 Volt			Bloque22BatVoltaje		Bloque22BatVolt
30		32			15			Battery Block 23 Voltage	Batt Blk23 Volt			Bloque23BatVoltaje		Bloque23BatVolt
31		32			15			Battery Block 24 Voltage	Batt Blk24 Volt			Bloque24BatVoltaje		Bloque24BatVolt
32		32			15			Battery Block 25 Voltage	Batt Blk25 Volt			Bloque25BatVoltaje		Bloque25BatVolt
33		32			15			Temperature1				Temperature1			Temperatura1			Temperatura1
34		32			15			Temperature2				Temperature2			Temperatura2			Temperatura1
35		32			15			Battery Current				Battery Curr			Corriente Batería		Corri Bat
36		32			15			Battery Voltage				Battery Volt			Voltaje Batería			Voltaje Bater
40		32			15			Battery Block High			Batt Blk High			Bloque Batería Alto		BloqueBatAlto	
41		32			15			Battery Block Low			Batt Blk Low			Bloque de batería bajo	BloqueBatbajo
50		32			15			IPLU No Response			IPLU No Response		IPLUNinguna respuesta	IPLUNingresp
51		32			15			Battery Block1 Alarm		Batt Blk1 Alm			AlmBatBloque1			AlmBatBloque1
52		32			15			Battery Block2 Alarm		Batt Blk2 Alm			AlmBatBloque2			AlmBatBloque2
53		32			15			Battery Block3 Alarm		Batt Blk3 Alm			AlmBatBloque3			AlmBatBloque3
54		32			15			Battery Block4 Alarm		Batt Blk4 Alm			AlmBatBloque4			AlmBatBloque4
55		32			15			Battery Block5 Alarm		Batt Blk5 Alm			AlmBatBloque5			AlmBatBloque5
56		32			15			Battery Block6 Alarm		Batt Blk6 Alm			AlmBatBloque6			AlmBatBloque6
57		32			15			Battery Block7 Alarm		Batt Blk7 Alm			AlmBatBloque7			AlmBatBloque7
58		32			15			Battery Block8 Alarm		Batt Blk8 Alm			AlmBatBloque8			AlmBatBloque8
59		32			15			Battery Block9 Alarm		Batt Blk9 Alm			AlmBatBloque9			AlmBatBloque9
60		32			15			Battery Block10 Alarm		Batt Blk10 Alm			AlmBatBloque10			AlmBatBloque10
61		32			15			Battery Block11 Alarm		Batt Blk11 Alm			AlmBatBloque11			AlmBatBloque11
62		32			15			Battery Block12 Alarm		Batt Blk12 Alm			AlmBatBloque12			AlmBatBloque12
63		32			15			Battery Block13 Alarm		Batt Blk13 Alm			AlmBatBloque13			AlmBatBloque13
64		32			15			Battery Block14 Alarm		Batt Blk14 Alm			AlmBatBloque14			AlmBatBloque14
65		32			15			Battery Block15 Alarm		Batt Blk15 Alm			AlmBatBloque15			AlmBatBloque15
66		32			15			Battery Block16 Alarm		Batt Blk16 Alm			AlmBatBloque16			AlmBatBloque16
67		32			15			Battery Block17 Alarm		Batt Blk17 Alm			AlmBatBloque17			AlmBatBloque17
68		32			15			Battery Block18 Alarm		Batt Blk18 Alm			AlmBatBloque18			AlmBatBloque18
69		32			15			Battery Block19 Alarm		Batt Blk19 Alm			AlmBatBloque19			AlmBatBloque19
70		32			15			Battery Block20 Alarm		Batt Blk20 Alm			AlmBatBloque20			AlmBatBloque20
71		32			15			Battery Block21 Alarm		Batt Blk21 Alm			AlmBatBloque21			AlmBatBloque21
72		32			15			Battery Block22 Alarm		Batt Blk22 Alm			AlmBatBloque22			AlmBatBloque22
73		32			15			Battery Block23 Alarm		Batt Blk23 Alm			AlmBatBloque23			AlmBatBloque23
74		32			15			Battery Block24 Alarm		Batt Blk24 Alm			AlmBatBloque24			AlmBatBloque24
75		32			15			Battery Block25 Alarm		Batt Blk25 Alm			AlmBatBloque25			AlmBatBloque25
76		32			15			Battery Capacity			Battery Capacity		Capacidad Batería		Capa Batería
77		32			15			Capacity Percent			Capacity Percent		Porcentaje Capa			Porcent Capa
78		32			15			Enable						Enable					Habilitar				Habilitar
79		32			15			Disable						Disable					Inhabilitar				Inhabilitar
84		32			15			No							No						Ninguno					Ninguno
85		32			15			Yes							Yes						Sí						Sí

103		32			15			Existence State				Existence State		Estado existencia			EstadoExist
104		32			15			Existent					Existent			Existente					Existente
105		32			15			Not Existent				Not Existent		No existente				No Existente
106		32			15			IPLU Communication Fail		IPLU Comm Fail		IPLUFalloCom				IPLUFalloCom
107		32			15			Communication OK			Comm OK				Comunicación OK				Com OK
108		32			15			Communication Fail			Comm Fail			FalloCom					FalloCom
109		32			15			Rated Capacity				Rated Capacity				Capa nominal		Capa Nominal
110		32			15			Used by Batt Management		Used by Batt Management		UtilPorBatt Management	UtilPorBatManag
116		32			15			Battery Current Imbalance	Battery Current Imbalance	DeseqCorrBat		DeseqCorrBat