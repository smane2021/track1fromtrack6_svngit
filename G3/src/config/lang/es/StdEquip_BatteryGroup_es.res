﻿#
# Locale language support: Spanish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Voltage					Voltage			Tensión Batería				Tensión Batería
2		32			15			Total Current				Tot Current		Corriente total a Batería		Corriente total
3		32			15			Battery Temperature			Batt Temp		Temperatura Batería			Temp Bat
4		40			15			Tot Time in Shallow Disch Below 50V	Shallow DisTime		Total en descarga superficial		Descarga leve
5		40			15			Tot Time in Medium Disch Below 46.8V	Medium DisTime		Total en descarga media			Descarga media
6		40			15			Tot Time in Deep Disch Below 42V	Deep DisTime		Total en descarga profunda		Descarg profund
7		40			15			No Of Times in Shallow Disch Below 50V	No Of ShallDis		Descargas superficiales			Descargas leves
8		40			15			No Of Times in Medium Disch Below 46.8V	No Of MediumDis		Descargas medias			Descarg medias
9		40			15			No Of Times in Deep Disch Below 42V	No Of DeepDis		Descargas profundas			Descg profundas
14		32			15			End Test on Voltage			End Test Volt		Final prueba por tensión		Fin tens prueba
15		32			15			Discharge Current Imbalance		Dsch Curr Imb		Corriente descarga desequilib		Desq I descarga
19		32			15			Abnormal Battery Current		AbnormalBatCurr		Corriente batería anormal		I Bat anormal
21		32			15			System Current Limit Active		SystemCurrLimit		Limitando corriente a baterías		Lim corr bat
23		32			15			Equalize/Float Charge Control		EQ/FLT Control		Control carga/flotación			Ctrl carg/flot
25		32			15			Battery Test Control			BattTestControl		Control prueba baterías			Ctrl prueba
30		32			15			Number of Battery Blocks		Num Batt Blocks		Número de elementos de Batería		Elementos Bat
31		32			15			End Test Time				End Test Time		Tiempo fin de prueba			T fin prueba
32		32			15			End Test Voltage			End Test Volt		Tensión fin de prueba			V fin prueba
33		32			15			End Test Capacity			EndTestCapacity		Capacidad fin de prueba			Cap fin prueba
34		32			15			Constant Current Test			ConstCurrTest		Prueba intensidad cte			Prueba I cte
35		32			15			Constant Current Test Current		ConstCurrT Curr		Corriente prueba I constante		I prueba cte
37		32			15			AC Fail Test				AC Fail Test		Autoprueba en fallo de red		Prba Fallo Red
38		32			15			Short Test				Short Test		Prueba corta				Prueba corta
39		32			15			Short Test Cycle			ShortTest Cycle		Ciclo de prueba corta			Cicl prb corta
40		32			15			Short Test Max Difference Curr		Max Diff Curr		Max dif corriente prueba corta		Max dif corr
41		32			15			Short Test Time				Short Test Time		Duración prueba corta			T prba corta
42		32			15			Float Charge Voltage			Float Voltage		Tensión nominal				Tens nominal
43		32			15			Equalize Charge Voltage			EQ Voltage		Tensión de carga			Tensión carga
44		32			15			Maximum Equalize Charge Time		Maximum EQ Time		Tiempo máximo de carga			Max t carga
45		32			15			Equalize Stop Current			EQ Stop Curr		Corriente Final de Carga		I Final Carga
46		32			15			Equalize Stop Delay Time		EQ Stop Delay		Retardo Fin de Carga			Retard Fin Carg
47		32			15			Automatic Equalize			Auto EQ			Carga automática habilitada		Autocarga
48		32			15			Equalize Start Current			EQ Start Curr		Corriente inicio Carga			I autocarga
49		32			15			Equalize Start Capacity			EQ Start Cap		Capacidad inicio Carga			Cap autocarga
50		32			15			Cyclic Equalize Charge			Cyclic EQ		Carga cíclica habilitada		Carga cíclica
51		32			15			Cyclic Equalize Charge Interval		Cyc EQ Interval		Intervalo carga cíclica			Ciclo de Carga
52		32			15			Cyclic Equalize Charge Duration		Cyc EQ Duration		Duración carga cíclica			T carga cíclica
53		32			20			Temp Compensation Center		TempComp Center		Base compensación temperatura		Base comp temp
54		32			15			Compensation Coefficient		TempComp Coeff		Factor Compensación Temp		Coef comp temp
55		32			15			Battery Current Limit			Batt Curr Lmt		Límite corriente a batería		Lim corr bat
56		32			15			Battery Type Number			Batt Type Num		Número tipo de batería			Num tipo bat
57		32			15			Rated Capacity				Rated Capacity		Capacidad nominal C10			Capacidad C10
58		32			15			Charging Efficiency			Charging Eff		Coeficiente de capacidad		Coef capacidad
59		32			15			Time in 0.1C10 Discharge Curr		Time 0.1C10		Tiempo I descarga 0.1C10		Tiempo 0.1C10
60		32			15			Time in 0.2C10 Discharge Curr		Time 0.2C10		Tiempo I descarga 0.2C10		Tiempo 0.2C10
61		32			15			Time in 0.3C10 Discharge Curr		Time 0.3C10		Tiempo I descarga 0.3C10		Tiempo 0.3C10
62		32			15			Time in 0.4C10 Discharge Curr		Time 0.4C10		Tiempo I descarga 0.4C10		Tiempo 0.4C10
63		32			15			Time in 0.5C10 Discharge Curr		Time 0.5C10		Tiempo I descarga 0.5C10		Tiempo 0.5C10
64		32			15			Time in 0.6C10 Discharge Curr		Time 0.6C10		Tiempo I descarga 0.6C10		Tiempo 0.6C10
65		32			15			Time in 0.7C10 Discharge Curr		Time 0.7C10		Tiempo I descarga 0.7C10		Tiempo 0.7C10
66		32			15			Time in 0.8C10 Discharge Curr		Time 0.8C10		Tiempo I descarga 0.8C10		Tiempo 0.8C10
67		32			15			Time in 0.9C10 Discharge Curr		Time 0.9C10		Tiempo I descarga 0.9C10		Tiempo 0.9C10
68		32			15			Time in 1.0C10 Discharge Curr		Time 1.0C10		Tiempo I descarga 1.0C10		Tiempo 1.0C10
70		32			15			Temperature Sensor Failure		TempSensorFail		Fallo sensor temperatura		Fallo sensor T
71		32			15			High Temperature			High Temp		Alta temperatura			Alta temperat
72		32			15			Very High Temperature			Very Hi Temp		Temperatura muy alta			Muy alta temp
73		32			15			Low Temperature				Low Temp		Baja temperatura			Baja temp
74		32			15			Planned Battery Test Running		PlanBattTestRun		Prueba programada en curso		P bat planif
77		32			15			Short Battery Test Running		ShortBatTestRun		Prueba corta				Prueba corta
81		32			15			Automatic Equalize			Auto EQ			Carga automática			Autocarga
83		32			15			Abnormal Battery Current		Abnorm Bat Curr		Corriente anormal a batería		Corr Bat mal
84		32			15			Temperature Compensation Active		TempComp Active		Compensación temperatura activa		Comp Temp Act
85		32			15			Battery Current Limit Active		Batt Curr Lmt		Limitando corriente a baterías		Lim corr Bat
86		32			15			Battery Charge Prohibited Alarm		BattChgProhiAlm		Carga no permitida			Carga prohib
87		32			15			No					No			No					No
88		32			15			Yes					Yes			Sí					Sí
90		32			15			None					None			Ninguno					Ninguno
91		32			15			Temperature 1				Temp 1			Temperatura 1				Temp 1
92		32			15			Temperature 2				Temp 2			Temperatura 2				Temp 2
93		32			15			Temp 3 (OB)				Temp3 (OB)		Sensor T3 (OB)				Temp3 (OB)
94		32			15			Temp 4 (IB)				Temp4 (IB)		Sensor T4 (IB)				Temp4 (IB)
95		32			15			Temp 5 (IB)				Temp5 (IB)		Sensor T5 (IB)				Temp5 (IB)
96		32			15			Temp 6 (EIB)				Temp6 (EIB)		Sensor T6 (EIB)				Temp6 (EIB)
97		32			15			Temp 7 (EIB)				Temp7 (EIB)		Sensor T7 (EIB)				Temp7 (EIB)
98		32			15			0					0			0					0
99		32			15			1					1			1					1
100		32			15			2					2			2					2
113		32			15			Float Charge				Float Charge		Carga/Flotación				Carga/Flota
114		32			15			Equalize Charge				EQ Charge		Carga					Carga
121		32			15			Disabled				Disabled		Deshabilitar				Deshabilitar
122		32			15			Enabled					Enabled			Habilitar				Habilitar
136		32			15			Record Threshold			RecordThresh		Umbral de registro			Umbral registr
137		32			15			Estimated Backup Time			Est Back Time		Tiempo restante estimado		Restante
138		32			15			Battery Management State		Battery State		Estado batería				Estado bat
139		32			15			Float Charge				Float Charge		Carga/Flotación				Carga/Flota
140		32			15			Short Test				Short Test		Prueba corta				Prueba corta
141		32			15			Equalize Charge for Test		EQ for Test		Carga antes de prueba			Precarga prueba
142		32			15			Manual Test				Manual Test		Prueba manual				Prueba manual
143		32			15			Planned Test				Planned Test		Prueba programada			Prueba program
144		32			15			AC Failure Test				AC Fail Test		Prueba en Fallo de Red			Prueba Fallo CA
145		32			15			AC Failure				AC Failure		Fallo de Red				Fallo de Red
146		32			15			Manual Equalize Charge			Manual EQ		Carga manual				Carga manual
147		32			15			Auto Equalize Charge			Auto EQ			Carga automática			Autocarga
148		32			15			Cyclic Equalize Charge			Cyclic EQ		Carga cíclica				Carga cíclica
152		32			15			Over Current Setpoint			Over Current		Nivel de sobrecorriente			Sobrecorriente
153		32			15			Stop Battery Test			Stop Batt Test		Parar prueba de baterías		Parar prueba
154		32			15			Battery Group				Battery Group		Grupo de baterías			Grupo Batería
157		32			15			Master Battery Test in Progress		Master BT		Prueba maestra bat en curso		Prueba maestra
158		32			15			Master Equalize Charge in Progr		Master EQ		Carga maestra en curso			Carga maestra
165		32			15			Test Voltage Level			Test Volt		Tensión de prueba			Tensión prueba
166		32			15			Bad Battery				Bad Battery		Batería Mal				Bateria Mal
168		32			15			Reset Bad Battery Alarm			Reset Bad Batt		Cesar alarma Batería Mal		Cesar Bat Mal
172		32			15			Start Battery Test			Start Batt Test		Iniciar prueba bat			Iniciar prueba
173		32			15			Stop					Stop			Parar					Parar
174		32			15			Start					Start			Iniciar					Iniciar
175		32			15			No. of Scheduled Tests per Year		No Of Pl Tests		Num pruebas programadas por año		Num pruebas/año
176		32			15			Planned Test 1				Planned Test1		Prueba programada 1			Prueba prog1
177		32			15			Planned Test 2				Planned Test2		Prueba programada 2			Prueba prog2
178		32			15			Planned Test 3				Planned Test3		Prueba programada 3			Prueba prog3
179		32			15			Planned Test 4				Planned Test4		Prueba programada 4			Prueba prog4
180		32			15			Planned Test 5				Planned Test5		Prueba programada 5			Prueba prog5
181		32			15			Planned Test 6				Planned Test6		Prueba programada 6			Prueba prog6
182		32			15			Planned Test 7				Planned Test7		Prueba programada 7			Prueba prog7
183		32			15			Planned Test 8				Planned Test8		Prueba programada 8			Prueba prog8
184		32			15			Planned Test 9				Planned Test9		Prueba programada 9			Prueba prog9
185		32			15			Planned Test 10				Planned Test10		Prueba programada 10			Prueba prog10
186		32			15			Planned Test 11				Planned Test11		Prueba programada 11			Prueba prog11
187		32			15			Planned Test 12				Planned Test12		Prueba programada 12			Prueba prog12
188		32			15			Reset Battery Capacity			Reset Capacity		Reiniciar Capacidad Baterías		Res Cap Bat
191		32			15			Reset Abnormal Batt Curr Alarm		Reset AbCur Alm		Cesar alarma corr anormal bat		Cesar I bat mal
192		32			15			Reset Discharge Curr Imbalance		Reset ImCur Alm		Cesar I descarga desequilb		Cesar I desqulb
193		32			15			Expected Current Limitation		ExpCurrLmt		Limitación corriente prevista		Lim corr prev
194		32			15			Battery Test In Progress		In Batt Test		Prueba de batería en progeso		Prueba de bat
195		32			15			Low Capacity Level			Low Cap Level		Nivel de Baja capacidad			Nivel Baja Cap
196		32			15			Battery Discharge			Battery Disch		Descarga Batería			Descarga Bat
197		32			15			Over Voltage				Over Volt		Sobretensión				Sobretensión
198		32			15			Low Voltage				Low Volt		Subtensión				Subtensión
200		32			15			Number of Battery Shunts		No.BattShunts		Número de shunts de batería		Shunts Batería
201		32			15			Imbalance Protection			ImB Protection		Protección desequilibrio		Prot Desequilb
202		32			15			Sensor for Temp Compensation		Sens TempComp		Sensor Compensación Temp		Sensor CompTemp
203		32			15			Number of EIB Batteries			No.EIB Battd		Número de baterías EIB			Num Bat EIB
204		32			15			Normal					Normal			Normal					Normal
205		32			15			Special for NA				Special			Especial				Especial
206		32			15			Battery Volt Type			Batt Volt Type		Tipo tensión batería			Tensión Bat
207		32			15			Very High Temp Voltage			VHi TempVolt		Tensión a muy alta Temp			V Muy Alta Temp
209		32			15			Sensor No. for Battery			Sens Battery		Sensor de baterías			Sensor baterías
212		32			15			Very High Battery Temp Action		VHiBattT Act		Acción a muy alta temperatura		Acción AltaTemp
213		32			15			Disabled				Disabled		Ninguna					Ninguna
214		32			15			Lower Voltage				Lower Voltage		Reducir tensión				Reducir tensión
215		32			15			Disconnect				Disconnect		Desconectar				Desconectar
216		32			15			Reconnection Temperature		Reconnect Temp		Temperatura de reconexión		T reconexión
217		32			15			Very High Temp Voltage(24V)		VHi TempVolt		Tensión a muy alta Temp(24V)		V Muy Alta Temp
218		32			15			Nominal Voltage(24V)			FC Voltage		Tensión nominal(24V)			Tens nominal
219		32			15			Equalize Charge Voltage(24V)		BC Voltage		Tensión carga batería(24V)		Tens carga
220		32			15			Test Voltage Level(24V)			Test Volt		Tensión de prueba(24V)			Tensión prueba
221		32			15			Test End Voltage(24V)			Test End Volt		Tensión fin de prueba(24V)		V fin prueba
222		32			15			Current Limitation			CurrLimit		Limitación de corriente			Limit Corriente
223		32			15			Battery Volt For North America		BattVolt for NA		Tensión Bat para Norteamérica		TensBat USA
224		32			15			Battery Changed				Battery Changed		Batería cambiada			Bat cambiada
225		32			15			Lowest Capacity for Battery Test	Lowest Cap for BT	Capacidad mínima para prueba		Mín Cap prueba
226		32			15			Temperature8				Temp8			Temperatura8				Temperatura8
227		32			15			Temperature9				Temp9			Temperatura9				Temperatura9
228		32			15			Temperature10				Temp10			Temperatura10				Temperatura10
229		32			15			Largedu Rated Capacity			Largedu Rated Capacity	Capacidad UD grande			Cap UD grande
230		32			15			Clear Battery Test Fail Alarm		Clear Test Fail		Cesar Fallo Prueba Baterías		Cesar F Prueba
231		32			15			Battery Test Failure			Batt Test Fail		Fallo prueba de baterías		Fallo Prueba
232		32			15			Max					Max			Máxima					Máxima
233		32			15			Average					Average			Media					Media
234		32			15			AverageSMBRC				AverageSMBRC		Media SMBRC				Media SMBRC
235		32			15			Compensation Temperature		Comp Temp		Temperatura Compensación		Temp Comp Bat
236		32			15			High Compensation Temperature		Hi Comp Temp		Nivel Alta Temp Compensación		Alta Temp Comp
237		32			15			Low Compensation Temperature		Lo Comp Temp		Nivel Baja Temp Compensación		Baja Temp Comp
238		32			15			High Compensation Temperature		High Comp Temp		Alta temperatura Compensación		Alta Temp Comp
239		32			15			Low Compensation Temperature		Low Comp Temp		Baja temperatura Compensación		Baja Temp Comp
240		35			15			Very High Compensation Temperature	VHi Comp Temp		Nivel muy Alta Temp Compensación	MuyAlta TComp
241		35			15			Very High Compensation Temperature	VHi Comp Temp		Muy Alta Temp Compensación		MuyAlta TComp
242		32			15			Compensation Sensor Fault		CompTempFail		Fallo sensor Temperatura Comp		Fallo Sens Comp
243		32			15			Calculate Battery Current		Calc Current		Calcular Corriente a Batería		Calc corr Bat
244		32			15			Start Equalize Charge Ctrl(EEM)		Star EQ(EEM)		Iniciar Carga (EEM)			Inic Carga(EEM)
245		32			15			Start Float Charge Ctrl(for EEM)	Start FLT (EEM)		Start FC (EEM)				Start FC (EEM)
246		32			15			Start Resistance Test (for EEM)		StartRTest(EEM)		Start Res Test(EEM)			Start RTest EEM
247		32			15			Stop Resistance Test (for EEM)		Stop RTest(EEM)		Stop RTest(EEM)				Stop RTest(EEM)
248		32			15			Reset BattCapacity(Internal Use)	Reset Cap(Int)		Reset Cap(Int)				Reset Cap(Int)
249		32			15			Reset Battery Capacity (for EEM)	Reset Cap(EEM)		Reset Battery Capacity (for EEM)	Reset Cap(EEM)
250		32			15			Clear Bad Battery Alarm(for EEM)	ClrBadBatt(EEM)		Clear Bad Battery Alarm(for EEM)	ClrBadBatt(EEM)
251		32			15			Temperature 1				Temperature 1		Temperatura 1				Temperatura 1
252		32			15			Temperature 2				Temperature 2		Temperatura 2				Temperatura 2
253		32			15			Temperature 3 (OB)			Temp3 (OB)		Temp3 (OB)				Temp3 (OB)
254		32			15			IB2 Temperature 1			IB2 Temp1		IB2-Temp1				IB2-Temp1
255		32			15			IB2 Temperature 2			IB2 Temp2		IB2-Temp2				IB2-Temp2
256		32			15			EIB Temperature 1			EIB Temp1		EIB-Temp1				EIB-Temp1
257		32			15			EIB Temperature 2			EIB Temp2		EIB-Temp2				EIB-Temp2
258		32			15			SMTemp1 T1				SMTemp1 T1		SMTemp1-T1				SMTemp1-T1
259		32			15			SMTemp1 T2				SMTemp1 T2		SMTemp1-T2				SMTemp1-T2
260		32			15			SMTemp1 T3				SMTemp1 T3		SMTemp1-T3				SMTemp1-T3
261		32			15			SMTemp1 T4				SMTemp1 T4		SMTemp1-T4				SMTemp1-T4
262		32			15			SMTemp1 T5				SMTemp1 T5		SMTemp1-T5				SMTemp1-T5
263		32			15			SMTemp1 T6				SMTemp1 T6		SMTemp1-T6				SMTemp1-T6
264		32			15			SMTemp1 T7				SMTemp1 T7		SMTemp1-T7				SMTemp1-T7
265		32			15			SMTemp1 T8				SMTemp1 T8		SMTemp1-T8				SMTemp1-T8
266		32			15			SMTemp2 T1				SMTemp2 T1		SMTemp2-T1				SMTemp2-T1
267		32			15			SMTemp2 T2				SMTemp2 T2		SMTemp2-T2				SMTemp2-T2
268		32			15			SMTemp2 T3				SMTemp2 T3		SMTemp2-T3				SMTemp2-T3
269		32			15			SMTemp2 T4				SMTemp2 T4		SMTemp2-T4				SMTemp2-T4
270		32			15			SMTemp2 T5				SMTemp2 T5		SMTemp2-T5				SMTemp2-T5
271		32			15			SMTemp2 T6				SMTemp2 T6		SMTemp2-T6				SMTemp2-T6
272		32			15			SMTemp2 T7				SMTemp2 T7		SMTemp2-T7				SMTemp2-T7
273		32			15			SMTemp2 T8				SMTemp2 T8		SMTemp2-T8				SMTemp2-T8
274		32			15			SMTemp3 T1				SMTemp3 T1		SMTemp3-T1				SMTemp3-T1
275		32			15			SMTemp3 T2				SMTemp3 T2		SMTemp3-T2				SMTemp3-T2
276		32			15			SMTemp3 T3				SMTemp3 T3		SMTemp3-T3				SMTemp3-T3
277		32			15			SMTemp3 T4				SMTemp3 T4		SMTemp3-T4				SMTemp3-T4
278		32			15			SMTemp3 T5				SMTemp3 T5		SMTemp3-T5				SMTemp3-T5
279		32			15			SMTemp3 T6				SMTemp3 T6		SMTemp3-T6				SMTemp3-T6
280		32			15			SMTemp3 T7				SMTemp3 T7		SMTemp3-T7				SMTemp3-T7
281		32			15			SMTemp3 T8				SMTemp3 T8		SMTemp3-T8				SMTemp3-T8
282		32			15			SMTemp4 T1				SMTemp4 T1		SMTemp4-T1				SMTemp4-T1
283		32			15			SMTemp4 T2				SMTemp4 T2		SMTemp4-T2				SMTemp4-T2
284		32			15			SMTemp4 T3				SMTemp4 T3		SMTemp4-T3				SMTemp4-T3
285		32			15			SMTemp4 T4				SMTemp4 T4		SMTemp4-T4				SMTemp4-T4
286		32			15			SMTemp4 T5				SMTemp4 T5		SMTemp4-T5				SMTemp4-T5
287		32			15			SMTemp4 T6				SMTemp4 T6		SMTemp4-T6				SMTemp4-T6
288		32			15			SMTemp4 T7				SMTemp4 T7		SMTemp4-T7				SMTemp4-T7
289		32			15			SMTemp4 T8				SMTemp4 T8		SMTemp4-T8				SMTemp4-T8
290		32		15		SMTemp5 T1				SMTemp5 T1		SMTemp5-T1				SMTemp5-T1
291		32		15		SMTemp5 T2				SMTemp5 T2		SMTemp5-T2				SMTemp5-T2
292		32		15		SMTemp5 T3				SMTemp5 T3		SMTemp5-T3				SMTemp5-T3
293		32		15		SMTemp5 T4				SMTemp5 T4		SMTemp5-T4				SMTemp5-T4
294		32		15		SMTemp5 T5				SMTemp5 T5		SMTemp5-T5				SMTemp5-T5
295		32		15		SMTemp5 T6				SMTemp5 T6		SMTemp5-T6				SMTemp5-T6
296		32		15		SMTemp5 T7				SMTemp5 T7		SMTemp5-T7				SMTemp5-T7
297		32		15		SMTemp5 T8				SMTemp5 T8		SMTemp5-T8				SMTemp5-T8
298		32		15		SMTemp6 T1				SMTemp6 T1		SMTemp6-T1				SMTemp6-T1
299		32		15		SMTemp6 T2				SMTemp6 T2		SMTemp6-T2				SMTemp6-T2
300		32		15		SMTemp6 T3				SMTemp6 T3		SMTemp6-T3				SMTemp6-T3
301		32		15		SMTemp6 T4				SMTemp6 T4		SMTemp6-T4				SMTemp6-T4
302		32		15		SMTemp6 T5				SMTemp6 T5		SMTemp6-T5				SMTemp6-T5
303		32		15		SMTemp6 T6				SMTemp6 T6		SMTemp6-T6				SMTemp6-T6
304		32		15		SMTemp6 T7				SMTemp6 T7		SMTemp6-T7				SMTemp6-T7
305		32		15		SMTemp6 T8				SMTemp6 T8		SMTemp6-T8				SMTemp6-T8
306		32		15		SMTemp7 T1				SMTemp7 T1		SMTemp7-T1				SMTemp7-T1
307		32		15		SMTemp7 T2				SMTemp7 T2		SMTemp7-T2				SMTemp7-T2
308		32		15		SMTemp7 T3				SMTemp7 T3		SMTemp7-T3				SMTemp7-T3
309		32		15		SMTemp7 T4				SMTemp7 T4		SMTemp7-T4				SMTemp7-T4
310		32		15		SMTemp7 T5				SMTemp7 T5		SMTemp7-T5				SMTemp7-T5
311		32		15		SMTemp7 T6				SMTemp7 T6		SMTemp7-T6				SMTemp7-T6
312		32		15		SMTemp7 T7				SMTemp7 T7		SMTemp7-T7				SMTemp7-T7
313		32		15		SMTemp7 T8				SMTemp7 T8		SMTemp7-T8				SMTemp7-T8
314		32		15		SMTemp8 T1				SMTemp8 T1		SMTemp8-T1				SMTemp8-T1
315		32		15		SMTemp8 T2				SMTemp8 T2		SMTemp8-T2				SMTemp8-T2
316		32		15		SMTemp8 T3				SMTemp8 T3		SMTemp8-T3				SMTemp8-T3
317		32		15		SMTemp8 T4				SMTemp8 T4		SMTemp8-T4				SMTemp8-T4
318		32		15		SMTemp8 T5				SMTemp8 T5		SMTemp8-T5				SMTemp8-T5
319		32		15		SMTemp8 T6				SMTemp8 T6		SMTemp8-T6				SMTemp8-T6
320		32		15		SMTemp8 T7				SMTemp8 T7		SMTemp8-T7				SMTemp8-T7
321		32		15		SMTemp8 T8				SMTemp8 T8		SMTemp8-T8				SMTemp8-T8
351		32			15			Very High Temp1				VHi Temp1		Muy Alta Temperatura 1			Muy Alta Temp1
352		32			15			High Temp 1				High Temp 1		Alta Temperatura 1			Alta Temp1
353		32			15			Low Temp 1				Low Temp 1		Baja Temperatura 1			Baja Temp1
354		32			15			Very High Temp 2			VHi Temp 2		Muy Alta Temperatura 2			Muy Alta Temp2
355		32			15			High Temp 2				Hi Temp 2		Alta Temperatura 2			Alta Temp2
356		32			15			Low Temp 2				Low Temp 2		Baja Temperatura 2			Baja Temp2
357		32			15			Very High Temp 3 (OB)			VHi Temp3 (OB)		Muy Alta Temp3 (OB)			Muy Alta Temp3
358		32			15			High Temp 3 (OB)			Hi Temp3 (OB)		Alta Temp3 (OB)				Alta Temp3(OB)
359		32			15			Low Temp 3 (OB)				Low Temp3 (OB)		Baja Temp3 (OB)				Baja Temp3(OB)
360		32			15			Very High IB2 Temp1			VHi IB2 Temp1		Muy Alta IB2-Temp1			MuyAlta IB2T1
361		32			15			High IB2 Temp1				Hi IB2 Temp1		Alta IB2-Temp1				Alta IB2-T1
362		32			15			Low IB2 Temp1				Low IB2 Temp1		Baja IB2-Temp1				Baja IB2-T1
363		32			15			Very High IB2 Temp2			VHi IB2 Temp2		Muy Alta IB2-Temp2			MuyAlta IB2T2
364		32			15			High IB2 Temp2				Hi IB2 Temp2		Alta IB2-Temp2				Alta IB2-T2
365		32			15			Low IB2 Temp2				Low IB2 Temp2		Baja IB2-Temp2				Baja IB2-T2
366		32			15			Very High EIB Temp1			VHi EIB Temp1		Muy Alta EIB-Temp1			MuyAlta EIB-T1
367		32			15			High EIB Temp1				Hi EIB Temp1		Alta EIB-Temp1				Alta EIB-T1
368		32			15			Low EIB Temp1				Low EIB Temp1		Baja EIB-Temp1				Baja EIB-T1
369		32			15			Very High EIB Temp2			VHi EIB Temp2		Muy Alta EIB-Temp2			MuyAlta EIB-T2
370		32			15			High EIB Temp2				Hi EIB Temp2		Alta EIB-Temp2				Alta EIB-T2
371		32			15			Low EIB Temp2				Low EIB Temp2		Baja EIB-Temp2				Baja EIB-T2
372		32			15			Very High at Temp 8			VHi at Temp 8		Muy Alta Temperatura 8			Muy Alta Temp8
373		32			15			High at Temp 8				Hi at Temp 8		Alta Temperatura 8			Alta Temp8
374		32			15			Low at Temp 8				Low at Temp 8		Baja Temperatura 8			Baja Temp8
375		32			15			Very High at Temp 9			VHi at Temp 9		Muy Alta Temperatura 9			Muy Alta Temp9
376		32			15			High at Temp 9				Hi at Temp 9		Alta Temperatura 9			Alta Temp9
377		32			15			Low at Temp 9				Low at Temp 9		Baja Temperatura 9			Baja Temp9
378		32			15			Very High at Temp 10			VHi at Temp 10		Muy Alta Temperatura 10			Muy Alta Temp10
379		32			15			High at Temp 10				Hi at Temp 10		Alta Temperatura 10			Alta Temp10
380		32			15			Low at Temp 10				Low at Temp 10		Baja Temperatura 10			Baja Temp10
381		32			15			Very High at Temp 11			VHi at Temp 11		Muy Alta Temperatura 11			Muy Alta Temp11
382		32			15			High at Temp 11				Hi at Temp 11		Alta Temperatura 11			Alta Temp11
383		32			15			Low at Temp 11				Low at Temp 11		Baja Temperatura 11			Baja Temp11
384		32			15			Very High at Temp 12			VHi at Temp 12		Muy Alta Temperatura 12			Muy Alta Temp12
385		32			15			High at Temp 12				Hi at Temp 12		Alta Temperatura 12			Alta Temp12
386		32			15			Low at Temp 12				Low at Temp 12		Baja Temperatura 12			Baja Temp12
387		32			15			Very High at Temp 13			VHi at Temp 13		Muy Alta Temperatura 13			Muy Alta Temp13
388		32			15			High at Temp 13				Hi at Temp 13		Alta Temperatura 13			Alta Temp13
389		32			15			Low at Temp 13				Low at Temp 13		Baja Temperatura 13			Baja Temp13
390		32			15			Very High at Temp 14			VHi at Temp 14		Muy Alta Temperatura 14			Muy Alta Temp14
391		32			15			High at Temp 14				Hi at Temp 14		Alta Temperatura 14			Alta Temp14
392		32			15			Low at Temp 14				Low at Temp 14		Baja Temperatura 14			Baja Temp14
393		32			15			Very High at Temp 15			VHi at Temp 15		Muy Alta Temperatura 15			Muy Alta Temp15
394		32			15			High at Temp 15				Hi at Temp 15		Alta Temperatura 15			Alta Temp15
395		32			15			Low at Temp 15				Low at Temp 15		Baja Temperatura 15			Baja Temp15
396		32			15			Very High at Temp 16			VHi at Temp 16		Muy Alta Temperatura 16			Muy Alta Temp16
397		32			15			High at Temp 16				Hi at Temp 16		Alta Temperatura 16			Alta Temp16
398		32			15			Low at Temp 16				Low at Temp 16		Baja Temperatura 16			Baja Temp16
399		32			15			Very High at Temp 17			VHi at Temp 17		Muy Alta Temperatura 17			Muy Alta Temp17
400		32			15			High at Temp 17				Hi at Temp 17		Alta Temperatura 17			Alta Temp17
401		32			15			Low at Temp 17				Low at Temp 17		Baja Temperatura 17			Baja Temp17
402		32			15			Very High at Temp 18			VHi at Temp 18		Muy Alta Temperatura 18			Muy Alta Temp18
403		32			15			High at Temp 18				Hi at Temp 18		Alta Temperatura 18			Alta Temp18
404		32			15			Low at Temp 18				Low at Temp 18		Baja Temperatura 18			Baja Temp18
405		32			15			Very High at Temp 19			VHi at Temp 19		Muy Alta Temperatura 19			Muy Alta Temp19
406		32			15			High at Temp 19				Hi at Temp 19		Alta Temperatura 19			Alta Temp19
407		32			15			Low at Temp 19				Low at Temp 19		Baja Temperatura 19			Baja Temp19
408		32			15			Very High at Temp 20			VHi at Temp 20		Muy Alta Temperatura 20			Muy Alta Temp20
409		32			15			High at Temp 20				Hi at Temp 20		Alta Temperatura 20			Alta Temp20
410		32			15			Low at Temp 20				Low at Temp 20		Baja Temperatura 20			Baja Temp20
411		32			15			Very High at Temp 21			VHi at Temp 21		Muy Alta Temperatura 21			Muy Alta Temp21
412		32			15			High at Temp 21				Hi at Temp 21		Alta Temperatura 21			Alta Temp21
413		32			15			Low at Temp 21				Low at Temp 21		Baja Temperatura 21			Baja Temp21
414		32			15			Very High at Temp 22			VHi at Temp 22		Muy Alta Temperatura 22			Muy Alta Temp22
415		32			15			High at Temp 22				Hi at Temp 22		Alta Temperatura 22			Alta Temp22
416		32			15			Low at Temp 22				Low at Temp 22		Baja Temperatura 22			Baja Temp22
417		32			15			Very High at Temp 23			VHi at Temp 23		Muy Alta Temperatura 23			Muy Alta Temp23
418		32			15			High at Temp 23				Hi at Temp 23		Alta Temperatura 23			Alta Temp23
419		32			15			Low at Temp 23				Low at Temp 23		Baja Temperatura 23			Baja Temp23
420		32			15			Very High at Temp 24			VHi at Temp 24		Muy Alta Temperatura 24			Muy Alta Temp24
421		32			15			High at Temp 24				Hi at Temp 24		Alta Temperatura 24			Alta Temp24
422		32			15			Low at Temp 24				Low at Temp 24		Baja Temperatura 24			Baja Temp24
423		32			15			Very High at Temp 25			VHi at Temp 25		Muy Alta Temperatura 25			Muy Alta Temp25
424		32			15			High at Temp 25				Hi at Temp 25		Alta Temperatura 25			Alta Temp25
425		32			15			Low at Temp 25				Low at Temp 25		Baja Temperatura 25			Baja Temp25
426		32			15			Very High at Temp 26			VHi at Temp 26		Muy Alta Temperatura 26			Muy Alta Temp26
427		32			15			High at Temp 26				Hi at Temp 26		Alta Temperatura 26			Alta Temp26
428		32			15			Low at Temp 26				Low at Temp 26		Baja Temperatura 26			Baja Temp26
429		32			15			Very High at Temp 27			VHi at Temp 27		Muy Alta Temperatura 27			Muy Alta Temp27
430		32			15			High at Temp 27				Hi at Temp 27		Alta Temperatura 27			Alta Temp27
431		32			15			Low at Temp 27				Low at Temp 27		Baja Temperatura 27			Baja Temp27
432		32			15			Very High at Temp 28			VHi at Temp 28		Muy Alta Temperatura 28			Muy Alta Temp28
433		32			15			High at Temp 28				Hi at Temp 28		Alta Temperatura 28			Alta Temp28
434		32			15			Low at Temp 28				Low at Temp 28		Baja Temperatura 28			Baja Temp28
435		32			15			Very High at Temp 29			VHi at Temp 29		Muy Alta Temperatura 29			Muy Alta Temp29
436		32			15			High at Temp 29				Hi at Temp 29		Alta Temperatura 29			Alta Temp29
437		32			15			Low at Temp 29				Low at Temp 29		Baja Temperatura 29			Baja Temp29
438		32			15			Very High at Temp 30			VHi at Temp 30		Muy Alta Temperatura 30			Muy Alta Temp30
439		32			15			High at Temp 30				Hi at Temp 30		Alta Temperatura 30			Alta Temp30
440		32			15			Low at Temp 30				Low at Temp 30		Baja Temperatura 30			Baja Temp30
441		32			15			Very High at Temp 31			VHi at Temp 31		Muy Alta Temperatura 31			Muy Alta Temp31
442		32			15			High at Temp 31				Hi at Temp 31		Alta Temperatura 31			Alta Temp31
443		32			15			Low at Temp 31				Low at Temp 31		Baja Temperatura 31			Baja Temp31
444		32			15			Very High at Temp 32			VHi at Temp 32		Muy Alta Temperatura 32			Muy Alta Temp32
445		32			15			High at Temp 32				Hi at Temp 32		Alta Temperatura 32			Alta Temp32
446		32			15			Low at Temp 32				Low at Temp 32		Baja Temperatura 32			Baja Temp32
447		32			15			Very High at Temp 33			VHi at Temp 33		Muy Alta Temperatura 33			Muy Alta Temp33
448		32			15			High at Temp 33				Hi at Temp 33		Alta Temperatura 33			Alta Temp33
449		32			15			Low at Temp 33				Low at Temp 33		Baja Temperatura 33			Baja Temp33
450		32			15			Very High at Temp 34			VHi at Temp 34		Muy Alta Temperatura 34			Muy Alta Temp34
451		32			15			High at Temp 34				Hi at Temp 34		Alta Temperatura 34			Alta Temp34
452		32			15			Low at Temp 34				Low at Temp 34		Baja Temperatura 34			Baja Temp34
453		32			15			Very High at Temp 35			VHi at Temp 35		Muy Alta Temperatura 35			Muy Alta Temp35
454		32			15			High at Temp 35				Hi at Temp 35		Alta Temperatura 35			Alta Temp35
455		32			15			Low at Temp 35				Low at Temp 35		Baja Temperatura 35			Baja Temp35
456		32			15			Very High at Temp 36			VHi at Temp 36		Muy Alta Temperatura 36			Muy Alta Temp36
457		32			15			High at Temp 36				Hi at Temp 36		Alta Temperatura 36			Alta Temp36
458		32			15			Low at Temp 36				Low at Temp 36		Baja Temperatura 36			Baja Temp36
459		32			15			Very High at Temp 37			VHi at Temp 37		Muy Alta Temperatura 37			Muy Alta Temp37
460		32			15			High at Temp 37				Hi at Temp 37		Alta Temperatura 37			Alta Temp37
461		32			15			Low at Temp 37				Low at Temp 37		Baja Temperatura 37			Baja Temp37
462		32			15			Very High at Temp 38			VHi at Temp 38		Muy Alta Temperatura 38			Muy Alta Temp38
463		32			15			High at Temp 38				Hi at Temp 38		Alta Temperatura 38			Alta Temp38
464		32			15			Low at Temp 38				Low at Temp 38		Baja Temperatura 38			Baja Temp38
465		32			15			Very High at Temp 39			VHi at Temp 39		Muy Alta Temperatura 39			Muy Alta Temp39
466		32			15			High at Temp 39				Hi at Temp 39		Alta Temperatura 39			Alta Temp39
467		32			15			Low at Temp 39				Low at Temp 39		Baja Temperatura 39			Baja Temp39
468		32			15			Very High at Temp 40			VHi at Temp 40		Muy Alta Temperatura 40			Muy Alta Temp40
469		32			15			High at Temp 40				Hi at Temp 40		Alta Temperatura 40			Alta Temp40
470		32			15			Low at Temp 40				Low at Temp 40		Baja Temperatura 40			Baja Temp40
471		32			15			Very High at Temp 41			VHi at Temp 41		Muy Alta Temperatura 41			Muy Alta Temp41
472		32			15			High at Temp 41				Hi at Temp 41		Alta Temperatura 41			Alta Temp41
473		32			15			Low at Temp 41				Low at Temp 41		Baja Temperatura 41			Baja Temp41
474		32			15			Very High at Temp 42			VHi at Temp 42		Muy Alta Temperatura 42			Muy Alta Temp42
475		32			15			High at Temp 42				Hi at Temp 42		Alta Temperatura 42			Alta Temp42
476		32			15			Low at Temp 42				Low at Temp 42		Baja Temperatura 42			Baja Temp42
477		32			15			Very High at Temp 43			VHi at Temp 43		Muy Alta Temperatura 43			Muy Alta Temp43
478		32			15			High at Temp 43				Hi at Temp 43		Alta Temperatura 43			Alta Temp43
479		32			15			Low at Temp 43				Low at Temp 43		Baja Temperatura 43			Baja Temp43
480		32			15			Very High at Temp 44			VHi at Temp 44		Muy Alta Temperatura 44			Muy Alta Temp44
481		32			15			High at Temp 44				Hi at Temp 44		Alta Temperatura 44			Alta Temp44
482		32			15			Low at Temp 44				Low at Temp 44		Baja Temperatura 44			Baja Temp44
483		32			15			Very High at Temp 45			VHi at Temp 45		Muy Alta Temperatura 45			Muy Alta Temp45
484		32			15			High at Temp 45				Hi at Temp 45		Alta Temperatura 45			Alta Temp45
485		32			15			Low at Temp 45				Low at Temp 45		Baja Temperatura 45			Baja Temp45
486		32			15			Very High at Temp 46			VHi at Temp 46		Muy Alta Temperatura 46			Muy Alta Temp46
487		32			15			High at Temp 46				Hi at Temp 46		Alta Temperatura 46			Alta Temp46
488		32			15			Low at Temp 46				Low at Temp 46		Baja Temperatura 46			Baja Temp46
489		32			15			Very High at Temp 47			VHi at Temp 47		Muy Alta Temperatura 47			Muy Alta Temp47
490		32			15			High at Temp 47				Hi at Temp 47		Alta Temperatura 47			Alta Temp47
491		32			15			Low at Temp 47				Low at Temp 47		Baja Temperatura 47			Baja Temp47
492		32			15			Very High at Temp 48			VHi at Temp 48		Muy Alta Temperatura 48			Muy Alta Temp48
493		32			15			High at Temp 48				Hi at Temp 48		Alta Temperatura 48			Alta Temp48
494		32			15			Low at Temp 48				Low at Temp 48		Baja Temperatura 48			Baja Temp48
495		32			15			Very High at Temp 49			VHi at Temp 49		Muy Alta Temperatura 49			Muy Alta Temp49
496		32			15			High at Temp 49				Hi at Temp 49		Alta Temperatura 49			Alta Temp49
497		32			15			Low at Temp 49				Low at Temp 49		Baja Temperatura 49			Baja Temp49
498		32			15			Very High at Temp 50			VHi at Temp 50		Muy Alta Temperatura 50			Muy Alta Temp50
499		32			15			High at Temp 50				Hi at Temp 50		Alta Temperatura 50			Alta Temp50
500		32			15			Low at Temp 50				Low at Temp 50		Baja Temperatura 50			Baja Temp50
501		32			15			Very High at Temp 51			VHi at Temp 51		Muy Alta Temperatura 51			Muy Alta Temp51
502		32			15			High at Temp 51				Hi at Temp 51		Alta Temperatura 51			Alta Temp51
503		32			15			Low at Temp 51				Low at Temp 51		Baja Temperatura 51			Baja Temp51
504		32			15			Very High at Temp 52			VHi at Temp 52		Muy Alta Temperatura 52			Muy Alta Temp52
505		32			15			High at Temp 52				Hi at Temp 52		Alta Temperatura 52			Alta Temp52
506		32			15			Low at Temp 52				Low at Temp 52		Baja Temperatura 52			Baja Temp52
507		32			15			Very High at Temp 53			VHi at Temp 53		Muy Alta Temperatura 53			Muy Alta Temp53
508		32			15			High at Temp 53				Hi at Temp 53		Alta Temperatura 53			Alta Temp53
509		32			15			Low at Temp 53				Low at Temp 53		Baja Temperatura 53			Baja Temp53
510		32			15			Very High at Temp 54			VHi at Temp 54		Muy Alta Temperatura 54			Muy Alta Temp54
511		32			15			High at Temp 54				Hi at Temp 54		Alta Temperatura 54			Alta Temp54
512		32			15			Low at Temp 54				Low at Temp 54		Baja Temperatura 54			Baja Temp54
513		32			15			Very High at Temp 55			VHi at Temp 55		Muy Alta Temperatura 55			Muy Alta Temp55
514		32			15			High at Temp 55				Hi at Temp 55		Alta Temperatura 55			Alta Temp55
515		32			15			Low at Temp 55				Low at Temp 55		Baja Temperatura 55			Baja Temp55
516		32			15			Very High at Temp 56			VHi at Temp 56		Muy Alta Temperatura 56			Muy Alta Temp56
517		32			15			High at Temp 56				Hi at Temp 56		Alta Temperatura 56			Alta Temp56
518		32			15			Low at Temp 56				Low at Temp 56		Baja Temperatura 56			Baja Temp56
519		32			15			Very High at Temp 57			VHi at Temp 57		Muy Alta Temperatura 57			Muy Alta Temp57
520		32			15			High at Temp 57				Hi at Temp 57		Alta Temperatura 57			Alta Temp57
521		32			15			Low at Temp 57				Low at Temp 57		Baja Temperatura 57			Baja Temp57
522		32			15			Very High at Temp 58			VHi at Temp 58		Muy Alta Temperatura 58			Muy Alta Temp58
523		32			15			High at Temp 58				Hi at Temp 58		Alta Temperatura 58			Alta Temp58
524		32			15			Low at Temp 58				Low at Temp 58		Baja Temperatura 58			Baja Temp58
525		32			15			Very High at Temp 59			VHi at Temp 59		Muy Alta Temperatura 59			Muy Alta Temp59
526		32			15			High at Temp 59				Hi at Temp 59		Alta Temperatura 59			Alta Temp59
527		32			15			Low at Temp 59				Low at Temp 59		Baja Temperatura 59			Baja Temp59
528		32			15			Very High at Temp 60			VHi at Temp 60		Muy Alta Temperatura 60			Muy Alta Temp60
529		32			15			High at Temp 60				Hi at Temp 60		Alta Temperatura 60			Alta Temp60
530		32			15			Low at Temp 60				Low at Temp 60		Baja Temperatura 60			Baja Temp60
531		32			15			Very High at Temp 61			VHi at Temp 61		Muy Alta Temperatura 61			Muy Alta Temp61
532		32			15			High at Temp 61				Hi at Temp 61		Alta Temperatura 61			Alta Temp61
533		32			15			Low at Temp 61				Low at Temp 61		Baja Temperatura 61			Baja Temp61
534		32			15			Very High at Temp 62			VHi at Temp 62		Muy Alta Temperatura 62			Muy Alta Temp62
535		32			15			High at Temp 62				Hi at Temp 62		Alta Temperatura 62			Alta Temp62
536		32			15			Low at Temp 62				Low at Temp 62		Baja Temperatura 62			Baja Temp62
537		32			15			Very High at Temp 63			VHi at Temp 63		Muy Alta Temperatura 63			Muy Alta Temp63
538		32			15			High at Temp 63				Hi at Temp 63		Alta Temperatura 63			Alta Temp63
539		32			15			Low at Temp 63				Low at Temp 63		Baja Temperatura 63			Baja Temp63
540		32			15			Very High at Temp 64			VHi at Temp 64		Muy Alta Temperatura 64			Muy Alta Temp64
541		32			15			High at Temp 64				Hi at Temp 64		Alta Temperatura 64			Alta Temp64
542		32			15			Low at Temp 64				Low at Temp 64		Baja Temperatura 64			Baja Temp64
543		32			15			Very High at Temp 65			VHi at Temp 65		Muy Alta Temperatura 65			Muy Alta Temp65
544		32			15			High at Temp 65				Hi at Temp 65		Alta Temperatura 65			Alta Temp65
545		32			15			Low at Temp 65				Low at Temp 65		Baja Temperatura 65			Baja Temp65
546		32			15			Very High at Temp 66			VHi at Temp 66		Muy Alta Temperatura 66			Muy Alta Temp66
547		32			15			High at Temp 66				Hi at Temp 66		Alta Temperatura 66			Alta Temp66
548		32			15			Low at Temp 66				Low at Temp 66		Baja Temperatura 66			Baja Temp66
549		32			15			Very High at Temp 67			VHi at Temp 67		Muy Alta Temperatura 67			Muy Alta Temp67
550		32			15			High at Temp 67				Hi at Temp 67		Alta Temperatura 67			Alta Temp67
551		32			15			Low at Temp 67				Low at Temp 67		Baja Temperatura 67			Baja Temp67
552		32			15			Very High at Temp 68			VHi at Temp 68		Muy Alta Temperatura 68			Muy Alta Temp68
553		32			15			High at Temp 68				Hi at Temp 68		Alta Temperatura 68			Alta Temp68
554		32			15			Low at Temp 68				Low at Temp 68		Baja Temperatura 68			Baja Temp68
555		32			15			Very High at Temp 69			VHi at Temp 69		Muy Alta Temperatura 69			Muy Alta Temp69
556		32			15			High at Temp 69				Hi at Temp 69		Alta Temperatura 69			Alta Temp69
557		32			15			Low at Temp 69				Low at Temp 69		Baja Temperatura 69			Baja Temp69
558		32			15			Very High at Temp 70			VHi at Temp 70		Muy Alta Temperatura 70			Muy Alta Temp70
559		32			15			High at Temp 70				Hi at Temp 70		Alta Temperatura 70			Alta Temp70
560		32			15			Low at Temp 70				Low at Temp 70		Baja Temperatura 70			Baja Temp70
561		32			15			Very High at Temp 71			VHi at Temp 71		Muy Alta Temperatura 71			Muy Alta Temp71
562		32			15			High at Temp 71				Hi at Temp 71		Alta Temperatura 71			Alta Temp71
563		32			15			Low at Temp 71				Low at Temp 71		Baja Temperatura 71			Baja Temp71
564		32			15			ESNA Compensation Mode Enable		ESNAComp Mode		Modo Compensación ESNA			Modo CompESNA
565		32			15			ESNA Compensation High Voltage		ESNAComp Hi Volt	Alta tensión Compensación ESNA		Alta V (ESNA)
566		32			15			ESNA Compensation Low Voltage		ESNAComp LowVolt	Baja tensión Compensación ESNA		Baja V (ESNA)
567		32			15			BTRM Temperature			BTRM Temp		Temperatura BTRM			Temp BTRM
568		32			15			Very High BTRM Temperature		VHigh BTRM Temp		Muy Alta Temp BTRM			Tmuy Alta BTRM
569		32			15			High BTRM Temperature			High BTRM Temp		Alta Temp BTRM				T Alta BTRM
570		32			15			Temp Comp Max Voltage (24V)		Temp Comp Max V		Tensión Max Compensación (24V)		Vmax Comp(24V)
571		32			15			Temp Comp Min Voltage (24V)		Temp Comp Min V		Tensión Min Compensación (24V)		Vmin Comp(24V)
572		32			15			BTRM Temperature Sensor			BTRM TempSensor		Sensor Temperatura BTRM			SensorTemp BTRM
573		32			15			BTRM Temperature Sensor Fault		BTRM TempFault		Fallo Sensor temperatura BTRM		Fallo Sens BTRM
574		32			15			Li-Ion Group Avg Temperature		LiBatt AvgTemp		Temperatura media Grupo Li-Ion		T media Bat-Li
575		32			15			Number of Installed Batteries		Num Installed		Número de baterías instaladas		Núm instaladas
576		32			15			Number of Disconnected Batteries	NumDisconnected		Número de baterías desconectadas	Núm desconect
577		32			15			Inventory Update In Process		InventUpdating		Actualizando Inventario			Actualiz Invent
578		32			15			Number of No Reply Batteries		Num No Reply		Núm baterías sin comunicación		N Bat No Resp
579		32			15			Li-Ion Battery Lost			LiBatt Lost		Batería Li-Ion perdida			Bat-Li perdida
580		32			15			System Battery Type			Sys Batt Type		Tipo de batería				Tipo Batería
581		32			15			1 Li-Ion Battery Disconnect		1 LiBattDiscon		1 Batería Li-Ion desconectada		1 Bat-Li descon
582		32			15			2+Li-Ion Battery Disconnect		2+LiBattDiscon		2+Batería Li-Ion desconectada		2+Bat-Li descon
583		32			15			1 Li-Ion Battery No Reply		1 LiBattNoReply		1 Batería Li-Ion no responde		1 BatLi no resp
584		32			15			2+Li-Ion Battery No Reply		2+LiBattNoReply		2+Batería Li-Ion no responde		2+BatLi no resp
585		32			15			Clear Li-Ion Battery Lost		Clr LiBatt Lost		Cesar Batería Li-Ion perdida		Cesar B-Li perd
586		32			15			Clear					Clear			Borrar					Borrar
587		32			15			Float Charge Voltage(Solar)		Float Volt(S)		Tensión de flotación(Solar)		V flotación(S)
588		32			15			Equalize Charge Voltage(Solar)		EQ Voltage(S)		Tensión de carga (Solar)		V Carga (Sol)
589		32			15			Float Charge Voltage(RECT)		Float Volt(R)		Tensión de carga (Rec)			V Carga (Rec)
590		32			15			Equalize Charge Voltage(RECT)		EQ Voltage(R)		Tensión de carga (Rec)			V Carga (Rec)
591		32			15			Active Battery Current Limit		ABCL Point		Límite corriente a Batería-Li		Lim corr Bat-Li
592		32			15			Clear Li Battery CommInterrupt		ClrLiBatComFail		Cesar fallo COM Batería Li-Ion		Cesa COM Bat-Li
593		32			15			ABCL is Active				ABCL Active		ABCL is Active				ABCL Active
594		32			15			Last SMBAT Battery Number		Last SMBAT Num		Número último SMBAT			N último SMBAT
595		32			15			Voltage Adjust Gain			VoltAdjustGain		Ajuste Ganancia Tensión			Ganancia Tens
596		32			15			Curr Limited Mode			Curr Limit Mode		Modo Limitación Corriente		Modo Lim Corr
597		32			15			Current					Current			Corriente				Corriente
598		32			15			Voltage					Voltage			Tensión					Tensión
599		32			15			Battery Charge Prohibited Status	Charge Prohibit		Carga Batería Prohibida			Carga Prohibida
600		32			15			Battery Charge Prohibited Alarm		Charge Prohibit		Carga Batería Prohibida			Carga Prohibida
601		32			15			Battery Lower Capacity			Lower Capacity		Baja Capacidad Batería			Baja Capacidad
602		32			15			Charge Current				Charge Current		Corriente de Carga			Corriente Carga
603		32			15			Upper Limit				Upper Lmt		Límite superior				Límite superior
604		32			15			Stable Range Upper Limit		Stable Up Lmt		Límite Superior de Rango Estable	Lim Sup R.Estable
605		32			15			Stable Range Lower Limit		Stable Low Lmt		Límite Inferior de Rango Estable	Lim Inf R.Estable
606		32			15			Speed Set Point				Speed Set Point		Velocidad				Velocidad
607		32			15			Slow Speed Coefficient			Slow Coeff		Coeficiente Baja velocidad		Coef Baja velocid
608		32			15			Fast Speed Coefficient			Fast Coeff		Coeficiente Alta velocidad		Coef Alta velocid
609		32			15			Min Amplitude				Min Amplitude		Amplitud Mínima				Amplitud Min
610		32			15			Max Amplitude				Max Amplitude		Amplitud Máxima				Amplitud Max
611		32			15			Cycle Number				Cycle Num		Número de Ciclo				Núm Ciclo
613		32			15			EQTemp Comp Coefficient			EQCompCoeff		EQTemp Comp Coefficient			EQCompCoeff		
614		32			15			Mid-Point Volt(MB)			Mid-Point(MB)		Punto med (MB)				Punto med (MB)
615		32			15			Bad Battery Block			Bad BattBlock		Bloque de batería defectuoso		Bloq Bat Def
616		32			15			1					1			1					1
617		32			15			2					2			2					2
618		32			15			Voltage Diff(Mid)			Voltage Diff(Mid)	Dif Tensión(Med)			Dif Tensión(Med)
619		32			15			Bad Battery Block2			Bad BattBlock2		Bloque2 de batería defectuoso		Bloq2 Bat Def
620		32			15			Monitor BattCurrImbal		BattCurrImbal			Corr Bat Alm Dese		CorrBatAlmDese	
621		32			15			Diviation Limit				Diviation Lmt			Límite Desviación		Lmt Desviación	
622		32			15			Allowed Diviation Length	AllowDiviatLen			Long permitido Desvia	LongPermitDesv
623		32			15			Alarm Clear Time			AlarmClrTime			Borrar Alarma Tiempo	BorrarAlmTiem
624		32			15			Battery Test End 10Min		BT End 10Min			Battery Test End 10Min		BT End 10Min

700		32			15			Clear Batt Curr Pos Threshold		Pos Threshold		Claro Umbral Pos Bat Actual 		Umbral Pos
701		32			15			Clear Batt Curr Neg Threshold		Neg Threshold		Claro Umbral Neg Bat Actual		Umbral Neg
702		32			15			Battery Test Multiple Abort				BT MultiAbort		PruebaBatFinal 10min		PruebaBatFinal
703		32			15			Clear Battery Test Multiple Abort		ClrBTMultiAbort		BorrarBatPruebaAbortoMúlt	BorBatPruAboMúl
704		32			15			BT Interrupted-Main Fail				BT Intrp-MF			BT-Interrumpido principal Falla				BT Intrp-MF
705		32			15			Min Voltage for BCL					Min BCL Volt			Voltaje mínimo para BCL		Volt Mínimo BCL
706		32			15			Action of BattFuseAlm				ActBattFuseAlm		Acción alarma fusible batería		AcciAlmFusiBat
707		32			15			Adjust to Min Voltage				AdjustMinVolt		Ajustar a voltaje mínimo			AjustVoltMínimo
708		32			15			Adjust to Default Voltage			AdjustDefltVolt		Ajustar al voltaje predeter			AjustVoltPredet
