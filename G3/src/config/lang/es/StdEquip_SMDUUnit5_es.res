﻿#
# Locale language support: Spanish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		Tensión barra distribución		Tensión barra D
2		32			15			Load Current 1				Load Curr 1		Corriente 1 (Carga)			Corriente 1(C)
3		32			15			Load Current 2				Load Curr 2		Corriente 2 (Carga)			Corriente 2(C)
4		32			15			Load Current 3				Load Curr 3		Corriente 3 (Carga)			Corriente 3(C)
5		32			15			Load Current 4				Load Curr 4		Corriente 4 (Carga)			Corriente 4(C)
6		32			15			Load Fuse 1				Load Fuse1		Fusible carga 1				Fusible carga1
7		32			15			Load Fuse 2				Load Fuse2		Fusible carga 2				Fusible carga2
8		32			15			Load Fuse 3				Load Fuse3		Fusible carga 3				Fusible carga3
9		32			15			Load Fuse 4				Load Fuse4		Fusible carga 4				Fusible carga4
10		32			15			Load Fuse 5				Load Fuse5		Fusible carga 5				Fusible carga5
11		32			15			Load Fuse 6				Load Fuse6		Fusible carga 6				Fusible carga6
12		32			15			Load Fuse 7				Load Fuse7		Fusible carga 7				Fusible carga7
13		32			15			Load Fuse 8				Load Fuse8		Fusible carga 8				Fusible carga8
14		32			15			Load Fuse 9				Load Fuse9		Fusible carga 9				Fusible carga9
15		32			15			Load Fuse 10				Load Fuse10		Fusible carga 10			Fusible carga10
16		32			15			Load Fuse 11				Load Fuse11		Fusible carga 11			Fusible carga11
17		32			15			Load Fuse 12				Load Fuse12		Fusible carga 12			Fusible carga12
18		32			15			Load Fuse 13				Load Fuse13		Fusible carga 13			Fusible carga13
19		32			15			Load Fuse 14				Load Fuse14		Fusible carga 14			Fusible carga14
20		32			15			Load Fuse 15				Load Fuse15		Fusible carga 15			Fusible carga15
21		32			15			Load Fuse 16				Load Fuse16		Fusible carga 16			Fusible carga16
22		32			15			Run Time				Run Time		Tiempo de Operación			Tiempo Operando
23		32			15			LVD1 Control				LVD1 Control		Control LVD1				Control LVD1
24		32			15			LVD2 Control				LVD2 Control		Control LVD2				Control LVD2
25		32			15			LVD1 Voltage				LVD1 Voltage		Tensión LVD1				Tensión LVD1
26		32			15			LVR1 Voltage				LVR1 Voltage		Tensión LVR1				Tensión LVR1
27		32			15			LVD2 Voltage				LVD2 Voltage		Tensión LVD2				Tensión LVD2
28		32			15			LVR2 voltage				LVR2 voltage		Tensión LVR2				Tensión LVR2
29		32			15			On					On			Conectado				Conectado
30		32			15			Off					Off			Desconectado				Desconectado
31		32			15			Normal					Normal			Normal					Normal
32		32			15			Error					Error			Error					Error
33		32			15			On					On			Conectado				Conectado
34		32			15			Fuse 1 Alarm				Fuse1 Alarm		Alarma Fusible 1			Alarma fus1
35		32			15			Fuse 2 Alarm				Fuse2 Alarm		Alarma Fusible 2			Alarma fus2
36		32			15			Fuse 3 Alarm				Fuse3 Alarm		Alarma Fusible 3			Alarma fus3
37		32			15			Fuse 4 Alarm				Fuse4 Alarm		Alarma Fusible 4			Alarma fus4
38		32			15			Fuse 5 Alarm				Fuse5 Alarm		Alarma Fusible 5			Alarma fus5
39		32			15			Fuse 6 Alarm				Fuse6 Alarm		Alarma Fusible 6			Alarma fus6
40		32			15			Fuse 7 Alarm				Fuse7 Alarm		Alarma Fusible 7			Alarma fus7
41		32			15			Fuse 8 Alarm				Fuse8 Alarm		Alarma Fusible 8			Alarma fus8
42		32			15			Fuse 9 Alarm				Fuse9 Alarm		Alarma Fusible 9			Alarma fus9
43		32			15			Fuse 10 Alarm				Fuse10 Alarm		Alarma Fusible 10			Alarma fus10
44		32			15			Fuse 11 Alarm				Fuse11 Alarm		Alarma Fusible 11			Alarma fus11
45		32			15			Fuse 12 Alarm				Fuse12 Alarm		Alarma Fusible 12			Alarma fus12
46		32			15			Fuse 13 Alarm				Fuse13 Alarm		Alarma Fusible 13			Alarma fus13
47		32			15			Fuse 14 Alarm				Fuse14 Alarm		Alarma Fusible 14			Alarma fus14
48		32			15			Fuse 15 Alarm				Fuse15 Alarm		Alarma Fusible 15			Alarma fus15
49		32			15			Fuse 16 Alarm				Fuse16 Alarm		Alarma Fusible 16			Alarma fus16
50		32			15			HW Test Alarm				HW Test Alarm		Alarma prueba HW			Alarma test HW
51		32			15			SM-DU Unit 5				SM-DU 5			SM-DU 5					SM-DU 5
52		32			15			Battery Fuse 1 Voltage			BATT Fuse1 Volt		Tensión fusible batería 1		Tens fus bat1
53		32			15			Battery Fuse 2 Voltage			BATT Fuse2 Volt		Tensión fusible batería 2		Tens fus bat2
54		32			15			Battery Fuse 3 Voltage			BATT Fuse3 Volt		Tensión fusible batería 3		Tens fus bat3
55		32			15			Battery Fuse 4 Voltage			BATT Fuse4 Volt		Tensión fusible batería 4		Tens fus bat4
56		32			15			Battery Fuse 1 Status			BATT Fuse1 Stat		Estado fusible batería 1		Estado fus bat1
57		32			15			Battery Fuse 2 Status			BATT Fuse2 Stat		Estado fusible batería 2		Estado fus bat2
58		32			15			Battery Fuse 3 Status			BATT Fuse3 Stat		Estado fusible batería 3		Estado fus bat3
59		32			15			Battery Fuse 4 Status			BATT Fuse4 Stat		Estado fusible batería 4		Estado fus bat4
60		32			15			On					On			Conectado				Conectado
61		32			15			Off					Off			Desconectado				Desconectado
62		32			15			Battery Fuse 1 Alarm			Bat Fuse1 Alarm		Alarma fusible batería 1		Alarma fus1 bat
63		32			15			Battery Fuse 2 Alarm			Bat Fuse2 Alarm		Alarma fusible batería 2		Alarma fus2 bat
64		32			15			Battery Fuse 3 Alarm			Bat Fuse3 Alarm		Alarma fusible batería 3		Alarma fus3 bat
65		32			15			Battery Fuse 4 Alarm			Bat Fuse4 Alarm		Alarma fusible batería 4		Alarma fus4 bat
66		32			15			Total Load Current			Tot Load Curr		Corriente total Carga			Total Carga
67		32			15			Over Load Current Limit			Over Curr Lim		Valor de sobrecorriente			Sobrecorriente
68		32			15			Over Current				Over Current		Sobrecorriente				Sobrecorriente
69		32			15			LVD1 Enabled				LVD1 Enabled		LVD1 habilitado				LVD1 habilitado
70		32			15			LVD1 Mode				LVD1 Mode		Modo LVD1				Modo LVD1
71		32			15			LVD1 Reconnect Delay			LVD1Recon Delay		Retardo reconexión LVD1			RetarRecon LVD1
72		32			15			LVD2 Enabled				LVD2 Enabled		LVD2 habilitado				LVD2 habilitado
73		32			15			LVD2 Mode				LVD2 Mode		Modo LVD2				Modo LVD2
74		32			15			LVD2 Reconnect Delay			LVD2Recon Delay		Retardo reconexión LVD2			RetarRecon LVD2
75		32			15			LVD1 Status				LVD1 Status		Estado LVD1				Estado LVD1
76		32			15			LVD2 Status				LVD2 Status		Estado LVD2				Estado LVD2
77		32			15			Disabled				Disabled		Deshabilitado				Deshabilitado
78		32			15			Enabled					Enabled			Habilitado				Habilitado
79		32			15			By Voltage				By Volt			Por tensión				Por tensión
80		32			15			By Time					By Time			Por tiempo				Por tiempo
81		32			15			Bus Bar Volt Alarm			Bus Bar Alarm		Alarma Barra Distribución		Alarma bus Dist
82		32			15			Normal					Normal			Normal					Normal
83		32			15			Low					Low			Bajo					Bajo
84		32			15			High					High			Alto					Alto
85		32			15			Low Voltage				Low Voltage		Baja tensión				Baja tensión
86		32			15			High Voltage				High Voltage		Alta tensión				Alta tensión
87		32			15			Shunt 1 Current Alarm			Shunt1 Alarm		Alarma corriente shunt1			Alarma shunt1
88		32			15			Shunt 2 Current Alarm			Shunt2 Alarm		Alarma corriente shunt2			Alarma shunt2
89		32			15			Shunt 3 Current Alarm			Shunt3 Alarm		Alarma corriente shunt3			Alarma shunt3
90		32			15			Shunt 4 Current Alarm			Shunt4 Alarm		Alarma corriente shunt4			Alarma shunt4
91		32			15			Shunt 1 Over Current			Shunt1 Over Cur		Sobrecorriente en shunt1		Sobrecor shunt1
92		32			15			Shunt 2 Over Current			Shunt2 Over Cur		Sobrecorriente en shunt2		Sobrecor shunt2
93		32			15			Shunt 3 Over Current			Shunt3 Over Cur		Sobrecorriente en shunt3		Sobrecor shunt3
94		32			15			Shunt 4 Over Current			Shunt4 Over Cur		Sobrecorriente en shunt4		Sobrecor shunt4
95		32			15			Interrupt Times				Interrupt Times		Interrupciones				Interrupciones
96		32			15			Existent				Existent		Existente				Existente
97		32			15			Non-Existent				Non-Existent		Inexistente				Inexistente
98		32			15			Very Low				Very Low		Muy bajo				Muy bajo
99		32			15			Very High				Very High		Muy alto				Muy alto
100		32			15			Switch					Switch			Interruptor				Interruptor
101		32			15			LVD1 Failure				LVD1 Failure		Fallo LVD1				Fallo LVD1
102		32			15			LVD2 Failure				LVD2 Failure		Fallo LVD2				Fallo LVD2
103		32			15			HTD1 Enable				HTD1 Enable		Habilitar HTD1				Habilitar HTD1
104		32			15			HTD2 Enable				HTD2 Enable		Habilitar HTD2				Habilitar HTD2
105		32			15			Battery LVD				Battery LVD		LVD de batería				LVD de batería
106		32			15			No Battery				No Battery		Sin batería				Sin batería
107		32			15			LVD1					LVD1			LVD1					LVD1
108		32			15			LVD2					LVD2			LVD2					LVD2
109		32			15			Battery Always On			BattAlwaysOn		Batería siempre conectada		Siempre con Bat
110		32			15			Barcode					Barcode			Barcode					Barcode
111		32			15			DC Overvoltage				DC Overvolt		Sobretensión CC				Sobretensión CC
112		32			15			DC Undervoltage				DC Undervolt		Subtensión CC				Subtensión CC
113		32			15			Overcurrent 1				Overcurr 1		Sobrecorriente 1			Sobrecorrien 1
114		32			15			Overcurrent 2				Overcurr 2		Sobrecorriente 2			Sobrecorrien 2
115		32			15			Overcurrent 3				Overcurr 3		Sobrecorriente 3			Sobrecorrien 3
116		32			15			Overcurrent 4				Overcurr 4		Sobrecorriente 4			Sobrecorrien 4
117		32			15			Existence State				Existence State		Detección				Detección
118		32			15			Communication Interrupt			Comm Interrupt		Interrupción Comunicación		Interrup COM
119		32			15			Bus Voltage Status			Bus Status		Estado Bus Tensión			Estado Bus V
120		32			15			Communication OK			Comm OK			Comunicación bien			COM bien
121		32			15			None is Responding			None Responding		Ninguno Responde			No Responden
122		32			15			No Response				No Response		No responde				No responde
123		32			15			Rated Battery Capacity			Rated Bat Cap		Capacidad Estimada			Capacidad Est
124		32			15			Load Current 5				Load Curr 5		Corriente 5 (Carga)			Corriente 5(C)
125		32			15			Shunt 1 Voltage				Shunt 1 Voltage		Tensión Shunt 1				Tensión Shunt1
126		32			15			Shunt 1 Current				Shunt 1 Current		Corriente Shunt 1			Corrien Shunt1
127		32			15			Shunt 2 Voltage				Shunt 2 Voltage		Tensión Shunt 2				Tensión Shunt2
128		32			15			Shunt 2 Current				Shunt 2 Current		Corriente Shunt 2			Corrien Shunt2
129		32			15			Shunt 3 Voltage				Shunt 3 Voltage		Tensión Shunt 3				Tensión Shunt3
130		32			15			Shunt 3 Current				Shunt 3 Current		Corriente Shunt 3			Corrien Shunt3
131		32			15			Shunt 4 Voltage				Shunt 4 Voltage		Tensión Shunt 4				Tensión Shunt4
132		32			15			Shunt 4 Current				Shunt 4 Current		Corriente Shunt 4			Corrien Shunt4
133		32			15			Shunt 5 Voltage				Shunt 5 Voltage		Tensión Shunt 5				Tensión Shunt5
134		32			15			Shunt 5 Current				Shunt 5 Current		Corriente Shunt 5			Corrien Shunt5
150		32			15			Battery Current 1			Batt Curr 1		Corriente 1 (Batería)			Corriente 1(B)
151		32			15			Battery Current 2			Batt Curr 2		Corriente 2 (Batería)			Corriente 2(B)
152		32			15			Battery Current 3			Batt Curr 3		Corriente 3 (Batería)			Corriente 3(B)
153		32			15			Battery Current 4			Batt Curr 4		Corriente 4 (Batería)			Corriente 4(B)
154		32			15			Battery Current 5			Batt Curr 5		Corriente 5 (Batería)			Corriente 5(B)
170		32			15			High Current 1				Hi Current 1		Alta corriente 1			Alta Corr 1
171		32			15			Very High Current 1			VHi Current 1		Muy alta corriente 1			Muy alta corr1
172		32			15			High Current 2				Hi Current 2		Alta corriente 2			Alta Corr 2
173		32			15			Very High Current 2			VHi Current 2		Muy alta corriente 2			Muy alta corr2
174		32			15			High Current 3				Hi Current 3		Alta corriente 3			Alta Corr 3
175		32			15			Very High Current 3			VHi Current 3		Muy alta corriente 3			Muy alta corr3
176		32			15			High Current 4				Hi Current 4		Alta corriente 4			Alta Corr 4
177		32			15			Very High Current 4			VHi Current 4		Muy alta corriente 4			Muy alta corr4
178		32			15			High Current 5				Hi Current 5		Alta corriente 5			Alta Corr 5
179		32			15			Very High Current 5			VHi Current 5		Muy alta corriente 5			Muy alta corr5
220		32			15			Current 1 High Limit			Curr 1 Hi Lim		Límite Alta corriente 1			Lim Alta Corr1
221		32			15			Current 1 Very High Limit		Curr 1 VHi Lim		Límite Muy alta corriente 1		Muy alta corr1
222		32			15			Current 2 High Limit			Curr 2 Hi Lim		Límite Alta corriente 2			Lim Alta Corr2
223		32			15			Current 2 Very High Limit		Curr 2 VHi Lim		Límite Muy alta corriente 2		Muy alta corr2
224		32			15			Current 3 High Limit			Curr 3 Hi Lim		Límite Alta corriente 3			Lim Alta Corr3
225		32			15			Current 3 Very High Limit		Curr 3 VHi Lim		Límite Muy alta corriente 3		Muy alta corr3
226		32			15			Current 4 High Limit			Curr 4 Hi Lim		Límite Alta corriente 4			Lim Alta Corr4
227		32			15			Current 4 Very High Limit		Curr 4 VHi Lim		Límite Muy alta corriente 4		Muy alta corr4
228		32			15			Current 5 High Limit			Curr 5 Hi Lim		Límite Alta corriente 5			Lim Alta Corr5
229		32			15			Current 5 Very High Limit		Curr 5 VHi Lim		Límite Muy alta corriente 5		Muy alta corr5
270		32			15			Current 1 Breaker Size			Curr 1 Brk Size		Talla disyuntor corriente 1		Talla Disy I1
271		32			15			Current 2 Breaker Size			Curr 2 Brk Size		Talla disyuntor corriente 2		Talla Disy I2
272		32			15			Current 3 Breaker Size			Curr 3 Brk Size		Talla disyuntor corriente 3		Talla Disy I3
273		32			15			Current 4 Breaker Size			Curr 4 Brk Size		Talla disyuntor corriente 4		Talla Disy I4
274		32			15			Current 5 Breaker Size			Curr 5 Brk Size		Talla disyuntor corriente 5		Talla Disy I5
281		32			15			Shunt Size Switch			Shunt Size Switch	Configuración valor shunt		Cfg Valor Shunt
283		32			15			Shunt 1 Size Conflicting		Sh 1 Conflict		Conflicto config Shunt 1		Conflict Shunt1
284		32			15			Shunt 2 Size Conflicting		Sh 2 Conflict		Conflicto config Shunt 2		Conflict Shunt2
285		32			15			Shunt 3 Size Conflicting		Sh 3 Conflict		Conflicto config Shunt 3		Conflict Shunt3
286		32			15			Shunt 4 Size Conflicting		Sh 4 Conflict		Conflicto config Shunt 4		Conflict Shunt4
287		32			15			Shunt 5 Size Conflicting		Sh 5 Conflict		Conflicto config Shunt 5		Conflict Shunt5
290		32			15			By Software				By Software		Software				Software
291		32			15			By Dip Switch				By Dip Switch		Microint				Microint
292		32			15			No Supported				No Supported		Ninguno					Ninguno
293		32			15			Not Used				Not Used		No Utilizada				No Utilizada
294		32			15			General					General			General					General
295		32			15			Load					Load			Carga					Carga
296		32			15			Battery					Battery			Batería					Batería
297		32			15			Shunt1 Set As				Shunt1SetAs		Shunt1 fijado como			Shunt1 como
298		32			15			Shunt2 Set As				Shunt2SetAs		Shunt2 fijado como			Shunt2 como
299		32			15			Shunt3 Set As				Shunt3SetAs		Shunt3 fijado como			Shunt3 como
300		32			15			Shunt4 Set As				Shunt4SetAs		Shunt4 fijado como			Shunt4 como
301		32			15			Shunt5 Set As				Shunt5SetAs		Shunt5 fijado como			Shunt5 como
302		32			15			Shunt1 Reading				Shunt1Reading		Lectura Shunt1				Lectura Shunt1
303		32			15			Shunt2 Reading				Shunt2Reading		Lectura Shunt2				Lectura Shunt2
304		32			15			Shunt3 Reading				Shunt3Reading		Lectura Shunt3				Lectura Shunt3
305		32			15			Shunt4 Reading				Shunt4Reading		Lectura Shunt4				Lectura Shunt4
306		32			15			Shunt5 Reading				Shunt5Reading		Lectura Shunt5				Lectura Shunt5
307		32			15			Load1 Alarm Flag			Load1 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
308		32			15			Load2 Alarm Flag			Load2 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
309		32			15			Load3 Alarm Flag			Load3 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
310		32			15			Load4 Alarm Flag			Load4 Alarm Flag	Load4 Alarm Flag			Load4 Alarm Flag
311		32			15			Load5 Alarm Flag			Load5 Alarm Flag	Load5 Alarm Flag			Load5 Alarm Flag
500		32			15			Current1 High 1 Curr			Curr1 Hi1Cur		Corr1 Alta 1 Corr				Corr1Alta1Corr	
501		32			15			Current1 High 2 Curr			Curr1 Hi2Cur		Corr1 Alta 2 Corr				Corr1Alta2Corr	
502		32			15			Current2 High 1 Curr			Curr2 Hi1Cur		Corr2 Alta 1 Corr				Corr2Alta1Corr	
503		32			15			Current2 High 2 Curr			Curr2 Hi2Cur		Corr2 Alta 2 Corr				Corr2Alta2Corr	
504		32			15			Current3 High 1 Curr			Curr3 Hi1Cur		Corr3 Alta 1 Corr				Corr3Alta1Corr	
505		32			15			Current3 High 2 Curr			Curr3 Hi2Cur		Corr3 Alta 2 Corr				Corr3Alta2Corr	
506		32			15			Current4 High 1 Curr			Curr4 Hi1Cur		Corr4 Alta 1 Corr				Corr4Alta1Corr	
507		32			15			Current4 High 2 Curr			Curr4 Hi2Cur		Corr4 Alta 2 Corr				Corr4Alta2Corr	
508		32			15			Current5 High 1 Curr			Curr5 Hi1Cur		Corr5 Alta 1 Corr				Corr5Alta1Corr	
509		32			15			Current5 High 2 Curr			Curr5 Hi2Cur		Corr5 Alta 2 Corr				Corr5Alta2Corr
550		32			15			Source							Source				Fuente					Fuente		
