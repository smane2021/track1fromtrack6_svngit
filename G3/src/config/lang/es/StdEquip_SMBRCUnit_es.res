﻿#
# Locale language support: Spanish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
51		32			15			SM-BRC Unit				SM-BRC Unit		Unidad SMBRC				Unidad SMBRC
95		32			15			Interrupt Times				Interrupt Times		Núm de Interrupciones			Interrupciones
96		32			15			Existent				Existent		Existente				Existente
97		32			15			Non-Existent				Non-Existent		Inexistente				Inexistente
117		32			15			Existence State				Existence State		Detección				Detección
118		32			15			Communication Failure			Comm Fail		Interrupción Comunicación		Interrup COM
120		32			15			Communication OK			Comm OK			Comunicación bien			COM bien
121		32			15			All Communication Failure		All Comm Fail		Ninguno responde			Sin respuesta
122		32			15			Communication Failure			Comm Fail		No responde				No responde
123		32			15			Rated Capacity				Rated Capacity		Capacidad estimada			Capacidad est
150		32			15			Digital In Number			Digital In Num		Núm Digitales de Entrada (DI)		Núm DI
151		32			15			Digital Input 1				Digital Input1		DI 1					DI 1
152		32			15			Low					Low			Bajo					Bajo
153		32			15			High					High			Alto					Alto
154		32			15			Digital Input 2				Digital Input2		DI 2					DI 2
155		32			15			Digital Input 3				Digital Input3		DI 3					DI 3
156		32			15			Digital Input 4				Digital Input4		DI 4					DI 4
157		32			15			Digital Out Number			Digital Out Num		Núm Digitales de Salida (DO)		Núm DO
158		32			15			Digital Output 1			Digital Output1		DO 1					DO 1
159		32			15			Digital Output 2			Digital Output2		DO 2					DO 2
160		32			15			Digital Output 3			Digital Output3		DO 3					DO 3
161		32			15			Digital Output 4			Digital Output4		DO 4					DO 4
162		32			15			Operation State				Op State		Estado Operación			Estado Opera
163		32			15			Normal					Normal			Normal					Normal
164		32			15			Test					Test			Prueba					Prueba
165		32			15			Discharge				Discharge		Descarga				Descarga
166		32			15			Calibration				Calibration		Calibración				Calibración
167		32			15			Diagnostic				Diagnostic		Diagnóstico				Diagnóstico
168		32			15			Maintenance				Maintenance		Mantenimiento				Mantenimiento
169		32			15			Resistence Test Interval		Resist Test Int		Intervalo Prueba de Resistencia		Int Prueba Res
170		32			15			Ambient Temperature Value		Amb Temp Value		Temperatura Ambiente			Temp Ambiente
171		32			15			High Ambient Temperature		Hi Amb Temp		Alta Temperatura Ambiente		Alta Temp Amb
172		32			15			Configuration Number			Cfg Num			Núm de configuración			Núm Config
173		32			15			Number of Batteries			No. of Batt		Número de Baterías			Núm Baterías
174		32			15			Unit Sequence Number			Unit Seq Num		Número de Secuencia Unidad		Núm Secuencia
175		32			15			Start Battery Sequence			Start Batt Seq		Iniciar Secuencia de Batería		Inic SecuenBat
176		32			15			Low Ambient Temperature			Lo Amb Temp		Baja temperatura ambiente		Baja Temp Amb
177		34			15			Ambient Temperature Probe Failure	Amb Temp Fail		Fallo sensor Temperatura ambiente	Fallo Sens Amb
