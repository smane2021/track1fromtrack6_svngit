﻿#
#  Locale language support:tr
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
tr

[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN			FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32			15			Output AC Voltage			Output AC Volt			Çıkış AC Voltajı		Çıkış AC Volt
2	32			15			Output AC Current			Output AC Curr			Çıkış Akım Akımı		ÇıkışAkımAkım
3	32			15			Output Apparent Power			Apparent Power			Çıkış Görünen Güç		Görünür güç
4	32			15			Output Active Power			Active Power			Çıkış Aktif Gücü		Aktif güç
5	32			15			Input AC Voltage			Input AC Volt			Giriş AC Voltajı		Giriş AC Volt
6	32			15			Input AC Current			Input AC Curr			Giriş AC Akımı			Giriş AC Akımı
7	32			15			Input AC Power				Input AC Power			Giriş AC Gücü			Giriş AC Gücü
8	32			15			Input AC Power				Input AC Power			Giriş AC Gücü			Giriş AC Gücü
9	32			15			Input AC Frequency			Input AC Freq			Giriş AC Frekansı		Giriş AC Frekan
10	32			15			Input DC Voltage			Input DC Volt			Giriş DC Voltajı		Giriş DC Volt
11	32			15			Input DC Current			Input DC Curr			Giriş DC Akımı			Giriş DC Akımı
12	32			15			Input DC Power				Input DC Power			Giriş DC Gücü			Giriş DC Gücü
13	32			15			Communication Failure			Comm Fail			İletişim Arızası		İletişim Arızası
14	32			15			Existence State				Existence State			Varoluş hali			Varoluş hali
	
98	32			15			TSI Inverter				TSI Inverter			CI Çevirici			CI Çevirici
101	32			15			Fan Failure				Fan Fail			Fan sorunu			Fan sorunu	
102	32			15			Too Many Starts				Too Many Starts			Çok fazla başlama		Çok fazla başla	
103	32			15			Overload Too Long			Overload Long			Aşırı Yük Çok Uzun		Aşı Yük ÇokUzun
104	32			15			Out Of Sync				Out Of Sync			Senkron dışında			Senkron dışında
105	32			15			Temperature Too High			Temp Too High			Sıcaklık Çok Yüksek		Sıcak Çok Yük	
106	32			15			Communication Bus Failure		Com Bus Fail			İletişim Bus Hatası		İleti Bus Hata
107	32			15			Communication Bus Confilct		Com BusConfilct	İletişim	Veriyolu Çatışması		İleti Bus Çatı
108	32			15			No Power Source				No Power			Güç yok				Güç yok	
109	32			15			Communication Bus Failure		Com Bus Fail			İletişim Bus Hatası		İleti Bus Hata
110	32			15			Phase Not Ready				Phase Not Ready			Faz Hazır Değil			Faz Hazır Değil
111	32			15			Inverter Mismatch			Inv Mismatch			İnverter Uyuşmazlığı		İnver Uyuşma
112	32			15			Backfeed Error				Backfeed Error			Geri Besleme Hatası		GeriBeslHatası	
113	32			15			T2S Communication Bus Failure		Com Bus Fail			T2S İletişim Bus Hatası		İleti Bus Hata
114	32			15			T2S Communication Bus Failure		Com Bus Fail			T2S İletişim Bus Hatası		İleti Bus Hata
115	32			15			Overload Current			Overload Curr			Aşırı Yük Akımı			Aşırı YükAkımı	
116	32			15			Communication Bus Mismatch		Com Bus Mismatch		İleti Veri Uyuş			İleti Veri Uyuş
117	32			15			Temperature Derating			Temp Derating			Sıcaklık Düşürme		Sıcakl Düşürme	
118	32			15			Overload Power				Overload Power			Aşırı Yük Gücü			Aşırı Yük Gücü	
119	32			15			Undervoltage Derating			Undervolt Derat			Düşük Gerilim Düşürme		DüşükGeriDüşür	
120	32			15			Fan Failure				Fan Failure			Fan sorunu			Fan sorunu
121	32			15			Remote Off				Remote Off			Uzaktan Kapalı			Uzaktan Kapalı	
122	32			15			Manually Off				Manually Off			El ile Kapalı			El ile Kapalı	
123	32			15			Input AC Voltage Too Low		Input AC Too Low		GirişACÇokDüşük			GirişACÇokDüşük
124	32			15			Input AC Voltage Too High		Input AC Too High		GirişACÇok Yük			GirişACÇok Yük
125	32			15			Input AC Voltage Too Low		Input AC Too Low		GirişACÇokDüşük			GirişACÇokDüşük
126	32			15			Input AC Voltage Too High		Input AC Too High		GirişACÇok Yük			GirişACÇok Yük
127	32			15			Input AC Voltage Inconformity		Input AC Inconform		GirişACUyumsuz			GirişACUyumsuz
128	32			15			Input AC Voltage Inconformity		Input AC Inconform		GirişACUyumsuz			GirişACUyumsuz
129	32			15			Input AC Voltage Inconformity		Input AC Inconform		GirişACUyumsuz			GirişACUyumsuz
130	32			15			Power Disabled				Power Disabled			Güç Devre Dışı			Güç Devre Dışı	
131	32			15			Input AC Inconformity			Input AC Inconform		Giriş AC Uyumsuzluğu		GirişACUyumsuz
132	32			15			Input AC THD Too High			Input AC THD High		Giriş AC THD Çok Yüksek		GirişACTHDYük
133	32			15			Output AC Not Synchronized		AC Not Syned			Çıktı AC Senkronize Değil	Senkron dışında
134	32			15			Output AC Not Synchronized		AC Not Synced			Çıktı AC Senkronize Değil	Senkron dışında
135	32			15			Inverters Not Synchronized		Inverters Not Synced		İnvertSenkOlmad			İnvertSenkOlmad
136	32			15			Synchronization Failure			Sync Failure			Senkronizasyon Hatası		Senkron Hatası
137	32			15			Input AC Voltage Too Low		AC Voltage Too Low		GirişACÇokDüşük			GirişACÇokDüşük
138	32			15			Input AC Voltage Too High		AC Voltage Too High		GirişACÇok Yük			GirişACÇok Yük
139	32			15			Input AC Frequency Too Low		AC Frequency Low		Frekans Düşük			Frekans Düşük
140	32			15			Input AC Frequency Too High		AC Frequency High		Yüksek Frekans			Yüksek Frekans
141	32			15			Input DC Voltage Too Low		Input DC Too Low		GirişDCÇokDüşük			GirişDCÇokDüşük
142	32			15			Input DC Voltage Too High		Input DC Too High		GirişDCÇok Yük			GirişDCÇok Yük
143	32			15			Input DC Voltage Too Low		Input DC Too Low		GirişDCÇokDüşük			GirişDCÇokDüşük
144	32			15			Input DC Voltage Too High		Input DC Too High		GirişDCÇok Yük			GirişDCÇok Yük
145	32			15			Input DC Voltage Too Low		Input DC Too Low		GirişDCÇokDüşük			GirişDCÇokDüşük
146	32			15			Input DC Voltage Too Low		Input DC Too Low		GirişDCÇokDüşük			GirişDCÇokDüşük
147	32			15			Input DC Voltage Too High		Input DC Too High		GirişDCÇok Yük			GirişDCÇok Yük
148	32			15			Digital Input 1 Failure			DI1 Failure			Dijital Giriş 1 Arızası		DI1 Arızası
149	32			15			Digital Input 2 Failure			DI2 Failure			Dijital Giriş 2 Arızası		DI2 Arızası
150	32			15			Redundancy Lost				Redundancy Lost			Fazlalık Kayıp			Fazlalık Kayıp
151	32			15			Redundancy+1 Lost			Redund+1 Lost			Fazlalık+1 Kayıp		Fazla+1 Kayıp
152	32			15			System Overload				Sys Overload			Sistem aşırı yüklenmesi		Sis aşırı yükl
153	32			15			Main Source Lost			Main Lost			Ana Kaynak Kaybı		Ana Kaynak	
154	32			15			Secondary Source Lost			Secondary Lost			İkincil Kaynak Kaybedildi	İkincil Kayıp	
155	32			15			T2S Bus Failure				T2S Bus Failure			T2S Otobüs Arızası		Otobüs Arızası
156	32			15			T2S Failure				T2S Failure			T2S Başarısızlığı		T2S Başarısızl
157	32			15			Log Full				Log Full			Neredeyse Tam Giriş Logosu	Günlük Kaydı	
158	32			15			T2S Flash Error				Flash Error			T2S Flaş Hatası			T2S Flaş Hatası
159	32			15			Check Log File				Check Log File			Günlük Dosyası Kontrol		GünDosyaKontrol
160	32			15			Module Lost				Module Lost			Kayıp Modül			Kayıp Modül
