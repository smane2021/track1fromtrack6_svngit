#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name 
    
[LOCALE_LANGUAGE]
tr
[RES_INFO]
#R_ID	MAX_LEN_FULL	MAX_LEN_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32		16		Power System			Power System		Güc Sistemi			Güc Sistemi
2	32		15		System Voltage			Sys Volt		Sistem Gerilimi			Sistem Gerilimi
3	32		15		System Output Current		Output Current		Sistem Cikis Akimi		Cikis Akimi
4	32		15		System Output Power		Output Power		Sistem Cikis Gucu		Cikis Gucu
5	32		15		Total Power Consumption		Power Consump		Toplam Guc Tuketimi		Guc Tuketimi
6	32		15		Power Peak in 24 Hours		Power Peak		24 Saatteki Peak Guc		Peak Guc
7	32		15		Average Power in 24 Hours	Avg power		24 Saatteki Ortalama Guc	OrtalamaGuc
8	32		15		Hardware Write-protect Switch	Hardware Switch		HW Yazma-Korumasi Anahtari	HW Korumasi
9	32		15		Ambient Temperature		Amb Temp		Ortam Sicakligi			Ortam Sicakligi
10	32		15		Number of SM-DUs		No of SM-DUs		SMDU Sayisi			SMDU Sayisi
18	32		15		Relay Output 1			Relay Output 1		Role Cikis 1			Role 1
19	32		15		Relay Output 2			Relay Output 2		Role Cikis 2			Role 2
20	32		15		Relay Output 3			Relay Output 3		Role Cikis 3			Role 3
21	32		15		Relay Output 4			Relay Output 4		Role Cikis 4			Role 4
22	32		15		Relay Output 5			Relay Output 5		Role Cikis 5			Role 5
23	32		15		Relay Output 6			Relay Output 6		Role Cikis 6			Role 6
24	32		15		Relay Output 7			Relay Output 7		Role Cikis 7			Role 7
25	32		15		Relay Output 8			Relay Output 8		Role Cikis 8			Role 8
27	32		15		Undervoltage 1 Level		Undervoltage 1		Dusuk Gerilim 1			Dusuk Ger1
28	32		15		Undervoltage 2 Level		Undervoltage 2		Dusuk Gerilim 2			Dusuk Ger2
29	32		15		Overvoltage 1 Level		Overvoltage 1		Yuksek Gerilim 1		Yuksek Ger1
31	32		15		Temperature 1 High Limit	Temp 1 High		Yuksek Sicaklik 1 Limiti	Yuksek Sic 1
32	32		15		Temperature 1 Low Limit		Temp 1 Low		Dusuk Sicaklik 1 Limiti		Dusuk Sic 1
33	32		15		Auto/Man State			Auto/Man State		Oto/Manuel Durumu		Oto/Man Durumu
34	32		15		Outgoing Alarms Blocked		Alarms Blocked 		Giden Alarm Durduruldu		Alarm Durduruldu
35	32		15		Supervision Unit Fail		SupV Unit Fail		Denetleme Birimi Hata		Dahili Hata
36	32		15		CAN Communication Fail		CAN Comm Fail		CAN Haberlesme Hatasi		Can Hab.Hatasi
37	32		15		Mains Fail			Mains Fail		AC Sebeke Arizasi		AC Sebeke Arizasi
38	32		15		Undervoltage 1			Undervolt 1		Dusuk Gerilim 1			Dusuk Ger1
39	32		15		Undervoltage 2			Undervolt 2		Dusuk Gerilim 2			Dusuk Ger2
40	32		15		Overvoltage 1			Overvolt 1		Yuksek Gerilim1			Yuksek Ger1
41	32		15		High Temperature 1		High Temp 1		Yuksek Sicaklik 1		Yuksek Sic1
42	32		15		Low Temperature 1		Low Temp 1		Dusuk Sicaklik 1		Dusuk Sic1
43	32		15		Temperature 1 Sensor Fail	T1 Sensor Fail		Sicaklik Sensor1 Hatasi		T1 hatasi 1
44	32		15		Outgoing Alarms Blocked		Alarms Blocked		Giden Alarm Durduruldu		Alarm Durduruldu
45	32		15		Maintenance Alarm		MaintenanceAlrm		Bakim Alarmi			Bakim Alarmi
46	32		15		Not Protected			Not protected		Koruma Yok			Koruma Yok
47	32		15		Protected			Protected		Korunuyor			Korunuyor
48	32		15		Normal				Normal			Normal				Normal
49	32		15		Fail				Fail			Hata				Hata
50	32		15		Off				Off			Normal				Normal
51	32		15		On				On			Hata				Hata
52	32		15		Off				Off			Normal				Normal
53	32		15		On				On			Hata				Hata
54	32		15		Off				Off			Normal				Normal
55	32		15		On				On			Hata				Hata
56	32		15		Off				Off			Normal				Normal
57	32		15		On				On			Hata				Hata
58	32		15		Off				Off			Normal				Normal
59	32		15		On				On			Hata				Hata
60	32		15		Off				Off			Normal				Normal
61	32		15		On				On			Hata				Hata
62	32		15		Off				Off			Normal				Normal
63	32		15		On				On			Hata				Hata
64	32		15		Open				Open			Acik				Acik
65	32		15		Closed				Closed			Kapali				Kapali
66	32		15		Open				Open			Acik				Acik
67	32		15		Closed				Closed			Kapali				Kapali
78	32		15		Off				Off			Kapat				Kapat
79	32		15		On				On			Ac				Ac
80	32		15		Auto				Auto			Oto				Oto
81	32		15		Manual				Manual			Manual				Manual
82	32		15		Normal				Normal			Normal				Normal
83	32		15		Blocked				Blocked			Engelli				Engelli
85	32		15		Set to Auto Mode		To Auto Mode		Oto Modu Ayarla			Oto Mod
86	32		15		Set to Manual Mode		To Manual Mode		Manuel Modu Ayarla		Manuel Mod
87	32		15		Power Rate Level		PowerRate Level		Guc Orani Seviyesi		GucOrani Sev.
88	32		15		Current Power Peak Limit	Power Peak Lim		Peak Guc Akim limiti		Peak Guc Limiti
89	32		15		Peak				Peak			Peak				Peak
90	32		15		High				High			Yuksek				Yuksek
91	32		15		Flat				Flat			Duz				Duz
92	32		15		Low				Low			Dusuk				Dusuk
93	32		15		Lower Consumption		Lower Consump		Dusuk Tuketim			Dusuk Tuketim
94	32		15		Power Peak Savings		Pow Peak Save		Peak Guc Tasarrufu		PeakGucTasarrufu
95	32		15		Disable				Disable			Pasif				Pasif
96	32		15		Enable				Enable			Aktif				Aktif
97	32		15		Disable				Disable			Pasif				Pasif
98	32		15		Enable				Enable			Aktif				Aktif
99	32		15		Over Maximum Power		Over Power		Max.Gucun Ustunde		Yuksek Guc
100	32		15		Normal				Normal			Normal				Normal
101	32		15		Alarm				Alarm			Alarm				Alarm
102	32		15		Over Maximum Power Alarm	Over Power		MaxGucYuksek Alarmi		YuksekGucAlarmi
104	32		15		System Alarm Status		Alarm Status		Sistem Alarm Durumu		Alarm Durumu
105	32		15		No Alarms			No Alarms		Alarm Yok			Alarm Yok
106	32		15		Observation Alarm		Observation		Minor Alarm			Minor
107	32		15		Major Alarm			Major			Major Alarm			Major 
108	32		15		Critical Alarm			Critical		Kritik Alarm			Kritik
109	32		15		Maintenance Run Time		Mtn Run Time		Bakim Calisma Zamani		BakimCalismaZmn
110	32		15		Maintenance Interval		Mtn Interval		Bakim Araligi			Bakim Araligi
111	32		15		Maintenance Alarm Time		Mtn Alarm Time		Bakim Alarm Zamani		BakimAlarmZmn
112	32		15		Maintenance Time Out		Mtn Time Out		Bakim Suresi Doldu		BakimSureDoldu
113	32		15		No				No			Hayir				Hayir
114	32		15		Yes				Yes			Evet				Evet
115	32		15		Power Split Mode		Split Mode		Guc Paylasimi Modu		Guc Paylasimi
116	32		15		Slave Current Limit		Slave Cur Lmt		Slave Akim limiti		Slave Akim Lim
117	32		15		Slave Delta Voltage		Slave Delta vol		Slave Delta Gerilimi		Slave Delta Ger
118	32		15		Slave Proportional Coefficient	Slave P Coef		Slave Orantili Katsayi		SlaveOrnKatsayi
119	32		15		Slave Integral Time		Slave I Time		Slave Integral Zamani		Slave Int Zmn
120	32		15		Master Mode			Master			Master				Master
121	32		15		Slave Mode			Slave			Slave				Slave
122	32		15		MPCL Control Step		MPCL Ctl Step		MPCL Kontrol Adimi		MPCLKont.Adimi
123	32		15		MPCL Control Threshold		MPCL Ctl Thres		MPCL Kontrol Esigi		MPCLKont.Esigi
124	32		15		MPCL Battery Discharge Enabled	MPCL BatDisch		MPCL Aku desarj Aktif		MPCLAku Desarj
125	32		15		MPCL Diesel Control Enabled	MPCL DieselCtl		MPCL Dizel Jen. Aktif		MPCL DizelJen.
126	32		15		Disabled			Disabled		Pasif				Pasif
127	32		15		Enabled				Enabled			Aktif				Aktif
128	32		15		Disabled			Disabled		Pasif				Pasif
129	32		15		Enabled				Enabled			Aktif				Aktif
130	32		15		System Time			System Time		Sistem Zamani			Sistem Zamani
131	32		15		LCD Language			LCD Language		LCD Dili			LCD Dili
132	32		15		LCD Audible Alarm		Audible Alarm		LCD Sesli Alarm			LCD Sesli Alarm
133	32		15		English				English			Ingilizce			Ingilizce
134	32		15		Turkish				Turkish			Turkce				Turkce
135	32		15		On				On			Evet				Evet
136	32		15		Off				Off			Hayir				Hayir
137	32		15		3 Minutes			3 Min			3 Dakika			3 Dakika	
138	32		15		10 Minutes			10 Min			10 Dakika			10 Dakika	
139	32		15		1 hour				1 h			1 Saat				1 Saat
140	32		15		4 hours				4 h			4 Saat				4 Saat
141	32		15		Reset Maintenance Run Time	Reset Run Time		Bakim Calisma Suresini Sifirla	ClsmaSureSifirla
142	32		15		No				No			Hayir				Hayir
143	32		15		Yes				Yes			Evet				Evet
144	32		15		Alarm				Alarm			Alarm				Alarm
145	32		15		HW Status			HW Status		HW Durumu			HW Durumu
146	32		15		Normal				Normal			Normal				Normal
147	32		15		Configuration Error		Config Error		Konfigurasyon Hatasi		Konfig Hata
148	32		15		Flash Fail			Flash Fail		Flash Hatasi			Flash Hata
150	32		15		LCD Time Zone			Time Zone		LCD Zaman Dilimi		Zaman Dilimi
151	32		15		Time Sync Main Server		Time Sync Svr		Ana Server Zaman Senk.		Server Zmn Senk.	
152	32		15		Time Sync Backup Server		Backup Sync Svr		Yedek Server Zaman Senk.	YdkServerZmnSenk
153	32		15		Time Sync Interval		Sync Interval		Zaman Senk. Araligi		Senk.Araligi
155	32		15		Reset SCU			Reset SCU		Reset SCU			Reset SCU
156	32		15		Running Configuration Type	Config Type		Calisan Konfig.Tipi		Konfig Tipi
157	32		15		Normal Configuration		Normal Config		Konfigurasyon Normal		Konfig Normal
158	32		15		Backup Configuration		Backup Config		Yedek Konfigurasyon		Yedek Konfig
159	32		15		DeFail Configuration		DeFail Config		Varsayilan Konfigurasyon	VarsayilanKonfig
160	32		15		Configuration Error from Backup	Config Error 1		Yedek Konfig Hatasi		Konfig Hata1
161	32		15		Configuration Errorfrom DeFail	Config Error 2		Varsayilan Konfig Hatsi		Konfig Hata2
162	32		15		Control System Alarm LED	Ctrl Sys Alarm		Kontrol Sistemi Alarm LEDi	Kont.Sist.Alarm
163	32		15		Abnormal Load Current		Ab Load Curr		Anormal Yuk Akimi		AnormalYukAkim
164	32		15		Normal				Normal			Normal				Normal
165	32		15		Abnormal			Abnormal		Anormal				Anormal
167	32		15		Main Switch Block Condition	MS Block Cond		Ana Sigorta Engelleme Durumu	AnaSigEngelDur
168	32		15		No				No			Hayir				Hayir
169	32		15		Yes				Yes			Evet				Evet
170	32		15		Total Run Time			Total Run Time		Toplam Calisma Suresi		T.Calisma Sure
171	32		15		Total Number of Alarms		Total No. Alms		Toplam Alarm			Toplam Alarm
172	32		15		Total Number of OA		Total No. OA		Toplam Alarm Minor		Minor Alarm
173	32		15		Total Number of MA		Total No. MA		Toplam Alarm Major		Major Alarm
174	32		15		Total Number of CA		Total No. CA		Toplam Alarm Kritik		Kritik Alarm
175	32		15		SPD Fail			SPD Fail		SPD Arizasi			SPD Arizasi
176	32		15		Overvoltage 2 Level		Overvoltage 2		Yuksek Gerilim 2 Seviyesi	Yuksek Ger2Sev.
177	32		15		Overvoltage 2			Overvoltage 2		Yuksek Gerilim 2		Yuksek Ger2
178	32		15		Regulation Voltage		Regu Voltage		Regulasyon Gerilimi		Reg.Ger.
179	32		15		Regulation Voltage Range	Regu Volt Range		Regulasyon Gerilim Orani	Reg.Ger.Orani
180	32		15		Keypad Sound			Keypad Sound		Tus Takimi Sesi			Tus Takimi Ses	
181	32		15		On				On			Ac				Ac
182	32		15		Off				Off			Kapat				Kapat
183	32		15		Load Shunt Full Current		Load Shunt Curr		Yuk Sont Komple Akim		Yuk Sontu Akim
184	32		15		Load Shunt Full Voltage		Load Shunt Volt		Yuk Sont Komple Gerilim		Yuk Sontu Ger
185	32		15		Battery 1 Shunt Full Current	Bat1 Shunt Curr		Aku Sont1 Akim			Aku Sont1 Akim	
186	32		15		Battery 1 Shunt Full Voltage	Bat1 Shunt Volt		Aku Sont1 Gerilim		Aku Sont1 Ger	
187	32		15		Battery 2 Shunt Full Current	Bat2 Shunt Curr		Aku Sont2 Akim			Aku Sont1 Akim	
188	32		15		Battery 2 Shunt Full Voltage	Bat2 Shunt Volt		Aku Sont2 Gerilim		Aku Sont1 Ger	
219	32		15		DO6				DO6			DO6				DO6
220	32		15		DO7				DO7			DO7				DO7
221	32		15		DO8				DO8			DO8				DO8
222	32		15		RS232 Baudrate			RS232 Baudrate		RS232 Veri Iletisim Hizi		RS232 IletisHiz
223	32		15		Local Address			Local Addr		Yerel Adres			Yerel Adres
224	32		15		Callback Number			Callback Num		Geri Arama Tel			Geri Arama Tel
225	32		15		Interval Time of Callback	Interval		Geri Suresi Arama Arligi	Aralik
#226	32		15		Communication Password		Comm Password		Haberlesme Paralosi		Hab.Parola
227	32		15		DC Data Flag (On-Off)		DC DataFlag 1		DC Veri Bayrak (ac-Kapat)	DC Veri Bayrak1
228	32		15		DC Data Flag (Alarm)		DC DataFlag 2		DC Veri Bayrak (Alarm)		DC Veri Bayrak2
229	32		15		Relay Report Method		Relay Report		Role Rapor Metodu		Role Rapor 
230	32		15		Fixed				Fixed			Sabit				Sabit
231	32		15		User Defined			User Defined		Kullanici Tanimli		Kull. Tanimli
232	32		15		Rectifier Data Flag (Alarm)	RectDataFlag2		Dogrultucu Veri Bayragi(Alarm)	Dog.VeriBayrak2
233	32		15		Master Controlled		Master Ctrlled		Master Denetci			Master Denetci
234	32		15		Slave Controlled		Slave Ctrlled		Slave Denetci			Slave Denetci
236	32		15		Over Load Alarm Level		Over Load Lvl		Asiri Yuk Alarm Leveli		Asiri Yuk Lev1
237	32		15		Overload			Overload		Asiri Yuk 			Asiri Yuk 
238	32		15		System Data Flag (On-Off)	SysData Flag 1		Sistem Veri Bayragi(Ac-Kapat)	SistVeriBayrak1
239	32		15		System Data Flag (Alarm)	SysData Flag 2		Sistem Veri Bayragi(Alarm)	SistVeriBayrak2
240	32		15		Turkish Language		Turkish			Turkce				Turkce
241	32		15		Imbalance Protection		Imb Protect		Dengesizlik Korumasi		Dengesiz Koruma
242	32		15		Enable				Enable			Aktif				Aktif
243	32		15		Disable				Disable			Pasif				Pasif
244	32		15		Buzzer Control			Buzzer Control		Buzzer Kontrol			Buzzer Kontrol
245	32		15		Normal				Normal			Normal				Normal
246	32		15		Disable				Disable			Pasif				Pasif
247	32		15		Ambient Temperature Alarm	Amb Temp Alarm		Ortam Sicaklik Alarmi		Ort.Sic Alarm
248	32		15		Normal				Normal			Normal				Normal
249	32		15		Low				Low			Dusuk				Dusuk
250	32		15		High				High			Yuksek				Yuksek
251	32		15		Reallocate Rectifier Address	Realloc Addr		GercekYerlesim Dog.Adresi	GercekYerAdres
252	32		15		Normal				Normal			Normal				Normal
253	32		15		Realloc Addr			Realloc Addr		Gercek Yerlesim Adres		GercekYerAdres
254	32		15		Test State Set			Test State Set		Test Durumu Ayar		Test DurumAyar
255	32		15		Normal				Normal			Normal				Normal
256	32		15		Test State			Test State		Test Durumu			Test Durumu
257	32		15		Minification for Test		Minification		Testi en aza indirgeme		Indirgeme
258	32		15		Digital Input 1			DI 1			Dijital Giris 1			DI 1
259	32		15		Digital Input 2			DI 2			Dijital Giris 2			DI 2
260	32		15		Digital Input 3			DI 3			Dijital Giris 3			DI 3
261	32		15		Digital Input 4			DI 4			Dijital Giris 4			DI 4
262	32		15		System Type Value		Sys Type Value		Sistem Tipi Deger		Sist.Tipi
263	32		15		AC/DC Board Type		AC/DCBoardType		AC/DC Kart Tipi			AC/DC Kart Tipi
264	32		15		B14C3U1				B14C3U1			B14C3U1				B14C3U1
265	32		15		Large DU			Large DU		Genis DU			Genis DU
266	32		15		SPD				SPD			SPD				SPD
267	32		15		Temperature 1			Temp 1			Sicaklik 1			Sic 1
268	32		15		Temperature 2			Temp 2			Sicaklik 2			Sic 2
269	32		15		Temperature 3			Temp 3			Sicaklik 3			Sic 3
270	32		15		Temperature 4			Temp 4			Sicaklik 4			Sic 4
271	32		15		Temperature 5			Temp 5			Sicaklik 5			Sic 5
272	32		15		Auto Mode			Auto Mode		Oto Mod				Oto Mod
273	32		15		EMEA				EMEA			EMEA				EMEA
274	32		15		Other				Other			Baska				Baska
275	32		15		Float Voltage			Float Volt		Tampon Gerilim			FT Gerilim
276	32		15		Emergency Stop/Shutdown		Emerg Stop		Acil Durdurma/Kapatma		AcilDurd/Kapt
277	32		15		Shunt 1 Full Current		Shunt1 Current		Sont 1 Tam Akim			Sont 1 Akim
278	32		15		Shunt 2 Full Current		Shunt2 Current		Sont 2 Tam Akim			Sont 2 Akim
279	32		15		Shunt 3 Full Current		Shunt3 Current		Sont 3 Tam Akim			Sont 3 Akim
280	32		15		Shunt 1 Full Voltage		Shunt1 Voltage		Sont 1 Tam Gerilim		Sont 1 Gerilim
281	32		15		Shunt 2 Full Voltage		Shunt2 Voltage		Sont 2 Tam Gerilim		Sont 2 Gerilim
282	32		15		Shunt 3 Full Voltage		Shunt3 Voltage		Sont 3 Tam Gerilim		Sont 3 Gerilim
283	32		15		Temperature 1			Temp 1			Sicaklik 1			Sic 1
284	32		15		Temperature 2			Temp 2			Sicaklik 2			Sic 2
285	32		15		Temperature 3			Temp 3			Sicaklik 3			Sic 3
286	32		15		Temperature 4			Temp 4			Sicaklik 4			Sic 4
287	32		15		Temperature 5			Temp 5			Sicaklik 5			Sic 5
288	32		15		Very High Temperature 1 Limit	VeryHighTemp1		Cok Yuksek Sicaklik 1 Limit	CokYuksekSic1
289	32		15		Very High Temperature 2 Limit	VeryHighTemp2		Cok Yuksek Sicaklik 2 Limit	CokYuksekSic2
290	32		15		Very High Temperature 3 Limit	VeryHighTemp3		Cok Yuksek Sicaklik 3 Limit	CokYuksekSic3
291	32		15		Very High Temperature 4 Limit	VeryHighTemp4		Cok Yuksek Sicaklik 4 Limit	CokYuksekSic4
292	32		15		Very High Temperature 5 Limit	VeryHighTemp5		Cok Yuksek Sicaklik 5 Limit	CokYuksekSic5
293	32		15		High Temperature 2 Limit	High Temp2		Yuksek Sicaklik 2 Limit		Yuksek Sic2
294	32		15		High Temperature 3 Limit	High Temp3		Yuksek Sicaklik 3 Limit		Yuksek Sic3
295	32		15		High Temperature 4 Limit	High Temp4		Yuksek Sicaklik 4 Limit		Yuksek Sic4
296	32		15		High Temperature 5 Limit	High Temp5		Yuksek Sicaklik 5 Limit		Yuksek Sic5
297	32		15		Low Temperature 2 Limit		Low Temp2		Dusuk Sicaklik 2 Limit		Dusuk Sic2
298	32		15		Low Temperature 3 Limit		Low Temp3		Dusuk Sicaklik 3 Limit		Dusuk Sic3
299	32		15		Low Temperature 4 Limit		Low Temp4		Dusuk Sicaklik 4 Limit		Dusuk Sic4
300	32		15		Low Temperature 5 Limit		Low Temp5		Dusuk Sicaklik 5 Limit		Dusuk Sic5
301	32		15		Temperature 1 not Used		Temp1 not Used		Sicaklik 1 Kullanilmiyor	Sic1 Kull.miyor
302	32		15		Temperature 2 not Used		Temp2 not Used		Sicaklik 2 Kullanilmiyor	Sic2 Kull.miyor
303	32		15		Temperature 3 not Used		Temp3 not Used		Sicaklik 3 Kullanilmiyor	Sic3 Kull.miyor
304	32		15		Temperature 4 not Used		Temp4 not Used		Sicaklik 4 Kullanilmiyor	Sic4 Kull.miyor
305	32		15		Temperature 5 not Used		Temp5 not Used		Sicaklik 5 Kullanilmiyor	Sic5 Kull.miyor
306	32		15		High Temperature 2		High Temp 2		Yuksek Sicaklik 2		Yuksek Sic 2
307	32		15		Low Temperature 2		Low Temp 2		Dusuk Sicaklik 2		Dusuk Sic 2
308	32		15		Temperature 2 Sensor Fail	T2 Sensor Fail		Sicaklik Senson 2 Hata		T2 Hata
309	32		15		High Temperature 3		High Temp 3		Yuksek Sicaklik 3		Yuksek Sic 3
310	32		15		Low Temperature 3		Low Temp 3		Dusuk Sicaklik 3		Dusuk Sic 3
311	32		15		Temperature 3 Sensor Fail	T3 Sensor Fail		Sicaklik Senson 3 Hata		T3 Hata
312	32		15		High Temperature 4		High Temp 4		Yuksek Sicaklik 4		Yuksek Sic 4
313	32		15		Low Temperature 4		Low Temp 4		Dusuk Sicaklik 4		Dusuk Sic 4
314	32		15		Temperature 4 Sensor Fail	T4 Sensor Fail		Sicaklik Senson 4 Hata		T4 Hata
315	32		15		High Temperature 5		High Temp 5		Yuksek Sicaklik 5		Yuksek Sic 5
316	32		15		Low Temperature 5		Low Temp 5		Dusuk Sicaklik 5		Dusuk Sic 5
317	32		15		Temperature 5 Sensor Fail	T5 Sensor Fail		Sicaklik Senson 5 Hata		T5 Hata
318	32		15		Very High Temperature 1		Very High Temp1		Cok Yuksek Sicaklik 1		CokYuksekSic1
319	32		15		Very High Temperature 2		Very High Temp2		Cok Yuksek Sicaklik 2		CokYuksekSic2
320	32		15		Very High Temperature 3		Very High Temp3		Cok Yuksek Sicaklik 3		CokYuksekSic3
321	32		15		Very High Temperature 4		Very High Temp4		Cok Yuksek Sicaklik 4		CokYuksekSic4
322	32		15		Very High Temperature 5		Very High Temp5		Cok Yuksek Sicaklik 5		CokYuksekSic5
323	32		15		Energy Saving Status		Energy Saving Status	Enerji Tasarrufu Durmu		En.Tas.Durumu
324	32		15		Energy Saving			Energy Saving		Enerji Tasarrufu		En.Tasarrufu
325	32		15		Internal Use(I2C)		Internal Use		Dahili Kullanim (I2C)		Dahili Kull.
326	32		15		Disable				Disable			Hayir				Hayir
327	32		15		Emergency Stop			EStop			Acil Durdurma			Acil Durdurma
328	32		15		Emeregency Shutdown		EShutdown		Acil Kapatma			Acil Kapatma
329	32		15		Ambient				Ambient			Ortam				Ortam
330	32		15		Battery				Battery			Aku				Aku
331	32		15		Temperature 6			Temp 6			Sicaklik 6			Sic 6
332	32		15		Temperature 7			Temp 7			Sicaklik 7			Sic 7
333	32		15		Temperature 6			Temp 6			Sicaklik 6			Sic 6
334	32		15		Temperature 7			Temp 7			Sicaklik 7			Sic 7
335	32		15		Very High Temperature 6 Limit	VeryHighTemp6		Cok Yuksek Sicaklik 6 Limit	CokYuksekSic6
336	32		15		Very High Temperature 7 Limit	VeryHighTemp7		Cok Yuksek Sicaklik 7 Limit	CokYuksekSic7
337	32		15		High Temperature 6 Limit	High Temp 6		Yuksek Sicaklik 6 Limit		YuksekSic6
338	32		15		High Temperature 7 Limit	High Temp 7		Yuksek Sicaklik 6 Limit		YuksekSic6
339	32		15		Low Temperature 6 Limit		Low Temp 6		Dusuk Sicaklik 6 Limit		Dusuk Sic6
340	32		15		Low Temperature 7 Limit		Low Temp 7		Dusuk Sicaklik 7 Limit		Dusuk Sic7
341	32		15		EIB Temperature 1 not Used	EIB T1 not Used		EIB Sic1 Kullanilmiyor		EIB T1Kullmiyor
342	32		15		EIB Temperature 2 not Used	EIB T2 not Used		EIB Sic2 Kullanilmiyor		EIB T2Kullmiyor
343	32		15		High Temperature 6		High Temp 6		Yuksek Sicaklik 6		Yuksek Sic 6
344	32		15		Low Temperature 6		Low Temp 6		Dusuk Sicaklik 6		Dusuk Sic 6
345	32		15		Temperature 6 Sensor Fail	T6 Sensor Fail		Sicaklik Senson 6 Hata		T6 Hata
346	32		15		High Temperature 7		High Temp 7		Yuksek Sicaklik 7		Yuksek Sic 7
347	32		15		Low Temperature 7		Low Temp 7		Dusuk Sicaklik 7		Dusuk Sic 7
348	32		15		Temperature7 Sensor Fail	T7 Sensor Fail		Sicaklik Senson 7 Hata		T7 Hata
349	32		15		Very High Temperature 6		Very High Temp6		Cok Yuksek Sicaklik 6		CokYuksekSic6
350	32		15		Very High Temperature 7		Very High Temp7		Cok Yuksek Sicaklik 7		CokYuksekSic7

501	32		15		Manual Mode			Manual Mode		Manual Mod			Manual Mod

1111	32		15		System Temperature 1		System Temp 1		Sistem Sicaklik 1		Sist.Sic 1
1131	32		15		System Temperature 1		System Temp 1		Sistem Sicaklik 1		Sist.Sic 1
1132	32		15		Very High Battery Temp 1 Limit	VHighBattTemp1		Cok Yuksek Aku Sic1 Limit	CokYukskAkuSic1
1133	32		15		High Battery Temp 1 Limit	Hi Batt Temp 1		Yuksek Aku Sic1 Limit		Yuksek Aku Sic1
1134	32		15		Low Battery Temp 1 Limit	Lo Batt Temp 1		Dusuk Aku Sic1 Limit		Dusuk Aku Sic1
1141	32		15		System Temperature 1 not Used	Sys T1 not Used		Sistem Sic1 Kullanilmiyor	SistT1Kullmiyor
1142	34		15		System Temperature 1 Sensor Fail	Sys T1 Fail	Sistem Sic Sensor1 Hata		Sistem T1 Hata
1143	32		15		Very High Battery Temperature 1	VHighBattTemp1		Cok Yuksek Aku Sic1		CokYukskAkuSic1
1144	32		15		High Battery Temperature 1	Hi Batt Temp 1 		Yuksek Aku Sic1 		Yuksek Aku Sic1
1145	32		15		Low Battery Temperature 1	Lo Batt Temp 1		Dusuk Aku Sic1 			Dusuk Aku Sic1
#
1211	32		15		System Temperature 2		System Temp 2		Sistem Sicaklik 2		Sist.Sic 2
1231	32		15		System Temperature 2		System Temp 2		Sistem Sicaklik 2		Sist.Sic 2
1232	32		15		Very High Battery Temp 2 Limit	VHighBattTemp2		Cok Yuksek Aku Sic2 Limit	CokYukskAkuSic2
1233	32		15		High Battery Temp 2 Limit	Hi Batt Temp 2		Yuksek Aku Sic2 Limit		Yuksek Aku Sic2
1234	32		15		Low Battery Temp 2 Limit	Lo Batt Temp 2		Dusuk Aku Sic2 Limit		Dusuk Aku Sic2
1241	32		15		System Temperature 2 not Used	Sys T2 not Used		Sistem Sic2 Kullanilmiyor	SistT2Kullmiyor
1242	34		15		System Temperature 2 Sensor Fail	Sys T2 Fail	FSistem Sic Sensor2 Hata	Sistem T2 Hata
1243	32		15		Very High Battery Temperature 2	VHighBattTemp2		Cok Yuksek Aku Sic2		CokYukskAkuSic2
1244	32		15		High Battery Temperature 2	Hi Batt Temp 2		Yuksek Aku Sic2 		Yuksek Aku Sic2
1245	32		15		Low Battery Temperature 2	Lo Batt Temp 2		Dusuk Aku Sic2 			Dusuk Aku Sic2
#
1311	32		15		System Temperature 3		System Temp 3		Sistem Sicaklik 3		Sist.Sic 3
1331	32		15		System Temperature 3		System Temp 3		Sistem Sicaklik 3 		Sist.Sic 3
1332	32		15		Very High Battery Temp 3 Limit	VHighBattTemp3		Cok Yuksek Aku Sic3 Limit	CokYukskAkuSic3
1333	32		15		High Battery Temp 3 Limit	Hi Batt Temp 3		Yuksek Aku Sic3 Limit		Yuksek Aku Sic3
1334	32		15		Low Battery Temp 3 Limit	Lo Batt Temp 3		Dusuk Aku Sic3 Limit		Dusuk Aku Sic3
1341	32		15		System Temperature 3 not Used	Sys T3 not Used		Sistem Sic3 Kullanilmiyor	SistT3Kullmiyor
1342	34		15		System Temperature 3 Sensor Fail	Sys T3 Fail	Sistem Sic Sensor3 Hata		Sistem T3 Hata
1343	32		15		Very High Battery Temperature 3	VHighBattTemp3		Cok Yuksek Aku Sic3		CokYukskAkuSic3
1344	32		15		High Battery Temperature 3	Hi Batt Temp 3		Yuksek Aku Sic3 		Yuksek Aku Sic3
1345	32		15		Low Battery Temperature 3	Lo Batt Temp 3		Dusuk Aku Sic3 			Dusuk Aku Sic3
#
1411	32		15		IB2 Temperature 1		IB2 Temp 1		IB2 Sicaklik 1			IB2 Sic 1
1431	32		15		IB2 Temperature 1		IB2 Temp 1		IB2 Sicaklik 1			IB2 Sic 1
1432	32		15		Very High Battery Temp 4 Limit	VHighBattTemp4		Cok Yuksek Aku Sic4 Limit	CokYukskAkuSic4
1433	32		15		High Battery Temp 4 Limit	Hi Batt Temp 4		Yuksek Aku Sic4 Limit		Yuksek Aku Sic4
1434	32		15		Low Battery Temp 4 Limit	Lo Batt Temp 4		Dusuk Aku Sic4 Limit		Dusuk Aku Sic4
1441	32		15		IB2 Temperature 1 not Used	IB2 T1 not Used		IB2 Sic1 Kullanilmiyor		IB2 T1Kullmiyor
1442	32		15		IB2 Temperature 1 Sensor Fail	IB2 T1 Sens Flt		IB2 Sic1 Hata	 		IB2 T1 Hata
1443	32		15		Very High Battery Temperature 4	VHighBattTemp4		Cok Yuksek Aku Sic4		CokYukskAkuSic4
1444	32		15		High Battery Temperature 4	Hi Batt Temp 4		Yuksek Aku Sic4 		Yuksek Aku Sic4
1445	32		15		Low Battery Temperature 4	Lo Batt Temp 4		Dusuk Aku Sic4 			Dusuk Aku Sic4
#
1511	32		15		IB2 Temperature 2		IB2 Temp 2		IB2 Sicaklik 2			IB2 Sic 2
1531	32		15		IB2 Temperature 2		IB2 Temp 2		IB2 Sicaklik 2			IB2 Sic 2
1532	32		15		Very High Battery Temp 5 Limit	VHighBattTemp5		Cok Yuksek Aku Sic5 Limit	CokYukskAkuSic5
1533	32		15		High Battery Temp 5 Limit	Hi Batt Temp 5		Yuksek Aku Sic5 Limit		Yuksek Aku Sic5
1534	32		15		Low Battery Temp 5 Limit	Lo Batt Temp 5		Dusuk Aku Sic5 Limit		Dusuk Aku Sic5
1541	32		15		IB2 Temperature 2 not Used	IB2 T2 not Used		IB2 Sic2 Kullanilmiyor	 	IB2 T2Kullmiyor
1542	32		15		IB2 Temperature 2 Sensor Fail	IB2 T2 Sens Flt		IB2 Sic2 Hata	 		IB2 T2 Hata
1543	32		15		Very High Battery Temperature 5	VHighBattTemp5		Cok Yuksek Aku Sic5		CokYukskAkuSic5
1544	32		15		High Battery Temperature 5	Hi Batt Temp 5		Yuksek Aku Sic5 		Yuksek Aku Sic5
1545	32		15		Low Battery Temperature 5	Lo Batt Temp 5		Dusuk Aku Sic5 			Dusuk Aku Sic5
#
1611	32		15		EIB Temperature 1		EIB Temp 1		EIB Sicaklik 1			EIB Sic 1
1631	32		15		EIB Temperature 1		EIB Temp 1		EIB Sicaklik 1			EIB Sic 1
1632	32		15		Very High Battery Temp 6 Limit	VHighBattTemp6		Cok Yuksek Aku Sic6 Limit	CokYukskAkuSic6
1633	32		15		High Battery Temp 6 Limit	Hi Batt Temp 6		Yuksek Aku Sic6 Limit		Yuksek Aku Sic6
1634	32		15		Low Battery Temp 6 Limit	Lo Batt Temp 6		Dusuk Aku Sic6 Limit		Dusuk Aku Sic6
1641	32		15		EIB Temperature 1 not Used	EIB T1 not Used		EIB Sic1 Kullanilmiyor	 	EIB T1Kullmiyor
1642	32		15		EIB Temperature 1 Sensor Fail	EIB T1 Sens Flt		EIB Sic1 Hata		 	EIB T1 hata
1643	32		15		Very High Battery Temperature 6	VHighBattTemp6		Cok Yuksek Aku Sic6		CokYukskAkuSic6
1644	32		15		High Battery Temperature 6	Hi Batt Temp 6		Yuksek Aku Sic6 		Yuksek Aku Sic6
1645	32		15		Low Battery Temperature 6	Lo Batt Temp 6		Dusuk Aku Sic6 			Dusuk Aku Sic6
#
1711	32		15		EIB Temperature 2		EIB Temp 2		EIB Sicaklik 2			EIB Sic 2
1731	32		15		EIB Temperature 2		EIB Temp 2		EIB Sicaklik 2			EIB Sic 2
1732	32		15		Very High Battery Temp 7 Limit	VHighBattTemp7		Cok Yuksek Aku Sic7 Limit	CokYukskAkuSic7
1733	32		15		High Battery Temp 7 Limit	Hi Batt Temp 7		Yuksek Aku Sic7 Limit		Yuksek Aku Sic7
1734	32		15		Low Battery Temp 7 Limit	Lo Batt Temp 7		Dusuk Aku Sic7 Limit		Dusuk Aku Sic7
1741	32		15		EIB Temperature 2 not Used	EIB T2 not Used		EIB Sic2 Kullanilmiyor	 	EIB T2Kullmiyor
1742	32		15		EIB Temperature 2 Sensor Fail	EIB T2 Sens Flt		EIB Sic2 Hata		 	EIB T2 hata
1743	32		15		Very High Battery Temperature 7	VHighBattTemp7		Cok Yuksek Aku Sic7		CokYukskAkuSic7
1744	32		15		High Battery Temperature 7	Hi Batt Temp 7		Yuksek Aku Sic7 		Yuksek Aku Sic7
1745	32		15		Low Battery Temperature 7	Lo Batt Temp 7		Dusuk Aku Sic7 			Dusuk Aku Sic7
1746	32		15		DHCP Fail			DHCP Fail		DHCP Hata			DHCP Hata
1747	32		15		Internal Use(CAN)		Internal Use		Dahili Kullanim(CAN)		Dahili Kull
1748	32		15		Internal Use(485)		Internal Use		Dahili Kullanim(485)		Dahili Kull
1749	32		15		Address				Address			Adres				Adres
1750	32		15		201				201			201				201
1751	32		15		202				202			202				202
1752	32		15		203				203			203				203
1753	32		15		Master/Slave Mode		Master Mode		Master/Slave Modu		Master Modu
1754	32		15		Master				Master			Master				Master	
1755	32		15		Slave				Slave			Slave				Slave
1756	32		15		Alone				Alone			Yalniz				Yalniz
1757	32		15		SMBatTemp High Limit		SMBatTemHighLim		SM Aku Sic.Yuksek Limit		SMAkuSicYkskLim
1758	32		15		SMBatTemp Low Limit		SMBatTempLowLim		SM Aku Sic.Dusuk Limit		SMAkuSicDuskLim
1759	32		15		PLC Config Error		PLC Conf Error		PLC Konfig Hata			PLC Konf Hata	
1760	32		15		System Expansion		Sys Expansion		Sistem Arttirim			Sist Arttirma
1761	32		15		Inactive			Inactive		Hayir				Hayir
1762	32		15		Primary				Primary			Birincil			Birincil
1763	32		15		Secondary			Secondary		ikincil 			ikincil
1764	128		64		Mode Changed, Auto Config will be started!	Auto Config will be started!		Mod degisti,Oto konfig baslayacak.		Mod digisti,Oto konfig baslayacak.
1765	32		15		Former Lang			Former Lang		Onceki dil			Onceki dil
1766	32		15		Current Lang			Current Lang		Simdiki dil			Simdiki dil
1767	32		15		English				English			Ingilizce			Ingilizce
1768	32		15		Turkish				Turkish			Turkce			Turkce
1769	32		15		RS485 Communication Fail	485 Comm Fail		RS-485 Haberlesme Hatasi	485 Hab.Hatasi
1770	64		32		SMDU Sampler Mode		SMDU Sampler		SMDU ?rnekleme Modu		SMDU ornekleme
1771	32		15		CAN				CAN			CAN				CAN
1772	32		15		RS485				RS485			RS485				RS485
1773	32		15		To Auto Delay			To Auto Delay		Oto Gecikme			Oto Gecikme
1774	32		15		OBSERVATION SUMMARY		OA SUMMARY		Minor Ozet			Minor Ozet
1775	32		15		MAJOR SUMMARY			MA SUMMARY		Major Ozet			Major Ozet
1776	32		15		CRITICAL SUMMARY		CA SUMMARY		Kritik Ozet			Kritik Ozet
1777	32		15		All Rectifiers Current		All Rects Curr		Butun Dogrultucu Akimlari	Tum Dog Akim
1778	32		15		Rectifier Group Lost Status	RectGroup Lost		Dogrultucu Grubu Kayip Durumu	DogGrp Kayip
1779	32		15		Reset Rectifier Lost Alarm	ResetRectLost		Dog. Kayip Alarmi Sifirla 	DogKayipSifirla
1780	32		15		Last GroupRectifiers Num	GroupRect Number	Son Grup Dog No			DogGrup No
1781	32		15		Rectifier Group Lost		Rect Group Lost		Rectifier Grubu Kayip		Dog.Grp.Kayip
1782	32		15		High Ambient Temp 1 Limit	Hi Amb Temp 1		Yuksek Ortam Sicakligi 1 Limit	YukskOrtSic1Lim
1783	32		15		Low Ambient Temp 1 Limit	Lo Amb Temp 1		Dusuk Ortam Sicakligi 1 Limit	DusukOrtSic1Lim
1784	32		15		High Ambient Temp 2 Limit	Hi Amb Temp 2		Yuksek Ortam Sicakligi 2 Limit	YukskOrtSic2Lim
1785	32		15		Low Ambient Temp 2 Limit	Lo Amb Temp 2		Dusuk Ortam Sicakligi 2 Limit	DusukOrtSic2Lim
1786	32		15		High Ambient Temp 3 Limit	Hi Amb Temp 3		Yuksek Ortam Sicakligi 3 Limit	YukskOrtSic3Lim
1787	32		15		Low Ambient Temp 3 Limit	Lo Amb Temp 3		Dusuk Ortam Sicakligi 3 Limit	DusukOrtSic3Lim
1788	32		15		High Ambient Temp 4 Limit	Hi Amb Temp 4		Yuksek Ortam Sicakligi 4 Limit	YukskOrtSic4Lim
1789	32		15		Low Ambient Temp 4 Limit	Lo Amb Temp 4		Dusuk Ortam Sicakligi 4 Limit	DusukOrtSic4Lim
1790	32		15		High Ambient Temp 5 Limit	Hi Amb Temp 5		Yuksek Ortam Sicakligi 5 Limit	YukskOrtSic5Lim
1791	32		15		Low Ambient Temp 5 Limit	Lo Amb Temp 5		Dusuk Ortam Sicakligi 5 Limit	DusukOrtSic5Lim
1792	32		15		High Ambient Temp 6 Limit	Hi Amb Temp 6		Yuksek Ortam Sicakligi 6 Limit	YukskOrtSic6Lim
1793	32		15		Low Ambient Temp 6 Limit	Lo Amb Temp 6		Dusuk Ortam Sicakligi 6 Limit	DusukOrtSic6Lim
1794	32		15		High Ambient Temp 7 Limit	Hi Amb Temp 7		Yuksek Ortam Sicakligi 7 Limit	YukskOrtSic7Lim
1795	32		15		Low Ambient Temp 7 Limit	Lo Amb Temp 7		Dusuk Ortam Sicakligi 7 Limit	DusukOrtSic7Lim
1796	32		15		High Ambient Temperature 1	Hi Amb Temp 1		Yuksek Ortam Sicakligi 1	Yukesk Ort Sic1
1797	32		15		Low Ambient Temperature 1	Lo Amb Temp 1		Dusuk Ortam Sicakligi 1		Dusuk Ort Sic1
1798	32		15		High Ambient Temperature 2	Hi Amb Temp 1		Yuksek Ortam Sicakligi 2	Yukesk Ort Sic2
1799	32		15		Low Ambient Temperature 2	Lo Amb Temp 2		Dusuk Ortam Sicakligi 2		Dusuk Ort Sic2
1800	32		15		High Ambient Temperature 3	Hi Amb Temp 3		Yuksek Ortam Sicakligi 3	Yukesk Ort Sic3
1801	32		15		Low Ambient Temperature 3	Lo Amb Temp 3		Dusuk Ortam Sicakligi 3		Dusuk Ort Sic3
1802	32		15		High Ambient Temperature 4	Hi Amb Temp 4		Yuksek Ortam Sicakligi 4	Yukesk Ort Sic4
1803	32		15		Low Ambient Temperature 4	Lo Amb Temp 4		Dusuk Ortam Sicakligi 4		Dusuk Ort Sic4
1804	32		15		High Ambient Temperature 5	Hi Amb Temp 5		Yuksek Ortam Sicakligi 5	Yukesk Ort Sic5
1805	32		15		Low Ambient Temperature 5	Lo Amb Temp 5		Dusuk Ortam Sicakligi 5		Dusuk Ort Sic5
1806	32		15		High Ambient Temperature 6	Hi Amb Temp 6		Yuksek Ortam Sicakligi 6	Yukesk Ort Sic6
1807	32		15		Low Ambient Temperature 6	Lo Amb Temp 6		Dusuk Ortam Sicakligi 6		Dusuk Ort Sic6
1808	32		15		High Ambient Temperature 7	Hi Amb Temp 7		Yuksek Ortam Sicakligi 7	Yukesk Ort Sic7
1809	32		15		Low Ambient Temperature 7	Lo Amb Temp 7		Dusuk Ortam Sicakligi 7		Dusuk Ort Sic7
1810	32		15		Fast Sampler Flag		Fast SamplFlag		Fast Sampler Flag		Fast Sampler Flag
1811	32		15		LVD Quantity			LVD Quantity		LVD Sayisi			LVD Sayisi
1812	32		15		LCD Rotation			LCD Rotation		LCD Rotasyon			LCD Rotasyon
1813	32		15		0 deg				0 deg			0 Derece			0 Derece
1814	32		15		90 deg				90 deg			90 Derece			90 Derece
1815	32		15		180 deg				180 deg			180 Derece			180 Derece
1816	32		15		270 deg				270 deg			270 Derece			270 Derece
1817	32		15		Overvoltage 1			Overvolt 1		Yuksek Gerilim 1		Yuksek Ger1
1818	32		15		Overvoltage 2			Overvolt 2		Yuksek Gerilim2			Yuksek Ger2 
1819	32		15		Undervoltage 2			Undervolt 2		Dusuk Gerilim 2			Dusuk Ger2
1820	32		15		Undervoltage 1			Undervolt 1		Dusuk Gerilim 1			Dusuk Ger1
1821	32		15		Overvoltage 1			Overvolt 1		Yuksek Gerilim 1		Yuksek Ger1
1822	32		15		Overvoltage 2			Overvolt 2		Yuksek Gerilim 2		Yuksek Ger2 
1823	32		15		Undervoltage 2			Undervolt 2		Dusuk Gerilim 2			Dusuk Ger2
1824	32		15		Undervoltage 1			Undervolt 1		Dusuk Gerilim 1			Dusuk Ger1
1825	32		15		Failsafe Mode			Failsafe Mode		Hata Koruma Modu		HataKoruma Modu
1826	32   		15		Disable				Disable			Pasif				Pasif
1827	32		15		Enable				Enable			Aktif				Aktif
1828	32		15		Hybrid Mode			Hybrid Mode		Hibrit Modu			Hibrit Modu
1829	32   		15		Disable				Disable			Pasif				Pasif
1830	32		15		Capacity			Capacity		Kapasite			Kapasite
1831	32   		15		Cyclic				Cyclic			Dongusel			Dongusel
1832	32		15		DG Run at High Temperature	DG Run Hi Temp		DG Yuksek sicaklikCalismasi	DG YukseSic Cal
1833	32		15		Used DG for Hybrid		Used DG			Hibrit icin Kullanilan DG	Kullanilan DG
1834	32		15		DG1				DG1			DG1				DG1
1835	32		15		DG2				DG2			DG2				DG2
1836	32		15		Both				Both			Her Ykisi de			Her Ykisi de
1837	32		15		DI for Grid			DI for Grid		DI Grid icin			DI Grid icin
1838	32		15		DI 1				DI 1			DI 1				DI 1
1839	32		15		DI 2				DI 2			DI 2				DI 2
1840	32		15		DI 3				DI 3			DI 3				DI 3
1841	32		15		DI 4				DI 4			DI 4				DI 4
1842	32		15		DI 5				DI 5			DI 5				DI 5
1843	32		15		DI 6				DI 6			DI 6				DI 6
1844	32		15		DI 7				DI 7			DI 7				DI 7
1845	32		15		DI 8				DI 8			DI 8				DI 8
1846	32		15		Depth of Discharge		DepthDisch		Derin Desarj			DerinDesarj
1847	32		15		Discharge Duration		Disch Duration		Desarj Suresi			Desarj Suresi
1848	32		15		Discharge Start time		Disch Start		Desarj Baslama Zamani		Desarj Baslama
1849	32		15		Diesel Run Over Temperature	DG Run OverTemp		YuksekSicaklikta DG Calisma	DG YukSic Cal
1850	32		15		DG1 is Running			DG1 is Running		DG1 Calisiyor			DG1 Calisiyor
1851	32		15		DG2 is Running			DG2 is Running		DG2 Calisiyor			DG2 Calisiyor
1852	32		15		Hybrid High Load Level		High Load Lvl		Hibrid Yuksek Yuk Seviyesi	HibridYukSev
1853	32		15		Hybrid is High Load		High Load		Hibrid Yuksek Yuk		HibridYuksekYuk
1854	32		15		DG Run Time at High Temperature	DG Run HiTemp		YuksekSicaklikta DG Calisma Sure	DG YukSic Sure
1855	32		15		Equalising Start Time		EqualStartTime		EQ Baslama Zamani	EQ Baslama Zam.
1856	32		15		DG1 Fail			DG1 Fail		DG1 Hata			DG1 Hata	
1857	32		15		DG2 Fail			DG1 Fail		DG2 Hata			DG2 Hata	
1858	32		15		Diesel Alarm Delay		DG Alarm Delay		Dizel Alarm Gecikmesi		DG Alarm Gecik.
1859	32		15		Grid is on			Grid is on			Sebeke Acik			Sebeke Acik
1860	32		15		PowerSplit Contactor Mode	Contactor Mode		GucPaylasimi Kontaktor Modu		GucPay. Mod
1861	32		15		Ambient Temperature		Amb Temp		Ortam Sicakligi			Ortam Sic
1862	32		15		Ambient Temperature Sensor	Amb Temp Sensor		Ortam Sic. Sensoru		Ortam Sic.Sens
1863	32		15		None				None			Yok				Yok
1864	32		15		Temperature 1			Temp 1			Sicaklik 1			Sic 1
1865	32		15		Temperature 2			Temp 2			Sicaklik 2			Sic 2
1866	32		15		Temp 3 (OB)			Temp3 (OB)		Sic T3 (OB)			Sic 3 (OB)
1867	32		15		Temp 4 (IB)			Temp4 (IB)		Sic T4 (IB)			Sic 4 (IB)
1868	32		15		Temp 5 (IB)			Temp5 (IB)		Sic T5 (IB)			Sic 5 (IB)
1869	32		15		Temp 6 (EIB)			Temp6 (EIB)		Sic T6 (EIB)			Sic 6 (EIB)
1870	32		15		Temp 7 (EIB)			Temp7 (EIB)		Sic T7 (EIB)			Sic 7 (EIB)
1871	32		15		Temperature 8			Temp 8			Sicaklik 8			Sic 8
1872	32		15		Temperature 9			Temp 9			Sicaklik 9			Sic 9
1873	32		15		Temperature 10			Temp 10			Sicaklik 10			Sic 10
1874	32		15		High Ambient Temperature Level	High Amb Temp		Yuksek Ortam Sic.Seviyesi	Ort Sic Yuksek
1875	32		15		Low Ambient Temperature Level	Low Amb Temp		Dusuk Ortam Sic.Seviyesi	Ort Sic Dusuk
1876	32		15		High Ambient Temperature	High Amb Temp		yuksek Ortam Sicakligi		Ort Sic Yuksek
1877	32		15		Low Ambient Temperature		Low Amb Temp		Dusuk Ortam Sicakligi		Ort Sic Dusuk
1878	32		15		Ambient Temp Sensor Fail	Amb Sensor Err		Ortam Sic Sensor Hatasi		Ort.SicSen.Hata
1879	32		15		Digital Input 1			DI 1			DI 1				DI 1
1880	32		15		Digital Input 2			DI 2			DI 2				DI 2
1881	32		15		Digital Input 3			DI 3			DI 3				DI 3
1882	32		15		Digital Input 4			DI 4			DI 4				DI 4
1883	32		15		Digital Input 5			DI 5			DI 5				DI 5
1884	32		15		Digital Input 6			DI 6			DI 6				DI 6
1885	32		15		Digital Input 7			DI 7			DI 7				DI 7
1886	32		15		Digital Input 8			DI 8			DI 8				DI 8
1887	32		15		Open				Open			Acik				Acik
1888	32		15		Closed				Closed			Kapali				Kapali
1889	32		15		Relay Output 1			Relay Output 1		Role Cikis 1			Role Cikis 1
1890	32		15		Relay Output 2			Relay Output 2		Role Cikis 2			Role Cikis 2
1891	32		15		Relay Output 3			Relay Output 3		Role Cikis 3			Role Cikis 3
1892	32		15		Relay Output 4			Relay Output 4		Role Cikis 4			Role Cikis 4
1893	32		15		Relay Output 5			Relay Output 5		Role Cikis 5			Role Cikis 5
1894	32		15		Relay Output 6			Relay Output 6		Role Cikis 6			Role Cikis 6
1895	32		15		Relay Output 7			Relay Output 7		Role Cikis 7			Role Cikis 7
1896	32		15		Relay Output 8			Relay Output 8		Role Cikis 8			Role Cikis 8
1897	32		15		DI1 Alarm State			DI1 Alm State		Alarm DI 1 Durumu		Alm DI 1 Durumu
1898	32		15		DI2 Alarm State			DI2 Alm State   	Alarm DI 2 Durumu		Alm DI 2 Durumu
1899	32		15		DI3 Alarm State			DI3 Alm State		Alarm DI 3 Durumu		Alm DI 3 Durumu
1900	32		15		DI4 Alarm State			DI4 Alm State		Alarm DI 4 Durumu		Alm DI 4 Durumu
1901	32		15		DI5 Alarm State			DI5 Alm State		Alarm DI 5 Durumu		Alm DI 5 Durumu
1902	32		15		DI6 Alarm State			DI6 Alm State		Alarm DI 6 Durumu		Alm DI 6 Durumu
1903	32		15		DI7 Alarm State			DI7 Alm State   	Alarm DI 7 Durumu		Alm DI 7 Durumu
1904	32		15		DI8 Alarm State			DI8 Alm State		Alarm DI 8 Durumu		Alm DI 8 Durumu
1905	32		15		DI1 Alarm			DI1 Alarm		Alarm DI 1			Alarm DI 1
1906	32		15		DI2 Alarm			DI2 Alarm		Alarm DI 2			Alarm DI 2
1907	32		15		DI3 Alarm			DI3 Alarm		Alarm DI 3			Alarm DI 3
1908	32		15		DI4 Alarm			DI4 Alarm		Alarm DI 4			Alarm DI 4
1909	32		15		DI5 Alarm			DI5 Alarm		Alarm DI 5			Alarm DI 5
1910	32		15		DI6 Alarm			DI6 Alarm		Alarm DI 6			Alarm DI 6
1911	32		15		DI7 Alarm			DI7 Alarm		Alarm DI 7			Alarm DI 7
1912	32		15		DI8 Alarm			DI8 Alarm		Alarm DI 8			Alarm DI 8
1913	32		15		On				On			Ac				Ac
1914	32		15		Off				Off			Kapat				Kapat
1915	32		15		High				High			Yuksek				Yuksek
1916	32		15		Low				Low			Dusuk				Dusuk
1917	32		15		IB Number			IB Number		IB No				IB No
1918	32		15		IB Type				IB Type			IB Tipi				IB Tipi
1919	32		15		CSU Undervoltage 1		CSU UV1			CSU Dusuk Gerilim 1		CSU DusukGer1
1920	32		15		CSU Undervoltage 2		CSU UV2			CSU Dusuk Gerilim 2		CSU DusukGer2
1921	32		15		CSU Overvoltage			CSU OV			CSU Yuksek Gerilim		CSU YuksekGer
1922	32		15		CSU Communication Fail		CSU Comm Fail		CSU Haberlesme Hatasi		CSU Hab. Hatasi
1923	32		15		CSU External Alarm 1		CSU Ext Al1		CSU Harici Alarm 1		CSU HariciAlrm1
1924	32		15		CSU External Alarm 2		CSU Ext Al2		CSU Harici Alarm  2		CSU HariciAlrm2
1925	32		15		CSU External Alarm 3		CSU Ext Al3		CSU Harici Alarm  3		CSU HariciAlrm3
1926	32		15		CSU External Alarm 4		CSU Ext Al4		CSU Harici Alarm  4		CSU HariciAlrm4
1927	32		15		CSU External Alarm 5		CSU Ext Al5		CSU Harici Alarm  5		CSU HariciAlrm5
1928	32		15		CSU External Alarm 6		CSU Ext Al6		CSU Harici Alarm  6		CSU HariciAlrm6
1929	32		15		CSU External Alarm 7		CSU Ext Al7		CSU Harici Alarm  7		CSU HariciAlrm7
1930	32		15		CSU External Alarm 8		CSU Ext Al8		CSU Harici Alarm  8		CSU HariciAlrm8
1931	32		15	CSU External Communication Fail		CSUExtComm		CSU Harici Haberlesme Hatasi	HariciHabHatasi
1932	32		15	CSU Battery Current Limit Alarm		CSUBattCurrLim		CSU Aku Akim Limiti Alarm	Aku Akim Lim
1933	32		15		DCLCNumber			DCLCNumber		DCLC No				DCLC No
1934	32		15		BatLCNumber			BatLCNumber		AkuLCNo				AkuLCNo
1935	32		15		Very High Ambient Temperature	VHi Amb Temp		Cok Yuksek Ortam Sic.		Ortam Sic CokY
1936	32		15		System Type			System Type		Sistem Tipi			Sistem Tipi
1937	32		15		Normal				Normal			Normal				Normal
1938	32		15		Test				Test			Test				Test
1939	32		15		Auto Mode			Auto Mode		Oto Mod				Oto Mod	
1940	32		15		EMEA				EMEA			EMEA				EMEA
1941	32		15		Normal				Normal			Normal				Normal
1942	32		15		Bus Run Mode			Bus Run Mode		Baradan Calisma			COM SMDU
1943	32		15		NO				NO			NO				NO
1944	32		15		NC				NC			NC				NC
1945	32		15		Fail-Safe Mode(Hybrid)		FailSafe Mode 		Hata-Guvenli Mod(Hibrit)	Hata-GuvenliMod
1946	32		15		IB Communication Fail	IB Comm Fail 		IB haberlesme Hatasi		IB Hata
1947	32		15		Relay Test			Relay Test		Role Test			Role Test	
1948	32		15		Relay 1 Test			Relay 1 Test		Role 1 Test			Role 1 Test
1949	32		15		Relay 2 Test			Relay 2 Test		Role 2 Test			Role 2 Test
1950	32		15		Relay 3 Test			Relay 3 Test		Role 3 Test			Role 3 Test
1951	32		15		Relay 4 Test			Relay 4 Test		Role 4 Test			Role 4 Test
1952	32		15		Relay 5 Test			Relay 5 Test		Role 5 Test			Role 5 Test
1953	32		15		Relay 6 Test			Relay 6 Test		Role 6 Test			Role 6 Test
1954	32		15		Relay 7 Test			Relay 7 Test		Role 7 Test			Role 7 Test
1955	32		15		Relay 8 Test			Relay 8 Test		Role 8 Test			Role 8 Test
1956	32		15		Relay Test			Relay Test		Role Test			Role Test
1957	32		15		Relay Test Time			Relay Test Time		Role Test Zamani		RoleTest Zamani
1958	32		15		Manual				Manual			Manuel				Manuel
1959	32		15		Automatic			Automatic		Otomatik			Otomatik
1960	32		15		Temperature 1			Temperature 1		Sicaklik 1			Sic 1
1961	32		15		Temperature 2			Temperature 2		Sicaklik 2			Sic2
1962	32		15		Temperature 3 (OB)		Sic3 (OB)		Sicaklik3 (OB)			Sic3 (OB)
1963	32		15		IB2 Temperature 1		IB2 Temp1		IB2-Sic1			IB2-Sic1
1964	32		15		IB2 Temperature 2		IB2 Temp2		IB2-Sic2			IB2-Sic2
1965	32		15		EIB Temperature 1		EIB Temp1		EIB-Sic1			EIB-Sic1
1966	32		15		EIB Temperature 2		EIB Temp2		EIB-Sic2			EIB-Sic2
1967	32		15		SMTemp1 Temp1			SMTemp1 T1		SMSic1-T1			SMSic1-T1
1968	32		15		SMTemp1 Temp2			SMTemp1 T2		SMSic1-T2			SMSic1-T2
1969	32		15		SMTemp1 Temp3			SMTemp1 T3		SMSic1-T3			SMSic1-T3
1970	32		15		SMTemp1 Temp4			SMTemp1 T4		SMSic1-T4			SMSic1-T4
1971	32		15		SMTemp1 Temp5			SMTemp1 T5		SMSic1-T5			SMSic1-T5
1972	32		15		SMTemp1 Temp6			SMTemp1 T6		SMSic1-T6			SMSic1-T6
1973	32		15		SMTemp1 Temp7			SMTemp1 T7		SMSic1-T7			SMSic1-T7
1974	32		15		SMTemp1 Temp8			SMTemp1 T8		SMSic1-T8			SMSic1-T8
1975	32		15		SMTemp2 Temp1			SMTemp2 T1		SMSic2-T1			SMSic2-T1
1976	32		15		SMTemp2 Temp2			SMTemp2 T2		SMSic2-T2			SMSic2-T2
1977	32		15		SMTemp2 Temp3			SMTemp2 T3		SMSic2-T3			SMSic2-T3
1978	32		15		SMTemp2 Temp4			SMTemp2 T4		SMSic2-T4			SMSic2-T4
1979	32		15		SMTemp2 Temp5			SMTemp2 T5		SMSic2-T5			SMSic2-T5
1980	32		15		SMTemp2 Temp6			SMTemp2 T6		SMSic2-T6			SMSic2-T6
1981	32		15		SMTemp2 Temp7			SMTemp2 T7		SMSic2-T7			SMSic2-T7
1982	32		15		SMTemp2 Temp8			SMTemp2 T8		SMSic2-T8			SMSic2-T8
1983	32		15		SMTemp3 Temp1			SMTemp3 T1		SMSic3-T1			SMSic3-T1
1984	32		15		SMTemp3 Temp2			SMTemp3 T2		SMSic3-T2			SMSic3-T2
1985	32		15		SMTemp3 Temp3			SMTemp3 T3		SMSic3-T3			SMSic3-T3
1986	32		15		SMTemp3 Temp4			SMTemp3 T4		SMSic3-T4			SMSic3-T4
1987	32		15		SMTemp3 Temp5			SMTemp3 T5		SMSic3-T5			SMSic3-T5
1988	32		15		SMTemp3 Temp6			SMTemp3 T6		SMSic3-T6			SMSic3-T6
1989	32		15		SMTemp3 Temp7			SMTemp3 T7		SMSic3-T7			SMSic3-T7
1990	32		15		SMTemp3 Temp8			SMTemp3 T8		SMSic3-T8			SMSic3-T8
1991	32		15		SMTemp4 Temp1			SMTemp4 T1		SMSic4-T1			SMSic4-T1
1992	32		15		SMTemp4 Temp2			SMTemp4 T2		SMSic4-T2			SMSic4-T2
1993	32		15		SMTemp4 Temp3			SMTemp4 T3		SMSic4-T3			SMSic4-T3
1994	32		15		SMTemp4 Temp4			SMTemp4 T4		SMSic4-T4			SMSic4-T4
1995	32		15		SMTemp4 Temp5			SMTemp4 T5		SMSic4-T5			SMSic4-T5
1996	32		15		SMTemp4 Temp6			SMTemp4 T6		SMSic4-T6			SMSic4-T6
1997	32		15		SMTemp4 Temp7			SMTemp4 T7		SMSic4-T7			SMSic4-T7
1998	32		15		SMTemp4 Temp8			SMTemp4 T8		SMSic4-T8			SMSic4-T8
1999	32		15		SMTemp5 Temp1			SMTemp5 T1		SMSic5-T1			SMSic5-T1
2000	32		15		SMTemp5 Temp2			SMTemp5 T2		SMSic5-T2			SMSic5-T2
2001	32		15		SMTemp5 Temp3			SMTemp5 T3		SMSic5-T3			SMSic5-T3
2002	32		15		SMTemp5 Temp4			SMTemp5 T4		SMSic5-T4			SMSic5-T4
2003	32		15		SMTemp5 Temp5			SMTemp5 T5		SMSic5-T5			SMSic5-T5
2004	32		15		SMTemp5 Temp6			SMTemp5 T6		SMSic5-T6			SMSic5-T6
2005	32		15		SMTemp5 Temp7			SMTemp5 T7		SMSic5-T7			SMSic5-T7
2006	32		15		SMTemp5 Temp8			SMTemp5 T8		SMSic5-T8			SMSic5-T8
2007	32		15		SMTemp6 Temp1			SMTemp6 T1		SMSic6-T1			SMSic6-T1
2008	32		15		SMTemp6 Temp2			SMTemp6 T2		SMSic6-T2			SMSic6-T2
2009	32		15		SMTemp6 Temp3			SMTemp6 T3		SMSic6-T3			SMSic6-T3
2010	32		15		SMTemp6 Temp4			SMTemp6 T4		SMSic6-T4			SMSic6-T4
2011	32		15		SMTemp6 Temp5			SMTemp6 T5		SMSic6-T5			SMSic6-T5
2012	32		15		SMTemp6 Temp6			SMTemp6 T6		SMSic6-T6			SMSic6-T6
2013	32		15		SMTemp6 Temp7			SMTemp6 T7		SMSic6-T7			SMSic6-T7
2014	32		15		SMTemp6 Temp8			SMTemp6 T8		SMSic6-T8			SMSic6-T8
2015	32		15		SMTemp7 Temp1			SMTemp7 T1		SMSic7-T1			SMSic7-T1
2016	32		15		SMTemp7 Temp2			SMTemp7 T2		SMSic7-T2			SMSic7-T2
2017	32		15		SMTemp7 Temp3			SMTemp7 T3		SMSic7-T3			SMSic7-T3
2018	32		15		SMTemp7 Temp4			SMTemp7 T4		SMSic7-T4			SMSic7-T4
2019	32		15		SMTemp7 Temp5			SMTemp7 T5		SMSic7-T5			SMSic7-T5
2020	32		15		SMTemp7 Temp6			SMTemp7 T6		SMSic7-T6			SMSic7-T6
2021	32		15		SMTemp7 Temp7			SMTemp7 T7		SMSic7-T7			SMSic7-T7
2022	32		15		SMTemp7 Temp8			SMTemp7 T8		SMSic7-T8			SMSic7-T8
2023	32		15		SMTemp8 Temp1			SMTemp8 T1		SMSic8-T1			SMSic8-T1
2024	32		15		SMTemp8 Temp2			SMTemp8 T2		SMSic8-T2			SMSic8-T2
2025	32		15		SMTemp8 Temp3			SMTemp8 T3		SMSic8-T3			SMSic8-T3
2026	32		15		SMTemp8 Temp4			SMTemp8 T4		SMSic8-T4			SMSic8-T4
2027	32		15		SMTemp8 Temp5			SMTemp8 T5		SMSic8-T5			SMSic8-T5
2028	32		15		SMTemp8 Temp6			SMTemp8 T6		SMSic8-T6			SMSic8-T6
2029	32		15		SMTemp8 Temp7			SMTemp8 T7		SMSic8-T7			SMSic8-T7
2030	32		15		SMTemp8 Temp8			SMTemp8 T8		SMSic8-T8			SMSic8-T8
2031	32		15		System Temperature 1 Very High	Sys T1 VHi		Sistem Cok Yuksek Sicaklik1	Sist.Sic CokY1
2032	32		15		System Temperature 1 High	Sys T1 Hi		Sistem Sica. Yuksek 1		Sist.Sic.Yuksk1
2033	32		15		System Temperature 1 Low	Sys T1 Low		Sistem Sica. Dusuk 1		Sist.Sic.Dsk1
2034	32		15		System Temperature 2 Very High	Sys T2 VHi		Sistem Cok Yuksek Sicaklik2	Sist.Sic CokY2
2035	32		15		System Temperature 2 High	Sys T2 Hi		Sistem Sica. Yuksek 2		Sist.Sic.Yuksk2
2036	32		15		System Temperature 2 High	Sys T2 Hi		Sistem Sica. Dusuk 2		Sist.Sic.Dsk2
2037	32		15		System Temperature 3 Very High	Sys T3 VHi		Sistem Cok Yuksek Sicaklik3	Sist.Sic CokY3
2038	32		15		System Temperature 3 Low	Sys T3 Low		Sistem Sica. Yuksek 3		Sist.Sic.Yuksk3
2039	32		15		System Temperature 3 Low	Sys T3 Low		Sistem Sica. Dusuk 3		Sist.Sic.Dsk3
2040	32		15		IB2 Temperature 1 Very High 	IB2 T1 VHi		IB2 Sicaklik 1 Cok Yuksek 	IB2 T1 CokYuksk
2041	32		15		IB2 Temperature 1 High 		IB2 T1 Hi		IB2 Sicaklik 1 Yuksek 		IB2 T1 Yuksk
2042	32		15		IB2 Temperature 1 Low		IB2 T1 Low		IB2 Sicaklik 1 Dusuk 		IB2 T1 Dusuk
2043	32		15		IB2 Temperature 2 Very High 	IB2 T2 VHi		IB2 Sicaklik 2 Cok Yuksek 	IB2 T2 CokYuksk
2044	32		15		IB2 Temperature 2 High 		IB2 T2 Hi		IB2 Sicaklik 2 Yuksek 		IB2 T2 Yuksk
2045	32		15		IB2 Temperature 2 Low		IB2 T2 Low		IB2 Sicaklik 2 Dusuk 		IB2 T2 Dusuk
2046	32		15		EIB Temperature 1 Very High 	EIB T1 VHi		EIB Sicaklik 1 Cok Yuksek 	EIB T1 CokYuksk
2047	32		15		EIB Temperature 1 High 		EIB T1 Hi		EIB Sicaklik 1 Yuksek 		EIB T1 Yuksk
2048	32		15		EIB Temperature 1 Low		EIB T1 Low		EIB Sicaklik 1 Dusuk 		EIB T1 Dusuk 
2049	32		15		EIB Temperature 2 Very High 	EIB T2 VHi		EIB Sicaklik 2 Cok Yuksek 	EIB T2 CokYuksk
2050	32		15		EIB Temperature 2 High 		EIB T2 Hi		EIB Sicaklik 2 Yuksek 		EIB T2 Yuksk
2051	32		15		EIB Temperature 2 Low		EIB T2 Low		EIB Sicaklik 2 Dusuk 		EIB T2 Dusuk
#
2052		32		15		SMTemp1 Temp1 High 2	SMTemp1 T1 Hi2	SMSic1 Sic1 Yuksek2	SMSic1 T1 Yksk2
2053		32		15		SMTemp1 Temp1 High 1	SMTemp1 T1 Hi1	SMSic1 Sic1 Yuksek1	SMSic1 T1 Yksk1
2054		32		15		SMTemp1 Temp1 Low	SMTemp1 T1 Low	SMSic1 Sic1 Dusuk	SMSic1 T1 Dsk
2055		32		15		SMTemp1 Temp2 High 2	SMTemp1 T2 Hi2	SMSic1 Sic2 Yuksek2	SMSic1 T2 Yksk2
2056		32		15		SMTemp1 Temp2 High 1	SMTemp1 T2 Hi1	SMSic1 Sic2 Yuksek1	SMSic1 T2 Yksk1
2057		32		15		SMTemp1 Temp2 Low	SMTemp1 T2 Low	SMSic1 Sic2 Dusuk	SMSic1 T2 Dsk
2058		32		15		SMTemp1 Temp3 High 2	SMTemp1 T3 Hi2	SMSic1 Sic3 Yuksek2	SMSic1 T3 Yksk2
2059		32		15		SMTemp1 Temp3 High 1	SMTemp1 T3 Hi1	SMSic1 Sic3 Yuksek1	SMSic1 T3 Yksk1
2060		32		15		SMTemp1 Temp3 Low	SMTemp1 T3 Low	SMSic1 Sic3 Dusuk	SMSic1 T3 Dsk
2061		32		15		SMTemp1 Temp4 High 2	SMTemp1 T4 Hi2	SMSic1 Sic4 Yuksek2	SMSic1 T4 Yksk2
2062		32		15		SMTemp1 Temp4 High 1	SMTemp1 T4 Hi1	SMSic1 Sic4 Yuksek1	SMSic1 T4 Yksk1
2063		32		15		SMTemp1 Temp4 Low	SMTemp1 T4 Low	SMSic1 Sic4 Dusuk	SMSic1 T4 Dsk
2064		32		15		SMTemp1 Temp5 High 2	SMTemp1 T5 Hi2	SMSic1 Sic5 Yuksek2	SMSic1 T5 Yksk2
2065		32		15		SMTemp1 Temp5 High 1	SMTemp1 T5 Hi1	SMSic1 Sic5 Yuksek1	SMSic1 T5 Yksk1
2066		32		15		SMTemp1 Temp5 Low	SMTemp1 T5 Low	SMSic1 Sic5 Dusuk	SMSic1 T5 Dsk
2067		32		15		SMTemp1 Temp6 High 2	SMTemp1 T6 Hi2	SMSic1 Sic6 Yuksek2	SMSic1 T6 Yksk2
2068		32		15		SMTemp1 Temp6 High 1	SMTemp1 T6 Hi1	SMSic1 Sic6 Yuksek1	SMSic1 T6 Yksk1
2069		32		15		SMTemp1 Temp6 Low	SMTemp1 T6 Low	SMSic1 Sic6 Dusuk	SMSic1 T6 Dsk
2070		32		15		SMTemp1 Temp7 High 2	SMTemp1 T7 Hi2	SMSic1 Sic7 Yuksek2	SMSic1 T7 Yksk2
2071		32		15		SMTemp1 Temp7 High 1	SMTemp1 T7 Hi1	SMSic1 Sic7 Yuksek1	SMSic1 T7 Yksk1
2072		32		15		SMTemp1 Temp7 Low	SMTemp1 T7 Low	SMSic1 Sic7 Dusuk	SMSic1 T7 Dsk
2073		32		15		SMTemp1 Temp8 High 2	SMTemp1 T8 Hi2	SMSic1 Sic8 Yuksek2	SMSic1 T8 Yksk2
2074		32		15		SMTemp1 Temp8 High 1	SMTemp1 T8 Hi1	SMSic1 Sic8 Yuksek1	SMSic1 T8 Yksk1
2075		32		15		SMTemp1 Temp8 Low	SMTemp1 T8 Low	SMSic1 Sic8 Dusuk	SMSic1 T8 Dsk
2076		32		15		SMTemp2 Temp1 High 2	SMTemp2 T1 Hi2	SMSic2 Sic1 Yuksek2	SMSic2 T1 Yksk2
2077		32		15		SMTemp2 Temp1 High 1	SMTemp2 T1 Hi1	SMSic2 Sic1 Yuksek1	SMSic2 T1 Yksk1
2078		32		15		SMTemp2 Temp1 Low	SMTemp2 T1 Low	SMSic2 Sic1 Dusuk	SMSic2 T1 Dsk
2079		32		15		SMTemp2 Temp2 High 2	SMTemp2 T2 Hi2	SMSic2 Sic2 Yuksek2	SMSic2 T2 Yksk2
2080		32		15		SMTemp2 Temp2 High 1	SMTemp2 T2 Hi1	SMSic2 Sic2 Yuksek1	SMSic2 T2 Yksk1
2081		32		15		SMTemp2 Temp2 Low	SMTemp2 T2 Low	SMSic2 Sic2 Dusuk	SMSic2 T2 Dsk
2082		32		15		SMTemp2 Temp3 High 2	SMTemp2 T3 Hi2	SMSic2 Sic3 Yuksek2	SMSic2 T3 Yksk2
2083		32		15		SMTemp2 Temp3 High 1	SMTemp2 T3 Hi1	SMSic2 Sic3 Yuksek1	SMSic2 T3 Yksk1
2084		32		15		SMTemp2 Temp3 Low	SMTemp2 T3 Low	SMSic2 Sic3 Dusuk	SMSic2 T3 Dsk
2085		32		15		SMTemp2 Temp4 High 2	SMTemp2 T4 Hi2	SMSic2 Sic4 Yuksek2	SMSic2 T4 Yksk2
2086		32		15		SMTemp2 Temp4 High 1	SMTemp2 T4 Hi1	SMSic2 Sic4 Yuksek1	SMSic2 T4 Yksk1
2087		32		15		SMTemp2 Temp4 Low	SMTemp2 T4 Low	SMSic2 Sic4 Dusuk	SMSic2 T4 Dsk
2088		32		15		SMTemp2 Temp5 High 2	SMTemp2 T5 Hi2	SMSic2 Sic5 Yuksek2	SMSic2 T5 Yksk2
2089		32		15		SMTemp2 Temp5 High 1	SMTemp2 T5 Hi1	SMSic2 Sic5 Yuksek1	SMSic2 T5 Yksk1
2090		32		15		SMTemp2 Temp5 Low	SMTemp2 T5 Low	SMSic2 Sic5 Dusuk	SMSic2 T5 Dsk
2091		32		15		SMTemp2 Temp6 High 2	SMTemp2 T6 Hi2	SMSic2 Sic6 Yuksek2	SMSic2 T6 Yksk2
2092		32		15		SMTemp2 Temp6 High 1	SMTemp2 T6 Hi1	SMSic2 Sic6 Yuksek1	SMSic2 T6 Yksk1
2093		32		15		SMTemp2 Temp6 Low	SMTemp2 T6 Low	SMSic2 Sic6 Dusuk	SMSic2 T6 Dsk
2094		32		15		SMTemp2 Temp7 High 2	SMTemp2 T7 Hi2	SMSic2 Sic7 Yuksek2	SMSic2 T7 Yksk2
2095		32		15		SMTemp2 Temp7 High 1	SMTemp2 T7 Hi1	SMSic2 Sic7 Yuksek1	SMSic2 T7 Yksk1
2096		32		15		SMTemp2 Temp7 Low	SMTemp2 T7 Low	SMSic2 Sic7 Dusuk	SMSic2 T7 Dsk
2097		32		15		SMTemp2 Temp8 High 2	SMTemp2 T8 Hi2	SMSic2 Sic8 Yuksek2	SMSic2 T8 Yksk2
2098		32		15		SMTemp2 Temp8 High 1	SMTemp2 T8 Hi1	SMSic2 Sic8 Yuksek1	SMSic2 T8 Yksk1
2099		32		15		SMTemp2 Temp8 Low	SMTemp2 T8 Low	SMSic2 Sic8 Dusuk	SMSic2 T8 Dsk
2100		32		15		SMTemp3 Temp1 High 2	SMTemp3 T1 Hi2	SMSic3 Sic1 Yuksek2	SMSic3 T1 Yksk2
2101		32		15		SMTemp3 Temp1 High 1	SMTemp3 T1 Hi1	SMSic3 Sic1 Yuksek1	SMSic3 T1 Yksk1
2102		32		15		SMTemp3 Temp1 Low	SMTemp3 T1 Low	SMSic3 Sic1 Dusuk	SMSic3 T1 Dsk
2103		32		15		SMTemp3 Temp2 High 2	SMTemp3 T2 Hi2	SMSic3 Sic2 Yuksek2	SMSic3 T2 Yksk2
2104		32		15		SMTemp3 Temp2 High 1	SMTemp3 T2 Hi1	SMSic3 Sic2 Yuksek1	SMSic3 T2 Yksk1
2105		32		15		SMTemp3 Temp2 Low	SMTemp3 T2 Low	SMSic3 Sic2 Dusuk	SMSic3 T2 Dsk
2106		32		15		SMTemp3 Temp3 High 2	SMTemp3 T3 Hi2	SMSic3 Sic3 Yuksek2	SMSic3 T3 Yksk2
2107		32		15		SMTemp3 Temp3 High 1	SMTemp3 T3 Hi1	SMSic3 Sic3 Yuksek1	SMSic3 T3 Yksk1
2108		32		15		SMTemp3 Temp3 Low	SMTemp3 T3 Low	SMSic3 Sic3 Dusuk	SMSic3 T3 Dsk
2109		32		15		SMTemp3 Temp4 High 2	SMTemp3 T4 Hi2	SMSic3 Sic4 Yuksek2	SMSic3 T4 Yksk2
2110		32		15		SMTemp3 Temp4 High 1	SMTemp3 T4 Hi1	SMSic3 Sic4 Yuksek1	SMSic3 T4 Yksk1
2111		32		15		SMTemp3 Temp4 Low	SMTemp3 T4 Low	SMSic3 Sic4 Dusuk	SMSic3 T4 Dsk
2112		32		15		SMTemp3 Temp5 High 2	SMTemp3 T5 Hi2	SMSic3 Sic5 Yuksek2	SMSic3 T5 Yksk2
2113		32		15		SMTemp3 Temp5 High 1	SMTemp3 T5 Hi1	SMSic3 Sic5 Yuksek1	SMSic3 T5 Yksk1
2114		32		15		SMTemp3 Temp5 Low	SMTemp3 T5 Low	SMSic3 Sic5 Dusuk	SMSic3 T5 Dsk
2115		32		15		SMTemp3 Temp6 High 2	SMTemp3 T6 Hi2	SMSic3 Sic6 Yuksek2	SMSic3 T6 Yksk2
2116		32		15		SMTemp3 Temp6 High 1	SMTemp3 T6 Hi1	SMSic3 Sic6 Yuksek1	SMSic3 T6 Yksk1
2117		32		15		SMTemp3 Temp6 Low	SMTemp3 T6 Low	SMSic3 Sic6 Dusuk	SMSic3 T6 Dsk
2118		32		15		SMTemp3 Temp7 High 2	SMTemp3 T7 Hi2	SMSic3 Sic7 Yuksek2	SMSic3 T7 Yksk2
2119		32		15		SMTemp3 Temp7 High 1	SMTemp3 T7 Hi1	SMSic3 Sic7 Yuksek1	SMSic3 T7 Yksk1
2120		32		15		SMTemp3 Temp7 Low	SMTemp3 T7 Low	SMSic3 Sic7 Dusuk	SMSic3 T7 Dsk
2121		32		15		SMTemp3 Temp8 High 2	SMTemp3 T8 Hi2	SMSic3 Sic8 Yuksek2	SMSic3 T8 Yksk2
2122		32		15		SMTemp3 Temp8 High 1	SMTemp3 T8 Hi1	SMSic3 Sic8 Yuksek1	SMSic3 T8 Yksk1
2123		32		15		SMTemp3 Temp8 Low	SMTemp3 T8 Low	SMSic3 Sic8 Dusuk	SMSic3 T8 Dsk
2124		32		15		SMTemp4 Temp1 High 2	SMTemp4 T1 Hi2	SMSic4 Sic1 Yuksek2	SMSic4 T1 Yksk2
2125		32		15		SMTemp4 Temp1 High 1	SMTemp4 T1 Hi1	SMSic4 Sic1 Yuksek1	SMSic4 T1 Yksk1
2126		32		15		SMTemp4 Temp1 Low	SMTemp4 T1 Low	SMSic4 Sic1 Dusuk	SMSic4 T1 Dsk
2127		32		15		SMTemp4 Temp2 High 2	SMTemp4 T2 Hi2	SMSic4 Sic2 Yuksek2	SMSic4 T2 Yksk2
2128		32		15		SMTemp4 Temp2 High 1	SMTemp4 T2 Hi1	SMSic4 Sic2 Yuksek1	SMSic4 T2 Yksk1
2129		32		15		SMTemp4 Temp2 Low	SMTemp4 T2 Low	SMSic4 Sic2 Dusuk	SMSic4 T2 Dsk
2130		32		15		SMTemp4 Temp3 High 2	SMTemp4 T3 Hi2	SMSic4 Sic3 Yuksek2	SMSic4 T3 Yksk2
2131		32		15		SMTemp4 Temp3 High 1	SMTemp4 T3 Hi1	SMSic4 Sic3 Yuksek1	SMSic4 T3 Yksk1
2132		32		15		SMTemp4 Temp3 Low	SMTemp4 T3 Low	SMSic4 Sic3 Dusuk	SMSic4 T3 Dsk
2133		32		15		SMTemp4 Temp4 High 2	SMTemp4 T4 Hi2	SMSic4 Sic4 Yuksek2	SMSic4 T4 Yksk2
2134		32		15		SMTemp4 Temp4 High 1	SMTemp4 T4 Hi1	SMSic4 Sic4 Yuksek1	SMSic4 T4 Yksk1
2135		32		15		SMTemp4 Temp4 Low	SMTemp4 T4 Low	SMSic4 Sic4 Dusuk	SMSic4 T4 Dsk
2136		32		15		SMTemp4 Temp5 High 2	SMTemp4 T5 Hi2	SMSic4 Sic5 Yuksek2	SMSic4 T5 Yksk2
2137		32		15		SMTemp4 Temp5 High 1	SMTemp4 T5 Hi1	SMSic4 Sic5 Yuksek1	SMSic4 T5 Yksk1
2138		32		15		SMTemp4 Temp5 Low	SMTemp4 T5 Low	SMSic4 Sic5 Dusuk	SMSic4 T5 Dsk
2139		32		15		SMTemp4 Temp6 High 2	SMTemp4 T6 Hi2	SMSic4 Sic6 Yuksek2	SMSic4 T6 Yksk2
2140		32		15		SMTemp4 Temp6 High 1	SMTemp4 T6 Hi1	SMSic4 Sic6 Yuksek1	SMSic4 T6 Yksk1
2141		32		15		SMTemp4 Temp6 Low	SMTemp4 T6 Low	SMSic4 Sic6 Dusuk	SMSic4 T6 Dsk
2142		32		15		SMTemp4 Temp7 High 2	SMTemp4 T7 Hi2	SMSic4 Sic7 Yuksek2	SMSic4 T7 Yksk2
2143		32		15		SMTemp4 Temp7 High 1	SMTemp4 T7 Hi1	SMSic4 Sic7 Yuksek1	SMSic4 T7 Yksk1
2144		32		15		SMTemp4 Temp7 Low	SMTemp4 T7 Low	SMSic4 Sic7 Dusuk	SMSic4 T7 Dsk
2145		32		15		SMTemp4 Temp8 High 2	SMTemp4 T8 Hi2	SMSic4 Sic8 Yuksek2	SMSic4 T8 Yksk2
2146		32		15		SMTemp4 Temp8 Hi1	SMTemp4 T8 Hi1	SMSic4 Sic8 Yuksek1	SMSic4 T8 Yksk1
2147		32		15		SMTemp4 Temp8 Low	SMTemp4 T8 Low	SMSic4 Sic8 Dusuk	SMSic4 T8 Dsk
2148		32		15		SMTemp5 Temp1 High 2	SMTemp5 T1 Hi2	SMSic5 Sic1 Yuksek2	SMSic5 T1 Yksk2
2149		32		15		SMTemp5 Temp1 High 1	SMTemp5 T1 Hi1	SMSic5 Sic1 Yuksek1	SMSic5 T1 Yksk1
2150		32		15		SMTemp5 Temp1 Low	SMTemp5 T1 Low	SMSic5 Sic1 Dusuk	SMSic5 T1 Dsk
2151		32		15		SMTemp5 Temp2 High 2	SMTemp5 T2 Hi2	SMSic5 Sic2 Yuksek2	SMSic5 T2 Yksk2
2152		32		15		SMTemp5 Temp2 High 1	SMTemp5 T2 Hi1	SMSic5 Sic2 Yuksek1	SMSic5 T2 Yksk1
2153		32		15		SMTemp5 Temp2 Low	SMTemp5 T2 Low	SMSic5 Sic2 Dusuk	SMSic5 T2 Dsk
2154		32		15		SMTemp5 Temp3 High 2	SMTemp5 T3 Hi2	SMSic5 Sic3 Yuksek2	SMSic5 T3 Yksk2
2155		32		15		SMTemp5 Temp3 High 1	SMTemp5 T3 Hi1	SMSic5 Sic3 Yuksek1	SMSic5 T3 Yksk1
2156		32		15		SMTemp5 Temp3 Low	SMTemp5 T3 Low	SMSic5 Sic3 Dusuk	SMSic5 T3 Dsk
2157		32		15		SMTemp5 Temp4 High 2	SMTemp5 T4 Hi2	SMSic5 Sic4 Yuksek2	SMSic5 T4 Yksk2
2158		32		15		SMTemp5 Temp4 High 1	SMTemp5 T4 Hi1	SMSic5 Sic4 Yuksek1	SMSic5 T4 Yksk1
2159		32		15		SMTemp5 Temp4 Low	SMTemp5 T4 Low	SMSic5 Sic4 Dusuk	SMSic5 T4 Dsk
2160		32		15		SMTemp5 Temp5 High 2	SMTemp5 T5 Hi2	SMSic5 Sic5 Yuksek2	SMSic5 T5 Yksk2
2161		32		15		SMTemp5 Temp5 High 1	SMTemp5 T5 Hi1	SMSic5 Sic5 Yuksek1	SMSic5 T5 Yksk1
2162		32		15		SMTemp5 Temp5 Low	SMTemp5 T5 Low	SMSic5 Sic5 Dusuk	SMSic5 T5 Dsk
2163		32		15		SMTemp5 Temp6 High 2	SMTemp5 T6 Hi2	SMSic5 Sic6 Yuksek2	SMSic5 T6 Yksk2
2164		32		15		SMTemp5 Temp6 High 1	SMTemp5 T6 Hi1	SMSic5 Sic6 Yuksek1	SMSic5 T6 Yksk1
2165		32		15		SMTemp5 Temp6 Low	SMTemp5 T6 Low	SMSic5 Sic6 Dusuk	SMSic5 T6 Dsk
2166		32		15		SMTemp5 Temp7 High 2	SMTemp5 T7 Hi2	SMSic5 Sic7 Yuksek2	SMSic5 T7 Yksk2
2167		32		15		SMTemp5 Temp7 High 1	SMTemp5 T7 Hi1	SMSic5 Sic7 Yuksek1	SMSic5 T7 Yksk1
2168		32		15		SMTemp5 Temp7 Low	SMTemp5 T7 Low	SMSic5 Sic7 Dusuk	SMSic5 T7 Dsk
2169		32		15		SMTemp5 Temp8 High 2	SMTemp5 T8 Hi2	SMSic5 Sic8 Yuksek2	SMSic5 T8 Yksk2
2170		32		15		SMTemp5 Temp8 High 1	SMTemp5 T8 Hi1	SMSic5 Sic8 Yuksek1	SMSic5 T8 Yksk1
2171		32		15		SMTemp5 Temp8 Low	SMTemp5 T8 Low	SMSic5 Sic8 Dusuk	SMSic5 T8 Dsk
2172		32		15		SMTemp6 Temp1 High 2	SMTemp6 T1 Hi2	SMSic6 Sic1 Yuksek2	SMSic6 T1 Yksk2
2173		32		15		SMTemp6 Temp1 High 1	SMTemp6 T1 Hi1	SMSic6 Sic1 Yuksek1	SMSic6 T1 Yksk1
2174		32		15		SMTemp6 Temp1 Low	SMTemp6 T1 Low	SMSic6 Sic1 Dusuk	SMSic6 T1 Dsk
2175		32		15		SMTemp6 Temp2 High 2	SMTemp6 T2 Hi2	SMSic6 Sic2 Yuksek2	SMSic6 T2 Yksk2
2176		32		15		SMTemp6 Temp2 High 1	SMTemp6 T2 Hi1	SMSic6 Sic2 Yuksek1	SMSic6 T2 Yksk1
2177		32		15		SMTemp6 Temp2 Low	SMTemp6 T2 Low	SMSic6 Sic2 Dusuk	SMSic6 T2 Dsk
2178		32		15		SMTemp6 Temp3 High 2	SMTemp6 T3 Hi2	SMSic6 Sic3 Yuksek2	SMSic6 T3 Yksk2
2179		32		15		SMTemp6 Temp3 High 1	SMTemp6 T3 Hi1	SMSic6 Sic3 Yuksek1	SMSic6 T3 Yksk1
2180		32		15		SMTemp6 Temp3 Low	SMTemp6 T3 Low	SMSic6 Sic3 Dusuk	SMSic6 T3 Dsk
2181		32		15		SMTemp6 Temp4 High 2	SMTemp6 T4 Hi2	SMSic6 Sic4 Yuksek2	SMSic6 T4 Yksk2
2182		32		15		SMTemp6 Temp4 High 1	SMTemp6 T4 Hi1	SMSic6 Sic4 Yuksek1	SMSic6 T4 Yksk1
2183		32		15		SMTemp6 Temp4 Low	SMTemp6 T4 Low	SMSic6 Sic4 Dusuk	SMSic6 T4 Dsk
2184		32		15		SMTemp6 Temp5 High 2	SMTemp6 T5 Hi2	SMSic6 Sic5 Yuksek2	SMSic6 T5 Yksk2
2185		32		15		SMTemp6 Temp5 High 1	SMTemp6 T5 Hi1	SMSic6 Sic5 Yuksek1	SMSic6 T5 Yksk1
2186		32		15		SMTemp6 Temp5 Low	SMTemp6 T5 Low	SMSic6 Sic5 Dusuk	SMSic6 T5 Dsk
2187		32		15		SMTemp6 Temp6 High 2	SMTemp6 T6 Hi2	SMSic6 Sic6 Yuksek2	SMSic6 T6 Yksk2
2188		32		15		SMTemp6 Temp6 High 1	SMTemp6 T6 Hi1	SMSic6 Sic6 Yuksek1	SMSic6 T6 Yksk1
2189		32		15		SMTemp6 Temp6 Low	SMTemp6 T6 Low	SMSic6 Sic6 Dusuk	SMSic6 T6 Dsk
2190		32		15		SMTemp6 Temp7 High 2	SMTemp6 T7 Hi2	SMSic6 Sic7 Yuksek2	SMSic6 T7 Yksk2
2191		32		15		SMTemp6 Temp7 High 1	SMTemp6 T7 Hi1	SMSic6 Sic7 Yuksek1	SMSic6 T7 Yksk1
2192		32		15		SMTemp6 Temp7 Low	SMTemp6 T7 Low	SMSic6 Sic7 Dusuk	SMSic6 T7 Dsk
2193		32		15		SMTemp6 Temp8 High 2	SMTemp6 T8 Hi2	SMSic6 Sic8 Yuksek2	SMSic6 T8 Yksk2
2194		32		15		SMTemp6 Temp8 High 1	SMTemp6 T8 Hi1	SMSic6 Sic8 Yuksek1	SMSic6 T8 Yksk1
2195		32		15		SMTemp6 Temp8 Low	SMTemp6 T8 Low	SMSic6 Sic8 Dusuk	SMSic6 T8 Dsk
2196		32		15		SMTemp7 Temp1 High 2	SMTemp7 T1 Hi2	SMSic7 Sic1 Yuksek2	SMSic7 T1 Yksk2
2197		32		15		SMTemp7 Temp1 High 1	SMTemp7 T1 Hi1	SMSic7 Sic1 Yuksek1	SMSic7 T1 Yksk1
2198		32		15		SMTemp7 Temp1 Low	SMTemp7 T1 Low	SMSic7 Sic1 Dusuk	SMSic7 T1 Dsk
2199		32		15		SMTemp7 Temp2 High 2	SMTemp7 T2 Hi2	SMSic7 Sic2 Yuksek2	SMSic7 T2 Yksk2
2200		32		15		SMTemp7 Temp2 High 1	SMTemp7 T2 Hi1	SMSic7 Sic2 Yuksek1	SMSic7 T2 Yksk1
2201		32		15		SMTemp7 Temp2 Low	SMTemp7 T2 Low	SMSic7 Sic2 Dusuk	SMSic7 T2 Dsk
2202		32		15		SMTemp7 Temp3 High 2	SMTemp7 T3 Hi2	SMSic7 Sic3 Yuksek2	SMSic7 T3 Yksk2
2203		32		15		SMTemp7 Temp3 High 1	SMTemp7 T3 Hi1	SMSic7 Sic3 Yuksek1	SMSic7 T3 Yksk1
2204		32		15		SMTemp7 Temp3 Low	SMTemp7 T3 Low	SMSic7 Sic3 Dusuk	SMSic7 T3 Dsk
2205		32		15		SMTemp7 Temp4 High 2	SMTemp7 T4 Hi2	SMSic7 Sic4 Yuksek2	SMSic7 T4 Yksk2
2206		32		15		SMTemp7 Temp4 High 1	SMTemp7 T4 Hi1	SMSic7 Sic4 Yuksek1	SMSic7 T4 Yksk1
2207		32		15		SMTemp7 Temp4 Low	SMTemp7 T4 Low	SMSic7 Sic4 Dusuk	SMSic7 T4 Dsk
2208		32		15		SMTemp7 Temp5 High 2	SMTemp7 T5 Hi2	SMSic7 Sic5 Yuksek2	SMSic7 T5 Yksk2
2209		32		15		SMTemp7 Temp5 High 1	SMTemp7 T5 Hi1	SMSic7 Sic5 Yuksek1	SMSic7 T5 Yksk1
2210		32		15		SMTemp7 Temp5 Low	SMTemp7 T5 Low	SMSic7 Sic5 Dusuk	SMSic7 T5 Dsk
2211		32		15		SMTemp7 Temp6 High 2	SMTemp7 T6 Hi2	SMSic7 Sic6 Yuksek2	SMSic7 T6 Yksk2
2212		32		15		SMTemp7 Temp6 High 1	SMTemp7 T6 Hi1	SMSic7 Sic6 Yuksek1	SMSic7 T6 Yksk1
2213		32		15		SMTemp7 Temp6 Low	SMTemp7 T6 Low	SMSic7 Sic6 Dusuk	SMSic7 T6 Dsk
2214		32		15		SMTemp7 Temp7 High 2	SMTemp7 T7 Hi2	SMSic7 Sic7 Yuksek2	SMSic7 T7 Yksk2
2215		32		15		SMTemp7 Temp7 High 1	SMTemp7 T7 Hi1	SMSic7 Sic7 Yuksek1	SMSic7 T7 Yksk1
2216		32		15		SMTemp7 Temp7 Low	SMTemp7 T7 Low	SMSic7 Sic7 Dusuk	SMSic7 T7 Dsk
2217		32		15		SMTemp7 Temp8 High 2	SMTemp7 T8 Hi2	SMSic7 Sic8 Yuksek2	SMSic7 T8 Yksk2
2218		32		15		SMTemp7 Temp8 High 1	SMTemp7 T8 Hi1	SMSic7 Sic8 Yuksek1	SMSic7 T8 Yksk1
2219		32		15		SMTemp7 Temp8 Low	SMTemp7 T8 Low	SMSic7 Sic8 Dusuk	SMSic7 T8 Dsk
2220		32		15		SMTemp8 Temp1 High 2	SMTemp8 T1 Hi2	SMSic8 Sic1 Yuksek2	SMSic8 T1 Yksk2
2221		32		15		SMTemp8 Temp1 High 1	SMTemp8 T1 Hi1	SMSic8 Sic1 Yuksek1	SMSic8 T1 Yksk1
2222		32		15		SMTemp8 Temp1 Low	SMTemp8 T1 Low	SMSic8 Sic1 Dusuk	SMSic8 T1 Dsk
2223		32		15		SMTemp8 Temp2 High 2	SMTemp8 T2 Hi2	SMSic8 Sic2 Yuksek2	SMSic8 T2 Yksk2
2224		32		15		SMTemp8 Temp2 High 1	SMTemp8 T2 Hi1	SMSic8 Sic2 Yuksek1	SMSic8 T2 Yksk1
2225		32		15		SMTemp8 Temp2 Low	SMTemp8 T2 Low	SMSic8 Sic2 Dusuk	SMSic8 T2 Dsk
2226		32		15		SMTemp8 Temp3 High 2	SMTemp8 T3 Hi2	SMSic8 Sic3 Yuksek2	SMSic8 T3 Yksk2
2227		32		15		SMTemp8 Temp3 High 1	SMTemp8 T3 Hi1	SMSic8 Sic3 Yuksek1	SMSic8 T3 Yksk1
2228		32		15		SMTemp8 Temp3 Low	SMTemp8 T3 Low	SMSic8 Sic3 Dusuk	SMSic8 T3 Dsk
2229		32		15		SMTemp8 Temp4 High 2	SMTemp8 T4 Hi2	SMSic8 Sic4 Yuksek2	SMSic8 T4 Yksk2
2230		32		15		SMTemp8 Temp4 High 1	SMTemp8 T4 Hi1	SMSic8 Sic4 Yuksek1	SMSic8 T4 Yksk1
2231		32		15		SMTemp8 Temp4 Low	SMTemp8 T4 Low	SMSic8 Sic4 Dusuk	SMSic8 T4 Dsk
2232		32		15		SMTemp8 Temp5 High 2	SMTemp8 T5 Hi2	SMSic8 Sic5 Yuksek2	SMSic8 T5 Yksk2
2233		32		15		SMTemp8 Temp5 High 1	SMTemp8 T5 Hi1	SMSic8 Sic5 Yuksek1	SMSic8 T5 Yksk1
2234		32		15		SMTemp8 Temp5 Low	SMTemp8 T5 Low	SMSic8 Sic5 Dusuk	SMSic8 T5 Dsk
2235		32		15		SMTemp8 Temp6 High 2	SMTemp8 T6 Hi2	SMSic8 Sic6 Yuksek2	SMSic8 T6 Yksk2
2236		32		15		SMTemp8 Temp6 High 1	SMTemp8 T6 Hi1	SMSic8 Sic6 Yuksek1	SMSic8 T6 Yksk1
2237		32		15		SMTemp8 Temp6 Low	SMTemp8 T6 Low	SMSic8 Sic6 Dusuk	SMSic8 T6 Dsk
2238		32		15		SMTemp8 Temp7 High 2	SMTemp8 T7 Hi2	SMSic8 Sic7 Yuksek2	SMSic8 T7 Yksk2
2239		32		15		SMTemp8 Temp7 High 1	SMTemp8 T7 Hi1	SMSic8 Sic7 Yuksek1	SMSic8 T7 Yksk1
2240		32		15		SMTemp8 Temp7 Low	SMTemp8 T7 Low	SMSic8 Sic7 Dusuk	SMSic8 T7 Dsk
2241		32		15		SMTemp8 Temp8 High 2	SMTemp8 T8 Hi2	SMSic8 Sic8 Yuksek2	SMSic8 T8 Yksk2
2242		32		15		SMTemp8 Temp8 High 1	SMTemp8 T8 Hi1	SMSic8 Sic8 Yuksek1	SMSic8 T8 Yksk1
2243		32		15		SMTemp8 Temp8 Low	SMTemp8 T8 Low	SMSic8 Sic8 Dusuk	SMSic8 T8 Dsk
2244	32		15		None				None			Hicbiri				Hicbiri
2245	32		15		High Load Level1		HighLoadLevel1		Yuksek Yuk Seviyesi 1 		Yuksek Yuk 1
2246	32		15		High Load Level2		HighLoadLevel2		Yuksek Yuk Seviyesi 2 		Yuksek Yuk 2
2247	32		15		Maximum				Maximum			Maksimum		Maksimum
2248	32		15		Average				Average			Ortalama			Ortalama
2249		32		15		Solar Mode		Solar Mode		Solar Modu		Solar Modu
2250		32		15		Running Way(For Solar)	Running Way		Calisma Yolu		Calisma Yolu
2251		32		15		Disabled			Disabled			Devre Disi	Devre Disi
2252		32		15		RECT-SOLAR		RECT-SOLAR		DOGRULTUCU-SOLAR		DOGRULTUCU-SOLAR
2253		32		15		SOLAR			SOLAR			SOLAR		SOLAR
2254		32		15		RECT First		RECT First		Once Dogrultucu		Once Dogrultucu
2255		32		15		Solar First		Solar First		Once Solar		Once Solar
2256		32		15		Please reboot after changing			Please reboot		Lutfen degistirdikten sonra yeniden deneyin.	Lutfen Y.Deneyin
2257		32		15		CSU Fail			CSU Fail		CSU Ariza		CSU Ariza	
2258		32		15		SSL and SNMPV3		SSL and SNMPV3		SSL ve SNMPV3		SSL ve SNMPV3	
2259		32		15		Adjust Bus Input Voltage	Adjust In-Volt		Bara Giris Gerilimi Ayarla	Giris Geril.Ayar	
2260		32		15		Confirm Voltage Supplied	Confirm Voltage		Saglanan Gerilim Onayla		Sag.Gerilim Onay
2261		32		15			Converter Only				Converter Only		Sadece Donusturucu			Sadece Donust.
2262		32		15		Net-Port Reset Interval		Reset Interval		Net-Port Araligi Reset		Net-Port Ara.Reset
2263		32		15		DC1 Load			DC1 Load		DC1 Yuk		DC1 Yuk

2264	32		15		DI9					DI9					Dijital Giris 9			Dijital Giris 9
2265	32		15		DI10				DI10				Dijital Giris 10			Dijital Giris 10
2266	32		15		DI11				DI11				Dijital Giris 11		Dijital Giris 11
2267	32		15		DI12				DI12				Dijital Giris 12			Dijital Giris 12
2268	32			15		Digital Input 9		DI 9		Dijital Giris 9			Dijital Giris 9
2269	32			15		Digital Input 10	DI 10	Dijital Giris 10			Dijital Giris 10
2270	32			15		Digital Input 11	DI 11	Dijital Giris 11			Dijital Giris 11
2271	32			15		Digital Input 12	DI 12	Dijital Giris 12			Dijital Giris 12
2272	32			15		DI9 Alarm State		DI9 Alm State		Dijital Giris 9 Alarm Durumu		D.Giris9Alm 
2273	32			15		DI10 Alarm State	DI10 Alm State	Dijital Giris 10 Alarm Durumu		D.Giris10Alm  
2274	32			15		DI11 Alarm State	DI11 Alm State	Dijital Giris 11 Alarm Durumu		D.Giris11Alm 
2275	32			15		DI12 Alarm State	DI12 Alm State	Dijital Giris 12 Alarm Durumu		D.Giris12Alm 
2276	32			15		DI9 Alarm			DI9 Alarm			Dijital Giris 9 Alarm		D.Giris 9 Alarm
2277	32			15		DI10 Alarm			DI10 Alarm			Dijital Giris 10 Alarm		D.Giris 10 Alarm
2278	32			15		DI11 Alarm			DI11 Alarm			Dijital Giris 11 Alarm		D.Giris 11 Alarm
2279	32			15		DI12 Alarm			DI12 Alarm			Dijital Giris 12 Alarm		D.Giris 12 Alarm
2280	32			15		None			None		Hicbiri			Hicbiri
2281	32			15		Exist			Exist		Mevcut			Mevcut
2282	32			15		IB01 State			IB01 State		IB01 Durumu			IB01 Durumu
2283	32		15		Relay Output 14			Relay Output 14		Role Cikis 14			Role Cikis 14   
2284	32		15		Relay Output 15			Relay Output 15		Role Cikis 15			Role Cikis 15 
2285	32		15		Relay Output 16			Relay Output 16		Role Cikis 16			Role Cikis 16  
2286	32		15		Relay Output 17			Relay Output 17		Role Cikis 17			Role Cikis 17 
2287	32		15		Time Display Format		Time Format		Zaman Gosterim Formati		Zaman Formati  
2288	32		15		EMEA				EMEA		EMEA				EMEA  
2289	32		15		NA				NA		NA				NA 
2290	32		15		CN				CN		CN				CN  
2291	32		15		Help				Help		Yard?m				Yard?m  
2292	32		15		Current Protocol Type		Protocol	Guncel Protokol Tipi			Protokol  
2293	32		15		EEM				EEM		EEM				EEM  
2294	32		15		YDN23				YDN23		YDN23				YDN23
2295	32		15		Modbus				ModBus		ModBus				ModBus
2296	32		15		SMS Alarm Level			SMS Alarm Level		SMS Alarm Seviyesi			SMS Alarm Seviy.
2297	32		15		Disabled			Disabled			Etkin Degil			Etkin Degil
2298	32		15		Observation		Observation		Minor		Minor
2299	32		15		Major			Major			Major		Major
2300	32		15		Critical		Critical		Kritik		Kritik 
2301	64		64		SPD is not connected to system or SPD is broken.	SPD is not connected to system or SPD is broken.	SPD sisteme bagli degil veya SPD bozuldu.	SPD sisteme bagli degil veya SPD bozuldu.
2302	32		15		Big Screen		Big Screen		Buyuk Ekran		Buyuk Ekran
2303	32		15		EMAIL Alarm Level			EMAIL Alarm Level		EMAIL Alarm Seviyesi			EMAIL Alarm Seviye
2304	32		15		HLMS Protocol Type	Protocol Type		HLMS Protokol Tipi		HLMS Protokol Tipi
2305	32		15		EEM			EEM			EEM			EEM
2306	32		15		YDN23			YDN23			YDN23			YDN23
2307	32		15		Modbus			Modbus			Modbus			Modbus
2308		32		15		24V Display State		24V Display 		24V Ekran Durumu		24V Ekran Durumu
2309		32		15		Syst Volt Level				Syst Volt Level				Sistem Gerilim Seviyesi			Sistem Ger Seviye
2310		32		15		48 V System				48 V System				48V Sistem				48V Sistem
2311		32		15		24 V System				24 V System				24V Sistem				24V Sistem
2312		32		15		Power Input Type			Power Input Type			Guc Giris Tipi				Guc Giris Tipi
2313		32		15		AC Input				AC Input				AC Giris				AC Giris
2314		32		15		Diesel Input				Diesel Input				Dizel Giris				Dizel Giris
2315		32		15		2nd IB2 Temp1		2nd IB2 Temp1	Ikinci IB2 Sicaklik1		Ikinci IB2 Sic1
2316		32		15		2nd IB2 Temp2		2nd IB2 Temp2	Ikinci IB2 Sicaklik2		Ikinci IB2 Sic2
2317		32		15		EIB2 Temp1		EIB2 Temp1	EIB2 Sicaklik1		EIB2 Sicaklik1
2318		32		15		EIB2 Temp2		EIB2 Temp2	EIB2 Sicaklik2		EIB2 Sicaklik2
2319		32		15		2nd IB2 Temp1 High 2 	2nd IB2 T1 Hi2	Ikinci IB2 Sicaklik1 Yuksek 2	Ikinci IB2 Sic1 Yuk 2
2320		32		15		2nd IB2 Temp1 High 1 	2nd IB2 T1 Hi1	Ikinci IB2 Sicaklik1 Yuksek 1	Ikinci IB2 Sic1 Yuk 1
2321		32		15		2nd IB2 Temp1 Low 	2nd IB2 T1 Low	Ikinci IB2 Sicaklik1 Dusuk	Ikinci IB2 Sic1 Dus
2322		32		15		2nd IB2 Temp2 High 2 	2nd IB2 T2 Hi2	Ikinci IB2 Sicaklik2 Yuksek 2	Ikinci IB2 Sic2 Yuk 2
2323		32		15		2nd IB2 Temp2 High 1 	2nd IB2 T2 Hi1	Ikinci IB2 Sicaklik2 Yuksek 1	Ikinci IB2 Sic2 Yuk 1
2324		32		15		2nd IB2 Temp2 Low 	2nd IB2 T2 Low	Ikinci IB2 Sicaklik2 Dusuk	Ikinci IB2 Sic2 Dus
2325		32		15		EIB2 Temp1 High 2 	EIB2 T1 Hi2	EIB2 Sicaklik1 Yuksek 2		EIB2 Sic1 Yuk 2
2326		32		15		EIB2 Temp1 High 1 	EIB2 T1 Hi1	EIB2 Sicaklik1 Yuksek 1		EIB2 Sic1 Yuk 1
2327		32		15		EIB2 Temp1 Low 		EIB2 T1 Low	EIB2 Sicaklik1 Dusuk		EIB2 Sic1 Dus
2328		32		15		EIB2 Temp2 High 2 	EIB2 T2 Hi2	EIB2 Sicaklik2 Yuksek 2		EIB2 Sic2 Yuk 2
2329		32		15		EIB2 Temp2 High 1 	EIB2 T2 Hi1	EIB2 Sicaklik2 Yuksek 1		EIB2 Sic2 Yuk 1
2330		32		15		EIB2 Temp2 Low 		EIB2 T2 Low	EIB2 Sicaklik2 Dusuk		EIB2 Sic2 Dus
2331		32		15		Relay 14 Test			Relay 14 Test	Role 14 Test			Role 14 Test
2332		32		15		Relay 15 Test			Relay 15 Test	Role 15 Test			Role 15 Test
2333		32		15		Relay 16 Test			Relay 16 Test	Role 16 Test			Role 16 Test
2334		32		15		Relay 17 Test			Relay 17 Test	Role 17 Test			Role 17 Test	
2335		32		15		Total Output Rated		Total Output Rated	Anma Toplam Cikis	Anma Toplam Cikis	
2336		32		15		Load current capacity		Load capacity	Yuk Aki Kapasitesi		Yuk Aki Kapas.
2337		32		15		EES System Mode			EES System Mode	  ES Sistem Modu	ES Sistem Modu
2338		32		15		SMS Modem Fail			SMS Modem Fail	SM Modem Hatasi		SM Modem Hatasi
2339		32		15		SMDU-EIB Mode			SMDU-EIB Mode	SMDU-EIB Modu		SMDU-EIB Modu
2340		32		15		Load Switch			Load Switch		Yuk Anahtari		Yuk Anahtari		
2341		32		15		Manual State			Manual State	Manuel Durumu	Manuel Durumu	
2342		32		15		Converter Voltage Level		Volt Level	Donusturucu Gerilim Seviyesi	Donus. Ger Seviye	
2343		32		15		CB Threshold  Value		Threshold Value	Sigorta Esik Degeri		Esik Degeri
2344		32		15		CB Overload  Value		Overload Value	Sigorta Asiri Yuk Degeri	Asiri Yuk Degeri
2345		32		15		SNMP Config Error		SNMP Config Err		SNMP Yapilandirma Hatasi		SNMP Yapilan. Hata
2346		32		15		Cab X Num(Valid After Reset)	Cabinet X Num		Kabin X Num (Gecerli Reset sonrasi)	Kabin X Num
2347		32		15		Cab Y Num(Valid After Reset)	Cabinet Y Num		Kabin Y Num (Gecerli Reset sonrasi)	Kabin Y Num
2348		32		15		CB Threshold Power		Threshold Power		CB Esik Güc				Esik Güc 
2349		32		15		CB Overload Power		Overload Power		CB Güc Asiri yük			Güc Asiri yük 
2350		32		15		With FCUP			With FCUP		FCUP ile				FCUP ile
2351		32		15		FCUP Existence State		FCUP Exist State	Varlik Durumu FCUP			Varlik Durumu FCUP

2357		32			15			DO1 Normal State  			DO1 Normal		DO1 Normal State  			DO1 Normal
2358		32			15			DO2 Normal State  			DO2 Normal		DO2 Normal State  			DO2 Normal
2359		32			15			DO3 Normal State  			DO3 Normal		DO3 Normal State  			DO3 Normal
2360		32			15			DO4 Normal State  			DO4 Normal		DO4 Normal State  			DO4 Normal
2361		32			15			DO5 Normal State  			DO5 Normal		DO5 Normal State  			DO5 Normal
2362		32			15			DO6 Normal State  			DO6 Normal		DO6 Normal State  			DO6 Normal
2363		32			15			DO7 Normal State  			DO7 Normal		DO7 Normal State  			DO7 Normal
2364		32			15			DO8 Normal State  			DO8 Normal		DO8 Normal State  			DO8 Normal
2365		32			15			Non-Energized				Non-Energized		Non-Energized				Non-Energized
2366		32			15			Energized				Energized		Energized				Energized
2367		32			15			DO14 Normal State  			DO14 Normal		DO14 Normal State  			DO14 Normal
2368		32			15			DO15 Normal State  			DO15 Normal		DO15 Normal State  			DO15 Normal
2369		32			15			DO16 Normal State  			DO16 Normal		DO16 Normal State  			DO16 Normal
2370		32			15			DO17 Normal State  			DO17 Normal		DO17 Normal State  			DO17 Normal

2371		32			15			SSH Enabled  			        SSH Enabled		SSH leştirme				SSH leştirme
2372		32			15			Disabled				Disabled		etkin değildir  			etkin değildir
2373		32			15			Enabled					Enabled			etkinleştirme				etkinleştirme 
2374		32			15			Page Type For Login			Page Type		Page Type For Login			Page Type
2375		32			15			Standard				Standard		Standard				Standard
2376		32			15			For BT					For BT			For BT					For BT	
2377		32			15			Fiamm Battery				Fiamm Battery		Fiamm akü				Fiamm Akü
2378		32			15			Correct Temperature1			Correct Temp1			Correct Temperature1			Correct Temp1
2379		32			15			Correct Temperature2			Correct Temp2			Correct Temperature2			Correct Temp2

2503		32			15			NTP Function Enable			NTPFuncEnable		NTP Enable				NTP Enable
2504		32			15			SW_Switch1			SW_Switch1			SW_Anahtar1				SW_Anahtar1	
2505		32			15			SW_Switch2			SW_Switch2			SW_Anahtar2				SW_Anahtar2	
2506		32			15			SW_Switch3			SW_Switch3			SW_Anahtar3				SW_Anahtar3	
2507		32			15			SW_Switch4			SW_Switch4			SW_Anahtar4				SW_Anahtar4	
2508		32			15			SW_Switch5			SW_Switch5			SW_Anahtar5				SW_Anahtar5	
2509		32			15			SW_Switch6			SW_Switch6			SW_Anahtar6				SW_Anahtar6	
2510		32			15			SW_Switch7			SW_Switch7			SW_Anahtar7				SW_Anahtar7	
2511		32			15			SW_Switch8			SW_Switch8			SW_Anahtar8				SW_Anahtar8	
2512		32			15			SW_Switch9			SW_Switch9			SW_Anahtar9				SW_Anahtar9	
2513		32			15			SW_Switch10			SW_Switch10			SW_Anahtar10			SW_Anahtar10	


3000	32		15		Digital Input 1			DI 1			DI 1				DI 1
3001	32		15		Digital Input 2			DI 2			DI 2				DI 2
3002	32		15		Digital Input 3			DI 3			DI 3				DI 3
3003	32		15		Digital Input 4			DI 4			DI 4				DI 4
3004	32		15		Digital Input 5			DI 5			DI 5				DI 5
3005	32		15		Digital Input 6			DI 6			DI 6				DI 6
3006	32		15		Digital Input 7			DI 7			DI 7				DI 7
3007	32		15		Digital Input 8			DI 8			DI 8				DI 8
3008	32		15		DI 1 Activation Level		DI 1 Act Lvl		DI 1 Aktivasyon Seviyesi	DI 1 Act.Sev.
3009	32		15		DI 2 Activation Level		DI 2 Act Lvl		DI 2 Aktivasyon Seviyesi	DI 2 Act.Sev.
3010	32		15		DI 3 Activation Level		DI 3 Act Lvl		DI 3 Aktivasyon Seviyesi	DI 3 Act.Sev.
3011	32		15		DI 4 Activation Level		DI 4 Act Lvl		DI 4 Aktivasyon Seviyesi	DI 4 Act.Sev.
3012	32		15		DI 5 Activation Level		DI 5 Act Lvl		DI 5 Aktivasyon Seviyesi	DI 5 Act.Sev.
3013	32		15		DI 6 Activation Level		DI 6 Act Lvl		DI 6 Aktivasyon Seviyesi	DI 6 Act.Sev.
3014	32		15		DI 7 Activation Level		DI 7 Act Lvl		DI 7 Aktivasyon Seviyesi	DI 7 Act.Sev.
3015	32		15		DI 8 Activation Level		DI 8 Act Lvl		DI 8 Aktivasyon Seviyesi	DI 8 Act.Sev.
3016	32		15		High				High			Yuksek				Yuksek
3017	32		15		Low				Low			Dusuk				Dusuk
3018	32		15		DI1 Alarm			DI1 Alarm		DI1 Alarm			DI1 Alarm
3019	32		15		DI2 Alarm			DI2 Alarm		DI2 Alarm			DI2 Alarm
3020	32		15		DI3 Alarm			DI3 Alarm		DI3 Alarm			DI3 Alarm
3021	32		15		DI4 Alarm			DI4 Alarm		DI4 Alarm			DI4 Alarm
3022	32		15		DI5 Alarm			DI5 Alarm		DI5 Alarm			DI5 Alarm
3023	32		15		DI6 Alarm			DI6 Alarm		DI6 Alarm			DI6 Alarm
3024	32		15		DI7 Alarm			DI7 Alarm		DI7 Alarm			DI7 Alarm
3025	32		15		DI8 Alarm			DI8 Alarm		DI8 Alarm			DI8 Alarm
3026	32		15		Relay Output 1			Relay Output 1		Role Cikis 1			Role Cikis 1
3027	32		15		Relay Output 2			Relay Output 2		Role Cikis 2			Role Cikis 2
3028	32		15		Relay Output 3			Relay Output 3		Role Cikis 3			Role Cikis 3
3029	32		15		Relay Output 4			Relay Output 4		Role Cikis 4			Role Cikis 4
3030	32		15		Relay Output 5			Relay Output 5		Role Cikis 5			Role Cikis 5
3031	32		15		Relay Output 6			Relay Output 6		Role Cikis 6			Role Cikis 6
3032	32		15		Relay Output 7			Relay Output 7		Role Cikis 7			Role Cikis 7
3033	32		15		Relay Output 8			Relay Output 8		Role Cikis 8			Role Cikis 8
3035	32		15		Battery Temperature		Batt Temp		Aku Sicakligi			Aku Sic
3036	32		15		Ambient Temperature		Amb Temp		Ortam Sicakligi			Ortam Sic
3037	32		15		ACUF AC Fail			ACUF AC Fail		ACUF AC Sebeke Arizasi		ACUF AC Seb Ariza

