#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
tr


[RES_INFO]																			
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE							
1		32			15			EIB Battery			EIB Battery		EIB Aku				EIB Aku			
2		32			15			Battery Current			Batt Current		Aku Akimi			Aku Akimi				
3		32			15			Battery Voltage			Batt Voltage		Aku Gerilimi			Aku Gerilimi			
4		32			15			Battery Capacity (Ah)		Batt Cap(Ah)		Aku Kapasitesi (Ah)		Aku Kap (Ah)				
5		32			15			Battery Capacity (%)		Batt Cap(%)		Aku Kapasitesi (%)		Aku Kap (%)					
6		32			15			Current Limit Exceeded		Curr Lmt Exceed		Akim Limiti Asildi		Akim Lim.Asildi						
7		32			15			Over Battery Current		Over Current		Yuksek Aku Akimi		Yuksek Akim				
8		32			15			Low Capacity			Low Capacity		Dusuk Kapasite			Dusuk Kapasite			
9		32			15			Yes				Yes			Evet				Evet
10		32			15			No				No			Hayir				Hayir
26		32			15			State				State			Durum				Durum
27		32			15			Battery Block 1 Voltage		Batt B1 Volt		Aku 1 Gerilimi			Aku 1 Gerilimi					
28		32			15			Battery Block 2 Voltage		Batt B2 Volt		Aku 2 Gerilimi			Aku 2 Gerilimi					
29		32			15			Battery Block 3 Voltage		Batt B3 Volt		Aku 3 Gerilimi			Aku 3 Gerilimi					
30		32			15			Battery Block 4 Voltage		Batt B4 Volt		Aku 4 Gerilimi			Aku 4 Gerilimi					
31		32			15			Battery Block 5 Voltage		Batt B5 Volt		Aku 5 Gerilimi			Aku 5 Gerilimi					
32		32			15			Battery Block 6 Voltage		Batt B6 Volt		Aku 6 Gerilimi			Aku 6 Gerilimi					
33		32			15			Battery Block 7 Voltage		Batt B7 Volt		Aku 7 Gerilimi			Aku 7 Gerilimi					
34		32			15			Battery Block 8 Voltage		Batt B8 Volt		Aku 8 Gerilimi			Aku 8 Gerilimi					
35		32			15			Battery Management		Batt Manage		Aku Yonetimi			Aku Yonetimi					
36		32			15			Enable				Enable			Aktif				Aktif
37		32			15			Disable				Disable			Pasif				Pasif
38		32			15			Failure				Failure			Hata				Hata
39		32			15			Shunt Full Current		Shunt Current		Sont Tam Akim			Sont Akim				
40		32			15			Shunt Full Voltage		Shunt Voltage		Sont Tam Gerilim		Sont Gerilim				
41		32			15			On				On			Acik				Acik	
42		32			15			Off				Off			Kapali				Kapali
43		32			15			Failure				Failure			Hata				Hata
44		32			15			Used Temperature Sensor		Used Sensor		Kullanilan Sicaklik Sensoru	Sic. Sensoru					
87		32			15			None				None			Hicbiri				Hicbiri
91		32			15			Temperature Sensor 1		Temp Sensor 1		Sicaklik Sensoru 1		Sic. Sensoru 1					
92		32			15			Temperature Sensor 2		Temp Sensor 2		Sicaklik Sensoru 2		Sic. Sensoru 2					
93		32			15			Temperature Sensor 3		Temp Sensor 3		Sicaklik Sensoru 3		Sic. Sensoru 3					
94		32			15			Temperature Sensor 4		Temp Sensor 4		Sicaklik Sensoru 4		Sic. Sensoru 4					
95		32			15			Temperature Sensor 5		Temp Sensor 5		Sicaklik Sensoru 5		Sic. Sensoru 5					
96		32			15			Rated Capacity			Rated Capacity		Hesaplanan Kapasite		Kapasite K10				
97		32			15			Battery Temperature		Battery Temp		Aku Sicakligi			Aku Sic.					
98		32			15			Battery Temperature Sensor	BattTempSensor		Aku Sicaklik Sensoru		Aku Sic Sensoru							
99		32			15			None				None			Hicbiri				Hicbiri
100		32			15			Temperature 1			Temp 1			Sicaklik 1			Sic 1		
101		32			15			Temperature 2			Temp 2			Sicaklik 2			Sic 2		
102		32			15			Temperature 3			Temp 3			Sicaklik 3			Sic 3		
103		32			15			Temperature 4			Temp 4			Sicaklik 4			Sic 4		
104		32			15			Temperature 5			Temp 5			Sicaklik 5			Sic 5		
105		32			15			Temperature 6			Temp 6			Sicaklik 6			Sic 6		
106		32			15			Temperature 7			Temp 7			Sicaklik 7			Sic 7		
107		32			15			Temperature 8			Temp 8			Sicaklik 8			Sic 8		
108		32			15			Temperature 9			Temp 9			Sicaklik 9			Sic 9		
109		32			15			Temperature 10			Temp 10			Sicaklik 10			Sic 10		
110		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		Battery Current Imbalance Alarm		BattCurrImbalan	
111		32			15			EIB1 Battery2				EIB1 Battery2				EIB1 Battery2				EIB1 Battery2
112		32			15			EIB1 Battery3				EIB1 Battery3				EIB1 Battery3				EIB1 Battery3
113		32			15			EIB2 Battery1				EIB2 Battery1				EIB2 Battery1				EIB2 Battery1
114		32			15			EIB2 Battery2				EIB2 Battery2				EIB2 Battery2				EIB2 Battery2
115		32			15			EIB2 Battery3				EIB2 Battery3				EIB2 Battery3				EIB2 Battery3
116		32			15			EIB3 Battery1				EIB3 Battery1				EIB3 Battery1				EIB3 Battery1
117		32			15			EIB3 Battery2				EIB3 Battery2				EIB3 Battery2				EIB3 Battery2
118		32			15			EIB3 Battery3				EIB3 Battery3				EIB3 Battery3				EIB3 Battery3
119		32			15			EIB4 Battery1				EIB4 Battery1				EIB4 Battery1				EIB4 Battery1
120		32			15			EIB4 Battery2				EIB4 Battery2				EIB4 Battery2				EIB4 Battery2
121		32			15			EIB4 Battery3				EIB4 Battery3				EIB4 Battery3				EIB4 Battery3
122		32			15			EIB1 Battery1				EIB1 Battery1				EIB1 Battery1				EIB1 Battery1

150		32			15		Battery 1					Batt 1		Battery 1					Batt 1
151		32			15		Battery 2					Batt 2		Battery 2					Batt 2
152		32			15		Battery 3					Batt 3		Battery 3					Batt 3
