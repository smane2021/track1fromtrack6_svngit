
#  Web Private Configuration File, Version 1.00
#                All rights reserved.
# Copyright(c) 2021, Vertiv Tech Co., Ltd.
#
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
#Standard mandatory section

[LOCAL_LANGUAGE]
tr

[LOCAL_LANGUAGE_VERSION]
1.00


#Define the web pages's resource file
#The field of Default Name is the english language
#The field of Local Name must be transfered  the local language according to the Default Name
#Define web pages number
[NUM_OF_PAGES]
82

#WebPages Resource
[control_cgi.htm:Number]
0

[control_cgi.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[data_sampler.htm:Number]
0

[data_sampler.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE


[dialog.htm:Number]
6

[dialog.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE		Description
1		ID_OK			32			N/A			OK			N/A		OK			N/A			NA
2		ID_CANCEL		32			N/A			Cancel			N/A		Iptal			N/A			NA
3		ID_SIGNAL_NAME		32			N/A			Name			N/A		Isim			N/A			NA
4		ID_SAMPLER		32			N/A			Status			N/A		Durum			N/A			NA
5		ID_SAMPLE_CHANNEL	32			N/A			Status Channel		N/A		Durum Kanali		N/A			NA
6		ID_SIGNAL_NEW_NAME	32			N/A			New Name		N/A		Yeni Isim		N/A			NA
	

[editsignalname.js:Number]
0

[editsignalname.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[equip_data.htm:Number]
12

[equip_data.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE			 ABBR_IN_LOCALE
1		ID_TIPS0	32			N/A	Failed to communicate with 				N/A		 	Baglanti Hatasi											N/A
2		ID_TIPS1	64			N/A	[OK]Reconnect. [Cancel]Stop exploring		N/A		[OK]Yeniden Baglandi.[Iptal]Taramayi Durdur							N/A
3		ID_TIPS2	64			N/A	Failed to communicate with the application	N/A		Uygulamaya Baglanti Hatasi									N/A
4		ID_TIPS3	128			N/A	Number of rectifiers has changed, the pages will be refreshed.	N/A	Dogrultucu sayisi degisti, sayfa yenilenecek.						N/A
5		ID_TIPS4	128			N/A	System Configuration has changed, the pages will be refreshed.				N/A		Sistem Konfigurasyonu degisti, sayfa yenilenecek.						N/A
6		ID_TIPS5	128			N/A	Number of DC Distributions has changed, the pages will be refreshed.			N/A		DC Dagitim sayisi degisti, sayfa yenilenecek.							N/A
7		ID_TIPS6	128			N/A	Number of batteries has changed, the pages will be refreshed.				N/A		Aku sayisi degisti, sayfa yenilenecek.								N/A
8		ID_TIPS8	256			N/A			Monitoring is in Auto Configuration Process. Please wait a moment (About 1 minute).	N/A		Goruntuleme Oto Konfigurasyon Surecinde Lutfen bekleyin. (Yaklasik 1dk)				N/A
9		ID_TIPS9	256			N/A			System Configuration has changed, the pages will be refreshed.				N/A		Sistem Konfigurasyonu degisti, Sayfa yenilenecek.						N/A
10		ID_TIPS10	256			N/A			Number of converters has changed, the pages will be refreshed.				N/A		Konverter sayisi degisti, sayfa yenilenecek.							N/A
11		ID_TIPS11	256			N/A			Controller in System Expansion Secondary Mode.						N/A		Denetci Sistem Genisletme Ikincil Modunda							N/A
12		ID_TIPS12	256			N/A			System configuration changed. The pages will be refreshed.				N/A		Sistem Konfigurasyonu degisti, sayfa yenilenecek.						N/A



[j01_tree_maker.js:Number]
1

[j01_tree_maker.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_DIR		32			N/A			/cgi-bin/eng/				N/A		/cgi-bin/loc/		N/A

[j02_tree_view.js:Number]
35

[j02_tree_view.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE				ABBR_IN_LOCALE
1		ID_SCUP			16		N/A			System					N/A		Sistem					N/A
2		ID_DEVICE_EXPLORE	32		N/A			DEVICES					N/A		CIHAZLAR				N/A
3		ID_SYSTEM		16		N/A			MAINTENANCE				N/A		BAKIM					N/A
4		ID_NETWORK_SETTING	32		N/A			Network Configuration			N/A		Network Konfigurasyon			N/A
5		ID_NMS_SETTING		32		N/A			NMS Configuration			N/A		NMS Konfigurasyon			N/A
6		ID_ESR_SETTING		32		N/A			MC Configuration			N/A		MC Konfigurasyon			N/A
7		ID_USER			64		N/A			User Configuration			N/A		Kullanici Konfigurasyon			N/A
8		ID_MAINTENANCE		16		N/A			CONFIGURATION				N/A		KONFIGURASYON				N/A
9		ID_FILE_MANAGE		32		N/A			Upload/Download				N/A		Yukle/Indir				N/A
10		ID_MODIFY_CFG		64		N/A			Site Information Modification		N/A		Saha Bilgisi Degistirme			N/A
11		ID_TIME_CFG		64		N/A			Time Settings				N/A		Zaman Ayarlari				N/A
12		ID_QUERY		16		N/A			QUERY					N/A		SORGU					N/A
13		ID_SITEMAP		16		N/A			Site Map				N/A		Saha Haritasi				N/A
14		ID_ALARM		32		N/A			ALARMS					N/A		ALARMLAR				N/A
15		ID_ACTIVE_ALARM		32		N/A			Active					N/A		Aktif					N/A
16		ID_HISTORY_ALARM	32		N/A			Alarm History				N/A		Alarm Gecmisi				N/A
17		ID_QUERY_HIS_DATA	32		N/A			Data History				N/A		Veri Gecmisi				N/A
18		ID_QUERY_LOG_DATA	32		N/A			System Log				N/A		Sistem Kaydi				N/A
19		ID_QUERY_BATT_DATA	32		N/A			Battery Test Log			N/A		Aku Test Kaydi				N/A
20		ID_CLEAR_DATA		32		N/A			Clear Data				N/A		Kayit Sil				N/A
21		ID_RESTORE_DEFAULT	32		N/A			Restore Default				N/A		Varsayilani Geri Yukle			N/A
22		ID_PRODUCT_INFO		32		N/A			Site Inventory				N/A		Saha Envanteri				N/A
23		ID_EDIT_CFGFILE		64		N/A			Configuration Info Modification		N/A		Konfig Bilgisi Degistirme		N/A
24		ID_YDN23_SETTING	32		N/A			MC Configuration			N/A		MC Konfigurasyon			N/A
25		ID_MODIFY_CFG1		64		N/A			Site Information Modification		N/A		Saha Bilgisi Degistirme			N/A
26		ID_MODIFY_CFG2		64		N/A			Equipment Information Modification	N/A		Donanim Bilgisi Degistirme		N/A
27		ID_MODIFY_CFG3		64		N/A			Signal Information Modification		N/A		Sinyal Bilgisi Degistirme		N/A
28		ID_EDIT_CFGFILE1	64		N/A			Configuration Of Alarm Suppression	N/A		Alarm Bastirma Konfigurasyonu		N/A
29		ID_EDIT_CFGFILE2	64		N/A			Configuration Of Alarm Relays		N/A		Alarm Role Konfigurasyonu		N/A
30		ID_EDIT_CFGFILE3	64		N/A			Configuration Of PLC			N/A		PLC Konfigurasyonu			N/A
31		ID_USER_DEF_PAGES	64		N/A			QUICK SETTINGS				N/A		HIZLI AYARLAR				N/A
32		ID_EDIT_CFGGCPS		32		N/A			Power Split				N/A		Guc Paylasimi				N/A
33		ID_GET_PARAM		32		N/A			Get Parameter Settings			N/A		Parametre Ayarlarini Al			N/A
34		ID_AUTO_CONFIG		32		N/A			Auto Configuration			N/A		Oto Konfigurasyon			N/A
35		ID_SYS_STATUS		32		N/A			SYSTEM STATUS				N/A		SISTEM DURUMU				N/A



[j04_gfunc_def.js:Number]
0

[j04_gfunc_def.js]
#Sequence ID	RES_ID		    MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[j05_signal_def.js:Number]
0

[j05_signal_def.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[j07_equiplist_def.js:Number]
0

[j07_equiplist_def.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[j31_menu_script.js:Number]
0

[j31_menu_script.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[j31_table_script.js:Number]
0


[j31_table_script.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[j50_event_log.js:Number]
0

[j50_event_log.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[j76_device_def.htm:Number]
0

[j76_device_def.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[j77_equiplist_def.htm:Number]
0

[j77_equiplist_def.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[login.htm:Number]
40

[login.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN	FULL_IN_LOCALE				ABBR_IN_LOCALE
1		ID_USER_NAME		8			N/A			User				N/A		Kullanici				N/A
2		ID_PASSWORD		16			N/A			Password			N/A		Sifre					N/A
3		ID_CANCEL		16			N/A			Cancel				N/A		Iptal					N/A
4		ID_OK			8			N/A			OK				N/A		OK					N/A
5		ID_LOGIN		16			N/A			Login				N/A		Giris					N/A
6		ID_TIPS0		64			N/A			Incorrect user or password.	N/A		Hatali Kullanici yada Sifre		N/A
7		ID_TIPS1		64			N/A			Incorrect user or password.	N/A		Hatali Kullanici yada Sifre		N/A
8		ID_TIPS2		64			N/A			Your browser isn't supported.	N/A		WEB Tarayicisi Desteklenmiyor.		N/A
9		ID_TIPS3		64			N/A		The cookie was blocked, you may not explore the pages normally.							N/A	Cerez engellendi, sayfalari acamayabilirsin.			N/A
10		ID_TIPS4		128			N/A		The system supports Microsoft IE 5.5 or above. \nYour browser is not fully supported. Please update.		N/A	Sistem Microsoft IE 5.5 veya uzerini destekler.\nTarayiciniz tam olarak desteklenmiyor. Guncelleyin.		N/A
11		ID_ERROR0		64 			N/A			Unknown error			N/A		Bilinmeyen Hata				N/A
12		ID_ERROR1		64			N/A			Success				N/A		Basarili				N/A
13		ID_ERROR2		64			N/A			Incorrect user or password.	N/A		Hatali Kullanici ya da Sifre		N/A
14		ID_ERROR3		64			N/A			Incorrect user or password.	N/A		Hatali Kullanici ya da Sifre		N/A
15		ID_ERROR4		64			N/A			Failed to communicate with the application.	N/A		Baglanti Hatasi		N/A
16		ID_ERROR5		64			N/A			Over 5 connections, please wait.	N/A	5 uzeri baglanti, Lutfen bekleyiniz	N/A
17		ID_ENGLISH_VERSION	16			N/A			English				N/A		English					N/A
18		ID_LOCAL_VERSION	16			N/A			Turkish				N/A		Turkce					N/A
19		ID_SERIAL_NUMBER	20			N/A			ACU+ Serial Number		N/A		ACU Seri No				N/A
20		ID_HARDWARE_VERSION	32			N/A			Hardware Version		N/A		Hardware Versiyon			N/A
21		ID_SOFTWARE_VERSION	32			N/A			Software Version		N/A		Software Versiyon			N/A
22		ID_PRODUCT_NUM		32			N/A			Product Model			N/A		Urun Modeli					N/A
23		ID_PRO_NUMBER		32			N/A			Controller			N/A		Denetci					N/A
24		ID_CFG_VERSION		22			N/A			Configuration Version		N/A		Konfigurasyon Versiyon			N/A
25		ID_LOCAL2_VERSION	16			N/A			Local 2				N/A		Yerel 2					N/A
26		ID_ERROR6		128			N/A			Controller is starting up, \nplease wait.	N/A	Denetci Basliyor, Lutfen bekleyin	N/A
27		ID_OPT_RESOLUTION	32			N/A			Optimal Resolution		N/A		En Uygun Cozum				N/A
28		ID_ERROR7		256			N/A			Monitoring is in Auto Configuration Process. Please wait a moment (About 1 minute).		N/A	Goruntuleme Oto Konfigurasyon Surecinde Lutfen bekleyin. (Yaklasik 1dk)	N/A
29		ID_ERROR8		64			N/A			Controller in System Expansion Secondary Mode.						N/A		Denetci Sistem Genisletme ikincil Modunda			N/A
30		ID_LOGIN		64			N/A			Login				N/A		Giris					N/A
31		ID_SITENAME		64			N/A			Site Name			N/A		Saha Adi				N/A
32		ID_SYSTEMNAME		64			N/A			System Name			N/A		Sistem Adi				N/A
33		ID_TEXT1		128			N/A			You are requesting access	N/A		Giris Talep Ediyorsunuz			N/A
34		ID_TEXT2		64			N/A			located at			N/A		yerinde					N/A
35		ID_TEXT3		256			N/A			The user name and password for this device is set by the system administrator		N/A		Bu Cihaz icin Kullanici Adi ve Sifre Admin Tarafindan ayarlanir		N/A
36		ID_LOGIN		64			N/A			Login				N/A		Giris					N/A
37		ID_CSSDIR		32			N/A			/cgi-bin/eng			N/A		/cgi-bin/loc				N/A
38		ID_CSSDIR2		32			N/A			/cgi-bin/eng			N/A		/cgi-bin/loc				N/A
39		ID_CSSDIR3		32			N/A			/cgi-bin/eng			N/A		/cgi-bin/loc				N/A
40		ID_LOGIN2		64			N/A			Login				N/A		Giris					N/A

[p01_main_frame.htm:Number]
2

[p01_main_frame.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_EXPLORE	32			16			Controller Web Explorer	N/A			Denetci WEB Tarayicisi		N/A
2		ID_SITE		16			16			Site			N/A			Yer				N/A


[p02_tree_view.htm:Number]
15

[p02_tree_view.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN						ABBR_IN_EN	FULL_IN_LOCALE						ABBR_IN_LOCALE
1		ID_TIPS0		64			N/A			This page was locked, please login again.		N/A		Bu sayfa kilitli tekrar giris yapin			N/A
2		ID_LANGUAGE_VERSION	64			N/A			Local Web Pages Link					N/A		Yerel WEB Sayfasi Linki					N/A
3		ID_SITE_MAP		64			N/A			Site Map						N/A		Saha Haritasi						N/A
4		ID_ALARM		128			N/A			Please wait for the requested page to be loaded.	N/A		Sayfa yuklenirken lutfen bekleyiniz.			N/A
5		ID_SYS_STATUS		32			N/A			System Status						N/A		Sistem Durumu						N/A
6		ID_SERIAL_NUMBER	32			N/A			Serial Number						N/A		Seri No							N/A
7		ID_HARDWARE_VERSION	32			N/A			Hardware Version					N/A		Donanm Versiyonu					N/A
8		ID_SOFTWARE_VERSION	32			N/A			Software Version					N/A		Yazilim Versiyonu					N/A
9		ID_PRODUCT_NUM		32			N/A			Product Model						N/A		Model							N/A
10		ID_CFG_VERSION		32			N/A			Configuration Version					N/A		Konfigurasyon						N/A
11		ID_SYSTEMNAME		64			N/A			System Name						N/A		Sistem Adi						N/A
12		ID_RECTNAME		32			N/A			Rectifiers						N/A		Dogrultucular						N/A
13		ID_RECTNAME2		32			N/A			Rectifiers						N/A		Dogrultucular						N/A
14		ID_SYS_SPEC		64			N/A			System Specifications					N/A		Sistem Ozellikleri					N/A
15		ID_CONTROL_SPEC		64			N/A			Controller Specifications				N/A		Denetci Ozellikleri					N/A


[p03_main_menu.htm:Number]
0

[p03_main_menu.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE


[p04_main_title.htm:Number]
3

[p04_main_title.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_SAMPLE	16			N/A			STATUS		N/A		DURUM			N/A
2		ID_CONTROL	16			N/A			CONTROL		N/A		KONTROL			N/A
3		ID_SETTING	16			N/A			SETTINGS	N/A		AYARLAR			N/A



[p05_equip_sample.htm:Number]
8


[p05_equip_sample.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL		MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_INDEX		16				N/A			Index		N/A		Icerik			N/A
2		ID_SIGNAL_NAME		16				N/A			Name		N/A		Isim			N/A
3		ID_SIGNAL_VALUE		16				N/A			Value		N/A		Deger			N/A
4		ID_SIGNAL_UNIT		16				N/A			Unit		N/A		Birim			N/A
5		ID_SAMPLE_TIME		16				N/A			Time		N/A		Zaman			N/A
6		ID_SAMPLER		16				N/A			Status		N/A		Durum			N/A
7		ID_CHANNEL		16				N/A			Channel		N/A		Kanal			N/A
8		ID_NO_SIG		64				N/A		There is no Status signal in this equipment.	N/A	Cihazda durum sinyali yok.		N/A

[p06_equip_control.htm:Number]
24

[p06_equip_control.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN	FULL_IN_LOCALE					ABBR_IN_LOCALE
1		ID_INDEX		16			N/A			Index				N/A		Icerik						N/A
2		ID_SIGNAL_NAME		16			N/A			Name				N/A		Isim						N/A
3		ID_SIGNAL_VALUE		16			N/A			Value				N/A		Deger						N/A
4		ID_SIGNAL_UNIT		16			N/A			Unit				N/A		Birim						N/A
5		ID_SAMPLE_TIME		16			N/A			Time				N/A		Zaman						N/A
6		ID_SET_VALUE		16			N/A			Set value			N/A		Ayar Degeri					N/A
7		ID_SET			16			N/A			Set				N/A		Ayarla						N/A
8		ID_ERROR0		32			N/A			Failure.			N/A		Hata						N/A
9		ID_ERROR1		32			N/A			Success.			N/A		Basarili					N/A
10		ID_ERROR2		64			N/A			Failure, conflict in settings.		N/A	Hata. Ayarlar celiskili.				N/A
11		ID_ERROR3		32			N/A			Failure, you are not authorized.	N/A	Hata. Yetkiniz Yok.			N/A
12		ID_ERROR4		64			N/A			No information to send.		N/A		Gonderilecek bilgi yok.				N/A
13		ID_ERROR5		64			N/A			Failure, Controller is hardware protected.	N/A	Hata. Denetci HW korumali		N/A
14		ID_SET_TYPE		16			N/A			Set				N/A		Ayarla						N/A
15		ID_SHOW_TIPS0		64			N/A			Greater than the max value.	N/A		Max degerden buyuk				N/A
16		ID_SHOW_TIPS1		64			N/A			Less than the min value.	N/A		Min. degerden kucuk				N/A
17		ID_SHOW_TIPS2		64			N/A			Can't be empty.			N/A		Bos birakilamaz.					N/A
18		ID_SHOW_TIPS3		64			N/A			Enter a number please.		N/A		Lutfen sayi girin.			N/A
19		ID_SHOW_TIPS4		64			N/A			Are you sure?			N/A		Emin misiniz?					N/A
20		ID_SHOW_TIPS5		64			N/A			Failure, you are not authorized.	N/A	Hata. Yetkiniz Yok!				N/A
21		ID_TIPS1		64			N/A			Send control			N/A		Kontrol Gonder					N/A
22		ID_SAMPLER		16			N/A			Status				N/A		Durum						N/A
23		ID_CHANNEL		16			N/A			Channel				N/A		Kanal						N/A
24		ID_NO_SIG		64			N/A			There is no Control signal in this equipment.	N/A	Bu cihazda kontrol sinyali yok.		N/A

[p07_equip_setting.htm:Number]
29


[p07_equip_setting.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN	FULL_IN_LOCALE						ABBR_IN_LOCALE
1		ID_INDEX		16			N/A			Index						N/A		Icerik							N/A
2		ID_SIGNAL_NAME		16			N/A			Name						N/A		Isim							N/A
3		ID_SIGNAL_VALUE		16			N/A			Value						N/A		Deger							N/A
4		ID_SIGNAL_UNIT		16			N/A			Unit						N/A		Birim							N/A
5		ID_SAMPLE_TIME		16			N/A			Time						N/A		Zaman							N/A
6		ID_SET_VALUE		16			N/A			Set Value					N/A		Deger Ayarla						N/A
7		ID_SET			16			N/A			Set						N/A		Ayarla 							N/A
8		ID_ERROR0		32			N/A			Failure.					N/A		Hata							N/A
9		ID_ERROR1		32			N/A			Success.					N/A		Basarili						N/A
10		ID_ERROR2		64			N/A			Failure, conflict in setting.			N/A		Hata. Ayarlar celiskili.					N/A
11		ID_ERROR3		32			N/A			Failure, you are not authorized.		N/A		Hata. Yetkiniz Yok.					N/A
12		ID_ERROR4		64			N/A			No information to send.				N/A		Gonderilecek bilgi yok.					N/A
13		ID_ERROR5		128			N/A			Failure, Controller is hardware protected	N/A		Hata. Denetci HW korumali				N/A
14		ID_SET_TYPE		16			N/A			Set						N/A		Ayar							N/A
15		ID_SHOW_TIPS0		64			N/A			Greater than the max value:			N/A		Max. degerden buyuk					N/A
16		ID_SHOW_TIPS1		64			N/A			Less than the min value:			N/A		min. degerden kucuk					N/A
17		ID_SHOW_TIPS2		64			N/A			Can't be null.					N/A		Bos birakilamaz.						N/A
18		ID_SHOW_TIPS3		64			N/A			Input number please.				N/A		Kontrol degeri son degere esit.				N/A
19		ID_SHOW_TIPS4		64			N/A			The control value is equal to the last value.	N/A		Kontrol degeri eski degerle ayni.			N/A
20		ID_SHOW_TIPS5		64			N/A			Failure, you are not authorized.		N/A		Hata. Yetkiniz Yok					N/A
21		ID_TIPS1		64			N/A			Send setting					N/A		Ayarlari Gonder						N/A
22		ID_SAMPLER		16			N/A			Status						N/A		Durum							N/A
23		ID_CHANNEL		16			N/A			Channel						N/A		Kanal							N/A
24		ID_MONTH_ERROR		32			N/A			Incorrect month.				N/A		Yanlis Ay						N/A
25		ID_DAY_ERROR		32			N/A			Incorrect day.					N/A		Yanlis Gun						N/A
26		ID_HOUR_ERROR		32			N/A			Incorrect hour.					N/A		Yanlis Saat						N/A
27		ID_FORMAT_ERROR		64			N/A			Incorrect format, should be			N/A		Yanlis format				N/A
28		ID_RELOAD_PAGE		64			N/A			./eng/p07_equip_setting.htm			N/A		./loc/p07_equip_setting.htm				N/A
29		ID_NO_SIG		64			N/A			There is no Setting signal in this equipment.	N/A		Bu cihazda kontrol sinyali yok				N/A

[p08_alarm_frame.htm:Number]
1

[p08_alarm_frame.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_DIR		32			N/A			/cgi-bin/eng/				N/A		/cgi-bin/loc/		N/A

[p09_alarm_title.htm:Number]
9

[p09_alarm_title.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_ALL_ALARM		16			N/A			All Alarms	N/A		Butun Alarmlar		N/A
2		ID_OBSERVATION_ALARM	16			N/A			Minor		N/A		Minor			N/A
3		ID_MAJOR_ALARM		16			N/A			Major		N/A		Major			N/A
4		ID_CRITICAL_ALARM	16			N/A			Critical	N/A		Kritik			N/A
5		ID_AUTO			16			N/A			Auto Popup	N/A		Oto Pop-up		N/A
6		ID_ALL_ALARM1		16			N/A			All Alarms	N/A		Butun Alarmlar		N/A
7		ID_OBSERVATION_ALARM1	16			N/A			Minor		N/A		Minor			N/A
8		ID_MAJOR_ALARM1		16			N/A			Major		N/A		Major			N/A
9		ID_CRITICAL_ALARM1	16			N/A			Critical	N/A		Kritik			N/A

[p10_alarm_show.htm:Number]
8

[p10_alarm_show.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_INDEX		16			N/A			Index				N/A		Icerik			N/A
2		ID_SIGNAL_NAME		16			N/A			Name				N/A		Isim			N/A
3		ID_ALARM_LEVEL		16			N/A			Alarm Level			N/A		Alarm Seviyesi		N/A
4		ID_SAMPLE_TIME		16			N/A			Time				N/A		Zaman			N/A
5		ID_RELATIVE_DEVICE	16			N/A			Device				N/A		Cihaz			N/A
6		ID_OA			16			N/A			MI				N/A		Minor			N/A
7		ID_MA			16			N/A			MA				N/A		Major			N/A
8		ID_CA			16			N/A			CA				N/A		Kritik			N/A

[p11_network_config.htm:Number]
21

#In ID_TIPS1,ID_TIPS2, ID_TIPS4, the '\n' and 'nnn.nnn.nnn.nnn','10.75.14.171' is about the format of show pages, so that you don't need to tranfer it.
[p11_network_config.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN											ABBR_IN_EN	FULL_IN_LOCALE										ABBR_IN_LOCALE
1		ID_SCUP_IP		32			N/A			Controller Network IP										N/A		Denetci Network IP									N/A
2		ID_SCUP_MASK		8			N/A			Mask												N/A		Mask											N/A
3		ID_SCUP_GATEWAY		8			N/A			Gateway												N/A		Ag Gecidi										N/A
4		ID_SAVE_PARAMETER	16			N/A			Save Parameter											N/A		Parametre Kaydet											N/A
5		ID_SCUP_NETWORK_HEAD	32			N/A			Controller network parameter settings.								N/A		Denetci Network Parametre Ayarlari							N/A
6		ID_ERROR0		32			N/A			Unknown error.											N/A		Bilinmeyen Hata										N/A
7		ID_ERROR1		32			N/A			Success, Controller is rebooting.								N/A		Basarili. Denetci yeniden baslatiliyor.							N/A
8		ID_ERROR2		32			N/A			Failure, incorrect input.									N/A		Hata. Yanlis giris.									N/A
9		ID_ERROR3		32			N/A			Failure, incomplete information.								N/A		Hata. Eksik bilgi.									N/A
10		ID_ERROR4		32			N/A			Failure, you are not authorized.								N/A		Hata. Yetkiniz yok.									N/A
11		ID_ERROR5		32			N/A			Failure, Controller is hardware protected							N/A		Hata. Denetci HW korumali.								N/A
12		ID_TIPS0		32			N/A			Set network parameter										N/A		Network parametrelerini ayarla								N/A
13		ID_TIPS1		128			N/A			IP address error.\nFormat shall be 'nnn.nnn.nnn.nnn', e.g. 10.75.14.171				N/A		IP Adresi Hatali!\n Format 'nnn.nnn.nnn.nnn', Orn. 10.75.14.171				N/A
14		ID_TIPS2		128			N/A			IP mask error.\nFormat shall be'nnn.nnn.nnn.nnn', e.g. 255.255.0.0				N/A		IP Mask Hatali \nFormat:'nnn.nnn.nnn.nnn', orn. 255.255.0.0				N/A
15		ID_TIPS3		32			N/A			IP & Mask error.										N/A		IP & Mask Hatali									N/A
16		ID_TIPS4		128			N/A			Gateway IP error.\nCorrect shall be'nnn.nnn.nnn.nnn' e.g. 10.75.14.254, 0.0.0.0 as no gateway	N/A		Ag Gecidi Hatali \nFormat: 'nnn.nnn.nnn.nnn' Orn. 10.75.14.254, 0.0.0.0 sin gateway	N/A
17		ID_TIPS5		64			N/A			IP, gateway and mask mismatch. Try again.							N/A		IP & Gateway Eslesmiyor. Tekrar deneyin.							N/A
18		ID_TIPS6		64			N/A			Please wait, Controller is rebooting...								N/A		Lutfen bekleyin. Denetci yeniden baslatiliyor.						N/A
19		ID_TIPS7		64			N/A			Since parameters have been modified, Controller is rebooting...					N/A		Parametreler degistirilmis. Denetci yeniden baslatiliyor.							N/A
20		ID_TIPS8		64			N/A			Controller card homepage will be refreshed after.						N/A		Denetci anasayfasi sonra yenilenecek.							N/A
21		ID_ERROR6		32			N/A			Failure, DHCP is ON.										N/A		Hata. DHCP aktif.									N/A


[p12_nms_config.htm:Number]
40

[p12_nms_config.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL		MAX_LEN_OF_BYTE_ABBR		FULL_IN_EN										ABBR_IN_EN		FULL_IN_LOCALE					ABBR_IN_LOCALE
1		ID_ERROR0		16				N/A				Unknown error.										N/A			Bilinmeyen Hata.				N/A
2		ID_ERROR1		64				N/A				Failure, the NMS already exists.							N/A			Hata. NMS zaten mevcut.				N/A
3		ID_ERROR2		32				N/A				Success											N/A			Basarili					N/A
4		ID_ERROR3		64				N/A				Failure, incomplete information.							N/A			Hata, Eksik bilgi.				N/A
5		ID_ERROR4		64				N/A				Failure, you are not authorized.							N/A			Hata. Yetkiniz yok.				N/A
6		ID_ERROR5		64				N/A				Controller is hardware protected, can't be modified.					N/A			Denetci HW korumali, modifiye edilemez		N/A
7		ID_ERROR6		64				N/A				Failure, the maximum number is exceeded.						N/A			Hata: Max. Numara Asildi.			N/A
8		ID_NMS_HEAD1		32				N/A				NMS Configuration									N/A			NMS Konfigurasyon				N/A
9		ID_NMS_HEAD2		32				N/A				Current NMS										N/A			Suanki NMS					N/A
10		ID_NMS_IP		16				N/A				NMS IP											N/A			IP NMS						N/A
11		ID_NMS_AUTHORITY	16				N/A				Authority										N/A			Yetki						N/A
12		ID_NMS_TRAP		32				N/A				Accepted Trap Level									N/A			Kabul edilmis Trap Seviyesi			N/A
13		ID_NMS_IP		16				N/A				NMS IP											N/A			IP NMS						N/A
14		ID_NMS_AUTHORITY	16				N/A				Authority										N/A			Yetki						N/A
15		ID_NMS_TRAP		32				N/A				Accepted Trap Level									N/A			Kabul edilmis Trap Seviyesi			N/A
16		ID_NMS_ADD		16				N/A				Add New NMS										N/A			Yeni NMS Ekle					N/A
17		ID_NMS_MODIFY		32				N/A				Modify NMS										N/A			NMS Degistir					N/A
18		ID_NMS_DELETE		32				N/A				Delete NMS										N/A			NMS Sil					N/A
19		ID_NMS_PUBLIC		32				N/A				Public Community									N/A			Genel Topluluk					N/A
20		ID_NMS_PRIVATE		32				N/A				Private Community									N/A			Ozel Topluluk					N/A
21		ID_NMS_LEVEL0		16				N/A				Not Used										N/A			Kullanilmiyor					N/A
22		ID_NMS_LEVEL1		16				N/A				No Access										N/A			Erisim Yok					N/A
23		ID_NMS_LEVEL2		32				N/A				Query Authority										N/A			Yetki Sorgusu					N/A
24		ID_NMS_LEVEL3		32				N/A				Control Authority									N/A			Yetki Kontrol					N/A
25		ID_NMS_LEVEL4		32				N/A				Administrator										N/A			Administrador					N/A
26		ID_NMS_TRAP_LEVEL0	16				N/A				Not Used										N/A			Kullanilmiyor					N/A
27		ID_NMS_TRAP_LEVEL1	16				N/A				All Alarms										N/A			Butun Alarmlar					N/A
28		ID_NMS_TRAP_LEVEL2	16				N/A				Major Alarms										N/A			Major Alarmlar					N/A
29		ID_NMS_TRAP_LEVEL3	16				N/A				Critical Alarms										N/A			Kritik Alamlar					N/A
30		ID_NMS_TRAP_LEVEL4	16				N/A				No traps										N/A			Trap Yok					N/A
31		ID_TIPS0		128				N/A				Incorrect IP address of NMS.\nFormat should be 'nnn.nnn.nnn.nnn',\ne.g.,10.76.8.29	N/A			NMS IP Mask Hatali \nFormat:'nnn.nnn.nnn.nnn', orn. 10.76.8.29	N/A
32		ID_TIPS1		128				N/A				Public Community and Private Community can't be empty. Please try again.		N/A			Ozel Topluluk ve Genel Topluluk bos birakilamaz. Lutfen terar deneyin.	N/A
33		ID_TIPS2		128				N/A				already exists, please try again.							N/A			Mevcutta Var. Tekrar deneyiniz						N/A
34		ID_TIPS3		128				N/A				does not exist,so the password can't be modified, please try again.			N/A			Mevcut degil. Sifre modifiye edilemiyor.Tekrar deneyiniz.		N/A
35		ID_TIPS4		128				N/A				Please select one or more NMS before clicking this button.				N/A			Butona basmadan once 1 veya daha cok NMS secin.			N/A
36		ID_TIPS5		128				N/A				NMS Info Configuration									N/A			NMS bilgi konfigurasyon							N/A
37		ID_NMS_PUBLIC		128				N/A				Public Community									N/A			Genel Topluluk								N/A
38		ID_NMS_PRIVATE		128				N/A				Private Community									N/A			Ozel Topluluk								N/A
39		ID_TRAP_HEAD		128				N/A				Change NMS's Trap (All current NMS's trap will be changed)				N/A			NMS Trap Degistir. (Butun NMS Trapler degisecek)			N/A
40		ID_SET_TRAP		128				N/A				Trap Degistir										N/A			Trap Degistir								N/A


[p13_esr_config.htm:Number]
93

[p13_esr_config.htm]
#Sequence ID	RES_ID				MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN						ABBR_IN_EN		FULL_IN_LOCALE							ABBR_IN_LOCALE
1		ID_ERROR0			64			N/A			Success.						N/A			Basarili.							N/A
2		ID_ERROR1			64			N/A			Failure. 						N/A			Hata.								N/A
3		ID_ERROR2			64			N/A			Failure, MC Service was stopped. 			N/A			Hata. MC Servis durdu						N/A
4		ID_ERROR3			64			N/A			Failure, invalid parameter.				N/A			Hata. Gecersiz parametre.					N/A
5		ID_ERROR4			64			N/A			Failure, invalid data.					N/A			Hata. Gecersiz veri.						N/A
6		ID_ERROR5			64			N/A			Controller is hardware protected, can't be modified.	N/A			Denetci HW korumali modifiye edilemez.				N/A
7		ID_ERROR6			64			N/A			service is busy, config cannot be changed now.		N/A			Servis Mesgul, Konfig degistirilemiyor				N/A
8		ID_ERROR7			64			N/A			non-shared port has already been occupied.		N/A			Paylasimsiz Port kullanimda					N/A
9		ID_ERROR8			64			N/A			Failure, you are not authorized.			N/A			Hata. Yetkiniz yok.						N/A
10		ID_ESR_HEAD			32			N/A			MC Configuration					N/A			MC Konfigurasyon						N/A
11		ID_PROTOCOL_TYPE		32			N/A			Protocol Type						N/A			Protokol Tipi							N/A
12		ID_PROTOCOL_MEDIA		32			N/A			Protocol Media						N/A			Protokol Medya							N/A
13		ID_CALLBACK_IN_USE		32			N/A			Callback in Use						N/A			Geri Arama Kullanimda						N/A
14		ID_REPORT_IN_USER		32			N/A			Report in Use						N/A			Rapor Kullanimda						N/A
15		ID_MAX_ALARM_REPORT		32			N/A			Max Alarm Report Attempts				N/A			Max Alarm Raporu 						N/A
16		ID_RANGE_FROM			32			N/A			Range							N/A			Oran								N/A
17		ID_CALL_ELAPSE_TIME		32			N/A			Call Elapse Time					N/A			Arama Gecen Sure						N/A
18		ID_RANGE_FROM			32			N/A			Range							N/A			Oran								N/A
19		ID_MAIN_REPORT_PHONE		32			N/A			Primary Report Phone Number				N/A			Rapor Tel No 1							N/A
20		ID_SECOND_REPORT_PHONE		32			N/A			Secondary Report Phone Number				N/A			Rapor Tel No 2							N/A
21		ID_CALLBACK_PHONE		32			N/A			Callback Phone Number					N/A			Geri Arama Tel no						N/A
22		ID_REPORT_IP			32			N/A			Primary Report IP					N/A			Rapor IP 1							N/A
23		ID_SECOND_IP			32			N/A			Secondary Report IP					N/A			Rapor IP 2							N/A
24		ID_SECURITY_IP			32			N/A			Security Connection IP					N/A			Guvenlik Baglantisi						N/A
25		ID_SECURITY_IP			32			N/A			Security Connection IP					N/A			Guvenlik Baglantisi						N/A
26		ID_SAFETY_LEVEL			32			N/A			Safety Level						N/A			Guvenlik Seviyesi						N/A
27		ID_MODIFY			32			N/A			Modify							N/A			Degistir							N/A
28		ID_CCID				16			N/A			CCID							N/A			CCID								N/A
29		ID_SOCID			16			N/A			SOCID							N/A			SOCID								N/A
30		ID_PROTOCOL0			16			N/A			EEM							N/A			EEM								N/A
31		ID_PROTOCOL1			16			N/A			RSOC							N/A			RSOC								N/A
32		ID_PROTOCOL2			16			N/A			SOCTPE							N/A			SOCTPE								N/A
33		ID_MEDIA0			16			N/A			Leased Line						N/A			Kiralanmis Hat							N/A
34		ID_MEDIA1			16			N/A			Modem							N/A			Modem								N/A
35		ID_MEDIA2			16			N/A			TCP/IP							N/A			TCP/IP								N/A
36		ID_USE0				16			N/A			Not used						N/A			Kullanilmiyor							N/A
37		ID_USE1				16			N/A			Used							N/A			Kullaniliyor							N/A
38		ID_SECURITY0			128			N/A			All commands are available				N/A			Butun komutlar kullanilabilir 					N/A
39		ID_SECURITY1			128			N/A			Only read commands are available			N/A			Sadece okuma komutu kullanilabilir					N/A
40		ID_SECURITY2			128			N/A			Only the Call Back ('CB') command are available		N/A			Sadece geri arama komutu kullanilabilir					N/A
41		ID_TIPS1			128			N/A			IP address error.\nFormat shall be 'nnn.nnn.nnn.nnn' e.g. 10.75.14.171	N/A	IP Adresi Hatali.\n Format 'nnn.nnn.nnn.nnn', Orn. 10.75.14.171		N/A
42		ID_TIPS2			128			N/A			IP mask error.\nFormat shall be 'nnn.nnn.nnn.nnn' e.g. 255.255.0.0	N/A	IP mask error.\nFormat shall be'nnn.nnn.nnn.nnn', e.g. 255.255.0.0	N/A
43		ID_TIPS3			32			N/A			IP & Mask error.					N/A			IP & Mask Hatali.						N/A
44		ID_TIPS4			128			N/A			Gateway IP error.\nFormat shall be 'nnn.nnn.nnn.nnn' e.g. 10.75.14.254, 0.0.0.0 as no gateway	N/A	Ag Gecidi Hatali. \nFormat: 'nnn.nnn.nnn.nnn' Orn. 10.75.14.254, 0.0.0.0 sin gateway	45		ID_TIPS5			128			N/A			IP, gateway and mask mismatch. Try again.		N/A			IP, a?gecidi ve mask uyumsuzlu�u. Tekrar deneyin.			N/A
45		ID_TIPS5			128			N/A			IP, gateway and mask mismatch. Try again.		N/A			IP, a?gecidi ve mask uyumsuzlu�u. Tekrar deneyin.			N/A
46		ID_TIPS6			128			N/A			CCID error, enter a number please.			N/A			CCID Hatali.Lutfen numara giriniz!				N/A
47		ID_TIPS7			128			N/A			SOCID error, enter a number please.			N/A			SOCID Hatali. Lutfen numara giriniz!				N/A
48		ID_TIPS8			128			N/A			Max alarm report alarm attempt is invalid.		N/A			Max. alarm raporu alarm denemesi gecersiz			N/A
49		ID_TIPS9			128			N/A			Max call elapse time is invalid.			N/A			Max arama gecen sure gecersiz					N/A
50		ID_TIPS10			128			N/A			Primary report phone number is invalid.			N/A			Birinci rapor tel no gecersiz					N/A
51		ID_TIPS11			128			N/A			Secondary report phone number is invalid.		N/A			Ikinci rapor telefon no gecersiz					N/A
52		ID_TIPS12			128			N/A			Callback report phone number is invalid.		N/A			Geri arama telefon no gecersiz					N/A
53		ID_TIPS13			128			N/A			Primary report IP is invalid.				N/A			Birinci rapor IP gecersiz						N/A
54		ID_TIPS14			128			N/A			Secondary report IP is invalid.				N/A			2. rapor IP gecersiz						N/A
55		ID_TIPS15			128			N/A			Security IP 1 is invalid.				N/A			Guvenlik IP1 gecersiz						N/A
56		ID_TIPS16			128			N/A			Security IP 2 is invalid.				N/A			Guvenlik IP1 gecersiz						N/A
57		ID_TIPS17			128			N/A			MC Configuration.					N/A			MC Konfigurasyon						N/A
58		ID_TIPS18			128			N/A			Can't be 0.						N/A			0 olamaz.							N/A
59		ID_TIPS19			128			N/A			Input error.						N/A			Giris hatali							N/A
60		ID_COMMON_PARAM			64			N/A			Protocol media common config				N/A			Protokol media ortak konfig					N/A
61		ID_MEDIA_PARAM0			64			N/A			Serial port parameters					N/A			Seri port parametreleri						N/A
62		ID_MEDIA_PARAM1			64			N/A			Serial port parameters & phone number			N/A			Seri port parametreleri ve telefon no				N/A
63		ID_MEDIA_PARAM2			32			N/A			TCP/IP Port Number					N/A			TCP/IP Port Numarasi							N/A
64		ID_RANGE_FROM1			64			N/A			Range 0-255						N/A			Oran 0-255							N/A
65		ID_RANGE_FROM2			64			N/A			Range 0-600						N/A			oran 0-600							N/A
66		ID_TIPS20			64			N/A			input error						N/A			Error de entrada						N/A
67		ID_TIPS21			64			N/A			Max alarm report attempts input error			N/A			Max alarm raporu denemeleri giris hatasi		N/A
68		ID_TIPS22			64			N/A			Max alarm report attempts input error			N/A			Max alarm raporu denemeleri giris hatasi		N/A
69		ID_TIPS23			64			N/A			Input error						N/A			Giris hatali							N/A
70		ID_TIPS24			64			N/A			The port input error					N/A			Port giris hatasi						N/A
71		ID_NO_PROTOCOL_TIPS		128			N/A			Please enter protocol					N/A			Protokol giriniz!						N/A
72		ID_YDN23_RANGE_FROM1		64			N/A			Range 0-5						N/A			Oran 0-5							N/A
73		ID_YDN23_RANGE_FROM2		64			N/A			Range 0-300						N/A			oran 0-300							N/A
74		ID_YDN23_1ST_REPORT_PHONE	32			N/A			Primary Report Phone Number				N/A			Birinci Rapor Telefon No							N/A
75		ID_YDN23_2ND_REPORT_PHONE	32			N/A			Secondary Report Phone Number				N/A			Ikinci Rapor Telefon No							N/A
76		ID_YDN23_3RD_REPORT_PHONE	32			N/A			Third Report Phone Number				N/A			Ucuncu Rapor Telefon No							N/A
77		ID_YDN23_REPORT_IN_USER		32			N/A			Report in Use						N/A			Rapor Kullanimda						N/A
78		ID_YDN23_SELF_ADDRESS		16			N/A			Own Address						N/A			Kendi Adresi							N/A
79		ID_YDN23_MAX_ALARM_REPORT	32			N/A			Max Alarm Report Attempts				N/A			Max Alarm Rapor Denemesi					N/A
80		ID_YDN23_CALL_ELAPSE_TIME	32			N/A			Call Elapse Time					N/A			Arama Gecen Sure						N/A
81		ID_PROTOCOL3			16			N/A			YDN23							N/A			YDN23								N/A
82		ID_TIPS8			128			N/A			Max alarm report alarm attempt is invalid.		N/A			Max Alarm Rapor denemesi gecersiz				N/A
83		ID_TIPS9			128			N/A			Max call elapse time is invalid.			N/A			max Arama Gecen Sure gecersiz					N/A
84		ID_TIPS21			64			N/A			Max alarm report alarm attempt is invalid.		N/A			Max Alarm Rapor denemesi gecersiz				N/A
85		ID_TIPS22			64			N/A			Max alarm report alarm attempt is invalid.		N/A			Max Alarm Rapor denemesi gecersiz				N/A
86		ID_TIPS23			64			N/A			Input error.						N/A			Giris Hatali							N/A
87		ID_RANGE_FROM1			64			N/A			Range 0-255						N/A			Oran 0-255							N/A
88		ID_RANGE_FROM2			64			N/A			Range 0-600						N/A			Oran 0-600							N/A
89		ID_MAIN_REPORT_PHONE		32			N/A			Primary Report Phone Number				N/A			Birinci Rapor Telefon No							N/A
90		ID_SECOND_REPORT_PHONE		32			N/A			Secondary Report Phone Number				N/A			Ikinci Rapor Telefon No 						N/A
91		ID_CALLBACK_PHONE		32			N/A			Callback Phone Number					N/A			Geri Arama Telefon No						N/A
92		ID_MAX_ALARM_REPORT		32			N/A			Max Alarm Report Attempts				N/A			Max Alarm Rapor Denemesi					N/A
93		ID_CALL_ELAPSE_TIME		32			N/A			Call Elapse Time					N/A			Arama Gecen Sure						N/A

[p14_user_config.htm:Number]
38

[p14_user_config.htm]
#Sequence ID	RES_ID				MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN									ABBR_IN_EN	FULL_IN_LOCALE								ABBR_IN_LOCALE
1		ID_WEB_HEAD			32			N/A			User Configuration								N/A		Kullanici Konfig							N/A
2		ID_CURRENT_USER			32			N/A			Current users									N/A		Mevcut Kullanicilar							N/A
3		ID_USER_NAME_INTABLE		32			N/A			User Name   									N/A		Kullanici Adi								N/A
4		ID_USER_AUTHORITY_INTABLE	32			N/A			Authority									N/A		Yetki									N/A
5		ID_PASSWORD			32			N/A			Password									N/A		Sifre									N/A
6		ID_USER_NAME			32			N/A			User Name									N/A		Kullanici Adi								N/A
7		ID_USER_AUTHORITY		32			N/A			Authority									N/A		Yetki									N/A
8		ID_CONFIRM			32			N/A			Confirm										N/A		Onayla									N/A
9		ID_USER_ADD			32			N/A			Add New User									N/A		Yeni Kullanici Ekle							N/A
10		ID_USER_MODIFY			32			N/A			Modify User									N/A		Kullaniciyi Degistir							N/A
11		ID_USER_DELETE			32			N/A			Delete Selected User								N/A		Secilen Kullaniciyi Sil							N/A
12		ID_ERROR0			32			N/A			Unknown error.									N/A		Bilinmeyen Hata								N/A
13		ID_ERROR1			32			N/A			Success										N/A		Basarili							N/A
14		ID_ERROR2			64			N/A			Failure, incomplete information.						N/A		Hata. Eksik Bilgi.							N/A
15		ID_ERROR3			64			N/A			Failure, the user name already exists.						N/A		Hata. Kullanici adi mevcut						N/A
16		ID_ERROR4			64			N/A			Failure, you are not authorized.						N/A		Hata. Yetkiniz yok.							N/A
17		ID_ERROR5			64			N/A			Controller is hardware protected, can't be modified.				N/A		Denetci HW korumali, degistirilemez!					N/A
18		ID_ERROR6			64			N/A			Failure, only you can change your own password.					N/A		Hata. Sadece kendiniz sifrenizi degistirebilirsiniz.			N/A
19		ID_ERROR7			64			N/A			Failure, deleting admin is not allowed.						N/A		Hata. Admini silme izni yok.						N/A
20		ID_ERROR8			64			N/A			Failure, can't delete the logged in user.					N/A		Hata. Kullanici oturum acmis,silemezsiniz.			N/A
21		ID_ERROR9			128			N/A			The user already exists, please try again.					N/A		Kullanici mevcut, daha sonra tekrar deneyin.				N/A
22		ID_ERROR10			128			N/A			Failure, too many users.							N/A		Hata. Cok fazla kullanici.						N/A
23		ID_ERROR11			128			N/A			Failure, user does not exist.							N/A		Hata. Boyle bir kullanici yok.						N/A
24		ID_TIPS1			32			N/A			Please enter a user name.							N/A		Lutfen kullanici adi girin.						N/A
25		ID_TIPS2			128			N/A			The user name cannot start or end with space.					N/A		kullanici adi bosluk ile baslayamaz ve bitemez!				N/A
26		ID_TIPS3			128			N/A			The inputs for 'Password' and 'Confirm' must be the same, please try agian.	N/A		Girilen sifre ile onaylanan sifre ayni olmali, tekrar deneyiniz!	N/A
27		ID_TIPS4			128			N/A			Please rememeber the password.							N/A		Lutfen sifreyi hatirlayin!						N/A
28		ID_TIPS5			128			N/A			A password must be entered.							N/A		Sifre girilmeli!							N/A
29		ID_TIPS6			128			N/A			Please remember the password.							N/A		Lutfen sifreyi hatirlayin!						N/A
30		ID_TIPS7			128			N/A			already exists, please try again.						N/A		Zaten Mevcut, lutfen tekrar deneyin!					N/A
31		ID_TIPS8			128			N/A			does not exist,so the password can't be modified, please try again.		N/A		Mevcut degil. Bu nedenle sifre degistirilemez. Daha sonra tekrar deneyin	N/A
32		ID_TIPS9			128			N/A			Please select one or more users before clicking this button.			N/A		Butona basmadan bir yada daha cok kullanici secin			N/A
33		ID_AUTHORITY_LEVEL0		32			N/A			Browser										N/A		Gozat   								N/A
34		ID_AUTHORITY_LEVEL1		32			N/A			Operator									N/A		Operator								N/A
35		ID_AUTHORITY_LEVEL2		32			N/A			Engineer									N/A		Muhendis								N/A
36		ID_AUTHORITY_LEVEL3		32			N/A			Administrator									N/A		Admin									N/A
37		ID_INVALIDATE_CHAR		64			N/A			Invalid character in input.							N/A		Gecersiz karakter girisi!						N/A
38		ID_TIPS10			128			N/A			The following characters must not be included in user name, please try again.	N/A		Kullanici adi belirtilen karakterleri icermelidir. Lutfen tekarar deneyin	N/A

[p15_show_acutime.htm:Number]
0

[p15_show_acutime.htm.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[p16_filemanage_title.htm:Number]
8

[p16_filemanage_title.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE			ABBR_IN_LOCALE
1		ID_ERROR0	32			N/A			Unknown error.				N/A		Bilinmeyen Hata.		N/A
2		ID_ERROR1	32			N/A			Success					N/A		Basarili.				N/A
3		ID_ERROR2	32			N/A			Failure, not enough space.		N/A		Hata. Yeterli bosluk yok	N/A
4		ID_ERROR3	32			N/A			Failure, you are not authorized.	N/A		Hata. Yetkiniz yok.		N/A
5		ID_ERROR4	32			N/A			Success, stop Controller		N/A		Basarili. Denetciyi durdur			N/A
6		ID_ERROR5	32			N/A			Success, start Controller		N/A		Basarili. Denetciyi baslat			N/A
7		ID_UPLOAD	32			N/A			Upload					N/A		Yukle 				N/A
8		ID_DOWNLOAD	32			N/A			Download				N/A		Indir			N/A
9		ID_TIPS0	128			N/A			Do you want to quit the download pages? Click Yes to start Controller, Click No to cancel this operation.	N/A	Yukleme sayfasindan cikmak istediginize emin misiniz? Denetciyi baslatmak icin Evete basin, Iptal etmek icin hayira basin		N/A

[p17_login_overtime.htm:Number]
5

[p17_login_overtime.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_LOGIN_HEAD		32			N/A			Overtime		N/A			Zaman Asimi		N/A
2		ID_OK			32			N/A			Login again		N/A			Tekrar giris yapin		N/A
3		ID_CANCEL		32			N/A			Cancel			N/A			Iptal			N/A
4		ID_EXPLORE_INFO		256			N/A			Login time expired, so you can only explore data.<br> Click OK to login again if you want to control device.<br>Click Cancel to continue exploring data.<br>	N/A		Giris zaman asimi. Tekrar giris yapmak icin Evete basiniz. Iptal etmek icin hayira basiniz.		N/A
5		ID_TIPS0		64			N/A			You have connected to Controller. This window will be closed.		N/A				Denetciye baglandiniz. Bu pencere kapanacak.		N/A

[p18_restore_default.htm:Number]
8

[p18_restore_default.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN									ABBR_IN_EN	FULL_IN_LOCALE									ABBR_IN_LOCALE
1		ID_HEAD			128			N/A			Restore to Default								N/A		Varsayilani Geri Yukle								N/A
2		ID_TIPS0		128			N/A			Restore to default configuration. The system will be rebooted.			N/A		Varsayilan konfigurasyonu geri yukle. Sistem yeniden baslatilacak.			N/A
3		ID_RESTORE_DEFAULT	64			N/A			Restore to Default								N/A		Varsayilani geri yukle								N/A
4		ID_TIPS1		128			N/A			Restore to default configuration will force system to reboot. Are you sure?	N/A		Varsayilan konfigurasyonu geri yuklemek sistemi yeniden baslatacak, emin misiniz?	N/A
5		ID_TIPS2		128			N/A			Controller is hardware protected, can't be restored!				N/A		Denetci HW korumali, geri yukleme yapilamaz.					N/A
6		ID_TIPS3		128			N/A			Are you sure you want to reboot Controller?					N/A		Denetciyi yeniden baslatmaya emin misiniz?					N/A
7		ID_TIPS4		128			N/A			Failure, you are not authorized!						N/A		Hata. Yetkiniz yok.								N/A
8		ID_START_SCUP		32			N/A			Reboot Controller 								N/A		Denetciyi Yeniden Baslat.							N/A

[p19_time_config.htm:Number]
28

[p19_time_config.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN																ABBR_IN_EN	FULL_IN_LOCALE							ABBR_IN_LOCALE
1		ID_TIME_HEAD		32			N/A			Time Settings																N/A		Zaman Ayarlari							N/A
2		ID_TIME_HEAD		32			N/A			Time Settings																N/A		Zaman Ayarlari							N/A
3		ID_SELECT1		64			N/A			Get Time Automatically from the Following Time Servers											N/A		Zaman ayarlarini otomatik olarak asagidaki serverdan al.	N/A
4		ID_PRIMARY_SERVER	32			N/A			Primary Server																N/A		Birinci Server							N/A
5		ID_SECONDARY_SERVER	32			N/A			Secondary Server															N/A		Ikinci Server							N/A
6		ID_TIMER_INTERVAL	64			N/A			Interval to Adjust Time															N/A		Zaman ayarlama araligi						N/A
7		ID_SPECIFY_TIME		32			N/A			Specify Time																N/A		Zamani belirle							N/A
8		ID_GET_TIME		32			N/A			Get Local Time																N/A		Yerel Zamani al							N/A
9		ID_DATE			16			N/A			Date																	N/A		Tarih								N/A
10		ID_TIME			16			N/A			Time																	N/A		Zaman								N/A
11		ID_SUBMIT		16			N/A			Apply																	N/A		Uygula								N/A
12		ID_ERROR0		32			N/A			Unknown error.																N/A		Bilinmeyen Hata.						N/A
13		ID_ERROR1		16			N/A			Success.																N/A		Basarili							N/A
14		ID_ERROR2		128			N/A			Failure, incorrect time setting.													N/A		Hata. Yanlis zaman ayari.					N/A
15		ID_ERROR3		128			N/A			Failure, incomplete information.													N/A		Hata. Eksik bilgi.						N/A
16		ID_ERROR4		128			N/A			Failure, you are not authorized.													N/A		Hata. Yetkiniz yok.						N/A
17		ID_ERROR5		64			N/A			Controller is hardware protected, can't be modified.											N/A		Denetci HW korumali, degistirilemez.				N/A
18		ID_TIPS0		128			N/A			Incorrect IP address of primary server.\nCorrect format should be 'nnn.nnn.nnn.nnn', e.g, 129.9.1.10.\n0.0.0.0 means no time server	N/A		1. server IP adresi yanlis.\nFormat 'nnn.nnn.nnn.nnn', orn, 129.9.1.10.  '0.0.0.0' server yok anlamindadir.		N/A
19		ID_TIPS1		128			N/A			Incorrect IP address of secondary server.\nCorrect format should be 'nnn.nnn.nnn.nnn', e.g., 129.9.1.10.\n0.0.0.0 means no time server	N/A		1. server IP adresi yanlis.\nFormat 'nnn.nnn.nnn.nnn', orn, 129.9.1.10.  '0.0.0.0' server yok anlamindadir.		N/A
20		ID_TIPS2		128			N/A			Incorrect time interval.\nTime interval should be a positive integer.									N/A		Zaman araligi hatali\nPozitif bir deger girilmelidir.	N/A
21		ID_TIPS3		128			N/A			Synchronizing time, please wait...													N/A		Zaman senkronize ediliyor, lutfen bekleyiniz...			N/A
22		ID_TIPS4		128			N/A			Incorrect date setting.\nCorrect format should be 'yyyy/mm/dd', e.g., 2000/09/31							N/A		Yanlis tarih ayari.\nFormat: 'yyyy/aa/gg', orn. 2000/09/31	N/A
23		ID_TIPS5		128			N/A			Incorrect time setting. \nCorrect format should be 'hh:mm:ss', e.g., 8:23:08								N/A		Yanlis zaman ayari.\nFormat: 'sasa:dd:ss', orn. 8:23:08		N/A
24		ID_TIPS6		128			N/A			Date must be set between '1970/01/01 00:00:00' and '2038/01/01 00:00:00'.								N/A		Tarih '1999/01/01 00:00:00' ile '2038/02/06 00:00:00'araliginda olmalidir.		N/A
25		ID_MINUTES		16			N/A			Minutes																	N/A		Dakika								N/A
26		ID_ZONE			16			N/A			Local Zone																N/A		Yerel bolge							N/A
27		ID_GET_ZONE		32			N/A			Get Local Zone																N/A		Yerel bolgeyi al						N/A
28		ID_TIPS7		128			N/A			The time server has been set, so the time will be set by time server.									N/A		Zaman serveri ayarlandi, zaman bilgisi oradan alinacak!	N/A

[p20_history_frame.htm:Number]	
0
																								   	
[p20_history_frame.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[p21_history_title.htm:Number]
4

[p21_history_title.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_DATA			16			N/A			Data History			N/A		Veri Gecmisi		N/A
2		ID_ALARM		16			N/A			Alarm History			N/A		Alarm Gecmisi		N/A
3		ID_LOG			16			N/A			Log				N/A		Kayit			N/A
4		ID_BAT			16			N/A			Battery				N/A		Aku			N/A


[p22_history_dataquery.htm:Number]
53

[p22_history_dataquery.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN	FULL_IN_LOCALE						ABBR_IN_LOCALE
1		ID_HISTORY_HEAD		32			N/A			Query Data History				N/A		Veri Gecmisi sorgula					N/A
2		ID_DEVICE		16			N/A			Device						N/A		Cihaz							N/A
3		ID_SIGNAL		16			N/A			Signal Name					N/A		Sinyal Adi						N/A
4		ID_VALUE		8			N/A			Value						N/A		Deger							N/A
5		ID_QUERY_TYPE		16			N/A			Query Type					N/A		Sorgu Tipi						N/A
6		ID_QUERY		8			N/A			Query						N/A		Sorgu							N/A
7		ID_HISTORY		16			N/A			History						N/A		Gecmis							N/A
8		ID_STATISTIC		16			N/A			Statistics					N/A		Istatistik						N/A
9		ID_YEAR			8			N/A			Year						N/A		Sene							N/A
10		ID_MONTH		8			N/A			Month						N/A		Ay							N/A
11		ID_DAY			8			N/A			Day						N/A		Gun							N/A
12		ID_FROM			8			N/A			From						N/A		den							N/A
13		ID_TO			8			N/A			To						N/A		e							N/A
14		ID_YEAR			8			N/A			Year						N/A		Sene							N/A
15		ID_MONTH		8			N/A			Month						N/A		Ay							N/A
16		ID_DAY			8			N/A			Day						N/A		Gun							N/A
17		ID_ERROR0		32			N/A			Unknown error.					N/A		Bilinmeyen hata.						N/A
18		ID_ERROR1		32			N/A			Success.					N/A		Basarili						N/A
19		ID_ERROR2		32			N/A			No data to be queried.				N/A		Sorgulanacak veri yok.					N/A
20		ID_ERROR3		32			N/A			Failure						N/A		Hata							N/A
21		ID_ERROR4		32			N/A			Failure, you are not authorized.		N/A		Hata. Yetkiniz yok					N/A
22		ID_ERROR5		64			N/A			Failed to communicate with Controller.		N/A		Hata. Denetci iletisimi.					N/A
23		ID_QUERY		16			N/A			Query						N/A		Sorgu							N/A
24		ID_SIGNAL		16			N/A			Signal						N/A		Sinyal							N/A
25		ID_VALUE		16			N/A			Value						N/A		Deger							N/A
26		ID_UNIT			16			N/A			Unit						N/A		Birim							N/A
27		ID_TIME			16			N/A			Time						N/A		Zaman							N/A
28		ID_DEVICE_NAME		16			N/A			Device Name					N/A		Cihaz Adi						N/A
29		ID_INDEX		16			N/A			Index						N/A		Icerik							N/A
30		ID_ALARM_STATUS		16			N/A			Status						N/A		Durum							N/A
31		ID_INDEX		16			N/A			Index						N/A		Icerik							N/A
32		ID_DEVICE		16			N/A			Device						N/A		Cihaz							N/A
33		ID_SIGNAL		16			N/A			Singal						N/A		Sinyal							N/A
34		ID_CURRENT_VALUE	16			N/A			Current Value					N/A		Simdiki Deger						N/A
35		ID_AVERAGE_VALUE	16			N/A			Average Value					N/A		Ortalama deger						N/A
36		ID_STAT_TIME		16			N/A			Statistics Time					N/A		Istatistik Zaman					N/A
37		ID_MAX_VALUE		16			N/A			Max Value					N/A		Max. Deger						N/A
38		ID_MAX_TIME		16			N/A			Max Time					N/A		Max. Zaman						N/A
39		ID_MIN_VALUE		16			N/A			Min Value					N/A		Min. Deger						N/A
40		ID_MIN_TIME		16			N/A			Min Time					N/A		Min. Zaman						N/A
41		ID_UNIT			16			N/A			Unit						N/A		Birim							N/A
42		ID_DOWNLOAD		32			N/A			Download					N/A		Indir							N/A
43		ID_TIPS			64			N/A			End time should be bigger than start time	N/A		Bitis zamani baslama zamanindan buyuk olmalidir	N/A
44		ID_MONTH_ERROR		32			N/A			Incorrect month.				N/A		Yanlis Ay.					N/A
45		ID_DAY_ERROR		32			N/A			Incorrect day.					N/A		Yanlis gun!						N/A
46		ID_HOUR_ERROR		32			N/A			Incorrect hour.					N/A		Yanlis  saat!						N/A
47		ID_FORMAT_ERROR		64			N/A			Incorrect format.				N/A		Yanlis  format!						N/A
48		ID_YEAR_ERROR		32			N/A			Incorrect year.					N/A		Yanlis yil!						N/A
49		ID_MINUTE_ERROR		32			N/A			Incorrect minute.				N/A		Yanlis dakika!						N/A
50		ID_SECOND_ERROR		32			N/A			Incorrect second.				N/A		Yanlissaniye!						N/A
51		ID_INCORRECT_PARAM	64			N/A			Incorrect parameter.				N/A		Yanlisparametre!					N/A
52		ID_ALL_DEVICE		64			N/A			All Devices					N/A		Butun cihazlar						N/A
53		ID_MAX_NUMBER_TIPS	128			N/A			The maximum number of records to be shown is 500.	N/A		Gosterilecek maksimum kayit sayisi 500 olacaktir.			N/A



[p23_history_alarmquery.htm:Number]
43

[p23_history_alarmquery.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN	FULL_IN_LOCALE						ABBR_IN_LOCALE
1		ID_ALARM_HEAD		64			N/A			Query Alarm History				N/A		Alarm sorgu gecmisi					N/A
2		ID_YEAR			16			N/A			Year						N/A		Sene							N/A
3		ID_MONTH		16			N/A			Month						N/A		Ay							N/A
4		ID_DAY			16			N/A			Day						N/A		Gun							N/A
5		ID_FROM			16			N/A			From						N/A		den							N/A
6		ID_TO			16			N/A			To						N/A		e							N/A
7		ID_YEAR			16			N/A			Year						N/A		Sene							N/A
8		ID_MONTH		16			N/A			Month						N/A		Ay							N/A
9		ID_DAY			16			N/A			Day						N/A		Gun							N/A
10		ID_DEVICE_NAME		16			N/A			Device						N/A		Cihaz							N/A
11		ID_QUERY		16			N/A			Query						N/A		Sorgu							N/A
12		ID_DOWNLOAD		16			N/A			Download					N/A		Indir							N/A
13		ID_INDEX		16			N/A			Index						N/A		Icerik							N/A
14		ID_DEVICE		16			N/A			Device						N/A		Cihaz							N/A
15		ID_ALARM_LEVEL		16			N/A			Alarm Level					N/A		Alarm Seviyesi						N/A
16		ID_VALUE		16			N/A			Value						N/A		Deger							N/A
17		ID_START_TIME		16			N/A			Start Time					N/A		Baslama Zamani						N/A
18		ID_END_TIME		16			N/A			End Time					N/A		Bitis Zamani						N/A
19		ID_ERROR0		32			N/A			Unknown error.					N/A		Bilinmeyen Hata.						N/A
20		ID_ERROR1		32			N/A			Success.					N/A		Basarili.						N/A
21		ID_ERROR2		32			N/A			No data.					N/A		Veri yok						N/A
22		ID_ERROR3		32			N/A			Failure.					N/A		hata							N/A
23		ID_ERROR4		32			N/A			Failure, you are not authorized.		N/A		Hata. Yetkiniz yok					N/A
24		ID_ERROR5		64			N/A			Failed to communicate with the Controller.	N/A		Hata. Denetci ilehaberlesme hatasi				N/A
25		ID_ERROR6		64			N/A			Cleared successfully.				N/A		Silme basarili.						N/A
26		ID_OA			16			N/A			MI						N/A		Minor							N/A
27		ID_MA			16			N/A			MA						N/A		Major							N/A
28		ID_CA			16			N/A			CA						N/A		Kritik							N/A
29		ID_SIGNAL_NAME		16			N/A			Name						N/A		Isim							N/A
30		ID_CLEAR_CLARM		16			N/A			Clear Alarm					N/A		Alarmi Sil						N/A
31		ID_SET			16			N/A			Set						N/A		Ayarla							N/A
32		ID_DOWNLOAD		32			N/A			Download					N/A		Indir						N/A
33		ID_TIPS			64			N/A			End time should be bigger than start time	N/A		Bitis zamani baslama zamanindan buyuk olmalidir		N/A
34		ID_MONTH_ERROR		32			N/A			Incorrect month.				N/A		Yanlis Ay.					N/A
35		ID_DAY_ERROR		32			N/A			Incorrect day.					N/A		Yanlis Gun.						N/A
36		ID_HOUR_ERROR		32			N/A			Incorrect hour.					N/A		Yanlis Saat.						N/A
37		ID_FORMAT_ERROR		64			N/A			Incorrect format.				N/A		Yanlis Format.						N/A
38		ID_YEAR_ERROR		32			N/A			Incorrect year.					N/A		Yanlis Sene.						N/A
39		ID_MINUTE_ERROR		32			N/A			Incorrect minute.				N/A		Yanlis Dakika.						N/A
40		ID_SECOND_ERROR		32			N/A			Incorrect second.				N/A		Yanlis Saniye.						N/A
41		ID_INCORRECT_PARAM	64			N/A			Incorrect parameter.				N/A		Yanlis Parametre.					N/A
42		ID_ALL_DEVICE		64			N/A			All Devices					N/A		Butun Cihazlar						N/A
43		ID_MAX_NUMBER_TIPS	128			N/A			Maximum number of records to be shown is 500.	N/A		Gosterilecek maksimum kayit sayisi 500 olacaktir.					N/A


[p24_history_logquery.htm:Number]
93

[p24_history_logquery.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN	FULL_IN_LOCALE					ABBR_IN_LOCALE
1		ID_LOG_HEAD		32			N/A			Query History Log				N/A		Gecmis Kayit Sorgusu				N/A
2		ID_YEAR			16			N/A			Year						N/A		Sene						N/A
3		ID_MONTH		16			N/A			Month						N/A		Ay						N/A
4		ID_DAY			16			N/A			Day						N/A		Gun						N/A
5		ID_FROM			16			N/A			From						N/A		den						N/A
6		ID_TO			16			N/A			To						N/A		e						N/A
7		ID_YEAR			16			N/A			Year						N/A		Sene						N/A
8		ID_MONTH		16			N/A			Month						N/A		Ay						N/A
9		ID_DAY			16			N/A			Day						N/A		Gun						N/A
10		ID_QUERY_TYPE		16			N/A			Query Type					N/A		Sorgu Tipi					N/A
11		ID_QUERY		16			N/A			Query						N/A		Sorgu						N/A
12		ID_CONTROL_LOG		16			N/A			Control Log					N/A		Kontrol Kaydi					N/A
13		ID_SYSTEM_LOG		16			N/A			System Log					N/A		Sistem Kaydi					N/A
14		ID_ERROR0		32			N/A			Unknown error.					N/A		Bilinmeyen hata.				N/A
15		ID_ERROR1		32			N/A			Control log success.				N/A		Kontrol kaydi basarili.				N/A
16		ID_ERROR2		32			N/A			No data to be queried.				N/A		Sorgulanacak veri yok.				N/A
17		ID_ERROR3		32			N/A			Failure.					N/A		Hata.						N/A
18		ID_ERROR4		64			N/A			Failure, you are not authorized.		N/A		Hata. Yetkiniz yok!				N/A
19		ID_ERROR5		32			N/A			Failed to communicate with Controller.		N/A		Denetci ilehaberlesme hatasi.			N/A
20		ID_ERROR6		32			N/A			Acquired battery log successfully.		N/A		Elde edilen aku kaydi basarili.			N/A
21		ID_ERROR7		32			N/A			Acquired system log successfully.		N/A		Elde edilen sistem kaydi basarili.		N/A
22		ID_BATTERY_TEST_REASON0	64			N/A			Start planned test				N/A		Planlanan testi baslat				N/A
23		ID_BATTERY_TEST_REASON1	64			N/A			Start manual test				N/A		Manuel testi baslat				N/A
24		ID_BATTERY_TEST_REASON2	64			N/A			Start AC fail test				N/A		AC kesik testini baslat				N/A
25		ID_BATTERY_TEST_REASON3	64			N/A			Start test for Master Power start test		N/A		Master sistem baslatma testini baslat		N/A
26		ID_BATTERY_TEST_REASON4	64			N/A			Other reason					N/A		Baska sebep					N/A
27		ID_BATTERY_END_REASON0	64			N/A			End by manual					N/A		Manuel durdur					N/A
28		ID_BATTERY_END_REASON1	64			N/A			End by alarm					N/A		Alarmla durdur					N/A
29		ID_BATTERY_END_REASON2	64			N/A			Time up						N/A		Zaman doldu					N/A
30		ID_BATTERY_END_REASON3	64			N/A			Capacity full					N/A		Kapasite full					N/A
31		ID_BATTERY_END_REASON4	64			N/A			End by voltage					N/A		Gerilimle Bitir					N/A
32		ID_BATTERY_END_REASON5	64			N/A			End by AC fail					N/A		AC kesilince bitir				N/A
33		ID_BATTERY_END_REASON6	64			N/A			End by AC restore				N/A		AC gelince bitir				N/A
34		ID_BATTERY_END_REASON7	64			N/A			End by AC fail test discharge			N/A		AC kesik testi desarjiyla bitir			N/A
35		ID_BATTERY_END_REASON8	64			N/A			End test for Master Power stop test		N/A		Master sistem durudurma testi icin bitir	N/A
36		ID_BATTERY_END_REASON9	128			N/A			End a PowerSplit BT for Auto/Man turn to Man	N/A		GucPaylasimi aku testini manuele donerek bitir  	N/A
37		ID_BATTERY_END_REASON10	128			N/A			End PowerSplit Man-BT for Auto/Man turn to Auto	N/A		GucPaylasimi manuel aku testini otomatige donerek bitir	N/A
38		ID_BATTERY_END_REASON11	64			N/A			End by other reason				N/A		Baska sebeple bitir				N/A
39		ID_BATTERY_TEST_RESULT0	64			N/A			No result					N/A		Sonuc yok					N/A
40		ID_BATTERY_TEST_RESULT1	64			N/A			Good result					N/A		Sonuc iyi					N/A
41		ID_BATTERY_TEST_RESULT2	64			N/A			Bad result					N/A		Sonuc kotu					N/A
42		ID_BATTERY_TEST_RESULT3	64			N/A			It's a Power Split Test				N/A		Bu GucPaylasimi testidir			N/A
43		ID_BATTERY_TEST_RESULT4	64			N/A			Other result					N/A		Baska sebep					N/A
44		SYS_LOG_HEAD0		64			N/A			Index						N/A		Icerik						N/A
45		SYS_LOG_HEAD1		64			N/A			Task Name					N/A		Task adi					N/A
46		SYS_LOG_HEAD2		64			N/A			Information Level				N/A		Bilgi seviyesi					N/A
47		SYS_LOG_HEAD3		64			N/A			Log Time					N/A		Kayit zamani					N/A
48		SYS_LOG_HEAD4		64			N/A			Information					N/A		Bilgi						N/A
49		CTL_LOG_HEAD0		64			N/A			Index						N/A		Icerik						N/A
50		CTL_LOG_HEAD1		64			N/A			Equipment Name					N/A		Cihaz adi					N/A
51		CTL_LOG_HEAD2		64			N/A			Signal Name					N/A		Sinyal adi				N/A
52		CTL_LOG_HEAD3		64			N/A			Control Value					N/A		Degeri Kontrol et				N/A
53		CTL_LOG_HEAD4		64			N/A			Unit						N/A		Birim						N/A
54		CTL_LOG_HEAD5		64			N/A			Time						N/A		Zaman						N/A
55		CTL_LOG_HEAD6		64			N/A			Sender Name					N/A		Gonderen adi					N/A
56		CTL_LOG_HEAD7		64			N/A			Sender Type					N/A		Gonderen tipi					N/A
57		CTL_LOG_HEAD8		64			N/A			Control Result					N/A		Kontrol sonucu					N/A
58		ID_CTL_RESULT0		64			N/A			Success						N/A		Basarili					N/A
59		ID_CTL_RESULT1		64			N/A			No memory					N/A		Hafiza yok					N/A
60		ID_CTL_RESULT2		64			N/A			Time out					N/A		Sure doldu					N/A
61		ID_CTL_RESULT3		64			N/A			Failure						N/A		Hata						N/A
62		ID_CTL_RESULT4		64			N/A			Communication busy				N/A		haberlesme mesgul				N/A
63		ID_CTL_RESULT5		64			N/A			Control was suppressed				N/A		Kontrol durduruldu				N/A
64		ID_CTL_RESULT6		64			N/A			Control was disabled				N/A		Kontrol engellendi				N/A
65		ID_CTL_RESULT7		64			N/A			Control was disabled				N/A		Kontrol engellendi				N/A
66		ID_DOWNLOAD		32			N/A			Download					N/A		Indir						N/A
67		ID_TIPS			64			N/A			End time should be bigger than start time	N/A		Bitis zamani baslama zamanindan buyuk olmalidir	N/A
68		ID_MONTH_ERROR		32			N/A			Incorrect month.				N/A		Yanlis ay.					N/A
69		ID_DAY_ERROR		32			N/A			Incorrect day.					N/A		Yanlis gun.					N/A
70		ID_HOUR_ERROR		32			N/A			Incorrect hour.					N/A		Yanlis saat.					N/A
71		ID_FORMAT_ERROR		64			N/A			Incorrect format.				N/A		Yanlis format.					N/A
72		ID_YEAR_ERROR		32			N/A			Incorrect year.					N/A		Yanlis sene.					N/A
73		ID_MINUTE_ERROR		32			N/A			Incorrect minute.				N/A		Yanlis dakika					N/A
74		ID_SECOND_ERROR		32			N/A			Incorrect second.				N/A		Yanlis saniye.					N/A
75		ID_INCORRECT_PARAM	64			N/A			Incorrect parameter.				N/A		Yanlis parametre.				N/A
76		ID_DISEL_TEST_REASON0	64			N/A			Planned test					N/A		Planlanan test					N/A
77		ID_DISEL_TEST_REASON1	64			N/A			Manual start					N/A		Manuel baslat					N/A
78		ID_DISEL_TEST_RESULT0	64			N/A			Normal						N/A		Normal						N/A
79		ID_DISEL_TEST_RESULT1	64			N/A			Manual Stop					N/A		Manuel durdur					N/A
80		ID_DISEL_TEST_RESULT2	64			N/A			Time is up					N/A		Zaman doldu					N/A
81		ID_DISEL_TEST_RESULT3	64			N/A			In Manual Mode					N/A		manuel Modda					N/A
82		ID_DISEL_TEST_RESULT4	64			N/A			Low Battery Voltage				N/A		Dusuk akuGerilimi				N/A
83		ID_DISEL_TEST_RESULT5	64			N/A			High Water Temperature				N/A		Yuksek Su Sicakligi				N/A
84		ID_DISEL_TEST_RESULT6	64			N/A			Low Oil Pressure				N/A		Dusuk Yag Basinci				N/A
85		ID_DISEL_TEST_RESULT7	64			N/A			Low Fuel Level					N/A		Dusuk Yakit Seviyesi				N/A
86		ID_DISEL_TEST_RESULT8	64			N/A			Diesel Failure					N/A		Dizel hata					N/A
87		ID_DISEL_TEST_HEAD0	64			N/A			Index						N/A		Icerik						N/A
88		ID_DISEL_TEST_HEAD1	64			N/A			Start Time					N/A		Baslama Zamani					N/A
89		ID_DISEL_TEST_HEAD2	64			N/A			End Time					N/A		Bitis Zamani					N/A
90		ID_DISEL_TEST_HEAD3	64			N/A			Start Reason					N/A		Baslama Nedeni					N/A
91		ID_DISEL_TEST_HEAD4	64			N/A			Test Result					N/A		Test Sonucu					N/A
92		ID_DISEL_LOG		64			N/A			Diesel Test Log					N/A		Dizel Test Kaydi				N/A
93		ID_MAX_NUMBER_TIPS	128			N/A			The maximum number of records to be shown is 500.	N/A		Gosterilecek maksimum kayit sayisi 500 olacaktir.			N/A


[p25_online_frame.htm:Number]
0

[p25_online_frame.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[p26_online_title.htm:Number]
3

[p26_online_title.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL		MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_MODIFY_SCUP		16				N/A			Modify Controller		N/A		Denetciyi Degistir		N/A
2		ID_MODIFY_DEVICE	16				N/A			Modify Device			N/A		Cihazi Degistir			N/A
3		ID_MODIFY_ALARM		16				N/A			Modify Signal			N/A		Sinyali Degistir		N/A

[p27_online_modifysystem.htm:Number]
21

[p27_online_modifysystem.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR		FULL_IN_EN							ABBR_IN_EN	FULL_IN_LOCALE								ABBR_IN_LOCALE
1		ID_ERROR0		32				N/A			Failure.							N/A		Hata										N/A
2		ID_ERROR1		32				N/A			Success.							N/A		Basarili									N/A
3		ID_ERROR2		32				N/A			Unknown error							N/A		Bilinmeyen Hata									N/A
4		ID_ERROR3		32				N/A			Failure, you are not authorized.				N/A		Hata. Yetkiniz yok.								N/A
5		ID_ERROR4		32				N/A			Failed to communicate						N/A		Iletisim Hatasi									N/A
6		ID_DEVICE		32				N/A			Device Name							N/A		Cihaz Adi									N/A
7		ID_SIGNAL		32				N/A			Signal Name							N/A		Sinyal Adi								N/A
8		ID_VALUE		32				N/A			Value								N/A		Deger										N/A
9		ID_SETTING_VALUE	32				N/A			Set Value							N/A		Deger Ayarla									N/A
10		ID_SET			32				N/A			Set								N/A		Ayarla										N/A
11		ID_TIPS0		128				N/A			Input Error.							N/A		Giris Hatali.									N/A
12		ID_TIPS1		128				N/A			Invalid characters were included in input.\nPlease try again.	N/A		Giriste gecersiz karakterler var.\nLutfen tekrar deneyiniz.			N/A
13		ID_TIPS2		128				N/A			Modify								N/A		Degistir									N/A
14		ID_SIGNAL_TYPE		32				N/A			Signal Type							N/A		Sinyal Tipi									N/A
15		ID_ERROR5		32				N/A			Failure, Controller is hardware protected			N/A		Hata. Denetci HW korumali							N/A
16		ID_SET2			32				N/A			Set								N/A		Ayarla										N/A
17		ID_SITE			32				N/A			Site								N/A		Yer										N/A
18		ID_ERROR5		64				N/A			The length must be no more than 32 characters			N/A		Uzunluk max 32 karater olmali				 			N/A
19		ID_SITE_NAME		64				N/A			Site Name							N/A		Yer Adi									N/A
20		ID_SITE_LOCATION	64				N/A			Site Location							N/A		Yer Tanimi									N/A
21		ID_SITE_DESCTIPTION	64				N/A			Site Description						N/A		Saha Aciklamasi									N/A


[p28_online_modifydevice.htm:Number]
23

[p28_online_modifydevice.htm]
#Sequence ID	RES_ID				MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN									ABBR_IN_EN	FULL_IN_LOCALE											ABBR_IN_LOCALE
1		ID_ERROR0			32			N/A			Failure.									N/A		Hata.												N/A
2		ID_ERROR1			32			N/A			Success.									N/A		Basarili.											N/A
3		ID_ERROR2			32			N/A			Unknown error.									N/A		Bilinmeyen hata.											N/A
4		ID_ERROR3			32			N/A			Failure, you are not authorized.						N/A		Hata. Yetkiniz yok.										N/A
5		ID_ERROR4			64			N/A			Failed to communicate								N/A		Haberlesme hatasi										N/A
6		ID_DEVICE			32			N/A			Device Name									N/A		Cihaz Adi											N/A
7		ID_NEWDEVICE			64			N/A			New Device Name									N/A		Yeni Cihaz Adi											N/A
8		ID_SET				16			N/A			Set										N/A		Ayarla												N/A
9		ID_TIPS0			64			N/A			Enter device name please.							N/A		Cihaz adi giriniz										N/A
10		ID_TIPS1			128			N/A			None of these characters must not be included in user name.\nPlease try again.	N/A		Bu karakterlerin hicbiri kullanici adinda yer almamalidir\nTekrar deneyiniz!			N/A
11		ID_TIPS2			32			N/A			No Device									N/A		Cihaz Yok											N/A
12		ID_TIPS3			32			N/A			Modify Device									N/A		Cihazi Degistir											N/A
13		ID_SET				16			N/A			Set										N/A		Ayarla												N/A
14		ID_MODIY_FULL_NAME		16			N/A			Full Name									N/A		Tam Adi												N/A
15		ID_MODIY_ABBR_NAME		16			N/A			Abbr Name									N/A		Kisa Adi											N/A
16		ID_INDEX			16			N/A			Index										N/A		Icerik												N/A
17		ID_DEVICE_ABBR_NAME		32			N/A			Device Abbr Name								N/A		Cihaz Kisa Adi											N/A
18		ID_MODIFY_NAME_TYPE		32			N/A			Modify Name Type								N/A		Isim tipini degistir										N/A
19		ID_TOOLONG_NAME16		64			N/A			The length must be no more than 16 characters					N/A		Uzunluk max 16 karakter olmali									N/A
20		ID_TOOLONG_NAME32		64			N/A			The length must be no more than 32 characters					N/A		Uzunluk max 32 karakter olmali									N/A
21		ID_ERROR5			64			N/A			Failure, Controller is hardware protected					N/A		Hata. Denetci HW korumali									N/A
22		ID_INVALID_CHAR			64			N/A			Invalid character.								N/A		Gecersiz karakter										N/A
23		ID_TIPS4			128			N/A			[Tips]The new device name will be showed in device tree after you reconnect.	N/A		[Aviso]Cihaz adi yeniden baglanti yapildiktan sonra cihaz agacinda gosterilecektir!			N/A

[p29_online_modifyalarm.htm:Number]
48

[p29_online_modifyalarm.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN	FULL_IN_LOCALE						ABBR_IN_LOCALE
1		ID_ERROR0		32			N/A			Failure.					N/A		Hata.							N/A
2		ID_ERROR1		32			N/A			Success.					N/A		Basarili.						N/A
3		ID_ERROR2		32			N/A			Unknown error.   				N/A		Bilinmeyen Hata.						N/A
4		ID_ERROR3		32			N/A			Failure, you are not authorized.		N/A		Hata. Yetkiniz yok.					N/A
5		ID_ERROR4		32			N/A			Failed to communicate				N/A		Haberlesme hatasi					N/A
6		ID_ERROR5		64			N/A			Failure, Controller is hardware protected	N/A		Hata. Denetci HW korumali				N/A
7		ID_ALARM_HEAD		32			N/A			Query Device Type				N/A		Sorgulama Cihaz Tipi					N/A
8		ID_SINGAL_EXPLORE	32			N/A			Explore Signals					N/A		Sinyali Arastir						N/A
9		ID_INDEX		32			N/A			Index						N/A		Icerik							N/A
10		ID_SIGNAL_NAME		32			N/A			Full Signal Name				N/A		Sinyal Tam Adi						N/A
11		ID_ALARM_LEVEL		32			N/A			Alarm Level					N/A		Alarm seviyesi						N/A
12		ID_NEW_NAME		32			N/A			New Name					N/A		Yeni Isim						N/A
13		ID_NEW_LEVEL		32			N/A			New Level					N/A		Yeni seviye						N/A
14		ID_SET			8			N/A			Set						N/A		Ayarla							N/A
15		ID_INDEX		16			N/A			Index						N/A		Icerik							N/A
16		ID_SIGNAL_NAME		32			N/A			Full Signal Name				N/A		Sinyal Tam Adi						N/A
17		ID_NEW_NAME		16			N/A			New Name					N/A		Yeni isim						N/A
18		ID_SET			8			N/A			Set						N/A		Ayarla							N/A
19		ID_DEVICE_NAME		16			N/A			Device						N/A		Cihaz							N/A
20		ID_TYPE			16			N/A			Type						N/A		Tip							N/A
21		ID_NA			16			N/A			NA						N/A		NA							N/A
22		ID_OA			16			N/A			MI						N/A		Minor							N/A
23		ID_MA			16			N/A			MA						N/A		Major							N/A
24		ID_CA			16			N/A			CA						N/A		Kritik							N/A
25		ID_SAMPLE_SIGNAL	32			N/A			Status Signal					N/A		Durum Sinyali						N/A
26		ID_CONTROL_SIGNAL	32			N/A			Control Signal					N/A		Kontrol Sinyali						N/A
27		ID_SETTING_SIGNAL	32			N/A			Setting Signal					N/A		Ayarlama Sinyali					N/A
28		ID_ALARM_SIGNAL		32			N/A			Alarm Signal					N/A		Alarm Sinyali						N/A
29		ID_NO_SAMPLE		32			N/A			No Status Signal				N/A		Durum Sinyali Yok					N/A
30		ID_NO_CONTROL		32			N/A			No Control Signal				N/A		Kontrol Sinyali Yok					N/A
31		ID_NO_SETTING		32			N/A			No Setting Signal				N/A		Ayarlama Sinyali Yok					N/A
32		ID_NO_ALARM		32			N/A			No Alarm Signal					N/A		Alarm Sinyali Yok					N/A
33		ID_NO_SIGNAL_TYPE	32			N/A			No this signal type				N/A		Bu sinyal tipi yok					N/A
34		ID_VAR_SET		8			N/A			Set						N/A		Ayarla							N/A
35		ID_SIGNAL_TYPE		32			N/A			Signal Type					N/A		Sinyal Tipi						N/A
36		ID_SHOW_TIPS0		32			N/A			The new name can't be blank.			N/A		Yeni Isim bos olamaz					N/A
37		ID_SET			8			N/A			Set						N/A		Ayarla							N/A
38		ID_SIGNAL_ABBR_NAME	32			N/A			Signal Abbr Name				N/A		Sinyal Kisa Adi						N/A
39		ID_SIGNAL_ABBR_NAME	32			N/A			Signal Abbr Name				N/A		Sinyal Kisa Adi						N/A
40		ID_MODIY_FULL_NAME	32			N/A			Full Name					N/A		Tam Adi						N/A
41		ID_MODIY_ABBR_NAME	32			N/A			Abbr Name					N/A		Kisa Adi						N/A
42		ID_MODIY_FULL_NAME	32			N/A			Full Name					N/A		Tam Adi							N/A
43		ID_MODIY_ABBR_NAME	32			N/A			Abbr Name					N/A		Kisa Adi						N/A
44		ID_NEW_NAME_TYPE	32			N/A			Modify type					N/A		Tipi degistir						N/A
45		ID_NEW_NAME_TYPE	32			N/A			Modify type					N/A		Tipi degistir						N/A
46		ID_INVALID_CHAR		64			N/A			Invalid character				N/A		Gecersiz Karakter					N/A
47		ID_TOOLONG_NAME16	64			N/A			The length must be no more than 16 characters	N/A		Uzunluk max 16 karater olmali				N/A
48		ID_TOOLONG_NAME32	64			N/A			The length must be no more than 32 characters	N/A		Uzunluk max 32 karater olmali				N/A

[p30_acu_signal_value.htm:Number]
13

[p30_acu_signal_value.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL		MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN	FULL_IN_LOCALE			ABBR_IN_LOCALE
1		ID_ERROR0		32			N/A			Failure.					N/A		Hata.				N/A
2		ID_ERROR1		32			N/A			Success.					N/A		Basarili.			N/A
3		ID_ERROR2		32			N/A			Unknown error.					N/A		Bilinmeyen Hata			N/A
4		ID_ERROR3		32			N/A			Failure, you are not authorized.		N/A		Hata. Yetkiniz yok!		N/A
5		ID_ERROR4		32			N/A			Failed to communicate				N/A		Haberlesme hatasi		N/A
6		ID_ERROR5		32			N/A			Failure, Controller is hardware protected	N/A		Hata. Denetci HW korumali	N/A
7		ID_TIPS2		32			N/A			No device					N/A		Cihaz yok			N/A
8		ID_DEVICE		32			N/A			Equipment					N/A		Donanim				N/A
9		ID_PRODUCT_NUMBER	32			N/A			Product Number					N/A		Urun No				N/A
10		ID_PRODUCT_VERSION	32			N/A			Product Revision				N/A		Urun Revizyonu			N/A
11		ID_PRODUCT_SERIAL	32			N/A			Serial Number					N/A		Seri Numarasi				N/A
12		ID_PRODUCT_SWVERSION	32			N/A			Software Revision				N/A		Yazilim Revizyonu		N/A
13		ID_PRODUCT_INFO_HEAD	32			N/A			Product Information				N/A		Urun Bilgisi			N/A

[p31_close_system.htm:Number]
9

[p31_close_system.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN								ABBR_IN_EN	FULL_IN_LOCALE						ABBR_IN_LOCALE
1		ID_HEAD			16			N/A			Stop Controller								N/A		Denetciyi durdur						N/A
2		ID_TIPS			128			N/A			Upload/Download needs to stop the controller. Do you want to stop the controller?	N/A		Yukleme/indirme islemi icin denetci durdurulmalidir. Durdurmak istiyor musunuz?		N/A
3		ID_CLOSE_SCUP		16			N/A			Stop Controller								N/A		Denetciyi durdur						N/A
4		ID_CANCEL		16			N/A			Cancel									N/A		Iptal								N/A
5		ID_ERROR0		32			N/A			Unknown error.								N/A		Bilinmeyen hata.						N/A
6		ID_ERROR1		128			N/A			Controller was stopped successfully. You can download file.		N/A		Denetci basarili sekilde durduruldu. Dosyayi indirebilirsiniz.	N/A
7		ID_ERROR2		64			N/A			Fail to stop Controller.						N/A		Dentciyi durdurma hatali.					N/A
8		ID_ERROR3		64			N/A			You are not authorized to stop the controller.				N/A		Denetciyi durdurma yetkiniz yok.				N/A
9		ID_ERROR4		64			N/A			Failed to communicate with the controller.				N/A		Denetci ile haberlesme hatasi.					N/A

[p32_start_system.htm:Number]
9


[p32_start_system.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN								ABBR_IN_EN	FULL_IN_LOCALE					ABBR_IN_LOCALE
1		ID_HEAD			16			N/A			Start Controller							N/A		Denetciyi Baslat						N/A
2		ID_TIPS			128			N/A			Download is ready. Do you want to start Controller?			N/A		Indirmeye hazir. Denetciyi baslatmak ister misiniz?		N/A
3		ID_START_SCUP		16			N/A			Start Controller							N/A		Denetciyi Baslat						N/A
4		ID_CANCEL		16			N/A			Cancel									N/A		Iptal								N/A
5		ID_ERROR0		32			N/A			Unknown error.								N/A		Bilinmeyen hata.						N/A
6		ID_ERROR1		128			N/A			Controller was started successfully. You can connect in 1 minute.	N/A		Denetci basarili sekilde baslatildi. 1 dk icinde baglanabilirsiniz.	N/A
7		ID_ERROR2		64			N/A			Fail to stop the controller.						N/A		Denetciyi durdurma hatasi						N/A
8		ID_ERROR3		64			N/A			You are not authorized to start the controller!				N/A		Denetciyi baslatma yetkiniz yok!				N/A
9		ID_ERROR4		64			N/A			Failed to communicate with the controller.				N/A		Denetci ile haberlesme hatasi					N/A

[p33_replace_file.htm:Number]
43


[p33_replace_file.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN							ABBR_IN_EN	FULL_IN_LOCALE						ABBR_IN_LOCALE
1		ID_REPLACE_HEAD		16			N/A			Replace File							N/A		Dosyayi degistir					N/A
2		ID_REPLACE_UPLOADED	32			N/A			Has been downloaded						N/A		Yuklendi						N/A
3		ID_SELECE_FILE_TYPE	64			N/A			Please select download file type				N/A		Indirilecek dosya tipini seciniz			N/A
4		ID_FILE_CONFIGURE	16			N/A			Configuration File						N/A		Konfig Dosyasi						N/A
5		ID_FILE_APPLICATION	32			N/A			Application File						N/A		Uygulama Dosyasi					N/A
6		ID_SELECT_REPLACE_FILE	64			N/A			Please select the file that is to be replaced			N/A		Degistirilecek dosyayi seciniz				N/A
7		ID_REPLACE		16			N/A			Replace								N/A		Degistir						N/A
8		ID_RETURN		16			N/A			Return								N/A		Geri Don						N/A
9		ID_DOWNLOAD_HEAD	32			N/A			Upload File							N/A		Dosya Yukle						N/A
10		ID_FILE1		32			N/A			Select File							N/A		Dosya Sec						N/A
11		ID_FILE0		32			N/A			File in Controller						N/A		Denetcideki dosyalar					N/A
12		ID_ERROR0		32			N/A			Unknown error.							N/A		Bilinmeyen hata						N/A
13		ID_ERROR1		64			N/A			File successfully uploaded.					N/A		Dosya yukleme basarili!					N/A
14		ID_ERROR2		64			N/A			Failed to download file.					N/A		Dosya indirme hatasi!					N/A
15		ID_ERROR3		64			N/A			Failed to download file, the file is too large.			N/A		Dosya indirme hatasi. Dosya cok buyuk!			N/A
16		ID_ERROR4		64			N/A			Failure, you are not authorized.				N/A		Hata. Yetkiniz yok!					N/A
17		ID_ERROR5		64			N/A			Controller successfully started.				N/A		Denetci basarili sekilde baslatildi			N/A
18		ID_ERROR6		64			N/A			Successfully to download file.					N/A		Dosya indirme basarili!					N/A
19		ID_ERROR7		64			N/A			Failed to download file.					N/A		Dosya indirme hatasi!					N/A
20		ID_ERROR8		64			N/A			Failed to upload file.						N/A		Dosya yukleme hatasi!					N/A
21		ID_ERROR9		64			N/A			File successfully uploaded.					N/A		Dosya yukleme basarili!				N/A
22		ID_UPLOAD		64			N/A			Upload								N/A		Yukle							N/A
23		ID_DOWNLOAD		64			N/A			Upload								N/A		Yukle							N/A
24		ID_STARTSCUP		64			N/A			Start Controller						N/A		Denetciyi baslat					N/A
25		ID_SHOWTIPS0		64			N/A			Are you sure you want to start the Controller?			N/A		Denetciyi baslatmak istediginize emin misiniz?		N/A
26		ID_SHOWTIPS1		128			N/A			Please reboot Controller before you leave this page. \n Are you sure to leave?		N/A	Bu sayfayi kapatmadan once denetciyi yeniden baslatin. \n Cikmak istediginize emin misiniz?	N/A
27		ID_SHOWTIPS2		128			N/A			Controller will reboot. Close this IE and wait a couple of minutes before connecting.	N/A	Denetci yeniden baslatilacak. Bu sayfayi kapatin ve baglanmadan once birkac dk bekleyin.		N/A
28		ID_SHOWTIPS3		64			N/A			It's time to start the controller.					N/A		Denetciyi baslatma zamani			N/A
29		ID_SHOWTIPS4		64			N/A			This format isn't supported, select again please.		N/A		Bu format Bu format desteklenmiyor, lutfen yeniden seciniz	N/A
30		ID_CONFIG_TAR		32			N/A			Configuration Package						N/A		Konfig Paketi						N/A
31		ID_LANG_TAR		32			N/A			Language Package						N/A		Dil Paketi						N/A
32		ID_PROGRAM_TAR		32			N/A			Program package							N/A		Program Paketi						N/A
33		ID_SHOWTIPS5		64			N/A			The file name can't be blank.					N/A		Dosya ismi bos olamaz!					N/A
34		ID_SHOWTIPS6		64			N/A			Are you sure you want to upload					N/A		Yuklemek istediginize emin misiniz?			N/A
35		ID_SHOWTIPS7		128			N/A			Incorrect file type or unallowed character included. Please upload *.tar.gz or *.tar.		N/A	Yanlis dosya tipi yada izin verilmeyen karakter iceriyor. Lutfen *tar.gz veya *.tar formatinda yukleyiniz!		N/A
36		ID_SHOWTIPS8		128			N/A			Are you sure you want to start Controller?			N/A		Denetciyi baslatmak istediginize emin misiniz ?		N/A
37		ID_TIPS6		64			N/A			Please wait, Controller is rebooting...				N/A		Denetci Yeniden baslatiliyor, lutfen bekleyiniz...	N/A
38		ID_TIPS7		64			N/A			Since parameters have been modified, Controller is rebooting...	N/A		Parametre degisikligi oldugundan denetci yeniden baslatiliyor...			N/A
39		ID_TIPS8		64			N/A			Controller homepage will be refreshed after			N/A		Denetci anasayfasi yenilenecek	N/A
40		ID_SOLUTION_FILE	64			N/A			Solution File							N/A		Cozum dosyasi					N/A
41		ID_UPLOAD		64			N/A			Download							N/A		Indir						N/A
42		ID_SHOWTIPS9		512			N/A			Caution: Only Configure Package (file format of tar or tar.gz) can be uploaded.If the uploaded file is NOT correct, the controller will run abnormally. The controller must be restarted manually after the upload.		N/A		Dikkat: Konfig paketi sadece (tar veya tar.gz formatinda) yuklenebilir.Eger yuklenen dosya dogru degilse, denetci anormal calisir. Denetci manuel olarak yeniden baslatilmalidir.!		N/A
43		ID_ERROR10		64			N/A			Failed to upload file, the controller is hardware protected.	N/A		Dosya yukleme hatasi. denetci HW korumali!		N/A


[p34_history_batterylogquery.htm:Number]
285


[p34_history_batterylogquery.htm]
#Sequence ID	RES_ID					MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN						ABBR_IN_EN	FULL_IN_LOCALE						ABBR_IN_LOCALE
1		ID_BATTERY_TEST_START_REASON0		64			N/A			Planned battery test.					N/A		Planlanan Aku testi					N/A
2		ID_BATTERY_TEST_START_REASON1		64			N/A			Manual start of battery test				N/A		Aku testi manuel baslat					N/A
3		ID_BATTERY_TEST_START_REASON2		64			N/A			Mains fail start of battery test			N/A		AC Kesik aku testi baslat				N/A
4		ID_BATTERY_TEST_START_REASON3		64			N/A			Master controlled start of battery test			N/A		Master kontrollu aku testi baslat			N/A
5		ID_BATTERY_TEST_START_REASON4		64			N/A			Other reason						N/A		Baska Sebep						N/A
6		ID_BATTERY_TEST_END_REASON0		64			N/A			Manual stop of test					N/A		Testi manuel olarak bitir					N/A
7		ID_BATTERY_TEST_END_REASON1		64			N/A			Test stopped due to alarm				N/A		Test Alarmdan dolayi durdu				N/A
8		ID_BATTERY_TEST_END_REASON2		64			N/A			Test stopped due to time-out				N/A		Test Zaman asimindan dolayi durdu			N/A
9		ID_BATTERY_TEST_END_REASON3		64			N/A			Test stopped due to capacity limit exceeded		N/A		Test kapasite Limitinden dolayi durdu			N/A
10		ID_BATTERY_TEST_END_REASON4		64			N/A			Test stopped due to voltage limit exceeded		N/A		Test gerilim Limitinden dolayi durdu			N/A
11		ID_BATTERY_TEST_END_REASON5		64			N/A			Test stopped due to mains fail				N/A		Test Ac kesintiden dolayi durdu				N/A
12		ID_BATTERY_TEST_END_REASON6		64			N/A			Mains fail test stopped due to mains restored		N/A		AC Kesik Aku testi AC gelidiginden durdu		N/A
13		ID_BATTERY_TEST_END_REASON7		64			N/A			Mains fail test stopped due to test disabled		N/A		AC Kesik Aku testi pasiflestirildiginden durdu		N/A
14		ID_BATTERY_TEST_END_REASON8		64			N/A			Master controlled stop of battery test			N/A		Master kontrollu aku testi bitir			N/A
15		ID_BATTERY_TEST_END_REASON9		64			N/A			PowerSplit test stopped due to manual mode		N/A		GucPaylasimiTesti manuel moddan dolayi durdu			N/A
16		ID_BATTERY_TEST_END_REASON10		64			N/A			PowerSplit test stopped due to automatic mode		N/A		GucPaylasimiTesti oto moddan dolayi durdu			N/A
17		ID_BATTERY_TEST_END_REASON11		64			N/A			Stopped due to other reason				N/A		Baska sebepten durdu					N/A
18		ID_BATTERY_TEST_RESULT0			16			N/A			No test result						N/A		Test Sonucu yok					N/A
19		ID_BATTERY_TEST_RESULT1			16			N/A			Battery is OK						N/A		Aku OK						N/A
20		ID_BATTERY_TEST_RESULT2			16			N/A			Battery is bad						N/A		Aku Kotu					N/A
21		ID_BATTERY_TEST_RESULT3			16			N/A			It's a Power Split Test					N/A		Bu gucPaylasimi Testi				N/A
22		ID_BATTERY_TEST_RESULT4			16			N/A			Other result						N/A		Baska Sonuc					N/A
23		ID_ERROR0				64			N/A			Unknown error.						N/A		Bilinmeyen Hata					N/A
24		ID_ERROR1				64			N/A			Acquired control log successfully.			N/A		Elde Eilen denetci Kaydi Basarili		N/A
25		ID_ERROR2				64			N/A			No data to be queried.					N/A		Sorgulanacak kayit yok				N/A
26		ID_ERROR3				64			N/A			Failure.						N/A		Hata!						N/A
27		ID_ERROR4				64			N/A			Failure, you are not authorized.			N/A		Hata. Yetkiniz yok.				N/A
28		ID_ERROR5				64			N/A			Failed to communicate with the controller.		N/A		Denetci ile Haberlesme Hatasi			N/A
29		ID_ERROR6				64			N/A			Acquired battery log successfully.			N/A		Elde Edilen Aku Kaydi Basarili		N/A
30		ID_ERROR7				64			N/A			Acquired System log successfully.			N/A		Elde Edilen Sistem Kaydi Basarili		N/A
31		ID_LOG_HEAD				16			N/A			Battery test log query					N/A		Aku Test Sorgusu				N/A
32		ID_TIPS					32			N/A			Select Battery Test log:				N/A		Aku Test Kaydi Sec:				N/A
33		ID_QUERY				16			N/A			Query							N/A		Sorgu						N/A
34		ID_HEAD0				32			N/A			Battery 1 Current					N/A		Aku Akim 1					N/A
35		ID_HEAD1				32			N/A			Battery 1 Voltage					N/A		Aku Gerilim 1					N/A
36		ID_HEAD2				32			N/A			Battery 1 Capacity					N/A		Aku Kapasite 1					N/A
37		ID_HEAD3				32			N/A			Battery 2 Current					N/A		Aku Akim 2					N/A
38		ID_HEAD4				32			N/A			Battery 2 Voltage					N/A		Aku Gerilim 2					N/A
39		ID_HEAD5				32			N/A			Battery 2 Capacity					N/A		Aku Kapasite2					N/A
40		ID_HEAD6				32			N/A			EIB1 Battery 1 Current					N/A		EIB1 Aku Akim 1					N/A
41		ID_HEAD7				32			N/A			EIB1 Battery 1 Voltage					N/A		EIB1 Aku Gerilim 1				N/A
42		ID_HEAD8				32			N/A			EIB1 Battery 1 Capacity					N/A		EIB1 Aku Kapasite 1				N/A
43		ID_HEAD9				32			N/A			EIB1 Battery 2 Current					N/A		EIB1 Aku Akim 2					N/A
44		ID_HEAD10				32			N/A			EIB1 Battery 2 Voltage					N/A		EIB1 Aku Gerilim 2				N/A
45		ID_HEAD11				32			N/A			EIB1 Battery 2 Capacity					N/A		EIB1 Aku Kapasite 2				N/A
46		ID_HEAD12				32			N/A			EIB2 Battery 1 Current					N/A		EIB2 Aku Akim 1					N/A
47		ID_HEAD13				32			N/A			EIB2 Battery 1 Voltage					N/A		EIB2 Aku Gerilim 1				N/A
48		ID_HEAD14				32			N/A			EIB2 Battery 1 Capacity					N/A		EIB2 Aku Kapasite 1				N/A
49		ID_HEAD15				32			N/A			EIB2 Battery 2 Current					N/A		EIB2 Aku Akim 2					N/A
50		ID_HEAD16				32			N/A			EIB2 Battery 2 Voltage					N/A		EIB2 Aku Gerilim 2				N/A
51		ID_HEAD17				32			N/A			EIB2 Battery 2 Capacity					N/A		EIB2 Aku Kapasite 2				N/A
52		ID_HEAD18				32			N/A			EIB3 Battery 1 Current					N/A		EIB3 Aku Akim 1					N/A
53		ID_HEAD19				32			N/A			EIB3 Battery 1 Voltage					N/A		EIB3 Aku Gerilim 1				N/A
54		ID_HEAD20				32			N/A			EIB3 Battery 1 Capacity					N/A		EIB3 Aku Kapasite 1				N/A
55		ID_HEAD21				32			N/A			EIB3 Battery 2 Current					N/A		EIB3 Aku Akim 2					N/A
56		ID_HEAD22				32			N/A			EIB3 Battery 2 Voltage					N/A		EIB3 Aku Gerilim 2 				N/A
57		ID_HEAD23				32			N/A			EIB3 Battery 2 Capacity					N/A		EIB3 Aku Kapasite 2				N/A
58		ID_HEAD24				32			N/A			EIB4 Battery 1 Current					N/A		EIB4 Aku Akim 1					N/A
59		ID_HEAD25				32			N/A			EIB4 Battery 1 Voltage					N/A		EIB4 Aku Gerilim 1				N/A
60		ID_HEAD26				32			N/A			EIB4 Battery 1 Capacity					N/A		EIB4 Aku Kapasite 1				N/A
61		ID_HEAD27				32			N/A			EIB4 Battery 2 Current					N/A		EIB4 Aku Akim 2					N/A
62		ID_HEAD28				32			N/A			EIB4 Battery 2 Voltage					N/A		EIB4 Aku Gerilim 2				N/A
63		ID_HEAD29				32			N/A			EIB4 Battery 2 Capacity					N/A		EIB4 Aku Kapasite 2				N/A
64		ID_BATTERY_TEST_SUMMARY0		32			N/A			Index							N/A		Icerik						N/A
65		ID_BATTERY_TEST_SUMMARY1		32			N/A			Record Time						N/A		Kayit Zamani					N/A
66		ID_BATTERY_TEST_SUMMARY2		32			N/A			System Voltage						N/A		Sistem Gerilimi					N/A
67		ID_BATTERY_SUMMARY_HEAD0		32			N/A			Start Time						N/A		Baslama Zamani					N/A
68		ID_BATTERY_SUMMARY_HEAD1		32			N/A			End Time						N/A		Bitis Zamani					N/A
69		ID_BATTERY_SUMMARY_HEAD2		32			N/A			Start Reason						N/A		Baslama Nedeni					N/A
70		ID_BATTERY_SUMMARY_HEAD3		32			N/A			End Reason						N/A		Bitis Nedeni					N/A
71		ID_BATTERY_SUMMARY_HEAD4		32			N/A			Test Result						N/A		Test Sonucu					N/A
72		ID_DOWNLOAD				32			N/A			Download						N/A		Indir						N/A
73		ID_HEAD30				32			N/A			SMDU1 Battery 1 Current					N/A		SMDU1 Aku Akim 1				N/A
74		ID_HEAD31				32			N/A			SMDU1 Battery 1 Voltage					N/A		SMDU1 Aku Gerilim 1				N/A
75		ID_HEAD32				32			N/A			SMDU1 Battery 1 Capacity				N/A		SMDU1 Aku Kapasite 1				N/A
76		ID_HEAD33				32			N/A			SMDU1 Battery 2 Current					N/A		SMDU1 Aku Akim 2				N/A
77		ID_HEAD34				32			N/A			SMDU1 Battery 2 Voltage					N/A		SMDU1 Aku Gerilim 2				N/A
78		ID_HEAD35				32			N/A			SMDU1 Battery 2 Capacity				N/A		SMDU1 Aku Kapasite 2				N/A
79		ID_HEAD36				32			N/A			SMDU1 Battery 3 Current					N/A		SMDU1 Aku Akim 3				N/A
80		ID_HEAD37				32			N/A			SMDU1 Battery 3 Voltage					N/A		SMDU1 Aku Gerilim 3				N/A
81		ID_HEAD38				32			N/A			SMDU1 Battery 3 Capacity				N/A		SMDU1 Aku Kapasite 3				N/A
82		ID_HEAD39				32			N/A			SMDU1 Battery 4 Current					N/A		SMDU1 Aku Akim 4				N/A
83		ID_HEAD40				32			N/A			SMDU1 Battery 4 Voltage					N/A		SMDU1 Aku Gerilim 4				N/A
84		ID_HEAD41				32			N/A			SMDU1 Battery 4 Capacity				N/A		SMDU1 Aku Kapasite 4				N/A
85		ID_HEAD42				32			N/A			SMDU2 Battery 1 Current					N/A		SMDU2 Aku Akim 1				N/A
86		ID_HEAD43				32			N/A			SMDU2 Battery 1 Voltage					N/A		SMDU2 Aku Gerilim 1				N/A
87		ID_HEAD44				32			N/A			SMDU2 Battery 1 Capacity				N/A		SMDU2 Aku Kapasite 1				N/A
88		ID_HEAD45				32			N/A			SMDU2 Battery 2 Current					N/A		SMDU2 Aku Akim 2				N/A
89		ID_HEAD46				32			N/A			SMDU2 Battery 2 Voltage					N/A		SMDU2 Aku Gerilim 2				N/A
90		ID_HEAD47				32			N/A			SMDU2 Battery 2 Capacity				N/A		SMDU2 Aku Kapasite 2				N/A
91		ID_HEAD48				32			N/A			SMDU2 Battery 3 Current					N/A		SMDU2 Aku Akim 3				N/A
92		ID_HEAD49				32			N/A			SMDU2 Battery 3 Voltage					N/A		SMDU2 Aku Gerilim 3				N/A
93		ID_HEAD50				32			N/A			SMDU2 Battery 3 Capacity				N/A		SMDU2 Aku Kapasite 3				N/A
94		ID_HEAD51				32			N/A			SMDU2 Battery 4 Current					N/A		SMDU2 Aku Akim 4				N/A
95		ID_HEAD52				32			N/A			SMDU2 Battery 4 Voltage					N/A		SMDU2 Aku Gerilim 4				N/A
96		ID_HEAD53				32			N/A			SMDU2 Battery 4 Capacity				N/A		SMDU2 Aku Kapasitea 4				N/A
97		ID_HEAD54				32			N/A			SMDU3 Battery 1 Current					N/A		SMDU3 Aku Akim 1				N/A
98		ID_HEAD55				32			N/A			SMDU3 Battery 1 Voltage					N/A		SMDU3 Aku Gerilim 1				N/A
99		ID_HEAD56				32			N/A			SMDU3 Battery 1 Capacity				N/A		SMDU3 Aku Kapasite 1				N/A
100		ID_HEAD57				32			N/A			SMDU3 Battery 2 Current					N/A		SMDU3 Aku Akim 2				N/A
101		ID_HEAD58				32			N/A			SMDU3 Battery 2 Voltage					N/A		SMDU3 Aku Gerilim 2				N/A
102		ID_HEAD59				32			N/A			SMDU3 Battery 2 Capacity				N/A		SMDU3 Aku Kapasite 2				N/A
103		ID_HEAD60				32			N/A			SMDU3 Battery 3 Current					N/A		SMDU3 Aku Akim 3				N/A
104		ID_HEAD61				32			N/A			SMDU3 Battery 3 Voltage					N/A		SMDU3 Aku Gerilim 3				N/A
105		ID_HEAD62				32			N/A			SMDU3 Battery 3 Capacity				N/A		SMDU3 Aku Kapasite 3				N/A
106		ID_HEAD63				32			N/A			SMDU3 Battery 4 Current					N/A		SMDU3 Aku Akim 4				N/A
107		ID_HEAD64				32			N/A			SMDU3 Battery 4 Voltage					N/A		SMDU3 Aku Gerilim 4				N/A
108		ID_HEAD65				32			N/A			SMDU3 Battery 4 Capacity				N/A		SMDU3 Aku Kapasite 4				N/A
109		ID_HEAD66				32			N/A			SMDU4 Battery 1 Current					N/A		SMDU4 Aku Akim 1				N/A
110		ID_HEAD67				32			N/A			SMDU4 Battery 1 Voltage					N/A		SMDU4 Aku Gerilim 1				N/A
111		ID_HEAD68				32			N/A			SMDU4 Battery 1 Capacity				N/A		SMDU4 Aku Kapasite 1				N/A
112		ID_HEAD69				32			N/A			SMDU4 Battery 2 Current					N/A		SMDU4 Aku Akim 2				N/A
113		ID_HEAD70				32			N/A			SMDU4 Battery 2 Voltage					N/A		SMDU4 Aku Gerilim 2				N/A
114		ID_HEAD71				32			N/A			SMDU4 Battery 2 Capacity				N/A		SMDU4 Aku Kapasite 2				N/A
115		ID_HEAD72				32			N/A			SMDU4 Battery 3 Current					N/A		SMDU4 Aku Akim 3				N/A
116		ID_HEAD73				32			N/A			SMDU4 Battery 3 Voltage					N/A		SMDU4 Aku Gerilim 3				N/A
117		ID_HEAD74				32			N/A			SMDU4 Battery 3 Capacity				N/A		SMDU4 Aku Kapasite 3				N/A
118		ID_HEAD75				32			N/A			SMDU4 Battery 4 Current					N/A		SMDU4 Aku Akim 4				N/A
119		ID_HEAD76				32			N/A			SMDU4 Battery 4 Voltage					N/A		SMDU4 Aku Gerilim 4				N/A
120		ID_HEAD77				32			N/A			SMDU4 Battery 4 Capacity				N/A		SMDU4 Aku Kapasite 4				N/A
121		ID_HEAD78				32			N/A			SMDU5 Battery 1 Current					N/A		SMDU5 Aku Akim 1				N/A
122		ID_HEAD79				32			N/A			SMDU5 Battery 1 Voltage					N/A		SMDU5 Aku Gerilim 1				N/A
123		ID_HEAD80				32			N/A			SMDU5 Battery 1 Capacity				N/A		SMDU5 Aku Kapasite 1				N/A
124		ID_HEAD81				32			N/A			SMDU5 Battery 2 Current					N/A		SMDU5 Aku Akim 2				N/A
125		ID_HEAD82				32			N/A			SMDU5 Battery 2 Voltage					N/A		SMDU5 Aku Gerilim 2				N/A
126		ID_HEAD83				32			N/A			SMDU5 Battery 2 Capacity				N/A		SMDU5 Aku Kapasite 2				N/A
127		ID_HEAD84				32			N/A			SMDU5 Battery 3 Current					N/A		SMDU5 Aku Akim 3				N/A
128		ID_HEAD85				32			N/A			SMDU5 Battery 3 Voltage					N/A		SMDU5 Aku Gerilim 3				N/A
129		ID_HEAD86				32			N/A			SMDU5 Battery 3 Capacity				N/A		SMDU5 Aku Kapasite 3				N/A
130		ID_HEAD87				32			N/A			SMDU5 Battery 4 Current					N/A		SMDU5 Aku Akim 4				N/A
131		ID_HEAD88				32			N/A			SMDU5 Battery 4 Voltage					N/A		SMDU5 Aku Gerilim 4				N/A
132		ID_HEAD89				32			N/A			SMDU5 Battery 4 Capacity				N/A		SMDU5 Aku Kapasite 4				N/A
133		ID_HEAD90				32			N/A			SMDU6 Battery 1 Current					N/A		SMDU6 Aku Akim 1				N/A
134		ID_HEAD91				32			N/A			SMDU6 Battery 1 Voltage					N/A		SMDU6 Aku Gerilim 1				N/A
135		ID_HEAD92				32			N/A			SMDU6 Battery 1 Capacity				N/A		SMDU6 Aku Kapasite 1				N/A
136		ID_HEAD93				32			N/A			SMDU6 Battery 2 Current					N/A		SMDU6 Aku Akim 2				N/A
137		ID_HEAD94				32			N/A			SMDU6 Battery 2 Voltage					N/A		SMDU6 Aku Gerilim 2				N/A
138		ID_HEAD95				32			N/A			SMDU6 Battery 2 Capacity				N/A		SMDU6 Aku Kapasite 2				N/A
139		ID_HEAD96				32			N/A			SMDU6 Battery 3 Current					N/A		SMDU6 Aku Akim 3				N/A
140		ID_HEAD97				32			N/A			SMDU6 Battery 3 Voltage					N/A		SMDU6 Aku Gerilim 3				N/A
141		ID_HEAD98				32			N/A			SMDU6 Battery 3 Capacity				N/A		SMDU6 Aku Kapasite 3				N/A
142		ID_HEAD99				32			N/A			SMDU6 Battery 4 Current					N/A		SMDU6 Aku Akim 4				N/A
143		ID_HEAD100				32			N/A			SMDU6 Battery 4 Voltage					N/A		SMDU6 Aku Gerilim 4				N/A
144		ID_HEAD101				32			N/A			SMDU6 Battery 4 Capacity				N/A		SMDU6 Aku Kapasite 4				N/A
145		ID_HEAD102				32			N/A			SMDU7 Battery 1 Current					N/A		SMDU7 Aku Akim 1				N/A
146		ID_HEAD103				32			N/A			SMDU7 Battery 1 Voltage					N/A		SMDU7 Aku Gerilim 1				N/A
147		ID_HEAD104				32			N/A			SMDU7 Battery 1 Capacity				N/A		SMDU7 Aku Kapasite 1				N/A
148		ID_HEAD105				32			N/A			SMDU7 Battery 2 Current					N/A		SMDU7 Aku Akim 2				N/A
149		ID_HEAD106				32			N/A			SMDU7 Battery 2 Voltage					N/A		SMDU7 Aku Gerilim 2				N/A
150		ID_HEAD107				32			N/A			SMDU7 Battery 2 Capacity				N/A		SMDU7 Aku Kapasite 2				N/A
151		ID_HEAD108				32			N/A			SMDU7 Battery 3 Current					N/A		SMDU7 Aku Akim 3				N/A
152		ID_HEAD109				32			N/A			SMDU7 Battery 3 Voltage					N/A		SMDU7 Aku Gerilim 3				N/A
153		ID_HEAD110				32			N/A			SMDU7 Battery 3 Capacity				N/A		SMDU7 Aku Kapasite 3				N/A
154		ID_HEAD111				32			N/A			SMDU7 Battery 4 Current					N/A		SMDU7 Aku Akim 4				N/A
155		ID_HEAD112				32			N/A			SMDU7 Battery 4 Voltage					N/A		SMDU7 Aku Gerilim 4				N/A
156		ID_HEAD113				32			N/A			SMDU7 Battery 4 Capacity				N/A		SMDU7 Aku Kapasite 4				N/A
157		ID_HEAD114				32			N/A			SMDU8 Battery 1 Current					N/A		SMDU8 Aku Akim 1				N/A
158		ID_HEAD115				32			N/A			SMDU8 Battery 1 Voltage					N/A		SMDU8 Aku Gerilim 1				N/A
159		ID_HEAD116				32			N/A			SMDU8 Battery 1 Capacity				N/A		SMDU8 Aku Kapasite 1				N/A
160		ID_HEAD117				32			N/A			SMDU8 Battery 2 Current					N/A		SMDU8 Aku Akim 2				N/A
161		ID_HEAD118				32			N/A			SMDU8 Battery 2 Voltage					N/A		SMDU8 Aku Gerilim 2				N/A
162		ID_HEAD119				32			N/A			SMDU8 Battery 2 Capacity				N/A		SMDU8 Aku Kapasite 2				N/A
163		ID_HEAD120				32			N/A			SMDU8 Battery 3 Current					N/A		SMDU8 Aku Akim 3				N/A
164		ID_HEAD121				32			N/A			SMDU8 Battery 3 Voltage					N/A		SMDU8 Aku Gerilim 3				N/A
165		ID_HEAD122				32			N/A			SMDU8 Battery 3 Capacity				N/A		SMDU8 Aku Kapasite 3				N/A
166		ID_HEAD123				32			N/A			SMDU8 Battery 4 Current					N/A		SMDU8 Aku Akim 4				N/A
167		ID_HEAD124				32			N/A			SMDU8 Battery 4 Voltage					N/A		SMDU8 Aku Gerilim 4				N/A
168		ID_HEAD125				32			N/A			SMDU8 Battery 4 Capacity				N/A		SMDU8 Aku Kapasite 4				N/A
169		ID_HEAD126				32			N/A			EIB1 Battery 3 Current					N/A		EIB1 Aku Akim 3					N/A
170		ID_HEAD127				32			N/A			EIB1 Battery 3 Voltage					N/A		EIB1 Aku Gerilim 3				N/A
171		ID_HEAD128				32			N/A			EIB1 Battery 3 Capacity					N/A		EIB1 Aku Kapasite 3				N/A
172		ID_HEAD129				32			N/A			EIB2 Battery 3 Current					N/A		EIB2 Aku Akim  3				N/A
173		ID_HEAD130				32			N/A			EIB2 Battery 3 Voltage					N/A		EIB2 Aku Gerilim 3				N/A
174		ID_HEAD131				32			N/A			EIB2 Battery 3 Capacity					N/A		EIB2 Cku Kapasite 3				N/A
175		ID_HEAD132				32			N/A			EIB3 Battery 3 Current					N/A		EIB3 Aku Akim  3				N/A
176		ID_HEAD133				32			N/A			EIB3 Battery 3 Voltage					N/A		EIB3 Aku Gerilim 3				N/A
177		ID_HEAD134				32			N/A			EIB3 Battery 3 Capacity					N/A		EIB3 Cku Kapasite 3				N/A
178		ID_HEAD135				32			N/A			EIB4 Battery 3 Current					N/A		EIB4 Aku Akim  3				N/A
179		ID_HEAD136				32			N/A			EIB4 Battery 3 Voltage					N/A		EIB4 Aku Gerilim 3				N/A
180		ID_HEAD137				32			N/A			EIB4 Battery 3 Capacity					N/A		EIB4 Cku Kapasite 3				N/A
181		ID_HEAD138				32			N/A			EIB1 Block 1 Voltage					N/A		EIB1 Gerilim 1					N/A
182		ID_HEAD139				32			N/A			EIB1 Block 2 Voltage					N/A		EIB1 Gerilim 2					N/A
183		ID_HEAD140				32			N/A			EIB1 Block 3 Voltage					N/A		EIB1 Gerilim 3					N/A
184		ID_HEAD141				32			N/A			EIB1 Block 4 Voltage					N/A		EIB1 Gerilim 4					N/A
185		ID_HEAD142				32			N/A			EIB1 Block 5 Voltage					N/A		EIB1 Gerilim 5					N/A
186		ID_HEAD143				32			N/A			EIB1 Block 6 Voltage					N/A		EIB1 Gerilim 6					N/A
187		ID_HEAD144				32			N/A			EIB1 Block 7 Voltage					N/A		EIB1 Gerilim 7					N/A
188		ID_HEAD145				32			N/A			EIB1 Block 8 Voltage					N/A		EIB1 Gerilim 8					N/A
189		ID_HEAD146				32			N/A			EIB2 Block 1 Voltage					N/A		EIB2 Gerilim 1					N/A
190		ID_HEAD147				32			N/A			EIB2 Block 2 Voltage					N/A		EIB2 Gerilim 2					N/A
191		ID_HEAD148				32			N/A			EIB2 Block 3 Voltage					N/A		EIB2 Gerilim 3					N/A
192		ID_HEAD149				32			N/A			EIB2 Block 4 Voltage					N/A		EIB2 Gerilim 4					N/A
193		ID_HEAD150				32			N/A			EIB2 Block 5 Voltage					N/A		EIB2 Gerilim 5					N/A
194		ID_HEAD151				32			N/A			EIB2 Block 6 Voltage					N/A		EIB2 Gerilim 6					N/A
195		ID_HEAD152				32			N/A			EIB2 Block 7 Voltage					N/A		EIB2 Gerilim 7					N/A
196		ID_HEAD153				32			N/A			EIB2 Block 8 Voltage					N/A		EIB2 Gerilim 8					N/A
197		ID_HEAD154				32			N/A			EIB3 Block 1 Voltage					N/A		EIB3 Gerilim 1					N/A
198		ID_HEAD155				32			N/A			EIB3 Block 2 Voltage					N/A		EIB3 Gerilim 2					N/A
199		ID_HEAD156				32			N/A			EIB3 Block 3 Voltage					N/A		EIB3 Gerilim 3					N/A
200		ID_HEAD157				32			N/A			EIB3 Block 4 Voltage					N/A		EIB3 Gerilim 4					N/A
201		ID_HEAD158				32			N/A			EIB3 Block 5 Voltage					N/A		EIB3 Gerilim 5					N/A
202		ID_HEAD159				32			N/A			EIB3 Block 6 Voltage					N/A		EIB3 Gerilim 6					N/A
203		ID_HEAD160				32			N/A			EIB3 Block 7 Voltage					N/A		EIB3 Gerilim 7					N/A
204		ID_HEAD161				32			N/A			EIB3 Block 8 Voltage					N/A		EIB3 Gerilim 8					N/A
205		ID_HEAD162				32			N/A			EIB4 Block 1 Voltage					N/A		EIB4 Gerilim 1					N/A
206		ID_HEAD163				32			N/A			EIB4 Block 2 Voltage					N/A		EIB4 Gerilim 2					N/A
207		ID_HEAD164				32			N/A			EIB4 Block 3 Voltage					N/A		EIB4 Gerilim 3					N/A
208		ID_HEAD165				32			N/A			EIB4 Block 4 Voltage					N/A		EIB4 Gerilim 4					N/A
209		ID_HEAD166				32			N/A			EIB4 Block 5 Voltage					N/A		EIB4 Gerilim 5					N/A
210		ID_HEAD167				32			N/A			EIB4 Block 6 Voltage					N/A		EIB4 Gerilim 6					N/A
211		ID_HEAD168				32			N/A			EIB4 Block 7 Voltage					N/A		EIB4 Gerilim 7					N/A
212		ID_HEAD169				32			N/A			EIB4 Block 8 Voltage					N/A		EIB4 Gerilim 8					N/A
213		ID_HEAD170				32			N/A			Temperature 1						N/A		Sicaklik 1						N/A
214		ID_HEAD171				32			N/A			Temperature 2						N/A		Sicaklik 2						N/A
215		ID_HEAD172				32			N/A			Temperature 3						N/A		Sicaklik 3						N/A
216		ID_HEAD173				32			N/A			Temperature 4						N/A		Sicaklik 4						N/A
217		ID_HEAD174				32			N/A			Temperature 5						N/A		Sicaklik 5						N/A
218		ID_HEAD175				32			N/A			Temperature 6						N/A		Sicaklik 6						N/A
219		ID_HEAD176				32			N/A			Temperature 7						N/A		Sicaklik 7						N/A
220		ID_HEAD177				32			N/A			Battery 1 Current					N/A		Aku 1 Akim						N/A
221		ID_HEAD178				32			N/A			Battery 1 Voltage					N/A		Aku 1 Gerilim						N/A
222		ID_HEAD179				32			N/A			Battery 1 Capacity					N/A		Aku 1 Kapasite						N/A
223		ID_HEAD180				32			N/A			LargeDUBattery 1 Current				N/A		Genis Dagitim Aku 1 Akim				N/A
224		ID_HEAD181				32			N/A			LargeDUBattery 1 Voltage				N/A		Genis Dagitim Aku 1 Gerilim				N/A
225		ID_HEAD182				32			N/A			LargeDUBattery 1 Capacity				N/A		Genis Dagitim Aku 1 Kapasite				N/A
226		ID_HEAD183				32			N/A			LargeDUBattery 2 Current				N/A		UGenis Dagitim Aku 2 Akim				N/A
227		ID_HEAD184				32			N/A			LargeDUBattery 2 Voltage				N/A		Genis Dagitim Aku 2 Gerilim				N/A
228		ID_HEAD185				32			N/A			LargeDUBattery 2 Capacity				N/A		Genis Dagitim Aku 2 Kapasite				N/A
229		ID_HEAD186				32			N/A			LargeDUBattery 3 Current				N/A		Genis Dagitim Aku 3 Akim				N/A
230		ID_HEAD187				32			N/A			LargeDUBattery 3 Voltage				N/A		Genis Dagitim Aku 3 Gerilim				N/A
231		ID_HEAD188				32			N/A			LargeDUBattery 3 Capacity				N/A		Genis Dagitim Aku 3 Kapasite				N/A
232		ID_HEAD189				32			N/A			LargeDUBattery 4 Current				N/A		Genis Dagitim Aku 4 Akim				N/A
233		ID_HEAD190				32			N/A			LargeDUBattery 4 Voltage				N/A		Genis Dagitim Aku 4 Gerilim				N/A
234		ID_HEAD191				32			N/A			LargeDUBattery 4 Capacity				N/A		Genis Dagitim Aku 4 Kapasite				N/A
235		ID_HEAD192				32			N/A			LargeDUBattery 5 Current				N/A		Genis Dagitim Aku 5 Akim				N/A
236		ID_HEAD193				32			N/A			LargeDUBattery 5 Voltage				N/A		Genis Dagitim Aku 5 Gerilim				N/A
237		ID_HEAD194				32			N/A			LargeDUBattery 5 Capacity				N/A		Genis Dagitim Aku 5 Kapasite				N/A
238		ID_HEAD195				32			N/A			LargeDUBattery 6 Current				N/A		Genis Dagitim Aku 6 Akim				N/A
239		ID_HEAD196				32			N/A			LargeDUBattery 6 Voltage				N/A		Genis Dagitim Aku 6 Gerilim				N/A
240		ID_HEAD197				32			N/A			LargeDUBattery 6 Capacity				N/A		Genis Dagitim Aku 6 Kapasite				N/A
241		ID_HEAD198				32			N/A			LargeDUBattery 7 Current				N/A		Genis Dagitim Aku 7 Akim				N/A
242		ID_HEAD199				32			N/A			LargeDUBattery 7 Voltage				N/A		Genis Dagitim Aku 7 Gerilim				N/A
243		ID_HEAD200				32			N/A			LargeDUBattery 7 Capacity				N/A		Genis Dagitim Aku 7 Kapasite				N/A
244		ID_HEAD201				32			N/A			LargeDUBattery 8 Current				N/A		Genis Dagitim Aku 8 Akim				N/A
245		ID_HEAD202				32			N/A			LargeDUBattery 8 Voltage				N/A		Genis Dagitim Aku 8 Gerilim				N/A
246		ID_HEAD203				32			N/A			LargeDUBattery 8 Capacity				N/A		Genis Dagitim Aku 8 Kapasite				N/A
247		ID_HEAD204				32			N/A			LargeDUBattery 9 Current				N/A		Genis Dagitim Aku 9 Akim				N/A
248		ID_HEAD205				32			N/A			LargeDUBattery 9 Voltage				N/A		Genis Dagitim Aku 9 Gerilim				N/A
249		ID_HEAD206				32			N/A			LargeDUBattery 9 Capacity				N/A		Genis Dagitim Aku 9 Kapasite				N/A
250		ID_HEAD207				32			N/A			LargeDUBattery 10 Current				N/A		Genis Dagitim Aku 10 Akim				N/A
251		ID_HEAD208				32			N/A			LargeDUBattery 10 Voltage				N/A		Genis Dagitim Aku 10 Gerilim				N/A
252		ID_HEAD209				32			N/A			LargeDUBattery 10 Capacity				N/A		Genis Dagitim Aku 10 Kapasite				N/A
253		ID_HEAD210				32			N/A			LargeDUBattery 11 Current				N/A		Genis Dagitim Aku 11 Akim				N/A
254		ID_HEAD211				32			N/A			LargeDUBattery 11 Voltage				N/A		Genis Dagitim Aku 11 Gerilim				N/A
255		ID_HEAD212				32			N/A			LargeDUBattery 11 Capacity				N/A		Genis Dagitim Aku 11 Kapasite				N/A
256		ID_HEAD213				32			N/A			LargeDUBattery 12 Current				N/A		Genis Dagitim Aku 12 Akim				N/A
257		ID_HEAD214				32			N/A			LargeDUBattery 12 Voltage				N/A		Genis Dagitim Aku 12 Gerilim				N/A
258		ID_HEAD215				32			N/A			LargeDUBattery 12 Capacity				N/A		Genis Dagitim Aku 12 Kapasite				N/A
259		ID_HEAD216				32			N/A			LargeDUBattery 13 Current				N/A		Genis Dagitim Aku 13 Akim				N/A
260		ID_HEAD217				32			N/A			LargeDUBattery 13 Voltage				N/A		Genis Dagitim Aku 13 Gerilim				N/A
261		ID_HEAD218				32			N/A			LargeDUBattery 13 Capacity				N/A		Genis Dagitim Aku 13 Kapasite				N/A
262		ID_HEAD219				32			N/A			LargeDUBattery 14 Current				N/A		Genis Dagitim Aku 14 Akim				N/A
263		ID_HEAD220				32			N/A			LargeDUBattery 14 Voltage				N/A		Genis Dagitim Aku 14 Gerilim				N/A
264		ID_HEAD221				32			N/A			LargeDUBattery 14 Capacity				N/A		Genis Dagitim Aku 14 Kapasite				N/A
265		ID_HEAD222				32			N/A			LargeDUBattery 15 Current				N/A		Genis Dagitim Aku 15 Akim				N/A
266		ID_HEAD223				32			N/A			LargeDUBattery 15 Voltage				N/A		Genis Dagitim Aku 15 Gerilim				N/A
267		ID_HEAD224				32			N/A			LargeDUBattery 15 Capacity				N/A		Genis Dagitim Aku 15 Kapasite				N/A
268		ID_HEAD225				32			N/A			LargeDUBattery 16 Current				N/A		Genis Dagitim Aku 16 Akim				N/A
269		ID_HEAD226				32			N/A			LargeDUBattery 16 Voltage				N/A		Genis Dagitim Aku 16 Gerilim				N/A
270		ID_HEAD227				32			N/A			LargeDUBattery 16 Capacity				N/A		Genis Dagitim Aku 16 Kapasite				N/A
271		ID_HEAD228				32			N/A			LargeDUBattery 17 Current				N/A		Genis Dagitim Aku 17 Akim				N/A
272		ID_HEAD229				32			N/A			LargeDUBattery 17 Voltage				N/A		Genis Dagitim Aku 17 Gerilim				N/A
273		ID_HEAD230				32			N/A			LargeDUBattery 17 Capacity				N/A		Genis Dagitim Aku 17 Kapasite				N/A
274		ID_HEAD231				32			N/A			LargeDUBattery 18 Current				N/A		Genis Dagitim Aku 18 Akim				N/A
275		ID_HEAD232				32			N/A			LargeDUBattery 18 Voltage				N/A		Genis Dagitim Aku 18 Gerilim				N/A
276		ID_HEAD233				32			N/A			LargeDUBattery 18 Capacity				N/A		Genis Dagitim Aku 18 Kapasite				N/A
277		ID_HEAD234				32			N/A			LargeDUBattery 19 Current				N/A		Genis Dagitim Aku 19 Akim				N/A
278		ID_HEAD235				32			N/A			LargeDUBattery 19 Voltage				N/A		Genis Dagitim Aku 19 Gerilim				N/A
279		ID_HEAD236				32			N/A			LargeDUBattery 19 Capacity				N/A		Genis Dagitim Aku 19 Kapasite				N/A
280		ID_HEAD237				32			N/A			LargeDUBattery 20 Current				N/A		Genis Dagitim Aku 20 Akim				N/A
281		ID_HEAD238				32			N/A			LargeDUBattery 20 Voltage				N/A		Genis Dagitim Aku 20 Gerilim				N/A
282		ID_HEAD239				32			N/A			LargeDUBattery 20 Capacity				N/A		Genis Dagitim Aku 20 Kapasite				N/A
283		ID_HEAD240				32			N/A			Temperature 8						N/A		Sicaklik 8						N/A
284		ID_HEAD241				32			N/A			Temperature 9						N/A		Sicaklik 9						N/A
285		ID_HEAD242				32			N/A			Temperature 10						N/A		Sicaklik 10						N/A
     
         
[p35_status_switch.htm:Number]
3

[p35_status_switch.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE				ABBR_IN_LOCALE
1		ID_AUTO_POP		16			N/A			Auto Popup				N/A		Oto AcilabilirPencere				N/A
2		ID_AUTO_POP_1		16			N/A			Auto Popup				N/A		Oto AcilabilirPencere				N/A
3		ID_DISABLE_AUTO_POP	32			N/A			Disable Auto Popup			N/A			Oto AcilabilirPencere Devre Disi		N/A

[p36_clear_data.htm:Number]
18

[p36_clear_data.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN	FULL_IN_LOCALE					ABBR_IN_LOCALE
1		ID_ERROR0		64			N/A			Failed to clear data.				N/A		Veri silme hatali				N/A
2		ID_ERROR1		64			N/A			was cleared.					N/A		Silindi					N/A
3		ID_ERROR2		64			N/A			Unknown error.					N/A		Bilinmeyen Hata!				N/A
4		ID_ERROR3		128			N/A			Failure, you are not authorized.		N/A		Hata. Yetkiniz yok				N/A
5		ID_ERROR4		64			N/A			Failed to communicate with the controller.	N/A		Denetci ile haberlesme hatasi			N/A
6		ID_ERROR5		64			N/A			Failure, Controller is hardware protected.	N/A		Hata. Denetci HW korumali			N/A
7		ID_HISTORY_ALARM	64			N/A			Alarm History					N/A		Alarm Gecmisi					N/A
8		ID_HISTORY_DATA		64			N/A			Data History					N/A		Veri Gecmisi					N/A
9		ID_HISTORY_STATDATA	64			N/A			Data Statistics					N/A		Veri Istatistigi				N/A
10		ID_HISTORY_CONTROL	64			N/A			Control Command Log				N/A		Kontrol Komut Kaydi				N/A
11		ID_HISTORY_BATTERY	64			N/A			Battery Test Log				N/A		Aku Test Kaydi					N/A
12		ID_HISTORY_PARAM	64			N/A			Runtime Persistent Data				N/A		Calisma Zamani Devam Verisi			N/A
13		ID_HISTORY_SCUP_RUNNING	32			N/A			System Runtime Log				N/A		Sistem Calisma Zamani Kaydi			N/A
14		ID_HEAD			32			N/A			Clear						N/A		Temizle						N/A
15		ID_TIPS			32			N/A			Clear						N/A		Temizle						N/A
16		ID_CLEAR		32			N/A			Clear						N/A		Temizle						N/A
17		ID_TIPS1		64			N/A			Are you sure you want to clear the data?	N/A		veriyi silmeye emin misiniz?			N/A
18		ID_HISTORY_DISEL_TEST	64			N/A			Diesel Test Log					N/A		Dizel Test Kaydi				N/A

[p37_edit_config_file.htm:Number]
0

[p37_edit_config_file.htm.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE				


[p38_title_config_file.htm:Number]
3

[p38_title_config_file.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_PLC			16			N/A			PLC Configuration			N/A		Konfig PLC		N/A
2		ID_ALARM_REG		32			N/A			ALARM_REG CFG				N/A		Konfig Reg Alarm	N/A
3		ID_AlARM		32			N/A			Alarm Suppression			N/A		Alarm Bastirma		N/A

[p39_edit_config_plc.htm:Number]
51

[p39_edit_config_plc.htm]
#Sequence ID	RES_ID				MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN	FULL_IN_LOCALE					ABBR_IN_LOCALE
1		ID_Operator			16			N/A			Operator					N/A		Operator					N/A
2		ID_Input1			16			N/A			Input 1						N/A		Giris 1						N/A
3		ID_Input2			16			N/A			Input 2						N/A		Giris2						N/A
4		ID_Param1			16			N/A			Parameter 1					N/A		Parametre 1					N/A
5		ID_Param2			16			N/A			Parameter 2					N/A		Parametre 2					N/A
6		ID_Output			16			N/A			Output						N/A		Cikis						N/A
7		ID_EquipName/Register1		32			N/A			Equipment Name/Register				N/A		Ekipman Adi/Kaydet				N/A
8		ID_SignalType1			16			N/A			Signal Type					N/A		Sinyal Tipi					N/A
9		ID_SignalName1			16			N/A			Signal Name					N/A		Sinyal Adi					N/A
10		ID_EquipName/Register2		32			N/A			Equipment Name/Register				N/A		Ekipman Adi/Kaydet				N/A
11		ID_SignalType2			16			N/A			Signal Type					N/A		Sinyal Tipi					N/A
12		ID_SignalName2			16			N/A			Signal Name					N/A		Sinyal Adi					N/A
13		ID_EquipName/Register3		32			N/A			Equipment Name/Register				N/A		Ekipman Adi/Kaydet				N/A
14		ID_SignalType3			16			N/A			Signal Type					N/A		Sinyal Tipi					N/A
15		ID_SignalName3			16			N/A			Signal Name					N/A		Sinyal Adi					N/A
16		ID_ADD				16			N/A			Add						N/A		Ekle						N/A
17		ID_Delete			16			N/A			Delete						N/A		Sil						N/A
18		ID_Sampling			16			N/A			Sampling					N/A		Ornekle						N/A
19		ID_Control			16			N/A			Control						N/A		Kontrol						N/A
20		ID_Setting			16			N/A			Settings					N/A		Ayarlar						N/A
21		ID_Alarm			16			N/A			Alarm						N/A		Alarm						N/A
22		ID_ERROR5			64			N/A			PLC configuration modified error		N/A		PLC Konfig degistirme hatasi			N/A
23		ID_ERROR6			128			N/A			PLC configuration modified successfully,\nTo go into effect, restart the Controller!	N/A	PLC Konfig ifadesi degistirme basarili\ndegisiklikligin uygulanmasi icin sistemi yeniden baslatin		N/A
24		ID_CONFIRM_1			64			N/A			Are you sure to delete?				N/A		Silmek icin emin misiniz?			N/A
25		ID_ERROR0			64			N/A			PLC configuration file error.			N/A		PLC Konfig dosya hatasi!			N/A
26		ID_ERROR1			128			N/A			Unknow error.					N/A		Bilinmeyen hata!				N/A
27		ID_INFO1			64			N/A			SYMBOL INFORMATION				N/A		Sembol BILGISI					N/A
28		ID_INFO2			64			N/A			1:R, which defines a Register.			N/A		1:R, Kaydet.					N/A
29		ID_INFO3			128			N/A			Usage: R(Register_ID); 0 = &lt; Register_ID &lt;= 99	N/A	Kullanim: R(Register_ID); 0 = &lt; Register_ID &lt;= 99	N/A
30		ID_INFO4			64			N/A			2:P, which defines a Parameter.			N/A		2:P, Parametre					N/A
31		ID_INFO5			64			N/A			Usage: P(The Value)				N/A		Kullanim: P(Deger)				N/A
32		ID_INFO6			128			N/A			3:SET, which represents SET command.		N/A		3:SET, Ayarla					N/A
33		ID_INFO7			128			N/A			Usage: SET _ _ Parameter1 _ Output		N/A		Kullanim: SET _ _ Parametre1 _ Cikis		N/A
34		ID_INFO8			128			N/A			4:AND, which represents AND command.		N/A		4:AND, VE					N/A
35		ID_INFO9			128			N/A			Usage: AND Input1 Input2 _ _ Output		N/A		Kullanim: AND Giris1 Giris2 _ _ Cikis		N/A
36		ID_INFO10			128			N/A			5:OR, which represents OR command.		N/A		5:OR, VEYA					N/A
37		ID_INFO11			128			N/A			Usage: OR Input1 Input2 _ _ Output		N/A		Kullanim: OR Giris1 Giris2 _ _ Cikis		N/A
38		ID_INFO12			128			N/A			6:NOT, which represents NOT command.		N/A		6:NOT, NOT					N/A
39		ID_INFO13			128			N/A			Usage: NOT Input1 _ _ _ Output			N/A		Kullanim: NOT Giris1 _ _ _ Cikis		N/A
40		ID_INFO14			128			N/A			7:XOR, which represents XOR command.		N/A		7:XOR, XOR 					N/A
41		ID_INFO15			128			N/A			Usage: XOR Input1 Input2 _ _ Output		N/A		Kullanim: XOR Giris1 Giris2 _ _ Cikis		N/A
42		ID_INFO16			128			N/A			8:GT, which represents Greater Than command.	N/A		8:GT, Buyuktur					N/A
43		ID_INFO17			128			N/A			Usage: GT Input1 _ Parameter1 Parameter2 Output	N/A		Kullanim, GT Giris1 _ Parametre1 Parametre2 Cikis	N/A
44		ID_INFO18			128			N/A			9:LT, which represents Less Than command.	N/A		9:LT, Kucuktur					N/A
45		ID_INFO19			128			N/A			Usage: LT Input1 _ Parameter1 Parameter2 Output	N/A		Kullanim: LT Giris1 _ Parametre1 Parametre2 Cikis	N/A
46		ID_INFO20			128			N/A			10:DS, which represents Delay command.		N/A		10:DS, Gecikme					N/A
47		ID_INFO21			128			N/A			Usage: DS Input1 _ Parameter1 _ Output		N/A		Kullanim: DS Giris1 _ Parametre1 _ Cikis	N/A
48		ID_INFO22			32			N/A			LIMITATION					N/A		SINIRLAMA					N/A
49		ID_INFO23			256			N/A			All Output signals must be of enum type, and not an alarm signal.	N/A	Butun cikis sinyalleri enum tipi olmalidir, ve alarm sinyali olmamalidir.	N/A
50		ID_INFO24			128			N/A			LT and GT's Input1 values must be of type Float, Unsigned or Long.	N/A	LT ve GT giris1 degerleri float,uzun ve imzasiz  tipi olmalidir.		N/A
51		ID_Delete2			16			N/A			Delete						N/A		Borrar						N/A




[p40_cfg_plc_Popup.htm:Number]
67

[p40_cfg_plc_Popup.htm]
#Sequence ID	RES_ID				MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_TITLE			32			N/A			PLC CONFIG			N/A		KONFIG PLC		N/A
2		ID_Operator			16			N/A			Operator			N/A		Operator		N/A
3		ID_Input1			16			N/A			Input 1				N/A		Giris 1			N/A
4		ID_Input2			16			N/A			Input 2				N/A		Giris 2			N/A
5		ID_Param1			16			N/A			Parameter 1			N/A		Parametre 1		N/A
6		ID_Param2			16			N/A			Parameter 2			N/A		Parametre 1		N/A
7		ID_Output			16			N/A			Output				N/A		Cikis			N/A
8		ID_Signal1			16			N/A			Signal				N/A		Sinyal			N/A
9		ID_Register1			16			N/A			Register			N/A		Kayit Ol			N/A
10		ID_Signal2			16			N/A			Signal				N/A		Sinyal			N/A
11		ID_Register2			16			N/A			Register			N/A		Kayit Ol			N/A
12		ID_Signal3			16			N/A			Signal				N/A		Sinyal			N/A
13		ID_Register3			16			N/A			Register			N/A		Kayit Ol			N/A
14		ID_EquipName/Register1		32			N/A			Equipment Name/Register		N/A		Ekipman Adi/Kaydet	N/A
15		ID_SignalType1			16			N/A			Signal Type			N/A		Sinyal Tipi		N/A
16		ID_SignalName1			16			N/A			Signal Name			N/A		Sinyal Adi		N/A
17		ID_EquipName/Register2		32			N/A			Equipment Name/Register		N/A		Ekipman Adi/Kaydeto	N/A
18		ID_SignalType2			16			N/A			Signal Type			N/A		Sinyal Tipi		N/A
19		ID_SignalName2			16			N/A			Signal Name			N/A		Sinyal Adi		N/A
20		ID_EquipName/Register3		32			N/A			Equipment Name/Register		N/A		Ekipman Adi/Kaydet	N/A
21		ID_SignalType3			16			N/A			Signal Type			N/A		Sinyal Tipi		N/A
22		ID_SignalName3			16			N/A			Signal Name			N/A		Sinyal Adi		N/A
23		ID_Sampling1			16			N/A			Sampling			N/A		Ornekle			N/A
24		ID_Control1			16			N/A			Control				N/A		Kontrol			N/A
25		ID_Setting1			16			N/A			Setting				N/A		Ayarlar			N/A
26		ID_Alarm1			16			N/A			Alarm				N/A		Alarm			N/A
27		ID_Sampling2			16			N/A			Sampling			N/A		Ornekle			N/A
28		ID_Control2			16			N/A			Control				N/A		Kontrol			N/A
29		ID_Setting2			16			N/A			Setting				N/A		Ayarlar			N/A
30		ID_Alarm2			16			N/A			Alarm				N/A		Alarm			N/A
31		ID_Sampling3			16			N/A			Sampling			N/A		Ornekle			N/A
32		ID_Control3			16			N/A			Control				N/A		Kontrol			N/A
33		ID_Setting3			16			N/A			Setting				N/A		Ayarlar			N/A
34		ID_ADD				16			N/A			Add				N/A		Ekle			N/A
35		ID_CANCEL			16			N/A			Cancel				N/A		Iptal			N/A
36		ID_OUTPUT_ERROR1		32			N/A			Output error			N/A		Cikis Hata		N/A
37		ID_PARAM1_ERROR			32			N/A			Parameter 1 error		N/A		Parametre 1 Hata	N/A
38		ID_OUTPUT_ERROR2		32			N/A			Output error			N/A		Cikis Hata		N/A
39		ID_ALL_ERROR1			32			N/A			Input/Output error		N/A		Giris/Cikis Hata	N/A
40		ID_INPUT1_ERROR1		32			N/A			Input 1 error			N/A		Giris 1 Hata		N/A
41		ID_INPUT2_ERROR1		32			N/A			Input 2 error			N/A		Giris 2 Hata		N/A
42		ID_OUTPUT_ERROR3		32			N/A			Output error			N/A		Cikis Hata		N/A
43		ID_ALL_ERROR2			32			N/A			Input/Output error		N/A		Giris/Cikis Hata	N/A
44		ID_INPUT1_ERROR2		32			N/A			Input 1 error			N/A		Giris 1 Hata		N/A
45		ID_INPUT2_ERROR2		32			N/A			Input 2 error			N/A		Giris 2 Hata		N/A
46		ID_OUTPUT_ERROR4		32			N/A			Output error			N/A		Cikis Hata		N/A
47		ID_ALL_ERROR3			32			N/A			Input/Output error		N/A		Giris/Cikis Hata	N/A
48		ID_INPUT1_ERROR3		32			N/A			Input 1 error			N/A		Giris 1 Hata		N/A
49		ID_OUTPUT_ERROR5		32			N/A			Output error			N/A		Cikis Hata		N/A
50		ID_ALL_ERROR4			32			N/A			Input/Output error		N/A		Giris/Cikis Hata	N/A
51		ID_INPUT1_ERROR4		32			N/A			Input 1 error			N/A		Giris 1 Hata		N/A
52		ID_INPUT2_ERROR3		32			N/A			Input 2 error			N/A		Giris 2 Hata		N/A
53		ID_OUTPUT_ERROR6		32			N/A			Output error			N/A		Cikis Hata		N/A
54		ID_ALL_ERROR5			32			N/A			Input/Output error		N/A		Giris/Cikis Hata	N/A
55		ID_INPUT1_ERROR5		32			N/A			Input 1 error			N/A		Giris 1 Hata		N/A
56		ID_OUTPUT_ERROR7		32			N/A			Output error			N/A		Cikis Hata		N/A
57		ID_PARAM1_ERROR2		32			N/A			Parameter 1 error		N/A		Parametre 1 Hata	N/A
58		ID_PARAM2_ERROR1		32			N/A			Parameter 2 error		N/A		Parametre 2 Hata	N/A
59		ID_ALL_ERROR6			32			N/A			Input/Output error		N/A		Giris/Cikis Hata	N/A
60		ID_INPUT1_ERROR6		32			N/A			Input 1 error			N/A		Giris 1 Hata		N/A
61		ID_OUTPUT_ERROR8		32			N/A			Output error			N/A		Cikis Hata		N/A
62		ID_PARAM1_ERROR3		32			N/A			Parameter 1 error		N/A		Parametre 1 Hata	N/A
63		ID_PARAM2_ERROR2		32			N/A			Parameter 2 error		N/A		Parametre 2 Hata	N/A
64		ID_ALL_ERROR7			32			N/A			Input/Output error		N/A		Giris/Cikis Hata	N/A
65		ID_INPUT1_ERROR7		32			N/A			Input 1 error			N/A		Giris 1 Hata		N/A
66		ID_OUTPUT_ERROR9		32			N/A			Output error			N/A		Cikis Hata		N/A
67		ID_PARAM1_ERROR4		32			N/A			Parameter 1 error		N/A		Parametre 1 Hata	N/A


[p41_edit_config_alarmReg.htm:Number]
14


[p41_edit_config_alarmReg.htm]
1		ID_TITLE1			32			N/A			Please Select Equipment:		N/A		Ekipman seciniz				N/A
2		ID_Choice			32			N/A			Please Select				N/A		Lutfen Seciniz				N/A
3		ID_Choice2			32			N/A			Please Select				N/A		Lutfen Seciniz				N/A
4		ID_TITLE2			32			N/A			Alarm Relay Configuration		N/A		Alarm Role Konfigurasyonu		N/A
5		ID_STDEQUIPNAME			32			N/A			Standard Equipment Name			N/A		Standart Ekipman adi			N/A
6		ID_AlarmID			32			N/A			Alarm Signal ID				N/A		Alarm Sinyal ID				N/A
7		ID_AlarmName			32			N/A			Alarm Signal Name			N/A		Alarm Sinyal Adi			N/A
8		ID_AlarmReg			32			N/A			Alarm Relay Number			N/A		Alarm Role Numarasi				N/A
9		ID_Reg				32			N/A			New Relay Number			N/A		Yeni Role Numarasi			N/A
10		ID_Edit				16			N/A			Modify					N/A		Degistir				N/A
11		ID_ERROR5			64			N/A			Alarm relay modified error		N/A		Alarm role degisikligi hatali		N/A
12		ID_ERROR6			64			N/A			Alarm relay modified successfully.	N/A		Alarm role degisikligi basarili.			N/A
13		ID_ERROR4			64			N/A			Failure, you are not authorized.	N/A		Hata. Yetkiniz yok.			N/A
14		ID_Edit				16			N/A			Modify					N/A		Degistir				N/A





[p42_edit_config_alarm.htm:Number]
19

[p42_edit_config_alarm.htm]
1		ID_Edit				16			N/A			Modify							N/A		Degistir			N/A
2		ID_TITLE1			32			N/A			Please Select Equipment:				N/A		Ekipman seciniz			N/A
3		ID_TITLE2			32			N/A			Alarm Suppression					N/A		Alarm Bastirma			N/A
4		ID_AlarmID			32			N/A			Alarm Signal ID						N/A		Alarm Sinyal ID			N/A
5		ID_AlarmName			32			N/A			Alarm Signal Name					N/A		Alarm Sinyal Adi		N/A
6		ID_AlarmSuppress		32			N/A			Alarm Suppressing Expression				N/A		Alarm Bastirma Ifadesi		N/A
7		ID_TITLE3			64			N/A			Number of signals in alarm suppression expression:	N/A		Alarm bastirma ifadesi sinyal sayisi:	N/A
8		ID_Choice			32			N/A			Please Select						N/A		Lutfen Seciniz			N/A
9		ID_TITLE4			32			N/A			Alarm Suppression Expression:				N/A		Alarm Bastirma Ifadesi: 	N/A
10		ID_Submit			16			N/A			Submit							N/A		Onayla				N/A
11		ID_Cancel			16			N/A			Cancel							N/A		Iptal				N/A
12		ID_STDEQUIPNAME			32			N/A			Standard Equipment Name					N/A		Standart Ekipman Adi		N/A
13		ID_Choice2			32			N/A			Please Select						N/A		Lutfen Seciniz			N/A
14		ID_ERROR5			64			N/A			Alarm suppression expression modified error		N/A		Alarm bastirma ifadesi degistirme hatasi		N/A
15		ID_ERROR6			128			N/A			Alarm suppression expression modified successfully,\nto go into effect,restart the system.	N/A	Alarm bastirma ifadesi degistirme basarili\ndegisiklikligin uygulanmasi icin sistemi yeniden baslatin		N/A
16		ID_OPERATOR_ERROR1		32			N/A			Operator error						N/A		Operator Hata			N/A
17		ID_OPERATOR_ERROR2		32			N/A			Operator error						N/A		Operator Hata			N/A
18		ID_SELF_ERROR			64			N/A			Do not suppress the signal with same signal		N/A		Sinyali ayni sinyalle bastirmayin	N/A
19		ID_ERROR4			64			N/A			Failure, you are not authorized.			N/A		Hata. Yetkiniz yok!		N/A




[p43_ydn_config.htm:Number]
41


[p43_ydn_config.htm]
1		ID_ERROR0		64			N/A			Success.						N/A			Basarili						N/A
2		ID_ERROR1		64			N/A			Failure. 						N/A			Hata						N/A
3		ID_ERROR2		64			N/A			Failure, YDN23 Service has exited. 			N/A			Hata. YDN23 Servisinden cikildi.			N/A
4		ID_ERROR3		64			N/A			Failure, invalid parameter.				N/A			Hata. Gecersiz parametre  			N/A
5		ID_ERROR4		64			N/A			Failure, invalid  data.					N/A			Hata. Gecersiz veri			N/A
6		ID_ERROR5		64			N/A			Controller is hardware protected, can't be modified.	N/A			Denetci donanim korumali degistirelemez.	N/A
7		ID_ERROR6		64			N/A			service is busy, cannot change configuration now.	N/A			Servis mesgul simdi konfigurasyonu degistiremezsiniz.	N/A
8		ID_ERROR7		64			N/A			non-shared port has already been occupied.		N/A			Paylasilmayan port zaten mesgul		N/A
9		ID_ERROR8		64			N/A			Failure, you are not authorized.			N/A			Hata. Yetkiniz yok.			N/A
10		ID_YDN_HEAD		64			N/A			Background Protocol Configuration			N/A			Arkaplan Protokolu Yapilandirmasi		N/A
11		ID_PROTOCOL_TYPE	32			N/A			Protocol Type						N/A			Protokol Tipi				N/A
12		ID_PROTOCOL_MEDIA	32			N/A			Port Type						N/A			Port Tipi					N/A
13		ID_REPORT_IN_USER	32			N/A			Alarm Report						N/A			Alarm Rapor			N/A
14		ID_MAX_ALARM_REPORT	32			N/A			Max Alarm Report Attempts				N/A			Max. Alarm Rapor Denemesi		N/A
15		ID_RANGE_FROM		32			N/A			Range							N/A			Aralik						N/A
16		ID_CALL_ELAPSE_TIME	32			N/A			Call Elapse Time					N/A			Gecen Arama Suresi				N/A
17		ID_RANGE_FROM		32			N/A			Range							N/A			Aralik						N/A
18		ID_MAIN_REPORT_PHONE	32			N/A			First Report Phone Number				N/A			Ilk Rapor Telefon Numarasi					N/A
19		ID_SECOND_REPORT_PHONE	32			N/A			Second Report Phone Number				N/A			Ikinci Rapor Telefon Numarasi						N/A
20		ID_CALLBACK_PHONE	32			N/A			Third Report Phone Number				N/A			Ucuncu Rapor Telefon Numarasi						N/A
21		ID_COMMON_PARAM		64			N/A			Port Parameter						N/A			Port Parametresi				N/A
22		ID_MODIFY		32			N/A			Modify							N/A			Degistir				N/A
23		ID_PROTOCOL0		16			N/A			YDN23							N/A			YDN23						N/A
24		ID_PROTOCOL1		16			N/A			RSOC							N/A			RSOC						N/A
25		ID_PROTOCOL2		16			N/A			SOC/TPE							N/A			SOC/TPE						N/A
26		ID_MEDIA0		16			N/A			RS-232							N/A			RS-232						N/A
27		ID_MEDIA1		16			N/A			Modem							N/A			Modem						N/A
28		ID_MEDIA2		16			N/A			Ethernet						N/A			Ethernet					N/A
29		ID_TIPS8		128			N/A			Max alarm report attempts is incorrect.			N/A			Max alarm raporu denemeleri yanlis.	N/A
30		ID_TIPS9		128			N/A			Max call elapse time is incorrect.			N/A			Tiempo Max gecen arama suresi yanlis.	N/A
31		ID_TIPS10		128			N/A			Main report phone number is incorrect.			N/A			Ana rapor telefon numarasi yanlis.	N/A
32		ID_TIPS11		128			N/A			Second report phone number is incorrect.		N/A			Ikinci rapor telefon numarasi yanlis.	N/A
33		ID_TIPS12		128			N/A			Callback report phone number is incorrect.		N/A			Geri arama rapor telefon numarasi yanlis.		N/A
34		ID_TIPS21		64			N/A			Max alarm report attempts is incorrect			N/A			Max alarm raporu denemeleri yanlis.		N/A
35		ID_TIPS22		64			N/A			Max alarm report attempts is incorrect			N/A			Max alarm raporu denemeleri yanlis.		N/A
36		ID_TIPS23		64			N/A			input error						N/A			Giris Hatasi				N/A
37		ID_TIPS24		64			N/A			The port is incorrect					N/A			Port yanlis				N/A
38		ID_CCID			16			N/A			Own Address						N/A			Kendi Adresi				N/A
39		ID_RANGE_FROM		32			N/A			Range							N/A			Aralik						N/A
40		ID_TIPS6		128			N/A			Address is incorrect, enter a number please.		N/A			Adres yanlis. Lutfen numara girin.	N/A
41		ID_NO_PROTOCOL_TIPS	128			N/A			Please enter protocol.					N/A			Lutfen protokol girin.			N/A







[p47_web_title.htm:Number]
0

[p47_web_title.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[p78_get_setting_param.htm:Number]
8

[p78_get_setting_param.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN										ABBR_IN_EN	FULL_IN_LOCALE							ABBR_IN_LOCALE
1		ID_HEAD			32			N/A			Get Parameter Settings									N/A		Parametre Ayarlari Al						N/A
2		ID_CLOSE_ACU		32			N/A			Get Parameter Settings									N/A		Parametre Ayarlari Al						N/A
3		ID_ERROR0		32			N/A			Unknown error.										N/A		Bilinmeyen Hata							N/A
4		ID_ERROR1		128			N/A			Retrieve of paramater settings was successfull.						N/A		Parametre ayarlarini erisim basarili				N/A
5		ID_ERROR2		64			N/A			Failed to retrieve paramater settings.							N/A		Parametre ayarlarina erisim hatali				N/A
6		ID_ERROR3		64			N/A			You are not authorized to close the controller!.					N/A		Denetciyi kapatma yetkiniz yok!					N/A
7		ID_ERROR4		64			N/A			Failed to communicate with the controller.						N/A		Denetci ile haberlesme hatasi						N/A
8		ID_TIPS			128			N/A			Through this function, user can retrive current parameter settings from the controller.	N/A		Bu fonksiyondan dolayi kullanici mevcut parametre ayarlarina erisebilir.		N/A

[p79_site_map.htm:Number]
28

[p79_site_map.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE				ABBR_IN_LOCALE
1		ID_SITE_MAP		16			N/A			Site Map				N/A		Saha Haritasi				N/A
2		ID_SITE_MAP		16			N/A			Site Map				N/A		Saha Haritasi				N/A
3		ID_MODIFY_SITE_INFO	32			N/A			Modify Site Information			N/A		Saha Bilgisini Degistir			N/A
4		ID_MODIFY_DEVICE_INFO	32			N/A			Modify Device Information		N/A		Cihaz Bilgisini Degistir		N/A
5		ID_MODIFY_ALARM_INFO	32			N/A			Modify Alarm Information		N/A		Alarm Bilgisini Degistir		N/A
6		ID_DEVICE_EXPLORE	16			N/A			DEVICES					N/A		CIHAZLAR				N/A
7		ID_SYSTEM		16			N/A			SETTINGS				N/A		AYARLAR					N/A
8		ID_NETWORK_SETTING	32			N/A			Network Configuration			N/A		Ag Konfigurasyonu			N/A
9		ID_NMS_SETTING		16			N/A			NMS Configuration			N/A		NMS Konfigurasyonu			N/A
10		ID_ESR_SETTING		16			N/A			MC Configuration			N/A		MC Konfigurasyonu			N/A
11		ID_USER			64			N/A			User Information Configuration		N/A		Kullanici Bilgisi Konfigurasyonu	N/A
12		ID_MAINTENANCE		16			N/A			MAINTENANCE				N/A		BAKIM					N/A
13		ID_FILE_MANAGE		32			N/A			Download				N/A		Yukle					N/A
14		ID_MODIFY_CFG		64			N/A			Modify Configuration Online		N/A		Online Konfig Degistir			N/A
15		ID_TIME_CFG		64			N/A			Time Synchronization			N/A		Zaman Senkronizasyonu			N/A
16		ID_QUERY		16			N/A			QUERY					N/A		SORGU					N/A
17		ID_ALARM		32			N/A			ALARMS					N/A		Alarmlar				N/A
18		ID_ACTIVE_ALARM		32			N/A			Active Alarms				N/A		Aktif Alarmlar				N/A
19		ID_HISTORY_ALARM	32			N/A			Alarm History				N/A		Alarm Gecmisi				N/A
20		ID_QUERY_HIS_DATA	32			N/A			Data History				N/A		Veri Gecmisi				N/A
21		ID_QUERY_LOG_DATA	32			N/A			Data Log				N/A		Veri Kaydi				N/A
22		ID_QUERY_BATT_DATA	32			N/A			Battery Test Data			N/A		Aku Test Verisi				N/A
23		ID_CLEAR_DATA		32			N/A			Clear Data				N/A		Veri Sil				N/A
24		ID_EDIT_CONFIGFILE	32			N/A			Edit Configuration File			N/A		Konfig dosyasiniduzenle		N/A
25		ID_CONFIG_ALARMSUPEXP	32			N/A			Alarm Suppression Configuration		N/A		Alarm Bastirma Konfigurasyonu		N/A
26		ID_CONFIG_ALARMRELAY	32			N/A			Alarm Relay Configuration		N/A		Alarm Role Konfigurasyonu		N/A
27		ID_CONFIG_PLC		32			N/A			PLC Configuration			N/A		PLC Konfigurasyonu			N/A
28		ID_YDN_SETTING		32			N/A			HLMS Configuration			N/A		HLMS Konfigurasyonu			N/A





[p80_status_view.htm:Number]
1

[p80_status_view.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_LEAVE		8			N/A			Logout				N/A		Cikis			N/A

[p81_user_def_page.htm:Number]
28


[p81_user_def_page.htm]
#Sequence ID	RES_ID		    MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN	FULL_IN_LOCALE					ABBR_IN_LOCALE
1		ID_INDEX	    16				N/A			Equipment Name					N/A		Ekipman Adi					N/A
2		ID_SIGNAL_NAME	    16				N/A			Signal Name					N/A		Sinyal Adi					N/A
3		ID_SIGNAL_VALUE	    16				N/A			Value						N/A		Deger						N/A
4		ID_SIGNAL_UNIT	    16				N/A			Unit						N/A		Birim						N/A
5		ID_SAMPLE_TIME	    16				N/A			Time						N/A		Zaman						N/A
6		ID_SET_VALUE	    16				N/A			Set Value					N/A		Deger Ayarla					N/A
7		ID_SET		    16				N/A			Set						N/A		Ayarla						N/A
8		ID_ERROR0	    32				N/A			Failure.					N/A		Hata						N/A
9		ID_ERROR1	    32				N/A			Success.					N/A		Basarili					N/A
10		ID_ERROR2	    64				N/A			Failure, conflict in settings.			N/A		Hata. Ayarda celiski var.			N/A
11		ID_ERROR3	    32				N/A			Failure, you are not authorized.		N/A		Hata. Yetkiniz yok.				N/A
12		ID_ERROR4	    64				N/A			No information to send.				N/A		Gonderilecek bilgi yok.				N/A
13		ID_ERROR5	    128				N/A			Failure, Controller is hardware protected	N/A		Hata. Denetci HW korumali			N/A
14		ID_SET_TYPE	    16				N/A			Set						N/A		Ayarla						N/A
15		ID_SHOW_TIPS0	    64				N/A			Greater than the maximum value:			N/A		Max. degerden yuksek:				N/A
16		ID_SHOW_TIPS1	    64				N/A			Less than the minimum value:			N/A		Min. degerden az:				N/A
17		ID_SHOW_TIPS2	    64				N/A			Can't be empty.					N/A		Bos birakilamaz.					N/A
18		ID_SHOW_TIPS3	    64				N/A			Enter a number please.				N/A		Lutfen numara giriniz. 				N/A
19		ID_SHOW_TIPS4	    64				N/A			The control value is equal to the last value.	N/A		Kontrol degeri eski degerle ayni.		N/A
20		ID_SHOW_TIPS5	    64				N/A			Failure, you are not authorized.		N/A		Hata. Yetkiniz yok.				N/A
21		ID_TIPS1	    64				N/A			Send modifications				N/A		Degisiklikleri Gonder				N/A
22		ID_SAMPLER	    16				N/A			Status						N/A		Durum						N/A
23		ID_CHANNEL	    16				N/A			Channel						N/A		Kanal						N/A
24		ID_MONTH_ERROR	    32				N/A			Incorrect month.				N/A		Yanlis Ay!					N/A
25		ID_DAY_ERROR	    32				N/A			Incorrect day.					N/A		Yanlis Gun!					N/A
26		ID_HOUR_ERROR	    32				N/A			Incorrect hour.					N/A		Yanlis Saat!					N/A
27		ID_FORMAT_ERROR	    64				N/A			Incorrect format.				N/A		Yanlis Format!					N/A
28		ID_MENU_PAGE	    32				N/A			/cgi-bin/eng/					N/A		/cgi-bin/loc/					N/A



[alai_tree.js:Number]
0

[alai_tree.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE


[j09_alai_tree_help.js:Number]
0

[j09_alai_tree_help.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE


[p44_cfg_powersplite.htm:Number]
36

[p44_cfg_powersplite.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN	FULL_IN_LOCALE			ABBR_IN_LOCALE
1		ID_Master		32		N/A			Master				N/A		Master				N/A
2		ID_Slave		32		N/A			Slave				N/A		Slave				N/A
3		ID_CFGMode		32		N/A			Modify				N/A		Degistir			N/A
4		ID_TITLE2		32		N/A			PowerSplit Configuration	N/A		GucPaylasimi Konfigurasyonu	N/A
5		ID_PSSigName		32		N/A			Signal Name			N/A		Sinyal Adi			N/A
6		ID_EquipName		32		N/A			Equipment Name			N/A		Ekipman Adi			N/A
7		ID_SigType		32		N/A			Signal Type			N/A		Sinyal Tipi			N/A
8		ID_SigName		32		N/A			Signal Name			N/A		Sinyal adi			N/A
9		ID_TITLE3		32		N/A			PowerSplit Configuration	N/A		Guc paylasimi konfigurasyonu	N/A
10		ID_PSSigName1		32		N/A			Signal Name			N/A		Sinyal Adi			N/A
11		ID_EquipName1		32		N/A			Equipment Name			N/A		Ekipman Adi			N/A
12		ID_SigType1		32		N/A			Signal Type			N/A		Sinyal Tipi			N/A
13		ID_SigName1		32		N/A			Signal Name			N/A		Sinyal Adi		N/A
14		ID_CFGSubmit		32		N/A			Submit				N/A		Iptal				N/A
15		ID_CFGCancel		32		N/A			Cancel				N/A		Iptal				N/A
16		ID_PS_MODE		32		N/A			PowerSplit Mode			N/A		GucPaylasimi Modu		N/A
17		ID_EQUIP_ERROR		32		N/A			Equipment Name Error		N/A		Ekipman Adi Hatasi		N/A
18		ID_TYPE_ERROR		32		N/A			Signal Type Error		N/A		Sinyal Tipi Hatasi		N/A
19		ID_SIG_ERROR		32		N/A			Signal Name Error		N/A		Sinyal Adi Hatasi		N/A
20		ID_Sampling		32		N/A			Sampling			N/A		Ornekleme			N/A
21		ID_Control		32		N/A			Control				N/A		Kontrol				N/A
22		ID_Setting		32		N/A			Setting				N/A		Ayar				N/A
23		ID_Sampling1		32		N/A			Sampling			N/A		Ornekleme			N/A
24		ID_Control1		32		N/A			Control				N/A		Kontrol				N/A
25		ID_Setting1		32		N/A			Setting				N/A		Ayar				N/A
26		ID_Alarm		32		N/A			Alarm				N/A		Alarm				N/A
27		ID_Alarm1		32		N/A			Alarm				N/A		Alarm				N/A
28		ID_EDIT			32		N/A			Edit				N/A		Duzenle				N/A
29		ID_ERROR5		64		N/A			PowerSplit modified error	N/A		GucPaylasimi modifikasyon Hata	N/A
30		ID_ERROR6		128		N/A			PowerSplit modified successfully.\nTo go into effect, restart the system.	N/A	GucPaylasimi modifikasyon basarili.\Sistemi yeniden baslatin.	N/A
31		ID_ERROR0		64		N/A			Configuration file error.	N/A		Konfigurasyon dosyasi hata!	N/A
32		ID_ERROR1		128		N/A			Unknown error.			N/A		bilinmeyen Hata!		N/A
33		ID_EXPLAIN		128		N/A			Other PowerSplit setting signals can be found here.	N/A	Diger GucPaylasimi sinyali bulunabilir.	N/A
34		ID_DIR			32		N/A			./eng				N/A		./loc				N/A
35		ID_CONFIRM_1		64		N/A			Are you sure?			N/A		Emin misiniz?			N/A
36		ID_SYSTEM		32		N/A			Power System			N/A		Guc Sistemi			N/A
37		ID_NO_SIGNAL_ALERT	64		N/A			No such signal.			N/A		Boyle bir sinyal yok.		N/A

[p77_auto_config.htm:Number]
9

[p77_auto_config.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN							ABBR_IN_EN	FULL_IN_LOCALE			ABBR_IN_LOCALE
1		ID_HEAD			32			N/A			Auto Configuration						N/A		Oto Konfigurasyon		N/A
2		ID_CLOSE_ACU		32			N/A			Auto Configuration						N/A		Oto Konfigurasyon		N/A
3		ID_ERROR0		32			N/A			Unknown error.							N/A		Bilinmeyen Hata!		N/A
4		ID_ERROR1		128			N/A			Auto Config has started. Please wait a while.			N/A		Oto Konfig basladi. Lutfen Bekleyiniz!		N/A
5		ID_ERROR2		64			N/A			Failed to retreive.						N/A		Erisim Hatali		N/A
6		ID_ERROR3		64			N/A			You do not have authority to stop the controller.		N/A		Denetciyi durdurma yetkiniz yok!		N/A
7		ID_ERROR4		64			N/A			Failed to communicate with the controller.			N/A		Denetci ile haberlesme hatasi		N/A
8		ID_AUTO_CONFIG		256			N/A			The controller will run auto configuration, and web browser will be closed. Please wait a moment(About 2-5 minute).	N/A	Denetci oto konfig baslatacak ve web sayfasi kapanacak.Lutfen bir sure bekleyiniz (2-5 dk)!		N/A
9		ID_TIPS			128			N/A			Through this function, you can run the auto configuration.	N/A		Bu fonksiyondan dolayi oto konfigurasyonu baslatabilirsiniz.		N/A

[copyright.htm:Number]
0

[copyright.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE


[global.css:Number]
0

[global.css]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE


[header.htm:Number]
3

[header.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_SITE		16			N/A			Site		N/A		Yer			N/A
2		ID_LOGIN	16			N/A			./eng/		N/A		./loc/			N/A
3		ID_LOGOUT	16			N/A			LOGOUT		N/A		CIKIS			N/A


[p01_home.htm:Number]
0

[p01_home.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE


[p01_home_index.htm:Number]
12

[p01_home_index.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_EVENT_STATUS		32			N/A			Event Status		N/A		Olay Durumu		N/A
2		ID_OUT_VOLTAGE		32			N/A			Output Voltage		N/A		Cikis Gerilimi		N/A
3		ID_OUT_CURRENT		32			N/A			Output Current		N/A		Cikis Akimi		N/A
4		ID_BATT_STATUS		32			N/A			Battery Status		N/A		Aku Durumu		N/A
5		ID_AMB_TEMP		14			N/A			Ambient Temp		N/A		OrtamSicakligi		N/A
6		ID_LOAD_TREND		32			N/A			Load Trend		N/A		Yuk Trendi		N/A
7		ID_TIME			32			N/A			Time			N/A		Zaman			N/A
8		ID_PEAK_CURRENT		32			N/A			Peak Current		N/A		Peak Akimi		N/A
9		ID_AVERAGE_CURRENT	32			N/A			Average Current		N/A		Ortalama Akim		N/A
10		ID_R			32			N/A			A			N/A		R			N/A
11		ID_S			32			N/A			B			N/A		S			N/A
12		ID_T			32			N/A			C			N/A		T			N/A


[p01_home_title.htm:Number]
1

[p01_home_title.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_SYS_STATUS	32			N/A			System Status	N/A		Sistem Durumu		N/A


[p_main_menu.html:Number]
0

[p_main_menu.html]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[excanvas.js:Number]
0

[excanvas.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[jquery.emsMeter.js:Number]
0

[jquery.emsMeter.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[jquery.emsplot.js:Number]
1

[jquery.emsplot.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_NO_DATA	64			N/A			There is no data to show!	N/A		Gosterecek hicbir veri yok!		N/A

[jquery.emsplot.style.css:Number]
0

[jquery.emsplot.style.css]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[jquery.emsThermometer.js:Number]
0

[jquery.emsThermometer.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[jquery.min.js:Number]
0

[jquery.min.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[line_data.htm:Number]
1

[line_data.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_NO_DATA	64			N/A			There is no data to show!	N/A		Gosterecek hicbir veri yok!		N/A

[p12_nmsv3_config.htm:Number]
50

[p12_nmsv3_config.htm]
#Sequence ID	RES_ID				MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN										ABBR_IN_EN		FULL_IN_LOCALE						ABBR_IN_LOCALE
1		ID_ERROR0			16			N/A			Unknown error.										N/A			Error desconocido!					N/A
2		ID_ERROR1			64			N/A			Failure, the NMS already exists.							N/A			Error. El servidor NMS ya existe			N/A
3		ID_ERROR2			32			N/A			Success											N/A			Exito							N/A
4		ID_ERROR3			64			N/A			Failure, incomplete information.							N/A			Fallo, información incompleta!				N/A
5		ID_ERROR4			64			N/A			Failure, you are not authorized.							N/A			Fallo. No tiene la autorización requerida		N/A
6		ID_ERROR5			64			N/A			Controller is hardware protected, can't be modified.					N/A			ACU protegida por HW. No se puede modificar!		N/A
7		ID_ERROR6			64			N/A			Failure, the maximum number is exceeded.						N/A			Fallo: Máximo excedido					N/A
8		ID_NMS_HEAD1			32			N/A			NMS V3 Configuration									N/A			Configuración NMS V3					N/A
9		ID_NMS_HEAD2			32			N/A			Current NMS										N/A			NMS actual						N/A
10		ID_NMS_IP			16			N/A			NMS IP											N/A			IP NMS							N/A
11		ID_NMS_AUTHORITY		16			N/A			Authority										N/A			Autoridad						N/A
12		ID_NMS_TRAP			32			N/A			Accepted Trap Level									N/A			Aceptar nivel Trap					N/A
13		ID_NMS_IP			16			N/A			NMS IP											N/A			IP NMS							N/A
14		ID_NMS_AUTHORITY		16			N/A			Authority										N/A			Autoridad						N/A
15		ID_NMS_TRAP			32			N/A			Accepted Trap Level									N/A			Aceptar nivel Trap					N/A
16		ID_NMS_ADD			16			N/A			Add New NMS										N/A			Añadir nuevo NMS					N/A
17		ID_NMS_MODIFY			32			N/A			Modify NMS										N/A			Modificar NMS						N/A
18		ID_NMS_DELETE			32			N/A			Delete NMS										N/A			Borrar NMS						N/A
19		ID_NMS_PUBLIC			32			N/A			Public Community									N/A			Comunidad pública					N/A
20		ID_NMS_PRIVATE			32			N/A			Private Community									N/A			Comunidad privada					N/A
21		ID_NMS_LEVEL0			16			N/A			Not Used										N/A			No utilizado						N/A
22		ID_NMS_LEVEL1			16			N/A			No Access										N/A			Sin acceso						N/A
23		ID_NMS_LEVEL2			32			N/A			Query Authority										N/A			Autoridad de consulta					N/A
24		ID_NMS_LEVEL3			32			N/A			Control Authority									N/A			Autoridad de Control					N/A
25		ID_NMS_LEVEL4			32			N/A			Administrator										N/A			Administrador						N/A
26		ID_NMS_TRAP_LEVEL0		16			N/A			NoAuthNoPriv										N/A			NoAuthNoPriv						N/A
27		ID_NMS_TRAP_LEVEL1		16			N/A			AuthNoPriv										N/A			AuthNoPriv						N/A
28		ID_NMS_TRAP_LEVEL2		16			N/A			AuthPriv										N/A			AuthPriv							N/A
29		ID_NMS_TRAP_LEVEL3		16			N/A			Critical Alarms										N/A			A1							N/A
30		ID_NMS_TRAP_LEVEL4		16			N/A			No traps										N/A			No hay Traps						N/A
31		ID_TIPS0			128			N/A			Incorrect IP address of NMS.\nFormat should be 'nnn.nnn.nnn.nnn',\ne.g.,10.76.8.29	N/A			Dirección IP de NMS incorrecta.\nFormato 'nnn.nnn.nnn.nnn'\ne.g.: 10.76.8.29	N/A
32		ID_TIPS1			128			N/A			Priv Password DES or Auth Password MD5 can't be empty. Please try again.		N/A			Contraseña no válida. Pruebe otra vez.			N/A
33		ID_TIPS2			128			N/A			already exists, please try again.							N/A			Ya existe, pruebe otra vez.				N/A
34		ID_TIPS3			128			N/A			does not exist, so it can't be modified. Please try again.				N/A			No existe. No se puede modificar. Inténtelo de nuevo.				N/A
35		ID_TIPS4			128			N/A			Please select one or more NMS before clicking this button.				N/A			Por favor, seleccione uno o más NMS antes de pulsar	N/A
36		ID_TIPS5			128			N/A			NMS Info Configuration									N/A			Configuración información NMS				N/A
41		ID_TIPS6			128			N/A			User Name can't be empty								N/A			El nombre de usuario no puede ser nulo.					N/A
42		ID_NMS_USERNAME			128			N/A			User Name										N/A			Nombre de Usuario							N/A
43		ID_NMS_TRAP_IP			128			N/A			Trap IP Address										N/A			Dirección IP de Trap							N/A
44		ID_NMS_TRAP_LEVEL		128			N/A			Trap Security Level									N/A			Nivel de Seguiridad de Trap					N/A
45		ID_NMS_DES			128			N/A			Priv Password DES									N/A			Contraseña DES Priv					N/A
46		ID_NMS_MD5			128			N/A			Auth Password MD5									N/A			Contraseña Autoriz MD5 					N/A
47		ID_NMS_ENGINID			128			N/A			Trap Engine ID										N/A			ID de máquina Trap						N/A
48		ID_NMS_USERNAME_DISPLAY		128			N/A			User Name										N/A			Nombre de Usuario							N/A
49		ID_TRAP_IP_DISPLAY		128			N/A			Trap IP Address										N/A			Dirección IP de Trap							N/A
50		ID_NMS_DES_DISPLAY		128			N/A			Priv Password DES									N/A			Contraseña DES Priv					N/A
51		ID_NMS_MD5_DISPLAY		128			N/A			Auth Password MD5									N/A			Contraseña Autoriz MD5					N/A
52		ID_NMS_TRAP_ENGINE		128			N/A			Trap Engine ID										N/A			ID de máquina Trap							N/A
53		ID_NMS_TRAP_LEVE_DISPLAY	128			N/A			Trap Security Level									N/A			Nivel de Seguiridad de Trap						N/A
54		ID_ERROR7			128			N/A			SNMPV3 is not supported									N/A			SNMPV3 no soportado					N/A

