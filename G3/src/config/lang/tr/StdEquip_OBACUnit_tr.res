#																			
#  Locale language support: Turkish																		
#																			
																			
#																			
# RES_ID: Resource ID 																			
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 																			
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)																			
# FULL_IN_EN: Full English name																			
# ABBR_IN_EN: Abbreviated English name																			
# FULL_IN_LOCALE: Full name in locale language																			
# ABBR_IN_LOCALE: Abbreviated locale name																			
#																			
[LOCALE_LANGUAGE]																			
tr																			
																			
																			
[RES_INFO]																			
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE							
1	32			15			Phase A Voltage				Phase A Volt		Faz R Gerilim			Faz R Gerilim			
2	32			15			Phase B Voltage				Phase B Volt		Faz S Gerilim			Faz S Gerilim			
3	32			15			Phase C Voltage				Phase C Volt		Faz T Gerilim			Faz T Gerilim			
4	32			15			Line AB Voltage				Line AB Volt		R-S Gerilim			R-S Gerilim			
5	32			15			Line BC Voltage				Line BC Volt		S-T Gerilim			S-T Gerilim			
6	32			15			Line CA Voltage				Line CA Volt		T-R Gerilim			T-R Gerilim			
7	32			15			Phase A Current				Phase A Curr		Faz R Akim			Faz R Akim				
8	32			15			Phase B Current				Phase B Curr		Faz S Akim			Faz S Akim				
9	32			15			Phase C Current				Phase C Curr		Faz T Akim			Faz T Akim				
10	32			15			Frequency				AC Frequency		Frekans				Frekans			
11	32			15			Total Real Power			Total RealPower		Toplam Gercek Guc		Top. Gercek Guc					
12	32			15			Phase A Real Power			Real Power A		Faz R Gercek Guc		Gercek Guc R					
13	32			15			Phase B Real Power			Real Power B		Faz S Gercek Guc		Gercek Guc S					
14	32			15			Phase C Real Power			Real Power C		Faz T Gercek Guc		Gercek Guc T					
15	32			15			Total Reactive Power			Tot React Power		Toplam Reaktif Guc		Top.Reaktif Guc						
16	32			15			Phase A Reactive Power			React Power A		Faz R Reaktif Guc		Reaktif Guc R					
17	32			15			Phase B Reactive Power			React Power B		Faz S Reaktif Guc		Reaktif Guc S					
18	32			15			Phase C Reactive Power			React Power C		Faz T Reaktif Guc		Reaktif Guc T					
19	32			15			Total Apparent Power			Total App Power		Toplam Gorunur Guc	Top. Gorunur Guc						
20	32			15			Phase A Apparent Power			App Power A		Faz R Gorunur Guc		Gorunur Guc R						
21	32			15			Phase B Apparent Power			App Power B		Faz S Gorunur Guc		Gorunur Guc S						
22	32			15			Phase C Apparent Power			App Power C		Faz T Gorunur Guc		Gorunur Guc T						
23	32			15			Power Factor				Power Factor		Guc Faktoru			Guc Faktoru				
24	32			15			Phase A Power Factor			Power Factor A		Guc Faktoru Faz A		Guc Faktoru R					
25	32			15			Phase B Power Factor			Power Factor B		Guc Faktoru Faz B		Guc Faktoru S					
26	32			15			Phase C Power Factor			Power Factor C		Guc Faktoru Faz C		Guc Faktoru T					
27	32			15			Phase A Current Crest Factor		Ia Crest Factor		Faz R Akim Crest Faktoru		Ir CrestFaktoru							
28	32			15			Phase B Current Crest Factor		Ib Crest Factor		Faz S Akim Crest Faktoru		Is CrestFaktoru							
29	32			15			Phase C Current Crest Factor		Ic Crest Factor		Faz T Akim Crest Faktoru		It CrestFaktoru							
30	32			15			Phase A Current THD			Current THD A		Faz R Akimi THD		Faz R Akimi THD					
31	32			15			Phase B Current THD			Current THD B		Faz S Akimi THD		Faz S Akimi THD					
32	32			15			Phase C Current THD			Current THD C		Faz T Akimi THD		Faz T Akimi THD					
33	32			15			Phase A Voltage THD			Voltage THD A		Faz R Gerilimi THD		Faz R Ger. THD					
34	32			15			Phase A Voltage THD			Voltage THD B		Faz S Gerilimi THD		Faz S Ger. THD					
35	32			15			Phase A Voltage THD			Voltage THD C		Faz T Gerilimi THD		Faz T Ger. THD 					
36	32			15			Total Real Energy			Tot Real Energy		Toplam Gercek Enerji		TopGercekEnerji					
37	32			15			Total Reactive Energy			Tot ReactEnergy		Toplam Reaktif Enerji	TopReaktifEnerji					
38	32			15			Total Apparent Energy			Tot App Energy		Toplam Gorunur Enerji	TopGorunurEnerji					
39	32			15			Ambient Temperature			Ambient Temp		Ortam Sicakligi		Ortam Sicakligi					
40	32			15			Nominal Line Voltage			Nominal L-Volt		Nominal Hat Gerilimi	Nominal H.Ger.					
41	32			15			Nominal Phase Voltage			Nominal PH-Volt		Nominal Faz Gerilimi	Nominal F.Ger.					
42	32			15			Nominal Frequency			Nom Frequency		Nominal Frekans		Nominal Frekans				
43	32			15			Mains Failure Alarm Threshold 1	Volt Threshold1		Sebeke Arizasi Alarm Esigi 1		Seb.AlarmEsigi1							
44	32			15			Mains Failure Alarm Threshold 2 	Volt Threshold2		Sebeke Arizasi Alarm Esigi 2		Seb.AlarmEsigi2						
45	32			15			Voltage Alarm Threshold 1		Volt AlmThres 1		Gerilim Alarm Esigi 1	Ger.AlarmEsigi1						
46	32			15			Voltage Alarm Threshold 2		Volt AlmThres 2		Gerilim Alarm Esigi 2	Ger.AlarmEsigi2						
47	32			15			Frequency Alarm Threshold		Freq AlarmThres		Frekans Alarm Esigi 	FreK.AlarmEsigi						
48	32			15			High Temperature Limit			High Temp Limit		Yuksek Sicaklik Limiti	 Yuk.Sic.Limit					
49	32			15			Low Temperature Limit			Low Temp Limit		Dusuk Sicaklik Limiti	 Dus.Sic.Limit					
50	32			15			Supervision Failure			Supervision Fail		Denetleme Arizasi		Denetl.Arizasi				
51	32			15			Line AB Overvoltage 1			L-AB Overvolt1		RS Hatty Yuksek Gerilim 1	RS-Hat YukGer1					
52	32			15			Line AB Overvoltage 2			L-AB Overvolt2		RS Hatty Yuksek Gerilim 2	RS-Hat YukGer2					
53	32			15			Line AB Undervoltage 1			L-AB Undervolt1		RS Hatty Dusuk Gerilim 1	RS-Hat DusGer1					
54	32			15			Line AB Undervoltage 2			L-AB Undervolt2		RS Hatty Dusuk Gerilim 2	RS-Hat DusGer2					
55	32			15			Line BC Overvoltage 1			L-BC Overvolt1		ST Hatty Yuksek Gerilim 1	ST-Hat YukGer1 				
56	32			15			Line BC Overvoltage 2			L-BC Overvolt2		ST Hatty Yuksek Gerilim 2	ST-Hat YukGer2				
57	32			15			Line BC Undervoltage 1			L-BC Undervolt1		ST Hatty Dusuk Gerilim 1	ST-Hat DusGer1 					
58	32			15			Line BC Undervoltage 2			L-BC Undervolt2		ST Hatty Dusuk Gerilim 2	ST-Hat DusGer2 					
59	32			15			Line CA Overvoltage 1			L-CA Overvolt1		TR Hatty Yuksek Gerilim 1	TR-Hat YukGer1				
60	32			15			Line CA Overvoltage 2			L-CA Overvolt2		TR Hatty Yuksek Gerilim 2	TR-Hat YukGer2				
61	32			15			Line CA Undervoltage 1			L-CA Undervolt1		TR Hatty Dusuk Gerilim 1	TR-Hat DusGer1 					
62	32			15			Line CA Undervoltage 2			L-CA Undervolt2		TR Hatty Dusuk Gerilim 2	TR-Hat DusGer2  					
63	32			15			Phase A Overvoltage 1			PH-A Overvolt1		R Fazi Yuksek Gerilim 1	R Fazi YukGer1 				
64	32			15			Phase A Overvoltage 2			PH-A Overvolt2		R Fazi Yuksek Gerilim 2	R Fazi YukGer2  				
65	32			15			Phase A Undervoltage 1			PH-A Undervolt1		R Fazi Dusuk Gerilim 1	R Fazi DusGer1 					
66	32			15			Phase A Undervoltage 2			PH-A Undervolt2		R Fazi Dusuk Gerilim 2	R Fazi DusGer2  					
67	32			15			Phase B Overvoltage 1			PH-B Overvolt1		S Fazi Yuksek Gerilim 1	S Fazi YukGer1				
68	32			15			Phase B Overvoltage 2			PH-B Overvolt2		S Fazi Yuksek Gerilim 2	S Fazi YukGer2 				
69	32			15			Phase B Undervoltage 1			PH-B Undervolt1		S Fazi Dusuk Gerilim 1	S Fazi DusGer1					
70	32			15			Phase B Undervoltage 2			PH-B Undervolt2		S Fazi Dusuk Gerilim 2	S Fazi DusGer2					
71	32			15			Phase C Overvoltage 1			PH-C Overvolt1		T Fazi Yuksek Gerilim 1	T Fazi YukGer1  				
72	32			15			Phase C Overvoltage 2			PH-C Overvolt2		T Fazi Yuksek Gerilim 2	T Fazi YukGer2  				
73	32			15			Phase C Undervoltage 1			PH-C Undervolt1		T Fazi Dusuk Gerilim 1	T Fazi DusGer1					
74	32			15			Phase C Undervoltage 2			PH-C Undervolt2		T Fazi Dusuk Gerilim 2	T Fazi DusGer2 					
75	32			15			Mains Failure				Mains Failure		Sebeke Arizasi		Sebeke Arizasi		
76	32			15			Severe Mains Failure			SevereMainsFail		Asiri Sebeke Arizasi	Asi.Seb.Arizasi					
77	32			15			High Frequency				High Frequency		Yuksek Frekans		Yuk. Frekans			
78	32			15			Low Frequency				Low Frequency		Dusuk Frekans		Dus. Frekans		
79	32			15			High Temperature				High Temp		Yuksek Sicaklik		Yuk. Sicaklik					
80	32			15			Low Temperature				Low Temperature		Dusuk Sicaklik		Dus. Sicaklik				
81	32			15			AC Unit					AC Unit			AC Birim			AC Birim	
82	32			15			Supervision Failure				Supervision Fail		Denetleme Arizasi		Denetl.Arizasi					
83	32			15			No					No			Hayir				Hayir
84	32			15			Yes					Yes			Evet				Evet
85	32			15			Phase A Mains Failure Counter		A Mains Fail Cnt	FazR Seb Aryza Sayaci		FazR Seb AriSay							
86	32			15			Phase B Mains Failure Counter		B Mains Fail Cnt	FazS Seb Aryza Sayaci		FazS Seb AriSay						
87	32			15			Phase C Mains Failure Counter		C Mains Fail Cnt	FazT Seb Aryza Sayaci		FazT Seb AriSay						
88	32			15			Frequency Failure Counter		Freq Fail Cnt		Frekans Ariza Sayaci	Frek Ari.Sayaci							
89	32			15			Reset Phase A Mains Fail Counter	Reset A FailCnt		FazR Seb Aryza Sayaci Reset		FazR Seb AriSayR 								
90	32			15			Reset Phase B Mains Fail Counter	Reset B FailCnt		FazS Seb Aryza Sayaci Reset		FazS Seb AriSayR								
91	32			15			Reset Phase C Mains Fail Counter	Reset C FailCnt		FazT Seb Aryza Sayaci Reset		FazT Seb AriSayR								
92	32			15			Reset Frequency Counter			ResFreqFailCnt		Frekans Sayaci Reset	FrekSayReset						
93	32			15			Current Alarm Threshold			Curr Alarm Limit	Akim Alarmi Esigi		AkimAlarmEsigi					
94	32			15			Phase A High Current			A High Current		Faz R Yuksek Akim		R Yuks.Akim					
95	32			15			Phase B High Current			B High Current		Faz S Yuksek Akim		S Yuks.Akim					
96	32			15			Phase C High Current			C High Current		Faz T Yuksek Akim		T Yuks.Akim					
97	32			15			State					State			Durum				Durum
98	32			15			Off					Off			Kapali				Kapali
99	32			15			On					On			Acik				Acik	
100	32			15			State					State			Durum				Durum
101	32			15			Existent					Existent			Mevcut			Mevcut		
102	32			15			Non-Existent				Non-Existent		Mevcut Degil 	Mevcut Degil			
103	32			15			AC Type					AC Type			AC Tipi			AC Tipi	
104	32			15			None					None			Hicbiri				Hicbiri
105	32			15			Single Phase				Single Phase		Tek Faz			Tek Faz
106	32			15			Three Phases				Three Phases		3 Faz			3 Faz 		
107	32			15			Mains Failure(Single)			Mains Failure		Sebeke Arizasi		Sebeke Arizasi					
108	32			15			Severe Mains Failure(Single)		SevereMainsFail		Asiri Sebeke Arizasi(Tek)		Asi.Seb.Arizasi						
