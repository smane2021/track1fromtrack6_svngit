#																	
#  Locale language support: Turkish																	
#																	
																	
#																	
# RES_ID: Resource ID 																	
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 																	
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)																	
# FULL_IN_EN: Full English name																	
# ABBR_IN_EN: Abbreviated English name																	
# FULL_IN_LOCALE: Full name in locale language																	
# ABBR_IN_LOCALE: Abbreviated locale name																	
#																	
[LOCALE_LANGUAGE]																	
tr																	
																	
																	
																	
[RES_INFO]																	
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE							
1		32			15			Fuse 1			Fuse 1			Sigorta 1		Sigorta 1	
2		32			15			Fuse 2			Fuse 2			Sigorta 2		Sigorta 2	
3		32			15			Fuse 3			Fuse 3			Sigorta 3		Sigorta 3	
4		32			15			Fuse 4			Fuse 4			Sigorta 4		Sigorta 4	
5		32			15			Fuse 5			Fuse 5			Sigorta 5		Sigorta 5	
6		32			15			Fuse 6			Fuse 6			Sigorta 6		Sigorta 6	
7		32			15			Fuse 7			Fuse 7			Sigorta 7		Sigorta 7	
8		32			15			Fuse 8			Fuse 8			Sigorta 8		Sigorta 8	
9		32			15			Fuse 9			Fuse 9			Sigorta 9		Sigorta 9	
10		32			15			Auxillary Load Fuse		Aux Load Fuse		Yuk Sigortasi	Yuk Sigortasi				
11		32			15			Fuse 1 Alarm		Fuse 1 Alarm		Yuk Sigortasi Atik	Yuk Sig Atik			
12		32			15			Fuse 2 Alarm		Fuse 2 Alarm		Alarm Sigorta 2		Alarm Sig. 2			
13		32			15			Fuse 3 Alarm		Fuse 3 Alarm		Alarm Sigorta 3		Alarm Sig. 3			
14		32			15			Fuse 4 Alarm		Fuse 4 Alarm		Alarm Sigorta 4		Alarm Sig. 4			
15		32			15			Fuse 5 Alarm		Fuse 5 Alarm		Alarm Sigorta 5		Alarm Sig. 5			
16		32			15			Fuse 6 Alarm		Fuse 6 Alarm		Alarm Sigorta 6		Alarm Sig. 6			
17		32			15			Fuse 7 Alarm		Fuse 7 Alarm		Alarm Sigorta 7		Alarm Sig. 7			
18		32			15			Fuse 8 Alarm		Fuse 8 Alarm		Alarm Sigorta 8		Alarm Sig. 8			
19		32			15			Fuse 9 Alarm		Fuse 9 Alarm		Alarm Sigorta 9		Alarm Sig. 9			
20		32			15			Auxillary Load Alarm	AuxLoad Alarm		Alarm Aux Yük		Alarm Aux Yuk				
21		32			15			On			On			Acik			Acik
22		32			15			Off			Off			Kapali			Kapali
23		32			15			DC Fuse Unit		DC Fuse Unit		DC Sigorta Unitesi		DC Sig Uni.			
24		32			15			Fuse 1 Voltage		Fuse 1 Voltage		Sig. 1 Gerilim		Sig. 1 Gerilim			
25		32			15			Fuse 2 Voltage		Fuse 2 Voltage		Sig. 2 Gerilim		Sig. 2 Gerilim			
26		32			15			Fuse 3 Voltage		Fuse 3 Voltage		Sig. 3 Gerilim		Sig. 3 Gerilim			
27		32			15			Fuse 4 Voltage		Fuse 4 Voltage		Sig. 4 Gerilim		Sig. 4 Gerilim			
28		32			15			Fuse 5 Voltage		Fuse 5 Voltage		Sig. 5 Gerilim		Sig. 5 Gerilim			
29		32			15			Fuse 6 Voltage		Fuse 6 Voltage		Sig. 6 Gerilim		Sig. 6 Gerilim			
30		32			15			Fuse 7 Voltage		Fuse 7 Voltage		Sig. 7 Gerilim		Sig. 7 Gerilim			
31		32			15			Fuse 8 Voltage		Fuse 8 Voltage		Sig. 8 Gerilim		Sig. 8 Gerilim			
32		32			15			Fuse 9 Voltage		Fuse 9 Voltage		Sig. 9 Gerilim		Sig. 9 Gerilim			
33		32			15			Fuse 10 Voltage		Fuse 10 Voltage		Sig. 10 Gerilim		Sig. 10 Gerilim			
34		32			15			Fuse 10			Fuse 10			Sigorta 10		Sigorta 10	
35		32			15			Fuse 10 Alarm 		Fuse 10 Alarm		Alarm Sigorta 10	Alarm Sigorta 10				
36		32			15			State			State			Durum			Durum
37		32			15			Failure			Failure			Hata			Hata
38		32			15			No			No			Hayir			Hayir
39		32			15			Yes			Yes			Evet			Evet
40	        32     			15             		Fuse 11				Fuse 11		Sigorta 11		Sigorta 11
41	        32     			15             		Fuse 12				Fuse 12		Sigorta 12		Sigorta 12
42	        32     			15             		Fuse 11 Alarm 	Fuse 11 Alarm			Sigorta 11 Alarmi	Sigorta 11 Alarm 
43	        32     			15             		Fuse 12 Alarm 	Fuse 12 Alarm			Sigorta 12 Alarmi	Sigorta 12 Alarm



