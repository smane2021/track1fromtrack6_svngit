#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
tr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			Bus Bar Voltage			Bus Bar Voltage			Bus Bar Gerilim		Bus Bar Gerilim
2	32			15			Load 1 Current			Load1 Current		Yuk 1 Akim			Yuk 1 Akim
3	32			15			Load 2 Current			Load2 Current		Yuk 2 Akim			Yuk 2 Akim
4	32			15			Load 3 Current			Load3 Current		Yuk 3 Akim			Yuk 3 Akim
5	32			15			Load 4 Current			Load4 Current		Yuk 4 Akim			Yuk 4 Akim
6	32			15			Load Fuse 1			Load Fuse1		Yuk 1 Sigorta			Yuk 1 Sig
7	32			15			Load Fuse 2			Load Fuse2		Yuk 2 Sigorta			Yuk 2 Sig
8	32			15			Load Fuse 3			Load Fuse3		Yuk 3 Sigorta			Yuk 3 Sig
9	32			15			Load Fuse 4			Load Fuse4		Yuk 4 Sigorta			Yuk 4 Sig
10	32			15			Load Fuse 5			Load Fuse5		Yuk 5 Sigorta			Yuk 5 Sig
11	32			15			Load Fuse 6			Load Fuse6		Yuk 6 Sigorta			Yuk 6 Sig
12	32			15			Load Fuse 7			Load Fuse7		Yuk 7 Sigorta			Yuk 7 Sig
13	32			15			Load Fuse 8			Load Fuse8		Yuk 8 Sigorta			Yuk 8 Sig
14	32			15			Load Fuse 9			Load Fuse9		Yuk 9 Sigorta			Yuk 9 Sig
15	32			15			Load Fuse 10			Load Fuse10		Yuk 10 Sigorta			Yuk 10 Sig
16	32			15			Load Fuse 11			Load Fuse11		Yuk 11 Sigorta			Yuk 11 Sig
17	32			15			Load Fuse 12			Load Fuse12		Yuk 12 Sigorta			Yuk 12 Sig
18	32			15			Load Fuse 13			Load Fuse13		Yuk 13 Sigorta			Yuk 13 Sig
19	32			15			Load Fuse 14			Load Fuse14		Yuk 14 Sigorta			Yuk 14 Sig
20	32			15			Load Fuse 15			Load Fuse15		Yuk 15 Sigorta			Yuk 15 Sig
21	32			15			Load Fuse 16			Load Fuse16		Yuk 16 Sigorta			Yuk 16 Sig
22	32			15			Run Time			Run Time		Calisma Zamani			Calisma Zamani
23	32			15			LVD1 Control			LVD1 Control		LVD1 Kontrol			LVD1 Kontrol
24	32			15			LVD2 Control			LVD2 Control		LVD2 Kontrol			LVD2 Kontrol
25	32			15			LVD1 Voltage			LVD1 Voltage		LVD1 Gerilimi			LVD1 Gerilimi
26	32			15			LVR1 Voltage			LVR1 Voltage		LVR1 Gerilimi			LVR1 Gerilimi
27	32			15			LVD2 Voltage			LVD2 Voltage		LVD2 Gerilimi			LVD2 Gerilimi
28	32			15			LVR2 voltage			LVR2 voltage		LVR2 Gerilimi			LVR2 Gerilimi
29	32			15			On				On			Acik				Acik
30	32			15			Off				Off			Kapali				Kapali	
31	32			15			Normal				Normal			Normal				Normal
32	32			15			Error				Error			Hata				Hata
33	32			15			On				On			Acik				Acik
34	32			15			Fuse 1 Alarm			Fuse1 Alarm		Sigorta 1 Alarm			Sig 1 Alarm
35	32			15			Fuse 2 Alarm			Fuse2 Alarm		Sigorta 2 Alarm			Sig 2 Alarm
36	32			15			Fuse 3 Alarm			Fuse3 Alarm		Sigorta 3 Alarm			Sig 3 Alarm
37	32			15			Fuse 4 Alarm			Fuse4 Alarm		Sigorta 4 Alarm			Sig 4 Alarm
38	32			15			Fuse 5 Alarm			Fuse5 Alarm		Sigorta 5 Alarm			Sig 5 Alarm
39	32			15			Fuse 6 Alarm			Fuse6 Alarm		Sigorta 6 Alarm			Sig 6 Alarm
40	32			15			Fuse 7 Alarm			Fuse7 Alarm		Sigorta 7 Alarm			Sig 7 Alarm
41	32			15			Fuse 8 Alarm			Fuse8 Alarm		Sigorta 8 Alarm			Sig 8 Alarm
42	32			15			Fuse 9 Alarm			Fuse9 Alarm		Sigorta 9 Alarm			Sig 9 Alarm
43	32			15			Fuse 10 Alarm			Fuse10 Alarm		Sigorta 10 Alarm		Sig 10 Alarm
44	32			15			Fuse 11 Alarm			Fuse11 Alarm		Sigorta 11 Alarm		Sig 11 Alarm
45	32			15			Fuse 12 Alarm			Fuse12 Alarm		Sigorta 12 Alarm		Sig 12 Alarm
46	32			15			Fuse 13 Alarm			Fuse13 Alarm		Sigorta 13 Alarm		Sig 13 Alarm
47	32			15			Fuse 14 Alarm			Fuse14 Alarm		Sigorta 14Alarm			Sig 14 Alarm
48	32			15			Fuse 15 Alarm			Fuse15 Alarm		Sigorta 15 Alarm		Sig 15 Alarm
49	32			15			Fuse 16 Alarm			Fuse16 Alarm		Sigorta 16 Alarm		Sig 16 Alarm
50	32			15			HW Test Alarm			HW Test Alarm		HW Test Alarm			HW Test Alarm
51	32			15			SM-DU Unit 5			SM-DU 5			SM-DU 5				SM-DU 5
52	32			15			Battery Fuse 1 Voltage		BATT Fuse1 Volt		Aku Sig 1 Gerilim		AkuSig1 Gerilim
53	32			15			Battery Fuse 2 Voltage		BATT Fuse2 Volt		Aku Sig 2 Gerilim		AkuSig2 Gerilim
54	32			15			Battery Fuse 3 Voltage		BATT Fuse3 Volt		Aku Sig 3 Gerilim		AkuSig3 Gerilim
55	32			15			Battery Fuse 4 Voltage		BATT Fuse4 Volt		Aku Sig 4 Gerilim		AkuSig4 Gerilim
56	32			15			Battery Fuse 1 Status		BATT Fuse1 Stat		Aku Sig 1 Durum			AkuSig1 Durum
57	32			15			Battery Fuse 2 Status		BATT Fuse2 Stat		Aku Sig 2 Durum			AkuSig2 Durum
58	32			15			Battery Fuse 3 Status		BATT Fuse3 Stat		Aku Sig 3 Durum			AkuSig3 Durum
59	32			15			Battery Fuse 4 Status		BATT Fuse4 Stat		Aku Sig 4 Durum			AkuSig4 Durum
60	32			15			On				On			Acik				Acik
61	32			15			Off				Off			Kapali				Kapali
62	32			15			Battery Fuse 1 Alarm		Bat Fuse1 Alarm		Aku Sig 1 Alarm			AkuSig1 Alarm
63	32			15			Battery Fuse 2 Alarm		Bat Fuse2 Alarm		Aku Sig 2 Alarm			AkuSig2 Alarm
64	32			15			Battery Fuse 3 Alarm		Bat Fuse3 Alarm		Aku Sig 3 Alarm			AkuSig3 Alarm
65	32			15			Battery Fuse 4 Alarm		Bat Fuse4 Alarm		Aku Sig 4 Alarm			AkuSig4 Alarm
66	32			15			Total Load Current		Tot Load Curr		Toplam Yuk Akim			Top Yuk Akim
67	32			15			Over Current Point(Load)		Over Curr Point		Asiri Akim Limit		Asiri Akim Limit
68	32			15			Over Current			Over Current		Asiri Akim			Asiri Akim
69	32			15			LVD1 Enabled			LVD1 Enabled		LVD1 Etkin			LVD1 Etkin
70	32			15			LVD1 Mode			LVD1 Mode		LVD1 Mod			LVD1 Mod
71	32			15			LVD1 Reconnect Delay		LVD1Recon Delay		LVD1Yeniden Bag.Gec		LVD1Yeniden Bag.Gec
72	32			15			LVD2 Enabled			LVD2 Enabled		LVD2 Etkin			LVD2 Etkin
73	32			15			LVD2 Mode			LVD2 Mode		LVD2 Mod			LVD2 Etkin
74	32			15			LVD2 Reconnect Delay		LVD2Recon Delay		LVD2Yeniden Bag.Gec		LVD2Yeniden Bag.Gec
75	32			15			LVD1 Status			LVD1 Status		LVD1 Durum			LVD1 Durum
76	32			15			LVD2 Status			LVD2 Status		LVD2 Durum			LVD2 Durum
77	32			15			Disabled			Disabled		Devre Disi			Devre Disi
78	32			15			Enabled				Enabled			Devrede				Devrede
79	32			15			By Voltage			By Volt			Volt Olarak			Volt Olarak
80	32			15			By Time				By Time			Zaman Olarak			Zaman Olarak
81	32			15			Bus Bar Volt Alarm		Bus Bar Alarm		Bus Bar Alarm			Bus Bar Alarm
82	32			15			Normal				Normal			Normal				Normal
83	32			15			Low				Low			Dusuk				Dusuk
84	32			15			High				High			Yuksek				Yuksek
85	32			15			Low Voltage			Low Voltage		Dusuk Gerilim			Dusuk Gerilim
86	32			15			High Voltage			High Voltage		Yuksek Gerilim			Yuksek Gerilim
87	32			15			Shunt 1 Current Alarm		Shunt1 Alarm		Sont1 Alarm			Sont1 Alarm
88	32			15			Shunt 2 Current Alarm		Shunt2 Alarm		Sont2 Alarm			Sont2 Alarm
89	32			15			Shunt 3 Current Alarm		Shunt3 Alarm		Sont3 Alarm			Sont3 Alarm
90	32			15			Shunt 4 Current Alarm		Shunt4 Alarm		Sont4 Alarm			Sont4 Alarm
91	32			15			Shunt 1 Over Current		Shunt1 Over Cur		Sont1 Asiri Akim		Sont1AsiriAkim
92	32			15			Shunt 2 Over Current		Shunt2 Over Cur		Sont2 Asiri Akim		Sont2AsiriAkim
93	32			15			Shunt 3 Over Current		Shunt3 Over Cur		Sont3 Asiri Akim		Sont3AsiriAkim
94	32			15			Shunt 4 Over Current		Shunt4 Over Cur		Sont4 Asiri Akim		Sont4AsiriAkim
95	32			15			Interrupt Times			Interrupt Times		Kesme Zamani			Kesme Zamani
96	32			15			Existent			Existent		Mevcut				Mevcut
97	32			15			Non-Existent			Non-Existent		Mevcut Degil			Mevcut Degil
98	32			15			Very Low			Very Low		Asiri Dusuk			Asiri Dusuk
99	32			15			Very High			Very High		Asiri Yusek			Asiri Dusuk
100	32			15			Switch				Switch			Anahtar				Anahtar
101	32			15			LVD1 Failure			LVD1 Failure		LVD1 Hata			LVD1 Hata
102	32			15			LVD2 Failure			LVD2 Failure		LVD2 Hata			LVD2 Hata
103	32			15			HTD1 Enable			HTD1 Enable		HTD1 Etkin			HTD1 Etkin
104	32			15			HTD2 Enable			HTD2 Enable		HTD2 Etkin			HTD2 Etkin
105	32			15			Battery LVD			Battery LVD		LVD Aku				LVD Aku
106	32			15			No Battery			No Battery		Aku Yok				Aku Yok	
107	32			15			LVD1				LVD1			LVD1				LVD1
108	32			15			LVD2				LVD2			LVD2				LVD2
109	32			15			Battery Always On		BattAlwaysOn		Aku Hep Acik			Aku Hep Acik
110	32			15			Barcode				Barcode			Barkod				Barkod
111	32			15			DC Overvoltage			DC Overvolt		DC Asiri Gerilim		DC Asiri Gerilim
112	32			15			DC Undervoltage			DC Undervolt		DC Dusuk Gerilim		DC Dusuk Gerilim
113	32			15			Overcurrent 1			Overcurr 1		Asiri Akim 1			Asiri Akim 1
114	32			15			Overcurrent 2			Overcurr 2		Asiri Akim 2			Asiri Akim 2
115	32			15			Overcurrent 3			Overcurr 3		Asiri Akim 3			Asiri Akim 3
116	32			15			Overcurrent 4			Overcurr 4		Asiri Akim 4			Asiri Akim 4
117	32			15			Existence State			Existence State		Mevcut Durum			Mevcut Durum
118	32			15			Communication Interrupt		Comm Interrupt		Haberlesme Kesik		Hab. Kesik
119	32			15			Bus Voltage Status		Bus Status		Bus Gerilim Durumu			Bus Durum
120	32			15			Communication OK		Comm OK			Haberlesme Ok			Hab. Ok
121	32			15			None is Responding		None Responding		Yanit Yok			Yanit Yok
122	32			15			No Response			No Response		Yanit Yok			Yanit Yok	
123	32			15			Rated Battery Capacity		Rated Bat Cap		Anma Aku Kapasitesi		Anma Aku Kap.
124	32			15			Load 5 Current			Load5 Current		Yuk 5 Akim			Yuk 5 Akim
125	32			15			Shunt 1 Voltage			Shunt 1 Voltage		Sont1 Gerilim			Sont1 Gerilim
126	32			15			Shunt 1 Current			Shunt 1 Current		Sont1 Akim			Sont1 Akim
127	32			15			Shunt 2 Voltage			Shunt 2 Voltage		Sont2 Gerilim			Sont2 Gerilim
128	32			15			Shunt 2 Current			Shunt 2 Current		Sont2 Akim			Sont2 Akim
129	32			15			Shunt 3 Voltage			Shunt 3 Voltage		Sont3 Gerilim			Sont3 Gerilim
130	32			15			Shunt 3 Current			Shunt 3 Current		Sont3 Akim			Sont3 Akim
131	32			15			Shunt 4 Voltage			Shunt 4 Voltage		Sont4 Gerilim			Sont4 Gerilim
132	32			15			Shunt 4 Current			Shunt 4 Current		Sont4 Akim			Sont4 Akim
133	32			15			Shunt 5 Voltage			Shunt 5 Voltage		Sont5 Gerilim			Sont5 Gerilim
134	32			15			Shunt 5 Current			Shunt 5 Current		Sont5 Akim			Sont5 Akim
#
170	32			15			High Current 1			Hi Current 1		Yuksek Akim1			Yuksek Akim1
171	32			15			Very High Current 1		VHi Current 1	  	Asiri Yuksek Akim1		AsYuk Akim1
172	32			15			High Current 2			Hi Current 2		Yuksek Akim2			Yuksek Akim2
173	32			15			Very High Current 2		VHi Current 2		Asiri Yuksek Akim2		 Akim2
174	32			15			High Current 3			Hi Current 3		Yuksek Akim3			Yuksek Akim3
175	32			15			Very High Current 3		VHi Current 3		Asiri Yuksek Akim3		 Akim3
176	32			15			High Current 4			Hi Current 4		Yuksek Akim4			Yuksek Akim4
177	32			15			Very High Current 4		VHi Current 4		Asiri Yuksek Akim4		AsYuk Akim4
178	32			15			High Current 5			Hi Current 5		Yuksek Akim5			Yuksek Akim5
179	32			15			Very High Current 5		VHi Current 5		Asiri Yuksek Akim5		 Akim5
#
220	32			15			Current 1 High Limit			Curr 1 Hi Lim		Yuksek Akim 1 Limit			Yuk Akim1 Lim	
221	32			15			Current 1 Very High Limit		Curr 1 VHi Lim		Asiri Yuksek Akim 1 Limit		AsYuk Akim1 Lim
222	32			15			Current 2 High Limit			Curr 2 Hi Lim		Yuksek Akim 2 Limit			Yuk Akim2 Lim
223	32			15			Current 2 Very High Limit		Curr 2 VHi Lim		Asiri Yuksek Akim 2 Limit		AsYuk Akim2 Lim
224	32			15			Current 3 High Limit			Curr 3 Hi Lim		Yuksek Akim 3 Limit			Yuk Akim3 Lim
225	32			15			Current 3 Very High Limit		Curr 3 VHi Lim		Asiri Yuksek Akim 3 Limit		AsYuk Akim3 Lim
226	32			15			Current 4 High Limit			Curr 4 Hi Lim		Yuksek Akim 4 Limit			Yuk Akim4 Lim
227	32			15			Current 4 Very High Limit		Curr 4 VHi Lim		Asiri Yuksek Akim 4 Limit		AsYuk Akim4 Lim
228	32			15			Current 5 High Limit			Curr 5 Hi Lim		Yuksek Akim 5 Limit			Yuk Akim5 Lim
229	32			15			Current 5 Very High Limit		Curr 5 VHi Lim		Asiri Yuksek Akim 5 Limit		AsYuk Akim5 Lim
#
270	32			15			Current 1 Breaker Size		Curr 1 Brk Size		Akim Kesici1 Ayar			Akim Kes1 Ayar
271	32			15			Current 2 Breaker Size		Curr 2 Brk Size		Akim Kesici2 Ayar			Akim Kes2 Ayar
272	32			15			Current 3 Breaker Size		Curr 3 Brk Size		Akim Kesici3 Ayar			Akim Kes3 Ayar
273	32			15			Current 4 Breaker Size		Curr 4 Brk Size		Akim Kesici4 Ayar			Akim Kes4 Ayar
274	32			15			Current 5 Breaker Size		Curr 5 Brk Size		Akim Kesici5 Ayar			Akim Kes5 Ayar

281	32			15			Shunt Size Switch			Shunt Size Switch		Sont Ayar Degeri		Sont Ayar
#282	32			15			Shunt 5 Coefficient Display		Shunt5 Display		Sont 5 Devrede			Sont 5 Devrede
283	32			15			Shunt 1 Size Conflicting		Sh 1 Conflict		Sont 1 Konfig			Sont 1 Konfig
284	32			15			Shunt 2 Size Conflicting		Sh 2 Conflict		Sont 2 Konfig			Sont 2 Konfig
285	32			15			Shunt 3 Size Conflicting		Sh 3 Conflict		Sont 3 Konfig			Sont 3 Konfig
286	32			15			Shunt 4 Size Conflicting		Sh 4 Conflict		Sont 4 Konfig			Sont 4 Konfig
287	32			15			Shunt 5 Size Conflicting		Sh 5 Conflict		Sont 5 Konfig			Sont 5 Konfig

290	32			15			By Software			By Software		Yazilim Tarafindan		Yazilim
291	32			15			By Dip Switch			By Dip Switch		Dip Switch			Dip Switch
292	32			15			No Supported			No Supported		Desteklenen Yok			Desteklenen Yok
293	32			15			Not Used				Not Used			Kullanılmamış			Kullanılmamış
294	32			15			General				General				Genel			Genel
295	32			15			Load						Load		Yuk			Yuk
296	32			15			Battery					Battery			Aku			Aku
297	32			15			Shunt1 Set As			Shunt1SetAs			Sont1 Ayar		Sont1 Ayar
298	32			15			Shunt2 Set As			Shunt2SetAs			Sont2 Ayar		Sont2 Ayar
299	32			15			Shunt3 Set As			Shunt3SetAs			Sont3 Ayar		Sont3 Ayar
300	32			15			Shunt4 Set As			Shunt4SetAs			Sont4 Ayar		Sont4 Ayar
301	32			15			Shunt5 Set As			Shunt5SetAs			Sont5 Ayar		Sont5 Ayar
302	32			15			Shunt1 Reading		Shunt1Reading		Sont1 Okuma				Sont1 Okuma
303	32			15			Shunt2 Reading		Shunt2Reading		Sont2 Okuma				Sont2 Okuma
304	32			15			Shunt3 Reading		Shunt3Reading		Sont3 Okuma				Sont3 Okuma
305	32			15			Shunt4 Reading		Shunt4Reading		Sont4 Okuma				Sont4 Okuma
306	32			15			Shunt5 Reading		Shunt5Reading		Sont5 Okuma				Sont5 Okuma
307	32			15			Load1 Alarm Flag			Load1 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
308	32			15			Load2 Alarm Flag			Load2 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
309	32			15			Load3 Alarm Flag			Load3 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
310	32			15			Load4 Alarm Flag			Load4 Alarm Flag	Load4 Alarm Flag			Load4 Alarm Flag
311	32			15			Load5 Alarm Flag			Load5 Alarm Flag	Load5 Alarm Flag			Load5 Alarm Flag
500		32			15			Current1 High 1 Curr			Curr1 Hi1Cur		Current1 High 1 Curr		Curr1 Hi1Cur	
501		32			15			Current1 High 2 Curr			Curr1 Hi2Cur		Current1 High 2 Curr		Curr1 Hi2Cur	
502		32			15			Current2 High 1 Curr			Curr2 Hi1Cur		Current2 High 1 Curr		Curr2 Hi1Cur	
503		32			15			Current2 High 2 Curr			Curr2 Hi2Cur		Current2 High 2 Curr		Curr2 Hi2Cur	
504		32			15			Current3 High 1 Curr			Curr3 Hi1Cur		Current3 High 1 Curr		Curr3 Hi1Cur	
505		32			15			Current3 High 2 Curr			Curr3 Hi2Cur		Current3 High 2 Curr		Curr3 Hi2Cur	
506		32			15			Current4 High 1 Curr			Curr4 Hi1Cur		Current4 High 1 Curr		Curr4 Hi1Cur	
507		32			15			Current4 High 2 Curr			Curr4 Hi2Cur		Current4 High 2 Curr		Curr4 Hi2Cur	
508		32			15			Current5 High 1 Curr			Curr5 Hi1Cur		Current5 High 1 Curr		Curr5 Hi1Cur	
509		32			15			Current5 High 2 Curr			Curr5 Hi2Cur		Current5 High 2 Curr		Curr5 Hi2Cur
550		32			15			Source							Source				Source						Source		
