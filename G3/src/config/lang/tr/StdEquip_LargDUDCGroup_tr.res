#																			
#  Locale language support: Turkish																			
#																			
																			
#																			
# RES_ID: Resource ID 																			
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 																			
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)																			
# FULL_IN_EN: Full English name																			
# ABBR_IN_EN: Abbreviated English name																			
# FULL_IN_LOCALE: Full name in locale language																			
# ABBR_IN_LOCALE: Abbreviated locale name																			
#																			
# Add by WJ For Three Language Support            																			
# FULL_IN_LOCALE2: Full name in locale2 language  																			
# ABBR_IN_LOCALE2: Abbreviated locale2 name       																			
																			
[LOCALE_LANGUAGE]																			
tr																			
																			
																			
																			
[RES_INFO]																			
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE							
1		32			15			LVD1				LVD1			LVD1				LVD1
2		32			15			LVD2				LVD2			LVD2				LVD2
3		32			15			LVD3				LVD3			LVD3				LVD3
4		32			15			DC Distribution Number		DC Distr Num		DC Dagitim No			DC Dagitim No					
5		32			15			LargDU DC Distribution Group	Large DC Group		GenisDU DC Dagitim Grup 	Genis DC Grup							
6		32			15			High Temperature 1 Limit	High Temp1		Yuksek Sicaklik Limit1		Yuk Sic 1							
7		32			15			High Temperature 2 Limit	High Temp2		Yuksek Sicaklik Limit2		Yuk Sic 2							
8		32			15			High Temperature 3 Limit	High Temp3		Yuksek Sicaklik Limit3		Yuk Sic 3							
9		32			15			Low Temperature 1 Limit		Low Temp1		Dusuk Sicaklik Limit1		Dusuk Sic 1						
10		32			15			Low Temperature 2 Limit		Low Temp2		Dusuk Sicaklik Limit2		Dusuk Sic 2						
11		32			15			Low Temperature 3 Limit		Low Temp3		Dusuk Sicaklik Limit3		Dusuk Sic 3						
12		32			15			LVD1 Voltage			LVD1 Voltage		LVD1 Gerilim			LVD1 Gerilim			
13		32			15			LVD2 Voltage			LVD2 Voltage		LVD2 Gerilim			LVD2 Gerilim			
14		32			15			LVD3 Voltage			LVD3 Voltage		LVD3 Gerilim			LVD3 Gerilim			
15		32			15			Overvoltage			Overvoltage		Yuksek Gerilim			Yuksek Gerilim			
16		32			15			Undervoltage			Undervoltage		Dusuk Gerilim			Dusuk Gerilim			
17		32			15			On				On			Acik				Acik	
18		32			15			Off				Off			Kapali				Kapali
19		32			15			On				On			Acik				Acik	
20		32			15			Off				Off			Kapali				Kapali
21		32			15			On				On			Acik				Acik	
22		32			15			Off				Off			Kapali				Kapali
23		32			15			Total Load Current		Total Load		Toplam Yuk Akimi		Toplam Yuk					
24		32			15			Total DCD Current		Total DCD Curr		Toplam DC Akimi			Toplam DC Akim					
25		32			15			DCDistribution Average Voltage	DCD Average Volt	DCDistribution Average Voltage	DCD Average Volt								
26		32			15			LVD1 Enabled			LVD1 Enabled		LVD1 Aktif			LVD1 Aktif				
27		32			15			LVD1 Mode			LVD1 Mode		LVD1 Mod			LVD1 Mod			
28		32			15			LVD1 Time			LVD1 Time		LVD1 Zaman			LVD1 Zaman			
29		32			15			LVD1 Reconnect Voltage		LVD1 ReconVolt		LDV1 Baglanma Gerilimi		LDV1 Bag Ger					
30		32			15			LVD1 Reconnect Delay		LVD1 ReconDelay		LDV1 Baglanma Gecikmesi		LDV1 Bag Gecikme					
31		32			15			LVD1 Dependency			LVD1 Depend		LVD1 Bagimli			LVD1 Bagimli				
32		32			15			LVD2 Enabled			LVD2 Enabled		LVD2 Aktif			LVD2 Aktif				
33		32			15			LVD2 Mode			LVD2 Mode		LVD2 Mod			LVD2 Mod			
34		32			15			LVD2 Time			LVD2 Time		LVD2 Zaman			LVD2 Zaman			
35		32			15			LVD2 Reconnect Voltage		LVD2 ReconVolt		LDV2 Baglanma Gerilimi		LDV2 Bag Ger					
36		32			15			LVD2 Reconnect Delay		LVD2 ReconDelay		LDV2 Baglanma Gecikmesi		LDV2 Bag Gecikme					
37		32			15			LVD2 Dependency			LVD2 Depend		LVD2 Bagimli			LVD2 Bagimli				
38		32			15			LVD3 Enabled			LVD3 Enabled		LVD3 Aktif			LVD3 Aktif				
39		32			15			LVD3 Mode			LVD3 Mode		LVD3 Mod			LVD3 Mod			
40		32			15			LVD3 Time			LVD3 Time		LVD3 Zaman			LVD3 Zaman			
41		32			15			LVD3 Reconnect Voltage		LVD3 ReconVolt		LDV3 Baglanma Gerilimi		LDV3 Bag Ger					
42		32			15			LVD3 Reconnect Delay		LVD3 ReconDelay		LDV3 Baglanma Gecikmesi		LDV3 Bag Gecikme					
43		32			15			LVD3 Dependency			LVD3 Depend		LVD3 Bagli			LVD3 Bagli				
44		32			15			Disabled			Disabled		Pasif				Pasif			
45		32			15			Enabled				Enabled			Aktif 				Aktif 	
46		32			15			Voltage				Voltage			Gerilim				Gerilim
47		32			15			Time				Time			Zaman				Zaman
48		32			15			None				None			Hicbiri				Hicbiri
49		32			15			LVD1				LVD1			LVD1				LVD1
50		32			15			LVD2				LVD2			LVD2				LVD2
51		32			15			LVD3				LVD3			LVD3				LVD3
52		32			15			Number of LVDs			No of LVDs		LVD Sayisi			LVD Sayisi			
53		32			15			0				0			0				0
54		32			15			1				1			1				1
55		32			15			2				2			2				2
56		32			15			3				3			3				3
57		32			15			Existence State			Existence State		Mevcut durum			Mevcut durum			
58		32			15			Existent			Existent		Mevcut  			Mevcut  			
59		32			15			Non-Existent			Non-Existent		Mevcut Degil			Mevcut Degil			
60		32			15			Battery Overvoltage		Batt OverVolt		Aku Yuksek gerilim		Aku Yuksek Ger					
61		32			15			Battery Undervoltage		Batt UnderVolt		Aku Dusuk gerilim		Aku Dusuk Ger					
