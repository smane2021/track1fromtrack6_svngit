﻿#
# Locale language support: Turkish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Inverter Group				Invt Group			Evirici modül grubu				Evirici modül grubu
2		32			15			Total Current				Tot Invt Curr		Modülün toplam akımı				Modülün toplam akımı

4		32			15			Inverter Capacity Used			Sys Cap Used		Modül kapasitesi				Sistem kapasitesi
5		32			15			Maximum Capacity Used			Max Cap Used		Maksimum kapasite				Maksimum kapasite
6		32			15			Minimum Capacity Used			Min Cap Used		Minimum kullanım kapasitesi				Min Cap Used
7		32			15			Total Inverters Communicating	Num Invts Comm			Normal iletişim modülü sayısı				Num Invts Comm
8		32			15			Valid Inverters					Valid Inverters			Etkili modül sayısı					Etkili modülsayısı
9		32			15			Number of Inverters				Num of Invts			İnverter modülü sayısı					Num of Invts
10		32			15			Number of Phase1				Number of Phase1		L1 Sayısı						L1 Sayısı
11		32			15			Number of Phase2				Number of Phase2		L2 numarası						L2 numarası
12		32			15			Number of Phase3				Number of Phase3		L3 numarası						L3 numarası
13		32			15			Current of Phase1				Current of Phase1		L1 akımı						L1 akımı
14		32			15			Current of Phase2				Current of Phase2		L2 akımı						L2 akımı
15		32			15			Current of Phase3				Current of Phase3		L3 akımı						L3 akımı
16		32			15			Power_kW of Phase1				Power_kW of Phase1		L1 güç Kw					L1 güç Kw
17		32			15			Power_kW of Phase2				Power_kW of Phase2		L2 güç Kw					L2 güç Kw
18		32			15			Power_kW of Phase3				Power_kW of Phase3		L3 güç Kw					L3 güç Kw
19		32			15			Power_kVA of Phase1				Power_kVA of Phase1		L1 güç KVA					L1 güç KVA
20		32			15			Power_kVA of Phase2				Power_kVA of Phase2		L2 güç KVA					L2 güç KVA
21		32			15			Power_kVA of Phase3				Power_kVA of Phase3		L3 verimlilik KVA					L3 verimlilik KVA
22		32			15			Rated Current					Rated Current			Anma akımı					Anma akımı
23		32			15			Input Total Energy				Input Energy			Toplam gücü girin					Toplam gücü girin
24		32			15			Output Total Energy				Output Energy			Toplam çıkış gücü					Toplam çıkış gücü	
25		32			15			Inverter Sys Set ASYNC			Sys Set ASYNC			Sistem ayarları zaman uyumsuz				Sys Set ASYNC
26		32			15			Inverter AC Phase Abnormal		ACPhaseAbno				Anormal AC fazı				Anormal AC fazı
27		32			15			Inverter REPO					REPO					REPO						REPO
28		32			15			Inverter Output Freq ASYNC		OutputFreqASYNC			Çıkış frekansı eşzamansız				OutputFreqASYNC	
29		32			15			Output On/Off			Output On/Off		Modül çıkış anahtarı				Modül çıkış anahtarı
30		32			15			Inverters LED Control			Invt LED Ctrl			Modül LED ışık kontrolü				Invt LED Ctrl
31		32			15			Fan Speed				Fan Speed			Fan çalışma hızı				Fan çalışma hızı
32		32			15			Invt Mode AC/DC					Invt Mode AC/DC			Invt Mode AC/DC				Invt Mode AC/DC
33		32			15			Output Voltage Level			Output Voltage Level	Çıkış voltaj seviyesi				Çıkış voltaj seviyesi
34		32			15			Output Frequency					Output Freq			Çıkış frekansı				Çıkış Frekansı
35		32			15			Source Ratio					Source Ratio			Giriş ölçeği					Giriş ölçeği
36		32			15			Normal					Normal			normal					normal
37		32			15			Fail					Fail			arıza				arıza
38		32			15			Switch Off All				Switch Off All		Tüm modülleri kapat				Tüm modülleri kapat
39		32			15			Switch On All				Switch On All		Tüm modülleri aç				Tüm modülleri aç
42		32			15			All Flashing				All Flashing		Tüm ışıklar yanıp sönüyor				All Flashing
43		32			15			Stop Flashing				Stop Flashing		Tüm ışıklar yanıp sönmüyor				Stop Flashing
44		32			15			Full Speed				Full Speed		son sürat				son sürat
45		32			15			Automatic Speed				Auto Speed		Otomatik hız ayarı				Otomatik hız ayarı
54		32			15			Disabled				Disabled		Hayır					Hayır
55		32			15			Enabled					Enabled			Evet					Evet
56		32			15			100V					100V			100V				100V
57		32			15			110V					110V			110V				110V
58		32			15			115V					115V			115V				115V
59		32			15			120V					120V			120V				120V
60		32			15			125V					125V			125V				125V
61		32			15			200V					200V			200V				200V
62		32			15			208V					208V			208V				208V
63		32			15			220V					220V			220V				220V
64		32			15			230V					230V			230V				230V
65		32			15			240V					240V			240V				240V
66		32			15			Auto					Auto			Auto				Auto
67		32			15			50Hz					50Hz			50Hz				50Hz
68		32			15			60Hz					60Hz			60Hz				60Hz
69		32			15			Input DC Current		Input DC Current		Giriş DC akımı				Giriş DC akımı
70		32			15			Input AC Current		Input AC Current		Giriş AC akımı				Giriş AC akımı
71		32			15			Inverter Work Status	InvtWorkStatus			İnvertör çalışma durumu				InvtWorkStatus
72		32			15			Off					Off						kapat							kapat
73		32			15			No					No						Hayır							Hayır
74		32			15			Yes					Yes						Evet							Evet
75		32			15			Part On				Part On					Kısmen açık						Kısmen açık
76		32			15			All On				All On					Tamamen açık						Tamamen açık

77		32			15			DC Low Voltage Off		DCLowVoltOff		DC Düşük Voltaj Kapalı			DCLowVoltOff
78		32			15			DC Low Voltage On		DCLowVoltOn		DC Düşük Voltaj Açık			DCLowVoltOn
79		32			15			DC High Voltage Off	DCHiVoltOff		DC Yüksek Voltaj Kapalı			DCHiVoltOff
80		32			15			DC High Voltage On	DCHighVoltOn		DC Yüksek Gerilim Açık			DCHighVoltOn
81		32			15			Last Inverters Quantity		Last Invts Qty		Orijinal modül sayısı				Last Invts Qty
82		32			15			Inverter Lost				Inverter Lost		Modül eksik				Modül eksik
83		32			15			Inverter Lost				Inverter Lost		Modül eksik			Modül eksik
84		32			15			Clear Inverter Lost Alarm		Clear Invt Lost		Net modül eksik alarmı			Clear Invt Lost
86		32			15			Confirm Inverter ID/Phase			Confirm ID/PH			Modül konumunu/Fazını onaylayın			Konumu/PH onayla
92		32			15			E-Stop Function				E-Stop Function		E-Stop işlevi				E-Stop işlevi
93		32			15			AC Phases				AC Phases		AC faz numarası				AC faz numarası
94		32			15			Single Phase				Single Phase		Basit					Basit
95		32			15			Three Phases				Three Phases		Üç faz					Üç faz
101		32			15			Sequence Start Interval			Start Interval		Sıralı önyükleme aralığı				Start Interval
104		64			15			All Inverters Communication Failure		AllInvtCommFail		Tüm modül iletişimi kesildi			AllInvtCommFail	
113		32			15			Invt Info Change (M/S Internal Use)	InvtInfo Change		İnverter bilgi değişiklikleri			İnv bilgisi değişti
119		32			15			Clear Inverter Comm Fail Alarm		ClrInvtCommFail		İnverter iletişiminde net kesinti			ClrInvtCommFail
126		32			15			None					None			Hayır					Hayır
143		32			15			Existence State				Existence State		var mı				var mı
144		32			15			Existent				Existent		var olmak					var olmak
145		32			15			Not Existent				Not Existent		mevcut değil					mevcut değil
146		32			15			Total Output Power			Output Power		Toplam çıkış gücü				Toplam çıkış gücü
147		32			15			Total Slave Rated Current		Total RatedCurr		Toplam anma akımı				Toplam anma akımı
148		32			15			Reset Inverter IDs			Reset Invt IDs		Modül konum numarasını temizleme			Reset Invt IDs
149		32			15			HVSD Voltage Difference (24V)		HVSD Volt Diff		HVSD fark basıncı (24V)				HVSD basınç farkı
150		32			15			Normal Update				Normal Update		Sıradan yükseltme				Sıradan yükseltme
151		32			15			Force Update				Force Update		Zorla yükseltme				Zorla yükseltme
152		32			15			Update OK Number			Update OK Num		Başarılı yükseltme sayısı				Update OK Num
153		32			15			Update State				Update State		Yükseltme durumu				Yükseltme durumu
154		32			15			Updating				Updating		yükseltme sırasında					yükseltme sırasında
155		32			15			Normal Successful			Normal Success		Sıradan yükseltme başarısı				Normal Success
156		32			15			Normal Failed				Normal Fail		Normal yükseltme başarısız				Normal Fail
157		32			15			Force Successful			Force Success		Zorla yükseltme başarısı				Force Success
158		32			15			Force Failed				Force Fail		Zorla yükseltme başarısız				Force Success
159		32			15			Communication Time-Out			Comm Time-Out		İletişim zaman aşımı				İletişim zaman aşımı
160		32			15			Open File Failed			Open File Fail		dosya açılamıyor				dosya açılamıyor
161		32			15			AC mode						AC mode					İletişim modu				İletişim modu
162		32			15			DC mode						DC mode					DC modu				DC modu

#163		32			15			Safety mode					Safety mode				Güvenli mod				Güvenli mod
#164		32			15			Idle  mode					Idle  mode				Bekleme modu				Bekleme modu
165		32			15			Max used capacity			Max used capacity		Maksimum kapasite				Maksimum kapasite
166		32			15			Min used capacity			Min used capacity		Minimum kapasite				Minimum kapasite
167		32			15			Average Voltage				Invt Voltage			Ortalama voltaj				Ortalama voltaj
168		32			15			Invt Redundancy Number		Invt Redu Num			İnvertör yedekliliği sayısı			Invt Redu Num
169		32			15			Inverter High Load			Inverter High Load		Yüksek inverter yükü				Yüksek inverter yükü
170		32			15			Rated Power					Rated Power				Anma gücü				Anma gücü

171		32			15			Clear Fault					Clear Fault				Alarmı temizle				Alarmı temizle
172		32			15			Reset Energy				Reset Energy			Gücü sıfırla				Gücü sıfırla

173		32			15			Syncronization Phase Failure			SynErrPhaType			Faz yapılandırması senkronize değil			SynErrPhaType
174		32			15			Syncronization Voltage Failure			SynErrVoltType			Voltaj seviyeleri senkronize değil			SynErrVoltType
175		32			15			Syncronization Frequency Failure			SynErrFreqLvl			Frekans seviyeleri senkronize değil			SynErrFreqLvl
176		32			15			Syncronization Mode Failure			SynErrWorkMode			Çalışma modu senkronize değil		SynErrWorkMode
177		32			15			Syn Err Low Volt TR			SynErrLVoltTR			Düşük voltaj aktarımı senkronize değil			SynErrLVoltTR
178		32			15			Syn Err Low Volt CB			SynErrLVoltCB			Düşük basınç geri dönüşü senkronize değil			SynErrLVoltCB
179		32			15			Syn Err High Volt TR		SynErrHVoltTR			Yüksek voltaj aktarımı senkronize değil			SynErrHVoltTR
180		32			15			Syn Err High Volt CB		SynErrHVoltCB			Yüksek basınç regresyonu senkronize değil			SynErrHVoltCB
181		32			15			Set Para To Invt			SetParaToInvt			İnvertör senkronizasyon parametreleri			SetParaToInvt
182		32			15			Get Para From Invt			GetParaFromInvt			Parametreleri inverterden okuma		GetParaFromInvt
183		32			15			Power Used					Power Used				Güç yüzdesi				Güç yüzdesi
184		32			15			Input AC Voltage			Input AC Volt			Giriş AC voltajı 			Giriş AC voltajı	
#185     32          15          Input AC Voltage Phase A       InpVoltPhA           İnvertör giriş faz voltajı A      InpVoltPhA
185		32			15			Input AC Phase A			Input AC VolPhA			Giriş AC voltajı 			Giriş AC voltajı	
186		32			15			Input AC Phase B			Input AC VolPhB			Giriş AC voltajı 			Giriş AC voltajı
187		32			15			Input AC Phase C			Input AC VolPhC			Giriş AC voltajı 			Giriş AC voltajı		
188		32			15			Total output voltage			Total output V			Toplam çıkış V			Toplam çıkış V
189		32			15			Power in kW					PowerkW				KW güç				KW güç
190		32			15			Power in kVA					PowerkVA				Güç kVA				Güç kVA
197		32			15			Maximum Power Capacity					Max Power Cap				Maksimum Güç Kapasitesi				MaksGüçKapa