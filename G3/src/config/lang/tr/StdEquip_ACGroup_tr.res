#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
tr


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN	FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			AC Group			AC Group	AC Grup				AC Grup
2		32			15			Total Phase A Current		Phase A Curr	R Fazi Toplam Akim		R Fazi Top.Akim
3		32			15			Total Phase B Current		Phase B Curr	S Fazi Toplam Akim		S Fazi Top.Akim
4		32			15			Total Phase C Current		Phase C Curr	T Fazi Toplam Akim		T Fazi Top.Akim	
5		32			15			Total Phase A Power		Phase A Power	R Fazi Toplam Guc		R Fazi Top. Guc
6		32			15			Total Phase B Power		Phase B Power	S Fazi Toplam Guc		S Fazi Top. Guc
7		32			15			Total Phase C Power		Phase C Power	T Fazi Toplam Guc		T Fazi Top. Guc
8		32			15			AC Unit Type			AC Unit Type	AC Birim Tipi			AC Birim Tipi
9		32			15			AC Board			AC Board	Kurulu AC			Kurulu AC
10		32			15			No AC Board			No ACBoard	AC Kart Yok 			Kurulu AC Yok
11		32			15			SM-AC				SM-AC		SM-AC				SM-AC
12		32			15			Mains Failure			Mains Failure	Sebeke Arizasi			Sebeke Arizasi
13		32			15			Existence State			Existence State	Mevcut Durum			Mevcut Durum
14		32			15			Existent			Existent	Mevcut				Mevcut
15		32			15			Non-Existent			Non-Existent	Mevcut Degil			Mevcut Degil
16		32			15			Total Input Current		Input Current	Toplam Giris Akim		Giris Akim
