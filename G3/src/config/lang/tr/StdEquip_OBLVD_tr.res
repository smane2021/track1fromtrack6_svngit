#																			
#  Locale language support: Turkish																			
#																			
																			
#																			
# RES_ID: Resource ID 																			
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 																			
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)																			
# FULL_IN_EN: Full English name																			
# ABBR_IN_EN: Abbreviated English name																			
# FULL_IN_LOCALE: Full name in locale language																			
# ABBR_IN_LOCALE: Abbreviated locale name																			
#																			
[LOCALE_LANGUAGE]																			
tr																			
																			
																			
[RES_INFO]																			
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_iN_LOCALE							
1		32			15			LVD Unit			LVD			LVD				LVD	
11		32			15			Connected			Connected		Bagli				Bagli		
12		32			15			Disconnected			Disconnected		Baglantisiz			Baglantisiz			
13		32			15			No				No			Hayir				Hayir
14		32			15			Yes				Yes			Evet				Evet
21		32			15			LVD1 Status			LVD1 Status		LVD1Durum			LVD1Durum			
22		32			15			LVD2 Status			LVD2 Status		LVD2 Durum			LVD2 Durum			
23		32			15			LVD1 Failure			LVD1 Failure		LVD1 Hata			LVD1 Hata			
24		32			15			LVD2 Failure			LVD2 Failure		LVD2 Hata			LVD2 Hata			
25		32			15			Communication Failure		Comm Failure		Haberlesme Hatasi		Hab. Hata					
26		32			15			State				State			Durum				Durum
27		32			15			LVD1 Control			LVD1 Control		Kontrol LVD1			Kontrol LVD1			
28		32			15			LVD2 Control			LVD2 Control		Kontrol LVD2			Kontrol LVD2			
29		32			15			LVD Disabled				LVD Disabled		LVD Disabled				LVD Disabled
31		32			15			LVD1				LVD1			LVD1				LVD1
32		32			15			LVD1 Mode			LVD1 Mode		LVD1Mod				LVD1Mod		
33		32			15			LVD1 Voltage			LVD1 Voltage		LVD1Gerilim			LVD1Gerilim			
34		32			15			LVD1 Reconnect Voltage		LVD1 ReconnVolt		Yeniden Baglanti Gerilim LVD1		Yen.Bag.GerilimLVD1					
35		32			15			LVD1 Reconnect Delay		LVD1 ReconDelay		Yeniden Baglanti Gecikme LVD1	Yen.Bag.Gecikme LVD1						
36		32			15			LVD1 Time			LVD1 Time		LVD1 Zaman			LVD1Zaman			
37		32			15			LVD1 Dependency			LVD1 Dependency		LVD1 Bagimli			LVD1Bagimli			
41		32			15			LVD2				LVD2			LVD2				LVD2
42		32			15			LVD2 Mode			LVD2 Mode		LVD2 Mod			LVD2 Mod			
43		32			15			LVD2 Voltage			LVD2 Voltage		LVD2 Gerilim			LVD2 Gerilim			
44		32			15			LVR2 Reconnect Voltage		LVR2 ReconnVolt		Yeniden Baglanti Gerilim LVD2		Yen.Bag.GerilimLVD2					
45		32			15			LVD2 Reconnect Delay  		LVD2 ReconDelay		Yeniden Baglanti Gecikme LVD2	Yen.Bag.Gecikme LVD2						
46		32			15			LVD2 Time			LVD2 Time		LVD2 Zaman			LVD2 Zaman			
47		32			15			LVD2 Dependency			LVD2 Dependency		LVD2 Bagimli			LVD2 Bagimli				
51		32			15			Disabled			Disabled		Devre Disi			Devre Disi			
52		32			15			Enabled				Enabled			Etkinlestir			Etkinlestir	 
53		32			15			By Voltage			By Voltage		Gerilim				Gerilim		
54		32			15			By Time				By Time			Zaman				Zaman
55		32			15			None				None			Hicbiri				Hicbiri
56		32			15			LVD1				LVD1			LVD1				LVD1
57		32			15			LVD2				LVD2			LVD2				LVD2
103		32			15			High Temp Disconnect 1		HTD1			Yuksek Sicaklik Baglantisiz 1	Yuk Sic Bag 1					
104		32			15			High Temp Disconnect 2		HTD2			Yuksek Sicaklik Baglantisiz 2	Yuk Sic Bag 2					
105		32			15			Battery LVD			Batt LVD		Aku LVD				Aku LVD 		
106		32			15			No Battery			No Batt			Aku Yok				Aku Yok	
107		32			15			LVD1				LVD1			LVD1				LVD1
108		32			15			LVD2				LVD2			LVD2				LVD2
109		32			15			Battery Always On		BattAlwaysOn		Aku Surekli Acik		Aku Surekli Acik 					
110		32			15			LVD Contactor Type		LVD Type		LVD Tipi			LVD Tipi				
111		32			15			Bistable			Bistable		Iki Konum			Iki Konum			
112		32			15			Monostable			Monostable		Tek Konum			Tek Konum			
113		32			15			Monostable with Sampler		Mono with Samp		Tek Ornek			Tek Ornek				
116		32			15			LVD1 Disconnected		LVD1 Disconnect		LVD1 Baglantisiz		LVD1 Baglantisiz					
117		32			15			LVD2 Disconnected		LVD2 Disconnect		LVD2 Baglantisiz		LVD2 Baglantisiz					
118		32			15			LVD1 Monostable with Sampler	LVD1 Mono Sample		LVD1 Tek Ornek			LVD1Tek Ornek						
119		32			15			LVD2 Monostable with Sampler	LVD2 Mono Sample		LVD2 Tek Ornek			LVD2 Tek Ornek						
125		32			15			State				State			Durum				Durum
126		32			15			LVD1 Voltage(24V)		LVD1 Voltage		Gerilim LVD1(24V)		Gerilim LVD1					
127		32			15			LVD1 Reconnect Voltage(24V)	LVD1 ReconnVolt		Yeniden Baglanti Gerilim LVD1(24V)	Yen.Bag.GerilimLVD1							
128		32			15			LVD2 Voltage(24V)		LVD2 Voltage		Gerilim LVD2(24V)			Gerilim LVD2				
129		32			15			LVD2 Reconnect Voltage(24V)	LVD2 ReconnVolt		Yeniden Baglanti Gerilim LVD2(24V)	Yen.Bag.GerilimLVD2							
130		32			15			LVD 3				LVD 3			LVD 3				LVD 3
200		32			15			Connect				Connect			Bagli				Bagli
201		32			15			Disconnect			Disconnect		Baglantisiz			Baglantisiz			
