#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
tr



[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			LVD Group				LVD Group		LVD Grup			LVD Grup
2		32			15			Battery LVD				Batt LVD		Aku LVD				Aku LVD
3		32			15			None					None			Hayir				Hayir
4		32			15			LVD1					LVD1			LVD1				LVD1
5		32			15			LVD2					LVD2			LVD2				LVD2
6		32			15			LVD3					LVD3			LVD3				LVD3
7		32			15			LVD Needs AC Fail			LVD NeedACFail		LVD AC Ariza Ihtiya?lari	LVD AC ArizaIht
8		32			15			Disable					Disable			Devre Disi			Devre Disi
9		32			15			Enable					Enable			Etkinlestir			Etkinlestir
10		32			15			Number of LVDs				Num LVDs		LVD Numaralari			LVD Numaralari
11		32			15			0					0			0				0
12		32			15			1					1			1				1
13		32			15			2					2			2				2
14		32			15			3					3			3				3
15		32			15			HTD Point				HTD Point		HTD Noktasi			HTD Noktasi
16		32			15			HTD Reconnect Point			HTD Recon Pnt		HTD Yeniden Baglanti Noktasi	HTD Y.Bagln.Nok
17		32			15			HTD Temperature Sensor			HTD Temp Sens		HTD Sicaklik Sensoru		HTD Sic.Sensor
18		32			15			Ambient Temperature			Ambient Temp		Ortam Sicakligi			Ortam Sicakligi
19		32			15			Battery Temperature			Battery Temp		Aku Sicakligi			Aku Sicakligi
20		32			15			Temperature 1				Temp1			Sicaklik 1			Sicaklik 1
21		32			15			Temperature 2				Temp2			Sicaklik 2			Sicaklik 2
22		32			15			Temperature 3				Temp3			Sicaklik 3			Sicaklik 3
23		32			15			Temperature 4				Temp4			Sicaklik 4			Sicaklik 4
24		32			15			Temperature 5				Temp5			Sicaklik 5			Sicaklik 5
25		32			15			Temperature 6				Temp6			Sicaklik 6			Sicaklik 6
26		32			15			Temperature 7				Temp7			Sicaklik 7			Sicaklik 7
27		32			15			Existence State				Exist State		Varlik Durumu			Varlik Durumu
28		32			15			Existent				Existent		Mevcut				Mevcut
29		32			15			Non-Existent				Non-Existent		Mevcut Degil			Mevcut Degil
31		32			15			LVD1 Enabled			LVD1 Enabled		LVD1 Etkin			LVD1 Etkin
32		32			15			LVD1 Mode			LVD1 Mode		LVD1 Modu			LVD1 Modu
33		32			15			LVD1 Voltage			LVD1 Voltage		LVD1 Gerilimi			LVD1 Gerilimi
34		32			15			LVD1 Reconnect Voltage		LVD1 ReconnVolt		LVD1 Yeniden Baglanti Gerilimi	LVD1 Y.Bagl.Ger
35		32			15			LVD1 Reconnect Delay  		LVD1 ReconDelay		LVD1 Yeniden Baglanti Gecikmesi	LVD1 Y.Bagl.Gec
36		32			15			LVD1 Time			LVD1 Time		LVD1 Zamani			LVD1 Zamani
37		32			15			LVD1 Dependency			LVD1 Dependency		LVD1 Bagimliligi		LVD1 Bagimliligi
41		32			15			LVD2 Enabled			LVD2 Enabled		LVD2 Etkin			LVD2 Etkin
42		32			15			LVD2 Mode			LVD2 Mode		LVD2 Modu			LVD2 Modu
43		32			15			LVD2 Voltage			LVD2 Voltage		LVD2 Gerilimi			LVD2 Gerilimi
44		32			15			LVR2 Reconnect Voltage		LVR2 ReconnVolt		LVD2 Yeniden Baglanti Gerilimi	LVD2 Y.Bagl.Ger
45		32			15			LVD2 Reconnect Delay  		LVD2 ReconDelay		LVD2 Yeniden Baglanti Gecikmesi	LVD2 Y.Bagl.Gec
46		32			15			LVD2 Time			LVD2 Time		LVD2 Zamani			LVD2 Zamani
47		32			15			LVD2 Dependency			LVD2 Dependency		LVD2 Bagimliligi		LVD2 Bagimliligi
51		32			15			Disabled				Disabled		Devre Disi			Devre Disi
52		32			15			Enabled					Enabled			Etkin				Etkin	
53		32			15			Voltage					Voltage			Gerilim				Gerilim	
54		32			15			Time					Time			Zaman				Zaman
55		32			15			None					None			Hiçbiri			Hiçbiri
56	        32     			15             		LVD1					LVD1			LVD1				LVD1
57	        32     			15             		LVD2					LVD2			LVD2				LVD2
103	        32     			15             		HTD1 Enable				HTD1 Enable		HTD1 Etkinlestir		HTD1 Etkinlestir
104	        32     			15             		HTD2 Enable				HTD2 Enable		HTD2 Etkinlestir		HTD2 Etkinlestir
105	        32     			15             		Battery LVD				Batt LVD		Aku LVD				Aku LVD
106	        32     			15             		No Battery				No Batt			Aku Yok				Aku Yok
107	        32     			15             		LVD1					LVD1			LVD1				LVD1
108	        32     			15             		LVD2					LVD2			LVD2				LVD2
109		32			15			Battery Always On			BattAlwaysOn		Aku Her Zaman Açık	AkuHerzamanAçık
110		32			15			LVD Contactor Type			LVD Type		LVD Kontaktor Tipi		LVD Kontak.Tip
111		32			15			Bistable				Bistable		Yki Durumlu			Yki Durumlu
112		32			15			Monostable				Mono-stable		Tek Durumlu			Tek Durumlu
113		32			15			Monostable with Sample			Mono with Samp		Örnek Yle Tek DUrumlu		Örnek.Tek Durumlu
116		32			15			LVD1 Disconnected			LVD1 Disconnected	LVD1 Baglantisiz		LVD1Baglantisiz
117		32			15			LVD2 Disconnected			LVD2 Disconnected	LVD2 Baglantisiz		LVD2Baglantisiz
#125		32			15			State					State			Durum				Durum
126		32			15			LVD1 Voltage(24V)				LVD1 Voltage		LVD1 Gerilimi			LVD1 Gerilimi
127		32			15			LVD1 Reconnect Voltage(24V)			LVD1 ReconnVolt		LVD1 Yeniden Baglanti Gerilimi	LVD1 Y.Bagl.Ger
128		32			15			LVD2 Voltage(24V)			LVD2 Voltage		LVD2 Gerilimi (24V)		LVD2 Gerilimi
129		32			15			LVD2 Reconnect Voltage(24V)		LVD2 ReconnVolt		LVD2YenidenBaglantiGerilimi(24V)	LVD2 Y.Bagl.Ger
130		32			15			LVD1 Control				LVD1		LVD1 Kontrol			LVD2 Kontrol
131		32			15			LVD2 Control				LVD2		LVD2 Kontrol			LVD2 Kontrol
132		32			15			LVD1 Reconnect				LVD1 Reconnect		LVD1 Yeniden Baglanti		LVD1YenidenBagl
133		32			15			LVD2 Reconnect				LVD2 Reconnect		LVD2 Yeniden Baglanti		LVD2YenidenBagl
134		32			15			HTD Point				HTD Point		HTD Noktasi			HTD Noktasi
135		32			15			HTD Reconnect Point			HTD Recon Point		HTD Yeniden Baglanti Noktasi	HTD Y.Bagln.Nok
136		32			15			HTD Temperature Sensor			HTD Temp Sensor		HTD Sicaklik Sensoru		HTD Sic. Sensoru
137		32			15			Ambient					Ambient			Ortam				Ortam
138		32			15			Battery					Battery			Aku				Aku
139		32			15			LVD Synchronize				LVD Synchronize		LVD Senkronize 			LVD Senkronize 
140		32			15			Relay for LVD3			Relay for LVD3			LVD 3 icin Role		LVD 3 icin Role
141		32			15			Relay Output 1			Relay Output 1			Role Cikisi 1		Role Cikisi 1
142		32			15			Relay Output 2			Relay Output 2			Role Cikisi 2		Role Cikisi 2
143		32			15			Relay Output 3			Relay Output 3			Role Cikisi 3		Role Cikisi 3
144		32			15			Relay Output 4			Relay Output 4			Role Cikisi 4		Role Cikisi 4
145		32			15			Relay Output 5			Relay Output 5			Role Cikisi 5		Role Cikisi 5
146		32			15			Relay Output 6			Relay Output 6			Role Cikisi 6		Role Cikisi 6
147		32			15			Relay Output 7			Relay Output 7			Role Cikisi 7		Role Cikisi 7
148		32			15			Relay Output 8			Relay Output 8			Role Cikisi 8		Role Cikisi 8
149		32			15			Relay Output 14			Relay Output 14			Role Cikisi 14		Role Cikisi 14 
150		32			15			Relay Output 15			Relay Output 15			Role Cikisi 15		Role Cikisi 15
151		32			15			Relay Output 16			Relay Output 16			Role Cikisi 16		Role Cikisi 16
152		32			15			Relay Output 17			Relay Output 17			Role Cikisi 17		Role Cikisi 17
153		32			15			LVD 3 Enable			LVD 3 Enable			LVD 3 Etkin		LVD 3 Etkin	
154		32			15			LVD Threshold			LVD Threshold			LVD Güç eşiği eşik	LVD eşik

















