#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
tr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			SM Temp Group			SMTemp Group		SM Sicaklik Grup		SM Sic.Grup
2		32			15			SM Temp Number			SMTemp Num		SM Sicaklik Numarasi	SM Sic.No
3		32			15			Interrupt State			Interr State		Kesme Durumu		Kesme Durumu
4		32			15			Existence State			Existence State		Mevcut Durum		Mevcut Durum
5		32			15			Existent			Existent		Mevcut			Mevcut
6		32			15			Non Existent			Non Existent		Mevcut Degil		Mevcut Degil	
11		32			15			SM Temp Lost			SMTemp Lost		SM Sicaklik Kayip		SM Sic.Kayip
12		32			15			Last Number of SMTemp		Last SMTemp No.		SM Sicakligin Son Numarasi	SM Sic.Son No
13		32			15			Clear SMTemp Lost Alarm		Clr SMTemp Lost		SM Sic. Kayip Alarm Temizle	Sic.KayipATemiz
14		32			15			Clear				Clear			Temizle			Temizle
