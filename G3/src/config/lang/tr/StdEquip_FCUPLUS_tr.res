﻿#
# Locale language support: Turkish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1	32		15		Temperature 1		Temperature 1		Sicaklik 1		Sicaklik 1
2	32		15		Temperature 2		Temperature 2		Sicaklik 2		Sicaklik 2
3	32		15		Temperature 3		Temperature 3		Sicaklik 3		Sicaklik 3
4	32		15		Humidity		Humidity		Nem			Nem
5	32		15		Temp 1 Alm		Temp 1 Alm		Sicaklik 1 Alarm	Sicaklik 1 Alarm
6	32		15		Temp 2 Alm		Temp 2 Alm		Sicaklik 2 Alarm	Sicaklik 2 Alarm
7	32		15		Temp 3 Alm		Temp 3 Alm		Sicaklik 3 Alarm	Sicaklik 3 Alarm
8	32		15		Humidity Alm		Humidity Alm		Nem Alarm		Nem Alarm
9	32		15		Fan 1 Alm		Fan 1 Alm		Fan 1 Alarm		Fan 1 Alarm
10	32		15		Fan 2 Alm		Fan 2 Alm		Fan 2 Alarm		Fan 2 Alarm
11	32		15		Fan 3 Alm		Fan 3 Alm		Fan 3 Alarm		Fan 3 Alarm
12	32		15		Fan 4 Alm		Fan 4 Alm		Fan 4 Alarm		Fan 4 Alarm
13	32		15		Fan 5 Alm		Fan 5 Alm		Fan 5 Alarm		Fan 5 Alarm
14	32		15		Fan 6 Alm		Fan 6 Alm		Fan 6 Alarm		Fan 6 Alarm
15	32		15		Fan 7 Alm		Fan 7 Alm		Fan 7 Alarm		Fan 7 Alarm
16	32		15		Fan 8 Alm		Fan 8 Alm		Fan 8 Alarm		Fan 8 Alarm
17	32		15		DI 1 Alm		DI 1 Alm		DI 1 Alarm		DI 1 Alarm
18	32		15		DI 2 Alm		DI 2 Alm		DI 1 Alarm		DI 1 Alarm
19	32		15		Fan Type		Fan Type		Fan Tipi		Fan Tipi
20	32		15		With Fan 3		With Fan 3		Fan 3 ile		Fan 3 ile
21	32		15		Fan 3 Ctrl Logic	F3 Ctrl Logic		Fan 3 Kontrol 		Fan 3 Kontrol 
22	32		15		With Heater		With Heater		Isiticili		Isiticili
23	32		15		With Humiture Sensor	With Hum Sensor		Nem Sensoru		Nem Sensoru
24	32		15		Fan 1 State		Fan 1 State		Fan 1 Durum		Fan 1 Durum
25	32		15		Fan 2 State		Fan 2 State		Fan 2 Durum		Fan 2 Durum
26	32		15		Fan 3 State		Fan 3 State		Fan 3 Durum		Fan 3 Durum
27	32		15		Temp Sensor Fail	T Sensor Fail		Sicaklik Sensor Arizasi		Sic.Sensor Ariza
28	32		15		Heat Change		Heat Change		Isı Degisimi		Isı Degisimi
29	32		15		Forced Vent		Forced Vent		Cebri Havalandirma	Cebri Havalandr
30	32		15		Not Exist		Not Exist		Mevcut Degil		Mevcut Degil
31	32		15		Exist		Exist		Mevcut			Mevcut
32	32		15		Heater Logic		Heater Logic		Isıtıcı Lojik		Isıtıcı Lojik
33	32		15		ETC Logic		ETC Logic		ETC Lojik		ETC Lojik
34	32		15		Stop			Stop			Durdur			Durdur
35	32		15		Start			Start			Baslat			Baslat
36	32		15		Temp1 Over		Temp1 Over		Sicaklik1 ustunde	Sicaklik1 ustunde
37	32		15		Temp1 Under		Temp1 Under		Sicaklik1 altinda	Sicaklik1 altinda
38	32		15		Temp2 Over		Temp2 Over		Sicaklik2 ustunde	Sicaklik2 ustunde
39	32		15		Temp2 Under		Temp2 Under		Sicaklik2 altinda	Sicaklik2 altinda
40	32		15		Temp3 Over		Temp3 Over		Sicaklik3 ustunde	Sicaklik3 ustunde
41	32		15		Temp3 Under		Temp3 Under		Sicaklik3 altinda	Sicaklik3 altinda
42	32		15		Humidity Over		Humidity Over		Nem ustunde		Nem ustunde
43	32		15		Humidity Under		Humidity Under		Nem altında		Nem altında
44	32		15		Temp1 Sensor		Temp1 Sensor		Sicaklik1 Sensor	Sicaklik1 Sensor
45	32		15		Temp2 Sensor		Temp2 Sensor		Sicaklik2 Sensor	Sicaklik2 Sensor
46	32		15		DI1 Alm Type		DI1 Alm Type		DI1 Alarm Tipi		DI1 Alarm Tipi
47	32		15		DI2 Alm Type		DI2 Alm Type		DI2 Alarm Tipi		DI2 Alarm Tipi
48	32		15		No. of Fans in Fan Group 1		Num of FanG1			Fan 1 Numara		Fan 1 Numara
49	32		15		No. of Fans in Fan Group 2		Num of FanG2			Fan 2 Numara		Fan 2 Numara
50	32		15		No. of Fans in Fan Group 3		Num of FanG3			Fan 3 Numara		Fan 3 Numara
51	32		15		T Sensor of F1		T Sensor of F1		F1 Sicaklik Sensor	F1 Sic. Sensor
52	32		15		T Sensor of F2		T Sensor of F2		F2 Sicaklik Sensor	F2 Sic. Sensor
53	32		15		T Sensor of F3		T Sensor of F3		F3 Sicaklik Sensor	F3 Sic. Sensor
54	32		15		T Sensor of Heater1	T Sensor of H1	Isitici1 Sicaklik Sensor	Isit.1 Sic.Sensor
55	32		15		T Sensor of Heater2	T Sensor of H2	Isitici2 Sicaklik Sensor	Isit.2 Sic.Sensor
56	32		15		HEX Fan Group 1 50% Speed Temp		H1 50%Speed Temp		Isitici 1 Yari Sicaklik		Isit.1 Yari Sic.
57	32		15		HEX Fan Group 1 100% Speed Temp		H1 100%Speed Temp		Isitici 1 Tam Sicaklik		Isit.1 Tam Sic.
58	32		15		HEX Fan Group 2 Start Temp			H2 Start Temp			Isitici 2 Sicaklik Baslat 		Isit. 2 Sic. Baslat 
59	32		15		HEX Fan Group 2 100% Speed Temp		H2 100%Speed Temp		Isitici 2 Tam Sicaklik		Isit.2 Tam Sic.
60	32		15		HEX Fan Group 2 Stop Temp			H2 Stop Temp			Isitici 2 Sicaklik Durdur	Isit.2 Sic. Durdur
61	32		15		Fan Filter Fan G1 Start Temp		FV1 Start Temp			Cebri Havalandirma 1 Sicaklik Baslat	C.Hava.1 Sic.Baslat
62	32		15		Fan Filter Fan G1 Max Speed Temp	FV1 Full Temp			Cebri Havalandirma 1 Tam Sicaklik	C.Hava.1 TamSic.
63	32		15		Fan Filter Fan G1 Stop Temp			FV1 Stop Temp			Cebri Havalandirma 1 Sicaklik Durdur	C.Hava.1 Sic.Durdur
64	32		15		Fan Filter Fan G2 Start Temp		FV2 Start Temp			Cebri Havalandirma2 Sicaklik Baslat	C.Hava.2 Sic.Baslat
65	32		15		Fan Filter Fan G2 max speed Temp	FV2 Full Temp			Cebri Havalandirma 2 Tam Sicaklik	C.Hava.2 TamSic.
66	32		15		Fan Filter Fan G2 Stop Temp			FV2 Stop Temp			Cebri Havalandirma 2 Sicaklik Durdur	C.Hava.2 Sic.Durdur
67	32		15		Fan Filter Fan G3 Start Temp		F3 Start Temp			Cebri Havalandirma 3 Sicaklik Baslat	C.Hava.3 Sic.Baslat
68	32		15		Fan Filter Fan G3 max speed Temp	F3 Full Temp			Cebri Havalandirma 3 Tam Sicaklik	C.Hava.3 TamSic.
69	32		15		Fan Filter Fan G3 Stop Temp			F3 Stop Temp			Cebri Havalandirma 3 Sicaklik Durdur	C.Hava.3 Sic.Durdur
70	32		15		Heater1 Start Temp	H1 Start Temp	Isitici1 Sicaklik Baslat 	Isitici1 Sic. Baslat 
71	32		15		Heater1 Stop Temp	H1 Stop Temp	Isitici1 Sicaklik Durdur	Isitici1 Sic. Durdur
72	32		15		Heater2 Start Temp	H2 Start Temp	Isitici2 Sicaklik Baslat 	Isitici2 Sic. Baslat
73	32		15		Heater2 Stop Temp	H2 Stop Temp	Isitici2 Sicaklik Durdur	Isitici2 Sic. Durdur
74	32		15		Fan Group 1 Max Speed		FG1 Max Speed		F1 Maksimum Hız		F1 Maksimum Hız
75	32		15		Fan Group 2 Max Speed		FG2 Max Speed		F2 Maksimum Hız		F2 Maksimum Hız
76	32		15		Fan Group 3 Max Speed		FG3 Max Speed		F3 Maksimum Hız		F3 Maksimum Hız
77	32		15		Fan Min Speed		Fan Min Speed		Fan Mininum Hız		Fan Mininum Hız
78	32		15		Close			Close			Kapali		Kapali
79	32		15		Open			Open			Acik		Acik
80	32		15		Self Rect Alm		Self Rect Alm		Self Dogrultucu Alarm		Self Dog. Alarm
81	32		15		With FCUP		With FCUP		FCUP ile		FCUP ile
82	32		15		Normal			Normal			Normal			Normal
83	32		15		Low				Low			Düsük		Düsük
84	32		15		High				High			Yuksek		Yuksek
85	32		15		Alarm				Alarm			Alarm		Alarm
86	32		15		Times of Communication Fail	Times Comm Fail		Haberlesme Hatasinin Suresi	Hab. Hata Suresi
87	32		15		Existence State			Existence State		Varlik Durumu	Varlik Durumu
88	32		15		Comm OK				Comm OK			Haberlesme OK	Haberlesme OK
89	32		15		All Batteries Comm Fail		AllBattCommFail		Tum Aku Haberlesme Hatasi	Tum Aku Hab. Hata
90	32		15		Communication Fail		Comm Fail		Haberlesme Hatasi	Haberlesme Hatasi
91	32		15		FCUPLUS				FCUP			FCUPLUS		FCUP
111	32		15		Enter Test Mode			Enter Test Mode				TestModunaGirin			TestModunaGirin		
112	32		15		Exit Test Mode			Exit Test Mode				Test Modundan Çık		Test ModundÇık		
113	32		15		Start Fan Group 1 Test		StartFanG1Test				FanG1TestiniBaşlat		FanG1TestBaşlat		
114	32		15		Start Fan Group 2 Test		StartFanG2Test				FanG2TestiniBaşlat		FanG2TestBaşlat			
115	32		15		Start Fan Group 3 Test		StartFanG3Test				FanG3TestiniBaşlat		FanG3TestBaşlat			
116	32		15		Start Heater1 Test		StartHeat1Test				Isıtı1TestiBaşlat		Isıt1TestBaşlat		
117	32		15		Start Heater2 Test		StartHeat2Test				Isıtı2TestiBaşlat		Isıt2TestBaşlat		
118	32		15		Clear					Clear						Açık					Açık				
120	32		15		Clear Fan1 Run Time		ClrFan1RunTime				AçıkFan1çalızamanı		AçıkFan1çalızam	
121	32		15		Clear Fan2 Run Time		ClrFan2RunTime				AçıkFan2çalızamanı		AçıkFan2çalızam		
122	32		15		Clear Fan3 Run Time		ClrFan3RunTime				AçıkFan3çalızamanı		AçıkFan3çalızam	
123	32		15		Clear Fan4 Run Time		ClrFan4RunTime				AçıkFan4çalızamanı		AçıkFan4çalızam		
124	32		15		Clear Fan5 Run Time		ClrFan5RunTime				AçıkFan5çalızamanı		AçıkFan5çalızam		
125	32		15		Clear Fan6 Run Time		ClrFan6RunTime				AçıkFan6çalızamanı		AçıkFan6çalızam		
126	32		15		Clear Fan7 Run Time		ClrFan7RunTime				AçıkFan7çalızamanı		AçıkFan7çalızam		
127	32		15		Clear Fan8 Run Time		ClrFan8RunTime				AçıkFan8çalızamanı		AçıkFan8çalızam		
128	32		15		Clear Heater1 Run Time	ClrHtr1RunTime				TemiHeat1ÇalışSüresi	TemHeat1ÇalıSür		
129	32		15		Clear Heater2 Run Time	ClrHtr2RunTime				TemiHeat2ÇalışSüresi	TemHeat2ÇalıSür		
130	32		15		Fan1 Speed Tolerance		Fan1 Spd Toler			Fan1 Hız Toleransı		Fan1HızTolerans	
131	32		15		Fan2 Speed Tolerance		Fan2 Spd Toler			Fan2 Hız Toleransı		Fan2HızTolerans	
132	32		15		Fan3 Speed Tolerance		Fan3 Spd Toler			Fan3 Hız Toleransı		Fan3HızTolerans	
133	32		15		Fan1 Rated Speed			Fan1 Rated Spd			Fan1 Anma Hızı			Fan1 Anma Hızı	
134	32		15		Fan2 Rated Speed			Fan2 Rated Spd			Fan2 Anma Hızı			Fan2 Anma Hızı	
135	32		15		Fan3 Rated Speed			Fan3 Rated Spd			Fan3 Anma Hızı			Fan3 Anma Hızı	
136	32		15		Heater Test Time			HeaterTestTime			Isıtıcı Test Süresi		IsıtıTestSüres
137	32		15		Heater Temperature Delta	HeaterTempDelta			Isıtıcı Sıcaklığı Delta	IsıtıSıcakDelta
140	32		15		Running Mode				Running Mode			Koşu modu				Koşu modu	
141	32		15		FAN1 Status					FAN1Status				FAN1 Durumu				FAN1 Durumu		
142	32		15		FAN2 Status					FAN2Status				FAN2 Durumu				FAN2 Durumu		
143	32		15		FAN3 Status					FAN3Status				FAN3 Durumu				FAN3 Durumu		
144	32		15		FAN4 Status					FAN4Status				FAN4 Durumu				FAN4 Durumu			
145	32		15		FAN5 Status					FAN5Status				FAN5 Durumu				FAN5 Durumu			
146	32		15		FAN6 Status					FAN6Status				FAN6 Durumu				FAN6 Durumu			
147	32		15		FAN7 Status					FAN7Status				FAN7 Durumu				FAN7 Durumu			
148	32		15		FAN8 Status					FAN8Status				FAN8 Durumu				FAN8 Durumu			
149	32		15		Heater1 Test Status			Heater1Status			Isıtı1TestDurumu		Isıtı1Durumu		
150	32		15		Heater2 Test Status			Heater2Status			Isıtı2TestDurumu		Isıtı2Durumu		
151	32		15		FAN1 Test Result			FAN1TestResult			FAN1 Test Sonucu			FAN1TestSonucu	
152	32		15		FAN2 Test Result			FAN2TestResult			FAN2 Test Sonucu			FAN2TestSonucu	
153	32		15		FAN3 Test Result			FAN3TestResult			FAN3 Test Sonucu			FAN3TestSonucu	
154	32		15		FAN4 Test Result			FAN4TestResult			FAN4 Test Sonucu			FAN4TestSonucu	
155	32		15		FAN5 Test Result			FAN5TestResult			FAN5 Test Sonucu			FAN5TestSonucu	
156	32		15		FAN6 Test Result			FAN6TestResult			FAN6 Test Sonucu			FAN6TestSonucu	
157	32		15		FAN7 Test Result			FAN7TestResult			FAN7 Test Sonucu			FAN7TestSonucu	
158	32		15		FAN8 Test Result			FAN8TestResult			FAN8 Test Sonucu			FAN8TestSonucu	
159	32		15		Heater1 Test Result			Heater1TestRslt			Isıtıcı1TestSonucu			Isıt1TestSonuc
160	32		15		Heater2 Test Result			Heater2TestRslt			Isıtıcı2TestSonucu			Isıt2TestSonuc
171	32		15		FAN1 Run Time				FAN1RunTime				FAN1 Çalışma Süresi			FAN1ÇalışSüres	
172	32		15		FAN2 Run Time				FAN2RunTime				FAN2 Çalışma Süresi			FAN2ÇalışSüres		
173	32		15		FAN3 Run Time				FAN3RunTime				FAN3 Çalışma Süresi			FAN3ÇalışSüres		
174	32		15		FAN4 Run Time				FAN4RunTime				FAN4 Çalışma Süresi			FAN4ÇalışSüres		
175	32		15		FAN5 Run Time				FAN5RunTime				FAN5 Çalışma Süresi			FAN5ÇalışSüres		
176	32		15		FAN6 Run Time				FAN6RunTime				FAN6 Çalışma Süresi			FAN6ÇalışSüres		
177	32		15		FAN7 Run Time				FAN7RunTime				FAN7 Çalışma Süresi			FAN7ÇalışSüres		
178	32		15		FAN8 Run Time				FAN8RunTime				FAN8 Çalışma Süresi			FAN8ÇalışSüres		
179	32		15		Heater1 Run Time			Heater1RunTime			Isıtı1ÇalışmaSüresi			Isıt1ÇalıSüres
180	32		15		Heater2 Run Time			Heater2RunTime			Isıtı1ÇalışmaSüresi			Isıt1ÇalıSüres

181	32		15		Normal						Normal					Normal						Normal	
182	32		15		Test						Test					Ölçek						Ölçek	
183	32		15		NA							NA						NA							NA		
184	32		15		Stop						Stop					durdurmak					durdurmak	
185	32		15		Run							Run						Koşmak						Koşmak		
186	32		15		Test						Test					Ölçek						Ölçek	

190	32		15		FCUP is Testing				FCUPIsTesting			FCUP Test Ediyor			FCUPTestEdiyor	
191	32		15		FAN1 Test Fail				FAN1TestFail			FAN1TestiBaşarı				FAN1TestBaşarı	
192	32		15		FAN2 Test Fail				FAN2TestFail			FAN2TestiBaşarı				FAN2TestBaşarı	
193	32		15		FAN3 Test Fail				FAN3TestFail			FAN3TestiBaşarı				FAN3TestBaşarı	
194	32		15		FAN4 Test Fail				FAN4TestFail			FAN4TestiBaşarı				FAN4TestBaşarı	
195	32		15		FAN5 Test Fail				FAN5TestFail			FAN5TestiBaşarı				FAN5TestBaşarı	
196	32		15		FAN6 Test Fail				FAN6TestFail			FAN6TestiBaşarı				FAN6TestBaşarı	
197	32		15		FAN7 Test Fail				FAN7TestFail			FAN7TestiBaşarı				FAN7TestBaşarı	
198	32		15		FAN8 Test Fail				FAN8TestFail			FAN8TestiBaşarı				FAN8TestBaşarı	
199	32		15		Heater1 Test Fail			Heater1TestFail			Isıtıcı1TestiBaşarı		Isıt1TestBaşar
200	32		15		Heater2 Test Fail			Heater2TestFail			Isıtıcı2TestiBaşarı		Isıt2TestBaşar
201	32		15		Fan is Test					Fan is Test				Fan Testi					Fan Testi		
202	32		15		Heater is Test				Heater is Test			Isıtıcı Testidir			IsıtıTestidir	
203	32		15		Version 106					Version 106				versiyon 106				versiyon 106		
204	32		15		Fan1 DownLimit Speed			Fan1 DnLmt Spd			Fan1 DownLimit Hızı		Fan1 DnLmt Hızı		
205	32		15		Fan2 DownLimit Speed			Fan2 DnLmt Spd			Fan2 DownLimit Hızı		Fan2 DnLmt Hızı		
206	32		15		Fan3 DownLimit Speed			Fan3 DnLmt Spd			Fan3 DownLimit Hızı		Fan3 DnLmt Hızı		
