#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
tr

[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32		15			SMDU Group			SMDU Group		SMDU Grup			SMDU Grup
2		32		15			Standby				Standby			Bekleme				Bekleme	
3		32		15			Refresh				Refresh			Yenile				Yenile	
4		32		15			Refresh Setting			Refresh Setting		Yenileme Ayary	 		Yenileme Ayary
5		32		15			Emergency-Stop Function		E-Stop			Acil-Durdurma Fonksiyonu	Acil Durdurma
6		32		15			Yes				Yes			Evet				Evet
7		32		15			Existence State			Existence State		Mevcut Durum			Mevcut Durum
8		32		15			Existent			Existent		Mevcut				Mevcut
9		32		15			Non-Existent			Non-Existent		Mevcut Degil			Mevcut Degil
10		32		15			Number of SM-DUs		No of SM-DUs		SMDU Numaralary			SMDU Numaralary	
11		32		15			SM-DU Config Changed		Cfg Changed		SMDU Ayarlary Degistirildi	SMDU Ayar.Degis
12		32		15			Not Changed			Not Changed		Degistirilmedi			Degistirilmedi
13		32		15			Changed				Changed			Degistirildi			Degistirildi	
