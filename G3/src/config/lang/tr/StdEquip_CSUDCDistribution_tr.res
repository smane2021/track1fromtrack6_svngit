#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
tr

[RES_INFO]																					
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE							
1		32			15			Distribution Fuse Tripped		Dist Fuse Trip		Yuk Sigortasi 1 Atik		Yuk Sig 1 Atik					
2		32			15			Contactor 1 Failure			Contactor1 fail		Kontaktor 1 Ariza			Kont.1 Ariza					
3		32			15			Distribution Fuse 2 Tripped		Dist fuse2 trip		Yuk Sigortasi 2 Atik		Yuk Sig 2 Atik					
4		32			15			Contactor 2 Failure			Contactor2 fail		Kontaktor 2 Ariza			Kont.2 Ariza					
5		32			15			Distribution Voltage			Dist voltage		Yuk Gerilimi 1			Yuk Gerilimi 1					
6		32			15			Distribution Current			Dist current		Yuk Akimi	 1			Yuk Akimi	 1				
7		32			15			Distribution Temperature		Dist temperature	Sicaklik				Sicaklik					
8		32			15			Distribution Current 2			Dist current2		Yuk Akimi 2				Yuk Akimi 2					
9		32			15			Distribution Voltage Fuse 2		Dist volt fuse2		Yuk Gerilimi 2			Yuk Gerilimi 2					
10		32			15			CSU DCDistribution			CSU_DCDistrib		CSU DC Yuk				CSU DC Yuk					
11		32			15			CSU DCDistribution Failure		CSU_DCDistfail		CSU DC Yuk Ariza			DC Yuk Ariza					
12		32			15			No					No			Hayir					Hayir					
13		32			15			Yes					Yes			Evet					Evet					
14		32			15			Existent				Existent		Mevcut					Mevcut					
15		32			15			Non-Existent				Non-Existent		Mevcut Degil				Mevcut Degil
