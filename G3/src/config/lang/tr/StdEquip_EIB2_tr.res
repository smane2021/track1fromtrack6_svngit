﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
zh

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			EIB-2				EIB-2			EIB-2			EIB-2
9		32			15			Bad Battery Block	Bad Batt Block		Kotu Aku Grubu		Kotu Aku Grup					
10		32			15			Load Current 1		Load Curr 1		Yuk Akimi 1		Yuk Akimi 1			
11		32			15			Load Current 2		Load Curr 2		Yuk Akimi 2		Yuk Akimi 2			
12		32			15			Relay Output 9		Relay Output 9		Role Cikisi 9		Role Cikisi 9				
13		32			15			Relay Output 10		Relay Output 10		Role Cikisi 10		Role Cikisi 10				
14		32			15			Relay Output 11		Relay Output 11		Role Cikisi 11		Role Cikisi 11				
15		32			15			Relay Output 12		Relay Output 12		Role Cikisi 12		Role Cikisi 12				
16		32			15			EIB Communication Failure	EIB Comm Fail	EIB Haberlesme Hatasi	EIB Hab.Hata				
17		32			15			State			State			Durum			Durum
18		32			15			Shunt 2 Full Current	Shunt 2 Curr		Sont 2 Tam Akim		Sont 2 Akim					
19		32			15			Shunt 3 Full Current	Shunt 3 Curr		Sont 3 Tam Akim		Sont 3 Akim					
20		32			15			Shunt 2 Full Voltage	Shunt 2 Volt		Sont 2 Tam Gerilim	Sont 2 Gerilim				
21		32			15			Shunt 3 Full Voltage	Shunt 3 Volt		Sont 3 Tam Gerilim	Sont 3 Gerilim				
22		32			15			Load Shunt 1		Load Shunt 1		Yuk Sontu 1		Yuk Sontu 1		
23		32			15			Load Shunt 2		Load Shunt 2		Yuk Sontu 2		Yuk Sontu 2		
24		32			15			Enable			Enable			Aktif			Aktif	
25		32			15			Disable			Disable			Pasif			Pasif	
26		32			15			Closed			Closed			Kapali			Kapali
27		32			15			Open			Open			Acik			Acik
28		32			15			State			State			Durum			Durum
29		32			15			No			No			Hayir			Hayir
30		32			15			Yes			Yes			Evet			Evet
31		32			15			EIB Communication Failure	EIB Comm Fail	EIB Haberlesme Hatasi	EIB Hab.Hata				
32		32			15			Voltage 1		Voltage 1		Gerilim 1		Gerilim 1			
33		32			15			Voltage 2		Voltage 2		Gerilim 2		Gerilim 2			
34		32			15			Voltage 3		Voltage 3		Gerilim 3		Gerilim 3			
35		32			15			Voltage 4		Voltage 4		Gerilim 4		Gerilim 4			
36		32			15			Voltage 5		Voltage 5		Gerilim 5		Gerilim 5			
37		32			15			Voltage 6		Voltage 6		Gerilim 6		Gerilim 6			
38		32			15			Voltage 7		Voltage 7		Gerilim 7		Gerilim 7			
39		32			15			Voltage 8		Voltage 8		Gerilim 8		Gerilim 8			
40		32			15			Battery Number		Batt No.		Aku Numarasi		Aku No			
41		32			15			0			0			0			0
42		32			15			1			1			1			1
43		32			15			2			2			2			2
44		32			15			Load Current 3		Load Curr 3		Yuk Akimi 3		Yuk Akimi 3			
45		32			15			3			3			3			3
46		32			15			Load Number		Load Num		Yuk Sayisi		Yuk Sayisi			
47		32			15			Shunt 1 Full Current	Shunt 1 Curr		Sont 1 Tam Akim		Sont 1 Akim					
48		32			15			Shunt 1 Full Voltage	Shunt 1 Volt		Sont 1 Tam Gerilim	Sont 1 gerilim				
49		32			15			Voltage Type		Voltage Type		Gerilim Tipi		Gerilim Tipi			
50		32			15			48(Block4)		48(Block4)		48(4 adet)		48(4 adet)			
51		32			15			Mid point		Mid point		Orta nokta		Orta Nokta			
52		32			15			24(Block2)		24(Block2)		24(2 adet)		24(2 adet)			
53		32			15			Block Voltage Diff(12V)	Block Diff(12V)		Adet Gerilimi (12V)	Adet Gerilimi (12V)					
54		32			15			Relay Output 13		Relay Output 13		Role Cikisi 13		Role 13				
55		32			15			Block Voltage Diff(Mid)	Block Diff(Mid)		Adet Gerilimi (Orta)	Adet Gerilimi (Orta)					
56		32			15			Number of Used Blocks	Used Blocks		Kullanilan Adet Sayisi	Kullanilan Adet					
78		32			15			Relay 9 Test		Relay 9 Test		Role 9 Test		Role 9 Test				
79		32			15			Relay 10 Test		Relay 10 Test		Role 10 Test		Role 10 Test				
80		32			15			Relay 11 Test		Relay 11 Test		Role 11 Test		Role 11 Test				
81		32			15			Relay 12 Test		Relay 12 Test		Role 12 Test		Role 12 Test				
82		32			15			Relay 13 Test		Relay 13 Test		Role 13 Test		Role 13 Test
83		32			15			Not Used			Not Used	Kullanilmamis		Kullanilmamis
84		32			15			General				General		Genel			Genel
85		32			15			Load				Load		Yuk			Yuk
86		32			15			Battery				Battery		Aku			Aku
87		32			15			Shunt1 Set As			Shunt1SetAs	Sont 1 Olarak Ayarla	Sont1OlarakAyar
88		32			15			Shunt2 Set As			Shunt2SetAs	Sont 2 Olarak Ayarla	Sont2OlarakAyar
89		32			15			Shunt3 Set As			Shunt3SetAs	Sont 3 Olarak Ayarla	Sont3OlarakAyar
90		32			15			Shunt1 Reading			Shunt1Reading		Sont 1 Okuma		Sont 1 Okuma
91		32			15			Shunt2 Reading			Shunt2Reading		Sont 2 Okuma		Sont 2 Okuma
92		32			15			Shunt3 Reading			Shunt3Reading		Sont 3 Okuma		Sont 3 Okuma
93		32			15			Temperature1			Temp1			Sicaklik 1		Sicaklik 1
94		32			15			Temperature2			Temp2			Sicaklik 2		Sicaklik 2
95		32			15			Load1 Alarm Flag		Load1 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
96		32			15			Load2 Alarm Flag		Load2 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
97		32			15			Load3 Alarm Flag		Load3 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
98		32			15			Current1 High Current		Curr 1 Hi		Current1 High Current			Curr 1 Hi
99		32			15			Current1 Very High Current	Curr 1 Very Hi		Current1 Very High Current		Curr 1 Very Hi
100		32			15			Current2 High Current		Curr 2 Hi		Current2 High Current			Curr 2 Hi
101		32			15			Current2 Very High Current	Curr 2 Very Hi		Current2 Very High Current		Curr 2 Very Hi
102		32			15			Current3 High Current		Curr 3 Hi		Current3 High Current			Curr 3 Hi
103		32			15			Current3 Very High Current	Curr 3 Very Hi		Current3 Very High Current		Curr 3 Very Hi
104		32			15			DO1 Normal State  		DO1 Normal		DO1 Normal State  			DO1 Normal
105		32			15			DO2 Normal State  		DO2 Normal		DO2 Normal State  			DO2 Normal
106		32			15			DO3 Normal State  		DO3 Normal		DO3 Normal State  			DO3 Normal
107		32			15			DO4 Normal State  		DO4 Normal		DO4 Normal State  			DO4 Normal
108		32			15			DO5 Normal State  		DO5 Normal		DO5 Normal State  			DO5 Normal
109		32			15			Non-Energized			Non-Energized		Non-Energized				Non-Energized
110		32			15			Energized			Energized		Energized				Energized

500		32			15			Current1 Break Size				Curr1 Brk Size		Current1 Break Size				Curr1 Brk Size	
501		32			15			Current1 High 1 Current Limit	Curr1 Hi1 Lmt		Current1 High 1 Current Limit	Curr1 Hi1 Lmt	
502		32			15			Current1 High 2 Current Limit	Curr1 Hi2 Lmt		Current1 High 2 Current Limit	Curr1 Hi2 Lmt	
503		32			15			Current2 Break Size				Curr2 Brk Size		Current2 Break Size				Curr2 Brk Size	
504		32			15			Current2 High 1 Current Limit	Curr2 Hi1 Lmt		Current2 High 1 Current Limit	Curr2 Hi1 Lmt	
505		32			15			Current2 High 2 Current Limit	Curr2 Hi2 Lmt		Current2 High 2 Current Limit	Curr2 Hi2 Lmt	
506		32			15			Current3 Break Size				Curr3 Brk Size		Current3 Break Size				Curr3 Brk Size	
507		32			15			Current3 High 1 Current Limit	Curr3 Hi1 Lmt		Current3 High 1 Current Limit	Curr3 Hi1 Lmt	
508		32			15			Current3 High 2 Current Limit	Curr3 Hi2 Lmt		Current3 High 2 Current Limit	Curr3 Hi2 Lmt	
509		32			15			Current1 High 1 Current			Curr1 Hi1Cur		Current1 High 1 Current			Curr1 Hi1Cur	
510		32			15			Current1 High 2 Current			Curr1 Hi2Cur		Current1 High 2 Current			Curr1 Hi2Cur	
511		32			15			Current2 High 1 Current			Curr2 Hi1Cur		Current2 High 1 Current			Curr2 Hi1Cur	
512		32			15			Current2 High 2 Current			Curr2 Hi2Cur		Current2 High 2 Current			Curr2 Hi2Cur	
513		32			15			Current3 High 1 Current			Curr3 Hi1Cur		Current3 High 1 Current			Curr3 Hi1Cur	
514		32			15			Current3 High 2 Current			Curr3 Hi2Cur		Current3 High 2 Current			Curr3 Hi2Cur	
