#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name
#
[LOCALE_LANGUAGE]
tr

[RES_INFO]																					
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN			FULL_IN_LOCALE			ABBR_iN_LOCALE							
1	32			15			Rectifier Group					Rect Group			Dogrultucu Grup			Dogrultucu Grup			
2	32			15			Total Current				        Tot Rect Curr			Toplam Akim			Toplam Akim			
3	32			15			Average Voltage					Rect Voltage			Ortalama Gerilim		Ort. Gerilim				
4	32			15			Rectifier Capacity Used			        Sys Cap Used			Kullanilmis Dogrultucu Kapasitesi	Kull.Dog.Kap				
5	32			15			Maximum Capacity Used			        Max Cap Used			Maksimum Kullanilmis Kapasitesi 	Max Kull. Kap						
6	32			15			Minimum Capacity Used			        Min Cap Used			Minimum Kullanilmis Kapasitesi	Min Kull. Kap						
7	32			15			Communicating Rectifiers			Comm Rect Num			Dogrultucular Iletisimde	Dogrul. Iletisimde							
8	32			15			Active Rectifiers				Active Rects			Dogrultucular Aktif		Dogrul. Aktif					
9	32			15			Installed Rectifiers				Inst Rects			Yuklu Dogrultucular		Yuklu Dogrul.					
10	32			15			Rectifier AC Fail Status			AC Fail Status			Dogrultucu AC Ariza Durumu	AC Ariza Durum							
11	32			15			Multi-rectifier Fail Status			Multi-Rect Fail			Coklu-Dogrultucu AC Ariza Durumu	Coklu-Dog. Ariza							
12	32			15			Rectifer Current Limit				Current Limit			Dogrultucu Akim Limiti	Dog.Akim Limiti						
13	32			15			Voltage Trim					Voltage Trim			Gerilim Trim		Gerilim Trim				
14	32			15			DC On/Off Control				DC On/Off Ctrl			DC Acik/Kapali Kontrol 		DC Acik/Kapali					
15	32			15			AC On/Off Control				AC On/Off Ctrl			AC Acik/Kapali Kontrol 		AC Acik/Kapali					
16	32			15			Rectifiers LEDs Control				LEDs Control			Dogrultucu Led Kontrol		Dog.Led Kontrol					
17	32			15			Fan Speed Control				Fan Speed Ctrl			Fan Hiz Kontrol			Fan Hiz Kntrl				
18	32			15			Rated Voltage					Rated Voltage			Anma Gerilimi			Anma Gerilimi			
19	32			15			Rated Current					Rated Current			Anma Akimi			Anma Akimi			
20	32			15			HVSD Limit					HVSD Limit			HVSD Limit			HVSD Limit					
21	32			15			Low Voltage Limit				Lo-Volt Limit			Dusuk Gerilim Limiti		Dus. Ger. Limiti					
22	32			15			High Temperature Limit				Hi-Temp Limit			Yuksek Sicaklik Limiti		Yuk. Sic. Limiti					
24	32			15			Restart Time on Overvoltage			OverVRestartT 			Asiri Gerilim Restart Zamani		Asiri Ger. Restart						
25	32			15			WALK-In Time					WALK-In Time			Walk-In Suresi		Walk-In Suresi				
26	32			15			WALK-In						WALK-in				Walk-In		Walk-In		
27	32			15			Minimum Redundancy				Min Redundancy			Minimum Yedeklik		Min Yedeklik					
28	32			15			Maximum Redundancy				Max Redundancy			Maksimum Yedeklik		Max Yedeklik					
29	32			15			Switch Off Delay				SwitchOff Delay			Gecikme Kapatma			Gecikme Kapatma				
30	32			15			Cycle Period					Cyc Period			Dongu Suresi			Dongu Suresi			
31	32			15			Cycle Activation Time				Cyc Act Time			Dongu Aktivasyon Zamani		Don. Akt. Zamani					
32	32			15			Rectifier AC Fail				Rect AC Fail			Dogrultucu AC Ariza		Dog. AC Ariza					
33	32			15			Multi-Rectifiers Fail			Multi-rect Fail			Coklu-Dogrultucu Ariza		Coklu-Dog. Ariza						
36	32			15			Normal						Normal				Normal				Normal
37	32			15			Fail						Fail				Ariza				Ariza
38	32			15			Switch Off All					Switch Off All			Tumunu Kapatin			Tumunu Kapatin			
39	32			15			Switch On All					Switch On All			Tumunu Acin			Tumunu Acin			
42	32			15			Flash All					Flash All			Tum Flas			Tum Flas			
43	32			15			Stop Flash					Stop Flash			Flas Durdur			Flas Durdur			
44	32			15			Full Speed					Full Speed			Tam Hiz				Tam Hiz		
45	32			15			Automatic Speed					Auto Speed			Otomatik Hiz			Otomatik Hiz			
46	32			32			Current Limit Control				Curr Limit Ctrl			Akim Limit Kontrol		Akim Limit Kntrl					
47	32			32			Full Capacity Control				Full Cap Ctrl			Tam Kapasite Kontrol		Tam Kap. Kntrl					
54	32			15			Disabled					Disabled			Devre Disi			Devre Disi			
55	32			15			Enabled						Enabled				Etkinlestir			Etkinlestir	
68	32			15			ECO Mode					ECO Mode 			ECO Mod				ECO Mod		
72	32			15			Rectifier On at AC Overvoltage			AC Overvolt ON			Dogrultucudaki AC Asiri Gerilim		AC Asiri Ger.						
73	32			15			No						No				Hayir				Hayir
74	32			15			Yes						Yes				Evet				Evet
77	32			15			Pre-Current Limit Turn-On			Pre-Curr Limit			On-Akim Siniri			On-Akim Siniri					
78	32			15			Rectifier Power type				Rect Power type			Dogrultucu Guc Tipi		Dog. Guc Tipi					
79	32			15			Double Supply					Double Supply			Cift Besleme			Cift Besleme			
80	32			15			Single Supply					Single Supply			Tek Besleme			Tek Besleme			
81	32			15			Last Rectifiers Quantity			Last Rect Qty			Son Dogrultucular Miktari	Son Dog. Miktari							
82	32			15			Rectifier Lost					Rectifier Lost			Dogrultucu Kayip		Dogrul. Kayip				
83	32			15			Rectifier Lost					Rectifier Lost			Dogrultucu Kayip		Dogrul. Kayip				
84	32			15			Reset Rectifier Lost Alarm			Reset Rect Lost			Dogrultucu Kayip Alarm Reseti	Dog. Kayip Reset							
85	32			15			Clear						Yes				Temizle				Temizle
86	32			15			Confirm Rect Position/Phase			Confirm Pos/PH			Dogrultucu Pozisyon/Faz Onay	Dog. Pos/Faz Onay							
87	32			15			Confirm						Confirm				Onay				Onay
88	32			15			Best Operating Point				Best Point			En Iyi Calisma Noktasi		En Iyi Nokt.					
89	32			15			Rectifier Redundancy				Rect Redundancy			Dogrultucu Yedek		Dog. Yedek					
90	32			15			Load Fluctuation Range				Fluct Range			Yuk Dalgalanma Araligi		Yuk Dalg Araligi					
91	32			15			System Energy Saving Point			EngySave Point			Sistem Enerji Tasarrufu Noktasi		Sist.En.Tas.Nokt.						
92	32			15			E-Stop Function					E-Stop Function			Acil Durdurma Fonksiyonu	E-Durdurma Fon					
93	32			15			AC Phases					AC Phases			AC Fazlar			AC Fazlar			
94	32			15			Single Phase					Single Phase			Tek Faz				Tek Faz		
95	32			15			Three Phases					Three Phases			3 Faz				3 Faz		
96	32			15			Input Current Limit				InputCurrLimit			Giris Akim Siniri 		Giris Akim Lim					
97	32			15			Double Supply					Double Supply			Cift Besleme			cift Besleme			
98	32			15			Single Supply					Single Supply			Tek Besleme			Tek Besleme			
99	32			15			Small Supply					Small Supply			Kucuk Besleme			Kucuk Besleme			
100	32			15			Restart on Overvoltage Enabled			DCOverVRestart			Asiri Gerilim Etkin Yeniden Baslat	Asiri DC Restart 							
101	32			15			Sequence Start Interval				Start Interval			Dizi Baslangic Araligi	Baslangic Aralik						
102	32			15			Rated Voltage					Rated Voltage			Anma Gerilim			Anma Gerilimi			
103	32			15			Rated Current					Rated Current			Anma Akimi			Anma Akimi			
104	32			15			All Rectifiers Comm Fail			AllRectCommFail			Tum Dogrultucular Iletisim Hatasi		Dog. Ilets. Hata						
105	32			15			Inactive					Inactive			Pasif				Pasif		
106	32			15			Active						Active				Aktif				Aktif
107	32			15			ECO Mode Active					ECO Mode Active			ECO Mod Aktif			ECO Mod Aktif			
108	32			15			ECO Cycle Alarm					ECO Cycle Alarm			ECO Dongu Alarmi		ECO Dongu Alarm				
109	32			15			Reset ECO Cycle Alarm				Reset Cycle Alm			Dongu Alarmi Reset		Dongu Alarm Reset					
110	32			15			High Voltage Limit(24V)				Hi-Volt Limit			Yuksek Gerilim Siniri(24V)	Yuk. Ger. Limit(24V)						
111	32			15			Rectifiers Trim(24V)				Rect Trim			Dogrultucular Trim (24V)		Dog. Trim(24V)					
112	32			15			Rated Voltage(Internal Use)			Rated Voltage			Anma Gerilimi(Dahili Kullanim)		Anma Gerilimi						
113	32			15			Rect Info Change(M/S Internal Use)		RectInfo Change			Dogrultucular Bilgisi Degisikligi	Dog. Bil. Degiskl.								
114	32			15			MixHE Power					MixHE Power			MixHE Gucu			MixHE Gucu			
115	32			15			Derated						Derated				Indirilen			indirilen	
116	32			15			Non-derated					Non-derated			Indirilmeyen			Indirilmeyen			
117	32			15			All Rectifiers ON Time				Rects ON Time			Tum Dogrultucularin Acik Zamani		Dog. Acik Zamani					
118	32			15			All Rectifiers On				All Rect On			Tum Dogrultucular Acik 			Tum Dog. Acik				
119	32			15			Clear Rectifier Comm Fail Alarm			ClrRectCommFail			Dog. Hab. Hatasi Alarm Temizle	Dog.Hab.Hata.Temiz							
120	32			15			HVSD						HVSD				HVSD				HVSD
121	32			15			HVSD Voltage Difference				HVSD Volt Diff			HVSD Gerilim Farki		HVSD Ger. Fark					
122	32			15			Total Rated Current of Work On			Total Rated Cur			Toplam Anma Akimi calisir	Top. Anma Akim							
123	32			15			Diesel Power Limitation				Disl Pwr Limit			Dizel Guc Sinirlama 		Dizel Guc Lim.					
124	32			15			Diesel DI Input					Diesel DI Input			Dizel DI Giris			Dizel Di Giris			
125	32			15			Diesel Power Limit Point Set			DislPwrLmt Set			Dizel Guc Sinir Noktasi Ayarla 		DizelGucLim.Ayar.						
126	32			15			None						None				Hicbiri				Hicbiri
127	32			15			DI 1						DI 1				DI 1				DI 1
128	32			15			DI 2						DI 2				DI 2				DI 2
129	32			15			DI 3						DI 3				DI 3				DI 3
130	32			15			DI 4						DI 4				DI 4				DI 4
131	32			15			DI 5						DI 5				DI 5				DI 5
132	32			15			DI 6						DI 6				DI 6				DI 6
133	32			15			DI 7						DI 7				DI 7				DI 7
134	32			15			DI 8						DI 8				DI 8				DI 8
135	32			15			Current Limit Point				Curr Limit Pt			Akim Sinir Noktasi			Akim Sinir Nokt.				
136	32			15			Current Limit					Curr Limit			Akim Siniri				Akim Siniri		
137	32			15			Maximum Current Limit Value			Max Curr Limit			Maksimum Akim Sinir Degeri		Max. Akim Lim.						
138	32			15			DeFail Current Limit Point			Def Curr Lmt Pt			Varsayilan Akim Siniri Noktasi		Vars. Akim Lim.						
139	32			15			Minimum Current Limit Value			Min Curr Limit			Minimum Akim Siniri Noktasi		Min. Akim Lim.						
140	32			15			AC Power Limit Mode			AC Power Lmt 		AC Guc Limiti			AC Guc Limiti
141	32			15			A					A			A				A
142	32			15			B					B			B				B
143	32			15			Existence State				Existence State		Varlik Durumu			Varlik Durumu
144	32			15			Existent				Existent		Mevcut				Mevcut	
145	32			15			Not Existent				Not Existent		Mevcut Degil			Mevcut Degil
146	32			15			Total Output Power			Output Power		Toplam Cikis Gucu		Toplam Cikis Gucu	 
147	32			15			Total Slave Rated Current		Total RatedCurr		Toplam Ikincil Anma Akimi	Top.IkincilAnmaAki
148	32			15			Reset Rectifier IDs		Reset Rect IDs			Dogrultucu ID Reset		Dogrul.ID Reset 	
149	32			15			HVSD Voltage Difference (24V)			HVSD Volt Diff		HVSD Gerilim Farki(24V)			HVSD Gerilim Fark
#changed by Frank Wu,28/30,20140217, for upgrading software of rectifier---start---
150	32			15			Normal Update				Normal Update		Normal Guncelleme			Normal Gunce.
151	32			15			Force Update				Force Update		Zorla Güncelleme		Zorla Güncel.  
152	32			15			Update OK Number			Update OK Num		Guncelleme OK Sayisi		Guncel.OKSay.
153	32			15			Update State				Update State		 Guncelleme Durumu		Guncel.Durumu
154	32			15			Updating					Updating	 Guncelleniyor			Guncelleniyor		
155	32			15			Normal Successful			Normal Success		Normal Basarili			Normal Basari.
156	32			15			Normal Failed				Normal Fail		 Normal Basarisiz		Normal Basari.
157	32			15			Force Successful			Force Success		 Zorla Basarili			Zorla Basarili
158	32			15			Force Failed				Force Fail		Zorla Basarisiz			Zorla Basarisiz
159	32			15			Communication Time-Out		Comm Time-Out			Haberlesme Zaman Asimi		Hab. Zaman Asimi
160	32			15			Open File Failed			Open File Fail		 Dosya Ac Basarisiz		Dosya Ac Basari.
161		32			15			Delay of LowRectCap Alarm		LowRectCapDelay				Delay of LowRectCap Alarm			LowRectCapDelay		
162		32			15			Maximum Redundacy 				MaxRedundacy 				Maximum Redundacy 					MaxRedundacy 		
163		32			15			Low Rectifier Capacity			Low Rect Cap				Low Rectifier Capacity			Low Rect Cap			
164		32			15			High Rectifier Capacity			High Rect Cap				High Rectifier Capacity			High Rect Cap			

165		32			15			Efficiency Tracker			Effi Track				Efficiency Tracker			Effi Track		
166		32			15			Benchmark Rectifier			Benchmark Rect			Benchmark Rectifier			Benchmark Rect	
167		32			15			R48-3500e3					R48-3500e3				R48-3500e3					R48-3500e3		
168		32			15			R48-3200					R48-3200				R48-3200					R48-3200		
169		32			15			96%							96%						96%							96%				
170		32			15			95%							95%						95%							95%				
171		32			15			94%							94%						94%							94%				
172		32			15			93%							93%						93%							93%				
173		32			15			92%							92%						92%							92%				
174		32			15			91%							91%						91%							91%				
175		32			15			90%							90%						90%							90%				
176		32			15			Cost per kWh				Cost per kWh			Cost per kWh				Cost per kWh	
177		32			15			Currency					Currency				Currency					Currency		
178		32			15			USD							USD						USD							USD				
179		32			15			CNY							CNY						CNY							CNY				
180		32			15			EUR							EUR						EUR							EUR				
181		32			15			GBP							GBP						GBP							GBP				
182		32			15			RUB							RUB						RUB							RUB				
183		32			15			Percentage 98% Rectifiers	Percent98%Rect			Percentage 98% Rectifiers	Percent98%Rect	
184		32			15			10%							10%						10%							10%				
185		32			15			20%							20%						20%							20%				
186		32			15			30%							30%						30%							30%				
187		32			15			40%							40%						40%							40%				
188		32			15			50%							50%						50%							50%				
189		32			15			60%							60%						60%							60%				
190		32			15			70%							70%						70%							70%				
191		32			15			80%							80%						80%							80%				
192		32			15			90%							90%						90%							90%				
193		32			15			100%						100%					100%						100%			
																					
200		32			15			Default Voltage			Default Voltage				Default Voltage			Default Voltage		
