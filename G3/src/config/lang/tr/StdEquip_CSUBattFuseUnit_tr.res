#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
tr

[RES_INFO]																	
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE				
1		32			15			Battery Fuse Tripped			Batt Fuse Trip		Aku Sigortasi Atik		Aku Sig.1 Atik 			
2		32			15			Battery 2 fuse tripped			Batt2 Fuse Trip		Aku Sigortasi 2 Atik		Aku Sig.2 Atik 			
3		32			15			Battery 1 Voltage			Batt 1 Voltage		Aku Gerilimi 1		Aku Gerilimi 1		
4		32			15			Battery 1 Current			Batt 1 Current		Aku Akimi 1		Aku Akimi 1		
5		32			15			Battery 1 Temperature			Batt 1 Temp		Aku Sicakligi 1		Aku Sic.1		
6		32			15			Battery 2 Voltage			Batt 2 Voltage		Aku Gerilimi 2		Aku Gerilimi 2		
7		32			15			Battery 2 Current			Batt 2 Current		Aku Akimi 2		Aku Akimi 2		
8		32			15			Battery 2 Temperature			Batt 2 Temp		Aku Sicakligi 2		Aku Sic. 2		
9		32			15			CSU Battery				CSU Battery		CSU Aku			CSU Aku
10		32			15			CSU Battery Failure			CSU BattFailure		CSU Aku Arizasi		CSU Aku Arizasi		
11		32			15			No					No			Hayir			Hayir	
12		32			15			Yes					Yes			Evet			Evet	
13		32			15			Battery 2 Connected			Batt2 Connected		Aku 2 Baglandi		Aku 2 Baglandi		
14		32			15			Battery 1 Connected			Batt1 Connected		Aku 1 Baglandi		Aku 1 Baglandi		
15		32			15			Existent				Existent		Mevcut			Mevcut
16		32			15			Non-Existent				Non-Existent		Mevcut Degil		Mevcut Degil

