﻿#
# Locale language support: tr
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		Bara Gerilimi		Bara Gerilimi
2		32			15			Load 1 Current				Load 1 Current		Yuk 1 Akimi		Yuk 1 Akim
3		32			15			Load 2 Current				Load 2 Current		Yuk 2 Akimi		Yuk 2 Akim
4		32			15			Load 3 Current				Load 3 Current		Yuk 3 Akimi		Yuk 3 Akim
5		32			15			Load 4 Current				Load 4 Current		Yuk 4 Akimi		Yuk 4 Akim
6		32			15			Load 5 Current				Load 5 Current		Yuk 5 Akimi		Yuk 5 Akim
7		32			15			Load 6 Current				Load 6 Current		Yuk 6 Akimi		Yuk 6 Akim
8		32			15			Load 7 Current				Load 7 Current		Yuk 7 Akimi		Yuk 7 Akim
9		32			15			Load 8 Current				Load 8 Current		Yuk 8 Akimi		Yuk 8 Akim
10		32			15			Load 9 Current				Load 9 Current		Yuk 9 Akimi		Yuk 9 Akim
11		32			15			Load 10 Current				Load 10 Current		Yuk 10 Akimi		Yuk 10 Akim
12		32			15			Load 11 Current				Load 11 Current		Yuk 11 Akimi		Yuk 11 Akim
13		32			15			Load 12 Current				Load 12 Current		Yuk 12 Akimi		Yuk 12 Akim
14		32			15			Load 13 Current				Load 13 Current		Yuk 13 Akimi		Yuk 13 Akim
15		32			15			Load 14 Current				Load 14 Current		Yuk 14 Akimi		Yuk 14 Akim
16		32			15			Load 15 Current				Load 15 Current		Yuk 15 Akimi		Yuk 15 Akim
17		32			15			Load 16 Current				Load 16 Current		Yuk 16 Akimi		Yuk 16 Akim
18		32			15			Load 17 Current				Load 17 Current		Yuk 17 Akimi		Yuk 17 Akim
19		32			15			Load 18 Current				Load 18 Current		Yuk 18 Akimi		Yuk 18 Akim
20		32			15			Load 19 Current				Load 19 Current		Yuk 19 Akimi		Yuk 19 Akim
21		32			15			Load 20 Current				Load 20 Current		Yuk 20 Akimi		Yuk 20 Akim
22		32			15			Load 21 Current				Load 21 Current		Yuk 21 Akimi		Yuk 21 Akim
23		32			15			Load 22 Current				Load 22 Current		Yuk 22 Akimi		Yuk 22 Akim
24		32			15			Load 23 Current				Load 23 Current		Yuk 23 Akimi		Yuk 23 Akim
25		32			15			Load 24 Current				Load 24 Current		Yuk 24 Akimi		Yuk 24 Akim
26		32			15			Load 25 Current				Load 25 Current		Yuk 25 Akimi		Yuk 25 Akim
27		32			15			Load 26 Current				Load 26 Current		Yuk 26 Akimi		Yuk 26 Akim
28		32			15			Load 27 Current				Load 27 Current		Yuk 27 Akimi		Yuk 27 Akim
29		32			15			Load 28 Current				Load 28 Current		Yuk 28 Akimi		Yuk 28 Akim
30		32			15			Load 29 Current				Load 29 Current		Yuk 29 Akimi		Yuk 29 Akim
31		32			15			Load 30 Current				Load 30 Current		Yuk 30 Akimi		Yuk 30 Akim
32		32			15			Load 31 Current				Load 31 Current		Yuk 31 Akimi		Yuk 31 Akim
33		32			15			Load 32 Current				Load 32 Current		Yuk 32 Akimi		Yuk 32 Akim
34		32			15			Load 33 Current				Load 33 Current		Yuk 33 Akimi		Yuk 33 Akim
35		32			15			Load 34 Current				Load 34 Current		Yuk 34 Akimi		Yuk 34 Akim
36		32			15			Load 35 Current				Load 35 Current		Yuk 35 Akimi		Yuk 35 Akim
37		32			15			Load 36 Current				Load 36 Current		Yuk 36 Akimi		Yuk 36 Akim
38		32			15			Load 37 Current				Load 37 Current		Yuk 37 Akimi		Yuk 37 Akim
39		32			15			Load 38 Current				Load 38 Current		Yuk 38 Akimi		Yuk 38 Akim
40		32			15			Load 39 Current				Load 39 Current		Yuk 39 Akimi		Yuk 39 Akim
41		32			15			Load 40 Current				Load 40 Current		Yuk 40 Akimi		Yuk 40 Akim

42		32			15			Power1					Power1			Guc 1			Guc 1
43		32			15			Power2					Power2			Guc 2			Guc 2
44		32			15			Power3					Power3			Guc 3			Guc 3
45		32			15			Power4					Power4			Guc 4			Guc 4
46		32			15			Power5					Power5			Guc 5			Guc 5
47		32			15			Power6					Power6			Guc 6			Guc 6
48		32			15			Power7					Power7			Guc 7			Guc 7
49		32			15			Power8					Power8			Guc 8			Guc 8
50		32			15			Power9					Power9			Guc 9			Guc 9
51		32			15			Power10					Power10			Guc 10			Guc 10
52		32			15			Power11					Power11			Guc 11			Guc 11
53		32			15			Power12					Power12			Guc 12			Guc 12
54		32			15			Power13					Power13			Guc 13			Guc 13
55		32			15			Power14					Power14			Guc 14			Guc 14
56		32			15			Power15					Power15			Guc 15			Guc 15
57		32			15			Power16					Power16			Guc 16			Guc 16
58		32			15			Power17					Power17			Guc 17			Guc 17
59		32			15			Power18					Power18			Guc 18			Guc 18
60		32			15			Power19					Power19			Guc 19			Guc 19
61		32			15			Power20					Power20			Guc 20			Guc 20
62		32			15			Power21					Power21			Guc 21			Guc 21	
63		32			15			Power22					Power22			Guc 22			Guc 22	
64		32			15			Power23					Power23			Guc 23			Guc 23	
65		32			15			Power24					Power24			Guc 24			Guc 24	
66		32			15			Power25					Power25			Guc 25			Guc 25	
67		32			15			Power26					Power26			Guc 26			Guc 26	
68		32			15			Power27					Power27			Guc 27			Guc 27	
69		32			15			Power28					Power28			Guc 28			Guc 28	
70		32			15			Power29					Power29			Guc 29			Guc 29	
71		32			15			Power30					Power30			Guc 30			Guc 30	
72		32			15			Power31					Power31			Guc 31			Guc 31	
73		32			15			Power32					Power32			Guc 32			Guc 32	
74		32			15			Power33					Power33			Guc 33			Guc 33	
75		32			15			Power34					Power34			Guc 34			Guc 34	
76		32			15			Power35					Power35			Guc 35			Guc 35	
77		32			15			Power36					Power36			Guc 36			Guc 36	
78		32			15			Power37					Power37			Guc 37			Guc 37	
79		32			15			Power38					Power38			Guc 38			Guc 38	
80		32			15			Power39					Power39			Guc 39			Guc 39	
81		32			15			Power40					Power40			Guc 40			Guc 40	

82		32			15			Energy Yesterday in Channel 1		CH1EnergyYTD		Kanal 1 Dünkü Enerji		K1 Dünkü Enerji
83		32			15			Energy Yesterday in Channel 2		CH2EnergyYTD		Kanal 2 Dünkü Enerji		K2 Dünkü Enerji
84		32			15			Energy Yesterday in Channel 3		CH3EnergyYTD		Kanal 3 Dünkü Enerji		K3 Dünkü Enerji
85		32			15			Energy Yesterday in Channel 4		CH4EnergyYTD		Kanal 4 Dünkü Enerji		K4 Dünkü Enerji
86		32			15			Energy Yesterday in Channel 5		CH5EnergyYTD		Kanal 5 Dünkü Enerji		K5 Dünkü Enerji
87		32			15			Energy Yesterday in Channel 6		CH6EnergyYTD		Kanal 6 Dünkü Enerji		K6 Dünkü Enerji
88		32			15			Energy Yesterday in Channel 7		CH7EnergyYTD		Kanal 7 Dünkü Enerji		K7 Dünkü Enerji
89		32			15			Energy Yesterday in Channel 8		CH8EnergyYTD		Kanal 8 Dünkü Enerji		K8 Dünkü Enerji
90		32			15			Energy Yesterday in Channel 9		CH9EnergyYTD		Kanal 9 Dünkü Enerji		K9 Dünkü Enerji
91		32			15			Energy Yesterday in Channel 10		CH10EnergyYTD		Kanal 10 Dünkü Enerji		K10 Dünkü Enerji
92		32			15			Energy Yesterday in Channel 11		CH11EnergyYTD		Kanal 11 Dünkü Enerji		K11 Dünkü Enerji
93		32			15			Energy Yesterday in Channel 12		CH12EnergyYTD		Kanal 12 Dünkü Enerji		K12 Dünkü Enerji
94		32			15			Energy Yesterday in Channel 13		CH13EnergyYTD		Kanal 13 Dünkü Enerji		K13 Dünkü Enerji
95		32			15			Energy Yesterday in Channel 14		CH14EnergyYTD		Kanal 14 Dünkü Enerji		K14 Dünkü Enerji
96		32			15			Energy Yesterday in Channel 15		CH15EnergyYTD		Kanal 15 Dünkü Enerji		K15 Dünkü Enerji
97		32			15			Energy Yesterday in Channel 16		CH16EnergyYTD		Kanal 16 Dünkü Enerji		K16 Dünkü Enerji
98		32			15			Energy Yesterday in Channel 17		CH17EnergyYTD		Kanal 17 Dünkü Enerji		K17 Dünkü Enerji
99		32			15			Energy Yesterday in Channel 18		CH18EnergyYTD		Kanal 18 Dünkü Enerji		K18 Dünkü Enerji
100		32			15			Energy Yesterday in Channel 19		CH19EnergyYTD		Kanal 19 Dünkü Enerji		K19 Dünkü Enerji
101		32			15			Energy Yesterday in Channel 20		CH20EnergyYTD		Kanal 20 Dünkü Enerji		K20 Dünkü Enerji
102		32			15			Energy Yesterday in Channel 21		CH21EnergyYTD		Kanal 21 Dünkü Enerji		K21 Dünkü Enerji
103		32			15			Energy Yesterday in Channel 22		CH22EnergyYTD		Kanal 22 Dünkü Enerji		K22 Dünkü Enerji
104		32			15			Energy Yesterday in Channel 23		CH23EnergyYTD		Kanal 23 Dünkü Enerji		K23 Dünkü Enerji
105		32			15			Energy Yesterday in Channel 24		CH24EnergyYTD		Kanal 24 Dünkü Enerji		K24 Dünkü Enerji
106		32			15			Energy Yesterday in Channel 25		CH25EnergyYTD		Kanal 25 Dünkü Enerji		K25 Dünkü Enerji
107		32			15			Energy Yesterday in Channel 26		CH26EnergyYTD		Kanal 26 Dünkü Enerji		K26 Dünkü Enerji
108		32			15			Energy Yesterday in Channel 27		CH27EnergyYTD		Kanal 27 Dünkü Enerji		K27 Dünkü Enerji
109		32			15			Energy Yesterday in Channel 28		CH28EnergyYTD		Kanal 28 Dünkü Enerji		K28 Dünkü Enerji
110		32			15			Energy Yesterday in Channel 29		CH29EnergyYTD		Kanal 29 Dünkü Enerji		K29 Dünkü Enerji
111		32			15			Energy Yesterday in Channel 30		CH30EnergyYTD		Kanal 30 Dünkü Enerji		K30 Dünkü Enerji
112		32			15			Energy Yesterday in Channel 31		CH31EnergyYTD		Kanal 31 Dünkü Enerji		K31 Dünkü Enerji
113		32			15			Energy Yesterday in Channel 32		CH32EnergyYTD		Kanal 32 Dünkü Enerji		K32 Dünkü Enerji
114		32			15			Energy Yesterday in Channel 33		CH33EnergyYTD		Kanal 33 Dünkü Enerji		K33 Dünkü Enerji
115		32			15			Energy Yesterday in Channel 34		CH34EnergyYTD		Kanal 34 Dünkü Enerji		K34 Dünkü Enerji
116		32			15			Energy Yesterday in Channel 35		CH35EnergyYTD		Kanal 35 Dünkü Enerji		K35 Dünkü Enerji
117		32			15			Energy Yesterday in Channel 36		CH36EnergyYTD		Kanal 36 Dünkü Enerji		K36 Dünkü Enerji
118		32			15			Energy Yesterday in Channel 37		CH37EnergyYTD		Kanal 37 Dünkü Enerji		K37 Dünkü Enerji
119		32			15			Energy Yesterday in Channel 38		CH38EnergyYTD		Kanal 38 Dünkü Enerji		K38 Dünkü Enerji
120		32			15			Energy Yesterday in Channel 39		CH39EnergyYTD		Kanal 39 Dünkü Enerji		K39 Dünkü Enerji
121		32			15			Energy Yesterday in Channel 40		CH40EnergyYTD		Kanal 40 Dünkü Enerji		K40 Dünkü Enerji

122		32			15			Total Energy in Channel 1		CH1TotalEnergy		Kanal 1 Toplam Enerji		K1 Toplam Enerji
123		32			15			Total Energy in Channel 2		CH2TotalEnergy		Kanal 2 Toplam Enerji		K2 Toplam Enerji
124		32			15			Total Energy in Channel 3		CH3TotalEnergy		Kanal 3 Toplam Enerji		K3 Toplam Enerji
125		32			15			Total Energy in Channel 4		CH4TotalEnergy		Kanal 4 Toplam Enerji		K4 Toplam Enerji
126		32			15			Total Energy in Channel 5		CH5TotalEnergy		Kanal 5 Toplam Enerji		K5 Toplam Enerji
127		32			15			Total Energy in Channel 6		CH6TotalEnergy		Kanal 6 Toplam Enerji		K6 Toplam Enerji
128		32			15			Total Energy in Channel 7		CH7TotalEnergy		Kanal 7 Toplam Enerji		K7 Toplam Enerji
129		32			15			Total Energy in Channel 8		CH8TotalEnergy		Kanal 8 Toplam Enerji		K8 Toplam Enerji
130		32			15			Total Energy in Channel 9		CH9TotalEnergy		Kanal 9 Toplam Enerji		K9 Toplam Enerji
131		32			15			Total Energy in Channel 10		CH10TotalEnergy		Kanal 10 Toplam Enerji		K10 Toplam Enerji
132		32			15			Total Energy in Channel 11		CH11TotalEnergy		Kanal 11 Toplam Enerji		K11 Toplam Enerji
133		32			15			Total Energy in Channel 12		CH12TotalEnergy		Kanal 12 Toplam Enerji		K12 Toplam Enerji
134		32			15			Total Energy in Channel 13		CH13TotalEnergy		Kanal 13 Toplam Enerji		K13 Toplam Enerji
135		32			15			Total Energy in Channel 14		CH14TotalEnergy		Kanal 14 Toplam Enerji		K14 Toplam Enerji
136		32			15			Total Energy in Channel 15		CH15TotalEnergy		Kanal 15 Toplam Enerji		K15 Toplam Enerji
137		32			15			Total Energy in Channel 16		CH16TotalEnergy		Kanal 16 Toplam Enerji		K16 Toplam Enerji
138		32			15			Total Energy in Channel 17		CH17TotalEnergy		Kanal 17 Toplam Enerji		K17 Toplam Enerji
139		32			15			Total Energy in Channel 18		CH18TotalEnergy		Kanal 18 Toplam Enerji		K18 Toplam Enerji
140		32			15			Total Energy in Channel 19		CH19TotalEnergy		Kanal 19 Toplam Enerji		K19 Toplam Enerji
141		32			15			Total Energy in Channel 20		CH20TotalEnergy		Kanal 20 Toplam Enerji		K20 Toplam Enerji
142		32			15			Total Energy in Channel 21		CH21TotalEnergy		Kanal 21 Toplam Enerji		K21 Toplam Enerji
143		32			15			Total Energy in Channel 22		CH22TotalEnergy		Kanal 22 Toplam Enerji		K22 Toplam Enerji
144		32			15			Total Energy in Channel 23		CH23TotalEnergy		Kanal 23 Toplam Enerji		K23 Toplam Enerji
145		32			15			Total Energy in Channel 24		CH24TotalEnergy		Kanal 24 Toplam Enerji		K24 Toplam Enerji
146		32			15			Total Energy in Channel 25		CH25TotalEnergy		Kanal 25 Toplam Enerji		K25 Toplam Enerji
147		32			15			Total Energy in Channel 26		CH26TotalEnergy		Kanal 26 Toplam Enerji		K26 Toplam Enerji
148		32			15			Total Energy in Channel 27		CH27TotalEnergy		Kanal 27 Toplam Enerji		K27 Toplam Enerji
149		32			15			Total Energy in Channel 28		CH28TotalEnergy		Kanal 28 Toplam Enerji		K28 Toplam Enerji
150		32			15			Total Energy in Channel 29		CH29TotalEnergy		Kanal 29 Toplam Enerji		K29 Toplam Enerji
151		32			15			Total Energy in Channel 30		CH30TotalEnergy		Kanal 30 Toplam Enerji		K30 Toplam Enerji
152		32			15			Total Energy in Channel 31		CH31TotalEnergy		Kanal 31 Toplam Enerji		K31 Toplam Enerji
153		32			15			Total Energy in Channel 32		CH32TotalEnergy		Kanal 32 Toplam Enerji		K32 Toplam Enerji
154		32			15			Total Energy in Channel 33		CH33TotalEnergy		Kanal 33 Toplam Enerji		K33 Toplam Enerji
155		32			15			Total Energy in Channel 34		CH34TotalEnergy		Kanal 34 Toplam Enerji		K34 Toplam Enerji
156		32			15			Total Energy in Channel 35		CH35TotalEnergy		Kanal 35 Toplam Enerji		K35 Toplam Enerji
157		32			15			Total Energy in Channel 36		CH36TotalEnergy		Kanal 36 Toplam Enerji		K36 Toplam Enerji
158		32			15			Total Energy in Channel 37		CH37TotalEnergy		Kanal 37 Toplam Enerji		K37 Toplam Enerji
159		32			15			Total Energy in Channel 38		CH38TotalEnergy		Kanal 38 Toplam Enerji		K38 Toplam Enerji
160		32			15			Total Energy in Channel 39		CH39TotalEnergy		Kanal 39 Toplam Enerji		K39 Toplam Enerji
161		32			15			Total Energy in Channel 40		CH40TotalEnergy		Kanal 40 Toplam Enerji		K40 Toplam Enerji

162		32			15			Load1 Alarm Flag			Load1 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
163		32			15			Load2 Alarm Flag			Load2 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
164		32			15			Load3 Alarm Flag			Load3 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
165		32			15			Load4 Alarm Flag			Load4 Alarm Flag	Load4 Alarm Flag			Load4 Alarm Flag
166		32			15			Load5 Alarm Flag			Load5 Alarm Flag	Load5 Alarm Flag			Load5 Alarm Flag
167		32			15			Load6 Alarm Flag			Load6 Alarm Flag	Load6 Alarm Flag			Load6 Alarm Flag
168		32			15			Load7 Alarm Flag			Load7 Alarm Flag	Load7 Alarm Flag			Load7 Alarm Flag
169		32			15			Load8 Alarm Flag			Load8 Alarm Flag	Load8 Alarm Flag			Load8 Alarm Flag
170		32			15			Load9 Alarm Flag			Load9 Alarm Flag	Load9 Alarm Flag			Load9 Alarm Flag
171		32			15			Load10 Alarm Flag			Load10 Alarm Flag	Load10 Alarm Flag			Load10 Alarm Flag
172		32			15			Load11 Alarm Flag			Load11 Alarm Flag	Load11 Alarm Flag			Load11 Alarm Flag
173		32			15			Load12 Alarm Flag			Load12 Alarm Flag	Load12 Alarm Flag			Load12 Alarm Flag
174		32			15			Load13 Alarm Flag			Load13 Alarm Flag	Load13 Alarm Flag			Load13 Alarm Flag
175		32			15			Load14 Alarm Flag			Load14 Alarm Flag	Load14 Alarm Flag			Load14 Alarm Flag
176		32			15			Load15 Alarm Flag			Load15 Alarm Flag	Load15 Alarm Flag			Load15 Alarm Flag
177		32			15			Load16 Alarm Flag			Load16 Alarm Flag	Load16 Alarm Flag			Load16 Alarm Flag
178		32			15			Load17 Alarm Flag			Load17 Alarm Flag	Load17 Alarm Flag			Load17 Alarm Flag
179		32			15			Load18 Alarm Flag			Load18 Alarm Flag	Load18 Alarm Flag			Load18 Alarm Flag
180		32			15			Load19 Alarm Flag			Load19 Alarm Flag	Load19 Alarm Flag			Load19 Alarm Flag
181		32			15			Load20 Alarm Flag			Load20 Alarm Flag	Load20 Alarm Flag			Load20 Alarm Flag
182		32			15			Load21 Alarm Flag			Load21 Alarm Flag	Load21 Alarm Flag			Load21 Alarm Flag
183		32			15			Load22 Alarm Flag			Load22 Alarm Flag	Load22 Alarm Flag			Load22 Alarm Flag
184		32			15			Load23 Alarm Flag			Load23 Alarm Flag	Load23 Alarm Flag			Load23 Alarm Flag
185		32			15			Load24 Alarm Flag			Load24 Alarm Flag	Load24 Alarm Flag			Load24 Alarm Flag
186		32			15			Load25 Alarm Flag			Load25 Alarm Flag	Load25 Alarm Flag			Load25 Alarm Flag
187		32			15			Load26 Alarm Flag			Load26 Alarm Flag	Load26 Alarm Flag			Load26 Alarm Flag
188		32			15			Load27 Alarm Flag			Load27 Alarm Flag	Load27 Alarm Flag			Load27 Alarm Flag
189		32			15			Load28 Alarm Flag			Load28 Alarm Flag	Load28 Alarm Flag			Load28 Alarm Flag
190		32			15			Load29 Alarm Flag			Load29 Alarm Flag	Load29 Alarm Flag			Load29 Alarm Flag
191		32			15			Load30 Alarm Flag			Load30 Alarm Flag	Load30 Alarm Flag			Load30 Alarm Flag
192		32			15			Load31 Alarm Flag			Load31 Alarm Flag	Load31 Alarm Flag			Load31 Alarm Flag
193		32			15			Load32 Alarm Flag			Load32 Alarm Flag	Load32 Alarm Flag			Load32 Alarm Flag
194		32			15			Load33 Alarm Flag			Load33 Alarm Flag	Load33 Alarm Flag			Load33 Alarm Flag
195		32			15			Load34 Alarm Flag			Load34 Alarm Flag	Load34 Alarm Flag			Load34 Alarm Flag
196		32			15			Load35 Alarm Flag			Load35 Alarm Flag	Load35 Alarm Flag			Load35 Alarm Flag
197		32			15			Load36 Alarm Flag			Load36 Alarm Flag	Load36 Alarm Flag			Load36 Alarm Flag
198		32			15			Load37 Alarm Flag			Load37 Alarm Flag	Load37 Alarm Flag			Load37 Alarm Flag
199		32			15			Load38 Alarm Flag			Load38 Alarm Flag	Load38 Alarm Flag			Load38 Alarm Flag
200		32			15			Load39 Alarm Flag			Load39 Alarm Flag	Load39 Alarm Flag			Load39 Alarm Flag
201		32			15			Load40 Alarm Flag			Load40 Alarm Flag	Load40 Alarm Flag			Load40 Alarm Flag

202		32			15			Bus Voltage Alarm			BusVolt Alarm		Bara Gerilim Durumu		Bara Ger. Durum
203		32			15			SMDUHH Fault				SMDUHH Fault		SMDUHH Durumu		SMDUHH Durumu
204		32			15			Barcode					Barcode			Barkod					Barkod
205		32			15			Times of Communication Fail		Times Comm Fail		Haberlesme Hata Zamani		Hab. Hata Zamani
206		32			15			Existence State				Existence State		Varlik Durumu		Varlik Durumu

207		32			15			Reset Energy Channel X			RstEnergyChanX		X Kanal Enerj Reset	 X K. Enerj Reset


208		32			15			Hall Calibrate Channel			CalibrateChan		Hol Kalibre Kanali	Hol Kalibre Kanal
209		32			15			Hall Calibrate Point 1			HallCalibrate1		Hol Kalibre Noktasi 1		Hol Kali. Nok.1
210		32			15			Hall Calibrate Point 2			HallCalibrate2		Hol Kalibre Noktasi 2		Hol Kali. Nok.2
#211		32			15			Dev1 Hall Coeff				Dev1 HallCoeff			Hol Katsayisi 1		Hol Katsayisi1
#212		32			15			Dev2 Hall Coeff				Dev2 HallCoeff			Hol Katsayisi 2		Hol Katsayisi2
#213		32			15			Dev3 Hall Coeff				Dev3 HallCoeff			Hol Katsayisi 3		Hol Katsayisi3
#214		32			15			Dev4 Hall Coeff				Dev4 HallCoeff			Hol Katsayisi 4		Hol Katsayisi4
#215		32			15			Dev5 Hall Coeff				Dev5 HallCoeff			Hol Katsayisi 5		Hol Katsayisi5
#216		32			15			Dev6 Hall Coeff				Dev6 HallCoeff			Hol Katsayisi 6		Hol Katsayisi6
#217		32			15			Dev7 Hall Coeff				Dev7 HallCoeff			Hol Katsayisi 7		Hol Katsayisi7
#218		32			15			Dev8 Hall Coeff				Dev8 HallCoeff			Hol Katsayisi 8		Hol Katsayisi8
#219		32			15			Dev9 Hall Coeff				Dev9 HallCoeff			Hol Katsayisi 9		Hol Katsayisi9
#220		32			15			Dev10 Hall Coeff			Dev10 HallCoeff			Hol Katsayisi 10		Hol Katsayisi10
#221		32			15			Dev11 Hall Coeff			Dev11 HallCoeff			Hol Katsayisi 11		Hol Katsayisi11
#222		32			15			Dev12 Hall Coeff			Dev12 HallCoeff			Hol Katsayisi 12		Hol Katsayisi12
#223		32			15			Dev13 Hall Coeff			Dev13 HallCoeff			Hol Katsayisi 13		Hol Katsayisi13
#224		32			15			Dev14 Hall Coeff			Dev14 HallCoeff			Hol Katsayisi 14		Hol Katsayisi14
#225		32			15			Dev15 Hall Coeff			Dev15 HallCoeff			Hol Katsayisi 15		Hol Katsayisi15
#226		32			15			Dev16 Hall Coeff			Dev16 HallCoeff			Hol Katsayisi 16		Hol Katsayisi16
#227		32			15			Dev17 Hall Coeff			Dev17 HallCoeff			Hol Katsayisi 17		Hol Katsayisi17
#228		32			15			Dev18 Hall Coeff			Dev18 HallCoeff			Hol Katsayisi 18		Hol Katsayisi18
#229		32			15			Dev19 Hall Coeff			Dev19 HallCoeff			Hol Katsayisi 19		Hol Katsayisi19
#230		32			15			Dev20 Hall Coeff			Dev20 HallCoeff			Hol Katsayisi 20		Hol Katsayisi20
#231		32			15			Dev21 Hall Coeff			Dev21 HallCoeff			Hol Katsayisi 21		Hol Katsayisi21
#232		32			15			Dev22 Hall Coeff			Dev22 HallCoeff			Hol Katsayisi 22		Hol Katsayisi22
#233		32			15			Dev23 Hall Coeff			Dev23 HallCoeff			Hol Katsayisi 23		Hol Katsayisi23
#234		32			15			Dev24 Hall Coeff			Dev24 HallCoeff			Hol Katsayisi 24		Hol Katsayisi24
#235		32			15			Dev25 Hall Coeff			Dev25 HallCoeff			Hol Katsayisi 25		Hol Katsayisi25
#236		32			15			Dev26 Hall Coeff			Dev26 HallCoeff			Hol Katsayisi 26		Hol Katsayisi26
#237		32			15			Dev27 Hall Coeff			Dev27 HallCoeff			Hol Katsayisi 27		Hol Katsayisi27
#238		32			15			Dev28 Hall Coeff			Dev28 HallCoeff			Hol Katsayisi 28		Hol Katsayisi28
#239		32			15			Dev29 Hall Coeff			Dev29 HallCoeff			Hol Katsayisi 29		Hol Katsayisi29
#240		32			15			Dev30 Hall Coeff			Dev30 HallCoeff			Hol Katsayisi 30		Hol Katsayisi30
#241		32			15			Dev31 Hall Coeff			Dev31 HallCoeff			Hol Katsayisi 31		Hol Katsayisi31
#242		32			15			Dev32 Hall Coeff			Dev32 HallCoeff			Hol Katsayisi 32		Hol Katsayisi32
#243		32			15			Dev33 Hall Coeff			Dev33 HallCoeff			Hol Katsayisi 33		Hol Katsayisi33
#244		32			15			Dev34 Hall Coeff			Dev34 HallCoeff			Hol Katsayisi 34		Hol Katsayisi34
#245		32			15			Dev35 Hall Coeff			Dev35 HallCoeff			Hol Katsayisi 35		Hol Katsayisi35
#246		32			15			Dev36 Hall Coeff			Dev36 HallCoeff			Hol Katsayisi 36		Hol Katsayisi36
#247		32			15			Dev37 Hall Coeff			Dev37 HallCoeff			Hol Katsayisi 37		Hol Katsayisi37
#248		32			15			Dev38 Hall Coeff			Dev38 HallCoeff			Hol Katsayisi 38		Hol Katsayisi38
#249		32			15			Dev39 Hall Coeff			Dev39 HallCoeff			Hol Katsayisi 39		Hol Katsayisi39
#250		32			15			Dev40 Hall Coeff			Dev40 HallCoeff			Hol Katsayisi 40		Hol Katsayisi40

211		32			15			Communication Fail			Comm Fail		Haberlesme Hatasi		Haber. Hatasi
212		32			15			Current1 High Current			Curr 1 Hi		Current1 High Current			Curr 1 Hi	
213		32			15			Current1 Very High Current		Curr 1 Very Hi		Current1 Very High Current		Curr 1 Very Hi	
214		32			15			Current2 High Current			Curr 2 Hi		Current2 High Current			Curr 2 Hi	
215		32			15			Current2 Very High Current		Curr 2 Very Hi		Current2 Very High Current		Curr 2 Very Hi	
216		32			15			Current3 High Current			Curr 3 Hi		Current3 High Current			Curr 3 Hi	
217		32			15			Current3 Very High Current		Curr 3 Very Hi		Current3 Very High Current		Curr 3 Very Hi	
218		32			15			Current4 High Current			Curr 4 Hi		Current4 High Current			Curr 4 Hi	
219		32			15			Current4 Very High Current		Curr 4 Very Hi		Current4 Very High Current		Curr 4 Very Hi	
220		32			15			Current5 High Current			Curr 5 Hi		Current5 High Current			Curr 5 Hi	
221		32			15			Current5 Very High Current		Curr 5 Very Hi		Current5 Very High Current		Curr 5 Very Hi	
222		32			15			Current6 High Current			Curr 6 Hi		Current6 High Current			Curr 6 Hi	
223		32			15			Current6 Very High Current		Curr 6 Very Hi		Current6 Very High Current		Curr 6 Very Hi	
224		32			15			Current7 High Current			Curr 7 Hi		Current7 High Current			Curr 7 Hi	
225		32			15			Current7 Very High Current		Curr 7 Very Hi		Current7 Very High Current		Curr 7 Very Hi	
226		32			15			Current8 High Current			Curr 8 Hi		Current8 High Current			Curr 8 Hi	
227		32			15			Current8 Very High Current		Curr 8 Very Hi		Current8 Very High Current		Curr 8 Very Hi	
228		32			15			Current9 High Current			Curr 9 Hi		Current9 High Current			Curr 9 Hi	
229		32			15			Current9 Very High Current		Curr 9 Very Hi		Current9 Very High Current		Curr 9 Very Hi	
230		32			15			Current10 High Current			Curr 10 Hi		Current10 High Current			Curr 10 Hi	
231		32			15			Current10 Very High Current		Curr 10 Very Hi		Current10 Very High Current		Curr 10 Very Hi	
232		32			15			Current11 High Current			Curr 11 Hi		Current11 High Current			Curr 11 Hi	
233		32			15			Current11 Very High Current		Curr 11 Very Hi		Current11 Very High Current		Curr 11 Very Hi	
234		32			15			Current12 High Current			Curr 12 Hi		Current12 High Current			Curr 12 Hi	
235		32			15			Current12 Very High Current		Curr 12 Very Hi		Current12 Very High Current		Curr 12 Very Hi	
236		32			15			Current13 High Current			Curr 13 Hi		Current13 High Current			Curr 13 Hi	
237		32			15			Current13 Very High Current		Curr 13 Very Hi		Current13 Very High Current		Curr 13 Very Hi	
238		32			15			Current14 High Current			Curr 14 Hi		Current14 High Current			Curr 14 Hi	
239		32			15			Current14 Very High Current		Curr 14 Very Hi		Current14 Very High Current		Curr 14 Very Hi	
240		32			15			Current15 High Current			Curr 15 Hi		Current15 High Current			Curr 15 Hi	
241		32			15			Current15 Very High Current		Curr 15 Very Hi		Current15 Very High Current		Curr 15 Very Hi	
242		32			15			Current16 High Current			Curr 16 Hi		Current16 High Current			Curr 16 Hi	
243		32			15			Current16 Very High Current		Curr 16 Very Hi		Current16 Very High Current		Curr 16 Very Hi	
244		32			15			Current17 High Current			Curr 17 Hi		Current17 High Current			Curr 17 Hi	
245		32			15			Current17 Very High Current		Curr 17 Very Hi		Current17 Very High Current		Curr 17 Very Hi	
246		32			15			Current18 High Current			Curr 18 Hi		Current18 High Current			Curr 18 Hi	
247		32			15			Current18 Very High Current		Curr 18 Very Hi		Current18 Very High Current		Curr 18 Very Hi	
248		32			15			Current19 High Current			Curr 19 Hi		Current19 High Current			Curr 19 Hi	
249		32			15			Current19 Very High Current		Curr 19 Very Hi		Current19 Very High Current		Curr 19 Very Hi	
250		32			15			Current20 High Current			Curr 20 Hi		Current20 High Current			Curr 20 Hi	
251		32			15			Current20 Very High Current		Curr 20 Very Hi		Current20 Very High Current		Curr 20 Very Hi	
252		32			15			Current21 High Current			Curr 21 Hi		Current21 High Current			Curr 21 Hi	
253		32			15			Current21 Very High Current		Curr 21 Very Hi		Current21 Very High Current		Curr 21 Very Hi	
254		32			15			Current22 High Current			Curr 22 Hi		Current22 High Current			Curr 22 Hi	
255		32			15			Current22 Very High Current		Curr 22 Very Hi		Current22 Very High Current		Curr 22 Very Hi	
256		32			15			Current23 High Current			Curr 23 Hi		Current23 High Current			Curr 23 Hi	
257		32			15			Current23 Very High Current		Curr 23 Very Hi		Current23 Very High Current		Curr 23 Very Hi	
258		32			15			Current24 High Current			Curr 24 Hi		Current24 High Current			Curr 24 Hi	
259		32			15			Current24 Very High Current		Curr 24 Very Hi		Current24 Very High Current		Curr 24 Very Hi	
260		32			15			Current25 High Current			Curr 25 Hi		Current25 High Current			Curr 25 Hi	
261		32			15			Current25 Very High Current		Curr 25 Very Hi		Current25 Very High Current		Curr 25 Very Hi	
262		32			15			Current26 High Current			Curr 26 Hi		Current26 High Current			Curr 26 Hi	
263		32			15			Current26 Very High Current		Curr 26 Very Hi		Current26 Very High Current		Curr 26 Very Hi	
264		32			15			Current27 High Current			Curr 27 Hi		Current27 High Current			Curr 27 Hi	
265		32			15			Current27 Very High Current		Curr 27 Very Hi		Current27 Very High Current		Curr 27 Very Hi	
266		32			15			Current28 High Current			Curr 28 Hi		Current28 High Current			Curr 28 Hi	
267		32			15			Current28 Very High Current		Curr 28 Very Hi		Current28 Very High Current		Curr 28 Very Hi	
268		32			15			Current29 High Current			Curr 29 Hi		Current29 High Current			Curr 29 Hi	
269		32			15			Current29 Very High Current		Curr 29 Very Hi		Current29 Very High Current		Curr 29 Very Hi	
270		32			15			Current30 High Current			Curr 30 Hi		Current30 High Current			Curr 30 Hi	
271		32			15			Current30 Very High Current		Curr 30 Very Hi		Current30 Very High Current		Curr 30 Very Hi	
272		32			15			Current31 High Current			Curr 31 Hi		Current31 High Current			Curr 31 Hi	
273		32			15			Current31 Very High Current		Curr 31 Very Hi		Current31 Very High Current		Curr 31 Very Hi	
274		32			15			Current32 High Current			Curr 32 Hi		Current32 High Current			Curr 32 Hi	
275		32			15			Current32 Very High Current		Curr 32 Very Hi		Current32 Very High Current		Curr 32 Very Hi	
276		32			15			Current33 High Current			Curr 33 Hi		Current33 High Current			Curr 33 Hi	
277		32			15			Current33 Very High Current		Curr 33 Very Hi		Current33 Very High Current		Curr 33 Very Hi	
278		32			15			Current34 High Current			Curr 34 Hi		Current34 High Current			Curr 34 Hi	
279		32			15			Current34 Very High Current		Curr 34 Very Hi		Current34 Very High Current		Curr 34 Very Hi	
280		32			15			Current35 High Current			Curr 35 Hi		Current35 High Current			Curr 35 Hi	
281		32			15			Current35 Very High Current		Curr 35 Very Hi		Current35 Very High Current		Curr 35 Very Hi	
282		32			15			Current36 High Current			Curr 36 Hi		Current36 High Current			Curr 36 Hi	
283		32			15			Current36 Very High Current		Curr 36 Very Hi		Current36 Very High Current		Curr 36 Very Hi	
284		32			15			Current37 High Current			Curr 37 Hi		Current37 High Current			Curr 37 Hi	
285		32			15			Current37 Very High Current		Curr 37 Very Hi		Current37 Very High Current		Curr 37 Very Hi	
286		32			15			Current38 High Current			Curr 38 Hi		Current38 High Current			Curr 38 Hi	
287		32			15			Current38 Very High Current		Curr 38 Very Hi		Current38 Very High Current		Curr 38 Very Hi	
288		32			15			Current39 High Current			Curr 39 Hi		Current39 High Current			Curr 39 Hi	
289		32			15			Current39 Very High Current		Curr 39 Very Hi		Current39 Very High Current		Curr 39 Very Hi	
290		32			15			Current40 High Current			Curr 40 Hi		Current40 High Current			Curr 40 Hi	
291		32			15			Current40 Very High Current		Curr 40 Very Hi		Current40 Very High Current		Curr 40 Very Hi	


292		32			15			Normal					Normal			Normal			Normal
293		32			15			Fail					Fail			Hata			Hata
294		32			15			Comm OK					Comm OK			Haberlesme OK		Haberlesme OK
295		32			15			All Batteries Comm Fail			AllBattCommFail		Tum Akuler Haberlesme Hatasi		Tum Aku Hab Hata
296		32			15			Communication Fail			Comm Fail		Haberlesme Hatasi		Haber. Hatasi
297		32			15			Existent				Existent		Mevcut			Mevcut
298		32			15			Not Existent				Not Existent		Mevcut Degil		Mevcut Degil
299		32			15			All Channels				All Channels		Tum Kanallar		Tum Kanallar

300		32			15			Channel 1				Channel 1		Kanal 1			Kanal 1
301		32			15			Channel 2				Channel 2		Kanal 2			Kanal 2
302		32			15			Channel 3				Channel 3		Kanal 3			Kanal 3
303		32			15			Channel 4				Channel 4		Kanal 4			Kanal 4
304		32			15			Channel 5				Channel 5		Kanal 5			Kanal 5
305		32			15			Channel 6				Channel 6		Kanal 6			Kanal 6
306		32			15			Channel 7				Channel 7		Kanal 7			Kanal 7
307		32			15			Channel 8				Channel 8		Kanal 8			Kanal 8
308		32			15			Channel 9				Channel 9		Kanal 9			Kanal 9
309		32			15			Channel 10				Channel 10		Kanal 10			Kanal 10
310		32			15			Channel 11				Channel 11		Kanal 11			Kanal 11
311		32			15			Channel 12				Channel 12		Kanal 12			Kanal 12
312		32			15			Channel 13				Channel 13		Kanal 13			Kanal 13
313		32			15			Channel 14				Channel 14		Kanal 14			Kanal 14
314		32			15			Channel 15				Channel 15		Kanal 15			Kanal 15
315		32			15			Channel 16				Channel 16		Kanal 16			Kanal 16
316		32			15			Channel 17				Channel 17		Kanal 17			Kanal 17
317		32			15			Channel 18				Channel 18		Kanal 18			Kanal 18
318		32			15			Channel 19				Channel 19		Kanal 19			Kanal 19
319		32			15			Channel 20				Channel 20		Kanal 20			Kanal 20
320		32			15			Channel 21				Channel 21		Kanal 21			Kanal 21
321		32			15			Channel 22				Channel 22		Kanal 22			Kanal 22
322		32			15			Channel 23				Channel 23		Kanal 23			Kanal 23
323		32			15			Channel 24				Channel 24		Kanal 24			Kanal 24
324		32			15			Channel 25				Channel 25		Kanal 25			Kanal 25
325		32			15			Channel 26				Channel 26		Kanal 26			Kanal 26
326		32			15			Channel 27				Channel 27		Kanal 27			Kanal 27
327		32			15			Channel 28				Channel 28		Kanal 28			Kanal 28
328		32			15			Channel 29				Channel 29		Kanal 29			Kanal 29
329		32			15			Channel 30				Channel 30		Kanal 30			Kanal 30
330		32			15			Channel 31				Channel 31		Kanal 31			Kanal 31
331		32			15			Channel 32				Channel 32		Kanal 32			Kanal 32
332		32			15			Channel 33				Channel 33		Kanal 33			Kanal 33
333		32			15			Channel 34				Channel 34		Kanal 34			Kanal 34
334		32			15			Channel 35				Channel 35		Kanal 35			Kanal 35
335		32			15			Channel 36				Channel 36		Kanal 36			Kanal 36
336		32			15			Channel 37				Channel 37		Kanal 37			Kanal 37
337		32			15			Channel 38				Channel 38		Kanal 38			Kanal 38
338		32			15			Channel 39				Channel 39		Kanal 39			Kanal 39
339		32			15			Channel 40				Channel 40		Kanal 40			Kanal 40

340		32			15			SMDUHH 1					SMDUHH 1			SMDUHH 1					SMDUHH 1

341		32			15			Dev1 Max Current			Dev1 Max Cur		Dev1 Mak Akım			Dev1 Mak Akım
342		32			15			Dev2 Max Current			Dev2 Max Cur		Dev2 Mak Akım			Dev2 Mak Akım
343		32			15			Dev3 Max Current			Dev3 Max Cur		Dev3 Mak Akım			Dev3 Mak Akım
344		32			15			Dev4 Max Current			Dev4 Max Cur		Dev4 Mak Akım			Dev4 Mak Akım
345		32			15			Dev5 Max Current			Dev5 Max Cur		Dev5 Mak Akım			Dev5 Mak Akım
346		32			15			Dev6 Max Current			Dev6 Max Cur		Dev6 Mak Akım			Dev6 Mak Akım
347		32			15			Dev7 Max Current			Dev7 Max Cur		Dev7 Mak Akım			Dev7 Mak Akım
348		32			15			Dev8 Max Current			Dev8 Max Cur		Dev8 Mak Akım			Dev8 Mak Akım
349		32			15			Dev9 Max Current			Dev9 Max Cur		Dev9 Mak Akım			Dev9 Mak Akım
350		32			15			Dev10 Max Current			Dev10 Max Cur		Dev10 Mak Akım			Dev10 Mak Akım
351		32			15			Dev11 Max Current			Dev11 Max Cur		Dev11 Mak Akım			Dev11 Mak Akım
352		32			15			Dev12 Max Current			Dev12 Max Cur		Dev12 Mak Akım			Dev12 Mak Akım
353		32			15			Dev13 Max Current			Dev13 Max Cur		Dev13 Mak Akım			Dev13 Mak Akım
354		32			15			Dev14 Max Current			Dev14 Max Cur		Dev14 Mak Akım			Dev14 Mak Akım
355		32			15			Dev15 Max Current			Dev15 Max Cur		Dev15 Mak Akım			Dev15 Mak Akım
356		32			15			Dev16 Max Current			Dev16 Max Cur		Dev16 Mak Akım			Dev16 Mak Akım
357		32			15			Dev17 Max Current			Dev17 Max Cur		Dev17 Mak Akım			Dev17 Mak Akım
358		32			15			Dev18 Max Current			Dev18 Max Cur		Dev18 Mak Akım			Dev18 Mak Akım
359		32			15			Dev19 Max Current			Dev19 Max Cur		Dev19 Mak Akım			Dev19 Mak Akım
360		32			15			Dev20 Max Current			Dev20 Max Cur		Dev20 Mak Akım			Dev20 Mak Akım
361		32			15			Dev21 Max Current			Dev21 Max Cur		Dev21 Mak Akım			Dev21 Mak Akım
362		32			15			Dev22 Max Current			Dev22 Max Cur		Dev22 Mak Akım			Dev22 Mak Akım
363		32			15			Dev23 Max Current			Dev23 Max Cur		Dev23 Mak Akım			Dev23 Mak Akım
364		32			15			Dev24 Max Current			Dev24 Max Cur		Dev24 Mak Akım			Dev24 Mak Akım
365		32			15			Dev25 Max Current			Dev25 Max Cur		Dev25 Mak Akım			Dev25 Mak Akım
366		32			15			Dev26 Max Current			Dev26 Max Cur		Dev26 Mak Akım			Dev26 Mak Akım
367		32			15			Dev27 Max Current			Dev27 Max Cur		Dev27 Mak Akım			Dev27 Mak Akım
368		32			15			Dev28 Max Current			Dev28 Max Cur		Dev28 Mak Akım			Dev28 Mak Akım
369		32			15			Dev29 Max Current			Dev29 Max Cur		Dev29 Mak Akım			Dev29 Mak Akım
370		32			15			Dev30 Max Current			Dev30 Max Cur		Dev30 Mak Akım			Dev30 Mak Akım
371		32			15			Dev31 Max Current			Dev31 Max Cur		Dev31 Mak Akım			Dev31 Mak Akım
372		32			15			Dev32 Max Current			Dev32 Max Cur		Dev32 Mak Akım			Dev32 Mak Akım
373		32			15			Dev33 Max Current			Dev33 Max Cur		Dev33 Mak Akım			Dev33 Mak Akım
374		32			15			Dev34 Max Current			Dev34 Max Cur		Dev34 Mak Akım			Dev34 Mak Akım
375		32			15			Dev35 Max Current			Dev35 Max Cur		Dev35 Mak Akım			Dev35 Mak Akım
376		32			15			Dev36 Max Current			Dev36 Max Cur		Dev36 Mak Akım			Dev36 Mak Akım
377		32			15			Dev37 Max Current			Dev37 Max Cur		Dev37 Mak Akım			Dev37 Mak Akım
378		32			15			Dev38 Max Current			Dev38 Max Cur		Dev38 Mak Akım			Dev38 Mak Akım
379		32			15			Dev39 Max Current			Dev39 Max Cur		Dev39 Mak Akım			Dev39 Mak Akım
380		32			15			Dev40 Max Current			Dev40 Max Cur		Dev40 Mak Akım			Dev40 Mak Akım

381		32			15			Dev1 Min Voltage			Dev1 Min Vol		Dev1 Min Gerilim			Dev1 Min Ger
382		32			15			Dev2 Min Voltage			Dev2 Min Vol		Dev2 Min Gerilim			Dev2 Min Ger
383		32			15			Dev3 Min Voltage			Dev3 Min Vol		Dev3 Min Gerilim			Dev3 Min Ger
384		32			15			Dev4 Min Voltage			Dev4 Min Vol		Dev4 Min Gerilim			Dev4 Min Ger
385		32			15			Dev5 Min Voltage			Dev5 Min Vol		Dev5 Min Gerilim			Dev5 Min Ger
386		32			15			Dev6 Min Voltage			Dev6 Min Vol		Dev6 Min Gerilim			Dev6 Min Ger
387		32			15			Dev7 Min Voltage			Dev7 Min Vol		Dev7 Min Gerilim			Dev7 Min Ger
388		32			15			Dev8 Min Voltage			Dev8 Min Vol		Dev8 Min Gerilim			Dev8 Min Ger
389		32			15			Dev9 Min Voltage			Dev9 Min Vol		Dev9 Min Gerilim			Dev9 Min Ger
390		32			15			Dev10 Min Voltage			Dev10 Min Vol		Dev10 Min Gerilim			Dev10 Min Ger
391		32			15			Dev11 Min Voltage			Dev11 Min Vol		Dev11 Min Gerilim			Dev11 Min Ger
392		32			15			Dev12 Min Voltage			Dev12 Min Vol		Dev12 Min Gerilim			Dev12 Min Ger
393		32			15			Dev13 Min Voltage			Dev13 Min Vol		Dev13 Min Gerilim			Dev13 Min Ger
394		32			15			Dev14 Min Voltage			Dev14 Min Vol		Dev14 Min Gerilim			Dev14 Min Ger
395		32			15			Dev15 Min Voltage			Dev15 Min Vol		Dev15 Min Gerilim			Dev15 Min Ger
396		32			15			Dev16 Min Voltage			Dev16 Min Vol		Dev16 Min Gerilim			Dev16 Min Ger
397		32			15			Dev17 Min Voltage			Dev17 Min Vol		Dev17 Min Gerilim			Dev17 Min Ger
398		32			15			Dev18 Min Voltage			Dev18 Min Vol		Dev18 Min Gerilim			Dev18 Min Ger
399		32			15			Dev19 Min Voltage			Dev19 Min Vol		Dev19 Min Gerilim			Dev19 Min Ger
400		32			15			Dev20 Min Voltage			Dev20 Min Vol		Dev20 Min Gerilim			Dev20 Min Ger
401		32			15			Dev21 Min Voltage			Dev21 Min Vol		Dev21 Min Gerilim			Dev21 Min Ger
402		32			15			Dev22 Min Voltage			Dev22 Min Vol		Dev22 Min Gerilim			Dev22 Min Ger
403		32			15			Dev23 Min Voltage			Dev23 Min Vol		Dev23 Min Gerilim			Dev23 Min Ger
404		32			15			Dev24 Min Voltage			Dev24 Min Vol		Dev24 Min Gerilim			Dev24 Min Ger
405		32			15			Dev25 Min Voltage			Dev25 Min Vol		Dev25 Min Gerilim			Dev25 Min Ger
406		32			15			Dev26 Min Voltage			Dev26 Min Vol		Dev26 Min Gerilim			Dev26 Min Ger
407		32			15			Dev27 Min Voltage			Dev27 Min Vol		Dev27 Min Gerilim			Dev27 Min Ger
408		32			15			Dev28 Min Voltage			Dev28 Min Vol		Dev28 Min Gerilim			Dev28 Min Ger
409		32			15			Dev29 Min Voltage			Dev29 Min Vol		Dev29 Min Gerilim			Dev29 Min Ger
410		32			15			Dev30 Min Voltage			Dev30 Min Vol		Dev30 Min Gerilim			Dev30 Min Ger
411		32			15			Dev31 Min Voltage			Dev31 Min Vol		Dev31 Min Gerilim			Dev31 Min Ger
412		32			15			Dev32 Min Voltage			Dev32 Min Vol		Dev32 Min Gerilim			Dev32 Min Ger
413		32			15			Dev33 Min Voltage			Dev33 Min Vol		Dev33 Min Gerilim			Dev33 Min Ger
414		32			15			Dev34 Min Voltage			Dev34 Min Vol		Dev34 Min Gerilim			Dev34 Min Ger
415		32			15			Dev35 Min Voltage			Dev35 Min Vol		Dev35 Min Gerilim			Dev35 Min Ger
416		32			15			Dev36 Min Voltage			Dev36 Min Vol		Dev36 Min Gerilim			Dev36 Min Ger
417		32			15			Dev37 Min Voltage			Dev37 Min Vol		Dev37 Min Gerilim			Dev37 Min Ger
418		32			15			Dev38 Min Voltage			Dev38 Min Vol		Dev38 Min Gerilim			Dev38 Min Ger
419		32			15			Dev39 Min Voltage			Dev39 Min Vol		Dev39 Min Gerilim			Dev39 Min Ger
420		32			15			Dev40 Min Voltage			Dev40 Min Vol		Dev40 Min Gerilim			Dev40 Min Ger
																
421		32			15			Dev1 Max Voltage			Dev1 Max Vol		Dev1 Mak Gerilim			Dev1 Mak Ger
422		32			15			Dev2 Max Voltage			Dev2 Max Vol		Dev2 Mak Gerilim			Dev2 Mak Ger
423		32			15			Dev3 Max Voltage			Dev3 Max Vol		Dev3 Mak Gerilim			Dev3 Mak Ger
424		32			15			Dev4 Max Voltage			Dev4 Max Vol		Dev4 Mak Gerilim			Dev4 Mak Ger
425		32			15			Dev5 Max Voltage			Dev5 Max Vol		Dev5 Mak Gerilim			Dev5 Mak Ger
426		32			15			Dev6 Max Voltage			Dev6 Max Vol		Dev6 Mak Gerilim			Dev6 Mak Ger
427		32			15			Dev7 Max Voltage			Dev7 Max Vol		Dev7 Mak Gerilim			Dev7 Mak Ger
428		32			15			Dev8 Max Voltage			Dev8 Max Vol		Dev8 Mak Gerilim			Dev8 Mak Ger
429		32			15			Dev9 Max Voltage			Dev9 Max Vol		Dev9 Mak Gerilim			Dev9 Mak Ger
430		32			15			Dev10 Max Voltage			Dev10 Max Vol		Dev10 Mak Gerilim			Dev10 Mak Ger
431		32			15			Dev11 Max Voltage			Dev11 Max Vol		Dev11 Mak Gerilim			Dev11 Mak Ger
432		32			15			Dev12 Max Voltage			Dev12 Max Vol		Dev12 Mak Gerilim			Dev12 Mak Ger
433		32			15			Dev13 Max Voltage			Dev13 Max Vol		Dev13 Mak Gerilim			Dev13 Mak Ger
434		32			15			Dev14 Max Voltage			Dev14 Max Vol		Dev14 Mak Gerilim			Dev14 Mak Ger
435		32			15			Dev15 Max Voltage			Dev15 Max Vol		Dev15 Mak Gerilim			Dev15 Mak Ger
436		32			15			Dev16 Max Voltage			Dev16 Max Vol		Dev16 Mak Gerilim			Dev16 Mak Ger
437		32			15			Dev17 Max Voltage			Dev17 Max Vol		Dev17 Mak Gerilim			Dev17 Mak Ger
438		32			15			Dev18 Max Voltage			Dev18 Max Vol		Dev18 Mak Gerilim			Dev18 Mak Ger
439		32			15			Dev19 Max Voltage			Dev19 Max Vol		Dev19 Mak Gerilim			Dev19 Mak Ger
440		32			15			Dev20 Max Voltage			Dev20 Max Vol		Dev20 Mak Gerilim			Dev20 Mak Ger
441		32			15			Dev21 Max Voltage			Dev21 Max Vol		Dev21 Mak Gerilim			Dev21 Mak Ger
442		32			15			Dev22 Max Voltage			Dev22 Max Vol		Dev22 Mak Gerilim			Dev22 Mak Ger
443		32			15			Dev23 Max Voltage			Dev23 Max Vol		Dev23 Mak Gerilim			Dev23 Mak Ger
444		32			15			Dev24 Max Voltage			Dev24 Max Vol		Dev24 Mak Gerilim			Dev24 Mak Ger
445		32			15			Dev25 Max Voltage			Dev25 Max Vol		Dev25 Mak Gerilim			Dev25 Mak Ger
446		32			15			Dev26 Max Voltage			Dev26 Max Vol		Dev26 Mak Gerilim			Dev26 Mak Ger
447		32			15			Dev27 Max Voltage			Dev27 Max Vol		Dev27 Mak Gerilim			Dev27 Mak Ger
448		32			15			Dev28 Max Voltage			Dev28 Max Vol		Dev28 Mak Gerilim			Dev28 Mak Ger
449		32			15			Dev29 Max Voltage			Dev29 Max Vol		Dev29 Mak Gerilim			Dev29 Mak Ger
450		32			15			Dev30 Max Voltage			Dev30 Max Vol		Dev30 Mak Gerilim			Dev30 Mak Ger
451		32			15			Dev31 Max Voltage			Dev31 Max Vol		Dev31 Mak Gerilim			Dev31 Mak Ger
452		32			15			Dev32 Max Voltage			Dev32 Max Vol		Dev32 Mak Gerilim			Dev32 Mak Ger
453		32			15			Dev33 Max Voltage			Dev33 Max Vol		Dev33 Mak Gerilim			Dev33 Mak Ger
454		32			15			Dev34 Max Voltage			Dev34 Max Vol		Dev34 Mak Gerilim			Dev34 Mak Ger
455		32			15			Dev35 Max Voltage			Dev35 Max Vol		Dev35 Mak Gerilim			Dev35 Mak Ger
456		32			15			Dev36 Max Voltage			Dev36 Max Vol		Dev36 Mak Gerilim			Dev36 Mak Ger
457		32			15			Dev37 Max Voltage			Dev37 Max Vol		Dev37 Mak Gerilim			Dev37 Mak Ger
458		32			15			Dev38 Max Voltage			Dev38 Max Vol		Dev38 Mak Gerilim			Dev38 Mak Ger
459		32			15			Dev39 Max Voltage			Dev39 Max Vol		Dev39 Mak Gerilim			Dev39 Mak Ger
460		32			15			Dev40 Max Voltage			Dev40 Max Vol		Dev40 Mak Gerilim			Dev40 Mak Ger

