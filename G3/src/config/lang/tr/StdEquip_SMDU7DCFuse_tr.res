﻿#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
tr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1	32			15			Load Fuse 1			Load Fuse1		Yuk Sigortasi 1				Yuk Sigortasi1
2	32			15			Load Fuse 2			Load Fuse2		Yuk Sigortasi 2				Yuk Sigortasi2
3	32			15			Load Fuse 3			Load Fuse3		Yuk Sigortasi 3				Yuk Sigortasi3
4	32			15			Load Fuse 4			Load Fuse4		Yuk Sigortasi 4				Yuk Sigortasi4
5	32			15			Load Fuse 5			Load Fuse5		Yuk Sigortasi 5				Yuk Sigortasi5
6	32			15			Load Fuse 6			Load Fuse6		Yuk Sigortasi 6				Yuk Sigortasi6
7	32			15			Load Fuse 7			Load Fuse7		Yuk Sigortasi 7				Yuk Sigortasi7
8	32			15			Load Fuse 8			Load Fuse8		Yuk Sigortasi 8				Yuk Sigortasi8
9	32			15			Load Fuse 9			Load Fuse9		Yuk Sigortasi 9				Yuk Sigortasi9
10	32			15			Load Fuse 10			Load Fuse10		Yuk Sigortasi 10			Yuk Sigortasi10
11	32			15			Load Fuse 11			Load Fuse11		Yuk Sigortasi 11			Yuk Sigortasi11
12	32			15			Load Fuse 12			Load Fuse12		Yuk Sigortasi 12			Yuk Sigortasi12
13	32			15			Load Fuse 13			Load Fuse13		Yuk Sigortasi 13			Yuk Sigortasi13
14	32			15			Load Fuse 14			Load Fuse14		Yuk Sigortasi 14			Yuk Sigortasi14
15	32			15			Load Fuse 15			Load Fuse15		Yuk Sigortasi 15			Yuk Sigortasi15
16	32			15			Load Fuse 16			Load Fuse16		Yuk Sigortasi 16			Yuk Sigortasi16
17	32			15			SMDU7 DC Fuse			SMDU7 DC Fuse		SMDU7 DC Sigorta			SMDU7 DC Sigorta	
18	32			15			State				State			Durum					Durum
19	32			15			Off				Off			Kapali					Kapali
20	32			15			On				On			Acik					Acik
21	32			15			Fuse 1 Alarm			Fuse 1 Alarm		Sigorta 1 Alarm				Sigorta1 Alarm	
22	32			15			Fuse 2 Alarm			Fuse 2 Alarm		Sigorta 2 Alarm				Sigorta2 Alarm
23	32			15			Fuse 3 Alarm			Fuse 3 Alarm		Sigorta 3 Alarm				Sigorta3 Alarm
24	32			15			Fuse 4 Alarm 			Fuse 4 Alarm		Sigorta 4 Alarm				Sigorta4 Alarm
25	32			15			Fuse 5 Alarm 			Fuse 5 Alarm		Sigorta 5 Alarm				Sigorta5 Alarm
26	32			15			Fuse 6 Alarm 			Fuse 6 Alarm		Sigorta 6 Alarm				Sigorta6 Alarm
27	32			15			Fuse 7 Alarm 			Fuse 7 Alarm		Sigorta 7 Alarm				Sigorta7 Alarm
28	32			15			Fuse 8 Alarm 			Fuse 8 Alarm		Sigorta 8 Alarm				Sigorta8 Alarm
29	32			15			Fuse 9 Alarm 			Fuse 9 Alarm		Sigorta 9 Alarm				Sigorta9 Alarm
30	32			15			Fuse 10 Alarm			Fuse 10 Alarm		Sigorta 10 Alarm			Sigorta10 Alarm
31	32			15			Fuse 11 Alarm			Fuse 11 Alarm		Sigorta 11 Alarm			Sigorta11 Alarm
32	32			15			Fuse 12 Alarm			Fuse 12 Alarm		Sigorta 12 Alarm			Sigorta12 Alarm
33	32			15			Fuse 13 Alarm			Fuse 13 Alarm		Sigorta 13 Alarm			Sigorta13 Alarm
34	32			15			Fuse 14 Alarm			Fuse 14 Alarm		Sigorta 14 Alarm			Sigorta14 Alarm
35	32			15			Fuse 15 Alarm			Fuse 15 Alarm		Sigorta 15 Alarm			Sigorta15 Alarm
36	32			15			Fuse 16 Alarm			Fuse 16 Alarm		Sigorta 16 Alarm			Sigorta16 Alarm
37	32			15			Interrupt Times			Interrupt Times		Kesme Zamanlari				Kesme Zamanlari
38	32			15			Communication Interrupt		Comm Interrupt		Iletisim Kesme				Iletisim Kesme
39	32			15			Load 1 Current			Load 1 Current		Yuk 1 Akim				Yuk 1 Akim
40	32			15			Load 2 Current			Load 2 Current		Yuk 2 Akim				Yuk 2 Akim
41	32			15			Load 3 Current			Load 3 Current		Yuk 3 Akim				Yuk 3 Akim
42	32			15			Load 4 Current			Load 4 Current		Yuk 4 Akim				Yuk 4 Akim
43	32			15			Load 5 Current			Load 5 Current		Yuk 5 Akim				Yuk 5 Akim
