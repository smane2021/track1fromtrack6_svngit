﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		Tensione barra distribuzione		Tens barra D
2		32			15			Load 1 Current				Load1 Current		Corrente carica 1			Corrente 1
3		32			15			Load 2 Current				Load2 Current		Corrente carica 2			Corrente 2
4		32			15			Load 3 Current				Load3 Current		Corrente carica 3			Corrente 3
5		32			15			Load 4 Current				Load4 Current		Corrente carica 4			Corrente 4
6		32			15			Load Fuse 1				Load Fuse1		Fusibile carica 1			Fusibile carica1
7		32			15			Load Fuse 2				Load Fuse2		Fusibile carica 2			Fusibile carica2
8		32			15			Load Fuse 3				Load Fuse3		Fusibile carica 3			Fusibile carica3
9		32			15			Load Fuse 4				Load Fuse4		Fusibile carica 4			Fusibile carica4
10		32			15			Load Fuse 5				Load Fuse5		Fusibile carica 5			Fusibile carica5
11		32			15			Load Fuse 6				Load Fuse6		Fusibile carica 6			Fusibile carica6
12		32			15			Load Fuse 7				Load Fuse7		Fusibile carica 7			Fusibile carica7
13		32			15			Load Fuse 8				Load Fuse8		Fusibile carica 8			Fusibile carica8
14		32			15			Load Fuse 9				Load Fuse9		Fusibile carica 9			Fusibile carica9
15		32			15			Load Fuse 10				Load Fuse10		Fusibile carica 10			Fusibile carica10
16		32			15			Load Fuse 11				Load Fuse11		Fusibile carica 11			Fusibile carica11
17		32			15			Load Fuse 12				Load Fuse12		Fusibile carica 12			Fusibile carica12
18		32			15			Load Fuse 13				Load Fuse13		Fusibile carica 13			Fusibile carica13
19		32			15			Load Fuse 14				Load Fuse14		Fusibile carica 14			Fusibile carica14
20		32			15			Load Fuse 15				Load Fuse15		Fusibile carica 15			Fusibile carica15
21		32			15			Load Fuse 16				Load Fuse16		Fusibile carica 16			Fusibile carica16
22		32			15			Run Time				Run Time		Tempo di funzionamento			Tempo funz
23		32			15			LVD1 Control				LVD1 Control		Controllo LVD1				Ctrl LVD1
24		32			15			LVD2 Control				LVD2 Control		Controllo LVD2				Ctrl LVD2
25		32			15			LVD1 Voltage				LVD1 Voltage		Tensione LVD1				Tens LVD1
26		32			15			LVR1 Voltage				LVR1 Voltage		Tensione LVR1				Tens LVR1
27		32			15			LVD2 Voltage				LVD2 Voltage		Tensione LVD2				Tens LVD2
28		32			15			LVR2 voltage				LVR2 voltage		Tensione LVR2				Tens LVR2
29		32			15			On					On			ACCESO					ACCESO
30		32			15			Off					Off			SPENTO					SPENTO
31		32			15			Normal					Normal			Normale					Normale
32		32			15			Error					Error			Errore					Errore
33		32			15			On					On			ACCESO					ACCESO
34		32			15			Fuse 1 Alarm				Fuse1 Alarm		Allarme Fusibile 1			Allarme fus1
35		32			15			Fuse 2 Alarm				Fuse2 Alarm		Allarme Fusibile 2			Allarme fus2
36		32			15			Fuse 3 Alarm				Fuse3 Alarm		Allarme Fusibile 3			Allarme fus3
37		32			15			Fuse 4 Alarm				Fuse4 Alarm		Allarme Fusibile 4			Allarme fus4
38		32			15			Fuse 5 Alarm				Fuse5 Alarm		Allarme Fusibile 5			Allarme fus5
39		32			15			Fuse 6 Alarm				Fuse6 Alarm		Allarme Fusibile 6			Allarme fus6
40		32			15			Fuse 7 Alarm				Fuse7 Alarm		Allarme Fusibile 7			Allarme fus7
41		32			15			Fuse 8 Alarm				Fuse8 Alarm		Allarme Fusibile 8			Allarme fus8
42		32			15			Fuse 9 Alarm				Fuse9 Alarm		Allarme Fusibile 9			Allarme fus9
43		32			15			Fuse 10 Alarm				Fuse10 Alarm		Allarme Fusibile 10			Allarme fus10
44		32			15			Fuse 11 Alarm				Fuse11 Alarm		Allarme Fusibile 11			Allarme fus11
45		32			15			Fuse 12 Alarm				Fuse12 Alarm		Allarme Fusibile 12			Allarme fus12
46		32			15			Fuse 13 Alarm				Fuse13 Alarm		Allarme Fusibile 13			Allarme fus13
47		32			15			Fuse 14 Alarm				Fuse14 Alarm		Allarme Fusibile 14			Allarme fus14
48		32			15			Fuse 15 Alarm				Fuse15 Alarm		Allarme Fusibile 15			Allarme fus15
49		32			15			Fuse 16 Alarm				Fuse16 Alarm		Allarme Fusibile 16			Allarme fus16
50		32			15			HW Test Alarm				HW Test Alarm		Allarme test HW				Allarme test HW
51		32			15			SM-DU Unit 7				SM-DU Unit 7		Unità 7 SM-DU				Unità 7 SM-DU
52		32			15			Battery Fuse 1 Voltage			BATT Fuse1 Volt		Tensione fusibile batteria 1		Tens fus bat1
53		32			15			Battery Fuse 2 Voltage			BATT Fuse2 Volt		Tensione fusibile batteria 2		Tens fus bat2
54		32			15			Battery Fuse 3 Voltage			BATT Fuse3 Volt		Tensione fusibile batteria 3		Tens fus bat3
55		32			15			Battery Fuse 4 Voltage			BATT Fuse4 Volt		Tensione fusibile batteria 4		Tens fus bat4
56		32			15			Battery Fuse 1 Status			BATT Fuse1 Stat		Stato fusibile batteria 1		Stato fus bat1
57		32			15			Battery Fuse 2 Status			BATT Fuse2 Stat		Stato fusibile batteria 2		Stato fus bat2
58		32			15			Battery Fuse 3 Status			BATT Fuse3 Stat		Stato fusibile batteria 3		Stato fus bat3
59		32			15			Battery Fuse 4 Status			BATT Fuse4 Stat		Stato fusibile batteria 4		Stato fus bat4
60		32			15			On					On			ACCESO					ACCESO
61		32			15			Off					Off			SPENTO					SPENTO
62		32			15			Battery Fuse 1 Alarm			Bat Fuse1 Alarm		Allarme fusibile batteria 1		Allarme fus1 bat
63		32			15			Battery Fuse 2 Alarm			Bat Fuse2 Alarm		Allarme fusibile batteria 2		Allarme fus2 bat
64		32			15			Battery Fuse 3 Alarm			Bat Fuse3 Alarm		Allarme fusibile batteria 3		Allarme fus3 bat
65		32			15			Battery Fuse 4 Alarm			Bat Fuse4 Alarm		Allarme fusibile batteria 4		Allarme fus4 bat
66		32			15			Total Load Current			Tot Load Curr		Corrente totale di carico		Carico totale
67		32			15			Over Load Current Limit			Over Curr Lim		Valore di sovracorrente			Sovracorrente
68		32			15			Over Current				Over Current		Sovracorrente				Sovracorrente
69		32			15			LVD1 Enabled				LVD1 Enabled		LVD1 abilitato				LVD1 abilitato
70		32			15			LVD1 Mode				LVD1 Mode		Modo LVD1				Modo LVD1
71		32			15			LVD1 Reconnect Delay			LVD1Recon Delay		Ritardo riconnessione LVD1		RitarRicon LVD1
72		32			15			LVD2 Enabled				LVD2 Enabled		LVD2 abilitato				LVD2 abilitato
73		32			15			LVD2 Mode				LVD2 Mode		Modo LVD2				Modo LVD2
74		32			15			LVD2 Reconnect Delay			LVD2Recon Delay		Ritardo riconnessione LVD2		RitarRicon LVD2
75		32			15			LVD1 Status				LVD1 Status		Stato LVD1				Stato LVD1
76		32			15			LVD2 Status				LVD2 Status		Stato LVD2				Stato LVD2
77		32			15			Disabled				Disabled		Disabilitato				Disabilitato
78		32			15			Enabled					Enabled			Abilitato				Abilitato
79		32			15			By Voltage				By Volt			Per tensione				Per tensione
80		32			15			By Time					By Time			Per tempo				Per tempo
81		32			15			Bus Bar Volt Alarm			Bus Bar Alarm		Allarme barra distribuzione		Allarme bus Dist
82		32			15			Normal					Normal			Normale					Normal
83		32			15			Low					Low			Basso					Basso
84		32			15			High					High			Alto					Alto
85		32			15			Low Voltage				Low Voltage		Tensione bassa				Tensione bassa
86		32			15			High Voltage				High Voltage		Tensione alta				Tensione bassa
87		32			15			Shunt 1 Current Alarm			Shunt1 Alarm		Allarme corrente shunt1			Allarme shunt1
88		32			15			Shunt 2 Current Alarm			Shunt2 Alarm		Allarme corrente shunt2			Allarme shunt2
89		32			15			Shunt 3 Current Alarm			Shunt3 Alarm		Allarme corrente shunt3			Allarme shunt3
90		32			15			Shunt 4 Current Alarm			Shunt4 Alarm		Allarme corrente shunt4			Allarme shunt4
91		32			15			Shunt 1 Over Current			Shunt1 Over Cur		Sovracorrente shunt1			Sovracor shunt1
92		32			15			Shunt 2 Over Current			Shunt2 Over Cur		Sovracorrente shunt2			Sovracor shunt2
93		32			15			Shunt 3 Over Current			Shunt3 Over Cur		Sovracorrente shunt3			Sovracor shunt3
94		32			15			Shunt 4 Over Current			Shunt4 Over Cur		Sovracorrente shunt4			Sovracor shunt4
95		32			15			Interrupt Times				Interrupt Times		Interruzioni				Interruzioni
96		32			15			Existent				Existent		Esistente				Esistente
97		32			15			Non-Existent				Non-Existent		Inesistente				Inesistente
98		32			15			Very Low				Very Low		Molto basso				Molto basso
99		32			15			Very High				Very High		Molto alto				Molto alto
100		32			15			Switch					Switch			Interruttore				Interruttore
101		32			15			LVD1 Failure				LVD1 Failure		Guasto LVD1				Gsto LVD1
102		32			15			LVD2 Failure				LVD2 Failure		Guasto LVD2				Gsto LVD2
103		32			15			HTD1 Enable				HTD1 Enable		Abilita HTD1				Abilita HTD1
104		32			15			HTD2 Enable				HTD2 Enable		Abilita HTD2				Abilita HTD2
105		32			15			Battery LVD				Battery LVD		LVD di batteria				LVD di batteria
106		32			15			No Battery				No Battery		Nessuna batteria			Nessuna batteria
107		32			15			LVD1					LVD1			LVD1					LVD1
108		32			15			LVD2					LVD2			LVD2					LVD2
109		32			15			Battery Always On			BattAlwaysOn		Batteria sempre connessa		Sempre connessa
110		32			15			Barcode					Barcode			Codice a barre				Cod a barre
111		32			15			DC Overvoltage				DC Overvolt		Sovratensione CC			Sovratens CC
112		32			15			DC Undervoltage				DC Undervolt		Sottotensione CC			Sottotens CC
113		32			15			Overcurrent 1				Overcurr 1		Sovracorrente 1				Sovracorr 1
114		32			15			Overcurrent 2				Overcurr 2		Sovracorrente 2				Sovracorr 2
115		32			15			Overcurrent 3				Overcurr 3		Sovracorrente 3				Sovracorr 3
116		32			15			Overcurrent 4				Overcurr 4		Sovracorrente 4				Sovracorr 4
117		32			15			Existence State				Existence State		Stato attuale				Stato attuale
118		32			15			Communication Interrupt			Comm Interrupt		Comunicazione interrotta		COM interr
119		32			15			Bus Voltage Status			Bus Status		Stato Bus				Stato bus V
120		32			15			Communication OK			Comm OK			Comunicazione ok			COM ok
121		32			15			None is Responding			None Responding		Nessuno risponde			Ness risponde
122		32			15			No Response				No Response		Nessuna risposta			Ness risposta
123		32			15			Rated Battery Capacity			Rated Bat Cap		Capacità nominale			Capacità nom
124		32			15			Load 5 Current				Load5 Current		Corrente carica 5			Corrente 5
125		32			15			Shunt 1 Voltage				Shunt 1 Voltage		Tensione shunt 1			V shunt 1
126		32			15			Shunt 1 Current				Shunt 1 Current		Corrente shunt 1			I shunt 1
127		32			15			Shunt 2 Voltage				Shunt 2 Voltage		Tensione shunt 2			V shunt 2
128		32			15			Shunt 2 Current				Shunt 2 Current		Corrente shunt 2			I shunt 2
129		32			15			Shunt 3 Voltage				Shunt 3 Voltage		Tensione shunt 3			V shunt 3
130		32			15			Shunt 3 Current				Shunt 3 Current		Corrente shunt 3			I shunt 3
131		32			15			Shunt 4 Voltage				Shunt 4 Voltage		Tensione shunt 4			V shunt 4
132		32			15			Shunt 4 Current				Shunt 4 Current		Corrente shunt 4			I shunt 4
133		32			15			Shunt 5 Voltage				Shunt 5 Voltage		Tensione shunt 5			V shunt 5
134		32			15			Shunt 5 Current				Shunt 5 Current		Corrente shunt 5			I shunt 5
150		32			15			Battery Current 1			Batt Curr 1		Corrente 1 (Batteria)			Corrente 1(B)
151		32			15			Battery Current 2			Batt Curr 2		Corrente 2 (Batteria)			Corrente 2(B)
152		32			15			Battery Current 3			Batt Curr 3		Corrente 3 (Batteria)			Corrente 3(B)
153		32			15			Battery Current 4			Batt Curr 4		Corrente 4 (Batteria)			Corrente 4(B)
154		32			15			Battery Current 5			Batt Curr 5		Corrente 5 (Batteria)			Corrente 5(B)
170		32			15			High Current 1				Hi Current 1		Corrente 1 alta				I1 alta
171		32			15			Very High Current 1			VHi Current 1		Corrente 1 mlto alta			I1 mlto alta
172		32			15			High Current 2				Hi Current 2		Corrente 2 alta				I2 alta
173		32			15			Very High Current 2			VHi Current 2		Corrente 2 mlto alta			I2 mlto alta
174		32			15			High Current 3				Hi Current 3		Corrente 3 alta				I3 alta
175		32			15			Very High Current 3			VHi Current 3		Corrente 3 mlto alta			I3 mlto alta
176		32			15			High Current 4				Hi Current 4		Corrente 4 alta				I4 alta
177		32			15			Very High Current 4			VHi Current 4		Corrente 4 mlto alta			I4 mlto alta
178		32			15			High Current 5				Hi Current 5		Corrente 5 alta				I5 alta
179		32			15			Very High Current 5			VHi Current 5		Corrente 5 mlto alta			I5 mlto alta
220		32			15			Current 1 High Limit			Curr 1 Hi Lim		Limite corrente 1 alto			Lmt I1 alto
221		32			15			Current 1 Very High Limit		Curr 1 VHi Lim		Limite corrente 1 mlto alto		Lmt I1 mlto >
222		32			15			Current 2 High Limit			Curr 2 Hi Lim		Limite corrente 2 alto			Lmt I2 alto
223		32			15			Current 2 Very High Limit		Curr 2 VHi Lim		Limite corrente 2 mlto alto		Lmt I2 mlto >
224		32			15			Current 3 High Limit			Curr 3 Hi Lim		Limite corrente 3 alto			Lmt I3 alto
225		32			15			Current 3 Very High Limit		Curr 3 VHi Lim		Limite corrente 3 mlto alto		Lmt I3 mlto >
226		32			15			Current 4 High Limit			Curr 4 Hi Lim		Limite corrente 4 alto			Lmt I4 alto
227		32			15			Current 4 Very High Limit		Curr 4 VHi Lim		Limite corrente 4 mlto alto		Lmt I4 mlto >
228		32			15			Current 5 High Limit			Curr 5 Hi Lim		Limite corrente 5 alto			Lmt I5 alto
229		32			15			Current 5 Very High Limit		Curr 5 VHi Lim		Limite corrente 5 mlto alto		Lmt I5 mlto >
270		32			15			Current 1 Breaker Size			Curr 1 Brk Size		Taglia interr corrente 1		Interr. I1
271		32			15			Current 2 Breaker Size			Curr 2 Brk Size		Taglia interr corrente 2		Interr. I2
272		32			15			Current 3 Breaker Size			Curr 3 Brk Size		Taglia interr corrente 3		Interr. I3
273		32			15			Current 4 Breaker Size			Curr 4 Brk Size		Taglia interr corrente 4		Interr. I4
274		32			15			Current 5 Breaker Size			Curr 5 Brk Size		Taglia interr corrente 5		Interr. I5
281		32			15			Shunt Size Switch			Shunt Size Switch	Taglia shunt switch			Shunt switch
283		32			15			Shunt 1 Size Conflicting		Sh 1 Conflict		Conflitto taglia shunt 1		Errore SH1
284		32			15			Shunt 2 Size Conflicting		Sh 2 Conflict		Conflitto taglia shunt 2		Errore SH2
285		32			15			Shunt 3 Size Conflicting		Sh 3 Conflict		Conflitto taglia shunt 3		Errore SH3
286		32			15			Shunt 4 Size Conflicting		Sh 4 Conflict		Conflitto taglia shunt 4		Errore SH4
287		32			15			Shunt 5 Size Conflicting		Sh 5 Conflict		Conflitto taglia shunt 5		Errore SH5
290		32			15			By Software				By Software		Col software				Col software
291		32			15			By Dip Switch				By Dip Switch		Coi dip switch				Coi dip switch
292		32			15			No Supported				No Supported		Non supportato				Non supportato
293		32			15			Not Used				Not Used		Non Usata				Non Usata
294		32			15			General					General			Generale				Generale
295		32			15			Load					Load			Carico					Carico
296		32			15			Battery					Battery			Batteria				Batteria
297		32			15			Shunt1 Set As				Shunt1SetAs		Shunt1 impostato come			Shunt1 Impost
298		32			15			Shunt2 Set As				Shunt2SetAs		Shunt2 impostato come			Shunt2 Impost
299		32			15			Shunt3 Set As				Shunt3SetAs		Shunt3 impostato come			Shunt3 Impost
300		32			15			Shunt4 Set As				Shunt4SetAs		Shunt4 impostato come			Shunt4 Impost
301		32			15			Shunt5 Set As				Shunt5SetAs		Shunt5 impostato come			Shunt5 Impost
302		32			15			Shunt1 Reading				Shunt1Reading		Lettura Shunt1				Lettura Shunt1
303		32			15			Shunt2 Reading				Shunt2Reading		Lettura Shunt2				Lettura Shunt2
304		32			15			Shunt3 Reading				Shunt3Reading		Lettura Shunt3				Lettura Shunt3
305		32			15			Shunt4 Reading				Shunt4Reading		Lettura Shunt4				Lettura Shunt4
306		32			15			Shunt5 Reading				Shunt5Reading		Lettura Shunt5				Lettura Shunt5
307		32			15			Load1 Alarm Flag			Load1 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
308		32			15			Load2 Alarm Flag			Load2 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
309		32			15			Load3 Alarm Flag			Load3 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
310		32			15			Load4 Alarm Flag			Load4 Alarm Flag	Load4 Alarm Flag			Load4 Alarm Flag
311		32			15			Load5 Alarm Flag			Load5 Alarm Flag	Load5 Alarm Flag			Load5 Alarm Flag
500		32			15			Current1 High 1 Curr			Curr1 Hi1Cur		Correnti1 Alta 1 Corr			Corr1Alta1Corr		
501		32			15			Current1 High 2 Curr			Curr1 Hi2Cur		Correnti1 Alta 2 Corr			Corr1Alta2Corr		
502		32			15			Current2 High 1 Curr			Curr2 Hi1Cur		Correnti2 Alta 1 Corr			Corr2Alta1Corr		
503		32			15			Current2 High 2 Curr			Curr2 Hi2Cur		Correnti2 Alta 2 Corr			Corr2Alta2Corr		
504		32			15			Current3 High 1 Curr			Curr3 Hi1Cur		Correnti3 Alta 1 Corr			Corr3Alta1Corr		
505		32			15			Current3 High 2 Curr			Curr3 Hi2Cur		Correnti3 Alta 2 Corr			Corr3Alta2Corr		
506		32			15			Current4 High 1 Curr			Curr4 Hi1Cur		Correnti4 Alta 1 Corr			Corr4Alta1Corr	
507		32			15			Current4 High 2 Curr			Curr4 Hi2Cur		Correnti4 Alta 2 Corr			Corr4Alta2Corr	
508		32			15			Current5 High 1 Curr			Curr5 Hi1Cur		Correnti5 Alta 1 Corr			Corr5Alta1Corr	
509		32			15			Current5 High 2 Curr			Curr5 Hi2Cur		Correnti5 Alta 2 Corr			Corr5Alta2Corr
550		32			15			Source							Source				Fonte						Fonte
