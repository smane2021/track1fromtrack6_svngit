﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		Tensione Barre				Tensione Barre
2		32			15			Load 1 Current				Load 1 Current		Corrente Carico 1			Corr Carico 1
3		32			15			Load 2 Current				Load 2 Current		Corrente Carico 2			Corr Carico 2
4		32			15			Load 3 Current				Load 3 Current		Corrente Carico 3			Corr Carico 3
5		32			15			Load 4 Current				Load 4 Current		Corrente Carico 4			Corr Carico 4
6		32			15			Load 5 Current				Load 5 Current		Corrente Carico 5			Corr Carico 5
7		32			15			Load 6 Current				Load 6 Current		Corrente Carico 6			Corr Carico 6
8		32			15			Load 7 Current				Load 7 Current		Corrente Carico 7			Corr Carico 7
9		32			15			Load 8 Current				Load 8 Current		Corrente Carico 8			Corr Carico 8
10		32			15			Load 9 Current				Load 9 Current		Corrente Carico 9			Corr Carico 9
11		32			15			Load 10 Current				Load 10 Current		Corrente Carico 10			Corr Carico 10
12		32			15			Load 11 Current				Load 11 Current		Corrente Carico 11			Corr Carico 11
13		32			15			Load 12 Current				Load 12 Current		Corrente Carico 12			Corr Carico 12
14		32			15			Load 13 Current				Load 13 Current		Corrente Carico 13			Corr Carico 13
15		32			15			Load 14 Current				Load 14 Current		Corrente Carico 14			Corr Carico 14
16		32			15			Load 15 Current				Load 15 Current		Corrente Carico 15			Corr Carico 15
17		32			15			Load 16 Current				Load 16 Current		Corrente Carico 16			Corr Carico 16
18		32			15			Load 17 Current				Load 17 Current		Corrente Carico 17			Corr Carico 17
19		32			15			Load 18 Current				Load 18 Current		Corrente Carico 18			Corr Carico 18
20		32			15			Load 19 Current				Load 19 Current		Corrente Carico 19			Corr Carico 19
21		32			15			Load 20 Current				Load 20 Current		Corrente Carico 20			Corr Carico 20
22		32			15			Power1					Power1			Potenza1				Potenza1
23		32			15			Power2					Power2			Potenza2				Potenza2
24		32			15			Power3					Power3			Potenza3				Potenza3
25		32			15			Power4					Power4			Potenza4				Potenza4
26		32			15			Power5					Power5			Potenza5				Potenza5
27		32			15			Power6					Power6			Potenza6				Potenza6
28		32			15			Power7					Power7			Potenza7				Potenza7
29		32			15			Power8					Power8			Potenza8				Potenza8
30		32			15			Power9					Power9			Potenza9				Potenza9
31		32			15			Power10					Power10			Potenza10				Potenza10
32		32			15			Power11					Power11			Potenza11				Potenza11
33		32			15			Power12					Power12			Potenza12				Potenza12
34		32			15			Power13					Power13			Potenza13				Potenza13
35		32			15			Power14					Power14			Potenza14				Potenza14
36		32			15			Power15					Power15			Potenza15				Potenza15
37		32			15			Power16					Power16			Potenza16				Potenza16
38		32			15			Power17					Power17			Potenza17				Potenza17
39		32			15			Power18					Power18			Potenza18				Potenza18
40		32			15			Power19					Power19			Potenza19				Potenza19
41		32			15			Power20					Power20			Potenza20				Potenza20
42		32			15			Energy Yesterday in Channel 1		CH1EnergyYTD		Energia Giornaliera Canale 1		Enrg Giorn Can 1
43		32			15			Energy Yesterday in Channel 2		CH2EnergyYTD		Energia Giornaliera Canale 2		Enrg Giorn Can 2
44		32			15			Energy Yesterday in Channel 3		CH3EnergyYTD		Energia Giornaliera Canale 3		Enrg Giorn Can 3
45		32			15			Energy Yesterday in Channel 4		CH4EnergyYTD		Energia Giornaliera Canale 4		Enrg Giorn Can 4
46		32			15			Energy Yesterday in Channel 5		CH5EnergyYTD		Energia Giornaliera Canale 5		Enrg Giorn Can 5
47		32			15			Energy Yesterday in Channel 6		CH6EnergyYTD		Energia Giornaliera Canale 6		Enrg Giorn Can 6
48		32			15			Energy Yesterday in Channel 7		CH7EnergyYTD		Energia Giornaliera Canale 7		Enrg Giorn Can 7
49		32			15			Energy Yesterday in Channel 8		CH8EnergyYTD		Energia Giornaliera Canale 8		Enrg Giorn Can 8
50		32			15			Energy Yesterday in Channel 9		CH9EnergyYTD		Energia Giornaliera Canale 9		Enrg Giorn Can 9
51		32			15			Energy Yesterday in Channel 10		CH10EnergyYTD		Energia Giornaliera Canale 10		Enrg Giorn Can 10
52		32			15			Energy Yesterday in Channel 11		CH11EnergyYTD		Energia Giornaliera Canale 11		Enrg Giorn Can 11
53		32			15			Energy Yesterday in Channel 12		CH12EnergyYTD		Energia Giornaliera Canale 12		Enrg Giorn Can 12
54		32			15			Energy Yesterday in Channel 13		CH13EnergyYTD		Energia Giornaliera Canale 13		Enrg Giorn Can 13
55		32			15			Energy Yesterday in Channel 14		CH14EnergyYTD		Energia Giornaliera Canale 14		Enrg Giorn Can 14
56		32			15			Energy Yesterday in Channel 15		CH15EnergyYTD		Energia Giornaliera Canale 15		Enrg Giorn Can 15
57		32			15			Energy Yesterday in Channel 16		CH16EnergyYTD		Energia Giornaliera Canale 16		Enrg Giorn Can 16
58		32			15			Energy Yesterday in Channel 17		CH17EnergyYTD		Energia Giornaliera Canale 17		Enrg Giorn Can 17
59		32			15			Energy Yesterday in Channel 18		CH18EnergyYTD		Energia Giornaliera Canale 18		Enrg Giorn Can 18
60		32			15			Energy Yesterday in Channel 19		CH19EnergyYTD		Energia Giornaliera Canale 19		Enrg Giorn Can 19
61		32			15			Energy Yesterday in Channel 20		CH20EnergyYTD		Energia Giornaliera Canale 20		Enrg Giorn Can 20
62		32			15			Total Energy in Channel 1		CH1TotalEnergy		Energia Totale Canale 1			Enrg Tot Can 1
63		32			15			Total Energy in Channel 2		CH2TotalEnergy		Energia Totale Canale 2			Enrg Tot Can 2
64		32			15			Total Energy in Channel 3		CH3TotalEnergy		Energia Totale Canale 3			Enrg Tot Can 3
65		32			15			Total Energy in Channel 4		CH4TotalEnergy		Energia Totale Canale 4			Enrg Tot Can 4
66		32			15			Total Energy in Channel 5		CH5TotalEnergy		Energia Totale Canale 5			Enrg Tot Can 5
67		32			15			Total Energy in Channel 6		CH6TotalEnergy		Energia Totale Canale 6			Enrg Tot Can 6
68		32			15			Total Energy in Channel 7		CH7TotalEnergy		Energia Totale Canale 7			Enrg Tot Can 7
69		32			15			Total Energy in Channel 8		CH8TotalEnergy		Energia Totale Canale 8			Enrg Tot Can 8
70		32			15			Total Energy in Channel 9		CH9TotalEnergy		Energia Totale Canale 9			Enrg Tot Can 9
71		32			15			Total Energy in Channel 10		CH10TotalEnergy		Energia Totale Canale 10		Enrg Tot Can 10
72		32			15			Total Energy in Channel 11		CH11TotalEnergy		Energia Totale Canale 11		Enrg Tot Can 11
73		32			15			Total Energy in Channel 12		CH12TotalEnergy		Energia Totale Canale 12		Enrg Tot Can 12
74		32			15			Total Energy in Channel 13		CH13TotalEnergy		Energia Totale Canale 13		Enrg Tot Can 13
75		32			15			Total Energy in Channel 14		CH14TotalEnergy		Energia Totale Canale 14		Enrg Tot Can 14
76		32			15			Total Energy in Channel 15		CH15TotalEnergy		Energia Totale Canale 15		Enrg Tot Can 15
77		32			15			Total Energy in Channel 16		CH16TotalEnergy		Energia Totale Canale 16		Enrg Tot Can 16
78		32			15			Total Energy in Channel 17		CH17TotalEnergy		Energia Totale Canale 17		Enrg Tot Can 17
79		32			15			Total Energy in Channel 18		CH18TotalEnergy		Energia Totale Canale 18		Enrg Tot Can 18
80		32			15			Total Energy in Channel 19		CH19TotalEnergy		Energia Totale Canale 19		Enrg Tot Can 19
81		32			15			Total Energy in Channel 20		CH20TotalEnergy		Energia Totale Canale 20		Enrg Tot Can 20
82		32			15			Normal					Normal			Normale					Normale
83		32			15			Low					Low			Bassa					Bassa
84		32			15			High					High			Alta					Alta
85		32			15			Under Voltage				Under Voltage		Sottotensione				Sottotensione
86		32			15			Over Voltage				Over Voltage		Sovratensione				Sovratensione
87		32			15			Shunt 1 Current Alarm			Shunt 1 Alarm		Allarme Corrente Shunt 1		All SH1
88		32			15			Shunt 2 Current Alarm			Shunt 2 Alarm		Allarme Corrente Shunt 2		All SH2
89		32			15			Shunt 3 Current Alarm			Shunt 3 Alarm		Allarme Corrente Shunt 3		All SH3
90		32			15			Bus Voltage Alarm			BusVolt Alarm		Stato Tensione Barre			Stato Vbarre
91		32			15			SMDUH Fault				SMDUH Fault		Stato SMDUH				Stato SMDUH
92		32			15			Shunt 2 Over Current			Shunt 2 OverCur		Sovracorrente Shunt 2			SovraCorr SH2
93		32			15			Shunt 3 Over Current			Shunt 3 OverCur		Sovracorrente Shunt 3			SovraCorr SH3
94		32			15			Shunt 4 Over Current			Shunt 4 OverCur		Sovracorrente Shunt 4			SovraCorr SH4
95		32			15			Times of Communication Fail		Times Comm Fail		Quante Volte Guasto Comunicazione	N Gsto Com
96		32			15			Existent				Existent		Esistente				Esistente
97		32			15			Not Existent				Not Existent		Non esistente				Non esistente
98		32			15			Very Low				Very Low		Molto Bassa				Molto Bassa
99		32			15			Very High				Very High		Molto Alta				Molto Alta
100		32			15			Switch					Switch			Swich					Swich
101		32			15			LVD1 Failure				LVD 1 Failure		Guasto LVD1				Guasto LVD1
102		32			15			LVD2 Failure				LVD 2 Failure		Guasto LVD2				Guasto LVD2
103		32			15			High Temperature Disconnect 1		HTD 1			Disconnessione 1 Alta Temperatura	HTD1
104		32			15			High Temperature Disconnect 2		HTD 2			Disconnessione 2 Alta Temperatura	HTD2
105		32			15			Battery LVD				Battery LVD		LVD Batteria				LVD Batteria
106		32			15			No Battery				No Battery		Nessuna Batteria			Nessuna Batteria
107		32			15			LVD 1					LVD 1			LVD1					LVD1
108		32			15			LVD 2					LVD 2			LVD2					LVD2
109		32			15			Battery Always On			Batt Always On		Batteria sempre ON			Batt sempre ON
110		32			15			Barcode					Barcode			Codice a barre				Codice a barre
111		32			15			DC Over Voltage				DC Over Volt		Sovratensione CC			Sovra Vcc
112		32			15			DC Under Voltage			DC Under Volt		Sottotensione CC			Sotto Vcc
113		32			15			Over Current 1				Over Curr 1		Sovracorrente 1				Sovra I1
114		32			15			Over Current 2				Over Curr 2		Sovracorrente 2				Sovra I2
115		32			15			Over Current 3				Over Curr 3		Sovracorrente 3				Sovra I3
116		32			15			Over Current 4				Over Curr 4		Sovracorrente 4				Sovra I4
117		32			15			Existence State				Existence State		Condizione				Condizione
118		32			15			Communication Fail			Comm Fail		Guasto Comunicazione			Gsto Com
119		32			15			Bus Voltage Status			Bus Volt Status		Stato Tensione Barre			Stato Vbarre
120		32			15			Comm OK					Comm OK			Comunicazione OK			Comunicazione OK
121		32			15			All Batteries Comm Fail			AllBattCommFail		Guasto Comunicazione Tutte Batterie	Gsto Com Batt
122		32			15			Communication Fail			Comm Fail		Guasto Comunicazione			Gsto Com
123		32			15			Rated Capacity				Rated Capacity		Capacità Nominale			Capacità Nominale
124		32			15			Load 5 Current				Load 5 Current		Corrente Carico 5			Corrente Carico 5
125		32			15			Shunt 1 Voltage				Shunt 1 Voltage		Tensione Shunt 1			Tensione Shunt 1
126		32			15			Shunt 1 Current				Shunt 1 Current		Corrente Shunt 1			Corrente Shunt 1
127		32			15			Shunt 2 Voltage				Shunt 2 Voltage		Tensione Shunt 2			Tensione Shunt 2
128		32			15			Shunt 2 Current				Shunt 2 Current		Corrente Shunt 2			Corrente Shunt 2
129		32			15			Shunt 3 Voltage				Shunt 3 Voltage		Tensione Shunt 3			Tensione Shunt 3
130		32			15			Shunt 3 Current				Shunt 3 Current		Corrente Shunt 3			Corrente Shunt 3
131		32			15			Shunt 4 Voltage				Shunt 4 Voltage		Tensione Shunt 4			Tensione Shunt 4
132		32			15			Shunt 4 Current				Shunt 4 Current		Corrente Shunt 4			Corrente Shunt 4
133		32			15			Shunt 5 Voltage				Shunt 5 Voltage		Tensione Shunt 5			Tensione Shunt 5
134		32			15			Shunt 5 Current				Shunt 5 Current		Corrente Shunt 5			Corrente Shunt 5
135		32			15			Normal					Normal			Normale					Normale
136		32			15			Fault					Fault			Guasto					Guasto
137		32			15			Hall Calibrate Point 1			HallCalibrate1		Punto 1 Calibrazione Sala		Calibraz1 Sala
138		32			15			Hall Calibrate Point 2			HallCalibrate2		Punto 2 Calibrazione Sala		Calibraz1 Sala
139		32			15			Energy Clear				EnergyClear		Cancella Giorno x Energia		Canc GiornoXEnerg
140		32			15			All Channels				All Channels		Tutti i canali				Tutti i canali
141		32			15			Channel 1				Channel 1		Canale 1				Canale 1
142		32			15			Channel 2				Channel 2		Canale 2				Canale 2
143		32			15			Channel 3				Channel 3		Canale 3				Canale 3
144		32			15			Channel 4				Channel 4		Canale 4				Canale 4
145		32			15			Channel 5				Channel 5		Canale 5				Canale 5
146		32			15			Channel 6				Channel 6		Canale 6				Canale 6
147		32			15			Channel 7				Channel 7		Canale 7				Canale 7
148		32			15			Channel 8				Channel 8		Canale 8				Canale 8
149		32			15			Channel 9				Channel 9		Canale 9				Canale 9
150		32			15			Channel 10				Channel 10		Canale 10				Canale 10
151		32			15			Channel 11				Channel 11		Canale 11				Canale 11
152		32			15			Channel 12				Channel 12		Canale 12				Canale 12
153		32			15			Channel 13				Channel 13		Canale 13				Canale 13
154		32			15			Channel 14				Channel 14		Canale 14				Canale 14
155		32			15			Channel 15				Channel 15		Canale 15				Canale 15
156		32			15			Channel 16				Channel 16		Canale 16				Canale 16
157		32			15			Channel 17				Channel 17		Canale 17				Canale 17
158		32			15			Channel 18				Channel 18		Canale 18				Canale 18
159		32			15			Channel 19				Channel 19		Canale 19				Canale 19
160		32			15			Channel 20				Channel 20		Canale 20				Canale 20
161		32			15			Hall Calibrate Channel			CalibrateChan		Canale Calibrazione Sala		Calibraz Canal
162		32			15			Hall Coeff 1				Hall Coeff 1		Coeff 1 Sala				Coeff 1 Sala
163		32			15			Hall Coeff 2				Hall Coeff 2		Coeff 2 Sala				Coeff 2 Sala
164		32			15			Hall Coeff 3				Hall Coeff 3		Coeff 3 Sala				Coeff 3 Sala
165		32			15			Hall Coeff 4				Hall Coeff 4		Coeff 4 Sala				Coeff 4 Sala
166		32			15			Hall Coeff 5				Hall Coeff 5		Coeff 5 Sala				Coeff 5 Sala
167		32			15			Hall Coeff 6				Hall Coeff 6		Coeff 6 Sala				Coeff 6 Sala
168		32			15			Hall Coeff 7				Hall Coeff 7		Coeff 7 Sala				Coeff 7 Sala
169		32			15			Hall Coeff 8				Hall Coeff 8		Coeff 8 Sala				Coeff 8 Sala
170		32			15			Hall Coeff 9				Hall Coeff 9		Coeff 9 Sala				Coeff 9 Sala
171		32			15			Hall Coeff 10				Hall Coeff 10		Coeff 10 Sala				Coeff 10 Sala
172		32			15			Hall Coeff 11				Hall Coeff 11		Coeff 11 Sala				Coeff 11 Sala
173		32			15			Hall Coeff 12				Hall Coeff 12		Coeff 12 Sala				Coeff 12 Sala
174		32			15			Hall Coeff 13				Hall Coeff 13		Coeff 13 Sala				Coeff 13 Sala
175		32			15			Hall Coeff 14				Hall Coeff 14		Coeff 14 Sala				Coeff 14 Sala
176		32			15			Hall Coeff 15				Hall Coeff 15		Coeff 15 Sala				Coeff 15 Sala
177		32			15			Hall Coeff 16				Hall Coeff 16		Coeff 16 Sala				Coeff 16 Sala
178		32			15			Hall Coeff 17				Hall Coeff 17		Coeff 17 Sala				Coeff 17 Sala
179		32			15			Hall Coeff 18				Hall Coeff 18		Coeff 18 Sala				Coeff 18 Sala
180		32			15			Hall Coeff 19				Hall Coeff 19		Coeff 19 Sala				Coeff 19 Sala
181		32			15			Hall Coeff 20				Hall Coeff 20		Coeff 20 Sala				Coeff 20 Sala
182		32			15			All Days				All Days		Ogni Giorno				Ogni Giorno
183		32			15			SMDUH 6					SMDUH 6			SMDUH 6					SMDUH 6
184		32			15			Reset Energy Channel X			RstEnergyChanX		Azzera Energia Canale X			Reset Enrg Can X
185		32			15			Load1 Alarm Flag			Load1 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
186		32			15			Load2 Alarm Flag			Load2 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
187		32			15			Load3 Alarm Flag			Load3 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
188		32			15			Load4 Alarm Flag			Load4 Alarm Flag	Load4 Alarm Flag			Load4 Alarm Flag
189		32			15			Load5 Alarm Flag			Load5 Alarm Flag	Load5 Alarm Flag			Load5 Alarm Flag
190		32			15			Load6 Alarm Flag			Load6 Alarm Flag	Load6 Alarm Flag			Load6 Alarm Flag
191		32			15			Load7 Alarm Flag			Load7 Alarm Flag	Load7 Alarm Flag			Load7 Alarm Flag
192		32			15			Load8 Alarm Flag			Load8 Alarm Flag	Load8 Alarm Flag			Load8 Alarm Flag
193		32			15			Load9 Alarm Flag			Load9 Alarm Flag	Load9 Alarm Flag			Load9 Alarm Flag
194		32			15			Load10 Alarm Flag			Load10 Alarm Flag	Load10 Alarm Flag			Load10 Alarm Flag
195		32			15			Load11 Alarm Flag			Load11 Alarm Flag	Load11 Alarm Flag			Load11 Alarm Flag
196		32			15			Load12 Alarm Flag			Load12 Alarm Flag	Load12 Alarm Flag			Load12 Alarm Flag
197		32			15			Load13 Alarm Flag			Load13 Alarm Flag	Load13 Alarm Flag			Load13 Alarm Flag
198		32			15			Load14 Alarm Flag			Load14 Alarm Flag	Load14 Alarm Flag			Load14 Alarm Flag
199		32			15			Load15 Alarm Flag			Load15 Alarm Flag	Load15 Alarm Flag			Load15 Alarm Flag
200		32			15			Load16 Alarm Flag			Load16 Alarm Flag	Load16 Alarm Flag			Load16 Alarm Flag
201		32			15			Load17 Alarm Flag			Load17 Alarm Flag	Load17 Alarm Flag			Load17 Alarm Flag
202		32			15			Load18 Alarm Flag			Load18 Alarm Flag	Load18 Alarm Flag			Load18 Alarm Flag
203		32			15			Load19 Alarm Flag			Load19 Alarm Flag	Load19 Alarm Flag			Load19 Alarm Flag
204		32			15			Load20 Alarm Flag			Load20 Alarm Flag	Load20 Alarm Flag			Load20 Alarm Flag
205		32			15			Current1 High Current			Curr 1 Hi		Current1 High Current			Curr 1 Hi
206		32			15			Current1 Very High Current		Curr 1 Very Hi		Current1 Very High Current		Curr 1 Very Hi
207		32			15			Current2 High Current			Curr 2 Hi		Current2 High Current			Curr 2 Hi
208		32			15			Current2 Very High Current		Curr 2 Very Hi		Current2 Very High Current		Curr 2 Very Hi
209		32			15			Current3 High Current			Curr 3 Hi		Current3 High Current			Curr 3 Hi
210		32			15			Current3 Very High Current		Curr 3 Very Hi		Current3 Very High Current		Curr 3 Very Hi
211		32			15			Current4 High Current			Curr 4 Hi		Current4 High Current			Curr 4 Hi
212		32			15			Current4 Very High Current		Curr 4 Very Hi		Current4 Very High Current		Curr 4 Very Hi
213		32			15			Current5 High Current			Curr 5 Hi		Current5 High Current			Curr 5 Hi
214		32			15			Current5 Very High Current		Curr 5 Very Hi		Current5 Very High Current		Curr 5 Very Hi
215		32			15			Current6 High Current			Curr 6 Hi		Current6 High Current			Curr 6 Hi
216		32			15			Current6 Very High Current		Curr 6 Very Hi		Current6 Very High Current		Curr 6 Very Hi
217		32			15			Current7 High Current			Curr 7 Hi		Current7 High Current			Curr 7 Hi
218		32			15			Current7 Very High Current		Curr 7 Very Hi		Current7 Very High Current		Curr 7 Very Hi
219		32			15			Current8 High Current			Curr 8 Hi		Current8 High Current			Curr 8 Hi
220		32			15			Current8 Very High Current		Curr 8 Very Hi		Current8 Very High Current		Curr 8 Very Hi
221		32			15			Current9 High Current			Curr 9 Hi		Current9 High Current			Curr 9 Hi
222		32			15			Current9 Very High Current		Curr 9 Very Hi		Current9 Very High Current		Curr 9 Very Hi
223		32			15			Current10 High Current			Curr 10 Hi		Current10 High Current			Curr 10 Hi
224		32			15			Current10 Very High Current		Curr 10 Very Hi		Current10 Very High Current		Curr 10 Very Hi
225		32			15			Current11 High Current			Curr 11 Hi		Current11 High Current			Curr 11 Hi
226		32			15			Current11 Very High Current		Curr 11 Very Hi		Current11 Very High Current		Curr 11 Very Hi
227		32			15			Current12 High Current			Curr 12 Hi		Current12 High Current			Curr 12 Hi
228		32			15			Current12 Very High Current		Curr 12 Very Hi		Current12 Very High Current		Curr 12 Very Hi
229		32			15			Current13 High Current			Curr 13 Hi		Current13 High Current			Curr 13 Hi
230		32			15			Current13 Very High Current		Curr 13 Very Hi		Current13 Very High Current		Curr 13 Very Hi
231		32			15			Current14 High Current			Curr 14 Hi		Current14 High Current			Curr 14 Hi
232		32			15			Current14 Very High Current		Curr 14 Very Hi		Current14 Very High Current		Curr 14 Very Hi
233		32			15			Current15 High Current			Curr 15 Hi		Current15 High Current			Curr 15 Hi
234		32			15			Current15 Very High Current		Curr 15 Very Hi		Current15 Very High Current		Curr 15 Very Hi
235		32			15			Current16 High Current			Curr 16 Hi		Current16 High Current			Curr 16 Hi
236		32			15			Current16 Very High Current		Curr 16 Very Hi		Current16 Very High Current		Curr 16 Very Hi
237		32			15			Current17 High Current			Curr 17 Hi		Current17 High Current			Curr 17 Hi
238		32			15			Current17 Very High Current		Curr 17 Very Hi		Current17 Very High Current		Curr 17 Very Hi
239		32			15			Current18 High Current			Curr 18 Hi		Current18 High Current			Curr 18 Hi
240		32			15			Current18 Very High Current		Curr 18 Very Hi		Current18 Very High Current		Curr 18 Very Hi
241		32			15			Current19 High Current			Curr 19 Hi		Current19 High Current			Curr 19 Hi
242		32			15			Current19 Very High Current		Curr 19 Very Hi		Current19 Very High Current		Curr 19 Very Hi
243		32			15			Current20 High Current			Curr 20 Hi		Current20 High Current			Curr 20 Hi
244		32			15			Current20 Very High Current		Curr 20 Very Hi		Current20 Very High Current		Curr 20 Very Hi
