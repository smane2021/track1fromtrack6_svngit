﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			IB					IB			IB					IB
8		32			15			Digital Input 1				DI 1			DI 1					DI 1
9		32			15			Digital Input 2				DI 2			DI 2					DI 2
10		32			15			Digital Input 3				DI 3			DI 3					DI 3
11		32			15			Digital Input 4				DI 4			DI 4					DI 4
12		32			15			Digital Input 5				DI 5			DI 5					DI 5
13		32			15			Digital Input 6				DI 6			DI 6					DI 6
14		32			15			Digital Input 7				DI 7			DI 7					DI 7
15		32			15			Digital Input 8				DI 8			DI 8					DI 8
16		32			15			Open					Open			Aperto					Aperto
17		32			15			Closed					Closed			Chiuso					Chiuso
18		32			15			Relay Output 1				Relay Output 1		Relé uscita 1				Relé 1
19		32			15			Relay Output 2				Relay Output 2		Relé uscita 2				Relé 2
20		32			15			Relay Output 3				Relay Output 3		Relé uscita 3				Relé 3
21		32			15			Relay Output 4				Relay Output 4		Relé uscita 4				Relé 4
22		32			15			Relay Output 5				Relay Output 5		Relé uscita 5				Relé 5
23		32			15			Relay Output 6				Relay Output 6		Relé uscita 6				Relé 6
24		32			15			Relay Output 7				Relay Output 7		Relé uscita 7				Relé 7
25		32			15			Relay Output 8				Relay Output 8		Relé uscita 8				Relé 8
26		32			15			DI1 Alarm State				DI1 Alarm State		Stato allarme DI1			Stato DI1
27		32			15			DI2 Alarm State				DI2 Alarm State		Stato allarme DI2			Stato DI2
28		32			15			DI3 Alarm State				DI3 Alarm State		Stato allarme DI3			Stato DI3
29		32			15			DI4 Alarm State				DI4 Alarm State		Stato allarme DI4			Stato DI4
30		32			15			DI5 Alarm State				DI5 Alarm State		Stato allarme DI5			Stato DI5
31		32			15			DI6 Alarm State				DI6 Alarm State		Stato allarme DI6			Stato DI6
32		32			15			DI7 Alarm State				DI7 Alarm State		Stato allarme DI7			Stato DI7
33		32			15			DI8 Alarm State				DI8 Alarm State		Stato allarme DI8			Stato DI8
34		32			15			State					State			Stato					Stato
35		32			15			Communication Failure			Comm Fail		Guasto IB				Guasto IB
36		32			15			Barcode					Barcode			Barcode					Barcode
37		32			15			On					On			ACCESO					ACCESO
38		32			15			Off					Off			SPENTO					SPENTO
39		32			15			High					High			Alto					Alto
40		32			15			Low					Low			Basso					Basso
41		32			15			DI1 Alarm				DI1 Alarm		Allarme DI1				Allarme DI1
42		32			15			DI2 Alarm				DI2 Alarm		Allarme DI2				Allarme DI2
43		32			15			DI3 Alarm				DI3 Alarm		Allarme DI3				Allarme DI3
44		32			15			DI4 Alarm				DI4 Alarm		Allarme DI4				Allarme DI4
45		32			15			DI5 Alarm				DI5 Alarm		Allarme DI5				Allarme DI5
46		32			15			DI6 Alarm				DI6 Alarm		Allarme DI6				Allarme DI6
47		32			15			DI7 Alarm				DI7 Alarm		Allarme DI7				Allarme DI7
48		32			15			DI8 Alarm				DI8 Alarm		Allarme DI8				Allarme DI8
78		32			15			Testing Relay 1				Testing Relay1		Prova Rele1				Prova Rele1
79		32			15			Testing Relay 2				Testing Relay2		Prova Rele2				Prova Rele2
80		32			15			Testing Relay 3				Testing Relay3		Prova Rele3				Prova Rele3
81		32			15			Testing Relay 4				Testing Relay4		Prova Rele4				Prova Rele4
82		32			15			Testing Relay 5				Testing Relay5		Prova Rele5				Prova Rele5
83		32			15			Testing Relay 6				Testing Relay6		Prova Rele6				Prova Rele6
84		32			15			Testing Relay 7				Testing Relay7		Prova Rele7				Prova Rele7
85		32			15			Testing Relay 8				Testing Relay8		Prova Rele8				Prova Rele8
86		32			15			Temp1					Temp1			Temp1					Temp1
87		32			15			Temp2					Temp2			Temp2					Temp2
88		32			15			DO1 Normal State  			DO1 Normal		DO1 Stato Normale				DO1 Normale
89		32			15			DO2 Normal State  			DO2 Normal		DO2 Stato Normale				DO2 Normale
90		32			15			DO3 Normal State  			DO3 Normal		DO3 Stato Normale				DO3 Normale
91		32			15			DO4 Normal State  			DO4 Normal		DO4 Stato Normale				DO4 Normale
92		32			15			DO5 Normal State  			DO5 Normal		DO5 Stato Normale				DO5 Normale
93		32			15			DO6 Normal State  			DO6 Normal		DO6 Stato Normale				DO6 Normale
94		32			15			DO7 Normal State  			DO7 Normal		DO7 Stato Normale				DO7 Normale
95		32			15			DO8 Normal State  			DO8 Normal		DO8 Stato Normale				DO8 Normale
96		32			15			Non-Energized				Non-Energized		Non-Energizzato			Non-Energizzato
97		32			15			Energized				Energized		Energizzato					Energizzato
