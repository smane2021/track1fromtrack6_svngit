﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Fuse Tripped			Batt Fuse Trip		Fusibile batt aperto			FusBatt aperto
2		32			15			Battery 2 Fuse Tripped			Bat 2 Fuse Trip		Fusibile batt2 aperto			FusBatt2 aperto
3		32			15			Battery 1 Voltage			Batt 1 Voltage		Tensione batteria 1			V Batt 1
4		32			15			Battery 1 Current			Batt 1 Current		Corrente batteria 1			I Batt 1
5		32			15			Battery 1 Temperature			Battery 1 Temp		Temperatura batteria 1			T Batt 1
6		32			15			Battery 2 Voltage			Batt 2 Voltage		Tensione batteria 2			V Batt 2
7		32			15			Battery 2 Current			Batt 2 Current		Corrente batteria 2			I Batt 2
8		32			15			Battery 2 Temperature			Battery 2 Temp		Temperatura batteria 2			T Batt 2
9		32			15			CSU Battery				CSU Battery		Batteria di CSU				Batt_CSU
10		32			15			CSU Battery Failure			CSU BatteryFail		Guasto batteria di CSU			Gsto Batt_CSU
11		32			15			No					No			No					No
12		32			15			Yes					Yes			Sì					Sì
13		32			15			Battery 2 Connected			Bat 2 Connected		Batteria 2 connessa			Batt2 connessa
14		32			15			Battery 1 Connected			Bat 1 Connected		Batteria 1 connessa			Batt1 connessa
15		32			15			Existent				Existent		Esistente				Esistente
16		32			15			Not Existent				Not Existent		Non esistente				Non esistente
