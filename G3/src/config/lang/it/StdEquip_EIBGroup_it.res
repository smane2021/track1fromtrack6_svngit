﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			EIB Group				EIB Group		Gruppo EIB				Gruppo EIB
2		32			15			Load Current				Load Current		Corrente di carico			Corrente
3		32			15			DC Distribution				DC			Distribuzione CC			Distr CC
4		32			15			Load Shunt				Load Shunt		Shunt di carico SCU			Shunt caric SCU
5		32			15			Disabled				Disabled		Disabilitato				Disabilitato
6		32			15			Enabled					Enabled			Abilitato				Abilitato
7		32			15			Existence State				Existence State		Stato attuale				Stato attuale
8		32			15			Existent				Existent		Esistente				Esistente
9		32			15			Non-Existent				Non-Existent		Inesistente				Inesistente
