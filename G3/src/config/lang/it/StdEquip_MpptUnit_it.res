﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Solar Converter				Solar Conv		Convertitore Solare			ConvSolare
2		32			15			DC Status				DC Status		Stato CC				StatoCC
3		32			15			DC Output Voltage			DC Voltage		Tensione Uscita CC			Vcc Usc
4		32			15			Origin Current				Origin Current		Corrente				Corrente
5		32			15			Temperature				Temperature		Temperatura				Temperatura
6		32			15			Used Capacity				Used Capacity		Capacità				Capacità
7		32			15			Input Voltage				Input Voltage		Tensione Ingresso			Vin
8		32			15			DC Output Current			DC Current		Corrente Uscita CC			Icc Usc
9		32			15			SN					SN			SN					S
10		32			15			Total Running Time			Running Time		Tempo Funzionamento			TempoFunz
11		32			15			Communication Fail Count		Comm Fail Count		Cont Guasto Comm			ConGstoComm
13		32			15			Derated by Temp				Derated by Temp		Deriva per Temp				DerivaT°
14		32			15			Derated					Derated			In deriva				In Deriva
15		32			15			Full Fan Speed				Full Fan Speed		Max Velocità ventola			MaxVelcVent
16		32			15			Walk-In					Walk-In			Riavvio					Riavvio
18		32			15			Current Limit				Current Limit		Limite corrente				LimitCorr
19		32			15			Voltage High Limit			Volt Hi-limit		Limite Alta Tensione			LimValta
20		32			15			Solar Status				Solar Status		Stato Solare				Stato Solare
21		32			15			Solar Converter Temperature High	SolarConvTempHi		AltaTemp Convertitore			AltaTempConv
22		32			15			Solar Converter Fault			SolarConv Fault		Guasto Convertitore			GstoConv
23		32			15			Solar Converter Protected		SolarConv Prot		Convertitore in protez			ProtezConv
24		32			15			Fan Failure				Fan Failure		Guasto Ventola				GstoVentola
25		32			15			Current Limit State			Curr Lmt State		Limitz Corrente				I Limitaz
26		32			15			EEPROM Failure				EEPROM Failure		GuastoEEPROM				GstoEEPROM
27		32			15			DC On/Off Control			DC On/Off Ctrl		Controllo CC				CtrlCC
29		32			15			LED Control				LED Control		Controllo LED				CtrlLED
30		32			15			Solar Converter Reset			SolarConvReset		Reset Convertitore			ResetConv
31		32			15			Input Fail				Input Fail		Input Fail				Input Fail
34		32			15			Over Voltage				Over Voltage		Sovratensione				SovraV
37		32			15			Current Limit				Current Limit		Limitaz Corrente			LimitzCorr
39		32			15			Normal					Normal			Normale					Normale
40		32			15			Limited					Limited			Limitata				Limitata
45		32			15			Normal					Normal			Normale					Normale
46		32			15			Full					Full			Piena					Piena
47		32			15			Disabled				Disabled		Disabilitata				Disabilit
48		32			15			Enabled					Enabled			Abilitata				Abilit
49		32			15			On					On			Acceso					Acceso
50		32			15			Off					Off			Spento					Spento
51		32			15			Day					Day			Giorno					Giorno
52		32			15			Failure					Failure			Guasto					Guasto
53		32			15			Normal					Normal			Normale					Normale
54		32			15			Over Temperature			Over Temp		SovraTemperatura			SovraTemp
55		32			15			Normal					Normal			Normale					Normale
56		32			15			Fault					Fault			Guasto					Guasto
57		32			15			Normal					Normal			Normale					Guasto
58		32			15			Protected				Protected		Protetto				Protetto
59		32			15			Normal					Normal			Normale					Normale
60		32			15			Failure					Failure			Guasto					Guasto
61		32			15			Normal					Normal			Normale					Normale
62		32			15			Alarm					Alarm			Allarme					Allarme
63		32			15			Normal					Normal			Normale					Normale
64		32			15			Failure					Failure			Guasto					Guasto
65		32			15			Off					Off			Spento					Spento
66		32			15			On					On			Acceso					Acceso
67		32			15			Off					Off			Spento					Spento
68		32			15			On					On			Acceso					Acceso
69		32			15			LED Control				LED Control		Controllo LED				CtrlLED
70		32			15			Cancel					Cancel			Cancella				Cancella
71		32			15			Off					Off			Spento					Spento
72		32			15			Reset					Reset			Reset					Reset
73		32			15			Solar Converter On			Solar Conv On		Convertitore ON				ConvertON
74		32			15			Solar Converter Off			Solar Conv Off		Convertitore OFF			ConvertOFF
75		32			15			Off					Off			Spento					Spento
76		32			15			LED Control				LED Control		Controllo LED				CtrlLED
77		32			15			Solar Converter Reset			SolarConv Reset		Reset Convertitore			ResetConv
80		34			15			Solar Converter Communication Fail	SolConvCommFail		GuastoComm Convertitore			GstoCommConv
84		32			15			Solar Converter High SN			SolConvHighSN		Alto SN Convertitore			SN Conv
85		32			15			Solar Converter Version			SolarConv Ver		Versione Convertitore			VersConv
86		32			15			Solar Converter Part Number		SolarConvPartNo		Numero Convertitore			N°Conv
87		32			15			Current Share State			Current Share		Ripartizione Corrente			RipartzCorr
88		32			15			Current Share Alarm			Curr Share Alm		Allarme Ripartiz Corrente		AllRipartzCorr
89		32			15			Over Voltage				Over Voltage		Sovratensione				Sovratensione
90		32			15			Normal					Normal			Normale					Normale
91		32			15			Over Voltage				Over Voltage		Sovratensione				Sovratensione
95		32			15			Low Voltage				Low Voltage		Bassa Tensione				BassaTensione
96		32			15			Input Under Voltage Protection		Low Input Protect	Input Under Voltage Protection		Low Input Protect
97		32			15			Solar Converter ID			Solar Conv ID		ID Convertitore				IDConv
98		32			15			DC Output Shut Off			DC Output Off		Uscita CC Spenta			UscCCSpenta
99		32			15			Solar Converter Phase			SolarConvPhase		Fase Convertitore			FaseConv
103		32			15			Severe Current Share Alarm		SevereCurrShare		Grave Allarme Ripart Corrente		GrveAllRipartz
104		32			15			Barcode 1				Barcode 1		Bar Code1				Bar Code1
105		32			15			Barcode 2				Barcode 2		Bar Code2				Bar Code2
106		32			15			Barcode 3				Barcode 3		Bar Code3				Bar Code3
107		32			15			Barcode 4				Barcode 4		Bar Code4				Bar Code4
108		34			15			Solar Converter Communication Fail	SolConvComFail		Guasto Comm Convertitore		GstoCommConv
109		32			15			No					No			No					No
110		32			15			Yes					Yes			Sì					Sì
111		32			15			Existence State				Existence State		Stato Attuale				Stato Attuale
113		32			15			Comm OK					Comm OK			Comm OK					CommOK
114		32			15			All Solar Converters Comm Fail		AllSolConvComF		Guasto Comm Convertitori		GstoCommConv
115		32			15			Communication Fail			Comm Fail		Guasto Comm				GstoComm
116		32			15			Valid Rated Current			Rated Current		Corrente Nominale			CorrNom
117		32			15			Efficiency				Efficiency		Efficienza				Efficienza
118		32			15			LT 93					LT 93			LT 93					LT 93
119		32			15			GT 93					GT 93			GT 93					GT 93
120		32			15			GT 95					GT 95			GT 95					GT 95
121		32			15			GT 96					GT 96			GT 96					GT 96
122		32			15			GT 97					GT 97			GT 97					GT 97
123		32			15			GT 98					GT 98			GT 98					GT 98
124		32			15			GT 99					GT 99			GT 99					GT 99
125		32			15			Solar Converter HVSD Status		HVSD Status		Stato HVSD				StatoHVSD
126		32			15			Solar Converter Reset			SolConvResetEEM		Reset Convertitore			ResetConv
127		32			15			Night					Night			Notte					Notte
128		32			15			Input Current				Input Current		Corrente Ingresso			Iingresso
129		32			15			Input Power				Input Power		Input Power				Input Power
130		32			15			Input Not DC				Input Not DC		Input Not DC				Input Not DC
131		32			15			Output Power				Output Power		Output Power				Output Power
