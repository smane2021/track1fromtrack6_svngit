﻿#
# Locale language support: it
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			SMDUHH Group				SMDUHH Group		Gruppo SMDUHH				Gruppo SMDUHH
2		32			15			Standby					Standby			Allarme					Allarme
3		32			15			Refresh					Refresh			Reset					Reset
4		32			15			Setting Refresh				Setting Refresh		Reset impostazioni			Reset impost
5		32			15			E-Stop					E-Stop			Funzione E-Stop				Funz E-Stop
6		32			15			Yes					Yes			Sí					Sí
7		32			15			Existence State				Existence State		Stato attuale				Stato attuale
8		32			15			Existent				Existent		Esistente				Esistente
9		32			15			Not Existent				Not Existent		Inesistente				Inesistente
10		32			15			Number of SMDUHHs			Num of SMDUHHs		Numero di SMDUHH			Num SMDUHH
11		32			15			SMDUHH Config Changed			Cfg Changed		Config SMDUHH cambiata			Cfg cambiata
12		32			15			Not Changed				Not Changed		Non cambiata				Non cambiata
13		32			15			Changed					Changed			Cambiata				Cambiata														