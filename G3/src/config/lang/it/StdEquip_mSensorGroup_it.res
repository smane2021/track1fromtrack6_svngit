﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			mSensor Group		mSensor Group					mSensor Gruppo		mSensor Gruppo				
																					
4		32			15			Normal				Normal							Normale				Normale						
5		32			15			Fail				Fail							Fallire				Fallire					
6		32			15			Yes					Yes								sì				sì							
7		32			15			Existence State		Existence State					Stato Esistenza		Stato Esistenza				
8		32			15			Existent			Existent						Esistente			Esistente					
9		32			15			Not Existent		Not Existent					Non Esiste		Non Esiste				
10		32			15			Number of mSensor	Num of mSensor					Numero di mSensor	Num di mSensor				
11		32			15			All mSensor Comm Fail			AllmSenComFail		Tutti mSensor Comm Fail			TutmSenComFail	
12		32			15			Interval of Impedance Test		ImpedTestInterv		IntervTestImpedenza		IntervTestImped	
13		32			15			Hour of Impedance Test			ImpedTestHour		Hour of Impedance Test			ImpedTestHour	
14		32			15			Time of Last Impedance Test		LastTestTime		Ultimo Tempo Prova		UltiTempProva	
