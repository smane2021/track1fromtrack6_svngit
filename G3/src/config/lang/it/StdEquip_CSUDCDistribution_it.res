﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Distribution Fuse Tripped		Dist Fuse Trip		Fusibile distribuzione aperto		FusDistr aperto
2		32			15			Contactor 1 Failure			Contactor1 Fail		Guasto Contattore 1			Contatt1 gsto
3		32			15			Distribution Fuse 2 Tripped		Dist Fuse2 Trip		Fusibile distribuzione2 aperto		FusDistr2 apert
4		32			15			Contactor 2 Failure			Contactor2 Fail		Guasto Contattore 2			Contatt2 gsto
5		32			15			Distribution Voltage			Distr Voltage		Tensione di distribuzione		V distr
6		32			15			Distribution Current			Distr Current		Corrente di distribuzione		I distr
7		32			15			Distribution Temperature		Distr Temp		Temperatura di distribuzione		T distr
8		32			15			Distribution 2 Current			Distr 2 Current		Corrente di distribuzione2		I distr 2
9		32			15			Distribution 2 Voltage			Distr 2 Voltage		Tensione di distribuzione2		V distr 2
10		32			15			CSU DC Distribution			CSU DC Distr		Distribuzione CC di CSU			Distr CC_CSU
11		32			15			CSU DC Distribution Failure		CSU DCDist Fail		Guasto distribuzione CC di CSU		Gsto CC_CSU
12		32			15			No					No			No					No
13		32			15			Yes					Yes			Sì					Sì
14		32			15			Existent				Existent		Esistente				Esistente
15		32			15			Not Existent				Not Existent		Non esistente				Non esistente
