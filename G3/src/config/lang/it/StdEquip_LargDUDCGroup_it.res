﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			LVD1					LVD1			LVD1					LVD1
2		32			15			LVD2					LVD2			LVD2					LVD2
3		32			15			LVD3					LVD3			LVD3					LVD3
4		32			15			DC Distribution Number			DC Distr Num		Num distribuz CC			Num distrib
5		32			15			LargDU DC Distribution Group		Large DC Group		Gruppo distribuz grande			Gruppo granDist
6		32			15			High Temperature 1 Limit		High Temp1		Limite alta temperatura 1		Lim alta Temp1
7		32			15			High Temperature 2 Limit		High Temp2		Limite alta temperatura 2		Lim alta Temp2
8		32			15			High Temperature 3 Limit		High Temp3		Limite alta temperatura 3		Lim alta Temp3
9		32			15			Low Temperature 1 Limit			Low Temp1		Limite bassa temperatura 1		Lim bassa Temp1
10		32			15			Low Temperature 2 Limit			Low Temp2		Limite bassa temperatura 2		Lim bassa Temp2
11		32			15			Low Temperature 3 Limit			Low Temp3		Limite bassa temperatura 3		Lim bassa Temp3
12		32			15			LVD1 Voltage				LVD1 Voltage		Tensione LVD1				Tensione LVD1
13		32			15			LVD2 Voltage				LVD2 Voltage		Tensione LVD2				Tensione LVD2
14		32			15			LVD3 Voltage				LVD3 Voltage		Tensione LVD3				Tensione LVD3
15		32			15			Overvoltage				Overvoltage		Sovratensione				Sovratensione
16		32			15			Undervoltage				Undervoltage		Sottotensione				Sottotensione
17		32			15			On					On			Connesso				Connesso
18		32			15			Off					Off			Disconnesso				Disconnesso
19		32			15			On					On			Connesso				Connesso
20		32			15			Off					Off			Disconnesso				Disconnesso
21		32			15			On					On			Connesso				Connesso
22		32			15			Off					Off			Disconnesso				Disconnesso
23		32			15			Total Load Current			Total Load		Corrente totale carico			Corrente totale
24		32			15			Total DCD Current			Total DCD Curr		Corrente totale distrib			Total corr dist
25		32			15			DCDistribution Average Voltage		DCD Average Volt	Tensione media distribuzione		Tens media dist
26		32			15			LVD1 Enabled				LVD1 Enabled		LVD1 abilitato				LVD1 abilitato
27		32			15			LVD1 Mode				LVD1 Mode		Modo LVD1				Modo LVD1
28		32			15			LVD1 Time				LVD1 Time		Tempo LVD1				Tempo LVD1
29		32			15			LVD1 Reconnect Voltage			LVD1 ReconVolt		Tensione riconnessione LVD1		Tens ricn LVD1
30		32			15			LVD1 Reconnect Delay			LVD1 ReconDelay		Ritardo riconnessione LVD1		Rit ricn LVD1
31		32			15			LVD1 Dependency				LVD1 Depend		Dipendenza LVD1				Dipend LVD1
32		32			15			LVD2 Enabled				LVD2 Enabled		LVD2 abilitato				LVD2 abilitato
33		32			15			LVD2 Mode				LVD2 Mode		Modo LVD2				Modo LVD2
34		32			15			LVD2 Time				LVD2 Time		Tempo LVD2				Tempo LVD2
35		32			15			LVD2 Reconnect Voltage			LVD2 ReconVolt		Tensione riconnessione LVD2		Tens ricn LVD2
36		32			15			LVD2 Reconnect Delay			LVD2 ReconDelay		Ritardo riconnessione LVD2		Rit ricn LVD2
37		32			15			LVD2 Dependency				LVD2 Depend		Dipendenza LVD2				Dipend LVD2
38		32			15			LVD3 Enabled				LVD3 Enabled		LVD3 abilitato				LVD3 abilitato
39		32			15			LVD3 Mode				LVD3 Mode		Modo LVD3				Modo LVD3
40		32			15			LVD3 Time				LVD3 Time		Tempo LVD3				Tempo LVD3
41		32			15			LVD3 Reconnect Voltage			LVD3 ReconVolt		Tensione riconnessione LVD3		Tens ricn LVD3
42		32			15			LVD3 Reconnect Delay			LVD3 ReconDelay		Ritardo riconnessione LVD3		Rit ricn LVD3
43		32			15			LVD3 Dependency				LVD3 Depend		Dipendenza LVD3				Dipend LVD3
44		32			15			Disabled				Disabled		Disabilitato				Disabilitato
45		32			15			Enabled					Enabled			Abilitato				Abilitato
46		32			15			Voltage					Voltage			Tensione				Tensione
47		32			15			Time					Time			Tempo					Tempo
48		32			15			None					None			No					No
49		32			15			LVD1					LVD1			LVD1					LVD1
50		32			15			LVD2					LVD2			LVD2					LVD2
51		32			15			LVD3					LVD3			LVD3					LVD3
52		32			15			Number of LVDs				No of LVDs		Numero di LVD				Num LVD
53		32			15			0					0			0					0
54		32			15			1					1			1					1
55		32			15			2					2			2					2
56		32			15			3					3			3					3
57		32			15			Existence State				Existence State		Stato attuale				Stato attuale
58		32			15			Existent				Existent		Esistente				Esistente
59		32			15			Non-Existent				Non-Existent		Inesistente				Inesistente
60		32			15			Battery Overvoltage			Batt OverVolt		Sovratensione batteria			Sovratens bat
61		32			15			Battery Undervoltage			Batt UnderVolt		Sottotensione batteria			Sottotens bat
