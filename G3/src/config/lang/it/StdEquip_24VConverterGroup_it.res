﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			24V Converter Group			24V Conv Group		Gruppo Convertitore 24V			Grp Conv 24V
19		32			15			Shunt 3 Rated Current			Shunt 3 Current		Shunt Corrente Totale			Corrente Shunt 3
21		32			15			Shunt 3 Rated Voltage			Shunt 3 Voltage		Shunt Tensione Totale			Tensione Shunt 3
26		32			15			Closed					Closed			Chiuso					Chiuso
27		32			15			Open					Open			Aperto					Aperto
29		32			15			No					No			No					No
30		32			15			Yes					Yes			Sì					Sì
3002		32			15			Converter Installed			Conv Installed		Converter Installed			Conv Installed
3003		32			15			No					No			No					No
3004		32			15			Yes					Yes			Sì					Sì
3005		32			15			Under Voltage				Under Volt		Sottotensione				Sotto V
3006		32			15			Over Voltage				Over Volt		Sovratensione				Sovra V
3007		32			15			Over Current				Over Current		Sovracorrente				Sovracorrente
3008		32			15			Under Voltage				Under Volt		Sottotensione				Sotto V
3009		32			15			Over Voltage				Over Volt		Sovratensione				Sovra V
3010		32			15			Over Current				Over Current		Sovracorrente				Sovracorrente
3011		32			15			Voltage					Voltage			Tensione				Tensione
3012		32			15			Total Current				Total Current		Corrente Totale				Corrente Totale
3013		32			15			Input Current				Input Current		Corrente Ingresso			Corrente Ingresso
3014		32			15			Efficiency				Efficiency		Rendimento				Rendimento
