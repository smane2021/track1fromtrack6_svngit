﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		Tensione barra distribuzione		Tens barra dist
2		32			15			Load 1 Current				Load1 Current		Corrente carico 1			Corrente 1
3		32			15			Load 2 Current				Load2 Current		Corrente carico 2			Corrente 2
4		32			15			Load 3 Current				Load3 Current		Corrente carico 3			Corrente 3
5		32			15			Load 4 Current				Load4 Current		Corrente carico 4			Corrente 4
6		32			15			Load Fuse 1				Load Fuse1		Fusibile carico 1			Fus carico1
7		32			15			Load Fuse 2				Load Fuse2		Fusibile carico 2			Fus carico2
8		32			15			Load Fuse 3				Load Fuse3		Fusibile carico 3			Fus carico3
9		32			15			Load Fuse 4				Load Fuse4		Fusibile carico 4			Fus carico4
10		32			15			Load Fuse 5				Load Fuse5		Fusibile carico 5			Fus carico5
11		32			15			Load Fuse 6				Load Fuse6		Fusibile carico 6			Fus carico6
12		32			15			Load Fuse 7				Load Fuse7		Fusibile carico 7			Fus carico7
13		32			15			Load Fuse 8				Load Fuse8		Fusibile carico 8			Fus carico8
14		32			15			Load Fuse 9				Load Fuse9		Fusibile carico 9			Fus carico9
15		32			15			Load Fuse 10				Load Fuse10		Fusibile carico 10			Fus carico10
16		32			15			Load Fuse 11				Load Fuse11		Fusibile carico 11			Fus carico11
17		32			15			Load Fuse 12				Load Fuse12		Fusibile carico 12			Fus carico12
18		32			15			Load Fuse 13				Load Fuse13		Fusibile carico 13			Fus carico13
19		32			15			Load Fuse 14				Load Fuse14		Fusibile carico 14			Fus carico14
20		32			15			Load Fuse 15				Load Fuse15		Fusibile carico 15			Fus carico15
21		32			15			Load Fuse 16				Load Fuse16		Fusibile carico 16			Fus carico16
22		32			15			Run Time				Run Time		Tempo di funzionamento			Tempo funz
23		32			15			LVD1 Control				LVD1 Control		Controllo LVD1				Ctrl LVD1
24		32			15			LVD2 Control				LVD2 Control		Controllo LVD2				Ctrl LVD2
25		32			15			LVD1 Voltage				LVD1 Voltage		Tensione LVD1				Tens LVD1
26		32			15			LVR1 Voltage				LVR1 Voltage		Tensione LVR1				Tens LVR1
27		32			15			LVD2 Voltage				LVD2 Voltage		Tensione LVD2				Tens LVD2
28		32			15			LVR2 voltage				LVR2 voltage		Tensione LVR2				Tens LVR2
29		32			15			On					On			ACCESO					ACCESO
30		32			15			Off					Off			SPENTO					SPENTO
31		32			15			Normal					Normal			Normale					Normale
32		32			15			Error					Error			Errore					Errore
33		32			15			On					On			ACCESO					ACCESO
34		32			15			Fuse 1 Alarm				Fuse1 Alarm		Allarme fusibile1			All fus1
35		32			15			Fuse 2 Alarm				Fuse2 Alarm		Allarme fusibile2			All fus2
36		32			15			Fuse 3 Alarm				Fuse3 Alarm		Allarme fusibile3			All fus3
37		32			15			Fuse 4 Alarm				Fuse4 Alarm		Allarme fusibile4			All fus4
38		32			15			Fuse 5 Alarm				Fuse5 Alarm		Allarme fusibile5			All fus5
39		32			15			Fuse 6 Alarm				Fuse6 Alarm		Allarme fusibile6			All fus6
40		32			15			Fuse 7 Alarm				Fuse7 Alarm		Allarme fusibile7			All fus7
41		32			15			Fuse 8 Alarm				Fuse8 Alarm		Allarme fusibile8			All fus8
42		32			15			Fuse 9 Alarm				Fuse9 Alarm		Allarme fusibile9			All fus9
43		32			15			Fuse 10 Alarm				Fuse10 Alarm		Allarme fusibile10			All fus10
44		32			15			Fuse 11 Alarm				Fuse11 Alarm		Allarme fusibile11			All fus11
45		32			15			Fuse 12 Alarm				Fuse12 Alarm		Allarme fusibile12			All fus12
46		32			15			Fuse 13 Alarm				Fuse13 Alarm		Allarme fusibile13			All fus13
47		32			15			Fuse 14 Alarm				Fuse14 Alarm		Allarme fusibile14			All fus14
48		32			15			Fuse 15 Alarm				Fuse15 Alarm		Allarme fusibile15			All fus15
49		32			15			Alarm					Alarm			Allarme					Allarme
50		32			15			HW Test Alarm				HW Test Alarm		Allarme test HW				Al test HW
51		32			15			SM-BRC Unit				SM-BRC Unit		Unità SMBRC				Unità SMBRC
52		32			15			Battery Fuse 1 Voltage			BATT Fuse1 Volt		Tensione fusibile batteria 1		Tens fus bat1
53		32			15			Battery Fuse 2 Voltage			BATT Fuse2 Volt		Tensione fusibile batteria 2		Tens fus bat2
54		32			15			Battery Fuse 3 Voltage			BATT Fuse3 Volt		Tensione fusibile batteria 3		Tens fus bat3
55		32			15			Battery Fuse 4 Voltage			BATT Fuse4 Volt		Tensione fusibile batteria 4		Tens fus bat4
56		32			15			Battery Fuse 1 Status			BATT Fuse1 Stat		Stato fusibile batteria 1		Stato fus bat1
57		32			15			Battery Fuse 2 Status			BATT Fuse2 Stat		Stato fusibile batteria 2		Stato fus bat2
58		32			15			Battery Fuse 3 Status			BATT Fuse3 Stat		Stato fusibile batteria 3		Stato fus bat3
59		32			15			Battery Fuse 4 Status			BATT Fuse4 Stat		Stato fusibile batteria 4		Stato fus bat4
60		32			15			On					On			ACCESO					ACCESO
61		32			15			Off					Off			SPENTO					SPENTO
62		32			15			Battery Fuse 1 Alarm			Bat Fuse1 Alarm		Allarme fusibile batteria 1		All fus1 bat
63		32			15			Battery Fuse 2 Alarm			Bat Fuse2 Alarm		Allarme fusibile batteria 2		All fus2 bat
64		32			15			Battery Fuse 3 Alarm			Bat Fuse3 Alarm		Allarme fusibile batteria 3		All fus3 bat
65		32			15			Battery Fuse 4 Alarm			Bat Fuse4 Alarm		Allarme fusibile batteria 4		All fus4 bat
66		32			15			Total Load Current			Tot Load Curr		Corrente totale carico			Corr totale
67		32			15			Over Load Current Limit			Over Curr Lim		Limite di sovracorrente			Sovracorrente
68		32			15			Over Current				Over Current		Sovracorrente				Sovracorrente
69		32			15			LVD1 Enabled				LVD1 Enabled		LVD1 abilitato				LVD1 abilitato
70		32			15			LVD1 Mode				LVD1 Mode		Modo LVD1				Modo LVD1
71		32			15			LVD1 Reconnect Delay			LVD1Recon Delay		Ritardo riconnessione LVD1		RitarRicon LVD1
72		32			15			LVD2 Enabled				LVD2 Enabled		LVD2 abilitato				LVD2 abilitato
73		32			15			LVD2 Mode				LVD2 Mode		Modo LVD2				Modo LVD2
74		32			15			LVD2 Reconnect Delay			LVD2Recon Delay		Ritardo riconnessione LVD2		RitarRicon LVD2
75		32			15			LVD1 Status				LVD1 Status		Stato LVD1				Stato LVD1
76		32			15			LVD2 Status				LVD2 Status		Stato LVD2				stato LVD2
77		32			15			Disabled				Disabled		Disabilitato				Disabilitato
78		32			15			Enabled					Enabled			Abilitato				Abilitato
79		32			15			By Voltage				By Volt			Per tensione				Per tensione
80		32			15			By Time					By Time			Per tempo				Per tempo
81		32			15			Bus Bar Volt Alarm			Bus Bar Alarm		Allarme V barra distribuzione		All V bus dist
82		32			15			Normal					Normal			Normale					Normale
83		32			15			Low					Low			Basso					Basso
84		32			15			High					High			Alto					Alto
85		32			15			Low Voltage				Low Voltage		Tensione bassa				Tensione bassa
86		32			15			High Voltage				High Voltage		Tensione alta				Tensione alta
87		32			15			Shunt 1 Current Alarm			Shunt1 Alarm		Allarme corrente shunt1			All shunt1
88		32			15			Shunt 2 Current Alarm			Shunt2 Alarm		Allarme corrente shunt2			All shunt2
89		32			15			Shunt 3 Current Alarm			Shunt3 Alarm		Allarme corrente shunt3			All shunt3
90		32			15			Shunt 4 Current Alarm			Shunt4 Alarm		Allarme corrente shunt4			All shunt4
91		32			15			Shunt 1 Over Current			Shunt1 Over Cur		Sovracorrente shunt1			Sovracorr shunt1
92		32			15			Shunt 2 Over Current			Shunt2 Over Cur		Sovracorrente shunt2			Sovracorr shunt2
93		32			15			Shunt 3 Over Current			Shunt3 Over Cur		Sovracorrente shunt3			Sovracorr shunt3
94		32			15			Shunt 4 Over Current			Shunt4 Over Cur		Sovracorrente shunt4			Sovracorr shunt4
95		32			15			Interrupt Times				Interrupt Times		Num di interruzioni			Num di interr
96		32			15			Existent				Existent		Esistente				Esistente
97		32			15			Non-Existent				Non-Existent		Inesistente				Inesistente
98		32			15			Very Low				Very Low		Molto basso				Molto basso
99		32			15			Very High				Very High		Molto alto				Molto alto
100		32			15			Switch					Switch			Interruttore				Interruttore
101		32			15			LVD1 Failure				LVD1 Failure		Guasto LVD1				Guasto LVD1
102		32			15			LVD2 Failure				LVD2 Failure		Guasto LVD2				Guasto LVD2
103		32			15			HTD1 Enable				HTD1 Enable		Abilita HTD1				Abilita HTD1
104		32			15			HTD2 Enable				HTD2 Enable		Abilita HTD2				Abilita HTD2
105		32			15			Battery LVD				Battery LVD		LVD di batteria				LVD di batteria
106		32			15			No Battery				No Battery		Nessuna batteria			Nessuna batt
107		32			15			LVD1					LVD1			LVD1					LVD1
108		32			15			LVD2					LVD2			LVD2					LVD2
109		32			15			Battery Always On			BattAlwaysOn		Batteria sempre connessa		Sempre con bat
110		32			15			Barcode					Barcode			Codice a barre				Barcode
111		32			15			DC Overvoltage				DC Overvolt		Sovratensione CC			Sovratens CC
112		32			15			DC Undervoltage				DC Undervolt		Sottotensione CC			Sottotens CC
113		32			15			Overcurrent 1				Overcurr 1		Sovracorrente 1				Sovracorrente1
114		32			15			Overcurrent 2				Overcurr 2		Sovracorrente 2				Sovracorrente2
115		32			15			Overcurrent 3				Overcurr 3		Sovracorrente 3				Sovracorrente3
116		32			15			Overcurrent 4				Overcurr 4		Sovracorrente 4				Sovracorrente4
117		32			15			Existence State				Existence State		Stato attuale				Stato attuale
118		32			15			Communication Failure			Comm Fail		Comunicazione interrotta		COM interrotta
119		32			15			Bus Voltage Status			Bus Status		Stato tensione bus			Stato bus V
120		32			15			Communication OK			Comm OK			Comunicazione ok			COM ok
121		32			15			All Communication Failure		All Comm Fail		Nessuno risponde			Ness risponde
122		32			15			Communication Failure			Comm Fail		Non risponde				Non risponde
123		32			15			Rated Capacity				Rated Capacity		Capacità nominale			Cap nomin
150		32			15			Digital In Number			Digital In Num		Num ingressi digitali (DI)		Num ingr DI
151		32			15			Digital Input 1				Digital Input1		DI 1					DI 1
152		32			15			Low					Low			Basso					Basso
153		32			15			High					High			Alto					Alto
154		32			15			Digital Input 2				Digital Input2		DI 2					DI 2
155		32			15			Digital Input 3				Digital Input3		DI 3					DI 3
156		32			15			Digital Input 4				Digital Input4		DI 4					DI 4
157		32			15			Digital Out Number			Digital Out Num		Num uscite digitali (DO)		Num DO
158		32			15			Digital Output 1			Digital Output1		DO 1					DO 1
159		32			15			Digital Output 2			Digital Output2		DO 2					DO 2
160		32			15			Digital Output 3			Digital Output3		DO 3					DO 3
161		32			15			Digital Output 4			Digital Output4		DO 4					DO 4
162		32			15			Operation State				Op State		Stato operativo				Stato operatv
163		32			15			Normal					Normal			Normale					Normale
164		32			15			Test					Test			Prova					Prova
165		32			15			Discharge				Discharge		In scarica				In scarica
166		32			15			Calibration				Calibration		Calibrazione				Calibrazione
167		32			15			Diagnostic				Diagnostic		Diagnostico				Diagnostico
168		32			15			Maintenance				Maintenance		Mantenimento				Mantenimento
169		32			15			Resistence Test Interval		Resist Test Int		Intervallo prova di resistenza		Int prova res
170		32			15			Ambient Temperature Value		Amb Temp Value		Temperatura ambiente			Temp ambiente
171		32			15			High Ambient Temperature		Hi Amb Temp		Alta temperatura Ambiente		Alta Temp Amb
172		32			15			Configuration Number			Cfg Num			Num di configurazione			Num config
173		32			15			Number of Batteries			No. of Batt		Numero di batterie			Num batteria
174		32			15			Unit Sequence Number			Unit Seq Num		Numero di sequenza unità		Num sequenza
175		32			15			Start Battery Sequence			Start Batt Seq		Inizio sequenza di batteria		Iniz sequenBat
176		32			15			Low Ambient Temperature			Lo Amb Temp		Bassa temperatura ambiente		Bassa temp amb
177		34			15			Ambient Temperature Probe Failure	Amb Temp Fail		Guasto sonda temp ambiente		Gsto sonda Tamb
