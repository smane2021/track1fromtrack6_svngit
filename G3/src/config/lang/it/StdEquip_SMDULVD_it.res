﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			LVD Ext					LVD Ext			LVD estensione				LVD estens
2		32			15			SMDU LVD				SMDU LVD		SMDU LVD				SMDU LVD
11		32			15			Connected				Connected		Connesso				Connesso
12		32			15			Disconnected				Disconnected		Disconnesso				Disconnesso
13		32			15			No					No			No					No
14		32			15			Yes					Yes			Sí					Sí
21		32			15			LVD1 Status				LVD1 Status		Stato LVD1				Stato LVD1
22		32			15			LVD2 Status				LVD2 Status		Stato LVD2				Stato LVD2
23		32			15			LVD1 Disconnected			LVD1 Disconn		LVD1 disconnesso			LVD1 aperto
24		32			15			LVD2 Disconnected			LVD2 Disconn		LVD2 disconnesso			LVD2 aperto
25		32			15			Communication Failure			Comm Failure		Comunicazione interrotta		COM interr
26		32			15			State					State			Stato					Stato
27		32			15			LVD1 Control				LVD1 Control		Controllo LVD1				Ctrl LVD1
28		32			15			LVD2 Control				LVD2 Control		Controllo LVD2				Ctrl LVD2
31		32			15			LVD1					LVD1			LVD1 abilitato				Funzione LVD1
32		32			15			LVD1 Mode				LVD1 Mode		Modo LVD1				Modo LVD1
33		32			15			LVD1 Voltage				LVD1 Voltage		Tensione LVD1				Tensione LVD1
34		32			15			LVD1 Reconnect Voltage			LVD1 Recon Volt		Tensione riconnessione LVD1		Tens ricon LVD1
35		32			15			LVD1 Reconnect Delay			LVD1 ReconDelay		Ritardo riconnessione LVD1		Rit ricon LVD1
36		32			15			LVD1 Time				LVD1 Time		Tempo LVD1				Tempo LVD1
37		32			15			LVD1 Dependency				LVD1 Dependency		Dipendenza LVD1				Dipend LVD1
41		32			15			LVD2					LVD2			Disconnessione LVD2			Disconn LVD2
42		32			15			LVD2 Mode				LVD2 Mode		Modo LVD2				Modo LVD2
43		32			15			LVD2 Voltage				LVD2 Voltage		Tensione LVD2				Tensione LVD2
44		32			15			LVD2 Reconnect Voltage			LVD2 Recon Volt		Tensione riconnessione LVD2		Tens ricon LVD2
45		32			15			LVD2 Reconnect Delay			LVD2 ReconDelay		Ritardo riconnessione LVD2		Rit ricon LVD2
46		32			15			LVD2 Time				LVD2 Time		Tempo LVD2				Tempo LVD2
47		32			15			LVD2 Dependency				LVD2 Dependency		Dipendenza LVD2				Dipend LVD2
51		32			15			Disabled				Disabled		Disabilitato				Disabilitato
52		32			15			Enabled					Enabled			Abilitato				Abilitato
53		32			15			Voltage					Voltage			Tensione				Tensione
54		32			15			Time					Time			Tempo					Tempo
55		32			15			None					None			Nessuno					Nessuno
56		32			15			LVD1					LVD1			LVD1					LVD1
57		32			15			LVD2					LVD2			LVD2					LVD2
103		32			15			High Temperature Disconnect 1		HTD1			Abilita HTD1				Abilita HTD1
104		32			15			High Temperature Disconnect 2		HTD2			Abilita HTD2				Abilita HTD2
105		32			15			Battery LVD				Battery LVD		LVD di batteria				LVD di batt
110		32			15			Communication Interrupt			Comm Interrupt		Comunicazione interrotta		COM interr
111		32			15			Interrupt Times				Interrupt Times		Interruzioni				Interruzioni
116		32			15			LVD1 Contactor Failure			LVD1 Failure		Guasto contattore LVD1			Gsto cont LVD1
117		32			15			LVD2 Contactor Failure			LVD2 Failure		Guasto contattore LVD2			Gsto cont LVD2
118		32			15			LVD 1 Voltage (24V)			LVD 1 Voltage		Tensione LVD1(24V)			Tensione LVD1
119		32			15			LVD 1 Reconnect Voltage (24V)		LVD1 Recon Volt		Tensione riconnessione LVD1(24V)	Tens ricon LVD1
120		32			15			LVD 2 Voltage (24V)			LVD 2 Voltage		Tensione LVD2				Tensione LVD2
121		32			15			LVD 2 Reconnect Voltage (24V)		LVD2 Recon Volt		Tensione riconnessione LVD2(24V)	Tens ricon LVD2
