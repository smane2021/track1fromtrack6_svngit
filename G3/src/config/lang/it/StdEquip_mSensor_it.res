﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			mSensor					mSensor				mSensor					mSensor			
3		32			15			Comm State				Comm State			Stato Comm				Stato Comm		
4		32			15			Existence State			Existence State		Stato Esistenza		Stato Esisten	
5		32			15			Yes						Yes					sì					sì			
6		32			15			Communication Fail		Comm Fail			Com Fallita		Com Fallita		
7		32			15			Existent				Existent			Esistente				Esistente		
8		32			15			Comm OK					Comm OK				Comm OK					Comm OK			

11		32			15			Voltage DC Chan 1		Voltage DC Chan1		Tensione Canale 1		TensioneCanale1
12		32			15			Voltage DC Chan 2		Voltage DC Chan2		Tensione Canale 2		TensioneCanale2
13		32			15			Post Temp Chan 1		Post Temp Chan1			Post Temp Canale1		PostTempCanale1
14		32			15			Post Temp Chan 2		Post Temp Chan2			Post Temp Canale2		PostTempCanale2
15		32			15			AC Ripple Chan 1		AC Ripple Chan1			AC OndulazioneCanale1		ACOndulaCanale1
16		32			15			AC Ripple Chan 2		AC Ripple Chan 2		AC OndulazioneCanale2		ACOndulaCanale2
17		32			15			Impedance Chan 1		Impedance Chan 1		ImpedenzaCanale1		ImpedCanale1
18		32			15			Impedance Chan 2		Impedance Chan 2		ImpedenzaCanale2		ImpedCanale2
19		32			15			DC Volt Status1			DC Volt Status1			Tensione Status1		TensioneStatus1	
20		32			15			Post Temp Status1		Post Temp Status1		Post Temp Status1		Post Temp Status1
21		32			15			Impedance Status1		Impedance Status1		Impeden Status1		Impeden Status1
22		32			15			DC Volt Status2			DC Volt Status2			Tensione Status2		Tensione Status2	
23		32			15			Post Temp Status2		Post Temp Status2		Post Temp Status2		Post Temp Status2
24		32			15			Impedance Status2		Impedance Status2		Impedenza Status2		ImpedenzaStatus2


25		32			15				DC Volt1No Value					DCVolt1NoValue				Tensione1NoValore					Tensi1NoValor				
26		32			15				DC Volt1Invalid						DCVolt1Invalid				Tensione1Invalido					Tensi1Invalido						
27		32			15				DC Volt1Busy						DCVolt1Busy					Tensione1Occupato					Tensi1Occupato						
28		32			15				DC Volt1Out of Range				DCVolt1OutRange				Tensione1GammaOut					Tensi1GammaOut				
29		32			15				DC Volt1Over Range					DCVolt1OverRange			Tensione1OverGamma					Tensi1OverGamma				
30		32			15				DC Volt1Under Range					DCVolt1UndRange				Tensione1UnderGamma					Tensi1UndGamma					
31		32			15				DC Volt1 Supply Issue				DCVolt1SupplyIss			Tensione1ProbAliment				Tensi1ProbAlime				
32		32			15				DC Volt1Module Harness				DCVolt1Harness				Tensione1ModuleFinimenti			Tensi1Finimenti				
33		32			15				DC Volt1Low							DCVolt1Low					Tensione1Basso						Tensi1Basso							
34		32			15				DC Volt1High						DCVolt1High					Tensione1Alto						Tensi1Alto						
35		32			15				DC Volt1Too Hot						DCVolt1Too Hot				Tensione1TroppoCaldo				Tensi1TropCaldo						
36		32			15				DC Volt1Too Cold					DCVolt1Too Cold				Tensione1TroppoFreddo				Tensi1TropFred					
37		32			15				DC Volt1Calibration Err				DCVolt1CalibrErr			Tensione1ErrCalibrazione			Tensi1ErrCalibr				
38		32			15				DC Volt1Calibration Overflow		DCVolt1CalibrOF				Tensione1CalibrazioneOverflow		Tensi1CalibrOF		
39		32			15				DC Volt1Not Used					DCVolt1Not Used				Tensione1NonUsato					Tensi1NonUsat					
40		32			15				Temp1No Value						Temp1 NoValue				Temp1NoValore						Temp1NoValor			
41		32			15				Temp1Invalid						Temp1 Invalid				Temp1Invalido						Temp1Invalido			
42		32			15				Temp1Busy							Temp1 Busy					Temp1Occupato						Temp1Occupato			
43		32			15				Temp1Out of Range					Temp1 OutRange				Temp1GammaOut						Temp1GammaOut			
44		32			15				Temp1Over Range						Temp1OverRange				Temp1OverGamma						Temp1OverGamma			
45		32			15				Temp1Under Range					Temp1UndRange				Temp1UnderGamma						Temp1UndGamma			
46		32			15				Temp1Volt Supply					Temp1SupplyIss				Temp1ProbAliment					Temp1ProbAlime			
47		32			15				Temp1Module Harness					Temp1Harness				Temp1ModuleFinimenti				Temp1Finimenti			
48		32			15				Temp1Low							Temp1 Low					Temp1Basso							Temp1Basso				
49		32			15				Temp1High							Temp1 High					Temp1Alto							Temp1Alto				
50		32			15				Temp1Too Hot						Temp1 Too Hot				Temp1TroppoCaldo					Temp1TropCaldo			
51		32			15				Temp1Too Cold						Temp1 Too Cold				Temp1TroppoFreddo					Temp1TropFred			
52		32			15				Temp1Calibration Err				Temp1 CalibrErr				Temp1ErrCalibrazione				Temp1ErrCalibr		
53		32			15				Temp1Calibration Overflow			Temp1 CalibrOF				Temp1CalibrazioneOverflow			Temp1CalibrOF			
54		32			15				Temp1Not Used						Temp1 Not Used				Temp1NonUsato						Temp1NonUsat			
55		32			15				Impedance1No Value					Imped1NoValue				Impedenza1NoValore					Imped1NoValor	
56		32			15				Impedance1Invalid					Imped1Invalid				Impedenza1Invalido					Imped1Invalido	
57		32			15				Impedance1Busy						Imped1Busy					Impedenza1Occupato					Imped1Occupato	
58		32			15				Impedance1Out of Range				Imped1OutRange				Impedenza1GammaOut					Imped1GammaOut	
59		32			15				Impedance1Over Range				Imped1OverRange				Impedenza1OverGamma					Imped1OverGamma	
60		32			15				Impedance1Under Range				Imped1UndRange				Impedenza1UnderGamma				Imped1UndGamma	
61		32			15				Impedance1Volt Supply				Imped1SupplyIss				Impedenza1ProbAliment				Imped1ProbAlime	
62		32			15				Impedance1Module Harness			Imped1Harness				Impedenza1ModuleFinimenti			Imped1Finimenti	
63		32			15				Impedance1Low						Imped1Low					Impedenza1Basso						Imped1Basso		
64		32			15				Impedance1High						Imped1High					Impedenza1Alto						Imped1Alto		
65		32			15				Impedance1Too Hot					Imped1Too Hot				Impedenza1TroppoCaldo				Imped1TropCaldo	
66		32			15				Impedance1Too Cold					Imped1Too Cold				Impedenza1TroppoFreddo				Imped1TropFred	
67		32			15				Impedance1Calibration Err			Imped1CalibrErr				Impedenza1ErrCalibrazione			Imped1ErrCalibr	
68		32			15				Impedance1Calibration Overflow		Imped1CalibrOF				Impedenza1CalibrazioneOverflow		Imped1CalibrOF	
69		32			15				Impedance1Not Used					Imped1Not Used				Impedenza1NonUsato					Imped1NonUsat	

70		32			15				DC Volt2No Value					DCVolt2NoValue				Tensione2NoValore					Tensi2NoValor	
71		32			15				DC Volt2Invalid						DCVolt2Invalid				Tensione2Invalido					Tensi2Invalido	
72		32			15				DC Volt2Busy						DCVolt2Busy					Tensione2Occupato					Tensi2Occupato	
73		32			15				DC Volt2Out of Range				DCVolt2OutRange				Tensione2GammaOut					Tensi2GammaOut	
74		32			15				DC Volt2Over Range					DCVolt2OverRange			Tensione2OverGamma					Tensi2OverGamma	
75		32			15				DC Volt2Under Range					DCVolt2UndRange				Tensione2UnderGamma					Tensi2UndGamma	
76		32			15				DC Volt2Volt Supply Issue			DCVolt2SupplyIss			Tensione2ProbAliment				Tensi2ProbAlime	
77		32			15				DC Volt2Module Harness				DCVolt2Harness				Tensione2ModuleFinimenti			Tensi2Finimenti	
78		32			15				DC Volt2Low							DCVolt2Low					Tensione2Basso						Tensi2Basso		
79		32			15				DC Volt2High						DCVolt2High					Tensione2Alto						Tensi2Alto		
80		32			15				DC Volt2Too Hot						DCVolt2Too Hot				Tensione2TroppoCaldo				Tensi2TropCaldo	
81		32			15				DC Volt2Too Cold					DCVolt2Too Cold				Tensione2TroppoFreddo				Tensi2TropFred	
82		32			15				DC Volt2Calibration Err				DCVolt2CalibrErr			Tensione2ErrCalibrazione			Tensi2ErrCalibr	
83		32			15				DC Volt2Calibration Overflow		DCVolt2CalibrOF				Tensione2CalibrazioneOverflow		Tensi2CalibrOF	
84		32			15				DC Volt2Not Used					DCVolt2Not Used				Tensione2NonUsato					Tensi2NonUsat	
85		32			15				Temp2No Value						Temp2 NoValue				Temp2NoValore						Temp2NoValor	
86		32			15				Temp2Invalid						Temp2 Invalid				Temp2Invalido						Temp2Invalido	
87		32			15				Temp2Busy							Temp2 Busy					Temp2Occupato						Temp2Occupato	
88		32			15				Temp2Out of Range					Temp2 OutRange				Temp2GammaOut						Temp2GammaOut	
89		32			15				Temp2Over Range						Temp2OverRange				Temp2OverGamma						Temp2OverGamma	
90		32			15				Temp2Under Range					Temp2UndRange				Temp2UnderGamma						Temp2UndGamma	
91		32			15				Temp2Volt Supply					Temp2SupplyIss				Temp2ProbAliment					Temp2ProbAlime	
92		32			15				Temp2Module Harness					Temp2Harness				Temp2ModuleFinimenti				Temp2Finimenti	
93		32			15				Temp2Low							Temp2 Low					Temp2Basso							Temp2Basso		
94		32			15				Temp2High							Temp2 High					Temp2Alto							Temp2Alto		
95		32			15				Temp2Too Hot						Temp2 Too Hot				Temp2TroppoCaldo					Temp2TropCaldo	
96		32			15				Temp2Too Cold						Temp2 Too Cold				Temp2TroppoFreddo					Temp2TropFred	
97		32			15				Temp2Calibration Err				Temp2 CalibrErr				Temp2ErrCalibrazione				Temp2ErrCalibr	
98		32			15				Temp2Calibration Overflow			Temp2 CalibrOF				Temp2CalibrazioneOverflow			Temp2CalibrOF	
99		32			15				Temp2Not Used						Temp2 Not Used				Temp2NonUsato						Temp2NonUsat	
100		32			15				Impedance2No Value					Imped2NoValue				Impedenza2NoValore					Imped2NoValor	
101		32			15				Impedance2Invalid					Imped2Invalid				Impedenza2Invalido					Imped2Invalido	
103		32			15				Impedance2Busy						Imped2Busy					Impedenza2Occupato					Imped2Occupato	
104		32			15				Impedance2Out of Range				Imped2OutRange				Impedenza2GammaOut					Imped2GammaOut	
105		32			15				Impedance2Over Range				Imped2OverRange				Impedenza2OverGamma					Imped2OverGamma	
106		32			15				Impedance2Under Range				Imped2UndRange				Impedenza2UnderGamma				Imped2UndGamma	
107		32			15				Impedance2Volt Supply				Imped2SupplyIss				Impedenza2ProbAliment				Imped2ProbAlime	
108		32			15				Impedance2Module Harness			Imped2Harness				Impedenza2ModuleFinimenti			Imped2Finimenti	
109		32			15				Impedance2Low						Imped2Low					Impedenza2Basso						Imped2Basso		
110		32			15				Impedance2High						Imped2High					Impedenza2Alto						Imped2Alto		
111		32			15				Impedance2Too Hot					Imped2Too Hot				Impedenza2TroppoCaldo				Imped2TropCaldo	
112		32			15				Impedance2Too Cold					Imped2Too Cold				Impedenza2TroppoFreddo				Imped2TropFred	
113		32			15				Impedance2Calibration Err			Imped2CalibrErr				Impedenza2ErrCalibrazione			Imped2ErrCalibr	
114		32			15				Impedance2Calibration Overflow		Imped2CalibrOF				Impedenza2CalibrazioneOverflow		Imped2CalibrOF	
115		32			15				Impedance2Not Used					Imped2Not Used				Impedenza2NonUsato					Imped2NonUsat	
