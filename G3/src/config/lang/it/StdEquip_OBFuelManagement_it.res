﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			16			OB Fuel Tank				OB Fuel Tank		OB Serbatoio Carburante			OB Serbatoio Carb
2		32			15			Remaining Fuel Height			Remained Height		Altezza Carburante Residuo		Altezza Resid
3		32			15			Remaining Fuel Volume			Remained Volume		Volume Carburante Residuo		Volume Resid
4		32			15			Remaining Fuel Percent			RemainedPercent		Percentuale Carburante Residuo		Percent Resid
5		32			15			Fuel Theft Alarm			Fuel Theft Alm		Allarme Furto Carburante		Alm Furto Carb
6		32			15			No					No			No					No
7		32			15			Yes					Yes			Sì					Sì
8		32			15			Multi-Shape Height Error Status		Multi Hgt Err		Stato Errore Altezza Multi-Sharp	Err Altzz Multi
9		32			15			Setting Configuration Done		Set Config Done		Impostazione Configurazione Eseguita	Conf Eseguit
10		32			15			Reset Theft Alarm			Reset Theft Alm		Reset Allarme Furto			Reset All Furto
11		32			15			Fuel Tank Type				Fuel Tank Type		Tipo Serbatoio Carburante		Tipo Serb Carb
12		32			15			Square Tank Length			Square Tank L		Lunghezza Cisterna			Lungh Cist
13		32			15			Square Tank Width			Square Tank W		Profondità Cisterna			Prof Cist
14		32			15			Square Tank Height			Square Tank H		Altezza Cisterna			Altezza Cist
15		32			15			Vertical Cylinder Tank Diameter		Vertical Tank D		Diametro Verticale Cisterna		D Vert Cist
16		32			15			Vertical Cylinder Tank Height		Vertical Tank H		Altezza Verticale Cisterna		H Vert Cist
17		32			15			Horiz Cylinder Tank Diameter		Horiz Tank D		Diametro Orizzontale Cisterna		D Orizz Cist
18		32			15			Horiz Cylinder Tank Length		Horiz Tank L		Lunghezza Orizzontale Cisterna		L Orizz Cist
20		32			15			Number of Calibration Points		Num Cal Points		Numero Punti Calibrazione		Num Punt Calibr
21		32			15			Height 1 of Calibration Point		Height 1 Point		Altezza 1 Punto Calibrazione		Altzz 1 Punt
22		32			15			Volume 1 of Calibration Point		Volume 1 Point		Volume 1 Punto Calibrazione		Vol 1 Punt
23		32			15			Height 2 of Calibration Point		Height 2 Point		Altezza 2 Punto Calibrazione		Altzz 2 Punt
24		32			15			Volume 2 of Calibration Point		Volume 2 Point		Volume 2 Punto Calibrazione		Vol 2 Punt
25		32			15			Height 3 of Calibration Point		Height 3 Point		Altezza 3 Punto Calibrazione		Altzz 3 Punt
26		32			15			Volume 3 of Calibration Point		Volume 3 Point		Volume 3 Punto Calibrazione		Vol 3 Punt
27		32			15			Height 4 of Calibration Point		Height 4 Point		Altezza 4 Punto Calibrazione		Altzz 4 Punt
28		32			15			Volume 4 of Calibration Point		Volume 4 Point		Volume 4 Punto Calibrazione		Vol 4 Punt
29		32			15			Height 5 of Calibration Point		Height 5 Point		Altezza 5 Punto Calibrazione		Altzz 5 Punt
30		32			15			Volume 5 of Calibration Point		Volume 5 Point		Volume 5 Punto Calibrazione		Vol 5 Punt
31		32			15			Height 6 of Calibration Point		Height 6 Point		Altezza 6 Punto Calibrazione		Altzz 6 Punt
32		32			15			Volume 6 of Calibration Point		Volume 6 Point		Volume 6 Punto Calibrazione		Vol 6 Punt
33		32			15			Height 7 of Calibration Point		Height 7 Point		Altezza 7 Punto Calibrazione		Altzz 7 Punt
34		32			15			Volume 7 of Calibration Point		Volume 7 Point		Volume 7 Punto Calibrazione		Vol 7 Punt
35		32			15			Height 8 of Calibration Point		Height 8 Point		Altezza 8 Punto Calibrazione		Altzz 8 Punt
36		32			15			Volume 8 of Calibration Point		Volume 8 Point		Volume 8 Punto Calibrazione		Vol 8 Punt
37		32			15			Height 9 of Calibration Point		Height 9 Point		Altezza 9 Punto Calibrazione		Altzz 9 Punt
38		32			15			Volume 9 of Calibration Point		Volume 9 Point		Volume 9 Punto Calibrazione		Vol 9 Punt
39		32			15			Height 10 of Calibration Point		Height 10 Point		Altezza 10 Punto Calibrazione		Altzz 10 Punt
40		32			15			Volume 10 of Calibration Point		Volume 10 Point		Volume 10 Punto Calibrazione		Vol 10 Punt
41		32			15			Height 11 of Calibration Point		Height 11 Point		Altezza 11 Punto Calibrazione		Altzz 11 Punt
42		32			15			Volume 11 of Calibration Point		Volume 11 Point		Volume 11 Punto Calibrazione		Vol 11 Punt
43		32			15			Height 12 of Calibration Point		Height 12 Point		Altezza 12 Punto Calibrazione		Altzz 12 Punt
44		32			15			Volume 12 of Calibration Point		Volume 12 Point		Volume 12 Punto Calibrazione		Vol 12 Punt
45		32			15			Height 13 of Calibration Point		Height 13 Point		Altezza 13 Punto Calibrazione		Altzz 13 Punt
46		32			15			Volume 13 of Calibration Point		Volume 13 Point		Volume 13 Punto Calibrazione		Vol 13 Punt
47		32			15			Height 14 of Calibration Point		Height 14 Point		Altezza 14 Punto Calibrazione		Altzz 14 Punt
48		32			15			Volume 14 of Calibration Point		Volume 14 Point		Volume 14 Punto Calibrazione		Vol 14 Punt
49		32			15			Height 15 of Calibration Point		Height 15 Point		Altezza 15 Punto Calibrazione		Altzz 15 Punt
50		32			15			Volume 15 of Calibration Point		Volume 15 Point		Volume 15 Punto Calibrazione		Vol 15 Punt
51		32			15			Height 16 of Calibration Point		Height 16 Point		Altezza 16 Punto Calibrazione		Altzz 16 Punt
52		32			15			Volume 16 of Calibration Point		Volume 16 Point		Volume 16 Punto Calibrazione		Vol 16 Punt
53		32			15			Height 17 of Calibration Point		Height 17 Point		Altezza 17 Punto Calibrazione		Altzz 17 Punt
54		32			15			Volume 17 of Calibration Point		Volume 17 Point		Volume 17 Punto Calibrazione		Vol 17 Punt
55		32			15			Height 18 of Calibration Point		Height 18 Point		Altezza 18 Punto Calibrazione		Altzz 18 Punt
56		32			15			Volume 18 of Calibration Point		Volume 18 Point		Volume 18 Punto Calibrazione		Vol 18 Punt
57		32			15			Height 19 of Calibration Point		Height 19 Point		Altezza 19 Punto Calibrazione		Altzz 19 Punt
58		32			15			Volume 19 of Calibration Point		Volume 19 Point		Volume 19 Punto Calibrazione		Vol 19 Punt
59		32			15			Height 20 of Calibration Point		Height 20 Point		Altezza 20 Punto Calibrazione		Altzz 20 Punt
60		32			15			Volume 20 of Calibration Point		Volume 20 Point		Volume 20 Punto Calibrazione		Vol 20 Punt
62		32			15			Square Tank				Square Tank		Cisterna Quadrata			CisterQuadrata
63		32			15			Vertical Cylinder Tank			Vert Cyl Tank		Cisterna Cilindrica Verticale		Cist Cil Vert
64		32			15			Horizontal Cylinder Tank		Horiz Cyl Tank		Cisterna Cilindrica Orizzontale		Cist Cil Orizz
65		32			15			Multi Shape Tank			MultiShapeTank		Serbatoio Multi-Sharp			Serb Multisharp
66		32			15			Low Fuel Level Limit			Low Level Limit		Limite Livello Carburante Basso		Lmt Liv Basso
67		32			15			High Fuel Level Limit			Hi Level Limit		Limite Livello Carburante Alto		Lmt Liv Alto
68		32			15			Maximum Consumption Speed		Max Flow Speed		Velocità Massimo Consumo		Veloc Max Cons
71		32			15			High Fuel Level Alarm			Hi Level Alarm		Allarme Livello Carburante Alto		All Liv Alto
72		32			15			Low Fuel Level Alarm			Low Level Alarm		Allarme Livello Carburante Basso	All Liv Basso
73		32			15			Fuel Theft Alarm			Fuel Theft Alm		Allarme Furto Carburante		All Furto Carb
74		32			15			Square Tank Height Error		Sq Tank Hgt Err		Errore Altezza Cisterna Quadrata	Err H Cist Sq
75		32			15			Vert Cylinder Tank Height Error		Vt Tank Hgt Err		Errore Altezza Cisterna Cilindrica Verticale	Err H Cist Vert
76		32			15			Horiz Cylinder Tank Height Error	Hr Tank Hgt Err		Errore Altezza Cisterna Cilindrica Orizzontale	Err H Cist Orizz
77		32			15			Tank Height Error			Tank Height Err		Errore Altezza Serbatoio		Err H Serb
78		32			15			Fuel Tank Config Error			Fuel Config Err		Errore Configurazione Serbatoio Carburante	Err Conf Carb
80		32			15			Fuel Tank Config Error Status		Config Err		Stato Errore Configurazione Serbatoio Carburante	Err Config
81		32			15			X1					X1			X1					X1
82		32			15			Y1					Y1			Y1					Y1
83		32			15			X2					X2			X2					X2
84		32			15			Y2					Y2			Y2					Y2
