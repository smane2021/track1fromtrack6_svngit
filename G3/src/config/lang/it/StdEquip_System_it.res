﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			16			Power System				Power System		Tipo sistema				Tipo sistema
2		32			15			System Output Voltage			Output Voltage		Tensione di sistema			Tens sistema
3		32			15			System Output Current			Output Current		Carico totale				Carico sistema
4		32			15			System Output Power			Output Power		Potenza del sistema			Potenza sist
5		32			15			Total Power Consumption			Power Consump		Consumo potenza totale			Consumo
6		32			15			Power Peak in 24 Hours			Power Peak		Picco di potenza in 24h			Picco pot 24h
7		32			15			Average Power in 24 Hours		Avg power		Potenza max in 24h			Pot media 24h
8		32			15			Hardware Write-protect Switch		Hardware Switch		Conmut Hw protez in scrittura		Protezione Hw
9		32			15			Ambient Temperature			Ambient Temp		Temperatura ambiente			Temp ambiente
10		32			15			Number of SM-DUs			No of SM-DUs		Numero di SM-DU				Num SM-DU
18		32			15			Relay Output 1				Relay Output 1		Relè di uscita 1			Relè usc 1
19		32			15			Relay Output 2				Relay Output 2		Relè di uscita 2			Relè usc 2
20		32			15			Relay Output 3				Relay Output 3		Relè di uscita 3			Relè usc 3
21		32			15			Relay Output 4				Relay Output 4		Relè di uscita 4			Relè usc 4
22		32			15			Relay Output 5				Relay Output 5		Relè di uscita 5			Relè usc 5
23		32			15			Relay Output 6				Relay Output 6		Relè di uscita 6			Relè usc 6
24		32			15			Relay Output 7				Relay Output 7		Relè di uscita 7			Relè usc 7
25		32			15			Relay Output 8				Relay Output 8		Relè di uscita 8			Relè usc 8
27		32			15			Undervoltage 1 Level			Undervoltage 1		Livello sottotensione 1			Sottotens 1
28		32			15			Undervoltage 2 Level			Undervoltage 2		Livello sottotensione 2			Sottotens 2
29		32			15			Overvoltage 1 Level			Overvoltage 1		Livello sovratensione 1			Sovratens 1
31		32			15			Temperature 1 High Limit		Temp 1 High		Lim alta temperatura 1			Alta temp 1
32		32			15			Temperature 1 Low Limit			Temp 1 Low		Lim bassa temperatura 1			Bassa temp 1
33		32			15			Auto/Man State				Auto/Man State		Stato man/auto				Stato
34		32			15			Outgoing Alarms Blocked			Alarms Blocked		Allarmi bloccati			All bloccati
35		32			15			Supervision Unit Internal Fault		Internal Fault		Guasto interno supervisione		Guasto PLC
36		32			15			CAN Communication Failure		CAN Comm Fault		Comunicazione interrotta		Gsto com CAN
37		32			15			Mains Failure				Mains Failure		Mancanza rete				Mancanza rete
38		32			15			Undervoltage 1				Undervolt 1		Sottotensione 1				Sottotens 1
39		32			15			Undervoltage 2				Undervolt 2		Sottotensione 2				Sottotens 2
40		32			15			Overvoltage 1				Overvolt 1		Sovratensione 1				Sovratens 1
41		32			15			High Temperature 1			High Temp 1		Alta temperatura 1			Alta temp 1
42		32			15			Low Temperature 1			Low Temp 1		Bassa temperatura 1			Bassa temp 1
43		32			15			Temperature 1 Sensor Fault		T1 Sensor Fault		Guasto sensore temperatura 1		Gsto sens 1
44		32			15			Outgoing Alarms Blocked			Alarms Blocked		Allarmi bloccati			All bloccati
45		32			15			Maintenance Alarm			MaintenanceAlrm		Allarme di manutenzione			All manutenz
46		32			15			Not Protected				Not protected		Non protetto				Non protetto
47		32			15			Protected				Protected		Protetto				Protetto
48		32			15			Normal					Normal			Normale					Normale
49		32			15			Fault					Fault			Guasto					Guasto
50		32			15			Off					Off			SPENTO					SPENTO
51		32			15			On					On			ACCESO					ACCESO
52		32			15			Off					Off			SPENTO					SPENTO
53		32			15			On					On			ACCESO					ACCESO
54		32			15			Off					Off			SPENTO					SPENTO
55		32			15			On					On			ACCESO					ACCESO
56		32			15			Off					Off			SPENTO					SPENTO
57		32			15			On					On			ACCESO					ACCESO
58		32			15			Off					Off			SPENTO					SPENTO
59		32			15			On					On			ACCESO					ACCESO
60		32			15			Off					Off			SPENTO					SPENTO
61		32			15			On					On			ACCESO					ACCESO
62		32			15			Off					Off			SPENTO					SPENTO
63		32			15			On					On			ACCESO					ACCESO
64		32			15			Open					Open			Aperto					Aperto
65		32			15			Closed					Closed			Chiuso					Chiuso
66		32			15			Open					Open			Aperto					Aperto
67		32			15			Closed					Closed			Chiuso					Chiuso
78		32			15			Off					Off			SPENTO					SPENTO
79		32			15			On					On			ACCESO					ACCESO
80		32			15			Auto					Auto			Auto					Auto
81		32			15			Manual					Manual			Manuale					Manuale
82		32			15			Normal					Normal			Normale					Normale
83		32			15			Blocked					Blocked			Bloccato				Bloccato
85		32			15			Set to Auto Mode			To Auto Mode		Imposta modo automatico			Modo auto
86		32			15			Set to Manual Mode			To Manual Mode		Imposta modo manuale			Modo manuale
87		32			15			Power Rate Level			PowerRate Level		Livello di potenza			Liv potenza
88		32			15			Current Power Peak Limit		Power Limit		Limite di potenza			Lim potenza
89		32			15			Peak					Peak			Picco					Picco
90		32			15			High					High			Alto					Alta
91		32			15			Flat					Flat			Media					Media
92		32			15			Low					Low			Basso					Basso
93		32			15			Lower Consumption			Lower Consump		Basso consumo abilitato			Bsso consumo
94		32			15			Power Peak Savings			Pow Peak Save		Limite di potenza abilitato		Lim pot abil
95		32			15			Disable					Disable			Disabilita				Disabilita
96		32			15			Enable					Enable			Abilita					abilita
97		32			15			Disable					Disable			Disabilita				Disabilita
98		32			15			Enable					Enable			Abilita					Abilita
99		32			15			Over Maximum Power			Over Power		Potenza massima superata		Sovrapotenza
100		32			15			Normal					Normal			Normale					Normale
101		32			15			Alarm					Alarm			Allarme					Allarme
102		32			15			Over Maximum Power Alarm		Over Power		Allarme di sovrapotenza			All sovrapot
104		32			15			System Alarm Status			Alarm Status		Allarme sistema				All sistema
105		32			15			No Alarms				No Alarms		Nessun allarme				Nessun all
106		32			15			Observation Alarm			Observation		Allarme lieve				All lieve
107		32			15			Major Alarm				Major			Allarme normale				All normale
108		32			15			Critical Alarm				Critical		Allarme critico				All critico
109		32			15			Maintenance Run Time			Mtn Run Time		Tempo di manutenzione			Temp manutenz
110		32			15			Maintenance Interval			Mtn Interval		Intervallo di manutenzione		Interv mantzn
111		32			15			Maintenance Alarm Time			Mtn Alarm Time		All tempo manutenzione			Ritrd t mantnz
112		32			15			Maintenance Time Out			Mtn Time Out		Fine tempo manutenzione			Fine t mantnz
113		32			15			No					No			No					No
114		32			15			Yes					Yes			Sí					Sí
115		32			15			Power Split Mode			Split Mode		Modo controllo power split		Power split
116		32			15			Slave Current Limit			Slave Cur Lmt		Limite corrente secondaria		Lim corr sec
117		32			15			Slave Delta Voltage			Slave Delta vol		Differenza di tens second		DeltaV sec
118		32			15			Slave Proportional Coefficient		Slave P Coef		Coefficiente proporzionale		Coef propnale
119		32			15			Slave Integral Time			Slave I Time		Tempo integrale				Tempo intgr
120		32			15			Master Mode				Master			Modo master				Modo master
121		32			15			Slave Mode				Slave			Modo slave				Modo slave
122		32			15			MPCL Control Step			MPCL Ctl Step		Passo controllo MPCL			Passo ctrl MPCL
123		32			15			MPCL Control Threshold			MPCL Ctl Thres		Soglia controllo MPCL			Soglia ctrl MPCL
124		32			15			MPCL Battery Discharge Enabled		MPCL BatDisch		Scarica bat MPCL abilitata		Scrca MPCL abil
125		32			15			MPCL Diesel Control Enabled		MPCL DieselCtl		Controllo GE MPCL abilitato		Ctl GE MPCL ab
126		32			15			Disabled				Disabled		Disabilitato				Disabilitato
127		32			15			Enabled					Enabled			Abilitato				Abilitato
128		32			15			Disabled				Disabled		Disabilitato				Disabilitato
129		32			15			Enabled					Enabled			Abilitato				Abilitato
130		32			15			System Time				System Time		Ora del sistema				Ora sistema
131		32			15			LCD Language				LCD Language		Lingua LCD				Lingua LCD
132		32			15			Audible Alarm				Audible Alarm		Suono allarme LCD			Suono all
133		32			15			English					English			Inglese					Inglese
134		32			15			Italian					Italian			Italiano				Italiano
135		32			15			On					On			ACCESO					ACCESO
136		32			15			Off					Off			SPENTO					SPENTO
137		32			15			3 Minutes				3 Min			3 min					3 min
138		32			15			10 Minutes				10 Min			10 min					10 min
139		32			15			1 Hour					1 Hour			1 ora					1 ora
140		32			15			4 Hours					4 Hour			4 ore					4 ore
141		32			15			Reset Maintenance Run Time		Reset Run Time		Reset tempo manutenzione		Reset t manutz
142		32			15			No					No			No					No
143		32			15			Yes					Yes			Sí					Sí
144		32			15			Alarm					Alarm			Allarme					Allarme
145		32			15			ACU+ HW Status				HW Status		Stato ACU+				Stato ACU+
146		32			15			Normal					Normal			Normale					Normale
147		32			15			Configuration Error			Config Error		Errore di configurazione		Errore config
148		32			15			Flash Fault				Flash Fault		Guasto intermittenza			Gsto intermtt
150		32			15			LCD Time Zone				Time Zone		Zona oraria LCD				Zona oraria
151		32			15			Time Sync Main Server			Time Sync Svr		Tempo sincr server principale		T sinc serv
152		32			15			Time Sync Backup Server			Backup Sync Svr		Tempo sincr server secondario		Serv sincBckup
153		32			15			Time Sync Interval			Sync Interval		Intervallo sincronismo			Interval sincr
155		32			15			Reset SCU				Reset SCU		Reset SCU				Reset SCU
156		32			15			Running Configuration Type		Config Type		Tipo di configurazione			Tipo config
157		32			15			Normal Configuration			Normal Config		Configurazione normale			Config normal
158		32			15			Backup Configuration			Backup Config		Configurazione di riserva		Config backup
159		32			15			Default Configuration			Default Config		Configurazione standard			Config stndrd
160		32			15			Configuration Error from Backup		Config Error 1		Errore configurazione riserva		Err config 1
161		32			15			Configuration Errorfrom Default		Config Error 2		Errore configurazione standard		Err config 2
162		32			15			Control System Alarm LED		Ctrl Sys Alarm		Controllo LED all sistema		Ctrl alrm sist
163		32			15			Abnormal Load Current			Ab Load Curr		Corrente anomala di carico		I crco anomala
164		32			15			Normal					Normal			Normale					Normale
165		32			15			Abnormal				Abnormal		Anormale				Anormale
167		32			15			Main Switch Block Condition		MS Block Cond		Condizione blocco int. princ		Bloq. disy CA
168		32			15			No					No			No					No
169		32			15			Yes					Yes			Sí					Sí
170		32			15			Total Run Time				Total Run Time		Tempo totale funzionamento		T di funzmnt
171		32			15			Total Number of Alarms			Total No. Alms		Totale allarmi				Tot allarmi
172		32			15			Total Number of OA			Total No. OA		Totale allarmi lievi			All lievi
173		32			15			Total Number of MA			Total No. MA		Totale allarmi normali			All normali
174		32			15			Total Number of CA			Total No. CA		Totale allarmi critici			All critici
175		32			15			SPD Fault				SPD Fault		Guasto SPD				Guasto SPD
176		32			15			Overvoltage 2 Level			Overvoltage 2		Livello di sovratensione 2		Sovratens 2
177		32			15			Overvoltage 2				Overvoltage 2		Sovratensione 2				Sovratens 2
178		32			15			Regulation Voltage			Regu Voltage		Regolazione di tensione abilit.		Regzn tens
179		32			15			Regulation Voltage Range		Regu Volt Range		Intervallo regolazione tens		Rango reg tens
180		32			15			Keypad Sound				Keypad Sound		Suono tastiera				Suono tasti
181		32			15			On					On			ACCESO					ACCESO
182		32			15			Off					Off			SPENTO					SPENTO
183		32			15			Load Shunt Full Current			Load Shunt Curr		Corrente shunt di carico		Corr shunt crco
184		32			15			Load Shunt Full Voltage			Load Shunt Volt		Tensione shunt di carico		Tens shunt crco
185		32			15			Battery 1 Shunt Full Current		Bat1 Shunt Curr		Corrente shunt bat 1			Corr shunt bat1
186		32			15			Battery 1 Shunt Full Voltage		Bat1 Shunt Volt		Tensione shunt bat 1			Tens shunt bat1
187		32			15			Battery 2 Shunt Full Current		Bat2 Shunt Curr		Corrente shunt bat 2			Corr shunt bat2
188		32			15			Battery 2 Shunt Full Voltage		Bat2 Shunt Volt		Tensione shunt bat 2			Tens shunt bat2
219		32			15			DO6					DO6			DO6					DO6
220		32			15			DO7					DO7			DO7					DO7
221		32			15			DO8					DO8			DO8					DO8
222		32			15			RS232 Baudrate				RS232 Baudrate		Connessione RS232			Conn RS232
223		32			15			Local Address				Local Addr		Indirizzo locale			Ind locale
224		32			15			Callback Number				Callback Num		Numero di richiamata			Num richiam
225		32			15			Interval Time of Callback		Interval		Intervallo tempo richiamata		Int t richiam
227		32			15			DC Data Flag (On-Off)			DC DataFlag 1		CC data flag				CC data flag
228		32			15			DC Data Flag (Alarm)			DC DataFlag 2		CC allarme data flag			All CC data
229		32			15			Relay Report Method			Relay Report		Metodo report per relè			Modo infoRelé
230		32			15			Fixed					Fixed			Fissato					Fissato
231		32			15			User Defined				User Defined		Utente definito				Utente Def
232		32			15			Rectifier Data Flag (Alarm)		RectDataFlag2		Allarme data flag raddrizzatore		All data RD
233		32			15			Master Controlled			Master Ctrlled		Modalità Master				Master
234		32			15			Slave Controlled			Slave Ctrlled		Modalità Slave				Slave
236		32			15			Over Load Alarm Level			Over Load Lvl		Allarme livello sovraccarico		All sovracc
237		32			15			Overload				Overload		Sovraccarico				Sovracc
238		32			15			System Data Flag (On-Off)		SysData Flag 1		Data flag sistema			Data sistem
239		32			15			System Data Flag (Alarm)		SysData Flag 2		Allarme data flag sistema		All data sistem
240		32			15			Lingua italiana				Italiano		Italiano				Italiano
241		32			15			Imbalance Protection			Imb Protect		Abilita protez squilibrio		Abil prtz sqlb
242		32			15			Enable					Enable			Abilitato				Abilitato
243		32			15			Disable					Disable			Disabilitato				Disabilitato
244		32			15			Buzzer Control				Buzzer Control		Controllo buzzer			Ctrl buzzer
245		32			15			Normal					Normal			Normale					Normale
246		32			15			Disable					Disable			Disabilita				Disabilita
247		32			15			Ambient Temperature Alarm		Amb Temp Alarm		Allarme temperatura ambiente		All temp amb
248		32			15			Normal					Normal			Normale					Normale
249		32			15			Low					Low			Basso					Basso
250		32			15			High					High			Alto					Alto
251		32			15			Reallocate Rectifier Address		Realloc Addr		Riassegna indirizzo raddrizz		Indir RD
252		32			15			Normal					Normal			Normale					Normale
253		32			15			Realloc Addr				Realloc Addr		Riassegna indirizzo			Nuovo indir
254		32			15			Test State Set				Test State Set		Imposta stato di prova			Imp stato prova
255		32			15			Normal					Normal			Normale					Normale
256		32			15			Test State				Test State		Stato di prova				Stato prova
257		32			15			Minification for Test			Minification		Sottovalutazione di prova		Sottovltz prova
258		32			15			Digital Input 1				DI 1			DI 1					DI 1
259		32			15			Digital Input 2				DI 2			DI 2					DI 2
260		32			15			Digital Input 3				DI 3			DI 3					DI 3
261		32			15			Digital Input 4				DI 4			DI 4					DI 4
262		32			15			System Type Value			Sys Type Value		Valore del tipo di sistema		Val tip sist
263		32			15			AC/DC Board Type			AC/DCBoardType		Tipo scheda AC/CC			Scheda AC/CC
264		32			15			B14C3U1					B14C3U1			B14C3U1					B14C3U1
265		32			15			Large DU				Large DU		Grande DU				Grande DU
266		32			15			SPD					SPD			SPD					SPD
267		32			15			Temperature 1				Temp 1			Temperatura 1				Temp 1
268		32			15			Temperature 2				Temp 2			Temperatura 2				Temp 2
269		32			15			Temperature 3				Temp 3			Temperatura 3				Temp 3
270		32			15			Temperature 4				Temp 4			Temperatura 4				Temp 4
271		32			15			Temperature 5				Temp 5			Temperatura 5				Temp 5
272		32			15			Auto Mode				Auto Mode		Modo automático				Modo auto
273		32			15			EMEA					EMEA			EMEA					EMEA
274		32			15			Other					Other			Altro					Altro
275		32			15			Float Voltage				Float Volt		Tensione nominale			Tens nom
276		32			15			Emergency Stop/Shutdown			Emerg Stop		Emergenza stop/spegnimento		EStop/ESpegn
277		32			15			Shunt 1 Full Current			Shunt1 Current		Corrente shunt 1			Corr shunt 1
278		32			15			Shunt 2 Full Current			Shunt2 Current		Corrente shunt 2			Corr shunt 2
279		32			15			Shunt 3 Full Current			Shunt3 Current		Corrente shunt 3			Corr shunt 3
280		32			15			Shunt 1 Full Voltage			Shunt1 Voltage		Tensione shunt 1			Tens shunt 1
281		32			15			Shunt 2 Full Voltage			Shunt2 Voltage		Tensione shunt 1			Tens shunt 1
282		32			15			Shunt 3 Full Voltage			Shunt3 Voltage		Tensione shunt 1			Tens shunt 1
283		32			15			Temperature 1				Temp 1			Sensore temperatura 1			Sens temp 1
284		32			15			Temperature 2				Temp 2			Sensore temperatura 2			Sens temp 2
285		32			15			Temperature 3				Temp 3			Sensore temperatura 3			Sens temp 3
286		32			15			Temperature 4				Temp 4			Sensore temperatura 4			Sens temp 4
287		32			15			Temperature 5				Temp 5			Sensore temperatura 5			Sens temp 5
288		32			15			Very High Temperature 1 Limit		VeryHighTemp1		Limite temperatura 1 molto alta		Mlt alta temp1
289		32			15			Very High Temperature 2 Limit		VeryHighTemp2		Limite temperatura 2 molto alta		Mlt alta temp2
290		32			15			Very High Temperature 3 Limit		VeryHighTemp3		Limite temperatura 3 molto alta		Mlt alta temp3
291		32			15			Very High Temperature 4 Limit		VeryHighTemp4		Limite temperatura 4 molto alta		Mlt alta temp4
292		32			15			Very High Temperature 5 Limit		VeryHighTemp5		Limite temperatura 5 molto alta		Mlt alta temp5
293		32			15			High Temperature 2 Limit		High Temp2		Limite alta temperatura 2		Lim alta temp2
294		32			15			High Temperature 3 Limit		High Temp3		Limite alta temperatura 3		Lim alta temp3
295		32			15			High Temperature 4 Limit		High Temp4		Limite alta temperatura 4		Lim alta temp4
296		32			15			High Temperature 5 Limit		High Temp5		Limite alta temperatura 5		Lim alta temp5
297		32			15			Low Temperature 2 Limit			Low Temp2		Limite bassa temperatura 2		Lim bssa temp2
298		32			15			Low Temperature 3 Limit			Low Temp3		Limite bassa temperatura 3		Lim bssa temp3
299		32			15			Low Temperature 4 Limit			Low Temp4		Limite bassa temperatura 4		Lim bssa temp4
300		32			15			Low Temperature 5 Limit			Low Temp5		Limite bassa temperatura 5		Lim bssa temp5
301		32			15			Temperature 1 not Used			Temp1 not Used		Temperatura 1 non usata			T1 non usata
302		32			15			Temperature 2 not Used			Temp2 not Used		Temperatura 2 non usata			T2 non usata
303		32			15			Temperature 3 not Used			Temp3 not Used		Temperatura 3 non usata			T3 non usata
304		32			15			Temperature 4 not Used			Temp4 not Used		Temperatura 4 non usata			T4 non usata
305		32			15			Temperature 5 not Used			Temp5 not Used		Temperatura 5 non usata			T5 non usata
306		32			15			High Temperature 2			High Temp 2		Alta temperatura 2			Alta temp 2
307		32			15			Low Temperature 2			Low Temp 2		Bassa temperatura 2			Bssa temp 2
308		32			15			Temperature 2 Sensor Fault		T2 Sensor Fault		Guasto sensore temperatura 2		Gsto sens 2
309		32			15			High Temperature 3			High Temp 3		Alta temperatura 3			Alta temp 3
310		32			15			Low Temperature 3			Low Temp 3		Bassa temperatura 3			Bssa temp 3
311		32			15			Temperature 3 Sensor Fault		T3 Sensor Fault		Guasto sensore temperatura 3		Gsto sens 3
312		32			15			High Temperature 4			High Temp 4		Alta temperatura 4			Alta temp 4
313		32			15			Low Temperature 4			Low Temp 4		Bassa temperatura 4			Bssa temp 4
314		32			15			Temperature 4 Sensor Fault		T4 Sensor Fault		Guasto sensore temperatura 4		Gsto sens 4
315		32			15			High Temperature 5			High Temp 5		Alta temperatura 5			Alta temp 5
316		32			15			Low Temperature 5			Low Temp 5		Bassa temperatura 5			Bssa temp 5
317		32			15			Temperature 5 Sensor Fault		T5 Sensor Fault		Guasto sensore temperatura 5		Gsto sens 5
318		32			15			Very High Temperature 1			Very High Temp1		Temperatura 1 molto alta		Mlt alta temp1
319		32			15			Very High Temperature 2			Very High Temp2		Temperatura 2 molto alta		Mlt alta temp2
320		32			15			Very High Temperature 3			Very High Temp3		Temperatura 3 molto alta		Mlt alta temp3
321		32			15			Very High Temperature 4			Very High Temp4		Temperatura 4 molto alta		Mlt alta temp4
322		32			15			Very High Temperature 5			Very High Temp5		Temperatura 5 molto alta		Mlt alta temp5
323		32			15			Energy Saving Status			Energy Saving Status	Stato risparmio energia			Stato risp En
324		32			15			Energy Saving				Energy Saving		Abilita risparmio energia		Abil risp En
325		32			15			Internal Use(I2C)			Internal Use		Uso interno (I2C)			Uso int I2C
326		32			15			Disable					Disable			Disabilita				Disabilita
327		32			15			Emergency Stop				EStop			Stop d'emergenza			EStop
328		32			15			Emeregency Shutdown			EShutdown		Spegnimento d'emergenza			ESpegnmt
329		32			15			Ambient					Ambient			Ambiente				Ambiente
330		32			15			Battery					Battery			Batteria				Batteria
331		32			15			Temperature 6				Temp 6			Temperatura 6				Temp 6
332		32			15			Temperature 7				Temp 7			Temperatura 7				Temp 7
333		32			15			Temperature 6				Temp 6			Sensore temperatura 6			Sens temp 6
334		32			15			Temperature 7				Temp 7			Sensore temperatura 7			Sens temp 7
335		32			15			Very High Temperature 6 Limit		VeryHighTemp6		Temperatura 6 molto alta		Mlt alta temp6
336		32			15			Very High Temperature 7 Limit		VeryHighTemp7		Temperatura 7 molto alta		Mlt alta temp7
337		32			15			High Temperature 6 Limit		High Temp 6		Limite alta temperatura 6		Lim alta temp6
338		32			15			High Temperature 7 Limit		High Temp 7		Limite alta temperatura 7		Lim alta temp7
339		32			15			Low Temperature 6 Limit			Low Temp 6		Limite bassa temperatura 6		Lim bssa temp6
340		32			15			Low Temperature 7 Limit			Low Temp 7		Limite bassa temperatura 7		Lim bssa temp7
341		32			15			EIB Temperature 1 not Used		EIB T1 not Used		Temp6 non usato				T6 non usato
342		32			15			EIB Temperature 2 not Used		EIB T2 not Used		Temp7 non usato				T7 non usato
343		32			15			High Temperature 6			High Temp 6		Alta temperatura 6			Alta temp 6
344		32			15			Low Temperature 6			Low Temp 6		Bassa temperatura 6			Bssa temp 6
345		32			15			Temperature 6 Sensor Fault		T6 Sensor Fault		Guasto sensore temperatura 6		Gsto sens 6
346		32			15			High Temperature 7			High Temp 7		Alta temperatura 7			Alta temp 7
347		32			15			Low Temperature 7			Low Temp 7		Bassa temperatura 7			Bssa temp 7
348		32			15			Temperature7 Sensor Fault		T7 Sensor Fault		Guasto sensore temperatura 7		Gsto sens 7
349		32			15			Very High Temperature 6			Very High Temp6		Temperatura 6 molto alta		Mlt alta temp6
350		32			15			Very High Temperature 7			Very High Temp7		Temperatura 7 molto alta		Mlt alta temp7
501		32			15			Manual Mode				Manual Mode		Modo manuale				Modo manuale
1111		32			15			System Temperature 1			System Temp 1		Temperatura 1				Temp 1
1131		32			15			System Temperature 1			System Temp 1		Abilita temp1				Abil temp1
1132		32			15			Very High Battery Temp 1 Limit		VHighBattTemp1		Limite temperatura 1 molto alta		Mlt alta temp1
1133		32			15			High Battery Temp 1 Limit		Hi Batt Temp 1		Lim alta temperatura 1			Alta temp 1
1134		32			15			Low Battery Temp 1 Limit		Lo Batt Temp 1		Lim bassa temperatura 1			Bssa temp 1
1141		32			15			System Temperature 1 not Used		Sys T1 not Used		Temp1 non usata				T1 non usata
1142		32			15			System Temperature 1 Sensor Fault	Sys T1 Sens Flt		Guasto sensore temperatura 1		Gsto sens 1
1143		32			15			Very High Battery Temperature 1		VHighBattTemp1		Temperatura 1 molto alta		Mlt alta temp1
1144		32			15			High Battery Temperature 1		Hi Batt Temp 1		Alta temperatura 1			Alta temp 1
1145		32			15			Low Battery Temperature 1		Lo Batt Temp 1		Bassa temperatura 1			Bssa temp 1
1211		32			15			System Temperature 2			System Temp 2		Temperatura 2				Temp 2
1231		32			15			System Temperature 2			System Temp 2		Abilita temp2				Abil temp2
1232		32			15			Very High Battery Temp 2 Limit		VHighBattTemp2		Limite temperatura 2 molto alta		Mlt alta temp2
1233		32			15			High Battery Temp 2 Limit		Hi Batt Temp 2		Limite alta temperatura 2		Lim alta temp2
1234		32			15			Low Battery Temp 2 Limit		Lo Batt Temp 2		Limite bassa temperatura 2		Lim bssa temp2
1241		32			15			System Temperature 2 not Used		Sys T2 not Used		Temp2 non usata				T2 non usata
1242		32			15			System Temperature 2 Sensor Fault	Sys T2 Sens Flt		Guasto sensore temperatura 2		Gsto sens 2
1243		32			15			Very High Battery Temperature 2		VHighBattTemp2		Temperatura 2 molto alta		Mlt alta temp2
1244		32			15			High Battery Temperature 2		Hi Batt Temp 2		Alta temperatura 2			Alta temp 2
1245		32			15			Low Battery Temperature 2		Lo Batt Temp 2		Bassa temperatura 2			Bssa temp 2
1311		32			15			System Temperature 3			System Temp 3		Temperatura 3				Temp 3
1331		32			15			System Temperature 3			System Temp 3		Abilita temp3				Abil temp3
1332		32			15			Very High Battery Temp 3 Limit		VHighBattTemp3		Limite temperatura 3 molto alta		Mlt alta temp3
1333		32			15			High Battery Temp 3 Limit		Hi Batt Temp 3		Limite alta temperatura 3		Lim alta temp3
1334		32			15			Low Battery Temp 3 Limit		Lo Batt Temp 3		Limite bassa temperatura 3		Lim bssa temp3
1341		32			15			System Temperature 3 not Used		Sys T3 not Used		Temp3 non usata				T3 non usata
1342		32			15			System Temperature 3 Sensor Fault	Sys T3 Sens Flt		Guasto sensore temperatura 3		Gsto sens 3
1343		32			15			Very High Battery Temperature 3		VHighBattTemp3		Temperatura 3 molto alta		Mlt alta temp3
1344		32			15			High Battery Temperature 3		Hi Batt Temp 3		Alta temperatura 3			Alta temp 3
1345		32			15			Low Battery Temperature 3		Lo Batt Temp 3		Bassa temperatura 3			Bssa temp 3
1411		32			15			IB2 Temperature 1			IB2 Temp 1		Temperatura 4				Temp 4
1431		32			15			IB2 Temperature 1			IB2 Temp 1		Abilita temp4				Abil temp4
1432		32			15			Very High Battery Temp 4 Limit		VHighBattTemp4		Limite temperatura 4 molto alta		Mlt alta temp4
1433		32			15			High Battery Temp 4 Limit		Hi Batt Temp 4		Limite alta temperatura 4		Lim alta temp4
1434		32			15			Low Battery Temp 4 Limit		Lo Batt Temp 4		Limite bassa temperatura 4		Lim bssa temp4
1441		32			15			IB2 Temperature 1 not Used		IB2 T1 not Used		Temp4 non usata				T4 non usata
1442		32			15			IB2 Temperature 1 Sensor Fault		IB2 T1 Sens Flt		Guasto sensore temperatura 4		Gsto sens 4
1443		32			15			Very High Battery Temperature 4		VHighBattTemp4		Temperatura 4 molto alta		Mlt alta temp4
1444		32			15			High Battery Temperature 4		Hi Batt Temp 4		Alta temperatura 4			Alta temp 4
1445		32			15			Low Battery Temperature 4		Lo Batt Temp 4		Bassa temperatura 4			Bssa temp 4
1511		32			15			IB2 Temperature 2			IB2 Temp 2		Temperatura 5				Temp 5
1531		32			15			IB2 Temperature 2			IB2 Temp 2		Abilita temp5				Abil temp5
1532		32			15			Very High Battery Temp 5 Limit		VHighBattTemp5		Limite temperatura 5 molto alta		Mlt alta temp5
1533		32			15			High Battery Temp 5 Limit		Hi Batt Temp 5		Limite alta temperatura 5		Lim alta temp5
1534		32			15			Low Battery Temp 5 Limit		Lo Batt Temp 5		Limite bassa temperatura 5		Lim bssa temp5
1541		32			15			IB2 Temperature 2 not Used		IB2 T2 not Used		Temp5 non usata				T5 non usata
1542		32			15			IB2 Temperature 2 Sensor Fault		IB2 T2 Sens Flt		Guasto sensore temperatura 5		Gsto sens 5
1543		32			15			Very High Battery Temperature 5		VHighBattTemp5		Temperatura 5 molto alta		Mlt alta temp5
1544		32			15			High Battery Temperature 5		Hi Batt Temp 5		Alta temperatura 5			Alta temp 5
1545		32			15			Low Battery Temperature 5		Lo Batt Temp 5		Bassa temperatura 5			Bssa temp 5
1611		32			15			EIB Temperature 1			EIB Temp 1		Temperatura 6				Temp 6
1631		32			15			EIB Temperature 1			EIB Temp 1		Abilita temp6				Abil temp6
1632		32			15			Very High Battery Temp 6 Limit		VHighBattTemp6		Temperatura 6 molto alta		Mlt alta temp6
1633		32			15			High Battery Temp 6 Limit		Hi Batt Temp 6		Límite alta temperatura 6		Lim alta temp6
1634		32			15			Low Battery Temp 6 Limit		Lo Batt Temp 6		Límite bassa temperatura 6		Lim bssa temp6
1641		32			15			EIB Temperature 1 not Used		EIB T1 not Used		Temp6 non usata				T6 non usata
1642		32			15			EIB Temperature 1 Sensor Fault		EIB T1 Sens Flt		Guastoo sensore temperatura 6		Gsto sens 6
1643		32			15			Very High Battery Temperature 6		VHighBattTemp6		Temperatura 6 molto alta		Mlt alta temp6
1644		32			15			High Battery Temperature 6		Hi Batt Temp 6		Alta temperatura 6			Alta temp 6
1645		32			15			Low Battery Temperature 6		Lo Batt Temp 6		Bassa temperatura 6			Bssa temp 6
1711		32			15			EIB Temperature 2			EIB Temp 2		Temperatura 7				Temp 7
1731		32			15			EIB Temperature 2			EIB Temp 2		Abilita temp6				Abil temp7
1732		32			15			Very High Battery Temp 7 Limit		VHighBattTemp7		Temperatura 7 molto alta		Mlt alta temp7
1733		32			15			High Battery Temp 7 Limit		Hi Batt Temp 7		Limite alta temperatura 7		Lim alta temp7
1734		32			15			Low Battery Temp 7 Limit		Lo Batt Temp 7		Limite bassa temperatura 7		Lim bssa temp7
1741		32			15			EIB Temperature 2 not Used		EIB T2 not Used		Temp7 non usata				T7 non usata
1742		32			15			EIB Temperature 2 Sensor Fault		EIB T2 Sens Flt		Guasto sensore temperatura 7		Gsto sens 7
1743		32			15			Very High Battery Temperature 7		VHighBattTemp7		Temperatura 7 molto alta		Mlt alta temp7
1744		32			15			High Battery Temperature 7		Hi Batt Temp 7		Alta temperatura 7			Alta temp 7
1745		32			15			Low Battery Temperature 7		Lo Batt Temp 7		Bassa temperatura 7			Bssa temp 7
1746		32			15			DHCP Failure				DHCP Failure		Guasto DHCP				Gsto DHCP
1747		32			15			Internal Use(CAN)			Internal Use		Uso interno (CAN)			Uso int CAN
1748		32			15			Internal Use(485)			Internal Use		Uso interno (485)			Uso int 485
1749		32			15			Address of Secondary System		Addr Second		Indirizzo (Slave)			Indir slave
1750		32			15			201					201			201					201
1751		32			15			202					202			202					202
1752		32			15			203					203			203					203
1753		32			15			Master/Slave Mode			Master Mode		Modo master/slave			Master/slave
1754		32			15			Master					Master			Master					Modo master
1755		32			15			Slave					Slave			Slave					Modo slave
1756		32			15			Alone					Alone			Da solo					Da solo
1757		32			15			SMBatTemp High Limit			SMBatTemHighLim		Limite alta temp SMBat			Lim alta tSMBat
1758		32			15			SMBatTemp Low Limit			SMBatTempLowLim		Limite bassa temp SMBat			Lim bssa tSMBat
1759		32			15			PLC Config Error			PLC Conf Error		Errore configurazione PLC		Err cfg PLC
1760		32			15			System Expansion			Sys Expansion		Espansione di sistema			Espans. sistema
1761		32			15			Inactive				Inactive		Inattivo				Inattivo
1762		32			15			Primary					Primary			Principale				Principale
1763		32			15			Secondary				Secondary		Secondario				Secondario
1764		128			64			Mode Changed, Auto Config will be started!	Auto Config will be started!	Modo camb, auto cfg ripartirà!		Modo camb, auto cfg ripartirà!
1765		32			15			Former Lang				Former Lang		Lingua					Lingua
1766		32			15			Current Lang				Current Lang		Lingua corrente				Lingua corr
1767		32			15			English					English			Inglese					Inglese
1768		32			15			Italian					Italian			Italiano				Italiano
1769		32			15			RS485 Communication Failure		485 Comm Fail		Comunicazione interrotta 485		Gsto com 485
1770		64			32			SMDU Sampler Mode			SMDU Sampler		Modalità semplice SMDU			Modo SMDU
1771		32			15			CAN					CAN			CAN					CAN
1772		32			15			RS485					RS485			RS485					RS485
1773		32			15			To Auto Delay				To Auto Delay		Ritardo per modo automatico		Rit modo aut
1774		32			15			OBSERVATION SUMMARY			OA SUMMARY		Riassunto all lievi			Elnc all lievi
1775		32			15			MAJOR SUMMARY				MA SUMMARY		Riassunto all normali			Elnc all norm
1776		32			15			CRITICAL SUMMARY			CA SUMMARY		Riassunto all critici			Elnc all crit
1777		32			15			All Rectifiers Current			All Rects Curr		Corrente tot raddrizzatori		Carico tot RD
1778		32			15			Rectifier Group Lost Status		RectGroup Lost		Stato gruppo RD mancanti		Grpo RD mancanti
1779		32			15			Reset Rectifier Lost Alarm		ResetRectLost		Reset all gruppo RD mancanti		Reset RD manc
1780		32			15			Last GroupRectifiers Num		GroupRect Number	Num gruppo RD all'ultimo riavv		Num RD a riavv
1781		32			15			Rectifier Group Lost			Rectifier Group Lost	Gruppo RD mancante			Grpo RD manc
1782		32			15			High Ambient Temp 1 Limit		Hi Amb Temp 1		Lim alta temp ambiente 1		Lim alta t amb1
1783		32			15			Low Ambient Temp 1 Limit		Lo Amb Temp 1		Lim bassa temp ambiente 1		Lim bssa t amb1
1784		32			15			High Ambient Temp 2 Limit		Hi Amb Temp 2		Lim alta temp ambiente 2		Lim alta t amb2
1785		32			15			Low Ambient Temp 2 Limit		Lo Amb Temp 2		Lim bassa temp ambiente 2		Lim bssa t amb2
1786		32			15			High Ambient Temp 3 Limit		Hi Amb Temp 3		Lim alta temp ambiente 3		Lim alta t amb3
1787		32			15			Low Ambient Temp 3 Limit		Lo Amb Temp 3		Lim bassa temp ambiente 3		Lim bssa t amb3
1788		32			15			High Ambient Temp 4 Limit		Hi Amb Temp 4		Lim alta temp ambiente 4		Lim alta t amb4
1789		32			15			Low Ambient Temp 4 Limit		Lo Amb Temp 4		Lim bassa temp ambiente 4		Lim bssa t amb4
1790		32			15			High Ambient Temp 5 Limit		Hi Amb Temp 5		Lim alta temp ambiente 5		Lim alta t amb5
1791		32			15			Low Ambient Temp 5 Limit		Lo Amb Temp 5		Lim bassa temp ambiente 5		Lim bssa t amb5
1792		32			15			High Ambient Temp 6 Limit		Hi Amb Temp 6		Lim alta temp ambiente 6		Lim alta t amb6
1793		32			15			Low Ambient Temp 6 Limit		Lo Amb Temp 6		Lim bassa temp ambiente 6		Lim bssa t amb6
1794		32			15			High Ambient Temp 7 Limit		Hi Amb Temp 7		Lim alta temp ambiente 7		Lim alta t amb7
1795		32			15			Low Ambient Temp 7 Limit		Lo Amb Temp 7		Lim bassa temp ambiente 7		Lim bssa t amb7
1796		32			15			High Ambient Temperature 1		Hi Amb Temp 1		Alta temperatura ambiente 1		Alta t amb1
1797		32			15			Low Ambient Temperature 1		Lo Amb Temp 1		Bassa temperatura ambiente 1		Bssa t amb1
1798		32			15			High Ambient Temperature 2		Hi Amb Temp 1		Alta temperatura ambiente 2		Alta t amb2
1799		32			15			Low Ambient Temperature 2		Lo Amb Temp 2		Bassa temperatura ambiente 2		Bssa t amb2
1800		32			15			High Ambient Temperature 3		Hi Amb Temp 3		Alta temperatura ambiente 3		Alta t amb3
1801		32			15			Low Ambient Temperature 3		Lo Amb Temp 3		Bassa temperatura ambiente 3		Bssa t amb3
1802		32			15			High Ambient Temperature 4		Hi Amb Temp 4		Alta temperatura ambiente 4		Alta t amb4
1803		32			15			Low Ambient Temperature 4		Lo Amb Temp 4		Bassa temperatura ambiente 4		Bssa t amb4
1804		32			15			High Ambient Temperature 5		Hi Amb Temp 5		Alta temperatura ambiente 5		Alta t amb5
1805		32			15			Low Ambient Temperature 5		Lo Amb Temp 5		Bassa temperatura ambiente 5		Bssa t amb5
1806		32			15			High Ambient Temperature 6		Hi Amb Temp 6		Alta temperatura ambiente 6		Alta t amb6
1807		32			15			Low Ambient Temperature 6		Lo Amb Temp 6		Bassa temperatura ambiente 6		Bssa t amb6
1808		32			15			High Ambient Temperature 7		Hi Amb Temp 7		Alta temperatura ambiente 7		Alta t amb7
1809		32			15			Low Ambient Temperature 7		Lo Amb Temp 7		Bassa temperatura ambiente 7		Bssa t amb7
1810		32			15			Fast Sampler Flag			Fast Sampler Flag	Limite esempio veloce			Lim esemp
1811		32			15			LVD Quantity				LVD Quantity		Quantità LVD				Quant LVD
1812		32			15			LCD Rotation				LCD Rotation		Rotazione LCD				Rotaz LCD
1813		32			15			0 deg					0 deg			0 gradi					0 gradi
1814		32			15			90 deg					90 deg			90 gradi				90 gradi
1815		32			15			180 deg					180 deg			180 gradi				180 gradi
1816		32			15			270 deg					270 deg			270 gradi				270 gradi
1817		32			15			Overvoltage 1				Overvolt 1		Sovratensione 1				Sovratens 1
1818		32			15			Overvoltage 2				Overvolt 2		Sovratensione 2				Sovratens 2
1819		32			15			Undervoltage 1				Undervolt 1		Sottotensione 1				Sottotens 1
1820		32			15			Undervoltage 2				Undervolt 2		Sottotensione 2				Sottotens 2
1821		32			15			Overvoltage 1				Overvolt 1		Sovratensione 1				Sovratens 1
1822		32			15			Overvoltage 2				Overvolt 2		Sovratensione 2				Sovratens 2
1823		32			15			Undervoltage 1				Undervolt 1		Sottotensione 1				Sottotens 1
1824		32			15			Undervoltage 2				Undervolt 2		Sottotensione 2				Sottotens 2
1825		32			15			Fail Safe Mode				Fail Safe		Errore modalità sicura			Err Modo norm
1826		32			15			Disable					Disable			Disabilita				Disabilita
1827		32			15			Enable					Enable			Abilita					Abilita
1828		32			15			Hybrid Mode				Hybrid Mode		Modalità ibrida				Modo hibrido
1829		32			15			Disable					Disable			Disabilita				Disabilita
1830		32			15			Capacity				Capacity		Capacità				Capacità
1831		32			15			Cyclic					Cyclic			Ciclico					Ciclico
1832		32			15			DG Run at High Temperature		DG Run Hi Temp		GE con Alta Temperatura			GE Alta Temp
1833		32			15			Used DG for Hybrid			Used DG			Utilizza GE con ibrido			GE in uso
1834		32			15			DG1					DG1			GE1					GE1
1835		32			15			DG2					DG2			GE2					GE2
1836		32			15			Both					Both			Entrambi				Entrambi
1837		32			15			DI for Grid				DI for Grid		DI per Grid				DI per Grid
1838		32			15			DI 1					DI 1			DI 1					DI 1
1839		32			15			DI 2					DI 2			DI 2					DI 2
1840		32			15			DI 3					DI 3			DI 3					DI 3
1841		32			15			DI 4					DI 4			DI 4					DI 4
1842		32			15			DI 5					DI 5			DI 5					DI 5
1843		32			15			DI 6					DI 6			DI 6					DI 6
1844		32			15			DI 7					DI 7			DI 7					DI 7
1845		32			15			DI 8					DI 8			DI 8					DI 8
1846		32			15			Depth of Discharge			DepthDisch		Profondità di scarica			Profon scarica
1847		32			15			Discharge Duration			Disch Duration		Durata di scarica			Durata scarica
1848		32			15			Discharge Start time			Disch Start		Inizio scarica				Inizio scarica
1849		32			15			Diesel Run Over Temperature		DG Run OverTemp		Sovratemperatura GE			SovraTemp GE
1850		32			15			DG1 is Running				DG1 is Running		GE1 in funzione				GE1 in funz
1851		32			15			DG2 is Running				DG2 is Running		GE2 in funzione				GE2 in funz
1852		32			15			Hybrid High Load Level			High Load Lvl		Alto carico Hibrido			Alta Carca Hibr
1853		32			15			Hybrid is High Load			High Load		Carico Hibrido è alto			Alta Carca Hibr
1854		32			15			DG Run Time at High Temperature		DG Run HiTemp		Tempo Alta Temp GE			Alta Temp GE
1855		32			15			Equalising Start Time			EqualStartTime		Tempo inizio Equalizzazione		Inizio Equaliz
1856		32			15			DG1 Failure				DG1 Failure		Guasto GE1				Gsto GE1
1857		32			15			DG2 Failure				DG1 Failure		Guasto GE2				Gsto GE2
1858		32			15			Diesel Alarm Delay			DG Alarm Delay		Ritardo Allarme G.E.			Ritardo All GE
1859		32			15			Grid is on				Grid is on		Grid connessa				Grid connessa
1860		32			15			PowerSplit Contactor Mode		Contactor Mode		Modo contattore PowerSplit		Modo PowerSplit
1861		32			15			Ambient Temperature			Amb Temp		Temperatura Ambiente			Temp Amb
1862		32			15			Ambient Temperature Sensor		Amb Temp Sensor		Sensore Temperatura Ambiente		Sensr Temp Amb
1863		32			15			None					None			Nessuno					Nessuno
1864		32			15			Temperature 1				Temp 1			Temperatura 1				Temp 1
1865		32			15			Temperature 2				Temp 2			Temperatura 2				Temp 2
1866		32			15			Temperature 3				Temp 3			Temperatura 3				Temp 3
1867		32			15			Temperature 4				Temp 4			Temperatura 4				Temp 4
1868		32			15			Temperature 5				Temp 5			Temperatura 5				Temp 5
1869		32			15			Temperature 6				Temp 6			Temperatura 6				Temp 6
1870		32			15			Temperature 7				Temp 7			Temperatura 7				Temp 7
1871		32			15			Temperature 8				Temp 8			Temperatura 8				Temp 8
1872		32			15			Temperature 9				Temp 9			Temperatura 9				Temp 9
1873		32			15			Temperature 10				Temp 10			Temperatura 10				Temp 10
1874		32			15			High Ambient Temperature Level		High Amb Temp		Alta temperatura ambiente		Alta Temp Amb
1875		32			15			Low Ambient Temperature Level		Low Amb Temp		Bassa temperatura ambiente		Bassa Temp Amb
1876		32			15			High Ambient Temperature		High Temperature	Alta temperatura ambiente		Alta Temp Amb
1877		32			15			Low Ambient Temperature			Low Temperature		Bassa temperatura ambiente		Bassa Temp Amb
1878		32			15			Ambient Temp Sensor Fault		Amb Sensor Flt		Guasto sensore temperatura		Gsto sensore T
1879		32			15			Digital Input 1				DI 1			DI 1					DI 1
1880		32			15			Digital Input 2				DI 2			DI 2					DI 2
1881		32			15			Digital Input 3				DI 3			DI 3					DI 3
1882		32			15			Digital Input 4				DI 4			DI 4					DI 4
1883		32			15			Digital Input 5				DI 5			DI 5					DI 5
1884		32			15			Digital Input 6				DI 6			DI 6					DI 6
1885		32			15			Digital Input 7				DI 7			DI 7					DI 7
1886		32			15			Digital Input 8				DI 8			DI 8					DI 8
1887		32			15			Open					Open			Aperto					Aperto
1888		32			15			Closed					Closed			Chiuso					Chiuso
1889		32			15			Relay Output 1				Relay Output 1		Relé uscita 1				Relé usc 1
1890		32			15			Relay Output 2				Relay Output 2		Relé uscita 2				Relé usc 2
1891		32			15			Relay Output 3				Relay Output 3		Relé uscita 3				Relé usc 3
1892		32			15			Relay Output 4				Relay Output 4		Relé uscita 4				Relé usc 4
1893		32			15			Relay Output 5				Relay Output 5		Relé uscita 5				Relé usc 5
1894		32			15			Relay Output 6				Relay Output 6		Relé uscita 6				Relé usc 6
1895		32			15			Relay Output 7				Relay Output 7		Relé uscita 7				Relé usc 7
1896		32			15			Relay Output 8				Relay Output 8		Relé uscita 8				Relé usc 8
1897		32			15			DI1 Alarm State				DI1 Alarm State		Allarme stato DI 1			Allarme DI 1
1898		32			15			DI2 Alarm State				DI2 Alarm State		Allarme stato DI 2			Allarme DI 2
1899		32			15			DI3 Alarm State				DI3 Alarm State		Allarme stato DI 3			Allarme DI 3
1900		32			15			DI4 Alarm State				DI4 Alarm State		Allarme stato DI 4			Allarme DI 4
1901		32			15			DI5 Alarm State				DI5 Alarm State		Allarme stato DI 5			Allarme DI 5
1902		32			15			DI6 Alarm State				DI6 Alarm State		Allarme stato DI 6			Allarme DI 6
1903		32			15			DI7 Alarm State				DI7 Alarm State		Allarme stato DI 7			Allarme DI 7
1904		32			15			DI8 Alarm State				DI8 Alarm State		Allarme stato DI 8			Allarme DI 8
1905		32			15			Protection DC Fault			Prot DC Fault		DC protezione contro guasti		DC prot guast
1906		32			15			Power Limitation			Power Limit		Limite di potenza			Lim pot
1907		32			15			DI3 Alarm				DI3 Alarm		Allarme DI 3				Allarme DI 3
1908		32			15			DI4 Alarm				DI4 Alarm		Allarme DI 4				Allarme DI 4
1909		32			15			DI5 Alarm				DI5 Alarm		Allarme DI 5				Allarme DI 5
1910		32			15			DI6 Alarm				DI6 Alarm		Allarme DI 6				Allarme DI 6
1911		32			15			DI7 Alarm				DI7 Alarm		Allarme DI 7				Allarme DI 7
1912		32			15			DI8 Alarm				DI8 Alarm		Allarme DI 8				Allarme DI 8
1913		32			15			On					On			ACCESO					ACCESO
1914		32			15			Off					Off			SPENTO					SPENTO
1915		32			15			High					High			Alto					Alto
1916		32			15			Low					Low			Basso					Basso
1917		32			15			IB Number				IB Number		Numero di IB				Num IB
1918		32			15			IB Type					IB Type			Tipo IB					Tipo IB
1919		32			15			CSU Undervoltage 1			CSU UV1			CSU sottotensione 1			CSU sottotens1
1920		32			15			CSU Undervoltage 2			CSU UV2			CSU sottotensione 2			CSU sottotens2
1921		32			15			CSU Overvoltage				CSU OV			CSU sovratensione			CSU Sovratens
1922		32			15			CSU Communication Fault			CSU Comm Fault		Guasto comunicazione CSU		Gsto COM CSU
1923		32			15			CSU External Alarm 1			CSU Ext Al1		CSU allarme esterno 1			CSU All EXt1
1924		32			15			CSU External Alarm 2			CSU Ext Al2		CSU allarme esterno 2			CSU All EXt2
1925		32			15			CSU External Alarm 3			CSU Ext Al3		CSU allarme esterno 3			CSU All EXt3
1926		32			15			CSU External Alarm 4			CSU Ext Al4		CSU allarme esterno 4			CSU All EXt4
1927		32			15			CSU External Alarm 5			CSU Ext Al5		CSU allarme esterno 5			CSU All EXt5
1928		32			15			CSU External Alarm 6			CSU Ext Al6		CSU allarme esterno 6			CSU All EXt6
1929		32			15			CSU External Alarm 7			CSU Ext Al7		CSU allarme esterno 7			CSU All EXt7
1930		32			15			CSU External Alarm 8			CSU Ext Al8		CSU allarme esterno 8			CSU All EXt8
1931		32			15			CSU External Communication Fault	CSUExtComm		Guasto comunicazione Ext CSU		Gsto COM Ext
1932		32			15			CSU Battery Current Limit Alarm		CSUBattCurrLim		CSU all limite corrente batt		Lim corr Bat
1933		32			15			DCLCNumber				DCLCNumber		Numero DCLC				Numero DCLC
1934		32			15			BatLCNumber				BatLCNumber		Numero BatLC				Numero BatLC
1935		32			15			Very High Ambient Temperature		VHi Amb Temp		Delta Temp				Delta Temp
1936		32			15			System Type				System Type		Tipo sistema				Tipo sistema
1937		32			15			Normal					Normal			Normale					Normale
1938		32			15			Test					Test			Test					Test
1939		32			15			Auto Mode				Auto Mode		Modo automatico				Modo Autom
1940		32			15			EMEA					EMEA			EMEA					EMEA
1941		32			15			Normal					Normal			Normale					Normale
1942		32			15			Bus Run Mode				Bus Run Mode		Modo BUS RUN				Modo BUS RUN
1943		32			15			NO					NO			NO					NO
1944		32			15			NC					NC			NC					NC
1945		32			15			Fail Safe Mode(Hybrid)			Fail Safe		Modo fallire sicuro (Ibrido)		Modo fall sic
1946		32			15			IB Communication Failure		IB Comm Fail		Guasto IB				Guasto IB
1947		32			15			Relay Testing				Relay Testing		Test dei relè				Test dei Relè
1948		32			15			Testing Relay 1				Testing Relay 1		Prova relè 1				Prova relè 1
1949		32			15			Testing Relay 2				Testing Relay 2		Prova relè 2				Prova relè 2
1950		32			15			Testing Relay 3				Testing Relay 3		Prova relè 3				Prova relè 3
1951		32			15			Testing Relay 4				Testing Relay 4		Prova relè 4				Prova relè 4
1952		32			15			Testing Relay 5				Testing Relay 5		Prova relè 5				Prova relè 5
1953		32			15			Testing Relay 6				Testing Relay 6		Prova relè 6				Prova relè 6
1954		32			15			Testing Relay 7				Testing Relay 7		Prova relè 7				Prova relè 7
1955		32			15			Testing Relay 8				Testing Relay 8		Prova relè 8				Prova relè 8
1956		32			15			Relay Test				Relay Test		Test Relè				Test relè
1957		32			15			Relay Test Time				Relay Test Time		Ora Test Relè				Ora Test Relè
1958		32			15			Manual					Manual			Manuale					Manuale
1959		32			15			Automatic				Automatic		Automatico				Automatico
1960		32			15			System Temperature 1			Sys Temp1		Temp1 (ACU)				Temp1 (ACU)
1961		32			15			System Temperature 2			Sys Temp2		Temp2 (ACU)				Temp2 (ACU)
1962		32			15			System Temperature 3			Sys Temp3		Temp3 (OB)				Temp3 (OB)
1963		32			15			IB2 Temperature 1			IB2 Temp1		IB2-Temp1				IB2-Temp1
1964		32			15			IB2 Temperature 2			IB2 Temp2		IB2-Temp2				IB2-Temp2
1965		32			15			EIB Temperature 1			EIB Temp1		EIB-Temp1				EIB-Temp1
1966		32			15			EIB Temperature 2			EIB Temp2		EIB-Temp2				EIB-Temp2
1967		32			15			SMTemp1 Temp1				SMTemp1 T1		SMTemp1 Temp1				SMTemp1 T1
1968		32			15			SMTemp1 Temp2				SMTemp1 T2		SMTemp1 Temp2				SMTemp1 T2
1969		32			15			SMTemp1 Temp3				SMTemp1 T3		SMTemp1 Temp3				SMTemp1 T3
1970		32			15			SMTemp1 Temp4				SMTemp1 T4		SMTemp1 Temp4				SMTemp1 T4
1971		32			15			SMTemp1 Temp5				SMTemp1 T5		SMTemp1 Temp5				SMTemp1 T5
1972		32			15			SMTemp1 Temp6				SMTemp1 T6		SMTemp1 Temp6				SMTemp1 T6
1973		32			15			SMTemp1 Temp7				SMTemp1 T7		SMTemp1 Temp7				SMTemp1 T7
1974		32			15			SMTemp1 Temp8				SMTemp1 T8		SMTemp1 Temp8				SMTemp1 T8
1975		32			15			SMTemp2 Temp1				SMTemp2 T1		SMTemp2 Temp1				SMTemp2 T1
1976		32			15			SMTemp2 Temp2				SMTemp2 T2		SMTemp2 Temp2				SMTemp2 T2
1977		32			15			SMTemp2 Temp3				SMTemp2 T3		SMTemp2 Temp3				SMTemp2 T3
1978		32			15			SMTemp2 Temp4				SMTemp2 T4		SMTemp2 Temp4				SMTemp2 T4
1979		32			15			SMTemp2 Temp5				SMTemp2 T5		SMTemp2 Temp5				SMTemp2 T5
1980		32			15			SMTemp2 Temp6				SMTemp2 T6		SMTemp2 Temp6				SMTemp2 T6
1981		32			15			SMTemp2 Temp7				SMTemp2 T7		SMTemp2 Temp7				SMTemp2 T7
1982		32			15			SMTemp2 Temp8				SMTemp2 T8		SMTemp2 Temp8				SMTemp2 T8
1983		32			15			SMTemp3 Temp1				SMTemp3 T1		SMTemp3 Temp1				SMTemp3 T1
1984		32			15			SMTemp3 Temp2				SMTemp3 T2		SMTemp3 Temp2				SMTemp3 T2
1985		32			15			SMTemp3 Temp3				SMTemp3 T3		SMTemp3 Temp3				SMTemp3 T3
1986		32			15			SMTemp3 Temp4				SMTemp3 T4		SMTemp3 Temp4				SMTemp3 T4
1987		32			15			SMTemp3 Temp5				SMTemp3 T5		SMTemp3 Temp5				SMTemp3 T5
1988		32			15			SMTemp3 Temp6				SMTemp3 T6		SMTemp3 Temp6				SMTemp3 T6
1989		32			15			SMTemp3 Temp7				SMTemp3 T7		SMTemp3 Temp7				SMTemp3 T7
1990		32			15			SMTemp3 Temp8				SMTemp3 T8		SMTemp3 Temp8				SMTemp3 T8
1991		32			15			SMTemp4 Temp1				SMTemp4 T1		SMTemp4 Temp1				SMTemp4 T1
1992		32			15			SMTemp4 Temp2				SMTemp4 T2		SMTemp4 Temp2				SMTemp4 T2
1993		32			15			SMTemp4 Temp3				SMTemp4 T3		SMTemp4 Temp3				SMTemp4 T3
1994		32			15			SMTemp4 Temp4				SMTemp4 T4		SMTemp4 Temp4				SMTemp4 T4
1995		32			15			SMTemp4 Temp5				SMTemp4 T5		SMTemp4 Temp5				SMTemp4 T5
1996		32			15			SMTemp4 Temp6				SMTemp4 T6		SMTemp4 Temp6				SMTemp4 T6
1997		32			15			SMTemp4 Temp7				SMTemp4 T7		SMTemp4 Temp7				SMTemp4 T7
1998		32			15			SMTemp4 Temp8				SMTemp4 T8		SMTemp4 Temp8				SMTemp4 T8
1999		32			15			SMTemp5 Temp1				SMTemp5 T1		SMTemp5 Temp1				SMTemp5 T1
2000		32			15			SMTemp5 Temp2				SMTemp5 T2		SMTemp5 Temp2				SMTemp5 T2
2001		32			15			SMTemp5 Temp3				SMTemp5 T3		SMTemp5 Temp3				SMTemp5 T3
2002		32			15			SMTemp5 Temp4				SMTemp5 T4		SMTemp5 Temp4				SMTemp5 T4
2003		32			15			SMTemp5 Temp5				SMTemp5 T5		SMTemp5 Temp5				SMTemp5 T5
2004		32			15			SMTemp5 Temp6				SMTemp5 T6		SMTemp5 Temp6				SMTemp5 T6
2005		32			15			SMTemp5 Temp7				SMTemp5 T7		SMTemp5 Temp7				SMTemp5 T7
2006		32			15			SMTemp5 Temp8				SMTemp5 T8		SMTemp5 Temp8				SMTemp5 T8
2007		32			15			SMTemp6 Temp1				SMTemp6 T1		SMTemp6 Temp1				SMTemp6 T1
2008		32			15			SMTemp6 Temp2				SMTemp6 T2		SMTemp6 Temp2				SMTemp6 T2
2009		32			15			SMTemp6 Temp3				SMTemp6 T3		SMTemp6 Temp3				SMTemp6 T3
2010		32			15			SMTemp6 Temp4				SMTemp6 T4		SMTemp6 Temp4				SMTemp6 T4
2011		32			15			SMTemp6 Temp5				SMTemp6 T5		SMTemp6 Temp5				SMTemp6 T5
2012		32			15			SMTemp6 Temp6				SMTemp6 T6		SMTemp6 Temp6				SMTemp6 T6
2013		32			15			SMTemp6 Temp7				SMTemp6 T7		SMTemp6 Temp7				SMTemp6 T7
2014		32			15			SMTemp6 Temp8				SMTemp6 T8		SMTemp6 Temp8				SMTemp6 T8
2015		32			15			SMTemp7 Temp1				SMTemp7 T1		SMTemp7 Temp1				SMTemp7 T1
2016		32			15			SMTemp7 Temp2				SMTemp7 T2		SMTemp7 Temp2				SMTemp7 T2
2017		32			15			SMTemp7 Temp3				SMTemp7 T3		SMTemp7 Temp3				SMTemp7 T3
2018		32			15			SMTemp7 Temp4				SMTemp7 T4		SMTemp7 Temp4				SMTemp7 T4
2019		32			15			SMTemp7 Temp5				SMTemp7 T5		SMTemp7 Temp5				SMTemp7 T5
2020		32			15			SMTemp7 Temp6				SMTemp7 T6		SMTemp7 Temp6				SMTemp7 T6
2021		32			15			SMTemp7 Temp7				SMTemp7 T7		SMTemp7 Temp7				SMTemp7 T7
2022		32			15			SMTemp7 Temp8				SMTemp7 T8		SMTemp7 Temp8				SMTemp7 T8
2023		32			15			SMTemp8 Temp1				SMTemp8 T1		SMTemp8 Temp1				SMTemp8 T1
2024		32			15			SMTemp8 Temp2				SMTemp8 T2		SMTemp8 Temp2				SMTemp8 T2
2025		32			15			SMTemp8 Temp3				SMTemp8 T3		SMTemp8 Temp3				SMTemp8 T3
2026		32			15			SMTemp8 Temp4				SMTemp8 T4		SMTemp8 Temp4				SMTemp8 T4
2027		32			15			SMTemp8 Temp5				SMTemp8 T5		SMTemp8 Temp5				SMTemp8 T5
2028		32			15			SMTemp8 Temp6				SMTemp8 T6		SMTemp8 Temp6				SMTemp8 T6
2029		32			15			SMTemp8 Temp7				SMTemp8 T7		SMTemp8 Temp7				SMTemp8 T7
2030		32			15			SMTemp8 Temp8				SMTemp8 T8		SMTemp8 Temp8				SMTemp8 T8
2031		32			15			System Temperature 1 Very High		Sys T1 VHi		Sistema Temperatura 1 molto alta	Sis T1 mltoAlta
2032		32			15			System Temperature 1 High		Sys T1 Hi		Sistema Temperatura 1 alta		Sis T1 Alta
2033		32			15			System Temperature 1 Low		Sys T1 Low		Sistema Temperatura 1 bassa		Sis T1 Bassa
2034		32			15			System Temperature 2 Very High		Sys T2 VHi		Sistema Temperatura 2 molto alta	Sis T2 mltoAlta
2035		32			15			System Temperature 2 High		Sys T2 Hi		Sistema Temperatura 2 alta		Sis T2 Alta
2036		32			15			System Temperature 2 Low		Sys T2 Hi		Sistema Temperatura 2 bassa		Sis T2 Bassa
2037		32			15			System Temperature 3 Very High		Sys T3 VHi		Sistema Temperatura 3 molto alta	Sis T3 mltoAlta
2038		32			15			System Temperature 3 High		Sys T3 Low		Sistema Temperatura 3 alta		Sis T3 Alta
2039		32			15			System Temperature 3 Low		Sys T3 Low		Sistema Temperatura 3 bassa		Sis T3 Bassa
2040		32			15			IB2 Temperature 1 Very High		IB2 T1 VHi		IB2 Temperatura 1 molto alta		IB2 T1 mltoAlta
2041		32			15			IB2 Temperature 1 High			IB2 T1 Hi		IB2 Temperatura 1 alta			IB2 T1 Alta
2042		32			15			IB2 Temperature 1 Low			IB2 T1 Low		IB2 Temperatura 1 bassa			IB2 T1 Bassa
2043		32			15			IB2 Temperature 2 Very High		IB2 T2 VHi		IB2 Temperatura 2 molto alta		IB2 T2 mltoAlta
2044		32			15			IB2 Temperature 2 High			IB2 T2 Hi		IB2 Temperatura 2 alta			IB2 T2 Alta
2045		32			15			IB2 Temperature 2 Low			IB2 T2 Low		IB2 Temperatura 2 bassa			IB2 T2 Bassa
2046		32			15			EIB Temperature 1 Very High		EIB T1 VHi		EIB Temperatura 1 molto alta		EIB T1 mltoAlta
2047		32			15			EIB Temperature 1 High			EIB T1 Hi		EIB Temperatura 1 alta			EIB T1 Alta
2048		32			15			EIB Temperature 1 Low			EIB T1 Low		EIB Temperatura 1 bassa			EIB T1 Bassa
2049		32			15			EIB Temperature 2 Very High		EIB T2 VHi		EIB Temperatura 1 molto alta		EIB T1 mltoAlta
2050		32			15			EIB Temperature 2 High			EIB T2 Hi		EIB Temperatura 1 alta			EIB T1 Alta
2051		32			15			EIB Temperature 2 Low			EIB T2 Low		EIB Temperatura 1 bassa			EIB T1 Bassa
2052		32			15			SMTemp1 Temp1 High 2			SMTemp1 T1 Hi2		SMTemp1 T1 Alta 2			SMTemp1 T1Alta2
2053		32			15			SMTemp1 Temp1 High 1			SMTemp1 T1 Hi1		SMTemp1 T1 Alta 1			SMTemp1 T1Alta1
2054		32			15			SMTemp1 Temp1 Low			SMTemp1 T1 Low		SMTemp1 T1 Bassa			SMTemp1 T1Bassa
2055		32			15			SMTemp1 Temp2 High 2			SMTemp1 T2 Hi2		SMTemp1 T2 Alta 2			SMTemp1 T2Alta2
2056		32			15			SMTemp1 Temp2 High 1			SMTemp1 T2 Hi1		SMTemp1 T2 Alta 1			SMTemp1 T2Alta1
2057		32			15			SMTemp1 Temp2 Low			SMTemp1 T2 Low		SMTemp1 T2 Bassa			SMTemp1 T2Bassa
2058		32			15			SMTemp1 Temp3 High 2			SMTemp1 T3 Hi2		SMTemp1 T3 Alta 2			SMTemp1 T3Alta2
2059		32			15			SMTemp1 Temp3 High 1			SMTemp1 T3 Hi1		SMTemp1 T3 Alta 1			SMTemp1 T3Alta1
2060		32			15			SMTemp1 Temp3 Low			SMTemp1 T3 Low		SMTemp1 T3 Bassa			SMTemp1 T3Bassa
2061		32			15			SMTemp1 Temp4 High 2			SMTemp1 T4 Hi2		SMTemp1 T4 Alta 2			SMTemp1 T4Alta2
2062		32			15			SMTemp1 Temp4 High 1			SMTemp1 T4 Hi1		SMTemp1 T4 Alta 1			SMTemp1 T4Alta1
2063		32			15			SMTemp1 Temp4 Low			SMTemp1 T4 Low		SMTemp1 T4 Bassa			SMTemp1 T4Bassa
2064		32			15			SMTemp1 Temp5 High 2			SMTemp1 T5 Hi2		SMTemp1 T5 Alta 2			SMTemp1 T5Alta2
2065		32			15			SMTemp1 Temp5 High 1			SMTemp1 T5 Hi1		SMTemp1 T5 Alta 1			SMTemp1 T5Alta1
2066		32			15			SMTemp1 Temp5 Low			SMTemp1 T5 Low		SMTemp1 T5 Bassa			SMTemp1 T5Bassa
2067		32			15			SMTemp1 Temp6 High 2			SMTemp1 T6 Hi2		SMTemp1 T6 Alta 2			SMTemp1 T6Alta2
2068		32			15			SMTemp1 Temp6 High 1			SMTemp1 T6 Hi1		SMTemp1 T6 Alta 1			SMTemp1 T6Alta1
2069		32			15			SMTemp1 Temp6 Low			SMTemp1 T6 Low		SMTemp1 T6 Bassa			SMTemp1 T6Bassa
2070		32			15			SMTemp1 Temp7 High 2			SMTemp1 T7 Hi2		SMTemp1 T7 Alta 2			SMTemp1 T7Alta2
2071		32			15			SMTemp1 Temp7 High 1			SMTemp1 T7 Hi1		SMTemp1 T7 Alta 1			SMTemp1 T7Alta1
2072		32			15			SMTemp1 Temp7 Low			SMTemp1 T7 Low		SMTemp1 T7 Bassa			SMTemp1 T7Bassa
2073		32			15			SMTemp1 Temp8 High 2			SMTemp1 T8 Hi2		SMTemp1 T8 Alta 2			SMTemp1 T8Alta2
2074		32			15			SMTemp1 Temp8 High 1			SMTemp1 T8 Hi1		SMTemp1 T8 Alta 1			SMTemp1 T8Alta1
2075		32			15			SMTemp1 Temp8 Low			SMTemp1 T8 Low		SMTemp1 T8 Bassa			SMTemp1 T8Bassa
2076		32			15			SMTemp2 Temp1 High 2			SMTemp2 T1 Hi2		SMTemp2 T1 Alta 2			SMTemp2 T1Alta2
2077		32			15			SMTemp2 Temp1 High 1			SMTemp2 T1 Hi1		SMTemp2 T1 Alta 1			SMTemp2 T1Alta1
2078		32			15			SMTemp2 Temp1 Low			SMTemp2 T1 Low		SMTemp2 T1 Bassa			SMTemp2 T1Bassa
2079		32			15			SMTemp2 Temp2 High 2			SMTemp2 T2 Hi2		SMTemp2 T2 Alta 2			SMTemp2 T2Alta2
2080		32			15			SMTemp2 Temp2 High 1			SMTemp2 T2 Hi1		SMTemp2 T2 Alta 1			SMTemp2 T2Alta1
2081		32			15			SMTemp2 Temp2 Low			SMTemp2 T2 Low		SMTemp2 T2 Bassa			SMTemp2 T2Bassa
2082		32			15			SMTemp2 Temp3 High 2			SMTemp2 T3 Hi2		SMTemp2 T3 Alta 2			SMTemp2 T3Alta2
2083		32			15			SMTemp2 Temp3 High 1			SMTemp2 T3 Hi1		SMTemp2 T3 Alta 1			SMTemp2 T3Alta1
2084		32			15			SMTemp2 Temp3 Low			SMTemp2 T3 Low		SMTemp2 T3 Bassa			SMTemp2 T3Bassa
2085		32			15			SMTemp2 Temp4 High 2			SMTemp2 T4 Hi2		SMTemp2 T4 Alta 2			SMTemp2 T4Alta2
2086		32			15			SMTemp2 Temp4 High 1			SMTemp2 T4 Hi1		SMTemp2 T4 Alta 1			SMTemp2 T4Alta1
2087		32			15			SMTemp2 Temp4 Low			SMTemp2 T4 Low		SMTemp2 T4 Bassa			SMTemp2 T4Bassa
2088		32			15			SMTemp2 Temp5 High 2			SMTemp2 T5 Hi2		SMTemp2 T5 Alta 2			SMTemp2 T5Alta2
2089		32			15			SMTemp2 Temp5 High 1			SMTemp2 T5 Hi1		SMTemp2 T5 Alta 1			SMTemp2 T5Alta1
2090		32			15			SMTemp2 Temp5 Low			SMTemp2 T5 Low		SMTemp2 T5 Bassa			SMTemp2 T5Bassa
2091		32			15			SMTemp2 Temp6 High 2			SMTemp2 T6 Hi2		SMTemp2 T6 Alta 2			SMTemp2 T6Alta2
2092		32			15			SMTemp2 Temp6 High 1			SMTemp2 T6 Hi1		SMTemp2 T6 Alta 1			SMTemp2 T6Alta1
2093		32			15			SMTemp2 Temp6 Low			SMTemp2 T6 Low		SMTemp2 T6 Bassa			SMTemp2 T6Bassa
2094		32			15			SMTemp2 Temp7 High 2			SMTemp2 T7 Hi2		SMTemp2 T7 Alta 2			SMTemp2 T7Alta2
2095		32			15			SMTemp2 Temp7 High 1			SMTemp2 T7 Hi1		SMTemp2 T7 Alta 1			SMTemp2 T7Alta1
2096		32			15			SMTemp2 Temp7 Low			SMTemp2 T7 Low		SMTemp2 T7 Bassa			SMTemp2 T7Bassa
2097		32			15			SMTemp2 Temp8 High 2			SMTemp2 T8 Hi2		SMTemp2 T8 Alta 2			SMTemp2 T8Alta2
2098		32			15			SMTemp2 Temp8 High 1			SMTemp2 T8 Hi1		SMTemp2 T8 Alta 1			SMTemp2 T8Alta1
2099		32			15			SMTemp2 Temp8 Low			SMTemp2 T8 Low		SMTemp2 T8 Bassa			SMTemp2 T8Bassa
2100		32			15			SMTemp3 Temp1 High 2			SMTemp3 T1 Hi2		SMTemp3 T1 Alta 2			SMTemp3 T1Alta2
2101		32			15			SMTemp3 Temp1 High 1			SMTemp3 T1 Hi1		SMTemp3 T1 Alta 1			SMTemp3 T1Alta1
2102		32			15			SMTemp3 Temp1 Low			SMTemp3 T1 Low		SMTemp3 T1 Bassa			SMTemp3 T1Bassa
2103		32			15			SMTemp3 Temp2 High 2			SMTemp3 T2 Hi2		SMTemp3 T2 Alta 2			SMTemp3 T2Alta2
2104		32			15			SMTemp3 Temp2 High 1			SMTemp3 T2 Hi1		SMTemp3 T2 Alta 1			SMTemp3 T2Alta1
2105		32			15			SMTemp3 Temp2 Low			SMTemp3 T2 Low		SMTemp3 T2 Bassa			SMTemp3 T2Bassa
2106		32			15			SMTemp3 Temp3 High 2			SMTemp3 T3 Hi2		SMTemp3 T3 Alta 2			SMTemp3 T3Alta2
2107		32			15			SMTemp3 Temp3 High 1			SMTemp3 T3 Hi1		SMTemp3 T3 Alta 1			SMTemp3 T3Alta1
2108		32			15			SMTemp3 Temp3 Low			SMTemp3 T3 Low		SMTemp3 T3 Bassa			SMTemp3 T3Bassa
2109		32			15			SMTemp3 Temp4 High 2			SMTemp3 T4 Hi2		SMTemp3 T4 Alta 2			SMTemp3 T4Alta2
2110		32			15			SMTemp3 Temp4 High 1			SMTemp3 T4 Hi1		SMTemp3 T4 Alta 1			SMTemp3 T4Alta1
2111		32			15			SMTemp3 Temp4 Low			SMTemp3 T4 Low		SMTemp3 T4 Bassa			SMTemp3 T4Bassa
2112		32			15			SMTemp3 Temp5 High 2			SMTemp3 T5 Hi2		SMTemp3 T5 Alta 2			SMTemp3 T5Alta2
2113		32			15			SMTemp3 Temp5 High 1			SMTemp3 T5 Hi1		SMTemp3 T5 Alta 1			SMTemp3 T5Alta1
2114		32			15			SMTemp3 Temp5 Low			SMTemp3 T5 Low		SMTemp3 T5 Bassa			SMTemp3 T5Bassa
2115		32			15			SMTemp3 Temp6 High 2			SMTemp3 T6 Hi2		SMTemp3 T6 Alta 2			SMTemp3 T6Alta2
2116		32			15			SMTemp3 Temp6 High 1			SMTemp3 T6 Hi1		SMTemp3 T6 Alta 1			SMTemp3 T6Alta1
2117		32			15			SMTemp3 Temp6 Low			SMTemp3 T6 Low		SMTemp3 T6 Bassa			SMTemp3 T6Bassa
2118		32			15			SMTemp3 Temp7 High 2			SMTemp3 T7 Hi2		SMTemp3 T7 Alta 2			SMTemp3 T7Alta2
2119		32			15			SMTemp3 Temp7 High 1			SMTemp3 T7 Hi1		SMTemp3 T7 Alta 1			SMTemp3 T7Alta1
2120		32			15			SMTemp3 Temp7 Low			SMTemp3 T7 Low		SMTemp3 T7 Bassa			SMTemp3 T7Bassa
2121		32			15			SMTemp3 Temp8 High 2			SMTemp3 T8 Hi2		SMTemp3 T8 Alta 2			SMTemp3 T8Alta2
2122		32			15			SMTemp3 Temp8 High 1			SMTemp3 T8 Hi1		SMTemp3 T8 Alta 1			SMTemp3 T8Alta1
2123		32			15			SMTemp3 Temp8 Low			SMTemp3 T8 Low		SMTemp3 T8 Bassa			SMTemp3 T8Bassa
2124		32			15			SMTemp4 Temp1 High 2			SMTemp4 T1 Hi2		SMTemp4 T1 Alta 2			SMTemp4 T1Alta2
2125		32			15			SMTemp4 Temp1 High 1			SMTemp4 T1 Hi1		SMTemp4 T1 Alta 1			SMTemp4 T1Alta1
2126		32			15			SMTemp4 Temp1 Low			SMTemp4 T1 Low		SMTemp4 T1 Bassa			SMTemp4 T1Bassa
2127		32			15			SMTemp4 Temp2 High 2			SMTemp4 T2 Hi2		SMTemp4 T2 Alta 2			SMTemp4 T2Alta2
2128		32			15			SMTemp4 Temp2 High 1			SMTemp4 T2 Hi1		SMTemp4 T2 Alta 1			SMTemp4 T2Alta1
2129		32			15			SMTemp4 Temp2 Low			SMTemp4 T2 Low		SMTemp4 T2 Bassa			SMTemp4 T2Bassa
2130		32			15			SMTemp4 Temp3 High 2			SMTemp4 T3 Hi2		SMTemp4 T3 Alta 2			SMTemp4 T3Alta2
2131		32			15			SMTemp4 Temp3 High 1			SMTemp4 T3 Hi1		SMTemp4 T3 Alta 1			SMTemp4 T3Alta1
2132		32			15			SMTemp4 Temp3 Low			SMTemp4 T3 Low		SMTemp4 T3 Bassa			SMTemp4 T3Bassa
2133		32			15			SMTemp4 Temp4 High 2			SMTemp4 T4 Hi2		SMTemp4 T4 Alta 2			SMTemp4 T4Alta2
2134		32			15			SMTemp4 Temp4 High 1			SMTemp4 T4 Hi1		SMTemp4 T4 Alta 1			SMTemp4 T4Alta1
2135		32			15			SMTemp4 Temp4 Low			SMTemp4 T4 Low		SMTemp4 T4 Bassa			SMTemp4 T4Bassa
2136		32			15			SMTemp4 Temp5 High 2			SMTemp4 T5 Hi2		SMTemp4 T5 Alta 2			SMTemp4 T5Alta2
2137		32			15			SMTemp4 Temp5 High 1			SMTemp4 T5 Hi1		SMTemp4 T5 Alta 1			SMTemp4 T5Alta1
2138		32			15			SMTemp4 Temp5 Low			SMTemp4 T5 Low		SMTemp4 T5 Bassa			SMTemp4 T5Bassa
2139		32			15			SMTemp4 Temp6 High 2			SMTemp4 T6 Hi2		SMTemp4 T6 Alta 2			SMTemp4 T6Alta2
2140		32			15			SMTemp4 Temp6 High 1			SMTemp4 T6 Hi1		SMTemp4 T6 Alta 1			SMTemp4 T6Alta1
2141		32			15			SMTemp4 Temp6 Low			SMTemp4 T6 Low		SMTemp4 T6 Bassa			SMTemp4 T6Bassa
2142		32			15			SMTemp4 Temp7 High 2			SMTemp4 T7 Hi2		SMTemp4 T7 Alta 2			SMTemp4 T7Alta2
2143		32			15			SMTemp4 Temp7 High 1			SMTemp4 T7 Hi1		SMTemp4 T7 Alta 1			SMTemp4 T7Alta1
2144		32			15			SMTemp4 Temp7 Low			SMTemp4 T7 Low		SMTemp4 T7 Bassa			SMTemp4 T7Bassa
2145		32			15			SMTemp4 Temp8 High 2			SMTemp4 T8 Hi2		SMTemp4 T8 Alta 2			SMTemp4 T8Alta2
2146		32			15			SMTemp4 Temp8 Hi1			SMTemp4 T8 Hi1		SMTemp4 T8 Alta 1			SMTemp4 T8Alta1
2147		32			15			SMTemp4 Temp8 Low			SMTemp4 T8 Low		SMTemp4 T8 Bassa			SMTemp4 T8Bassa
2148		32			15			SMTemp5 Temp1 High 2			SMTemp5 T1 Hi2		SMTemp5 T1 Alta 2			SMTemp5 T1Alta2
2149		32			15			SMTemp5 Temp1 High 1			SMTemp5 T1 Hi1		SMTemp5 T1 Alta 1			SMTemp5 T1Alta1
2150		32			15			SMTemp5 Temp1 Low			SMTemp5 T1 Low		SMTemp5 T1 Bassa			SMTemp5 T1Bassa
2151		32			15			SMTemp5 Temp2 High 2			SMTemp5 T2 Hi2		SMTemp5 T2 Alta 2			SMTemp5 T2Alta2
2152		32			15			SMTemp5 Temp2 High 1			SMTemp5 T2 Hi1		SMTemp5 T2 Alta 1			SMTemp5 T2Alta1
2153		32			15			SMTemp5 Temp2 Low			SMTemp5 T2 Low		SMTemp5 T2 Bassa			SMTemp5 T2Bassa
2154		32			15			SMTemp5 Temp3 High 2			SMTemp5 T3 Hi2		SMTemp5 T3 Alta 2			SMTemp5 T3Alta2
2155		32			15			SMTemp5 Temp3 High 1			SMTemp5 T3 Hi1		SMTemp5 T3 Alta 1			SMTemp5 T3Alta1
2156		32			15			SMTemp5 Temp3 Low			SMTemp5 T3 Low		SMTemp5 T3 Bassa			SMTemp5 T3Bassa
2157		32			15			SMTemp5 Temp4 High 2			SMTemp5 T4 Hi2		SMTemp5 T4 Alta 2			SMTemp5 T4Alta2
2158		32			15			SMTemp5 Temp4 High 1			SMTemp5 T4 Hi1		SMTemp5 T4 Alta 1			SMTemp5 T4Alta1
2159		32			15			SMTemp5 Temp4 Low			SMTemp5 T4 Low		SMTemp5 T4 Bassa			SMTemp5 T4Bassa
2160		32			15			SMTemp5 Temp5 High 2			SMTemp5 T5 Hi2		SMTemp5 T5 Alta 2			SMTemp5 T5Alta2
2161		32			15			SMTemp5 Temp5 High 1			SMTemp5 T5 Hi1		SMTemp5 T5 Alta 1			SMTemp5 T5Alta1
2162		32			15			SMTemp5 Temp5 Low			SMTemp5 T5 Low		SMTemp5 T5 Bassa			SMTemp5 T5Bassa
2163		32			15			SMTemp5 Temp6 High 2			SMTemp5 T6 Hi2		SMTemp5 T6 Alta 2			SMTemp5 T6Alta2
2164		32			15			SMTemp5 Temp6 High 1			SMTemp5 T6 Hi1		SMTemp5 T6 Alta 1			SMTemp5 T6Alta1
2165		32			15			SMTemp5 Temp6 Low			SMTemp5 T6 Low		SMTemp5 T6 Bassa			SMTemp5 T6Bassa
2166		32			15			SMTemp5 Temp7 High 2			SMTemp5 T7 Hi2		SMTemp5 T7 Alta 2			SMTemp5 T7Alta2
2167		32			15			SMTemp5 Temp7 High 1			SMTemp5 T7 Hi1		SMTemp5 T7 Alta 1			SMTemp5 T7Alta1
2168		32			15			SMTemp5 Temp7 Low			SMTemp5 T7 Low		SMTemp5 T7 Bassa			SMTemp5 T7Bassa
2169		32			15			SMTemp5 Temp8 High 2			SMTemp5 T8 Hi2		SMTemp5 T8 Alta 2			SMTemp5 T8Alta2
2170		32			15			SMTemp5 Temp8 High 1			SMTemp5 T8 Hi1		SMTemp5 T8 Alta 1			SMTemp5 T8Alta1
2171		32			15			SMTemp5 Temp8 Low			SMTemp5 T8 Low		SMTemp5 T8 Bassa			SMTemp5 T8Bassa
2172		32			15			SMTemp6 Temp1 High 2			SMTemp6 T1 Hi2		SMTemp6 T1 Alta 2			SMTemp6 T1Alta2
2173		32			15			SMTemp6 Temp1 High 1			SMTemp6 T1 Hi1		SMTemp6 T1 Alta 1			SMTemp6 T1Alta1
2174		32			15			SMTemp6 Temp1 Low			SMTemp6 T1 Low		SMTemp6 T1 Bassa			SMTemp6 T1Bassa
2175		32			15			SMTemp6 Temp2 High 2			SMTemp6 T2 Hi2		SMTemp6 T2 Alta 2			SMTemp6 T2Alta2
2176		32			15			SMTemp6 Temp2 High 1			SMTemp6 T2 Hi1		SMTemp6 T2 Alta 1			SMTemp6 T2Alta1
2177		32			15			SMTemp6 Temp2 Low			SMTemp6 T2 Low		SMTemp6 T2 Bassa			SMTemp6 T2Bassa
2178		32			15			SMTemp6 Temp3 High 2			SMTemp6 T3 Hi2		SMTemp6 T3 Alta 2			SMTemp6 T3Alta2
2179		32			15			SMTemp6 Temp3 High 1			SMTemp6 T3 Hi1		SMTemp6 T3 Alta 1			SMTemp6 T3Alta1
2180		32			15			SMTemp6 Temp3 Low			SMTemp6 T3 Low		SMTemp6 T3 Bassa			SMTemp6 T3Bassa
2181		32			15			SMTemp6 Temp4 High 2			SMTemp6 T4 Hi2		SMTemp6 T4 Alta 2			SMTemp6 T4Alta2
2182		32			15			SMTemp6 Temp4 High 1			SMTemp6 T4 Hi1		SMTemp6 T4 Alta 1			SMTemp6 T4Alta1
2183		32			15			SMTemp6 Temp4 Low			SMTemp6 T4 Low		SMTemp6 T4 Bassa			SMTemp6 T4Bassa
2184		32			15			SMTemp6 Temp5 High 2			SMTemp6 T5 Hi2		SMTemp6 T5 Alta 2			SMTemp6 T5Alta2
2185		32			15			SMTemp6 Temp5 High 1			SMTemp6 T5 Hi1		SMTemp6 T5 Alta 1			SMTemp6 T5Alta1
2186		32			15			SMTemp6 Temp5 Low			SMTemp6 T5 Low		SMTemp6 T5 Bassa			SMTemp6 T5Bassa
2187		32			15			SMTemp6 Temp6 High 2			SMTemp6 T6 Hi2		SMTemp6 T6 Alta 2			SMTemp6 T6Alta2
2188		32			15			SMTemp6 Temp6 High 1			SMTemp6 T6 Hi1		SMTemp6 T6 Alta 1			SMTemp6 T6Alta1
2189		32			15			SMTemp6 Temp6 Low			SMTemp6 T6 Low		SMTemp6 T6 Bassa			SMTemp6 T6Bassa
2190		32			15			SMTemp6 Temp7 High 2			SMTemp6 T7 Hi2		SMTemp6 T7 Alta 2			SMTemp6 T7Alta2
2191		32			15			SMTemp6 Temp7 High 1			SMTemp6 T7 Hi1		SMTemp6 T7 Alta 1			SMTemp6 T7Alta1
2192		32			15			SMTemp6 Temp7 Low			SMTemp6 T7 Low		SMTemp6 T7 Bassa			SMTemp6 T7Bassa
2193		32			15			SMTemp6 Temp8 High 2			SMTemp6 T8 Hi2		SMTemp6 T8 Alta 2			SMTemp6 T8Alta2
2194		32			15			SMTemp6 Temp8 High 1			SMTemp6 T8 Hi1		SMTemp6 T8 Alta 1			SMTemp6 T8Alta1
2195		32			15			SMTemp6 Temp8 Low			SMTemp6 T8 Low		SMTemp6 T8 Bassa			SMTemp6 T8Bassa
2196		32			15			SMTemp7 Temp1 High 2			SMTemp7 T1 Hi2		SMTemp7 T1 Alta 2			SMTemp7 T1Alta2
2197		32			15			SMTemp7 Temp1 High 1			SMTemp7 T1 Hi1		SMTemp7 T1 Alta 1			SMTemp7 T1Alta1
2198		32			15			SMTemp7 Temp1 Low			SMTemp7 T1 Low		SMTemp7 T1 Bassa			SMTemp7 T1Bassa
2199		32			15			SMTemp7 Temp2 High 2			SMTemp7 T2 Hi2		SMTemp7 T2 Alta 2			SMTemp7 T2Alta2
2200		32			15			SMTemp7 Temp2 High 1			SMTemp7 T2 Hi1		SMTemp7 T2 Alta 1			SMTemp7 T2Alta1
2201		32			15			SMTemp7 Temp2 Low			SMTemp7 T2 Low		SMTemp7 T2 Bassa			SMTemp7 T2Bassa
2202		32			15			SMTemp7 Temp3 High 2			SMTemp7 T3 Hi2		SMTemp7 T3 Alta 2			SMTemp7 T3Alta2
2203		32			15			SMTemp7 Temp3 High 1			SMTemp7 T3 Hi1		SMTemp7 T3 Alta 1			SMTemp7 T3Alta1
2204		32			15			SMTemp7 Temp3 Low			SMTemp7 T3 Low		SMTemp7 T3 Bassa			SMTemp7 T3Bassa
2205		32			15			SMTemp7 Temp4 High 2			SMTemp7 T4 Hi2		SMTemp7 T4 Alta 2			SMTemp7 T4Alta2
2206		32			15			SMTemp7 Temp4 High 1			SMTemp7 T4 Hi1		SMTemp7 T4 Alta 1			SMTemp7 T4Alta1
2207		32			15			SMTemp7 Temp4 Low			SMTemp7 T4 Low		SMTemp7 T4 Bassa			SMTemp7 T4Bassa
2208		32			15			SMTemp7 Temp5 High 2			SMTemp7 T5 Hi2		SMTemp7 T5 Alta 2			SMTemp7 T5Alta2
2209		32			15			SMTemp7 Temp5 High 1			SMTemp7 T5 Hi1		SMTemp7 T5 Alta 1			SMTemp7 T5Alta1
2210		32			15			SMTemp7 Temp5 Low			SMTemp7 T5 Low		SMTemp7 T5 Bassa			SMTemp7 T5Bassa
2211		32			15			SMTemp7 Temp6 High 2			SMTemp7 T6 Hi2		SMTemp7 T6 Alta 2			SMTemp7 T6Alta2
2212		32			15			SMTemp7 Temp6 High 1			SMTemp7 T6 Hi1		SMTemp7 T6 Alta 1			SMTemp7 T6Alta1
2213		32			15			SMTemp7 Temp6 Low			SMTemp7 T6 Low		SMTemp7 T6 Bassa			SMTemp7 T6Bassa
2214		32			15			SMTemp7 Temp7 High 2			SMTemp7 T7 Hi2		SMTemp7 T7 Alta 2			SMTemp7 T7Alta2
2215		32			15			SMTemp7 Temp7 High 1			SMTemp7 T7 Hi1		SMTemp7 T7 Alta 1			SMTemp7 T7Alta1
2216		32			15			SMTemp7 Temp7 Low			SMTemp7 T7 Low		SMTemp7 T7 Bassa			SMTemp7 T7Bassa
2217		32			15			SMTemp7 Temp8 High 2			SMTemp7 T8 Hi2		SMTemp7 T8 Alta 2			SMTemp7 T8Alta2
2218		32			15			SMTemp7 Temp8 High 1			SMTemp7 T8 Hi1		SMTemp7 T8 Alta 1			SMTemp7 T8Alta1
2219		32			15			SMTemp7 Temp8 Low			SMTemp7 T8 Low		SMTemp7 T8 Bassa			SMTemp7 T8Bassa
2220		32			15			SMTemp8 Temp1 High 2			SMTemp8 T1 Hi2		SMTemp8 T1 Alta 2			SMTemp8 T1Alta2
2221		32			15			SMTemp8 Temp1 High 1			SMTemp8 T1 Hi1		SMTemp8 T1 Alta 1			SMTemp8 T1Alta1
2222		32			15			SMTemp8 Temp1 Low			SMTemp8 T1 Low		SMTemp8 T1 Bassa			SMTemp8 T1Bassa
2223		32			15			SMTemp8 Temp2 High 2			SMTemp8 T2 Hi2		SMTemp8 T2 Alta 2			SMTemp8 T2Alta2
2224		32			15			SMTemp8 Temp2 High 1			SMTemp8 T2 Hi1		SMTemp8 T2 Alta 1			SMTemp8 T2Alta1
2225		32			15			SMTemp8 Temp2 Low			SMTemp8 T2 Low		SMTemp8 T2 Bassa			SMTemp8 T2Bassa
2226		32			15			SMTemp8 Temp3 High 2			SMTemp8 T3 Hi2		SMTemp8 T3 Alta 2			SMTemp8 T3Alta2
2227		32			15			SMTemp8 Temp3 High 1			SMTemp8 T3 Hi1		SMTemp8 T3 Alta 1			SMTemp8 T3Alta1
2228		32			15			SMTemp8 Temp3 Low			SMTemp8 T3 Low		SMTemp8 T3 Bassa			SMTemp8 T3Bassa
2229		32			15			SMTemp8 Temp4 High 2			SMTemp8 T4 Hi2		SMTemp8 T4 Alta 2			SMTemp8 T4Alta2
2230		32			15			SMTemp8 Temp4 High 1			SMTemp8 T4 Hi1		SMTemp8 T4 Alta 1			SMTemp8 T4Alta1
2231		32			15			SMTemp8 Temp4 Low			SMTemp8 T4 Low		SMTemp8 T4 Bassa			SMTemp8 T4Bassa
2232		32			15			SMTemp8 Temp5 High 2			SMTemp8 T5 Hi2		SMTemp8 T5 Alta 2			SMTemp8 T5Alta2
2233		32			15			SMTemp8 Temp5 High 1			SMTemp8 T5 Hi1		SMTemp8 T5 Alta 1			SMTemp8 T5Alta1
2234		32			15			SMTemp8 Temp5 Low			SMTemp8 T5 Low		SMTemp8 T5 Bassa			SMTemp8 T5Bassa
2235		32			15			SMTemp8 Temp6 High 2			SMTemp8 T6 Hi2		SMTemp8 T6 Alta 2			SMTemp8 T6Alta2
2236		32			15			SMTemp8 Temp6 High 1			SMTemp8 T6 Hi1		SMTemp8 T6 Alta 1			SMTemp8 T6Alta1
2237		32			15			SMTemp8 Temp6 Low			SMTemp8 T6 Low		SMTemp8 T6 Bassa			SMTemp8 T6Bassa
2238		32			15			SMTemp8 Temp7 High 2			SMTemp8 T7 Hi2		SMTemp8 T7 Alta 2			SMTemp8 T7Alta2
2239		32			15			SMTemp8 Temp7 High 1			SMTemp8 T7 Hi1		SMTemp8 T7 Alta 1			SMTemp8 T7Alta1
2240		32			15			SMTemp8 Temp7 Low			SMTemp8 T7 Low		SMTemp8 T7 Bassa			SMTemp8 T7Bassa
2241		32			15			SMTemp8 Temp8 High 2			SMTemp8 T8 Hi2		SMTemp8 T8 Alta 2			SMTemp8 T8Alta2
2242		32			15			SMTemp8 Temp8 High 1			SMTemp8 T8 Hi1		SMTemp8 T8 Alta 1			SMTemp8 T8Alta1
2243		32			15			SMTemp8 Temp8 Low			SMTemp8 T8 Low		SMTemp8 T8 Bassa			SMTemp8 T8Bassa
2244		32			15			None					None			Nessuno					Nessuno
2245		32			15			High Load Level1			HighLoadLevel1		Alto Carico Livello1			AltoCaricoLiv1
2246		32			15			High Load Level2			HighLoadLevel2		Alto Carico Livello2			AltoCaricoLiv2
2247		32			15			Maximum					Maximum			Massimo					Massimo
2248		32			15			Average					Average			Medio					Medio
2249		32			15			Solar Mode				Solar Mode		Modo Solare				Modo Solare
2250		32			15			Running Way(For Solar)			Running Way		In Corso (per solare)			In Corso
2251		32			15			Disable					Disable			Disabilita				Disabilita
2252		32			15			RECT-SOLAR				RECT-SOLAR		RD-SOLARE				RD-SOLARE
2253		32			15			SOLAR					SOLAR			SOLARE					SOLARE
2254		32			15			RECT First				RECT First		Primo RD				Primo RD
2255		32			15			SOLAR First				SOLAR First		Primo SOLARE				Primo SOLARE
2256		32			15			Please reboot after changing		Please reboot		Riavvio dopo cambiamenti		Riavvio
2257		32			15			CSU Failure				CSU Failure		CSU Guasta				CSU Guasta
2258		32			15			SSL and SNMPV3				SSL and SNMPV3		SSL e SNMPV3				SSL e SNMPV3
2259		32			15			Adjust Bus Input Voltage		Adjust In-Volt		Regolazione Tensione Ingresso Bus	Regolaz Tens IN
2260		32			15			Confirm Voltage Supplied		Confirm Voltage		Conferma Tensione Applicata		Conferma Tens
2261		32			15			Converter Only				Converter Only		Solo Convertitore			Solo Convertitore
2262		32			15			Net-Port Reset Interval			Reset Interval		Reset Intervallo Net-Port		Reset Intervallo
2263		32			15			DC1 Load				DC1 Load		Carico DC1				Carico DC1
2264		32			15			DI9					DI9			DI9					DI9
2265		32			15			DI10					DI10			DI10					DI10
2266		32			15			DI11					DI11			DI11					DI11
2267		32			15			DI12					DI12			DI12					DI12
2268		32			15			Digital Input 9				DI 9			Ingresso Digitale 9			DI 9
2269		32			15			Digital Input 10			DI 10			Ingresso Digitale 10			DI 10
2270		32			15			Digital Input 11			DI 11			Ingresso Digitale 11			DI 11
2271		32			15			Digital Input 12			DI 12			Ingresso Digitale 12			DI 12
2272		32			15			DI9 Alarm State				DI9 Alm State		Stato Allarme DI9			Stato All DI9
2273		32			15			DI10 Alarm State			DI10 Alm State		Stato Allarme DI10			Stato All DI10
2274		32			15			DI11 Alarm State			DI11 Alm State		Stato Allarme DI11			Stato All DI11
2275		32			15			DI12 Alarm State			DI12 Alm State		Stato Allarme DI12			Stato All DI12
2276		32			15			DI9 Alarm				DI9 Alarm		Allarme DI9				Allarme DI9
2277		32			15			DI10 Alarm				DI10 Alarm		Allarme DI9				Allarme DI9
2278		32			15			DI11 Alarm				DI11 Alarm		Allarme DI9				Allarme DI9
2279		32			15			DI12 Alarm				DI12 Alarm		Allarme DI9				Allarme DI9
2280		32			15			None					None			Nessuno					Nessuno
2281		32			15			Exist					Exist			Esiste					Esiste
2282		32			15			IB01 State				IB01 State		Stato IB01				Stato IB01
2283		32			15			Relay Output 14				Relay Output 14		Relè uscita 14				Relè usc14
2284		32			15			Relay Output 15				Relay Output 15		Relè uscita 15				Relè usc15
2285		32			15			Relay Output 16				Relay Output 16		Relè uscita 16				Relè usc16
2286		32			15			Relay Output 17				Relay Output 17		Relè uscita 17				Relè usc17
2287		32			15			Time Display Format			Time Format		Formato Ora Display			Formato Ora
2288		32			15			EMEA					EMEA			EMEA					EMEA
2289		32			15			NA					NA			NA					NA
2290		32			15			CN					CN			CN					CN
2291		32			15			Help					Help			Aiuto					Aiuto
2292		32			15			Current Protocol Type			Protocol		Tipo Protocollo Corrente		Protocollo
2293		32			15			EEM					EEM			EEM					EEM
2294		32			15			YDN23					YDN23			YDN23					YDN23
2295		32			15			Modbus					ModBus			ModBus					ModBus
2296		32			15			SMS Alarm Level				SMS Alarm Level		Livello Allarme SMS			Liv All SMS
2297		32			15			Disabled				Disabled		Disabilitato				Disabilitato
2298		32			15			Observation				Observation		Osservazione				Osservazione
2299		32			15			Major					Major			Grave					Grave
2300		32			15			Critical				Critical		Critico					Critico
2301		64			64			SPD is not connected to system or SPD is broken!	SPD is not connected to system or SPD is broken!	SPD non è connesso al sistema o è guasto	SPD Non conn o Guasto
2302		32			15			Big Screen				Big Screen		Schermo Grande				Schermo Grande
2303		32			15			EMAIL Alarm Level			EMAIL Alarm Level	Livello Allarme EMAIL			Live Alm EMAIL
2304		32			15			HLMS Protocol Type			Protocol Type		Tipo Protocollo HLMS			Tipo Protocollo
2305		32			15			EEM					EEM			EEM					EEM
2306		32			15			YDN23					YDN23			YDN23					YDN23
2307		32			15			Modbus					Modbus			Modbus					Modbus
2308		32			15			24V Display State			24V Display St		Stato Display 24V			Display 24V
2309		32			15			Syst Volt Level				Syst Volt Level		Livello Tensione Sistema		LivTensSistema
2310		32			15			48 V System				48 V System		Sistema 48V				Sistema 48V
2311		32			15			24 V System				24 V System		Sistema 24V				Sistema 24V
2312		32			15			Power Input Type			Power Input Type	Tipo Potenza Ingresso			Tipo Potenza Ingresso
2313		32			15			AC Input				AC Input		Ingresso CA				Ingresso CA
2314		32			15			Diesel Input				Diesel Input		Ingresso GE				Ingresso GE
2315		32			15			2nd IB2 Temp1				2nd IB2 Temp1		2nd Temp1 Secondo IB2			Temp1 SecoIB2
2316		32			15			2nd IB2 Temp1				2nd IB2 Temp1		2nd Temp2 Secondo IB2			Temp2 SecoIB2
2317		32			15			EIB2 Temp1				EIB2 Temp1		Temp1 EIB2				Temp1 EIB2
2318		32			15			EIB2 Temp2				EIB2 Temp2		Temp2 EIB2				Temp2 EIB2
2319		32			15			2nd IB2 Temp1 High 2			2nd IB2 T1 Hi2		Temp1 Alta 2 Secondo IB2		2nd IB2 T1 Alta2
2320		32			15			2nd IB2 Temp1 High 1			2nd IB2 T1 Hi1		Temp1 Alta 1 Secondo IB2		2nd IB2 T1 Alta1
2321		32			15			2nd IB2 Temp1 Low			2nd IB2 T1 Low		Temp1 Bassa Secondo IB2			2nd IB2 T1 Bassa
2322		32			15			2nd IB2 Temp2 High 2			2nd IB2 T2 Hi2		Temp2 Alta 2 Secondo IB2		2nd IB2 T2 Alta2
2323		32			15			2nd IB2 Temp2 High 1			2nd IB2 T2 Hi1		Temp2 Alta 1 Secondo IB2		2nd IB2 T2 Alta1
2324		32			15			2nd IB2 Temp2 Low			2nd IB2 T2 Low		Temp2 Bassa Secondo IB2			2nd IB2 T2 Bassa
2325		32			15			EIB2 Temp1 High 2			EIB2 T1 Hi2		Temp1 Alta 2 EIB2			EIB2 EIB2 T1 Alta2
2326		32			15			EIB2 Temp1 High 1			EIB2 T1 Hi1		Temp1 Alta 1 EIB2			EIB2 EIB2 T1 Alta1
2327		32			15			EIB2 Temp1 Low				EIB2 T1 Low		Temp1 bassa EIB2			EIB2 T1 Bassa
2328		32			15			EIB2 Temp2 High 2			EIB2 T2 Hi2		Temp2 Alta 2 EIB2			EIB2 EIB2 T2 Alta2
2329		32			15			EIB2 Temp2 High 1			EIB2 T2 Hi1		Temp2 Alta 1 EIB2			EIB2 EIB2 T2 Alta1
2330		32			15			EIB2 Temp2 Low				EIB2 T2 Low		Temp2 bassa EIB2			EIB2 T2 Bassa
2331		32			15			Testing Relay 14			Testing Relay14		Relè14 in prova				Relè14 in prova
2332		32			15			Testing Relay 15			Testing Relay15		Relè15 in prova				Relè15 in prova
2333		32			15			Testing Relay 16			Testing Relay16		Relè16 in prova				Relè16 in prova
2334		32			15			Testing Relay 17			Testing Relay17		Relè17 in prova				Relè17 in prova
2335		32			15			Total Output Rated			Total Output Rated	Uscita Nominale Totale			Uscita Nom Tot
2336		32			15			Load current capacity			Load capacity		Capacità Corrente Carico		Capacità Carco
2337		32			15			EES System Mode				EES System Mode		EES Modo Sistema			EES Modo Sistema
2338		32			15			SMS Modem Fail				SMS Modem Fail		SMS Modem Guasto			SMS Modem Gsto
2339		32			15			SMDU-EIB Mode				SMDU-EIB Mode		Modo SMDU-EIB				Modo SMDU-EIB
2340		32			15			Load Switch				Load Switch		Interruttore di carico			Interr carico
2341		32			15			Manual State				Manual State		Modo manuale				Modo manuale
2342		32			15			Converter Voltage Level			Volt Level		Livello Tensione Convertitore		Liv Tens Conv
2343		32			15			CB Threshold Value			Threshold Value		Valore Soglia CB			Val Soglia CB
2344		32			15			CB Overload Value			Overload Value		Valore Sovraccarico CB			Val Sovracc CB
2345		32			15			SNMP Config Error			SNMP Config Err		SNMP Config Error			SNMP Config Err
2346		32			15			Cab X Num(Valid After Reset)		Cabinet X Num		Cab X Num(Valid After Reset)		Cabinet X Num
2347		32			15			Cab Y Num(Valid After Reset)		Cabinet Y Num		Cab Y Num(Valid After Reset)		Cabinet Y Num
2348		32			15			CB Threshold Power			Threshold Power		CB Threshold Power			Threshold Power
2349		32			15			CB Overload Power			Overload Power		CB Overload Power			Overload Power
2350		32			15			With FCUP				With FCUP		With FCUP				With FCUP
2351		32			15			FCUP Existence State			FCUP Exist State	FCUP Existence State			FCUP Exist State
2356		32			15			DHCP Enable				DHCP Enable		DHCP Abilitato					DHCP Abilitato
2357		32			15			DO1 Normal State  			DO1 Normal		DO1 Stato Normale				DO1 Normale
2358		32			15			DO2 Normal State  			DO2 Normal		DO2 Stato Normale				DO2 Normale
2359		32			15			DO3 Normal State  			DO3 Normal		DO3 Stato Normale				DO3 Normale
2360		32			15			DO4 Normal State  			DO4 Normal		DO4 Stato Normale				DO4 Normale
2361		32			15			DO5 Normal State  			DO5 Normal		DO5 Stato Normale				DO5 Normale
2362		32			15			DO6 Normal State  			DO6 Normal		DO6 Stato Normale				DO6 Normale
2363		32			15			DO7 Normal State  			DO7 Normal		DO7 Stato Normale				DO7 Normale
2364		32			15			DO8 Normal State  			DO8 Normal		DO8 Stato Normale				DO8 Normale
2365		32			15			Non-Energized				Non-Energized		Non-Energizzato			Non-Energizzato
2366		32			15			Energized				Energized		Energizzato					Energizzato
2367		32			15			DO14 Normal State  			DO14 Normal		DO14 Stato Normale				DO14 Normale
2368		32			15			DO15 Normal State  			DO15 Normal		DO15 Stato Normale				DO15 Normale
2369		32			15			DO16 Normal State  			DO16 Normal		DO16 Stato Normale				DO16 Normale
2370		32			15			DO17 Normal State  			DO17 Normal		DO17 Stato Normale				DO17 Normale
2371		32			15			SSH Enabled  				SSH Enabled		SSH permettere					SSH permettere
2372		32			15			Disabled				Disabled		non abilitato					non abilitato		
2373		32			15			Enabled					Enabled			permettere					permettere
2374		32			15			Page Type For Login			Page Type		Page Type For Login			Page Type
2375		32			15			Standard				Standard		Standard				Standard
2376		32			15			For BT					For BT			For BT					For BT	
2377		32		15		Fiamm Battery		Fiamm Battery		Fiamm Batteria	Fiamm Batteria
2378		32			15			Correct Temperature1			Correct Temp1			Correct Temperature1			Correct Temp1
2379		32			15			Correct Temperature2			Correct Temp2			Correct Temperature2			Correct Temp2

2503		32			15			NTP Function Enable			NTPFuncEnable		NTP Enable				NTP Enable
2504		32			15			SW_Switch1			SW_Switch1			CommutateurSW1				CommutateurSW1	
2505		32			15			SW_Switch2			SW_Switch2			CommutateurSW2				CommutateurSW2	
2506		32			15			SW_Switch3			SW_Switch3			CommutateurSW3				CommutateurSW3	
2507		32			15			SW_Switch4			SW_Switch4			CommutateurSW4				CommutateurSW4	
2508		32			15			SW_Switch5			SW_Switch5			CommutateurSW5				CommutateurSW5	
2509		32			15			SW_Switch6			SW_Switch6			CommutateurSW6				CommutateurSW6	
2510		32			15			SW_Switch7			SW_Switch7			CommutateurSW7				CommutateurSW7	
2511		32			15			SW_Switch8			SW_Switch8			CommutateurSW8				CommutateurSW8	
2512		32			15			SW_Switch9			SW_Switch9			CommutateurSW9				CommutateurSW9	
2513		32			15			SW_Switch10			SW_Switch10			CommutateurSW10				CommutateurSW10	
