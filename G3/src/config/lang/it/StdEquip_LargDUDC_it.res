﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Temperature 1				Temp 1			Temperatura 1				Temp 1
2		32			15			Temperature 2				Temp 2			Temperatura 2				Temp 2
3		32			15			Temperature 3				Temp 3			Temperatura 3				Temp 3
4		32			15			DC Voltage				DC Voltage		Tensione CC				Tensione CC
5		32			15			Load Current				Load Current		Corrente di carico			Corrente
6		32			15			Branch 1 Current			Branch 1 Curr		Corrente ramo 1				Corrente ramo1
7		32			15			Branch 2 Current			Branch 2 Curr		Corrente ramo 2				Corrente ramo2
8		32			15			Branch 3 Current			Branch 3 Curr		Corrente ramo 3				Corrente ramo3
9		32			15			Branch 4 Current			Branch 4 Curr		Corrente ramo 4				Corrente ramo4
10		32			15			Branch 5 Current			Branch 5 Curr		Corrente ramo 5				Corrente ramo5
11		32			15			Branch 6 Current			Branch 6 Curr		Corrente ramo 6				Corrente ramo6
12		32			15			DC Overvoltage				DC Overvolt		Sovratensione CC			Sovratensione CC
13		32			15			DC Undervoltage				DC Undervolt		Sottotensione CC			Sottotensione CC
14		32			15			High Temperature 1			High Temp 1		Alta temperatura 1			Alta temp 1
15		32			15			High Temperature 2			High Temp 2		Alta temperatura 2			Alta temp 2
16		32			15			High Temperature 3			High Temp 3		Alta temperatura 3			Alta temp 3
17		32			15			DC Output 1 Disconnected		Output 1 Discon		Uscita 1 CC disconnessa			Uscita 1 disc
18		32			15			DC Output 2 Disconnected		Output 2 Discon		Uscita 2 CC disconnessa			Uscita 2 disc
19		32			15			DC Output 3 Disconnected		Output 3 Discon		Uscita 3 CC disconnessa			Uscita 3 disc
20		32			15			DC Output 4 Disconnected		Output 4 Discon		Uscita 4 CC disconnessa			Uscita 4 disc
21		32			15			DC Output 5 Disconnected		Output 5 Discon		Uscita 5 CC disconnessa			Uscita 5 disc
22		32			15			DC Output 6 Disconnected		Output 6 Discon		Uscita 6 CC disconnessa			Uscita 6 disc
23		32			15			DC Output 7 Disconnected		Output 7 Discon		Uscita 7 CC disconnessa			Uscita 7 disc
24		32			15			DC Output 8 Disconnected		Output 8 Discon		Uscita 8 CC disconnessa			Uscita 8 disc
25		32			15			DC Output 9 Disconnected		Output 9 Discon		Uscita 9 CC disconnessa			Uscita 9 disc
26		32			15			DC Output 10 Disconnected		Output10 Discon		Uscita 10 CC disconnessa		Uscita 10 disc
27		32			15			DC Output 11 Disconnected		Output11 Discon		Uscita 11 CC disconnessa		Uscita 11 disc
28		32			15			DC Output 12 Disconnected		Output12 Discon		Uscita 12 CC disconnessa		Uscita 12 disc
29		32			15			DC Output 13 Disconnected		Output13 Discon		Uscita 13 CC disconnessa		Uscita 13 disc
30		32			15			DC Output 14 Disconnected		Output14 Discon		Uscita 14 CC disconnessa		Uscita 14 disc
31		32			15			DC Output 15 Disconnected		Output15 Discon		Uscita 15 CC disconnessa		Uscita 15 disc
32		32			15			DC Output 16 Disconnected		Output16 Discon		Uscita 16 CC disconnessa		Uscita 16 disc
33		32			15			DC Output 17 Disconnected		Output17 Discon		Uscita 17 CC disconnessa		Uscita 17 disc
34		32			15			DC Output 18 Disconnected		Output18 Discon		Uscita 18 CC disconnessa		Uscita 18 disc
35		32			15			DC Output 19 Disconnected		Output19 Discon		Uscita 19 CC disconnessa		Uscita 19 disc
36		32			15			DC Output 20 Disconnected		Output20 Discon		Uscita 20 CC disconnessa		Uscita 20 disc
37		32			15			DC Output 21 Disconnected		Output21 Discon		Uscita 21 CC disconnessa		Uscita 21 disc
38		32			15			DC Output 22 Disconnected		Output22 Discon		Uscita 22 CC disconnessa		Uscita 22 disc
39		32			15			DC Output 23 Disconnected		Output23 Discon		Uscita 23 CC disconnessa		Uscita 23 disc
40		32			15			DC Output 24 Disconnected		Output24 Discon		Uscita 24 CC disconnessa		Uscita 24 disc
41		32			15			DC Output 25 Disconnected		Output25 Discon		Uscita 25 CC disconnessa		Uscita 25 disc
42		32			15			DC Output 26 Disconnected		Output26 Discon		Uscita 26 CC disconnessa		Uscita 26 disc
43		32			15			DC Output 27 Disconnected		Output27 Discon		Uscita 27 CC disconnessa		Uscita 27 disc
44		32			15			DC Output 28 Disconnected		Output28 Discon		Uscita 28 CC disconnessa		Uscita 28 disc
45		32			15			DC Output 29 Disconnected		Output29 Discon		Uscita 29 CC disconnessa		Uscita 29 disc
46		32			15			DC Output 30 Disconnected		Output30 Discon		Uscita 30 CC disconnessa		Uscita 30 disc
47		32			15			DC Output 31 Disconnected		Output31 Discon		Uscita 31 CC disconnessa		Uscita 31 disc
48		32			15			DC Output 32 Disconnected		Output32 Discon		Uscita 32 CC disconnessa		Uscita 32 disc
49		32			15			DC Output 33 Disconnected		Output33 Discon		Uscita 33 CC disconnessa		Uscita 33 disc
50		32			15			DC Output 34 Disconnected		Output34 Discon		Uscita 34 CC disconnessa		Uscita 34 disc
51		32			15			DC Output 35 Disconnected		Output35 Discon		Uscita 35 CC disconnessa		Uscita 35 disc
52		32			15			DC Output 36 Disconnected		Output36 Discon		Uscita 36 CC disconnessa		Uscita 36 disc
53		32			15			DC Output 37 Disconnected		Output37 Discon		Uscita 37 CC disconnessa		Uscita 37 disc
54		32			15			DC Output 38 Disconnected		Output38 Discon		Uscita 38 CC disconnessa		Uscita 38 disc
55		32			15			DC Output 39 Disconnected		Output39 Discon		Uscita 39 CC disconnessa		Uscita 39 disc
56		32			15			DC Output 40 Disconnected		Output40 Discon		Uscita 40 CC disconnessa		Uscita 40 disc
57		32			15			DC Output 41 Disconnected		Output41 Discon		Uscita 41 CC disconnessa		Uscita 41 disc
58		32			15			DC Output 42 Disconnected		Output42 Discon		Uscita 42 CC disconnessa		Uscita 42 disc
59		32			15			DC Output 43 Disconnected		Output43 Discon		Uscita 43 CC disconnessa		Uscita 43 disc
60		32			15			DC Output 44 Disconnected		Output44 Discon		Uscita 44 CC disconnessa		Uscita 44 disc
61		32			15			DC Output 45 Disconnected		Output45 Discon		Uscita 45 CC disconnessa		Uscita 45 disc
62		32			15			DC Output 46 Disconnected		Output46 Discon		Uscita 46 CC disconnessa		Uscita 46 disc
63		32			15			DC Output 47 Disconnected		Output47 Discon		Uscita 47 CC disconnessa		Uscita 47 disc
64		32			15			DC Output 48 Disconnected		Output48 Discon		Uscita 48 CC disconnessa		Uscita 48 disc
65		32			15			DC Output 49 Disconnected		Output49 Discon		Uscita 49 CC disconnessa		Uscita 49 disc
66		32			15			DC Output 50 Disconnected		Output50 Discon		Uscita 50 CC disconnessa		Uscita 50 disc
67		32			15			DC Output 51 Disconnected		Output51 Discon		Uscita 51 CC disconnessa		Uscita 51 disc
68		32			15			DC Output 52 Disconnected		Output52 Discon		Uscita 52 CC disconnessa		Uscita 52 disc
69		32			15			DC Output 53 Disconnected		Output53 Discon		Uscita 53 CC disconnessa		Uscita 53 disc
70		32			15			DC Output 54 Disconnected		Output54 Discon		Uscita 54 CC disconnessa		Uscita 54 disc
71		32			15			DC Output 55 Disconnected		Output55 Discon		Uscita 55 CC disconnessa		Uscita 55 disc
72		32			15			DC Output 56 Disconnected		Output56 Discon		Uscita 56 CC disconnessa		Uscita 56 disc
73		32			15			DC Output 57 Disconnected		Output57 Discon		Uscita 57 CC disconnessa		Uscita 57 disc
74		32			15			DC Output 58 Disconnected		Output58 Discon		Uscita 58 CC disconnessa		Uscita 58 disc
75		32			15			DC Output 59 Disconnected		Output59 Discon		Uscita 59 CC disconnessa		Uscita 59 disc
76		32			15			DC Output 60 Disconnected		Output60 Discon		Uscita 60 CC disconnessa		Uscita 60 disc
77		32			15			DC Output 61 Disconnected		Output61 Discon		Uscita 61 CC disconnessa		Uscita 61 disc
78		32			15			DC Output 62 Disconnected		Output62 Discon		Uscita 62 CC disconnessa		Uscita 62 disc
79		32			15			DC Output 63 Disconnected		Output63 Discon		Uscita 63 CC disconnessa		Uscita 63 disc
80		32			15			DC Output 64 Disconnected		Output64 Discon		Uscita 64 CC disconnessa		Uscita 64 disc
81		32			15			LVD1 State				LVD1 State		Stato LVD1				Stato LVD1
82		32			15			LVD2 State				LVD2 State		Stato LVD2				Stato LVD2
83		32			15			LVD3 State				LVD3 State		Stato LVD3				Stato LVD3
84		32			15			Not Responding				Not Responding		Non risponde				Non risponde
85		32			15			LVD1					LVD1			LVD1					LVD1
86		32			15			LVD2					LVD2			LVD2					LVD2
87		32			15			LVD3					LVD3			LVD3					LVD3
88		32			15			High Temperature 1 Limit		High Temp1		Limite alta temperatura 1		Lim alta Temp1
89		32			15			High Temperature 2 Limit		High Temp2		Limite alta temperatura 2		Lim alta Temp2
90		32			15			High Temperature 3 Limit		High Temp3		Limite alta temperatura 3		Lim alta Temp3
91		32			15			LVD1 Limit				LVD1 Limit		Limite LVD1				Limite LVD1
92		32			15			LVD2 Limit				LVD2 Limit		Limite LVD2				Limite LVD2
93		32			15			LVD3 Limit				LVD3 Limit		Limite LVD3				Limite LVD3
94		32			15			Battery Overvoltage Level		Batt Overvolt		Livello sovratensione			Sovratensione
95		32			15			Battery Undervoltage Level		Batt Undervolt		Livello sottotensione			Sottotensione
96		32			15			Temperature Coefficient			Temp Coeff		Coeff temperatura			Fattore temp
97		32			15			Current Sensor Coefficient		Sensor Coef		Coefficiente sensore corrente		Coeff sensor I
98		32			15			Battery Number				Battery Num		Numero di batteria			Num batteria
99		32			15			Temperature Number			Temp Num		Numero di temperatura			Num temp
100		32			15			Branch Current Coefficient		Bran-Curr Coef		Coeff corrente ramo			Coeff I-Ramo
101		32			15			Distribution Address			Address			Indirizzo distribuzione			Indir distrib
102		32			15			Current Measurement Output Num		Curr-output Num		Num uscita misura corrente		Uscita mis corr
103		32			15			Output Number				Output Num		Numero di Uscita			Núm de Uscita
104		32			15			DC Overvoltage				DC Overvolt		Sovratensione CC			Sovratensione
105		32			15			DC Undervoltage				DC Undervolt		Sottotensione CC			Sottotensione
106		32			15			DC Output 1 Disconnected		Output1 Discon		Uscita 1 CC disconnessa			Uscita 1 disc
107		32			15			DC Output 2 Disconnected		Output2 Discon		Uscita 2 CC disconnessa			Uscita 2 disc
108		32			15			DC Output 3 Disconnected		Output3 Discon		Uscita 3 CC disconnessa			Uscita 3 disc
109		32			15			DC Output 4 Disconnected		Output4 Discon		Uscita 4 CC disconnessa			Uscita 4 disc
110		32			15			DC Output 5 Disconnected		Output5 Discon		Uscita 5 CC disconnessa			Uscita 5 disc
111		32			15			DC Output 6 Disconnected		Output6 Discon		Uscita 6 CC disconnessa			Uscita 6 disc
112		32			15			DC Output 7 Disconnected		Output7 Discon		Uscita 7 CC disconnessa			Uscita 7 disc
113		32			15			DC Output 8 Disconnected		Output8 Discon		Uscita 8 CC disconnessa			Uscita 8 disc
114		32			15			DC Output 9 Disconnected		Output9 Discon		Uscita 9 CC disconnessa			Uscita 9 disc
115		32			15			DC Output 10 Disconnected		Output10 Discon		Uscita 10 CC disconnessa		Uscita 10 disc
116		32			15			DC Output 11 Disconnected		Output11 Discon		Uscita 11 CC disconnessa		Uscita 11 disc
117		32			15			DC Output 12 Disconnected		Output12 Discon		Uscita 12 CC disconnessa		Uscita 12 disc
118		32			15			DC Output 13 Disconnected		Output13 Discon		Uscita 13 CC disconnessa		Uscita 13 disc
119		32			15			DC Output 14 Disconnected		Output14 Discon		Uscita 14 CC disconnessa		Uscita 14 disc
120		32			15			DC Output 15 Disconnected		Output15 Discon		Uscita 15 CC disconnessa		Uscita 15 disc
121		32			15			DC Output 16 Disconnected		Output16 Discon		Uscita 16 CC disconnessa		Uscita 16 disc
122		32			15			DC Output 17 Disconnected		Output17 Discon		Uscita 17 CC disconnessa		Uscita 17 disc
123		32			15			DC Output 18 Disconnected		Output18 Discon		Uscita 18 CC disconnessa		Uscita 18 disc
124		32			15			DC Output 19 Disconnected		Output19 Discon		Uscita 19 CC disconnessa		Uscita 19 disc
125		32			15			DC Output 20 Disconnected		Output20 Discon		Uscita 20 CC disconnessa		Uscita 20 disc
126		32			15			DC Output 21 Disconnected		Output21 Discon		Uscita 21 CC disconnessa		Uscita 21 disc
127		32			15			DC Output 22 Disconnected		Output22 Discon		Uscita 22 CC disconnessa		Uscita 22 disc
128		32			15			DC Output 23 Disconnected		Output23 Discon		Uscita 23 CC disconnessa		Uscita 23 disc
129		32			15			DC Output 24 Disconnected		Output24 Discon		Uscita 24 CC disconnessa		Uscita 24 disc
130		32			15			DC Output 25 Disconnected		Output25 Discon		Uscita 25 CC disconnessa		Uscita 25 disc
131		32			15			DC Output 26 Disconnected		Output26 Discon		Uscita 26 CC disconnessa		Uscita 26 disc
132		32			15			DC Output 27 Disconnected		Output27 Discon		Uscita 27 CC disconnessa		Uscita 27 disc
133		32			15			DC Output 28 Disconnected		Output28 Discon		Uscita 28 CC disconnessa		Uscita 28 disc
134		32			15			DC Output 29 Disconnected		Output29 Discon		Uscita 29 CC disconnessa		Uscita 29 disc
135		32			15			DC Output 30 Disconnected		Output30 Discon		Uscita 30 CC disconnessa		Uscita 30 disc
136		32			15			DC Output 31 Disconnected		Output31 Discon		Uscita 31 CC disconnessa		Uscita 31 disc
137		32			15			DC Output 32 Disconnected		Output32 Discon		Uscita 32 CC disconnessa		Uscita 32 disc
138		32			15			DC Output 33 Disconnected		Output33 Discon		Uscita 33 CC disconnessa		Uscita 33 disc
139		32			15			DC Output 34 Disconnected		Output34 Discon		Uscita 34 CC disconnessa		Uscita 34 disc
140		32			15			DC Output 35 Disconnected		Output35 Discon		Uscita 35 CC disconnessa		Uscita 35 disc
141		32			15			DC Output 36 Disconnected		Output36 Discon		Uscita 36 CC disconnessa		Uscita 36 disc
142		32			15			DC Output 37 Disconnected		Output37 Discon		Uscita 37 CC disconnessa		Uscita 37 disc
143		32			15			DC Output 38 Disconnected		Output38 Discon		Uscita 38 CC disconnessa		Uscita 38 disc
144		32			15			DC Output 39 Disconnected		Output39 Discon		Uscita 39 CC disconnessa		Uscita 39 disc
145		32			15			DC Output 40 Disconnected		Output40 Discon		Uscita 40 CC disconnessa		Uscita 40 disc
146		32			15			DC Output 41 Disconnected		Output41 Discon		Uscita 41 CC disconnessa		Uscita 41 disc
147		32			15			DC Output 42 Disconnected		Output42 Discon		Uscita 42 CC disconnessa		Uscita 42 disc
148		32			15			DC Output 43 Disconnected		Output43 Discon		Uscita 43 CC disconnessa		Uscita 43 disc
149		32			15			DC Output 44 Disconnected		Output44 Discon		Uscita 44 CC disconnessa		Uscita 44 disc
150		32			15			DC Output 45 Disconnected		Output45 Discon		Uscita 45 CC disconnessa		Uscita 45 disc
151		32			15			DC Output 46 Disconnected		Output46 Discon		Uscita 46 CC disconnessa		Uscita 46 disc
152		32			15			DC Output 47 Disconnected		Output47 Discon		Uscita 47 CC disconnessa		Uscita 47 disc
153		32			15			DC Output 48 Disconnected		Output48 Discon		Uscita 48 CC disconnessa		Uscita 48 disc
154		32			15			DC Output 49 Disconnected		Output49 Discon		Uscita 49 CC disconnessa		Uscita 49 disc
155		32			15			DC Output 50 Disconnected		Output50 Discon		Uscita 50 CC disconnessa		Uscita 50 disc
156		32			15			DC Output 51 Disconnected		Output51 Discon		Uscita 51 CC disconnessa		Uscita 51 disc
157		32			15			DC Output 52 Disconnected		Output52 Discon		Uscita 52 CC disconnessa		Uscita 52 disc
158		32			15			DC Output 53 Disconnected		Output53 Discon		Uscita 53 CC disconnessa		Uscita 53 disc
159		32			15			DC Output 54 Disconnected		Output54 Discon		Uscita 54 CC disconnessa		Uscita 54 disc
160		32			15			DC Output 55 Disconnected		Output55 Discon		Uscita 55 CC disconnessa		Uscita 55 disc
161		32			15			DC Output 56 Disconnected		Output56 Discon		Uscita 56 CC disconnessa		Uscita 56 disc
162		32			15			DC Output 57 Disconnected		Output57 Discon		Uscita 57 CC disconnessa		Uscita 57 disc
163		32			15			DC Output 58 Disconnected		Output58 Discon		Uscita 58 CC disconnessa		Uscita 58 disc
164		32			15			DC Output 59 Disconnected		Output59 Discon		Uscita 59 CC disconnessa		Uscita 59 disc
165		32			15			DC Output 60 Disconnected		Output60 Discon		Uscita 60 CC disconnessa		Uscita 60 disc
166		32			15			DC Output 61 Disconnected		Output61 Discon		Uscita 61 CC disconnessa		Uscita 61 disc
167		32			15			DC Output 62 Disconnected		Output62 Discon		Uscita 62 CC disconnessa		Uscita 62 disc
168		32			15			DC Output 63 Disconnected		Output63 Discon		Uscita 63 CC disconnessa		Uscita 63 disc
169		32			15			DC Output 64 Disconnected		Output64 Discon		Uscita 64 CC disconnessa		Uscita 64 disc
170		32			15			Not Responding				Not Responding		Non risponde				Non risponde
171		32			15			LVD1					LVD1			LVD1					LVD1
172		32			15			LVD2					LVD2			LVD2					LVD2
173		32			15			LVD3					LVD3			LVD3					LVD3
174		32			15			High Temperature 1			High Temp 1		Alta temperatura 1			Alta temp 1
175		32			15			High Temperature 2			High Temp 2		Alta temperatura 2			Alta temp 2
176		32			15			High Temperature 3			High Temp 3		Alta temperatura 3			Alta temp 3
177		32			15			Large DU DC Distribution		Large DC Dist		Distribuzione CC grande			Distrib grande
178		32			15			Low Temperature 1 Limit			Low Temp1		Limite bassa temperatura 1		Lim bassa temp1
179		32			15			Low Temperature 2 Limit			Low Temp2		Limite bassa temperatura 2		Lim bassa temp2
180		32			15			Low Temperature 3 Limit			Low Temp3		Limite bassa temperatura 3		Lim bassa temp3
181		32			15			Low Temperature 1			Low Temp 1		Bassa temperatura 1			Bassa temp 1
182		32			15			Low Temperature 2			Low Temp 2		Bassa temperatura 2			Bassa temp 2
183		32			15			Low Temperature 3			Low Temp 3		Bassa temperatura 3			Bassa temp 3
184		32			15			Temperature 1 Alarm			Temp1 Alarm		Allarme temperatura 1			All temp 1
185		32			15			Temperature 2 Alarm			Temp2 Alarm		Allarme temperatura 2			All temp 2
186		32			15			Temperature 3 Alarm			Temp3 Alarm		Allarme temperatura 3			All temp 3
187		32			15			Voltage Alarm				Voltage Alarm		Allarme di tensione			All tensione
188		32			15			No Alarm				No Alarm		Nessun allarme				Nessun allarme
189		32			15			High Temperature			High Temp		Alta temperatura			Alta temp
190		32			15			Low Temperature				Low Temp		Bassa temperatura			Bassa temp
191		32			15			No Alarm				No Alarm		Nessun allarme				Nessun allarme
192		32			15			High Temperature			High Temp		Alta temperatura			Alta temp
193		32			15			Low Temperature				Low Temp		Bassa temperatura			Bassa temp
194		32			15			No Alarm				No Alarm		Nessun allarme				Nessun allarme
195		32			15			High Temperature			High Temp		Alta temperatura			Alta temp
196		32			15			Low Temperature				Low Temp		Bassa temperatura			Bassa temp
197		32			15			No Alarm				No Alarm		Nessun allarme				Nessun allarme
198		32			15			Overvoltage				Overvolt		Sovratensione CC			Sovratensione
199		32			15			Undervoltage				Undervolt		Sottotensione CC			Sottotensione
200		32			15			Voltage Alarm				Voltage Alarm		Allarme di tensione			All tensione
201		32			15			DC Distribution Not Responding		Not Responding		Distribuzione non risponde		Non risponde
202		32			15			Normal					Normal			Normale					Normale
203		32			15			Failure					Failure			Guasto					Guasto
204		32			15			DC Distribution Not Responding		Not Responding		Distribuzione non risponde		Non risponde
205		32			15			On					On			Connesso				Connesso
206		32			15			Off					Off			Disconnesso				Disconnesso
207		32			15			On					On			Connesso				Connesso
208		32			15			Off					Off			Disconnesso				Disconnesso
209		32			15			On					On			Connesso				Connesso
210		32			15			Off					Off			Disconnesso				Disconnesso
211		32			15			Temperature 1 Sensor Fault		T1 Sensor Fault		Guasto sensore temperatura 1		Guasto sens 1
212		32			15			Temperature 2 Sensor Fault		T2 Sensor Fault		Guasto sensore temperatura 2		Guasto sens 2
213		32			15			Temperature 3 Sensor Fault		T3 Sensor Fault		Guasto sensore temperatura 3		Guasto sens 3
214		32			15			Connected				Connected		Connesso				Connesso
215		32			15			Disconnected				Disconnected		Disconnesso				Disconnesso
216		32			15			Connected				Connected		Connesso				Connesso
217		32			15			Disconnected				Disconnected		Disconnesso				Disconnesso
218		32			15			Connected				Connected		Connesso				Connesso
219		32			15			Disconnected				Disconnected		Disconnesso				Disconnesso
220		32			15			Normal					Normal			Normale					Normale
221		32			15			Not Responding				Not Responding		Non risponde				Non risponde
222		32			15			Normal					Normal			Normale					Normale
223		32			15			Alarm					Alarm			Allarme					Allarme
224		32			15			Branch 7 Current			Branch 7 Curr		Corrente ramo 7				Corrente ramo7
225		32			15			Branch 8 Current			Branch 8 Curr		Corrente ramo 8				Corrente ramo8
226		32			15			Branch 9 Current			Branch 9 Curr		Corrente ramo 9				Corrente ramo9
227		32			15			Existence State				Existence State		Stato attuale				Stato attuale
228		32			15			Existent				Existent		Esistente				Esistente
229		32			15			Non-Existent				Non-Existent		Inesistente				Inesistente
230		32			15			Number of LVDs				No of LVDs		Numero di LVD				Num LVD
231		32			15			0					0			0					0
232		32			15			1					1			1					1
233		32			15			2					2			2					2
234		32			15			3					3			3					3
235		32			15			Battery Shutdown Number			Batt Shutdo No		Num disconnessioni batteria		N discon bat
236		32			15			Rated Capacity				Rated Capacity		Capacità nominale			Cap nomin
