﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Solar Converter Group			Solar ConvGrp		Gruppo Convertitore Solare		GrpConvSolar
2		32			15			Total Current				Total Current		Corrente Totale				I Totale
3		32			15			Average Voltage				Average Voltage		Tensione Media				V Media
4		32			15			System Capacity Used			Sys Cap Used		Capacità Sistema Usata			CapSisUsata
5		32			15			Maximum Used Capacity			Max Cap Used		Massima Capacità Usata			MaxCapSis
6		32			15			Minimum Used Capacity			Min Cap Used		Minima Capacità Usata			MinCapSis
7		36			15			Total Solar Converters Communicating	Num Solar Convs		Comm Convertitore Solare		NumConvSolar
8		32			15			Valid Solar Converter			Valid SolarConv		Convertitore Solare Valido		ConvSolarValido
9		32			15			Number of Solar Converter		SolConv Num		Numero convertitori solari		Num ConvSolar
11		34			15			Solar Converter(s) Failure Status	SolarConv Fail		Convertitore Solare Guasto		GstoConvSolar
12		32			15			Solar Converter Current Limit		SolConvCurrLim		Limite Corr Convertitore Solare		ILimConvSolar
13		32			15			Solar Converter Trim			Solar Conv Trim		Trim Conv Solare			TrimConvSolar
14		32			15			DC On/Off Control			DC On/Off Ctrl		Controllo CC				CtrlCC
16		32			15			Solar Converter LED Control		SolaConv LEDCtrl	Controllo LED Conv Solare		CtrlLEDConvSolar
17		32			15			Fan Speed Control			Fan Speed Ctrl		Controllo Velocità Ventola		CtrlVentola
18		32			15			Rated Voltage				Rated Voltage		Tensione Nominale			Vnom
19		32			15			Rated Current				Rated Current		Corrente Nominale			Inom
20		32			15			High Voltage Shutdown Limit		HVSD Limit		Limite Spegnimento Alta Tensione	LimiteHVSD
21		32			15			Low Voltage Limit			Low Volt Limit		Limite bassa Tensione			Limite V
22		32			15			High Temperature Limit			High Temp Limit		Limite Alta Temperatura			LimiteAltaT
24		32			15			HVSD Restart Time			HVSD Restart T		Tempo Riavvio HVSD			TRiavvioHVSD
25		32			15			Walk-In Time				Walk-In Time		Tempo Inserimento			TInserimento
26		32			15			Walk-In					Walk-In			Inserimento				Inserimento
27		32			15			Minimum Redundancy			Min Redundancy		Ridondanza Minima			MinRidond
28		32			15			Maximum Redundancy			Max Redundancy		Ridondanza Massima			MaxRidond
29		32			15			Turn Off Delay				Turn Off Delay		Ritardo Spegnimento			RitardoSpegn
30		32			15			Cycle Period				Cycle Period		Periodo Ciclo				PeriodoCicl
31		32			15			Cycle Activation Time			Cyc Active Time		Tempo Attivazione Ciclo			TAvvioCiclo
33		32			15			Multiple Solar Converter Failure	MultSolConvFail		Guasto Multiplo Conv Solare		GstoMultConv
36		32			15			Normal					Normal			Normale					Normale
37		32			15			Failure					Failure			Guasto					Guasto
38		32			15			Switch Off All				Switch Off All		Tutti Spenti				Tutti spenti
39		32			15			Switch On All				Switch On All		Tutti Accesi				Tutti accesi
42		32			15			All Flashing				All Flashing		Tutti Lampeggiano			TuttiLampegg
43		32			15			Stop Flashing				Stop Flashing		Ferma Lampeggio				NoLampeggio
44		32			15			Full Speed				Full Speed		Velocità Massima			VelocMax
45		32			15			Automatic Speed				Auto Speed		Velocità Automatica			VelocAutom
46		32			32			Current Limit Control			Curr Limit Ctrl		Controllo Limit Corrente		CtrlLimitCorr
47		32			32			Full Capability Control			Full Cap Ctrl		Controllo Piena Capacità		CtrlCapacità
54		32			15			Disabled				Disabled		Disabilitato				Disabilitato
55		32			15			Enabled					Enabled			Abilitato				Abilitato
68		32			15			ECO Mode				ECO Mode		Modo ECO				ModoECO
73		32			15			No					No			No					No
74		32			15			Yes					Yes			Sì					Sì
77		32			15			Pre-CurrLimit Turn-On			Pre-Curr Limit		Limite Pre-Corrente			LimPre-Corr
78		32			15			Solar Converter Power Type		SolConv PowType		Potenza Conv Solare			PConvSolar
79		32			15			Double Supply				Double Supply		Doppia alimentazione			DoppiaAlim
80		32			15			Single Supply				Single Supply		Singola Alimentazione			SingolaAlim
81		32			15			Last Solar Converter Quantity		LastSolConvQty		Ultima Quant Conv Solare		UltQtàConv
83		32			15			Solar Converter Lost			Solar Conv Lost		Conv Solare Mancante			ConvMncnte
84		32			15			Clear Solar Converter Lost Alarm	ClearSolConvLost	Reset Conv Solare Mancante		ResetConvMncte
85		32			15			Clear					Clear			Reset					Reset
86		32			15			Confirm Solar Converter ID		Confirm ID		Conferma N Conv Solare			Conferma Id
87		32			15			Confirm					Confirm			Conferma				Conferma
88		32			15			Best Operating Point			Best Oper Point		Miglior Punto Lavoro			PntoLavoro
89		32			15			Solar Converter Redundancy		Solar Conv Red		Ridondanza Conv Solare			RidonConv
90		32			15			Load Fluctuation Range			Fluct Range		Intervallo Fluttuazione Carico		DeltaFluttz
91		32			15			System Energy Saving Point		Energy Save Pt		Punto Sistema Risp Energia		PntoRispEnerg
92		32			15			E-Stop Function				E-Stop Function		Funzione E-Stop				FunzE-Stop
94		32			15			Single Phase				Single Phase		Monofase				Monofase
95		32			15			Three Phases				Three Phases		Trifase					Trifase
96		32			15			Input Current Limit			Input Curr Lmt		Limit Corrente Ingresso			LimIin
97		32			15			Double Supply				Double Supply		Doppia Alimentazione			DoppiaAlim
98		32			15			Single Supply				Single Supply		Singola Alimentazione			SinglAlim
99		32			15			Small Supply				Small Supply		Alimentazione Bassa			AlimBassa
100		32			15			Restart on HVSD				Restart on HVSD		Riavvio su HVSD				RiavvioHVSD
101		32			15			Sequence Start Interval			Start Interval		Intervallo Sequenza Riavvio		IntervAvvio
102		32			15			Rated Voltage				Rated Voltage		Tensione Nominale			Vnom
103		32			15			Rated Current				Rated Current		Corrente Nominale			Inom
104		32			15			All Solar Converters Comm Fail		SolarConvsComFail	Guasto COM Tutti conv Solari		GstoCOMConv
105		32			15			Inactive				Inactive		Inattivo				Inattivo
106		32			15			Active					Active			Attivo					Attivo
112		32			15			Rated Voltage (Internal Use)		Rated Voltage		Tensione Nominale (Uso interno)		Vnom
113		32			15			Solar Converter Info Change (M/S Internal Use)	SolarConvInfo		SolarConvInfo				SolarConvInfo
114		32			15			MixHE Power				MixHE Power		Potenza MixHE				PotzMixHE
115		32			15			Derated					Derated			In Deriva				In Deriva
116		32			15			Non-Derated				Non-Derated		Non In Deriva				NonDeriva
117		32			15			All Solar Converters On Time		SolarConvONTime		Tutti Conv Solari Attivi		ConvSolarAtt
118		32			15			All Solar Converters are On		AllSolarConvOn		Tutti Conv Solari Accesi		ConvSolarON
119		39			15			Clear Solar Converter Comm Fail Alarm	Clear Comm Fail		Reset Guasto Com Conv Solari		ResetGstoConv
120		32			15			HVSD					HVSD			HVSD					HVSD
121		32			15			HVSD Voltage Difference			HVSD Volt Diff		Differenza Tensione HVSD		DeltaVHVSD
122		32			15			Total Rated Current			Total Rated Cur		Corrente Nominale Totale		CorrNomTot
123		32			15			Diesel Generator Power Limit		DG Pwr Lmt		Limitaz Potenza GE			LmtP GE
124		32			15			Diesel Generator Digital Input		Diesel DI Input		Ingresso Digitale GE			DI GE
125		32			15			Diesel Gen Power Limit Point Set	DG Pwr Lmt Pt		Impostazione Limit Potenza GE		PntLmtP GE
126		32			15			None					None			Nessuno					Nessuno
140		32			15			Solar Converter Delta Voltage		SolaConvDeltaVol	DeltaV Conv Solare			DeltaVConv
141		32			15			Solar Status				Solar Status		Stato Solare				StatoSolar
142		32			15			Day					Day			Giorno					Giorno
143		32			15			Night					Night			Notte					Notte
144		32			15			Existence State				Existence State		Stato Attuale				StatoAtt
145		32			15			Existent				Existent		Esistente				Esistente
146		32			15			Not Existent				Not Existent		Non Esistente				NonEsistente
147		32			15			Total Input Current			Input Current		Corrente Totale Ingresso		Corre In
148		32			15			Total Input Power			Input Power		Potenza Totale Ingresso			Pot IN
149		32			15			Total Rated Current			Total Rated Cur		Corrente Nominale Totale		CorrNom Tot
150		32			15			Total Output Power			Output Power		Potenza Totale Uscita			Pot USC
151		32			15			Reset Solar Converter IDs		Reset Solar IDs		Reset Convertitore Solare Ids		Reset Solare Ids
152		32			15			Clear Solar Converter Comm Fail		ClrSolarCommFail	Clear Solar Converter Comm Fail		ClrSolarCommFail
