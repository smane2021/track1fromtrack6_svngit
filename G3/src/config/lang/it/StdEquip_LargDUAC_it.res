﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Manual					Manual			Manuale					Manuale
2		32			15			Auto					Auto			Auto					Auto
3		32			15			Off					Off			SPENTO					SPENTO
4		32			15			On					On			ACCESO					ACCESO
5		32			15			No Input				No Input		No input				No input
6		32			15			Input 1					Input 1			Ingresso 1				Ingresso 1
7		32			15			Input 2					Input 2			Ingresso 2				Ingresso 2
8		32			15			Input 3					Input 3			Ingresso 3				Ingresso 3
9		32			15			No Input				No Input		No input				No input
10		32			15			Input					Input			Input					Input
11		32			15			Close					Close			Chiuso					Chiuso
12		32			15			Open					Open			Aperto					Aperto
13		32			15			Close					Close			Chiuso					Chiuso
14		32			15			Open					Open			Aperto					Aperto
15		32			15			Close					Close			Chiuso					Chiuso
16		32			15			Open					Open			Aperto					Aperto
17		32			15			Close					Close			Chiuso					Chiuso
18		32			15			Open					Open			Aperto					Aperto
19		32			15			Close					Close			Chiuso					Chiuso
20		32			15			Open					Open			Aperto					Aperto
21		32			15			Close					Close			Chiuso					Chiuso
22		32			15			Open					Open			Aperto					Aperto
23		32			15			Close					Close			Chiuso					Chiuso
24		32			15			Open					Open			Aperto					Aperto
25		32			15			Close					Close			Chiuso					Chiuso
26		32			15			Open					Open			Aperto					Aperto
27		32			15			1-Phase					1-Phase			Monofase				Monofase
28		32			15			3-Phase					3-Phase			Trifase					Trifase
29		32			15			No Measurement				No Measurement		Nessuna misura				No misura
30		32			15			1-Phase					1-Phase			Monofase				Monofase
31		32			15			3-Phase					3-Phase			Trifase					Trifase
32		32			15			Response				Response		Risposta				Risposta
33		32			15			Not Responding				Not Responding		Non risponde				Non risponde
34		32			15			AC Distribution				AC Distribution		Distribuzione CA			Distribuz CA
35		32			15			Mains 1 Uab/Ua				1 Uab/Ua		Rete 1 Vrs/Vr				Rete1 Vrs/Vr
36		32			15			Mains 1 Ubc/Ub				1 Ubc/Ub		Rete 1 Vst/Vs				Rete1 Vst/Vs
37		32			15			Mains 1 Uca/Uc				1 Uca/Uc		Rete 1 Vrt/Vt				Rete1 Vrt/Vt
38		32			15			Mains 2 Uab/Ua				2 Uab/Ua		Rete 2 Vrs/Vr				Rete2 Vrs/Vr
39		32			15			Mains 2 Ubc/Ub				2 Ubc/Ub		Rete 2 Vst/Vs				Rete2 Vst/Vs
40		32			15			Mains 2 Uca/Uc				2 Uca/Uc		Rete 2 Vrt/Vt				Rete2 Vrt/Vt
41		32			15			Mains 3 Uab/Ua				3 Uab/Ua		Rete 3 Vrs/Vr				Rete3 Vrs/Vr
42		32			15			Mains 3 Ubc/Ub				3 Ubc/Ub		Rete 3 Vst/Vs				Rete3 Vst/Vs
43		32			15			Mains 3 Uca/Uc				3 Uca/Uc		Rete 3 Vrt/Vt				Rete3 Vrt/Vt
53		32			15			Working Phase A Current			Phase A Curr		Corrente fase R				Corr fase R
54		32			15			Working Phase B Current			Phase B Curr		Corrente fase S				Corr fase S
55		32			15			Working Phase C Current			Phase C Curr		Corrente fase T				Corr fase T
56		32			15			AC Input Frequency			AC Input Freq		Frequenza ingresso CA			Freq ingr CA
57		32			15			AC Input Switch Mode			AC Switch Mode		Modo commutaz ingresso CA		Modo ingr CA
58		32			15			Fault Lighting Status			Fault Lighting		Guasto luci				Guasto luci
59		32			15			Mains 1 Input Status			1 Input Status		Stato ingresso rete 1			Stat ingr rete1
60		32			15			Mains 2 Input Status			2 Input Status		Stato ingresso rete 2			Stat ingr rete2
61		32			15			Mains 3 Input Status			3 Input Status		Stato ingresso rete 3			Stat ingr rete3
62		32			15			AC Output 1 Status			Output 1 Status		Stato uscita CA 1			Stat usc CA 1
63		32			15			AC Output 2 Status			Output 2 Status		Stato uscita CA 2			Stat usc CA 2
64		32			15			AC Output 3 Status			Output 3 Status		Stato uscita CA 3			Stat usc CA 3
65		32			15			AC Output 4 Status			Output 4 Status		Stato uscita CA 4			Stat usc CA 4
66		32			15			AC Output 5 Status			Output 5 Status		Stato uscita CA 5			Stat usc CA 5
67		32			15			AC Output 6 Status			Output 6 Status		Stato uscita CA 6			Stat usc CA 6
68		32			15			AC Output 7 Status			Output 7 Status		Stato uscita CA 7			Stat usc CA 7
69		32			15			AC Output 8 Status			Output 8 Status		Stato uscita CA 8			Stat usc CA 8
70		32			15			AC Input Frequency High			Frequency High		Alta frecuenza ingresso CA		Alta frequenza
71		32			15			AC Input Frequency Low			Frequency Low		Bassa frequenza ingresso CA		Bassa frequenza
72		32			15			AC Input MCCB Trip			Input MCCB Trip		Ingresso trip MCCB CA			Ingr trip MCCB
73		32			15			SPD Trip				SPD Trip		Trip SPD				Trip SPD
74		32			15			AC Output MCCB Trip			OutputMCCB Trip		Uscita trip MCCB CA			Usc trip MCCB
75		32			15			AC Input 1 Failure			Input 1 Failure		Guasto ingresso CA 1			Guasto ingr1 CA
76		32			15			AC Input 2 Failure			Input 2 Failure		Guasto ingresso CA 2			Guasto ingr2 CA
77		32			15			AC Input 3 Failure			Input 3 Failure		Guasto ingresso CA 3			Guasto ingr3 CA
78		32			15			Mains 1 Uab/Ua Low			M1 Uab/a UnderV		Rete 1 bassa Vrs/Vr			Rt1 bassa Vrs/Vr
79		32			15			Mains 1 Ubc/Ub Low			M1 Ubc/b UnderV		Rete 1 bassa Vst/Vs			Rt1 Baja Vst/Vs
80		32			15			Mains 1 Uca/Uc Low			M1 Uca/c UnderV		Rete 1 bassa Vrt/Vt			Rt1 Baja Vrt/Vt
81		32			15			Mains 2 Uab/Ua Low			M2 Uab/a UnderV		Rete 2 bassa Vrs/Vr			Rt2 Baja Vrs/Vr
82		32			15			Mains 2 Ubc/Ub Low			M2 Ubc/b UnderV		Rete 2 bassa Vst/Vs			Rt2 Baja Vst/Vs
83		32			15			Mains 2 Uca/Uc Low			M2 Uca/c UnderV		Rete 2 bassa Vrt/Vt			Rt2 Baja Vrt/Vt
84		32			15			Mains 3 Uab/Ua Low			M3 Uab/a UnderV		Rete 3 bassa Vrs/Vr			Rt3 Baja Vrs/Vr
85		32			15			Mains 3 Ubc/Ub Low			M3 Ubc/b UnderV		Rete 3 bassa Vst/Vs			Rt3 Baja Vst/Vs
86		32			15			Mains 3 Uca/Uc Low			M3 Uca/c UnderV		Rete 3 bassa Vrt/Vt			Rt3 Baja Vrt/Vt
87		32			15			Mains 1 Uab/Ua High			M1 Uab/a OverV		Rete 1 alta Vrs/Vr			Rt1 Alta Vrs/Vr
88		32			15			Mains 1 Ubc/Ub High			M1 Ubc/b OverV		Rete 1 alta Vst/Vs			Rt1 Alta Vst/Vs
89		32			15			Mains 1 Uca/Uc High			M1 Uca/c OverV		Rete 1 alta Vrt/Vt			Rt1 Alta Vrt/Vt
90		32			15			Mains 2 Uab/Ua High			M2 Uab/a OverV		Rete 2 alta Vrs/Vr			Rt2 Alta Vrs/Vr
91		32			15			Mains 2 Ubc/Ub High			M2 Ubc/b OverV		Rete 2 alta Vst/Vs			Rt2 Alta Vst/Vs
92		32			15			Mains 2 Uca/Uc High			M2 Uca/c OverV		Rete 2 alta Vrt/Vt			Rt2 Alta Vrt/Vt
93		32			15			Mains 3 Uab/Ua High			M3 Uab/a OverV		Rete 3 alta Vrs/Vr			Rt3 Alta Vrs/Vr
93		32			15			Mains 3 Ubc/Ub High			M3 Ubc/b OverV		Rete 3 alta Vst/Vs			Rt3 Alta Vst/Vs
94		32			15			Mains 3 Uca/Uc High			M3 Uca/c OverV		Rete 3 alta Vrt/Vt			Rt3 Alta Vrt/Vt
95		32			15			Mains 1 Uab/Ua Failure			M1 Uab/a Fail		Guasto Ret1 Vrs/Vr			Gsto Rt1 RS/R
96		32			15			Mains 1 Ubc/Ub Failure			M1 Ubc/b Fail		Guasto Ret1 Vst/Vs			Gsto Rt1 ST/S
97		32			15			Mains 1 Uca/Uc Failure			M1 Uca/c Fail		Guasto Ret1 Vrt/Vt			Gsto Rt1 RT/T
98		32			15			Mains 2 Uab/Ua Failure			M2 Uab/a Fail		Guasto Ret2 Vrs/Vr			Gsto Rt2 RS/R
99		32			15			Mains 2 Ubc/Ub Failure			M2 Ubc/b Fail		Guasto Ret2 Vst/Vs			Gsto Rt2 ST/S
100		32			15			Mains 2 Uca/Uc Failure			M2 Uca/c Fail		Guasto Ret2 Vrt/Vt			Gsto Rt2 RT/T
101		32			15			Mains 3 Uab/Ua Failure			M3 Uab/a Fail		Guasto Ret3 Vrs/Vr			Gsto Rt3 RS/R
102		32			15			Mains 3 Ubc/Ub Failure			M3 Ubc/b Fail		Guasto Ret3 Vst/Vs			Gsto Rt3 ST/S
103		32			15			Mains 3 Uca/Uc Failure			M3 Uca/c Fail		Guasto Ret3 Vrt/Vt			Gsto Rt3 RT/T
104		32			15			No Response				No Response		Non risponde				Non risponde
105		32			15			Overvoltage Limit			Overvolt Limit		Limite sovratensione			Lim sovratens
106		32			15			Undervoltage Limit			Undervolt Limit		Limite sottotensione			Lim sottotens
107		32			15			Phase Failure Voltage			Phase Fail Volt		Tensione guasto fase			V guasto fase
108		32			15			Overfrequency Limit			Overfreq Limit		Limite alta frequenza			Lim alta freq
109		32			15			Underfrequency Limit			Underfreq Limit		Límite bassa frequenza			Lim bassa freq
110		32			15			Current Transformer Coeff		Curr Trans Coef		Coeff corr trasformatore		Coeff corrente
111		32			15			Input Type				Input Type		Tipo ingresso				Tipo ingr
112		32			15			Input Num				Input Num		Num ingresso				Num ingr
113		32			15			Current Measurement			Curr Measure		Misura di corrente			Misura corr
114		32			15			Output Num				Output Num		Num uscita				Num uscita
115		32			15			Distribution Address			Distr Addr		Indirizzo di distribuzione		Dir distrib
116		32			15			Mains 1 Failure				Mains 1 Fail		Mancanza rete 1				Manc rete1
117		32			15			Mains 2 Failure				Mains 2 Fail		Mancanza rete 2				Manc rete2
118		32			15			Mains 3 Failure				Mains 3 Fail		Mancanza rete 3				Manc rete3
119		32			15			Mains 1 Uab/Ua Failure			M1 Uab/a Fail		Mancanza rete1 Vrs/Vr			Manc rt1 RS/R
120		32			15			Mains 1 Ubc/Ub Failure			M1 Ubc/b Fail		Mancanza rete1 Vst/Vs			Manc rt1 ST/S
121		32			15			Mains 1 Uca/Uc Failure			M1 Uca/c Fail		Mancanza rete1 Vrt/Vt			Manc rt1 RT/T
122		32			15			Mains 2 Uab/Ua Failure			M2 Uab/a Fail		Mancanza rete2 Vrs/Vr			Manc rt2 RS/R
123		32			15			Mains 2 Ubc/Ub Failure			M2 Ubc/b Fail		Mancanza rete2 Vst/Vs			Manc rt2 ST/S
124		32			15			Mains 2 Uca/Uc Failure			M2 Uca/c Fail		Mancanza rete2 Vrt/Vt			Manc rt2 RT/T
125		32			15			Mains 3 Uab/Ua Failure			M3 Uab/a Fail		Mancanza rete3 Vrs/Vr			Manc rt3 RS/R
126		32			15			Mains 3 Ubc/Ub Failure			M3 Ubc/b Fail		Mancanza rete3 Vst/Vs			Manc rt3 ST/S
127		32			15			Mains 3 Uca/Uc Failure			M3 Uca/c Fail		Mancanza rete3 Vrt/Vt			Manc rt3 RT/T
128		32			15			Overfrequency				Overfrequency		Alta frequenza				Alta freq
129		32			15			Underfrequency				Underfrequency		Bassa frequenza				Bassa freq
130		32			15			Mains 1 Uab/Ua UnderVoltage		M1 Uab/a UnderV		Sottotensione rete1 R-S/R		SttensV rt1 RS/R
131		32			15			Mains 1 Ubc/Ub UnderVoltage		M1 Ubc/b UnderV		Sottotensione rete1 S-T/S		SttensV rt1 ST/S
132		32			15			Mains 1 Uca/Uc UnderVoltage		M1 Uca/c UnderV		Sottotensione rete1 R-T/T		SttensV rt1 RT/T
133		32			15			Mains 2 Uab/Ua UnderVoltage		M2 Uab/a UnderV		Sottotensione rete2 R-S/R		SttensV rt2 RS/R
134		32			15			Mains 2 Ubc/Ub UnderVoltage		M2 Ubc/b UnderV		Sottotensione rete2 S-T/S		SttensV rt2 ST/S
135		32			15			Mains 2 Uca/Uc UnderVoltage		M2 Uca/c UnderV		Sottotensione rete2 R-T/T		SttensV rt2 RT/T
136		32			15			Mains 3 Uab/Ua UnderVoltage		M3 Uab/a UnderV		Sottotensione rete3 R-S/R		SttensV rt3 RS/R
137		32			15			Mains 3 Ubc/Ub UnderVoltage		M3 Ubc/b UnderV		Sottotensione rete3 S-T/S		SttensV rt3 ST/S
138		32			15			Mains 3 Uca/Uc UnderVoltage		M3 Uca/c UnderV		Sottotensione rete3 R-T/T		SttensV rt3 RT/T
139		32			15			Mains 1 Uab/Ua OverVoltage		M1 Uab/a OverV		SOvratensione rete1 R-S/R		SovrV rt1 RS/R
140		32			15			Mains 1 Ubc/Ub OverVoltage		M1 Ubc/b OverV		SOvratensione rete1 S-T/S		SovrV rt1 ST/S
141		32			15			Mains 1 Uca/Uc OverVoltage		M1 Uca/c OverV		SOvratensione rete1 R-T/T		SovrV rt1 RT/T
142		32			15			Mains 2 Uab/Ua OverVoltage		M2 Uab/a OverV		SOvratensione rete2 R-S/R		SovrV rt2 RS/R
143		32			15			Mains 2 Ubc/Ub OverVoltage		M2 Ubc/b OverV		SOvratensione rete2 S-T/S		SovrV rt2 ST/S
144		32			15			Mains 2 Uca/Uc OverVoltage		M2 Uca/c OverV		SOvratensione rete2 R-T/T		SovrV rt2 RT/T
145		32			15			Mains 3 Uab/Ua OverVoltage		M3 Uab/a OverV		SOvratensione rete3 R-S/R		SovrV rt3 RS/R
146		32			15			Mains 3 Ubc/Ub Over Voltage		M3 Ubc/b OverV		SOvratensione rete3 S-T/S		SovrV rt3 ST/S
147		32			15			Mains 3 Uca/Uc Over Voltage		M3 Uca/c OverV		SOvratensione rete3 R-T/T		SovrV rt3 RT/T
148		32			15			AC Input MCCB Trip			In-MCCB Trip		Ingresso CA scatto MCCB			Ingr CA MCCB
149		32			15			AC Output MCCB Trip			Out-MCCB Trip		Uscita CA scatto MCCB			Usc CA MCCB
150		32			15			SPD Trip				SPD Trip		Scatto SPD				Scatto SPD
169		32			15			No Response				No Response		Non risponde				Non risponde
170		32			15			Mains Failure				Mains Failure		Mancanza rete				Mancanza rete
171		32			15			Large AC Distribution Unit		Large AC Dist		Unità distr CA grande			Distr CA grande
172		32			15			No Alarm				No Alarm		Nessun allarme				Nessun allarme
173		32			15			Overvoltage				Overvolt		Sovratensione				Sovratensione
174		32			15			Undervoltage				Undervolt		Sottotensione				Sottotensione
175		32			15			AC Phase Failure			AC Phase Fail		Guasto fase CA				Guasto fase CA
176		32			15			No Alarm				No Alarm		Nessun allarme				Nessun allarme
177		32			15			Overfrequency				Overfrequency		Alta frequenza				Alta frequenza
178		32			15			Underfrequency				Underfrequency		Bassa frequenza				Bassa frequenza
179		32			15			No Alarm				No Alarm		Nessun allarme				Nessun allarme
180		32			15			AC Overvoltage				AC Overvolt		Sovratensione CA			Svratensione CA
181		32			15			AC Undervoltage				AC Undervolt		Sottotensione CA			Sottotension CA
182		32			15			AC Phase Failure			AC Phase Fail		Guasto Fase CA				Guasto fase CA
183		32			15			No Alarm				No Alarm		Nessun allarme				Nessun allarme
184		32			15			AC Overvoltage				AC Overvolt		Sovratensione CA			Svratensione CA
185		32			15			AC Undervoltage				AC Undervolt		Sottotensione CA			Sottotension CA
186		32			15			AC Phase Failure			AC Phase Fail		Guasto fase CA				Guasto fase CA
187		32			15			Mains 1 Uab/Ua Alarm			M1 Uab/a Alarm		Allarme rete1 Vrs/Vr			Allarm Rt1 RS/R
188		32			15			Mains 1 Ubc/Ub Alarm			M1 Ubc/b Alarm		Allarme rete1 Vst/Vs			Allarm Rt1 ST/S
189		32			15			Mains 1 Uca/Uc Alarm			M1 Uca/c Alarm		Allarme rete1 Vrt/Vt			Allarm Rt1 RT/T
190		32			15			Frequency Alarm				Freq Alarm		Allarme frequenza			Allarme freq
191		32			15			No Response				No Response		Non risponde				Non risponde
192		32			15			Normal					Normal			Normale					Normale
193		32			15			Failure					Failure			Guasto					Guasto
194		32			15			No Response				No Response		Non risponde				Non risponde
195		32			15			Mains Input No				Mains Input No		Num ingresso rete			Num Ingr Rete
196		32			15			No. 1					No. 1			N 1					N 1
197		32			15			No. 2					No. 2			N 2					N 2
198		32			15			No. 3					No. 3			N 3					N 3
199		32			15			None					None			Nessuno					Nessuno
200		32			15			Emergency Light				Emergency Light		Luci d'emergenza			Luci emrgnza
201		32			15			Close					Close			No					No
202		32			15			Open					Open			Sí					Sí
203		32			15			Mains 2 Uab/Ua Alarm			M2 Uab/a Alarm		Allarme rete2 Vrs/Vr			Allarm Rt2 RS/R
204		32			15			Mains 2 Ubc/Ub Alarm			M2 Ubc/b Alarm		Allarme rete2 Vst/Vs			Allarm Rt2 ST/S
205		32			15			Mains 2 Uca/Uc Alarm			M2 Uca/c Alarm		Allarme rete2 Vrt/Vt			Allarm Rt2 RT/T
206		32			15			Mains 3 Uab/Ua Alarm			M3 Uab/a Alarm		Allarme rete3 Vrs/Vr			Allarm Rt3 RS/R
207		32			15			Mains 3 Ubc/Ub Alarm			M3 Ubc/b Alarm		Allarme rete3 Vst/Vs			Allarm Rt3 ST/S
208		32			15			Mains 3 Uca/Uc Alarm			M3 Uca/c Alarm		Allarme rete3 Vrt/Vt			Allarm Rt3 RT/T
209		32			15			Normal					Normal			Normale					Normale
210		32			15			Alarm					Alarm			Allarme					Allarme
211		32			15			AC Fuse Number				AC Fuse No.		Num di fusibile CA			Núm fusib CA
212		32			15			Existence State				Existence State		Stato attuale				Stato attuale
213		32			15			Existent				Existent		Esistente				Esistente
214		32			15			Non-Existent				Non-Existent		Inesistente				Inesistente
