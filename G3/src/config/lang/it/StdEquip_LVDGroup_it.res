﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			LVD Group				LVD Group		Gruppo LVD				Gruppo LVD
2		32			15			Battery LVD				Batt LVD		LVD di batteria				LVD di batteria
3		32			15			None					None			Nessuno					Nessuno
4		32			15			LVD1					LVD1			LVD1					LVD1
5		32			15			LVD2					LVD2			LVD2					LVD2
6		32			15			LVD3					LVD3			LVD3					LVD3
7		32			15			LVD Needs AC Fail			LVD NeedACFail		Richiede mancanza rete			Serve manc RT
8		32			15			Disable					Disable			No					No
9		32			15			Enable					Enable			Sí					Sí
10		32			15			Number of LVDs				Num LVDs		Numero di LVD				Num LVD
11		32			15			0					0			0					0
12		32			15			1					1			1					1
13		32			15			2					2			2					2
14		32			15			3					3			3					3
15		32			15			HTD Point				HTD Point		Soglia disconnes alta Temp		Disc alta temp
16		32			15			HTD Reconnect Point			HTD Recon Pnt		Soglia riconnes alta Temp		Ricon alta temp
17		32			15			HTD Temperature Sensor			HTD Temp Sens		Sensore disconnes alta Temp		Sens dis alta T
18		32			15			Ambient Temperature			Ambient Temp		Temp ambiente				Ambiente
19		32			15			Battery Temperature			Battery Temp		Batteria				Batteria
20		32			15			Temperature 1				Temp1			Temperatura 1				Temp 1
21		32			15			Temperature 2				Temp2			Temperatura 2				Temp 2
22		32			15			Temperature 3				Temp3			Temperatura 3				Temp 3
23		32			15			Temperature 4				Temp4			Temperatura 4				Temp 4
24		32			15			Temperature 5				Temp5			Temperatura 5				Temp 5
25		32			15			Temperature 6				Temp6			Temperatura 6				Temp 6
26		32			15			Temperature 7				Temp7			Temperatura 7				Temp 7
27		32			15			Existence State				Exist State		Stato attuale				Stato attuale
28		32			15			Existent				Existent		Esistente				Esistente
29		32			15			Non-Existent				Non-Existent		Inesistente				Inesistente
31		32			15			LVD1					LVD1			LVD1					LVD1
32		32			15			LVD1 Mode				LVD1 Mode		Modo LVD1				Modo LVD1
33		32			15			LVD1 Voltage				LVD1 Voltage		Tensione LVD1				Tensione LVD1
34		32			15			LVD1 Reconnect Voltage			LVD1 ReconnVolt		Tensione riconnes LVD1			Riconn LVD1
35		32			15			LVD1 Reconnect Delay			LVD1 ReconDelay		Ritardo riconnes LVD1			Ritardo RLVD1
36		32			15			LVD1 Time				LVD1 Time		Tempo LVD1				Tempo LVD1
37		32			15			LVD1 Dependency				LVD1 Dependency		Dipendenza LVD1				Dipend LVD1
41		32			15			LVD2					LVD2			LVD2					LVD2
42		32			15			LVD2 Mode				LVD2 Mode		Modo LVD2				Modo LVD2
43		32			15			LVD2 Voltage				LVD2 Voltage		Tensione LVD2				Tensione LVD2
44		32			15			LVR2 Reconnect Voltage			LVR2 ReconnVolt		Tensione riconnes LVD2			Riconn LVD2
45		32			15			LVD2 Reconnect Delay			LVD2 ReconDelay		Ritardo riconnes LVD2			Ritardo RLVD2
46		32			15			LVD2 Time				LVD2 Time		Tempo LVD2				Tempo LVD2
47		32			15			LVD2 Dependency				LVD2 Dependency		Dipendenza LVD2				Dipend LVD2
51		32			15			Disabled				Disabled		Disabilitato				Disabilitato
52		32			15			Enabled					Enabled			Abilitato				Abilitato
53		32			15			Voltage					Voltage			Tensione				Tensione
54		32			15			Time					Time			Tempo					Tempo
55		32			15			None					None			No					No
56		32			15			LVD1					LVD1			LVD1					LVD1
57		32			15			LVD2					LVD2			LVD2					LVD2
103		32			15			HTD1 Enable				HTD1 Enable		Disconnes alta Temp1			Disc alta T1
104		32			15			HTD2 Enable				HTD2 Enable		Disconne alta Temp2			Disc alta T2
105		32			15			Battery LVD				Batt LVD		LVD batteria				LVD batteria
106		32			15			No Battery				No Batt			Nessuna batteria			No batteria
107		32			15			LVD1					LVD1			LVD1					LVD1
108		32			15			LVD2					LVD2			LVD2					LVD2
109		32			15			Battery Always On			BattAlwaysOn		Batteria sempre connessa		Bat connessa
110		32			15			LVD Contactor Type			LVD Type		Tipo contattore LVD			Tipo contat LVD
111		32			15			Bistable				Bistable		Bistabile				Bistabile
112		32			15			Monostable				Monostable		Monostabile				Monostabile
113		32			15			Monostable with Sample			Mono with Samp		Stabile con impulso			Stabile imp
116		32			15			LVD1 Disconnected			LVD1 Disconnected	LVD1 disconnesso			LVD1 aperto
117		32			15			LVD2 Disconnected			LVD2 Disconnected	LVD2 disconnesso			LVD2 aperto
125		32			15			State					State			Stato					Stato
126		32			15			LVD1 Voltage(24V)			LVD1 Voltage		Tensione LVD1(24V)			Tensione LVD1
127		32			15			LVD1 Reconnect Voltage(24V)		LVD1 ReconnVolt		Tensione riconnes LVD1(24V)		V ricones LVD1
128		32			15			LVD2 Voltage(24V)			LVD2 Voltage		Tensione LVD2 (24V)			Tensione LVD2
129		32			15			LVD2 Reconnect Voltage(24V)		LVD2 ReconnVolt		Tensione riconnes LVD2 (24V)		V ricones LVD2
130		32			15			LVD1 Control				LVD1			Control LVD1				LVD1
131		32			15			LVD2 Control				LVD2			Control LVD2				LVD2
132		32			15			LVD1 Reconnect				LVD1 Reconnect		Riconnes LVD1				Ricon LVD1
133		32			15			LVD2 Reconnect				LVD2 Reconnect		Riconnes LVD2				Ricon LVD2
134		32			15			HTD Point				HTD Point		Soglia disconnes alta Temp		Disc alta Temp
135		32			15			HTD Reconnect Point			HTD Recon Point		Soglia riconnes alta Temp		Ricon alta Temp
136		32			15			HTD Temperature Sensor			HTD Temp Sensor		Sensor disconnes alta Temp		Sens dis alta T
137		32			15			Ambient					Ambient			Ambiente				Ambiente
138		32			15			Battery					Battery			Batteria				Batteria
139		32			15			LVD Synchronize				LVD Synchronize		Sincronismo LVD				Sincronismo LVD
140		32			15			Relay for LVD3				Relay for LVD3		Relè per LVD3				Relè per LVD3
141		32			15			Relay Output 1				Relay Output 1		Uscita Relè1				Uscita Relè1
142		32			15			Relay Output 2				Relay Output 2		Uscita Relè2				Uscita Relè2
143		32			15			Relay Output 3				Relay Output 3		Uscita Relè3				Uscita Relè3
144		32			15			Relay Output 4				Relay Output 4		Uscita Relè4				Uscita Relè4
145		32			15			Relay Output 5				Relay Output 5		Uscita Relè5				Uscita Relè5
146		32			15			Relay Output 6				Relay Output 6		Uscita Relè6				Uscita Relè6
147		32			15			Relay Output 7				Relay Output 7		Uscita Relè7				Uscita Relè7
148		32			15			Relay Output 8				Relay Output 8		Uscita Relè8				Uscita Relè8
149		32			15			Relay Output 14				Relay Output 14		Uscita Relè14				Uscita Relè14
150		32			15			Relay Output 15				Relay Output 15		Uscita Relè15				Uscita Relè15
151		32			15			Relay Output 16				Relay Output 16		Uscita Relè16				Uscita Relè16
152		32			15			Relay Output 17				Relay Output 17		Uscita Relè17				Uscita Relè17
153		32			15			LVD 3 Enable				LVD 3 Enable		LVD3Abilitato				LVD3Abilitato
154		32			15			LVD Threshold				LVD Threshold		LVD La soglia				LVD La soglia
