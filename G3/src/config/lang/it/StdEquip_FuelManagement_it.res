﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			16			Fuel Tank				Fuel Tank		Serbatorio carburante			Deposito carb
2		32			15			Fuel Height				Fuel Height		Altezza carburante			Altezza carb
3		32			15			Fuel Volume				Fuel Volume		Volume carburante			Volume carb
4		32			15			Fuel Percent				Fuel Percent		Percentuale carburante			Percent carb
5		32			15			Fuel Theft Status			Theft Status		Stato furto carbu			Allarme furto
6		32			15			No					No			No					No
7		32			15			Yes					Yes			Sí					Sí
8		32			15			Multi-Shape Height Error		Multi HeightErr		Errore altezza multiforme		Err altezza
9		32			15			Confirm Tank Configuration		Confirm Tank		Conferma serbatoio			Conf serb
10		32			15			Reset Theft Alarm			Reset Theft Alr		Reset allarme furto			Reset all
11		32			15			Fuel Tank Type				Tank Type		Tipo serbatoio				Tipo serb
12		32			15			Square Tank Length			Square Length		Lunghezza				Lunghezza
13		32			15			Square Tank Width			Square Width		Profondità				Profondità
14		32			15			Square Tank Height			Square Height		Altezza					Altezza
15		32			15			Standing Cylinder Tank Diameter		Standing Diameter	Diametro cilindro			Diametro V
16		32			15			Standing Cylinder Tank Height		Standing Height		Altezza cilindro			Altezza V
17		32			15			Lying Cylinder Tank Diameter		Lying Diameter		Diametro cilindro orizzontale		Diametr cilin-H
18		32			15			Lying Cylinder Tank Length		Lying Length		Lunghezza cilindro orizzontale		Largh cilin-H
20		32			15			Number of Calibration Points		Num of Calib		Numero di punti calibrazione		Punti calibraz
21		32			15			Height of Calibration Point 1		Height Calib1		Altezza calibrazione punto 1		Altezz punto 1
22		32			15			Volume of Calibration Point 1		Volume Calib1		Volume calibrazione punto 1		Volume punto 1
23		32			15			Height of Calibration Point 2		Height Calib2		Altezza calibrazione punto 2		Altezz punto 2
24		32			15			Volume of Calibration Point 2		Volume Calib2		Volume calibrazione punto 2		Volume punto 2
25		32			15			Height of Calibration Point 3		Height Calib3		Altezza calibrazione punto 3		Altezz punto 3
26		32			15			Volume of Calibration Point 3		Volume Calib3		Volume calibrazione punto 3		Volume punto 3
27		32			15			Height of Calibration Point 4		Height Calib4		Altezza calibrazione punto 4		Altezz punto 4
28		32			15			Volume of Calibration Point 4		Volume Calib4		Volume calibrazione punto 4		Volume punto 4
29		32			15			Height of Calibration Point 5		Height Calib5		Altezza calibrazione punto 5		Altezz punto 5
30		32			15			Volume of Calibration Point 5		Volume Calib5		Volume calibrazione punto 5		Volume punto 5
31		32			15			Height of Calibration Point 6		Height Calib6		Altezza calibrazione punto 6		Altezz punto 6
32		32			15			Volume of Calibration Point 6		Volume Calib6		Volume calibrazione punto 6		Volume punto 6
33		32			15			Height of Calibration Point 7		Height Calib7		Altezza calibrazione punto 7		Altezz punto 7
34		32			15			Volume of Calibration Point 7		Volume Calib7		Volume calibrazione punto 7		Volume punto 7
35		32			15			Height of Calibration Point 8		Height Calib8		Altezza calibrazione punto 8		Altezz punto 8
36		32			15			Volume of Calibration Point 8		Volume Calib8		Volume calibrazione punto 8		Volume punto 8
37		32			15			Height of Calibration Point 9		Height Calib9		Altezza calibrazione punto 9		Altezz punto 9
38		32			15			Volume of Calibration Point 9		Volume Calib9		Volume calibrazione punto 9		Volume punto 9
39		32			15			Height of Calibration Point 10		Height Calib10		Altezza calibrazione punto 10		Altezz punto10
40		32			15			Volume of Calibration Point 10		Volume Calib10		Volume calibrazione punto 10		Volume punto10
41		32			15			Height of Calibration Point 11		Height Calib11		Altezza calibrazione punto 11		Altezz punto11
42		32			15			Volume of Calibration Point 11		Volume Calib11		Volume calibrazione punto 11		Volume punto11
43		32			15			Height of Calibration Point 12		Height Calib12		Altezza calibrazione punto 12		Altezz punto12
44		32			15			Volume of Calibration Point 12		Volume Calib12		Volume calibrazione punto 12		Volume punto12
45		32			15			Height of Calibration Point 13		Height Calib13		Altezza calibrazione punto 13		Altezz punto13
46		32			15			Volume of Calibration Point 13		Volume Calib13		Volume calibrazione punto 13		Volume punto13
47		32			15			Height of Calibration Point 14		Height Calib14		Altezza calibrazione punto 14		Altezz punto14
48		32			15			Volume of Calibration Point 14		Volume Calib14		Volume calibrazione punto 14		Volume punto14
49		32			15			Height of Calibration Point 15		Height Calib15		Altezza calibrazione punto 15		Altezz punto15
50		32			15			Volume of Calibration Point 15		Volume Calib15		Volume calibrazione punto 15		Volume punto15
51		32			15			Height of Calibration Point 16		Height Calib16		Altezza calibrazione punto 16		Altezz punto16
52		32			15			Volume of Calibration Point 16		Volume Calib16		Volume calibrazione punto 16		Volume punto16
53		32			15			Height of Calibration Point 17		Height Calib17		Altezza calibrazione punto 17		Altezz punto17
54		32			15			Volume of Calibration Point 17		Volume Calib17		Volume calibrazione punto 17		Volume punto17
55		32			15			Height of Calibration Point 18		Height Calib18		Altezza calibrazione punto 18		Altezz punto18
56		32			15			Volume of Calibration Point 18		Volume Calib18		Volume calibrazione punto 18		Volume punto18
57		32			15			Height of Calibration Point 19		Height Calib19		Altezza calibrazione punto 19		Altezz punto19
58		32			15			Volume of Calibration Point 19		Volume Calib19		Volume calibrazione punto 19		Volume punto19
59		32			15			Height of Calibration Point 20		Height Calib20		Altezza calibrazione punto 20		Altezz punto20
60		32			15			Volume of Calibration Point 20		Volume Calib20		Volume calibrazione punto 20		Volume punto20
62		32			15			Square					Square			Prisma					Prisma
63		32			15			Standing Cylinder			Standing Cyl		Cilindro verticale			Cilindro V
64		32			15			Lying Cylinder				Lying Cylinder		Cilindro orizzontale			Cilindro H
65		32			15			Multi Shape				Multi Shape		Multiforma				Multiforma
66		32			15			Low Fuel Level Limit			Lo Level Limit		Limite livello basso			Lim liv basso
67		32			15			High Fuel Level Limit			Hi Level Limit		Limite livello alto			Lim liv alto
68		32			15			Maximum Consumption Speed		Max Consumpt		Consumo massimo				Consumo max
71		32			15			High Fuel Level Alarm			Hi Fuel Level		Alto livello carburante			Alto carb
72		32			15			Low Fuel Level Alarm			Lo Fuel Level		Basso livello carburante		Basso carb
73		32			15			Fuel Theft Alarm			Fuel Theft Alrm		Allarme furto carburante		All furto carb
74		32			15			Square Height Error			Square Hght Err		Errore altezza prisma			Err alt prisma
75		32			15			Standing Cylinder Height Error		Stand Hght Err		Errore altezza cilindro V		Err alt cilin-V
76		32			15			Lying Cylinder Height Error		Lying Hght Err		Errore altezza cilindro H		Err alt cilin-H
77		32			15			Multi-Shape Height Error		Multi Hght Err		Errore altezza multiforma		Err alt M-forma
78		32			15			Fuel Tank Config Error			Fuel Config Error	Errore config serbatoio			ErrCfg serb
80		32			15			Fuel Tank Config Error Status		Config Error Status	Stato errore config serb		Stato ErrCfg
