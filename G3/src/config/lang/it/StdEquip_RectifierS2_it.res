﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Group Ⅱ Rectifier			Group Ⅱ Rect		Raddrizzatore SⅡ			Raddrizz SⅡ
2		32			15			DC Status				DC Status		Stato CC				Stato CC
3		32			15			DC Output Voltage			DC Voltage		Tensione CC				Tensione CC
4		32			15			Origin Current				Origin Current		Corrente CA				Corrente CA
5		32			15			Temperature				Temperature		Temperatura				Temperat RD
6		32			15			Used Capacity				Used Capacity		Capacità utilizzata			Cap usata
7		32			15			AC Input Voltage			AC Voltage		Tensione ingresso CA			Vac RD
8		32			15			DC Output Current			DC Current		Corrente CC				Corrente CC
9		32			15			Rectifier SN				Rectifier SN		Numero di serie raddrizzatore		Num serie RD
10		32			15			Total Running Time			Running Time		Tempo di funzionamento			Tempo funzion
11		32			15			No Response Counter			NoResponseCount		Contatore non risponde			Conta non risp
12		32			15			Derated by AC				Derated by AC		Potenza diminuita per CA		Pot dim per CA
13		32			15			Derated by Temperature			Derated by Temp		Potenza dim per temperatura		Pot dim temp
14		32			15			Derated					Derated			Diminuita				Diminuita
15		32			15			Full Fan Speed				Full Fan Speed		Alta velocità ventola			Alta veloc vent
16		32			15			WALK-In Function			Walk-in Func		Funzione walk-in			Walk-in
17		32			15			AC On/Off				AC On/Off		Stato CA				Stato CA
18		32			15			Current Limitation			Curr Limit		Limitaz di corrente			Lim corrente
19		32			15			High Voltage Limit			High-v limit		Limitaz di tensione alta		Lim tens alta
20		32			15			AC Input Status				AC Status		Stato CA				Stato CA
21		32			15			Rectifier High Temperature		Rect Temp High		Alta temperatura raddr			Alta temp RD
22		32			15			Rectifier Fault				Rect Fault		Guasto raddrizzatore			Guasto RD
23		32			15			Rectifier Protected			Rect Protected		Raddrizzatore in protezione		RD in protez
24		32			15			Fan Failure				Fan Failure		Guasto ventola raddrizzatore		Guasto vent RD
25		32			15			Current Limit Status			Curr-lmt Status		Limitaz corr raddrizzatore		Lim corr RD
26		32			15			EEPROM Failure				EEPROM Failure		Guasto EEPROM				Guasto EEPROM
27		32			15			DC On/Off Control			DC On/Off Ctrl		Controllo CC raddrizzatore		Ctrl CC RD
28		32			15			AC On/Off Control			AC On/Off Ctrl		Controllo CA raddrizzatore		Ctrl CA RD
29		32			15			LED Control				LED Control		Controllo LED raddrizzatore		Ctrl LED RD
30		32			15			Rectifier Reset				Rect Reset		Reset raddrizzatore			Reset rectif
31		32			15			AC Input Failure			AC Failure		Guasto ingresso CA			Guasto ingr CA
34		32			15			Overvoltage				Overvoltage		Sovratensione				Sovratens RD
37		32			15			Current Limit				Current Limit		Limitazione corrente			Lim corr
39		32			15			Normal					Normal			Normale					Normale
40		32			15			Limited					Limited			Limitata				Limitata
45		32			15			Normal					Normal			Normale					Normale
46		32			15			Full					Full			Completa				Completa
47		32			15			Disabled				Disabled		Disabilitata				Disabilitata
48		32			15			Enabled					Enabled			Abilitata				Abilitata
49		32			15			On					On			ACCESO					ACCESO
50		32			15			Off					Off			SPENTO					SPENTO
51		32			15			Normal					Normal			Normale					Normale
52		32			15			Failure					Failure			Guasto					Guasto
53		32			15			Normal					Normal			Normale					Normale
54		32			15			Overtemperature				Overtemp		Sovratemperatura			Sovratemp
55		32			15			Normal					Normal			Normale					Normale
56		32			15			Fault					Fault			Guasto					Guasto
57		32			15			Normal					Normal			Normale					Normale
58		32			15			Protected				Protected		In protezione				In protez
59		32			15			Normal					Normal			Normale					Normale
60		32			15			Failure					Failure			Guasto					Guasto
61		32			15			Normal					Normal			Normale					Normale
62		32			15			Alarm					Alarm			Allarme					Allarme
63		32			15			Normal					Normal			Normale					Normale
64		32			15			Failure					Failure			Guasto					Guasto
65		32			15			Off					Off			SPENTO					SPENTO
66		32			15			On					On			ACCESO					ACCESO
67		32			15			Off					Off			SPENTO					SPENTO
68		32			15			On					On			ACCESO					ACCESO
69		32			15			Flash					Flash			Intermittenza				Intermittenza
70		32			15			Cancel					Cancel			Normale					Normale
71		32			15			Off					Off			SPENTO					SPENTO
72		32			15			Reset					Reset			Reset					Reset
73		32			15			Open Rectifier				On			ACCESO					ACCESO
74		32			15			Close Rectifier				Off			SPENTO					SPENTO
75		32			15			Off					Off			SPENTO					SPENTO
76		32			15			LED Control				Flash			Intermittenza				Intermittenza
77		32			15			Rectifier Reset				Rect Reset		Reset raddrizzatore			Reset RD
80		32			15			Communication Failure			Comm Fail		Comunicazione interrotta		Guasto COM
84		32			15			Rectifier High SN			Rect High SN		N serie raddrizzatore			N serie RD
85		32			15			Rectifier Version			Rect Version		Versión raddrizzatore			Versione RD
86		32			15			Rectifier Part Number			Rect Part No.		Classif raddrizzatore			Classif RD
87		32			15			Current Sharing State			Curr Sharing		Corrente ripartiz carico		Corr rip carico
88		32			15			Current Sharing Alarm			CurrSharing Alm		Allarme ripartiz carico			All rprtz carco
89		32			15			Overvoltage				Overvoltage		Sovratensione				Sovratens RD
90		32			15			Normal					Normal			Normale					Normale
91		32			15			Overvoltage				Overvoltage		Sovratensione				Sovratensione
92		32			15			Line AB Voltage				Line AB Volt		Tensione RS				Tensione RS
93		32			15			Line BC Voltage				Line BC Volt		Tensione ST				Tensione ST
94		32			15			Line CA Voltage				Line CA Volt		Tensione RT				Tensione RT
95		32			15			Low Voltage				Low Voltage		Tensione bassa				Tensione bassa
96		32			15			AC Undervoltage Protection		U-Volt Protect		Protezione sottotensione CA		Protez sotVCA
97		32			15			Rectifier Position			Rect Position		Posizione raddrizzatore			Posizione RD
98		32			15			DC Output Shut Off			DC Output Off		Uscita CC spenta			Uscita CC off
99		32			15			Rectifier Phase				Rect Phase		Fase raddrizzatore			Fase RD
100		32			15			L1					L1			R					R
101		32			15			L2					L2			S					S
102		32			15			L3					L3			T					T
103		32			15			Severe Current Sharing Alarm		SevereCurrShar		Grave all ripartiz carico		Grave all rprtz
104		32			15			Bar Code1				Bar Code1		Bar code1				Bar code1
105		32			15			Bar Code2				Bar Code2		Bar code2				Bar code2
106		32			15			Bar Code3				Bar Code3		Bar code3				Bar code3
107		32			15			Bar Code4				Bar Code4		Bar code4				Bar code4
108		32			15			Rectifier Failure			Rect Failure		Guasto raddrizzatore			Guasto RD
109		32			15			No					No			No					No
110		32			15			Yes					Yes			Sí					Sí
111		32			15			Existence State				Existence ST		Stato attuale				Stato attuale
113		32			15			Communication OK			Comm OK			Comunicazione OK			COM OK
114		32			15			None Responding				None Responding		Nessuno risponde			Ness risponde
115		32			15			No Response				No Response		Nessuna risposta			Ness risposta
116		32			15			Rated Current				Rated Current		Corrente				Corrente
117		32			15			Rated Efficiency			Rated Efficien		Rendimento				Rendimento
118		32			15			Less Than 93				LT 93			Meno di 93				Meno di 93
119		32			15			Greater Than 93				GT 93			Più di 93				Più di 93
120		32			15			Greater Than 95				GT 95			Più di 95				Più di 95
121		32			15			Greater Than 96				GT 96			Più di 96				Più di 96
122		32			15			Greater Than 97				GT 97			Più di 97				Più di 97
123		32			15			Greater Than 98				GT 98			Più di 98				Più di 98
124		32			15			Greater Than 99				GT 99			Più di 99				Più di 99
125		32			15			Redundancy Related Alarm		Redundancy Alarm	Allarme ridondanza			All ridnza
126		32			15			Rectifier HVSD Status			HVSD Status		Stato HVSD				Stato HVSD
276		32			15			Emergency Stop/Shutdown			EmerStop/Shutdown	Emergenza stop				Emergnz stop
