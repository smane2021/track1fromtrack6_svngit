﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			LVD 3 Unit				LVD 3			LVD 3					LVD 3
11		32			15			Connected				Connected		Connesso				Connesso
12		32			15			Disconnected				Disconnected		Disconnesso				Disconnesso
13		32			15			No					No			No					No
14		32			15			Yes					Yes			Sí					Sí
21		32			15			LVD3 Status				LVD3 Status		Stato LVD3				Stato LVD3
22		32			15			LVD2 Status				LVD2 Status		Stato LVD2				Stato LVD2
23		32			15			LVD3 Failure				LVD3 Failure		Guasto LVD3				Guasto LVD3
24		32			15			LVD2 Failure				LVD2 Failure		Guasto LVD2				Guasto LVD2
25		32			15			Communication Failure			Comm Failure		Guasto comunicazione			Guasto Com
26		32			15			State					State			Stato					Stato
27		32			15			LVD3 Control				LVD3 Control		Controllo LVD3				Control LVD3
28		32			15			LVD2 Control				LVD2 Control		Controllo LVD2				Control LVD2
31		32			15			LVD3					LVD3			LVD3					LVD3
32		32			15			LVD3 Mode				LVD3 Mode		Modo LVD3				Modo LVD3
33		32			15			LVD3 Voltage				LVD3 Voltage		Tensione LVD3				Tensione LVD3
34		32			15			LVD3 Reconnect Voltage			LVD3 ReconnVolt		Tensione riconn LVD3			Tens ricon LVD3
35		32			15			LVD3 Reconnect Delay			LVD3 ReconDelay		Ritardo riconn LVD3			Ritrd ricn LVD3
36		32			15			LVD3 Time				LVD3 Time		Tempo LVD3				Tempo LVD3
37		32			15			LVD3 Dependency				LVD3 Dependency		Dipendenza LVD3				Dipend LVD3
41		32			15			LVD2					LVD2			LVD2					LVD2
42		32			15			LVD2 Mode				LVD2 Mode		Modo LVD2				Modo LVD2
43		32			15			LVD2 Voltage				LVD2 Voltage		Tensione LVD2				Tensione LVD2
44		32			15			LVD2 Reconnect Voltage			LVD2 ReconnVolt		Tensione riconn LVD2			Tens recon LVD2
45		32			15			LVD2 Reconnect Delay			LVD2 ReconDelay		Ritardo riconn LVD2			Ritrd ricn LVD2
46		32			15			LVD2 Time				LVD2 Time		Tempo LVD2				Tempo LVD2
47		32			15			LVD2 Dependency				LVD2 Dependency		Dipendenza LVD2				Dipend LVD2
51		32			15			Disabled				Disabled		Disabilitato				Disabilitato
52		32			15			Enabled					Enabled			Abilitato				Abilitato
53		32			15			Voltage					Voltage			Tensione				Tensione
54		32			15			Time					Time			Tempo					Tempo
55		32			15			None					None			No					No
56		32			15			LVD1					LVD1			LVD1					LVD1
57		32			15			LVD2					LVD2			LVD2					LVD2
103		32			15			High Temp Disconnect 3			HTD3			HTD3					HTD3
104		32			15			High Temp Disconnect 2			HTD2			HTD2					HTD2
105		32			15			Battery LVD				Batt LVD		LVD di batteria				LVD batteria
106		32			15			No Battery				No Batt			Nessuna batteria			No batteria
107		32			15			LVD3					LVD3			LVD3					LVD3
108		32			15			LVD2					LVD2			LVD2					LVD2
109		32			15			Battery Always On			BattAlwaysOn		Batteria sempre connessa		Sempre con bat
110		32			15			LVD Contactor Type			LVD Type		Tipo contattore LVD			Tipo LVD
111		32			15			Bistable				Bistable		Bistabile				Bistable
112		32			15			Monostable				Monostable		Monostabile				Monostabile
113		32			15			Monostable with Sampler			Mono with Samp		Stabile con impulso			Con impulso
116		32			15			LVD3 Disconnected			LVD3 Disconnect		LVD3 aperto				LVD3 aperto
117		32			15			LVD2 Disconnected			LVD2 Disconnect		LVD2 aperto				LVD2 aperto
118		32			15			LVD3 Monostable with Sampler		LVD3 Mono Sample	LVD3 stabile con impulso		LVD3 con impls
119		32			15			LVD2 Monostable with Sampler		LVD2 Mono Sample	LVD2 stabile con impulso		LVD2 con impls
125		32			15			State					State			Stato					Stato
126		32			15			LVD3 Voltage(24V)			LVD3 Voltage		Tensione LVD3(24V)			Tensione LVD3
127		32			15			LVD3 Reconnect Voltage(24V)		LVD3 ReconnVolt		Tensione riconn LVD3(24V)		Tens ricon LVD3
128		32			15			LVD2 Voltage(24V)			LVD2 Voltage		Tensione LVD2(24V)			Tensione LVD2
129		32			15			LVD2 Reconnect Voltage(24V)		LVD2 ReconnVolt		Tensione riconn LVD2(24V)		Tens ricon LVD2
130		32			15			LVD 3					LVD 3			LVD 3					LVD 3
200		32			15			Connect					Connect			Connesso				Connesso
201		32			15			Disconnect				Disconnect		Disconnesso				Disconnesso
