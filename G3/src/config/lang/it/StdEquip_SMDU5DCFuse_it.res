﻿#
#  Locale language support: Italian
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
it



[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE			
1	32			15			Load Fuse 1			Load Fuse1		Fusibile carico 1			Fus carico1
2	32			15			Load Fuse 2			Load Fuse2		Fusibile carico 2			Fus carico2
3	32			15			Load Fuse 3			Load Fuse3		Fusibile carico 3			Fus carico3
4	32			15			Load Fuse 4			Load Fuse4		Fusibile carico 4			Fus carico4
5	32			15			Load Fuse 5			Load Fuse5		Fusibile carico 5			Fus carico5
6	32			15			Load Fuse 6			Load Fuse6		Fusibile carico 6			Fus carico6
7	32			15			Load Fuse 7			Load Fuse7		Fusibile carico 7			Fus carico7
8	32			15			Load Fuse 8			Load Fuse8		Fusibile carico 8			Fus carico8
9	32			15			Load Fuse 9			Load Fuse9		Fusibile carico 9			Fus carico9
10	32			15			Load Fuse 10			Load Fuse10		Fusibile carico 10			Fus carico10
11	32			15			Load Fuse 11			Load Fuse11		Fusibile carico 11			Fus carico11
12	32			15			Load Fuse 12			Load Fuse12		Fusibile carico 12			Fus carico12
13	32			15			Load Fuse 13			Load Fuse13		Fusibile carico 13			Fus carico13
14	32			15			Load Fuse 14			Load Fuse14		Fusibile carico 14			Fus carico14
15	32			15			Load Fuse 15			Load Fuse15		Fusibile carico 15			Fus carico15
16	32			15			Load Fuse 16			Load Fuse16		Fusibile carico 16			Fuse carico16
17	32			15			SMDU5 DC Fuse			SMDU5 DC Fuse		Fusibile CC SMDU5			Fus CC SMDU5
18	32			15			State				State			Stato					Stato
19	32			15			Off				Off			SPENTO				SPENTO
20	32			15			On				On			ACCESO					ACCESO
21	32			15			Fuse 1 Alarm			Fuse 1 Alarm		Allarme fusibile 1			Allarme fus1
22	32			15			Fuse 2 Alarm			Fuse 2 Alarm		Allarme fusibile 2			Allarme fus2
23	32			15			Fuse 3 Alarm			Fuse 3 Alarm		Allarme fusibile 3			Allarme fus3
24	32			15			Fuse 4 Alarm			Fuse 4 Alarm		Allarme fusibile 4			Allarme fus4
25	32			15			Fuse 5 Alarm			Fuse 5 Alarm		Allarme fusibile 5			Allarme fus5
26	32			15			Fuse 6 Alarm			Fuse 6 Alarm		Allarme fusibile 6			Allarme fus6
27	32			15			Fuse 7 Alarm			Fuse 7 Alarm		Allarme fusibile 7			Allarme fus7
28	32			15			Fuse 8 Alarm			Fuse 8 Alarm		Allarme fusibile 8			Allarme fus8
29	32			15			Fuse 9 Alarm			Fuse 9 Alarm		Allarme fusibile 9			Allarme fus9
30	32			15			Fuse 10 Alarm			Fuse 10 Alarm		Allarme fusibile 10			Allarme fus10
31	32			15			Fuse 11 Alarm			Fuse 11 Alarm		Allarme fusibile 11			Allarme fus11
32	32			15			Fuse 12 Alarm			Fuse 12 Alarm		Allarme fusibile 12			Allarme fus12
33	32			15			Fuse 13 Alarm			Fuse 13 Alarm		Allarme fusibile 13			Allarme fus13
34	32			15			Fuse 14 Alarm			Fuse 14 Alarm		Allarme fusibile 14			Allarme fus14
35	32			15			Fuse 15 Alarm			Fuse 15 Alarm		Allarme fusibile 15			Allarme fus15
36	32			15			Fuse 16 Alarm			Fuse 16 Alarm		Allarme fusibile 16			Allarme fus16
37	32			15			Interrupt Times			Interrupt Times		Interruzioni				Interruzioni
38	32			15			Communication Interrupt		Comm Interrupt		Comunicazione interrotta		COM interr
39	32			15			Load 1 Current			Load 1 Current		Corrente carico 1			I carico 1
40	32			15			Load 2 Current			Load 2 Current		Corrente carico 2			I carico 2
41	32			15			Load 3 Current			Load 3 Current		Corrente carico 3			I carico 3
42	32			15			Load 4 Current			Load 4 Current		Corrente carico 4			I carico 4
43	32			15			Load 5 Current			Load 5 Current		Corrente carico 5			I carico 5
