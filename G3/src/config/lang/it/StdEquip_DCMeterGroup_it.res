﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			DC Meter Group				DCMeter Group		Gruppo Misura CC			GrpMisCC
2		32			15			DC Meter Number				DCMeter Num		Numero Misura CC			N°MisCC
3		32			15			Communication Failure			Comm Fail		Guasto Comunicazione			GstoComm
4		32			15			Existence State				Existence		Stato Attuale				Attuale
5		32			15			Existent				Existent		Esistente				Esistente
6		32			15			Not Existent				Not Existent		Inesistente				Inesistente
11		32			15			DC Meter Lost				DCMeter Lost		Misura CC Mancante			MisCC Mncnte
12		32			15			DC Meter Num Last Time			DCMeter Num Last	Ultima Misura CC			UltimaMisCC
13		32			15			Clear DC Meter Lost Alarm		ClrDCMeterLost		Reset Allarme Misura CC Mancante	ResetMisCCMncnte
14		32			15			Clear					Clear			Reset					Reset
15		32			15			Total Energy Consumption		TotalEnergy		Consumo Energia Totale			EnergiaTotale
