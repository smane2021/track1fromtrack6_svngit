﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Rectifier Group				Rect Group		Gruppo raddrizzatore			Gruppo RD
2		32			15			Total Current				Tot Rect Curr		Corrente totale				Corre.RD
3		32			15			Average Voltage				Rect Voltage		Tensione media				Tensione RD.
4		32			15			Used Capacity				Sys Cap Used		Capacità utilizzata			Capacità usata
5		32			15			Maximum Used Capacity			Max Used Cap		Max capacità utilizzata			Max cap usata
6		32			15			Minimum Used Capacity			Min Used Cap		Min capacità utilizzata			Min cap usata
7		32			15			Communicating Rectifiers		Comm Rect Num		Raddrizzatori in comunicazione		Num com RD
8		32			15			Active Rectifiers			Active Rects		Raddrizzatori attivi			RD attivi
9		32			15			Installed Rectifiers			Inst Rects		Numero di raddrizzatori			Numero RD
10		32			15			Rectifier AC Failure Status		AC Fail Status		Mancanza rete RD			Manc rete RD
11		32			15			Multi-rectifier Failure Status		Multi-Rect Fail		Molti RD guasti				Molti RD guasti
12		32			15			Rectifer Current Limit			Current Limit		Limitazione corrente RD			Lim corr RD
13		32			15			Voltage Trim				Voltage Trim		Tensione uscita				Tens uscita
14		32			15			DC On/Off Control			DC On/Off Ctrl		Controllo uscita CC			Ctrl uscita CC
15		32			15			AC On/Off Control			AC On/Off Ctrl		Controllo ingresso CA			Ctrl ingr CA
16		32			15			Rectifiers LEDs Control			LEDs Control		Controllo LED				Ctrl LED
17		32			15			Fan Speed Control			Fan Speed Ctrl		Controllo velocità ventola		Ctrl ventola
18		32			15			Rated Voltage				Rated Voltage		Tensione media				Tens media
19		32			15			Rated Current				Rated Current		Corrente media				Corr media
20		32			15			High Voltage Limit			Hi-Volt Limit		Limite tensione alta			Alta tens RD
21		32			15			Low Voltage Limit			Lo-Volt Limit		Limite tensione bassa			Bassa tens RD
22		32			15			High Temperature Limit			Hi-Temp Limit		Limite alta temperatura			Lim sovrV RD
24		32			15			Restart Time on Overvoltage		OverVRestartT		Riavvio su sovratensione		Riavv svratens
25		32			15			WALK-In Time				WALK-In Time		Tempo walk-in				Tempo walk-in
26		32			15			WALK-In					WALK-In			Walk-in abilitato			Walk-in ON
27		32			15			Minimum Redundancy			Min Redundancy		Ridondanza minima			Min ridond
28		32			15			Maximum Redundancy			Max Redundancy		Ridondanza massima			Max ridond
29		32			15			Switch Off Delay			SwitchOff Delay		Ritardo disconnessione			Ritardo disc
30		32			15			Cycle Period				Cyc Period		Periodo di ciclo			Prdo ciclo
31		32			15			Cycle Activation Time			Cyc Act Time		Ora ciclo				Ora ciclo
32		32			15			Rectifier AC Failure			Rect AC Fail		Guasto CA raddrizzatori			Guasto CA RD
33		32			15			Multi-Rectifiers Failure		Multi-rect Fail		Molti raddrizzatori guasti		Mlti RD guasti
36		32			15			Normal					Normal			Normale					Normale
37		32			15			Failure					Failure			Guasto					Guasto
38		32			15			Switch Off All				Switch Off All		Tutti spenti				Tutti spenti
39		32			15			Switch On All				Switch On All		Tutti accesi				Tutti accesi
42		32			15			Flash All				Flash All		Intermittenza				Intermittenza
43		32			15			Stop Flash				Stop Flash		Stop intermittenza			Stop intermitt
44		32			15			Full Speed				Full Speed		Velocità massima			Max velocità
45		32			15			Automatic Speed				Auto Speed		Velocità automatica			Velocità Auto
46		32			32			Current Limit Control			Curr Limit Ctrl		Controllo limitaz di corrente		Ctrl lim corrente
47		32			32			Full Capacity Control			Full Cap Ctrl		Controllo a piena capacità		Control a piena capacità
54		32			15			Disabled				Disabled		Disabilitato				Disabilitato
55		32			15			Enabled					Enabled			Abilitato				Abilitato
68		32			15			ECO Mode				ECO Mode		Modo ECO				Modo ECO
72		32			15			Rectifier On at AC Overvoltage		AC Overvolt ON		Raddr accesi su sovratensione		R con sovraVca
73		32			15			No					No			No					No
74		32			15			Yes					Yes			Sí					Sí
77		32			15			Pre-Current Limit Turn-On		Pre-Curr Limit		Prelimitazione di corrente		Prelimit corr
78		32			15			Rectifier Power type			Rect Power type		Tipo potenza raddrizzatore		Tipo pot RD
79		32			15			Double Supply				Double Supply		Alimentazione doppia			Alim doppia
80		32			15			Single Supply				Single Supply		Alimentazione singola			Alim singola
81		32			15			Last Rectifiers Quantity		Last Rect Qty		Ultimo num raddrizzatori		Ult num RD
82		32			15			Rectifier Lost				Rectifier Lost		Raddrizzatore mancante			RD mancante
83		32			15			Rectifier Lost				Rectifier Lost		Raddrizzatore mancante			RD mancante
84		32			15			Reset Rectifier Lost Alarm		Reset Rect Lost		Reset raddrizzatore mancante		Reset RD mncnte
85		32			15			Reset					Reset			Reset					Reset
86		32			15			Confirm Rect Position/Phase		Confirm Pos/PH		Conferma posizione/fase RD		Conf pos/fase
87		32			15			Confirm					Confirm			Conferma				Conferma
88		32			15			Best Operating Point			Best Point		Punto migliore di lavoro		Punto migliore
89		32			15			Rectifier Redundancy			Rect Redundancy		Ridondanza				Ridondanza
90		32			15			Load Fluctuation Range			Fluct Range		Intervallo variazione carico		Int var carico
91		32			15			System Energy Saving Point		EngySave Point		Punto risparmio energia			Pto risp Ener
92		32			15			E-Stop Function				E-Stop Function		Funzione E-Stop				Funz E-stop
93		32			15			AC Phases				AC Phases		Fasi CA					Fasi CA
94		32			15			Single Phase				Single Phase		Monofase				Monofase
95		32			15			Three Phases				Three Phases		Trifase					Trifase
96		32			15			Input Current Limit			InputCurrLimit		Limite corrente ingresso		Lim corr ingr
97		32			15			Double Supply				Double Supply		Alimentazione doppia			Alim doppia
98		32			15			Single Supply				Single Supply		Alimentazione singola			Alim singola
99		32			15			Small Supply				Small Supply		Alimentazione piccola			Alim piccola
100		32			15			Restart on Overvoltage Enabled		DCOverVRestart		Riavvio su sovratensione		Riavv su sovraV
101		32			15			Sequence Start Interval			Start Interval		Intervallo di ripartenza		Interv riavvio
102		32			15			Rated Voltage				Rated Voltage		Tensione				Tensione
103		32			15			Rated Current				Rated Current		Corrente nominale			Corr nom
104		32			15			All Rectifiers Comm Fail		AllRectCommFail		Nessun raddrizzatore risponde		No RD risp
105		32			15			Inactive				Inactive		No					No
106		32			15			Active					Active			Sí					Sí
107		32			15			ECO Mode Active				ECO Mode Active		Modo ECO attivo				Modo ECO attiv
108		32			15			ECO Cycle Alarm				ECO Cycle Alarm		Modo ECO oscillante			Modo ECO oscil
109		32			15			Reset ECO Cycle Alarm			Reset Cycle Alm		Reset modo ECO oscillante		Reset oscill
110		32			15			High Voltage Limit(24V)			Hi-Volt Limit		Limite tensione alta(24V)		Lim altaV(24V)
111		32			15			Rectifiers Trim(24V)			Rect Trim		Tensione (24V)				Tensione (24V)
112		32			15			Rated Voltage(Internal Use)		Rated Voltage		Tensione nominale			Tensione
113		32			15			Rect Info Change(M/S Internal Use)	RectInfo Change		Cambio info RD				RD chg info
114		32			15			MixHE Power				MixHE Power		Potenza mista HE			Pot mista HE
115		32			15			Derated					Derated			Diminuzione				Diminuz
116		32			15			Non-derated				Non-derated		Nessuna diminuzione			No diminuz
117		32			15			All Rectifiers ON Time			Rects ON Time		Tempo di deumidificazione		Tempo deumid
118		32			15			All Rectifiers On			All Rect On		RD in deumidificazione			RD in deumid
119		32			15			Clear Rectifier Comm Fail Alarm		Clear Comm Fail		Reset all guasto comunicaz RD		Reset guasto COM
120		32			15			HVSD					HVSD			Abilita HVSD				Abilita HVSD
121		32			15			HVSD Voltage Difference			HVSD Volt Diff		Differenza tensione VSD			Dif tens HVSD
122		32			15			Total Rated Current of Work On		Total Rated Cur		Corrente totale				Corr totale
123		32			15			Diesel Power Limitation			Disl Pwr Limit		Limitazione corrente per GE		Limit corr GE
124		32			15			Diesel DI Input				Diesel DI Input		Ingresso segnali GE			Ingr DI GE
125		32			15			Diesel Power Limit Point Set		DislPwrLmt Set		Limite potenza del GE			Lim pot GE
126		32			15			None					None			Nessuna					Nessuna
127		32			15			DI 1					DI 1			DI 1					DI 1
128		32			15			DI 2					DI 2			DI 2					DI 2
129		32			15			DI 3					DI 3			DI 3					DI 3
130		32			15			DI 4					DI 4			DI 4					DI 4
131		32			15			DI 5					DI 5			DI 5					DI 5
132		32			15			DI 6					DI 6			DI 6					DI 6
133		32			15			DI 7					DI 7			DI 7					DI 7
134		32			15			DI 8					DI 8			DI 8					DI 8
135		32			15			Current Limit Point			Curr Limit Pt		Limite corrente				Lim Corr
136		32			15			Current Limit				Curr Limit		Limitaz corrente abilitata		Lim Corr Max
137		32			15			Maximum Current Limit Value		Max Curr Limit		Valore limite corrente max		Vlim Corr Max
138		32			15			Default Current Limit Point		Def Curr Lmt Pt		Predefinito valore limite di corrente	Pred lim corr
139		32			15			Minimum Current Limit Value		Min Curr Limit		Valore minimo limite di corrente	Min lim corr
140		32			15			AC Power Limit Mode			AC Power Lmt		AC Modo Limitaz Potenza			AC LmtPot
141		32			15			A					A			A					A
142		32			15			B					B			B					B
143		32			15			Existence State				Existence State		Condizione				Condizione
144		32			15			Existent				Existent		Esistente				Esistente
145		32			15			Not Existent				Not Existent		Non esistente				Non esistente
146		32			15			Total Output Power			Output Power		Potenza Totale Uscita			Pot Usc
147		32			15			Total Slave Rated Current		Total RatedCurr		Corrente Nominale Totale Slave		CorrNom Tot
148		32			15			Reset Rectifier IDs			Reset Rect IDs		Reset Raddrizzatore Ids			Reset RD Ids
149		32			15			HVSD Voltage Difference (24V)		HVSD Volt Diff		HVSD Differenza Tensione (24V)		HVSD DeltaV
150		32			15			Normal Update				Normal Update		Aggiornamento Normale			Aggiorn Normale
151		32			15			Force Update				Force Update		Aggiornamento Forzato			Aggiorn Forzato
152		32			15			Update OK Number			Update OK Num		Aggiornamento Numero OK			Aggiorn Num OK
153		32			15			Update State				Update State		Aggiornamento Stato			Aggiorn Stato
154		32			15			Updating				Updating		Aggiornamento in corso			Aggiorn
155		32			15			Normal Successful			Normal Success		Normale Positivo			Norml Positiv
156		32			15			Normal Failed				Normal Fail		Normale Fallito				Norml Fallito
157		32			15			Force Successful			Force Success		Forzato Positivo			Forz Positiv
158		32			15			Force Failed				Force Fail		Forzato Fallito				Forz Fallito
159		32			15			Communication Time-Out			Comm Time-Out		Tempo Scaduto Comunicazione		Tempo Scad Com
160		32			15			Open File Failed			Open File Fail		Apertura File Fallita			Apert File Fall
161		32			15			Delay of LowRectCap Alarm		LowRectCapDelay				Ritardo Alm Basso Rect Cap			RitBassoRectCap		
162		32			15			Maximum Redundacy 				MaxRedundacy 				Massima Ridondanza 					Mas Ridondanza  		
163		32			15			Low Rectifier Capacity			Low Rect Cap				BassaCapRaddrizzatore			BassaCapRaddr			
164		32			15			High Rectifier Capacity			High Rect Cap				AltoCapRaddrizzatore			AltoCapRaddr			

165		32			15			Efficiency Tracker			Effi Track				Tracker Efficienza			Tracker Effic	
166		32			15			Benchmark Rectifier			Benchmark Rect			raddrizzatore Benchmark		Radd Benchmark	
167		32			15			R48-3500e3					R48-3500e3				R48-3500e3					R48-3500e3		
168		32			15			R48-3200					R48-3200				R48-3200					R48-3200		
169		32			15			96%							96%						96%							96%				
170		32			15			95%							95%						95%							95%				
171		32			15			94%							94%						94%							94%				
172		32			15			93%							93%						93%							93%				
173		32			15			92%							92%						92%							92%				
174		32			15			91%							91%						91%							91%				
175		32			15			90%							90%						90%							90%				
176		32			15			Cost per kWh				Cost per kWh			Costo per kWh				Costo per kWh	
177		32			15			Currency					Currency				Moneta						Moneta		
178		32			15			USD							USD						USD							USD				
179		32			15			CNY							CNY						CNY							CNY				
180		32			15			EUR							EUR						EUR							EUR				
181		32			15			GBP							GBP						GBP							GBP				
182		32			15			RUB							RUB						RUB							RUB				
183		32			15			Percentage 98% Rectifiers	Percent98%Rect			98% Raddrizzatori			98% Raddr	
184		32			15			10%							10%						10%							10%				
185		32			15			20%							20%						20%							20%				
186		32			15			30%							30%						30%							30%				
187		32			15			40%							40%						40%							40%				
188		32			15			50%							50%						50%							50%				
189		32			15			60%							60%						60%							60%				
190		32			15			70%							70%						70%							70%				
191		32			15			80%							80%						80%							80%				
192		32			15			90%							90%						90%							90%				
193		32			15			100%						100%					100%						100%			
																					
200		32			15			Default Voltage			Default Voltage				Tensione predefinita		Tensione Predef	
