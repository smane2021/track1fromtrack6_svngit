﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			DC Distribution				DC			Distribuzione CC			Distribuzione
2		32			15			DC Voltage				DC Voltage		Tensione CC				Tensione CC
3		32			15			Load Current				Load Current		Corrente di carico			Corrente
4		32			15			SCU Load Shunt Enable			SCU L-S Enable		Shunt di carico SCU			ShuntCarico SCU
5		32			15			Disabled				Disabled		No					No
6		32			15			Enabled					Enabled			Sí					Sí
7		32			15			Shunt Full Current			Shunt Current		Corrente shunt				Corrente shunt
8		32			15			Shunt Full Voltage			Shunt Voltage		Tensione shunt				Tensione shunt
9		32			15			Load Shunt Exists			Shunt Exists		Shunt di carico				Shunt carico
10		32			15			Yes					Yes			Sí					Sí
11		32			15			No					No			No					No
12		32			15			Overvoltage 1				Overvolt 1		Sovratensione 1				Sovratens 1
13		32			15			Overvoltage 2				Overvolt 2		Sovratensione 2				Sovratens 2
14		32			15			Undervoltage 1				Undervolt 1		Sottotensione 1				Sottotens 1
15		32			15			Undervoltage 2				Undervolt 2		Sottotensione 2				Sottotens 2
16		32			15			Overvoltage 1(24V)			Overvoltage 1		Sovratensione 1(24V)			Sovratens1(24V)
17		32			15			Overvoltage 2(24V)			Overvoltage 2		Sovratensione 2(24V)			Sovratens2(24V)
18		32			15			Undervoltage 1(24V)			Undervoltage 1		Sottotensione 1(24V)			Sottotens1(24V)
19		32			15			Undervoltage 2(24V)			Undervoltage 2		Sottotensione 2(24V)			Sottotens2(24V)
20		32			15			Total Load Current			TotalLoadCurr		Corrente totale carico			Corr totale
21		32			15			Load Alarm Flag				Load Alarm Flag		Load Alarm Flag				Load Alarm Flag
22		32			15			Current High Current			Curr Hi			Correnti Alta Corr					Corr Alta
23		32			15			Current Very High Current		Curr Very Hi		Corr molto Alta Corr		Corr Molto Alta
500		32			15			Current Break Size				Curr1 Brk Size		Correnti1 Rompere Size			Corr1RompSize		
501		32			15			Current High 1 Current Limit	Curr1 Hi1 Limit		Correnti1 LimCorrAlto1			Corr1LimAlto1		
502		32			15			Current High 2 Current Limit	Curr1 Hi2 Lmt		Correnti1 LimCorrAlto2			Corr1LimAlto2		
503		32			15			Current High 1 Curr				Curr Hi1Cur			Correnti Alta 1 Corr			CorrAlta1Corr		
504		32			15			Current High 2 Curr				Curr Hi2Cur			Correnti Alta 2 Corr			CorrAlta2Corr	
