﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			IPLU						IPLU					IPLU					IPLU
8		32			15			Battery Block 1 Voltage		Batt Blk1 Volt			BattBlocco1Tensione		BatBloc1Tensi
9		32			15			Battery Block 2 Voltage		Batt Blk2 Volt			BattBlocco2Tensione		BatBloc2Tensi
10		32			15			Battery Block 3 Voltage		Batt Blk3 Volt			BattBlocco3Tensione		BatBloc3Tensi
11		32			15			Battery Block 4 Voltage		Batt Blk4 Volt			BattBlocco4Tensione		BatBloc4Tensi
12		32			15			Battery Block 5 Voltage		Batt Blk5 Volt			BattBlocco5Tensione		BatBloc5Tensi
13		32			15			Battery Block 6 Voltage		Batt Blk6 Volt			BattBlocco6Tensione		BatBloc6Tensi
14		32			15			Battery Block 7 Voltage		Batt Blk7 Volt			BattBlocco7Tensione		BatBloc7Tensi
15		32			15			Battery Block 8 Voltage		Batt Blk8 Volt			BattBlocco8Tensione		BatBloc8Tensi
16		32			15			Battery Block 9 Voltage		Batt Blk9 Volt			BattBlocco9Tensione		BatBloc9Tensi
17		32			15			Battery Block 10 Voltage	Batt Blk10 Volt			BattBlocco10Tensione		BatBloc10Tensi
18		32			15			Battery Block 11 Voltage	Batt Blk11 Volt			BattBlocco11Tensione		BatBloc11Tensi
19		32			15			Battery Block 12 Voltage	Batt Blk12 Volt			BattBlocco12Tensione		BatBloc12Tensi
20		32			15			Battery Block 13 Voltage	Batt Blk13 Volt			BattBlocco13Tensione		BatBloc13Tensi
21		32			15			Battery Block 14 Voltage	Batt Blk14 Volt			BattBlocco14Tensione		BatBloc14Tensi
22		32			15			Battery Block 15 Voltage	Batt Blk15 Volt			BattBlocco15Tensione		BatBloc15Tensi
23		32			15			Battery Block 16 Voltage	Batt Blk16 Volt			BattBlocco16Tensione		BatBloc16Tensi
24		32			15			Battery Block 17 Voltage	Batt Blk17 Volt			BattBlocco17Tensione		BatBloc17Tensi
25		32			15			Battery Block 18 Voltage	Batt Blk18 Volt			BattBlocco18Tensione		BatBloc18Tensi
26		32			15			Battery Block 19 Voltage	Batt Blk19 Volt			BattBlocco19Tensione		BatBloc19Tensi
27		32			15			Battery Block 20 Voltage	Batt Blk20 Volt			BattBlocco20Tensione		BatBloc20Tensi
28		32			15			Battery Block 21 Voltage	Batt Blk21 Volt			BattBlocco21Tensione		BatBloc21Tensi
29		32			15			Battery Block 22 Voltage	Batt Blk22 Volt			BattBlocco22Tensione		BatBloc22Tensi
30		32			15			Battery Block 23 Voltage	Batt Blk23 Volt			BattBlocco23Tensione		BatBloc23Tensi
31		32			15			Battery Block 24 Voltage	Batt Blk24 Volt			BattBlocco24Tensione		BatBloc24Tensi
32		32			15			Battery Block 25 Voltage	Batt Blk25 Volt			BattBlocco25Tensione		BatBloc25Tensi
33		32			15			Temperature1				Temperature1			Temperature1				Temperature1
34		32			15			Temperature2				Temperature2			Temperature2				Temperature2
35		32			15			Battery Current				Battery Curr			Corrente Batt				Corrente Batt
36		32			15			Battery Voltage				Battery Volt			VoltBatteria				VoltBatt
40		32			15			Battery Block High			Batt Blk High			Batteria Block Alta			BattBlockAlta
41		32			15			Battery Block Low			Batt Blk Low			BattBlock Basso				BatBlockBasso	
50		32			15			IPLU No Response			IPLU No Response		IPLUNessunaRisposta			IPLUNessRispo
51		32			15			Battery Block1 Alarm		Batt Blk1 Alm			BattBlocco1Allarme			BattBloc1Alm
52		32			15			Battery Block2 Alarm		Batt Blk2 Alm			BattBlocco2Allarme			BattBloc2Alm
53		32			15			Battery Block3 Alarm		Batt Blk3 Alm			BattBlocco3Allarme			BattBloc3Alm
54		32			15			Battery Block4 Alarm		Batt Blk4 Alm			BattBlocco4Allarme			BattBloc4Alm
55		32			15			Battery Block5 Alarm		Batt Blk5 Alm			BattBlocco5Allarme			BattBloc5Alm
56		32			15			Battery Block6 Alarm		Batt Blk6 Alm			BattBlocco6Allarme			BattBloc6Alm
57		32			15			Battery Block7 Alarm		Batt Blk7 Alm			BattBlocco7Allarme			BattBloc7Alm
58		32			15			Battery Block8 Alarm		Batt Blk8 Alm			BattBlocco8Allarme			BattBloc8Alm
59		32			15			Battery Block9 Alarm		Batt Blk9 Alm			BattBlocco9Allarme			BattBloc9Alm
60		32			15			Battery Block10 Alarm		Batt Blk10 Alm			BattBlocco10Allarme			BattBloc10Alm
61		32			15			Battery Block11 Alarm		Batt Blk11 Alm			BattBlocco11Allarme			BattBloc11Alm
62		32			15			Battery Block12 Alarm		Batt Blk12 Alm			BattBlocco12Allarme			BattBloc12Alm
63		32			15			Battery Block13 Alarm		Batt Blk13 Alm			BattBlocco13Allarme			BattBloc13Alm
64		32			15			Battery Block14 Alarm		Batt Blk14 Alm			BattBlocco14Allarme			BattBloc14Alm
65		32			15			Battery Block15 Alarm		Batt Blk15 Alm			BattBlocco15Allarme			BattBloc15Alm
66		32			15			Battery Block16 Alarm		Batt Blk16 Alm			BattBlocco16Allarme			BattBloc16Alm
67		32			15			Battery Block17 Alarm		Batt Blk17 Alm			BattBlocco17Allarme			BattBloc17Alm
68		32			15			Battery Block18 Alarm		Batt Blk18 Alm			BattBlocco18Allarme			BattBloc18Alm
69		32			15			Battery Block19 Alarm		Batt Blk19 Alm			BattBlocco19Allarme			BattBloc19Alm
70		32			15			Battery Block20 Alarm		Batt Blk20 Alm			BattBlocco20Allarme			BattBloc20Alm
71		32			15			Battery Block21 Alarm		Batt Blk21 Alm			BattBlocco21Allarme			BattBloc21Alm
72		32			15			Battery Block22 Alarm		Batt Blk22 Alm			BattBlocco22Allarme			BattBloc22Alm
73		32			15			Battery Block23 Alarm		Batt Blk23 Alm			BattBlocco23Allarme			BattBloc23Alm
74		32			15			Battery Block24 Alarm		Batt Blk24 Alm			BattBlocco24Allarme			BattBloc24Alm
75		32			15			Battery Block25 Alarm		Batt Blk25 Alm			BattBlocco25Allarme			BattBloc25Alm
76		32			15			Battery Capacity			Battery Capacity		Capacità Batteria			Cap Batt
77		32			15			Capacity Percent			Capacity Percent		Capacità percent			Cap percent
78		32			15			Enable						Enable					Abilitare					Abilitare
79		32			15			Disable						Disable					Disabilitare				Disabilitare
84		32			15			No							No						No							No	
85		32			15			Yes							Yes						sì						sì

103		32			15			Existence State				Existence State		Stato Esisten				StatoEsisten
104		32			15			Existent					Existent			Esistente					Esistente
105		32			15			Not Existent				Not Existent		Non Esistente				NonEsistente
106		32			15			IPLU Communication Fail		IPLU Comm Fail		IPLUComFallita			IPLUComFallita
107		32			15			Communication OK			Comm OK				Comunicazione OK		Com OK
108		32			15			Communication Fail			Comm Fail			Comunicazione fallita	Com Fall
109		32			15			Rated Capacity				Rated Capacity				Capacità nominale		Cap Nominale
110		32			15			Used by Batt Management		Used by Batt Management		UtiloBattManagement		UtiloBattManagement
116		32			15			Battery Current Imbalance	Battery Current Imbalance	Squilibrio corrente Batt		SquilCorrBatt