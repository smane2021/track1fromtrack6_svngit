﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
zh

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			EIB-2				EIB-2			EIB-2			EIB-2
9		32			15			Bad Battery Block			Bad Batt Block		Elemento bat guasto			Elem bat guasto
10		32			15			Load Current 1				Load Curr 1		Corrente D1				Corr D1
11		32			15			Load Current 2				Load Curr 2		Corrente D2				Corr D2
12		32			15			Relay Output 1				Relay Output 1		Relé d'uscita 1				Relé uscita 1
13		32			15			Relay Output 2				Relay Output 2		Relé d'uscita 2				Relé uscita 2
14		32			15			Relay Output 3				Relay Output 3		Relé d'uscita 3				Relé uscita 3
15		32			15			Relay Output 4				Relay Output 4		Relé d'uscita 4				Relé uscita 4
16		32			15			EIB Communication Failure		EIB Comm Fail		Guasto EIB				Guasto EIB
17		32			15			State					State			Stato					Stato
18		32			15			Shunt 2 Full Current			Shunt 2 Curr		Corrente Shunt 2			Corr Shunt 2
19		32			15			Shunt 3 Full Current			Shunt 3 Curr		Corrente Shunt 3			Corr Shunt 3
20		32			15			Shunt 2 Full Voltage			Shunt 2 Volt		Tensione Shunt 2			Tens Shunt 2
21		32			15			Shunt 3 Full Voltage			Shunt 3 Volt		Tensione Shunt 3			Tens Shunt 3
22		32			15			Load Shunt 1				Load Shunt 1		Carico 1				Carico 1
23		32			15			Load Shunt 2				Load Shunt 2		Carico 2				Carico 2
24		32			15			Enable					Enable			Abilita					Abilita
25		32			15			Disable					Disable			Disabilita				Disabilita
26		32			15			Closed					Closed			Chiuso					Chiuso
27		32			15			Open					Open			Aperto					Aperto
28		32			15			State					State			Stato					Stato
29		32			15			No					No			No					No
30		32			15			Yes					Yes			Sí					Sí
31		32			15			EIB Communication Failure		EIB Comm Fail		Guasto EIB				Guasto EIB
32		32			15			Voltage 1				Voltage 1		Tensione 1				Tensione 1
33		32			15			Voltage 2				Voltage 2		Tensione 2				Tensione 2
34		32			15			Voltage 3				Voltage 3		Tensione 3				Tensione 3
35		32			15			Voltage 4				Voltage 4		Tensione 4				Tensione 4
36		32			15			Voltage 5				Voltage 5		Tensione 5				Tensione 5
37		32			15			Voltage 6				Voltage 6		Tensione 6				Tensione 6
38		32			15			Voltage 7				Voltage 7		Tensione 7				Tensione 7
39		32			15			Voltage 8				Voltage 8		Tensione 8				Tensione 8
40		32			15			Battery Number				Batt No.		Num di batteria				Num Batt
41		32			15			0					0			0					0
42		32			15			1					1			1					1
43		32			15			2					2			2					2
44		32			15			Load Current 3				Load Curr 3		Corriente D3				Corr D3
45		32			15			3					3			3					3
46		32			15			Load Number				Load Num		Número de carga				Núm Carga
47		32			15			Shunt 1 Full Current			Shunt 1 Curr		Corriente Shunt 1			Corr Shunt 1
48		32			15			Shunt 1 Full Voltage			Shunt 1 Volt		Tensión Shunt 1				Tens Shunt 1
49		32			15			Voltage Type				Voltage Type		Tipo de Tensión				Tipo Tensión
50		32			15			48(Block4)				48(Block4)		48(Elem4)				48(Elme4)
51		32			15			Mid point				Mid point		Punto medio				Punto medio
52		32			15			24(Block2)				24(Block2)		24(Elem2)				24(Elem2)
53		32			15			Block Voltage Diff(12V)			Block Diff(12V)		Dif Tensión Celda(12V)			Dif Vcel(12V)
54		32			15			Relay Output 5				Relay Output 5		Relé de Salida 5			Relé 5
55		32			15			Block Voltage Diff(Mid)			Block Diff(Mid)		Dif Tensión Celda(Mid)			Dif Vcel(Mid)
56		32			15			Number of Used Blocks			Used Blocks		N° elementi				N° elementi
78		32			15			Testing Relay 9				Test Relay 9		Prova relè 9				Prova relè 9
79		32			15			Testing Relay 10			Test Relay 10		Prova relè 10				Prova relè 10
80		32			15			Testing Relay 11			Test Relay 11		Prova relè 11				Prova relè 11
81		32			15			Testing Relay 12			Test Relay 12		Prova relè 12				Prova relè 12
82		32			15			Testing Relay 13			Test Relay 13		Prova relè 13				Prova relè 13
83		32			15			Not Used				Not Used		Non Usata				Non Usata
84		32			15			General					General			Generale				Generale
85		32			15			Load					Load			Carico					Carico
86		32			15			Battery					Battery			Batteria				Batteria
87		32			15			Shunt1 Set As				Shunt1SetAs		Shunt1 impostato come			Shunt1 Impost
88		32			15			Shunt2 Set As				Shunt2SetAs		Shunt2 impostato come			Shunt2 Impost
89		32			15			Shunt3 Set As				Shunt3SetAs		Shunt3 impostato come			Shunt3 Impost
90		32			15			Shunt1 Reading				Shunt1Reading		Lettura Shunt1				Lettura Shunt1
91		32			15			Shunt2 Reading				Shunt2Reading		Lettura Shunt2				Lettura Shunt2
92		32			15			Shunt3 Reading				Shunt3Reading		Lettura Shunt3				Lettura Shunt3
93		32			15			Temperature1				Temp1			Temp1					Temp1
94		32			15			Temperature2				Temp2			Temp2					Temp2
95		32			15			Load1 Alarm Flag			Load1 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
96		32			15			Load2 Alarm Flag			Load2 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
97		32			15			Load3 Alarm Flag			Load3 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
98		32			15			Current1 High Current			Curr 1 Hi		Current1 High Current			Curr 1 Hi
99		32			15			Current1 Very High Current		Curr 1 Very Hi		Current1 Very High Current		Curr 1 Very Hi
100		32			15			Current2 High Current			Curr 2 Hi		Current2 High Current			Curr 2 Hi
101		32			15			Current2 Very High Current		Curr 2 Very Hi		Current2 Very High Current		Curr 2 Very Hi
102		32			15			Current3 High Current			Curr 3 Hi		Current3 High Current			Curr 3 Hi
103		32			15			Current3 Very High Current		Curr 3 Very Hi		Current3 Very High Current		Curr 3 Very Hi
104		32			15			DO1 Normal State  			DO1 Normal		DO1 Stato Normale				DO1 Normale
105		32			15			DO2 Normal State  			DO2 Normal		DO2 Stato Normale				DO2 Normale
106		32			15			DO3 Normal State  			DO3 Normal		DO3 Stato Normale				DO3 Normale
107		32			15			DO4 Normal State  			DO4 Normal		DO4 Stato Normale				DO4 Normale
108		32			15			DO5 Normal State  			DO5 Normal		DO5 Stato Normale				DO5 Normale
112		32			15			Non-Energized				Non-Energized		Non-Energizzato			Non-Energizzato
113		32			15			Energized				Energized		Energizzato					Energizzato
500		32			15			Current1 Break Size				Curr1 Brk Size		Correnti1 Rompere Size			Corr1RompSize
501		32			15			Current1 High 1 Current Limit	Curr1 Hi1 Lmt		Correnti1 LimCorrAlto1			Corr1LimAlto1
502		32			15			Current1 High 2 Current Limit	Curr1 Hi2 Lmt		Correnti1 LimCorrAlto2			Corr1LimAlto2
503		32			15			Current2 Break Size				Curr2 Brk Size		Correnti2 Rompere Size			Corr2RompSize
504		32			15			Current2 High 1 Current Limit	Curr2 Hi1 Lmt		Correnti2 LimCorrAlto1			Corr2LimAlto1
505		32			15			Current2 High 2 Current Limit	Curr2 Hi2 Lmt		Correnti2 LimCorrAlto2			Corr2LimAlto2
506		32			15			Current3 Break Size				Curr3 Brk Size		Correnti3 Rompere Size			Corr3RompSize
507		32			15			Current3 High 1 Current Limit	Curr3 Hi1 Lmt		Correnti3 LimCorrAlto1			Corr3LimAlto1
508		32			15			Current3 High 2 Current Limit	Curr3 Hi2 Lmt		Correnti3 LimCorrAlto2			Corr3LimAlto2
509		32			15			Current1 High 1 Current			Curr1 Hi1Cur		Correnti1 Alta 1 Corr			Corr1Alta1Corr
510		32			15			Current1 High 2 Current			Curr1 Hi2Cur		Correnti1 Alta 2 Corr			Corr1Alta2Corr
511		32			15			Current2 High 1 Current			Curr2 Hi1Cur		Correnti2 Alta 1 Corr			Corr2Alta1Corr
512		32			15			Current2 High 2 Current			Curr2 Hi2Cur		Correnti2 Alta 2 Corr			Corr2Alta2Corr
513		32			15			Current3 High 1 Current			Curr3 Hi1Cur		Correnti3 Alta 1 Corr			Corr3Alta1Corr
514		32			15			Current3 High 2 Current			Curr3 Hi2Cur		Correnti3 Alta 2 Corr			Corr3Alta2Corr
