﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			EIB Battery				EIB Battery		EIB Bateria				EIB Bateria
2		32			15			Battery Current				Batt Current		Corrente de Bateria			Corrente de Bat
3		32			15			Battery Voltage				Batt Voltage		Tensão de Bateria			Tensão de Bat
4		32			15			Battery Capacity (Ah)			Batt Cap(Ah)		Capacidadee (Ah)			Capacidade (Ah)
5		32			15			Battery Capacity (%)			Batt Cap(%)		Capacidadee de Bateria (%)		Cap Bat (%)
6		32			15			Current Limit Exceeded			Curr Lmt Exceed		Limite Corrente excedido		Lim corr exced
7		32			15			Over Battery Current			Over Current		Sobrecorrente				Sobrecorrente
8		32			15			Low Capacity				Low Capacity		Baixa capacidade			Baixa capacidad
9		32			15			Yes					Yes			Sim					Sim
10		32			15			No					No			Não					Não
26		32			15			State					State			Estado					Estado
27		32			15			Battery Block 1 Voltage			Bat Block1 Volt		Tensão do Bloco Bateria 1		Tensão Bl.Bat.1
28		32			15			Battery Block 2 Voltage			Bat Block2 Volt		Tensão do Bloco Bateria 2		Tensão Bl.Bat.2
29		32			15			Battery Block 3 Voltage			Bat Block3 Volt		Tensão do Bloco Bateria 3		Tensão Bl.Bat.3
30		32			15			Battery Block 4 Voltage			Bat Block4 Volt		Tensão do Bloco Bateria 4		Tensão Bl.Bat.4
31		32			15			Battery Block 5 Voltage			Bat Block5 Volt		Tensão do Bloco Bateria 5		Tensão Bl.Bat.5
32		32			15			Battery Block 6 Voltage			Bat Block6 Volt		Tensão do Bloco Bateria 6		Tensão Bl.Bat.6
33		32			15			Battery Block 7 Voltage			Bat Block7 Volt		Tensão do Bloco Bateria 7		Tensão Bl.Bat.7
34		32			15			Battery Block 8 Voltage			Bat Block8 Volt		Tensão do Bloco Bateria 8		Tensão Bl.Bat.8
35		32			15			Battery Management			Batt Manage		Gerenciamento de bateria		Gerenciam Bat
36		32			15			Enable					Enable			Sim					Sim
37		32			15			Disable					Disable			Não					Não
38		32			15			Failure					Failure			Falha					Falha
39		32			15			Shunt Full Current			Shunt Current		Corrente Shunt				Corr Shunt
40		32			15			Shunt Full Voltage			Shunt Voltage		Tensão Shunt				Tens Shunt
41		32			15			On					On			Conectado				Conectado
42		32			15			Off					Off			Apagado					Apagado
43		32			15			Failure					Failure			Falha					Falha
44		32			15			Used Temperature Sensor			Used Sensor		Sensor de temperatura			Sensor Temp
87		32			15			None					None			Nenhum					Nenhum
91		32			15			Temperature Sensor 1			Temp Sensor 1		Sensor temperatura 1			Sens Temp 1
92		32			15			Temperature Sensor 2			Temp Sensor 2		Sensor temperatura 2			Sens Temp 2
93		32			15			Temperature Sensor 3			Temp Sensor 3		Sensor temperatura 3			Sens Temp 3
94		32			15			Temperature Sensor 4			Temp Sensor 4		Sensor temperatura 4			Sens Temp 4
95		32			15			Temperature Sensor 5			Temp Sensor 5		Sensor temperatura 5			Sens Temp 5
96		32			15			Rated Capacity				Rated Capacity		Capacidade nominal C10			Capacidade C10
97		32			15			Battery Temperature			Battery Temp		Temperatura de Bateria			Temp Bateria
98		32			15			Battery Temperature Sensor		BattTempSensor		Sensor Temperatura Bateria		Sensor Temp
99		32			15			None					None			Nenhum					Nenhum
100		32			15			Temperature 1				Temp 1			Temperatura 1				Temp 1
101		32			15			Temperature 2				Temp 2			Temperatura 2				Temp 2
102		32			15			Temperature 3				Temp 3			Temperatura 3				Temp 3
103		32			15			Temperature 4				Temp 4			Temperatura 4				Temp 4
104		32			15			Temperature 5				Temp 5			Temperatura 5				Temp 5
105		32			15			Temperature 6				Temp 6			Temperatura 6				Temp 6
106		32			15			Temperature 7				Temp 7			Temperatura 7				Temp 7
107		32			15			Temperature 8				Temp 8			Temperatura 8				Temp 8
108		32			15			Temperature 9				Temp 9			Temperatura 9				Temp 9
109		32			15			Temperature 10				Temp 10			Temperatura 10				Temp 10
110		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		Bateria Alarme Desequilíbrio Corr		BatDesequilCorr
111		32			15			EIB1 Battery2				EIB1 Battery2				EIB1 Bateria2				EIB1 Bateria2
112		32			15			EIB1 Battery3				EIB1 Battery3				EIB1 Bateria3				EIB1 Bateria3
113		32			15			EIB2 Battery1				EIB2 Battery1				EIB2 Bateria1				EIB2 Bateria1
114		32			15			EIB2 Battery2				EIB2 Battery2				EIB2 Bateria2				EIB2 Bateria2
115		32			15			EIB2 Battery3				EIB2 Battery3				EIB2 Bateria3				EIB2 Bateria3
116		32			15			EIB3 Battery1				EIB3 Battery1				EIB3 Bateria1				EIB3 Bateria1
117		32			15			EIB3 Battery2				EIB3 Battery2				EIB3 Bateria2				EIB3 Bateria2
118		32			15			EIB3 Battery3				EIB3 Battery3				EIB3 Bateria3				EIB3 Bateria3
119		32			15			EIB4 Battery1				EIB4 Battery1				EIB4 Bateria1				EIB4 Bateria1
120		32			15			EIB4 Battery2				EIB4 Battery2				EIB4 Bateria2				EIB4 Bateria2
121		32			15			EIB4 Battery3				EIB4 Battery3				EIB4 Bateria3				EIB4 Bateria3
122		32			15			EIB1 Battery1				EIB1 Battery1				EIB1 Bateria1				EIB1 Bateria1

150		32			15		Battery 1					Batt 1		Bateria 1					Bat 1
151		32			15		Battery 2					Batt 2		Bateria 2					Bat 2
152		32			15		Battery 3					Batt 3		Bateria 3					Bat 3
