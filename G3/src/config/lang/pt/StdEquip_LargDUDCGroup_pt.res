﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			LVD1					LVD1			LVD1					LVD1
2		32			15			LVD2					LVD2			LVD2					LVD2
3		32			15			LVD3					LVD3			LVD3					LVD3
4		32			15			DC Distribution Number			DC Distr Num		Núm de Distribuíção			Núm Distrib
5		32			15			LargDU DC Distribution Group		Large DC Group		Grupo Distribuíção grande		Grupo granDist
6		32			15			High Temperature 1 Limit		High Temp1		Límite alta temperatura 1		Lim alta Temp1
7		32			15			High Temperature 2 Limit		High Temp2		Límite alta temperatura 2		Lim alta Temp2
8		32			15			High Temperature 3 Limit		High Temp3		Límite alta temperatura 3		Lim alta Temp3
9		32			15			Low Temperature 1 Limit			Low Temp1		Límite baixa temperatura 1		Lim baixa Temp1
10		32			15			Low Temperature 2 Limit			Low Temp2		Límite baixa temperatura 2		Lim baixa Temp2
11		32			15			Low Temperature 3 Limit			Low Temp3		Límite baixa temperatura 3		Lim baixa Temp3
12		32			15			LVD1 Voltage				LVD1 Voltage		Tensão LVD1				Tensão LVD1
13		32			15			LVD2 Voltage				LVD2 Voltage		Tensão LVD2				Tensão LVD2
14		32			15			LVD3 Voltage				LVD3 Voltage		Tensão LVD3				Tensão LVD3
15		32			15			Overvoltage				Overvoltage		SobreTensão				SobreTensão
16		32			15			Undervoltage				Undervoltage		SubTensão				SubTensão
17		32			15			On					On			Conectado				Conectado
18		32			15			Off					Off			Apagado					Apagado
19		32			15			On					On			Conectado				Conectado
20		32			15			Off					Off			Apagado					Apagado
21		32			15			On					On			Conectado				Conectado
22		32			15			Off					Off			Apagado					Apagado
23		32			15			Total Load Current			Total Load		Corrente total carga			Carga Total
24		32			15			Total DCD Current			Total DCD Curr		Corrente total Distrib			Total Corr Dist
25		32			15			DCDistribution Average Voltage		DCD Average Volt	Tensão media Distribuíção		Tens media Dist
26		32			15			LVD1 Enabled				LVD1 Enabled		Desconexão LVD1			LVD1
27		32			15			LVD1 Mode				LVD1 Mode		Modo LVD1				Modo LVD1
28		32			15			LVD1 Time				LVD1 Time		Tempo LVD1				Tempo LVD1
29		32			15			LVD1 Reconnect Voltage			LVD1 ReconVolt		Tensão reconexão LVD1			Tens rec LVD1
30		32			15			LVD1 Reconnect Delay			LVD1 ReconDelay		Retardo reconexão LVD1			Ret recn LVD1
31		32			15			LVD1 Dependency				LVD1 Depend		Dependencia LVD1			Depend LVD1
32		32			15			LVD2 Enabled				LVD2 Enabled		Desconexão LVD2			LVD2
33		32			15			LVD2 Mode				LVD2 Mode		Modo LVD2				Modo LVD2
34		32			15			LVD2 Time				LVD2 Time		Tempo LVD2				Tempo LVD2
35		32			15			LVD2 Reconnect Voltage			LVD2 ReconVolt		Tensão reconexão LVD2			Tens rec LVD2
36		32			15			LVD2 Reconnect Delay			LVD2 ReconDelay		Retardo reconexão LVD2			Ret recn LVD2
37		32			15			LVD2 Dependency				LVD2 Depend		Dependencia LVD2			Depend LVD2
38		32			15			LVD3 Enabled				LVD3 Enabled		Desconexão LVD3			LVD3
39		32			15			LVD3 Mode				LVD3 Mode		Modo LVD3				Modo LVD3
40		32			15			LVD3 Time				LVD3 Time		Tempo LVD3				Tempo LVD3
41		32			15			LVD3 Reconnect Voltage			LVD3 ReconVolt		Tensão reconexão LVD3			Tens rec LVD3
42		32			15			LVD3 Reconnect Delay			LVD3 ReconDelay		Retardo reconexão LVD3			Ret recn LVD3
43		32			15			LVD3 Dependency				LVD3 Depend		Dependencia LVD3			Depend LVD3
44		32			15			Disabled				Disabled		Desabilitada				Desabilitada
45		32			15			Enabled					Enabled			Habilitada				Habilitada
46		32			15			Voltage					Voltage			Tensão					Tensão
47		32			15			Time					Time			Tempo					Tempo
48		32			15			None					None			Nenhum					Nenhum
49		32			15			LVD1					LVD1			LVD1					LVD1
50		32			15			LVD2					LVD2			LVD2					LVD2
51		32			15			LVD3					LVD3			LVD3					LVD3
52		32			15			Number of LVDs				No of LVDs		Número de LVDs				Núm LVDs
53		32			15			0					0			0					0
54		32			15			1					1			1					1
55		32			15			2					2			2					2
56		32			15			3					3			3					3
57		32			15			Existence State				Existence State		Detecção				Detecção
58		32			15			Existent				Existent		Existente				Existente
59		32			15			Non-Existent				Non-Existent		Não existente				Não existente
60		32			15			Battery Overvoltage			Batt OverVolt		SobreTensão Bateria			Sobretens Bat
61		32			15			Battery Undervoltage			Batt UnderVolt		SubTensão Bateria			SubTensão Bat
