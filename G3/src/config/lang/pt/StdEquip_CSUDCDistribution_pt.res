﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Distribution Fuse Tripped		Dist Fuse Trip		Fusível Distribuição disparado	Fus Distr Disp
2		32			15			Contactor 1 Failure			Contactor1 fail		Falha Contactor 1			Falha Contact1
3		32			15			Distribution Fuse 2 Tripped		Dist fuse2 trip		Fusível Distribuição 2 disparado	Fus2 Distr Disp
4		32			15			Contactor 2 Failure			Contactor2 fail		Falha Contactor 2			Falha Contact2
5		32			15			Distribution Voltage			Dist voltage		Tensão Distribuição			Tensão Distr
6		32			15			Distribution Current			Dist current		Corrente Distribuição			Corrente Dist
7		32			15			Distribution Temperature		Dist temperature	Temperatura Distribuição		Temperatura
8		32			15			Distribution Current 2			Dist current2		Corrente Distribuição 2		Corrente Dist2
9		32			15			Distribution Voltage Fuse 2		Dist volt fuse2		Tensão Distribuição 2		Tensão Distr2
10		32			15			CSU DCDistribution			CSU_DCDistrib		Distribuição CC CSU			CSU Distr CC
11		32			15			CSU DCDistribution Failure		CSU_DCDistfail		Falha Distribuição CSU		Falha Dist CSU
12		32			15			No					No			Não					Não
13		32			15			Yes					yes			Sim					Sim
14		32			15			Existent				Existent		Existente				Existente
15		32			15			Non-Existent				Non-Existent		Inexistente				Inexistente
