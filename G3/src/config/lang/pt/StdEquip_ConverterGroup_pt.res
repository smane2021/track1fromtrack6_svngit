﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Converter Group				Conv Group		Grupo Conversores			Grupo Convert
2		32			15			Total Current				Total Current		Corrente Total				Corr. Total
3		32			15			Average Voltage				Average Voltage		Tensão media				Tensão media
4		32			15			Converter Capacity Used			Conv Cap Used		Capacidade Conversor Usada		Cap.Conv.Usada
5		32			15			Maximum Capacity Used			Max Cap Used		Capacidade Máxima Usada		Cap.Máx.Usada
6		32			15			Minimum Capacity Used			Min Cap Used		Capacidade Mínima Usada		Cap.Mín.Usada
7		32			15			Communicating Converters		No. Comm Conv		Conversores en Comunicação		Conv en Comunic
8		32			15			Valid Converters			Valid Conv		Conversores válidos			Conv válidos
9		32			15			Number of Converters			No. Converters		Número de Conversores			Conversores
10		32			15			Converter AC Failure State			AC-Fail State		Conversores Falha de Red		Falha Red Conv
11		32			15			Multi-Converters Failure Status		Multi-Conv Fail		Falha múltiple Conversores		Falha MultiConv
12		32			15			Converter Current Limit			Current Limit		Límite Corrente Conversores		Lim Corr Conv
13		32			15			Converters Trim				Conv Trim		Corte Conversores			Corte Conv
14		32			15			DC On/Off Control			DC On/Off Ctrl		Control CC				Control CC
15		32			15			AC On/Off Control			AC On/Off Ctrl		Control CA				Control CA
16		32			15			Converters LEDs Control			LEDs Control		Control LEDs				Control LEDs
17		32			15			Fan Speed Control			Fan Speed Ctrl		Control ventiladores			Control Ventl
18		32			15			Rated Voltage				Rated Voltage		Nivel de Tensão			Nivel Tensão
19		32			15			Rated Current				Rated Current		Corrente				Corrente
20		32			15			High Voltage Limit			Hi-Volt Limit		Límite Alta Tensão			Lim Alta Tens
21		32			15			Low Voltage Limit			Lo-Volt Limit		Límite Baixa Tensão			Lim Baixa Tens
22		32			15			High Temperature Limit			Hi-Temp Limit		Límite Alta temperatura		Lim Alta Temp
24		32			15			Restart Time on Over Voltage		OverVRestartT		Tempo inicio tras sobretens		Inicio sobrtens
25		32			15			Walk-in Time				Walk-in Time		Tempo Entrada Suave			Tiem Ent Suave
26		32			15			Walk-in					Walk-in			Função Entrada Suave			Entrada Suave
27		32			15			Min Redundancy				Min Redundancy		Redundancia mínima			Min Redund
28		32			15			Max Redundancy				Max Redundancy		Redundancia máxima			Max Redund
29		32			15			Switch Off Delay			SwitchOff Delay		Retardo desconexão			Retardo desc
30		32			15			Cycle Period				Cyc Period		Período de ciclado			Días Ciclado
31		32			15			Cycle Activation Time			Cyc Act Time		Hora de ciclado				Hora Ciclado
32		32			15			Rectifier AC Failure			Rect AC Fail		Falha red rectificadores		Falha red rect
33		32			15			Multi-Converters Failure		Multi-conv Fail		Falha múltiple Conversor		Falha multiconv
36		32			15			Normal					Normal			Normal					Normal
37		32			15			Failure					Failure			Falha					Falha
38		32			15			Switch Off All				Switch Off All		Apagar todos				Apagar todos
39		32			15			Switch On All				Switch On All		Ligar todos				Ligar todos
42		32			15			All Flash				All Flash		Intermitente				Intermitente
43		32			15			Stop Flash				Stop Flash		Normal					Normal
44		32			15			Full Speed				Full Speed		Máxima velocidad			Max velocidad
45		32			15			Automatic Speed				Auto Speed		Velocidad automática			Velocidad Auto
46		32			32			Current Limit Control			Curr-Limit Ctl		Control límite de Corrente		Ctrl lim Corrente
47		32			32			Full Capacity Control			Full-Cap Ctl		Control a plena capacidad		Control a plena capacidad
54		32			15			Disabled				Disabled		Deshabilitada				Deshabilitada
55		32			15			Enabled					Enabled			Habilitada				Habilitada
68		32			15			System ECO				System ECO		Em espera				Em Espera
72		32			15			Turn-on at AC Over-voltage		Turn-on ACOverV		Ligado com sobreTensão CA		Conec sobreVCA
73		32			15			No					No			Não					Não
74		32			15			Yes					Yes			Sim					Sim
77		32			15			Pre-CurrLmt On Switch-on Enb		Pre-CurrLimit		Pre-limitação				Pre-limitação
81		32			15			Last Rectifiers Quantity		Last Rect Qty		Ultimo Total Rectificadores		Ultimo N Rectif
82		32			15			Converter Lost				Converter Lost		Conversor perdido			Conv perdido
83		32			15			Converter Lost				Converter Lost		Conversor perdido			Conv perdido
84		32			15			Limpar Converter Lost Alarm		Limpar Conv Lost	Limpar Conversor perdido		Limpar C perdido
85		32			15			Limpar					Limpar			Limpar					Limpar
86		32			15			Confirm Converters Position		Confirm Pos		Confirmar posição Conversor		Confirma Pos C
87		32			15			Confirm					Confirm			Aceitar					Aceitar
92		32			15			Emergency Stop Function			E-Stop Function		Parada de Emergência			Parada Emergên.
102		32			15			Rated Voltage				Rated Voltage		Rated Voltage				Rated Voltage
103		32			15			Rated Current				Rated Current		Rated Current				Rated Current
104		32			15			All Rectifiers No Response		Rects No Resp		Nenhum Retif. Responde			Rets Não Resp.
108		32			15			Existence State				Existence State		Estado Presente				Est.Presente
109		32			15			Existent				Existent		Existente				Existente
110		32			15			Non-Existent				Non-Existent		Inexistente				Inexistente
111		32			15			Average Current				Average Current		Corrente media				Corrente media
112		32			15			Default Current Limit			Current Limit		Límite de Corrente por defecto		Lim Corrente
113		32			15			Default Output Voltage			Output Voltage		Tensão Saída (Padrão)		Tensão Saída
114		32			15			UnderVoltage				UnderVoltage		Subtensão				Subtensão
115		32			15			OverVoltage				OverVoltage		Sobretensão				Sobretensão
116		32			15			OverCurrent				OverCurrent		Sobrecorrente				Sobrecorrente
117		32			15			Average Voltage				Average Voltage		Tensão Media				Tensão Media
118		32			15			HVSD Limit				HVSD Limit		Limite HVSD				Limite HVSD
119		32			15			Default HVSD Pst			Default HVSD Pst	HVSD Padrão				HVSD Padrão
120		32			15			Limpar Converter Comm Failure		Limpar Comm Fail	Limpar Falha Comunicação Conv		Limpar Falha COM
121		32			15			HVSD					HVSD			Habilitar HVSD				HVSD
135		32			15			Current Limit Point			Curr Limit Pt		Límite de Corrente			Lím Corrente
136		32			15			Current Limit enabled			Current Limit		Límite de Corrente			Lím Corrente
137		32			15			Maximum Current Limit Value		Max Curr Limit		Límite Corrente máxima		Lím Corr Max
138		32			15			Default Current Limit Point		Def Curr Lmt Pt		Límite Corrente por defecto		Lim Corr defec
139		32			15			Min Current Limit Value			Min Curr Limit		Límite Corrente mínima		Lím Corr Min
290		32			15			OverCurrent				OverCurrent		Sobrecorrente				Sobrecorrente
293		32			15			Limpar All Converters Comm Fail		ClrAllCommFail		Limpar Falhas COM Conversor		Limpar Falha COM
294		32			15			All Converters Comm Status		All Conv Status		Estado Comunicação Conversores	Estado COM Conv
295		32			15			Converter Trim(24V)			Conv Trim(24V)		Control Tensão 24V			Control CC 24V
296		32			15			Last Conv Qty(Internal Use)		LastConvQty(P)		Uso interno				Uso Interno
297		32			15			Def Currt Lmt Pt(Internal Use)		DefCurrLmtPt(P)		Lim Corrente interno			Uso Interno
298		32			15			Converter Protect			Conv Protect		Conversor Protegido			Conv. Prot.
299		32			15			Input Rated Voltage			Input RatedVolt		Tensão Entrada Nominal			TensãoEntr.Nom.
300		32			15			Total Rated Current			Total RatedCurr		Corrente Entrada Nominal		Corr.Entr.Nom.
301		32			15			Converter Type				Conv Type		Tipo Conversor				Tipo Conv.
302		32			15			24-48V Conv				24-48V Conv		Conv. 24-48V				Conv. 24-48V
303		32			15			48-24V Conv				48-24V Conv		Conv. 48-24V				Conv. 48-24V
304		32			15			400-48V Conv				400-48V Conv		Conv. 400-48V				Conv. 400-48V
305		32			15			Total Output Power			Output Power		Potência de Saída Total		Pot. de Saída Total
306		32			15			Reset Converter IDs			Reset Conv IDs		Reset ID's Conversor			Reset ID's Conv.
307		32			15			Input Voltage			Input Voltage		Tensão de entrada			Tensão de entra
