﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			LVD 3 Unit				LVD 3 Unit		Unidade LVD 3				Unidade LVD3
11		32			15			Connected				Connected		Conectado				Conectado
12		32			15			Disconnected				Disconnected		Desconectado				Desconectado
13		32			15			No					No			Não					Não
14		32			15			Yes					Yes			Sim					Sim
21		32			15			LVD 3 Status				LVD 3 Status		Estado LVD 3				Estado LVD 3
22		32			15			LVD 2 Status				LVD 2 Status		Estado LVD 2				Estado LVD 2
23		32			15			LVD 3 Fail				LVD 3 Fail		Falha LVD 3				Falha LVD 3
24		32			15			LVD 2 Fail				LVD 2 Fail		Falha LVD 2				Falha LVD 2
25		32			15			Communication Fail			Comm Fail		Falha Comunicação			Falha COM
26		32			15			State					State			Estado					Estado
27		32			15			LVD 3 Control				LVD 3 Control		Controle LVD 3				Controle LVD 3
28		32			15			LVD 2 Control				LVD 2 Control		Controle LVD 2				Controle LVD 2
31		32			15			LVD 3					LVD 3			LVD 3					LVD 3
32		32			15			LVD 3 Mode				LVD 3 Mode		Modo LVD 3				Modo LVD 3
33		32			15			LVD 3 Voltage				LVD 3 Voltage		Tensão LVD 3				Tensão LVD 3
34		32			15			LVD 3 Reconnect Voltage			LVD3 Recon Volt		Tensão Reconexão LVD 3		TensãoReconLVD3
35		32			15			LVD 3 Reconnect Delay			LVD3 ReconDelay		Atraso Reconexão LVD 3			AtrasoRec.LVD3
36		32			15			LVD 3 Time				LVD 3 Time		Tempo LVD 3				Tempo LVD 3
37		32			15			LVD 3 Dependency			LVD3 Dependency		Dependência LVD 3			DependênciaLVD3
41		32			15			LVD 2					LVD 2			LVD 2					LVD 2
42		32			15			LVD 2 Mode				LVD 2 Mode		Modo LVD 2				Modo LVD 2
43		32			15			LVD 2 Voltage				LVD 2 Voltage		Tensão LVD 2				Tensão LVD 2
44		32			15			LVD 2 Reconnect Voltage			LVD2 Recon Volt		Tensão Reconexão LVD 2		TensãoReconLVD2
45		32			15			LVD 2 Reconnect Delay			LVD2 ReconDelay		Atraso Reconexão LVD 2			AtrasoReconLVD2
46		32			15			LVD 2 Time				LVD 2 Time		Tempo LVD 2				Tempo LVD 2
47		32			15			LVD 2 Dependency			LVD2 Dependency		Dependência LVD 2			DependênciaLVD2
51		32			15			Disabled				Disabled		Desabilitado				Desabilitado
52		32			15			Enabled					Enabled			Habilitado				Habilitado
53		32			15			Voltage					Voltage			Tensão					Tensão
54		32			15			Time					Time			Tempo					Tempo
55		32			15			None					None			Nenhum					Nenhum
56		32			15			LVD 1					LVD 1			LVD 1					LVD 1
57		32			15			LVD 2					LVD 2			LVD 2					LVD 2
103		32			15			High Temp Disconnect 3			HTD 3			Desconexão Alta Temperatura 3		Desc Alta Temp.3
104		32			15			High Temp Disconnect 2			HTD 2			Desconexão Alta Temperatura 2		Desc Alta Temp 2
105		32			15			Battery LVD				Battery LVD		LVD Bateria				LVD Bateria
106		32			15			No Battery				No Battery		Sem Bateria				Sem Bateria
107		32			15			LVD 3					LVD 3			LVD 3					LVD 3
108		32			15			LVD 2					LVD 2			LVD 2					LVD 2
109		32			15			Battery Always On			Batt Always On		Bateria Sempre ON			Bat. Sempre ON
110		32			15			LVD Contactor Type			LVD Type		Tipo Contator LVD			Tipo Cont. LV
111		32			15			Bistable				Bistable		Biestável				Biestável
112		32			15			Mono-Stable				Mono-Stable		Monoestável				Monoestável
113		32			15			Mono w/Sample				Mono w/Sample		Mono c/ Amostra				Mono c/Amostra
116		32			15			LVD 3 Disconnect			LVD3 Disconnect		Desconexão LVD 3			Desc. LVD 3
117		32			15			LVD 2 Disconnect			LVD2 Disconnect		Desconexão LVD 2			Desc. LVD 2
118		32			15			LVD 3 Mono w/Sample			LVD3 Mono Sampl		LVD 3 Mono c/ Amostra			LVD3 Mono c/Am.
119		32			15			LVD 2 Mono w/Sample			LVD2 Mono Sampl		LVD 2 Mono c/ Amostra			LVD2 Mono c/Am.
125		32			15			State					State			Estado					Estado
126		32			15			LVD 3 Voltage (24V)			LVD 3 Voltage		Tensão LVD 3 (24V)			TensãoLVD3(24V)
127		32			15			LVD 3 Reconnect Voltage (24V)		LVD3 Recon Volt		Tensão Reconexão LVD 3 (24V)		VReconLVD3(24V)
128		32			15			LVD 2 Voltage (24V)			LVD 2 Voltage		Tensão LVD 2 (24V)			TensãoLVD2(24V)
129		32			15			LVD 2 Reconnect Voltage (24V)		LVD2 Recon Volt		Tensão Reconexão LVD 2 (24V)		VReconLVD2(24V)
130		32			15			LVD 3					LVD 3			LVD 3					LVD 3
