﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			SMDU Group				SMDU Group		Grupo SMDU				Grupo SMDU
2		32			15			Standby					Standby			Alerta					Alerta
3		32			15			Refresh					Refresh			Atualizar				Atualizar
4		32			15			Refresh Setting				Refresh Setting		Configurar Atualização		Config Atualiz
5		32			15			Emergency-Stop Function			E-Stop			Função Parada de Emergencia		Parada Emegen
6		32			15			Yes					Yes			Sim					Sim
7		32			15			Existence State				Existence State		Detecção				Detecção
8		32			15			Existent				Existent		Existente				Existente
9		32			15			Non-Existent				Non-Existent		Inexistente				Inexistente
10		32			15			Number of SM-DUs			No of SM-DUs		Número de SMDUs			Núm SMDUs
11		32			15			SM-DU Config Changed			Cfg Changed		Config SMDU Auterado			Config auterado
12		32			15			Not Changed				Not Changed		Não Auterado				Não Auterado
13		32			15			Changed					Changed			Auterado				Auterado
