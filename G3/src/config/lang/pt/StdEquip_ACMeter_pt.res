﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Voltage L1-N				Volt L1-N		Tensão L1-N				Tensão L1-N
2		32			15			Voltage L2-N				Volt L2-N		Tensão L2-N				Tensão L2-N
3		32			15			Voltage L3-N				Volt L3-N		Tensão L3-N				Tensão L3-N
4		32			15			Voltage L1-L2				Volt L1-L2		Tensão L1-L2				Tensão L1-L2
5		32			15			Voltage L2-L3				Volt L2-L3		Tensão L2-L3				Tensão L2-L3
6		32			15			Voltage L3-L1				Volt L3-L1		Tensão L3-L1				Tensão L3-L1
7		32			15			Current L1				Curr L1			Corrente L1				Corr. L1
8		32			15			Current L2				Curr L2			Corrente L2				Corr. L2
9		32			15			Current L3				Curr L3			Corrente L3				Corr. L3
10		32			15			Watt L1					Watt L1			Watt L1					Watt L1
11		32			15			Watt L2					Watt L2			Watt L2					Watt L2
12		32			15			Watt L3					Watt L3			Watt L3					Watt L3
13		32			15			VA L1					VA L1			VA L1					VA L1
14		32			15			VA L2					VA L2			VA L2					VA L2
15		32			15			VA L3					VA L3			VA L3					VA L3
16		32			15			VAR L1					VAR L1			VAR L1					VAR L1
17		32			15			VAR L2					VAR L2			VAR L2					VAR L2
18		32			15			VAR L3					VAR L3			VAR L3					VAR L3
19		32			15			AC Frequency				AC Frequency		Frequência CA				Frequência CA
20		32			15			Communication State			Comm State		Estado Comunicaτão			Estado COM.
21		32			15			Existence State				Existence State		Estado Presente				Estado Presente
22		32			15			AC Meter				AC Meter		Mediτão CA				Mediτão CA
23		32			15			On					On			On					On
24		32			15			Off					Off			Off					Off
25		32			15			Existent				Existent		Presente				Presente
26		32			15			Not Existent				Not Existent		Ausente					Ausente
27		32			15			Communication Fail			Comm Fail		Falha Comunicaτão			Falha COM.
28		32			15			V L-N ACC				V L-N ACC		V L-N ACC				V L-N ACC
29		32			15			V L-L ACC				V L-L ACC		V L-L ACC				V L-L ACC
30		32			15			Watt ACC				Watt ACC		Watt ACC				Watt ACC
31		32			15			VA ACC					VA ACC			VA ACC					VA ACC
32		32			15			VAR ACC					VAR ACC			VAR ACC					VAR ACC
33		32			15			DMD Watt ACC				DMD Watt ACC		DMD Watt ACC				DMD Watt ACC
34		32			15			DMD VA ACC				DMD VA ACC		DMD VA ACC				DMD VA ACC
35		32			15			PF L1					PF L1			PF L1					PF L1
36		32			15			PF L2					PF L2			PF L2					PF L2
37		32			15			PF L3					PF L3			PF L3					PF L3
38		32			15			PF ACC					PF ACC			PF ACC					PF ACC
39		32			15			Phase Sequence				Phase Sequence		Sequência Fase				Sequência Fase
40		32			15			L1-L2-L3				L1-L2-L3		L1-L2-L3				L1-L2-L3
41		32			15			L1-L3-L2				L1-L3-L2		L1-L3-L2				L1-L3-L2
42		32			15			Nominal Line Voltage			NominalLineVolt		Tensão Linha Nominal			V Linha Nominal
43		32			15			Nominal Phase Voltage			Nominal PH-Volt		Tensão Fase Nominal			V Fase Nominal
44		32			15			Nominal Frequency			Nominal Freq		Frequência Nominal			Freq. Nominal
45		32			15			Mains Failure Alarm Limit 1		Mains Fail Alm1		Alarme Falha Rede: Limite 1		FalhaRede:Alm.1
46		32			15			Mains Failure Alarm Limit 2		Mains Fail Alm2		Alarme Falha Rede: Limite 2		FalhaRede:Alm.2
47		32			15			Frequency Alarm Limit			Freq Alarm Lmt		Alarme Frequência Limite		Alm.Freq Limite
48		32			15			Current Alarm Limit			Curr Alm Limit		Alarme Corrente Limite			Alm.Corr.Limite
51		32			15			Line AB Over Voltage 1			L-AB Over Volt1		Sobretensão 1 Linha AB			Sobretens1 L-AB
52		32			15			Line AB Over Voltage 2			L-AB Over Volt2		Sobretensão 2 Linha AB			Sobretens2 L-AB
53		32			15			Line AB Under Voltage 1			L-AB UnderVolt1		Subtensão 1 Linha AB			Subtensão1 L-AB
54		32			15			Line AB Under Voltage 2			L-AB UnderVolt2		Subtensão 2 Linha AB			Subtensão2 L-AB
55		32			15			Line BC Over Voltage 1			L-BC Over Volt1		Sobretensão 1 Linha BC			Sobretens1 L-BC
56		32			15			Line BC Over Voltage 2			L-BC Over Volt2		Sobretensão 2 Linha BC			Sobretens2 L-BC
57		32			15			Line BC Under Voltage 1			L-BC UnderVolt1		Subtensão 1 Linha BC			Subtensão1 L-BC
58		32			15			Line BC Under Voltage 2			L-BC UnderVolt2		Subtensão 2 Linha BC			Subtensão2 L-BC
59		32			15			Line CA Over Voltage 1			L-CA Over Volt1		Sobretensão 1 Linha CA			Sobretens1 L-CA
60		32			15			Line CA Over Voltage 2			L-CA Over Volt2		Sobretensão 2 Linha CA			Sobretens2 L-CA
61		32			15			Line CA Under Voltage 1			L-CA UnderVolt1		Subtensão 1 Linha CA			Subtensão1 L-CA
62		32			15			Line CA Under Voltage 2			L-CA UnderVolt2		Subtensão 2 Linha CA			Subtensão2 L-CA
63		32			15			Phase A Over Voltage 1			PH-A Over Volt1		Sobretensão 1: Fase A			Sobretens1 PH-A
64		32			15			Phase A Over Voltage 2			PH-A Over Volt2		Sobretensão 2: Fase A			Sobretens2 PH-A
65		32			15			Phase A Under Voltage 1			PH-A UnderVolt1		Subtensão 1: Fase A			Subtensão1 PH-A
66		32			15			Phase A Under Voltage 2			PH-A UnderVolt2		Subtensão 2: Fase A			Subtensão2 PH-A
67		32			15			Phase B Over Voltage 1			PH-B Over Volt1		Sobretensão 1: Fase B			Sobretens1 PH-B
68		32			15			Phase B Over Voltage 2			PH-B Over Volt2		Sobretensão 2: Fase B			Sobretens2 PH-B
69		32			15			Phase B Under Voltage 1			PH-B UnderVolt1		Subtensão 1: Fase B			Subtensão1 PH-B
70		32			15			Phase B Under Voltage 2			PH-B UnderVolt2		Subtensão 2: Fase B			Subtensão2 PH-B
71		32			15			Phase C Over Voltage 1			PH-C Over Volt1		Sobretensão 1: Fase C			Sobretens1 PH-C
72		32			15			Phase C Over Voltage 2			PH-C Over Volt2		Sobretensão 2: Fase C			Sobretens2 PH-C
73		32			15			Phase C Under Voltage 1			PH-C UnderVolt1		Subtensão 1: Fase C			Subtensão1 PH-C
74		32			15			Phase C Under Voltage 2			PH-C UnderVolt2		Subtensão 2: Fase C			Subtensão2 PH-C
75		32			15			Mains Failure				Mains Failure		Falha de Rede				Falha de Rede
76		32			15			Severe Mains Failure			SevereMainsFail		Falha Severa de Rede			FalhaRedeSevera
77		32			15			High Frequency				High Frequency		Alta Frequência			Alta Frequência
78		32			15			Low Frequency				Low Frequency		Baixa Frequência			BaixaFrequência
79		32			15			Phase A High Current			PH-A High Curr		Alta Corrente Fase A			Alta Corr. PH-A
80		32			15			Phase B High Current			PH-B High Curr		Alta Corrente Fase B			Alta Corr. PH-B
81		32			15			Phase C High Current			PH-C High Curr		Alta Corrente Fase C			Alta Corr. PH-C
82		32			15			DMD W MAX				DMD W MAX		DMD W MAX				DMD W MAX
83		32			15			DMD VA MAX				DMD VA MAX		DMD VA MAX				DMD VA MAX
84		32			15			DMD A MAX				DMD A MAX		DMD A MAX				DMD A MAX
85		32			15			KWH(+) TOT				KWH(+) TOT		KWH(+) TOT				KWH(+) TOT
86		32			15			KVARH(+) TOT				KVARH(+) TOT		KVARH(+) TOT				KVARH(+) TOT
87		32			15			KWH(+) PAR				KWH(+) PAR		KWH(+) PAR				KWH(+) PAR
88		32			15			KVARH(+) PAR				KVARH(+) PAR		KVARH(+) PAR				KVARH(+) PAR
89		32			15			KWH(+) L1				KWH(+) L1		KWH(+) L1				KWH(+) L1
90		32			15			KWH(+) L2				KWH(+) L2		KWH(+) L2				KWH(+) L2
91		32			15			KWH(+) L3				KWH(+) L3		KWH(+) L3				KWH(+) L3
92		32			15			KWH(+) T1				KWH(+) T1		KWH(+) T1				KWH(+) T1
93		32			15			KWH(+) T2				KWH(+) T2		KWH(+) T2				KWH(+) T2
94		32			15			KWH(+) T3				KWH(+) T3		KWH(+) T3				KWH(+) T3
95		32			15			KWH(+) T4				KWH(+) T4		KWH(+) T4				KWH(+) T4
96		32			15			KVARH(+) T1				KVARH(+) T1		KVARH(+) T1				KVARH(+) T1
97		32			15			KVARH(+) T2				KVARH(+) T2		KVARH(+) T2				KVARH(+) T2
98		32			15			KVARH(+) T3				KVARH(+) T3		KVARH(+) T3				KVARH(+) T3
99		32			15			KVARH(+) T4				KVARH(+) T4		KVARH(+) T4				KVARH(+) T4
100		32			15			KWH(-) TOT				KWH(-) TOT		KWH(-) TOT				KWH(-) TOT
101		32			15			KVARH(-) TOT				KVARH(-) TOT		KVARH(-) TOT				KVARH(-) TOT
102		32			15			HOUR					HOUR			HORA					HORA
103		32			15			COUNTER 1				COUNTER 1		CONTADOR 1				CONTADOR 1
104		32			15			COUNTER 2				COUNTER 2		CONTADOR 2				CONTADOR 2
105		32			15			COUNTER 3				COUNTER 3		CONTADOR 3				CONTADOR 3
