﻿#
# Locale language support:  Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Phase A Voltage				Phase Volt A		Tensão fase R				Tensão R
2		32			15			Phase B Voltage				Phase Volt B		Tensão fase S				Tensão S
3		32			15			Phase C Voltage				Phase Volt C		Tensão fase T				Tensão T
4		32			15			Line Voltage AB				Line Volt AB		Tensão R-S				Tensão R-S
5		32			15			Line Voltage BC				Line Volt BC		Tensão S-T				Tensão S-T
6		32			15			Line Voltage CA				Line Volt CA		Tensão R-T				Tensão R-T
7		32			15			Phase A Current				Phase Curr A		Corrente fase R				Corrente R
8		32			15			Phase B Current				Phase Curr B		Corrente fase S				Corrente S
9		32			15			Phase C Current				Phase Curr C		Corrente fase T				Corrente T
10		32			15			Frequency				AC Frequency		Frequência				Frequência
11		32			15			Total Real Power			Total RealPower		Potência real total			Potência real
12		32			15			Phase A Real Power			Real Power A		Potência real fase R			Pot real R
13		32			15			Phase B Real Power			Real Power B		Potência real fase S			Pot real S
14		32			15			Phase C Real Power			Real Power C		Potência real fase T			Pot real T
15		32			15			Total Reactive Power			Tot React Power		Potência reativa total			Pot reativa
16		32			15			Phase A Reactive Power			React Power A		Potência reativa fase R		Pot reativa R
17		32			15			Phase B Reactive Power			React Power B		Potência reativa fase S		Pot reativa S
18		32			15			Phase C Reactive Power			React Power C		Potência reativa fase T		Pot reativa T
19		32			15			Total Apparent Power			Total App Power		Potência aparente total		Pot aparente
20		32			15			Phase A Apparent Power			App Power A		Potência aparente fase R		Pot aparente R
21		32			15			Phase B Apparent Power			App Power B		Potência aparente fase S		Pot aparente S
22		32			15			Phase C Apparent Power			App Power C		Potência aparente fase T		Pot aparente T
23		32			15			Power Fotor				Power Fotor		Fotor de Potência			Fotor Potência
24		32			15			Phase A Power Fotor			Power Fotor A		Fotor de Potência fase R		Fotor pot R
25		32			15			Phase B Power Fotor			Power Fotor B		Fotor de Potência fase S		Fotor pot S
26		32			15			Phase C Power Fotor			Power Fotor C		Fotor de Potência fase T		Fotor pot T
27		32			15			Phase A Current Crest Fotor		Ia Crest Fotor		Fotor cresta Corrente R			Fat cresta IR
28		32			15			Phase B Current Crest Fotor		Ib Crest Fotor		Fotor cresta Corrente S			Fat cresta IS
29		32			15			Phase C Current Crest Fotor		Ic Crest Fotor		Fotor cresta Corrente T			Fat cresta IT
30		32			15			Phase A Current THD			Current THD A		THD Corrente fase R			THD I fase R
31		32			15			Phase B Current THD			Current THD B		THD Corrente fase S			THD I fase S
32		32			15			Phase C Current THD			Current THD C		THD Corrente fase T			THD I fase T
33		32			15			Phase A Voltage THD			Voltage THD A		THD Tensão fase R			THD V fase R
34		32			15			Phase B Voltage THD			Voltage THD B		THD Tensão fase S			THD V fase S
35		32			15			Phase C Voltage THD			Voltage THD C		THD Tensão fase T			THD V fase T
36		32			15			Total Real Energy			Tot Real Energy		Energía Real total			Energía Real
37		32			15			Total Reactive Energy			Tot ReactEnergy		Energía reativa total			Energ reativa
38		32			15			Total Apparent Energy			Tot App Energy		Energía Aparente total			Energ Aparente
39		32			15			Ambient Temperature			Ambient Temp		Temperatura ambiente			Temp ambiente
40		32			15			Nominal Line Voltage			Nom LineVolt		Tensão nominal sistema			Tensão nominal
41		32			15			Nominal Phase Voltage			Nom PhaseVolt		Tensão nominal de fase			Tens Nom fase
42		32			15			Nominal Frequency			Nom Frequency		Frequência Nominal			Frequência Nom
43		32			15			Mains Failure Alarm Threshold 1		MFA Threshold 1		Limite alarme Falha Red			Lim Falha Red
44		32			15			Mains Failure Alarm Threshold 2		MFA Threshold 2		Limite alarme Falha Red Sev		Falha Red Sev
45		32			15			Voltage Alarm Threshold 1		Volt AlmThresh1		Limite alarme Tensão 1			Umb alarme V1
46		32			15			Voltage Alarm Threshold 2		Volt AlmThresh2		Limite alarme Tensão 2			Umb alarme V2
47		32			15			Frequency Alarm Threshold		Freq AlarmThres		Limite alarme Frequência		Umb alarm frec
48		32			15			High Temperature Limit			High Temp Limit		Límite Alta Temperatura		Lim alta temp
49		32			15			Low Temperature Limit			Low Temp Limit		Límite Baixa Temperatura		Lim Baixa temp
50		32			15			Supervision Fail			Supervision Fail	Falha supervisão			Falha Com SM
51		32			15			High Line Voltage AB			High L-Volt AB		Alta Tensão R-S			Alta Tens R-S
52		32			15			Very High Line Voltage AB		VHigh L-Volt AB		Mui alta Tensão R-S			Mui alta V R-S
53		32			15			Low Line Voltage AB			Low L-Volt AB		Baixa Tensão R-S			Baixa Tens R-S
54		32			15			Very Low Line Voltage AB		VLow L-Volt AB		Mui Baixa Tensão R-S			Mui Baixa V R-S
55		32			15			High Line Voltage BC			High L-Volt BC		Alta Tensão S-T			Alta Tens S-T
56		32			15			Very High Line Voltage BC		VHigh L-Volt BC		Mui alta Tensão S-T			Mui alta V S-T
57		32			15			Low Line Voltage BC			Low L-Volt BC		Baixa Tensão S-T			Baixa Tens S-T
58		32			15			Very Low Line Voltage BC		VLow L-Volt BC		Mui Baixa Tensão S-T			Mui Baixa V S-T
59		32			15			High Line Voltage CA			High L-Volt CA		Alta Tensão R-T			Alta Tens R-T
60		32			15			Very High Line Voltage CA		VHigh L-Volt CA		Mui alta Tensão R-T			Mui alta V R-T
61		32			15			Low Line Voltage CA			Low L-Volt CA		Baixa Tensão R-T			Baixa Tens R-T
62		32			15			Very Low Line Voltage CA		VLow L-Volt CA		Mui Baixa Tensão R-T			Mui Baixa V R-T
63		32			15			High Phase Voltage A			High Ph-Volt A		Alta Tensão fase R			Alta Tensão R
64		32			15			Very High Phase Voltage A		VHigh Ph-Volt A		Mui alta Tensão fase R			Mui alta Tens R
65		32			15			Low Phase Voltage A			Low Ph-Volt A		Baixa Tensão fase R			Baixa Tensão R
66		32			15			Very Low Phase Voltage A		VLow Ph-Volt A		Mui Baixa Tensão fase R		Mui Baixa Tens R
67		32			15			High Phase Voltage B			High Ph-Volt B		Alta Tensão fase S			Alta Tensão S
68		32			15			Very High Phase Voltage B		VHigh Ph-Volt B		Mui alta Tensão fase S			Mui alta Tens S
69		32			15			Low Phase Voltage B			Low Ph-Volt B		Baixa Tensão fase S			Baixa Tensão S
70		32			15			Very Low Phase Voltage B		VLow Ph-Volt B		Mui Baixa Tensão fase S		Mui Baixa Tens S
71		32			15			High Phase Voltage C			High Ph-Volt C		Alta Tensão fase T			Alta Tensão T
72		32			15			Very High Phase Voltage C		VHigh Ph-Volt C		Mui alta Tensão fase T			Mui alta Tens T
73		32			15			Low Phase Voltage C			Low Ph-Volt C		Baixa Tensão fase T			Baixa Tensão T
74		32			15			Very Low Phase Voltage C		VLow Ph-Volt C		Mui Baixa Tensão fase T		Mui Baixa Tens T
75		32			15			Mains Failure				Mains Failure		Falha de Red				Falha de Red
76		32			15			Severe Mains Failure			Severe MainFail		Falha de Red Severo			Falha Red Sev
77		32			15			High Frequency				High Frequency		Alta Frequência			Alta Frequência
78		32			15			Low Frequency				Low Frequency		Baixa Frequência			Baixa Frequência
79		32			15			High Temperature			High Temp		Alta temperatura			Alta temp
80		32			15			Low Temperature				Low Temperature		Baixa temperatura			Baixa temp
81		32			15			SMAC Unit				SMAC Unit		SMAC					SMAC
82		32			15			Supervision Fail			SMAC Fail		Falha de supervisão			Falha sup SMAC
83		32			15			No					No			Não					Não
84		32			15			Yes					Yes			Sim					Sim
85		32			15			Phase A Mains Failure Counter		A MainsFail Cnt		Contador Falhas fase R			Cont Falhas R
86		32			15			Phase B Mains Failure Counter		B MainsFail Cnt		Contador Falhas fase S			Cont Falhas S
87		32			15			Phase C Mains Failure Counter		C MainsFail Cnt		Contador Falhas fase T			Cont Falhas T
88		32			15			Frequency Failure Counter		Freq Fail Cnt		Contador Falhas Frequência		Cont FalhasFrec
89		32			15			Reset Phase A Mains Fail Counter	Reset A FailCnt		Iniciar cont Falhas fase R		Inic Falhas R
90		32			15			Reset Phase B Mains Fail Counter	Reset B FailCnt		Iniciar cont Falhas fase S		Inic Falhas S
91		32			15			Reset Phase C Mains Fail Counter	Reset C FailCnt		Iniciar cont Falhas fase T		Inic Falhas T
92		32			15			Reset Frequency Counter			Reset F FailCnt		Iniciar cont Falhas Frequência		Inic FalhasFrec
93		32			15			Current Alarm Threshold			Curr Alarm Lim		alarme alta Corrente			Alta Corrente
94		32			15			Phase A High Current			A High Current		Alta Corrente fase R			Alta I fase R
95		32			15			Phase B High Current			B High Current		Alta Corrente fase S			Alta I fase S
96		32			15			Phase C High Current			C High Current		Alta Corrente fase T			Alta I fase T
97		32			15			State					State			Estado					Estado
98		32			15			Off					Off			apagado					apagado
99		32			15			On					On			conectado				conectado
100		32			15			System Power				System Power		Potência del Sistema			Potência Sistem
101		32			15			Total System Power Consumption		Power Consump		Consumo total Sistema			Consumo total
102		32			15			Existence State				Existence State		Estado Presente				Est.Presente
103		32			15			Existent				Existent		Existente				Existente
104		32			15			Non-Existent				Non-Existent		Inexistente				Inexistente
500		32			15			Temperature Sensor Enabled		Temp Sensor		Sensor temperatura			Sensor Temp
501		32			15			No					No			Não					Não
502		32			15			Yes					Yes			Sim					Sim
