#
#  Locale language support:
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
pt


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			IncellBMS				IncesllBMS		IncellBMS			IncellBMS
2	32			15			Rated Capacity				Rated Capacity		Rated Capacity				Rated Capacity
3	32			15			Communication Fail			Comm Fail		Communication Fail			Comm Fail
4	32			15			Existence State				Existence State		Existence State				Existence State
5	32			15			Existent				Existent		Existent				Existent
6	32			15			Not Existent				Not Existent		Not Existent				Not Existent
7	32			15			Battery Voltage				Batt Voltage		Battery Voltage				Batt Voltage
8	32			15			Battery Current				Batt Current		Battery Current				Batt Current
9	32			15			Battery Rating (Ah)			Batt Rating(Ah)		Battery Rating (Ah)			Batt Rating(Ah)
10	32			15			Battery Capacity (%)			Batt Cap (%)		Battery Capacity (%)			Batt Cap (%)
11	32			15			Minimum Cell Temperature	Min Cell Temp			Minimum Cell Temperature	Min Cell Temp
12	32			15			Maximum Cell Temperature	Max Cell Temp		Maximum Cell Temperature	Max Cell Temp
13	32			15			Minimum Cell Voltage		Min Cell Volt		Minimum Cell Voltage		Min Cell Volt
14	32			15			Maximum Cell Voltage		Max Cell Volt		Maximum Cell Voltage		Max Cell Volt
15	32			15			Nominal Capacity(Ah)		Nominal Cap(Ah)		Nominal Capacity(Ah)		Nominal Cap(Ah)
16	32			15			Lifetime					Lifetime		Lifetime					Lifetime
17	32			15			Cycle Count					Cycle Count		Cycle Count					Cycle Count
29	32			15			Yes					Yes			Yes					Yes
30	32			15			No					No			No					No
31	32			15			Circuit Breaker Alarm		Circuit Breaker		Circuit Breaker Alarm		Circuit Breaker
32	32			15			Antitheft Alarm				Antitheft		Antitheft Alarm				Antitheft
33	32			15			Short circuit Alarm		Short Circuit		Short circuit Alarm		Short Circuit
34	32			15			Battery Under Voltage Alarm		BattUnderVolt		Battery Under Voltage Alarm		BattUnderVolt
35	32			15			Battery Over Voltage Alarm		BattOverVolt		Battery Over Voltage Alarm		BattOverVolt
36	32			15			Charge Over Current Alarm		ChargeOverCurr		Charge Over Current Alarm		ChargeOverCurr
37	32			15			Discharge Over Current Alarm		DisCharOverCurr		Discharge Over Current Alarm		DisCharOverCurr
38	32			15			High Battery Temperature Alarm		HighBattTemp		High Battery Temperature Alarm		HighBattTemp
39	32			15			Low Battery Temperature Alarm		LowBattTemp		Low Battery Temperature Alarm		LowBattTemp
40	32			15			Discharge Blocked Alarm		Dish Blocked		Discharge Blocked Alarm		Dish Blocked
41	32			15			Charge Blocked Alarm		Charge Blocked		Charge Blocked Alarm		Charge Blocked
60	32			15			Communication Address			CommAddress		Communication Address			CommAddress
