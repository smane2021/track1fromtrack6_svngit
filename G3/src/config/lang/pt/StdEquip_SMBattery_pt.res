﻿#
# Locale language support:  Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Current				Batt Current		Corrente de Bateria			Corrente de Bat
2		32			15			Battery Capacity			Batt Capacity		Capacidade Baterias			Capacidade Bat
3		32			15			Battery Voltage				Batt Voltage		Tensão de Baterias			Tensão Bat
4		32			15			Ambient Temperature			Ambient Temp		Temperatura ambiente			Temp Ambiente
5		32			15			Acid Temperature			Acid Temp		Temperatura de ácido			Temp ácido
6		32			15			Total Time of Batt Temp GT 30C	ToT GT 30C		Tiempo total bat t maior 30C		Bat maior 30C
7		32			15			Total Time of Batt Temp LT 10C	ToT LT 10C		Tiempo total bat t menor 10C		Bat menor 10C
8		32			15			Battery Block 1 Voltage			Block 1 Volt		Tensão de celula 1			Tens celula 1
9		32			15			Battery Block 2 Voltage			Block 2 Volt		Tensão de celula 2			Tens celula 2
10		32			15			Battery Block 3 Voltage			Block 3 Volt		Tensão de celula 3			Tens celula 3
11		32			15			Battery Block 4 Voltage			Block 4 Volt		Tensão de celula 4			Tens celula 4
12		32			15			Battery Block 5 Voltage			Block 5 Volt		Tensão de celula 5			Tens celula 5
13		32			15			Battery Block 6 Voltage			Block 6 Volt		Tensão de celula 6			Tens celula 6
14		32			15			Battery Block 7 Voltage			Block 7 Volt		Tensão de celula 7			Tens celula 7
15		32			15			Battery Block 8 Voltage			Block 8 Volt		Tensão de celula 8			Tens celula 8
16		32			15			Battery Block 9 Voltage			Block 9 Volt		Tensão de celula 9			Tens celula 9
17		32			15			Battery Block 10 Voltage		Block 10 Volt		Tensão de celula 10			Tens celula 10
18		32			15			Battery Block 11 Voltage		Block 11 Volt		Tensão de celula 11			Tens celula 11
19		32			15			Battery Block 12 Voltage		Block 12 Volt		Tensão de celula 12			Tens celula 12
20		32			15			Battery Block 13 Voltage		Block 13 Volt		Tensão de celula 13			Tens celula 13
21		32			15			Battery Block 14 Voltage		Block 14 Volt		Tensão de celula 14			Tens celula 14
22		32			15			Battery Block 15 Voltage		Block 15 Volt		Tensão de celula 15			Tens celula 15
23		32			15			Battery Block 16 Voltage		Block 16 Volt		Tensão de celula 16			Tens celula 16
24		32			15			Battery Block 17 Voltage		Block 17 Volt		Tensão de celula 17			Tens celula 17
25		32			15			Battery Block 18 Voltage		Block 18 Volt		Tensão de celula 18			Tens celula 18
26		32			15			Battery Block 19 Voltage		Block 19 Volt		Tensão de celula 19			Tens celula 19
27		32			15			Battery Block 20 Voltage		Block 20 Volt		Tensão de celula 20			Tens celula 20
28		32			15			Battery Block 21 Voltage		Block 21 Volt		Tensão de celula 21			Tens celula 21
29		32			15			Battery Block 22 Voltage		Block 22 Volt		Tensão de celula 22			Tens celula 22
30		32			15			Battery Block 23 Voltage		Block 23 Volt		Tensão de celula 23			Tens celula 23
31		32			15			Battery Block 24 Voltage		Block 24 Volt		Tensão de celula 24			Tens celula 24
32		32			15			Battery Block 25 Voltage		Block 25 Volt		Tensão de celula 25			Tens celula 25
33		32			15			Shunt Voltage				Shunt Voltage		Tensão Shunt				Tensão Shunt
34		32			15			Battery Leakage				Batt Leakage		Fuga de electrolito			Fuga bateria
35		32			15			Low Acid Level				Low Acid Level		Nivel de ácido baixo			Nivel bx acid
36		32			15			Battery Disconnected			Battery Discon		Bateria desconectada			Bat desconec
37		32			15			High Battery Temperature		High Batt Temp		Alta temperatura de Bateria		Alta temp bat
38		32			15			Low Battery Temperature			Low Batt Temp		Baixa temperatura de Bateria		Baixa temp bat
39		32			15			Block Voltage Difference		Block Volt Diff		Diferencia Tensão de celula		Dif tens elem
40		32			15			Battery Shunt Size			Batt Shunt Size		Shunt de Bateria			Shunt bat
41		32			15			Block Voltage difference		Block Volt Diff		Diferencia Tensão de celula		Dif tens elem
42		32			15			High Battery Temp Limit					High Temp Limit		Límite alta temperatura	bat	Lim alta temp
43		32			15			High Battery Temp Limit Hyst		High Temp Hyst		Histéresis lim alta temp bat		Hist alta temp
44		32			15			Low Battery Temp Limit			Low Temp Limit		Límite Baixa temperatura bat		Lim Baixa temp
45		32			15			Low Battery Temp Limit Hyst		Low Temp Hyst		Histéresis lim Baixa temp bat		Hist Baixa temp
46		32			15			Current Limit Exceeded			Over Curr Limit		Límite de corriente pasado		Lim corr pasdo
47		32			15			Battery Leakage				Battery Leakage		Fuga de electrolito			Fuga bateria
48		32			15			Low Acid Level				Low Acid Level		Nivel de ácido baixo			Nivel bjo acid
49		32			15			Battery Disconnected			Battery Discon		Bateria desconectada			Bat desconec
50		32			15			High Battery Temperature		High Batt Temp		Alta temperatura Bateria		Alta temp bat
51		32			15			Low Battery Temperature			Low Batt Temp		Baixa temperatura Bateria		Baixa temp bat
52		32			15			Cell Voltage Difference			Cell Volt Diff		Diferencia Tensão de celula		Dif tens elem
53		32			15			SM-BAT Unit Fail			SM-BAT Fail		Falha unidad SM				Falha SM-BAT
54		32			15			Battery Disconnected			Battery Discon		Bateria desconectada			Bat desconect
55		32			15			No					No			Não					Não
56		32			15			Yes					Yes			Sim					Sim
57		32			15			No					No			Não					Não
58		32			15			Yes					Yes			Sim					Sim
59		32			15			No					No			Não					Não
60		32			15			Yes					Yes			Sim					Sim
61		32			15			No					No			Não					Não
62		32			15			Yes					Yes			Sim					Sim
63		32			15			No					No			Não					Não
64		32			15			Yes					Yes			Sim					Sim
65		32			15			No					No			Não					Não
66		32			15			Yes					Yes			Sim					Sim
67		32			15			No					No			Não					Não
68		32			15			Yes					Yes			Sim					Sim
69		32			15			SM Battery				SM Batt			SM BAT					SM Bat
70		32			15			Over Battery Current			Ov Batt Current		SobreCorrente de Bateria		Sobrecorr bat
71		32			15			Battery Capacity(%)			Batt Cap(%)		Capacidad Bateria (%)			Cap bat(%)
72		32			15			SMBAT Fail				SMBAT Fail		Falha SMBAT				Falha SMBAT
73		32			15			No					No			Não					Não
74		32			15			Yes					Yes			Sim					Sim
75		32			15			AI 4					AI 4			AI 4					AI 4
76		32			15			AI 7					AI 7			AI 7					AI 7
77		32			15			DI 4					DI 4			DI 4					DI 4
78		32			15			DI 5					DI 5			DI 5					DI 5
79		32			15			DI 6					DI 6			DI 6					DI 6
80		32			15			DI 7					DI 7			DI 7					DI 7
81		32			15			DI 8					DI 8			DI 8					DI 8
82		32			15			Relay 1 Status				Relay 1 Status		Estado relé 1				Estado Rel 1
83		32			15			Relay 2 Status				Relay 2 Status		Estado relé 2				Estado Rel 2
84		32			15			No					No			Não					Não
85		32			15			Yes					Yes			Sim					Sim
86		32			15			No					No			Não					Não
87		32			15			Yes					Yes			Sim					Sim
88		32			15			No					No			Não					Não
89		32			15			Yes					Yes			Sim					Sim
90		32			15			No					No			Não					Não
91		32			15			Yes					Yes			Sim					Sim
92		32			15			No					No			Não					Não
93		32			15			Yes					Yes			Sim					Sim
94		32			15			Off					Off			Apagado					Apagado
95		32			15			On					On			Conectado				Conectado
96		32			15			Off					Off			Apagado					Apagado
97		32			15			On					On			Conectado				Conectado
98		32			15			Relay 1 On/Off				Relay1 On/Off		Ligar/Desl. Relé 1			Ligar/Desl. Relé 1
99		32			15			Relay 2 On/Off				Relay2 On/Off		Ligar/Desl. Relé 2			Ligar/Desl. Relé 2
100		32			15			AI 2					AI 2			AI 2					AI 2
101		32			15			Battery Temperature Sensor Fail		T Sensor Fail		Falha Sensor de Temperatura		Falha sensTemp
102		32			15			Low Capacity				Low Capacity		Baixa capacidad				Baixa capacidad
103		32			15			Existence State				Existence State		Detecção				Detecção
104		32			15			Existent				Existent		Existente				Existente
105		32			15			Non-Existent				Non-Existent		Inexistente				Inexistente
106		32			15			Battery Not Responding			Not Responding		Bateria no responde			No responde
107		32			15			Response				Response		Resposta				Resposta
108		32			15			Not Responding				Not Responding		Não responde				Não responde
109		32			15			Rated Capacity				Rated Capacity		Capacidad estimada			Capacidad Est
110		32			15			Battery Management			Batt Management		Gerenciamento Bateria			Gerenciam.Bat.
111		32			15			SM-BAT High Temperature Limit		SMBATHiTempLim		SMBAT Alta temperatura Bateria		SM Alta tempBat
112		32			15			SM-BAT Low Temperature Limit		SMBATLoTempLim		SMBAT Baixa temperatura Bateria		SM Baixa tempBat
113		32			15			SM-BAT Enable Temperature Sensor	SMBATTempEnable		Sensor Temperatura			Sensor Temp
114		32			15			Battery Not Responding			Not Responding		SMBAT Não responde			Não responde
115		32			15			Temperature Sensor not Used		TempSens Unused		Sensor Temp não utilizado		Sim sensor Temp
116		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		Bateria Alarme Desequilíbrio Corr		BatDesequilCorr
