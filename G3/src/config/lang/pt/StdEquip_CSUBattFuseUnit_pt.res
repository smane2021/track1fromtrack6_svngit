﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Fuse Tripped			Batt Fuse Trip		Fusílvel Bateria disparado		FusBat disp
2		32			15			Battery 2 fuse tripped			Batt2 Fuse Trip		Fusível Bateria 2 disparado		FusBat2 disp
3		32			15			Battery 1 Voltage			Batt 1 Voltage		Tensão Bateria 1			Tensão Bat1
4		32			15			Battery 1 Current			Batt 1 Current		Corrente Bateria 1			Corrente Bat1
5		32			15			Battery 1 Temperature			Batt 1 Temp		Temperatura Bateria 1			Temp Bat1
6		32			15			Battery 2 Voltage			Batt 2 Voltage		Tensão Bateria 2			Tensão Bat2
7		32			15			Battery 2 Current			Batt 2 Current		Corrente Bateria 2			Corrente Bat2
8		32			15			Battery 2 Temperature			Batt 2 Temp		Temperatura Bateria 2			Temp Bat2
9		32			15			CSU Battery				CSU Battery		Bateria CSU				Bateria CSU
10		32			15			CSU Battery Failure			CSU BattFailure		Falha Bateria CSU			Falha Bat CSU
11		32			15			No					No			Não					Não
12		32			15			Yes					Yes			Sim					Sim
13		32			15			Battery 2 Connected			Batt2 Connected		Bateria 2 conectada			Bat2 conectada
14		32			15			Battery 1 Connected			Batt1 Connected		Bateria 1 conectada			Bat1 conectada
15		32			15			Existent				Existent		Existente				Existente
16		32			15			Non-Existent				Non-Existent		Inexistente				Inexistente
