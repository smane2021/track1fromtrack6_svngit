﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Phase A Voltage				Phase A Volt		Tensão Fase R				Tensão Fase R
2		32			15			Phase B Voltage				Phase B Volt		Tensão Fase S				Tensão Fase S
3		32			15			Phase C Voltage				Phase C Volt		Tensão Fase T				Tensão Fase T
4		32			15			Line Voltage AB				Line Volt AB		Tensão R-S				Tensão R-S
5		32			15			Line Voltage BC				Line Volt BC		Tensão S-T				Tensão S-T
6		32			15			Line Voltage CA				Line Volt CA		Tensão R-T				Tensão R-T
7		32			15			Phase A Current				Phase Curr A		Corrente Fase R				Corrente R
8		32			15			Phase B Current				Phase Curr B		Corrente Fase S				Corrente S
9		32			15			Phase C Current				Phase Curr C		Corrente Fase T				Corrente T
10		32			15			Frequency				AC Frequency		Frequencia CA				Frequencia CA
11		32			15			Total Real Power			Total RealPower		Potencia real total			Potencia real
12		32			15			Phase A Real Power			Real Power A		Potencia real fase R			Pot real R
13		32			15			Phase B Real Power			Real Power B		Potencia real fase S			Pot real S
14		32			15			Phase C Real Power			Real Power C		Potencia real fase T			Pot real T
15		32			15			Total reative Power			Tot reat Power		Potencia reativa total			Pot reativa
16		32			15			Phase A reative Power			reat Power A		Potencia reativa R			Pot reativa R
17		32			15			Phase B reative Power			reat Power B		Potencia reativa S			Pot reativa S
18		32			15			Phase C reative Power			reat Power C		Potencia reativa T			Pot reativa T
19		32			15			Total Apparent Power			Total App Power		Potencia aparente total			Pot aparente
20		32			15			Phase A Apparent Power			App Power A		Potencia aparente fase R		Pot aparente R
21		32			15			Phase B Apparent Power			App Power B		Potencia aparente fase S		Pot aparente S
22		32			15			Phase C Apparent Power			App Power C		Potencia aparente fase T		Pot aparente T
23		32			15			Power Factor				Power Factor		Fator de potencia			Fator potencia
24		32			15			Phase A Power Factor			Power Factor A		Fator potencia fase R			Fator pot R
25		32			15			Phase B Power Factor			Power Factor B		Fator potencia fase S			Fator pot S
26		32			15			Phase C Power Factor			Power Factor C		Fator potencia fase T			Fator pot T
27		32			15			Phase A Current Crest Factor		Ia Crest Factor		Fator cresta Corrente R			Fat cresta IR
28		32			15			Phase B Current Crest Factor		Ib Crest Factor		Fator cresta Corrente S			Fat cresta IS
29		32			15			Phase C Current Crest Factor		Ic Crest Factor		Fator cresta Corrente T			Fat cresta IT
30		32			15			Phase A Current THD			Current THD A		THD Corrente fase R			THD I fase R
31		32			15			Phase B Current THD			Current THD B		THD Corrente fase S			THD I fase S
32		32			15			Phase C Current THD			Current THD C		THD Corrente fase T			THD I fase T
33		32			15			Phase A Voltage THD			Voltage THD A		THD Tensão fase R			THD Tens Fase R
34		32			15			Phase B Voltage THD			Voltage THD B		THD Tensão fase S			THD Tens Fase S
35		32			15			Phase C Voltage THD			Voltage THD C		THD Tensão fase T			THD Tens Fase T
36		32			15			Total Real Energy			Tot Real Energy		Energía Real total			Energia Real
37		32			15			Total reative Energy			Tot reatEnergy		Energía reativa total			Energia reat
38		32			15			Total Apparent Energy			Tot App Energy		Energía aparente total			Energ Aparente
39		32			15			Ambient Temperature			Ambient Temp		Temperatura ambiente			Temp ambiente
40		32			15			Nominal Line Voltage			Nominal L-Volt		Tensão nominal sistema			Tensão nominal
41		32			15			Nominal Phase Voltage			Nominal PH-Volt		Tensão nominal de fase			Tens nom fase
42		32			15			Nominal Frequency			Nom Frequency		Frequencia nominal			Frequencia nom
43		32			15			Mains Failure Alarm Threshold 1		MFA Threshold 1		Limiar alarme Falha Red 1		Alarm FalhaCA1
44		32			15			Mains Failure Alarm Threshold 2		MFA Threshold 2		Limiar alarme Falha Red 2		Alarm FalhaCA2
45		32			15			Voltage Alarm Threshold 1		Volt Alm Trld 1		Limiar alarme Tensão 1			Lim alarme V 1
46		32			15			Voltage Alarm Threshold 2		Volt Alm Trld 2		Limiar alarme Tensão 2			Lim alarme V 2
47		32			15			Frequency Alarm Threshold		Freq Alarm Trld		Limiar alarme frequencia		Lim alm frec
48		32			15			High Temperature Limit			High Temp Limit		Límite Alta Temperatura		Lim alta temp
49		32			15			Low Temperature Limit			Low Temp Limit		Límite Baixa Temperatura		Lim Baixa temp
50		32			15			Supervision Fail			Supervision Fail	Falha supervisão			Falha supervis
51		32			15			High Line Voltage AB			High L-Volt AB		Alta Tensão R-S			Alta tens R-S
52		32			15			Very High Line Voltage AB		VHigh L-Volt AB		Mui alta Tensão R-S			M alta Tens R-S
53		32			15			Low Line Voltage AB			Low L-Volt AB		Baixa Tensão R-S			Baixa tens R-S
54		32			15			Very Low Line Voltage AB		VLow L-Volt AB		Mui Baixa Tensão R-S			M Baix Tens R-S
55		32			15			High Line Voltage BC			High L-Volt BC		Alta Tensão S-T			Alta tens S-T
56		32			15			Very High Line Voltage BC		VHigh L-Volt BC		Mui alta Tensão S-T			M alta Tens S-T
57		32			15			Low Line Voltage BC			Low L-Volt BC		Baixa Tensão S-T			Baixa tens S-T
58		32			15			Very Low Line Voltage BC		VLow L-Volt BC		Mui Baixa Tensão S-T			M Baix Tens S-T
59		32			15			High Line Voltage CA			High L-Volt CA		Alta tens R-T			Alta tens R-T
60		32			15			Very High Line Voltage CA		VHigh L-Volt CA		Mui alta Tensão R-T			M alta Tens R-T
61		32			15			Low Line Voltage CA			Low L-Volt CA		Baixa Tensão R-T			Baixa tens R-T
62		32			15			Very Low Line Voltage CA		VLow L-Volt CA		Mui Baixa Tensão R-T			M Baix Tens R-T
63		32			15			High Phase Voltage A			High Ph-Volt A		Alta Tensão fase R			Alta Tensão R
64		32			15			Very High Phase Voltage A		VHigh Ph-Volt A		Mui alta Tensão fase R			M alta tens R
65		32			15			Low Phase Voltage A			Low Ph-Volt A		Baixa Tensão fase R			Baixa Tensão R
66		32			15			Very Low Phase Voltage A		VLow Ph-Volt A		Mui Baixa Tensão fase R		M Baixa tens R
67		32			15			High Phase Voltage B			High Ph-Volt B		Alta Tensão fase S			Alta Tensão S
68		32			15			Very High Phase Voltage B		VHigh Ph-Volt B		Mui alta Tensão fase S			Mui alta tens S
69		32			15			Low Phase Voltage B			Low Ph-Volt B		Baixa Tensão fase S			Baixa Tensão S
70		32			15			Very Low Phase Voltage B		VLow Ph-Volt B		Mui Baixa Tensão S			M Baixa tens S
71		32			15			High Phase Voltage C			High Ph-Volt C		Alta Tensão fase T			Alta Tensão T
72		32			15			Very High Phase Voltage C		VHigh Ph-Volt C		Mui alta Tensão fase T			M alta tens T
73		32			15			Low Phase Voltage C			Low Ph-Volt C		Baixa Tensão fase T			Baixa Tensão T
74		32			15			Very Low Phase Voltage C		VLow Ph-Volt C		Mui Baixa Tensão fase T		Mui Baixa tens T
75		32			15			Mains Failure				Mains Failure		Falha de Red				Falha de Red
76		32			15			Severe Mains Failure			Severe Main Fail	Falha de Red severo			Falha Red sever
77		32			15			High Frequency				High Frequency		Alta frequencia				Alta frequencia
78		32			15			Low Frequency				Low Frequency		Baixa frequencia			Baixa frequencia
79		32			15			High Temperature			High Temp		Alta temperatura			Alta temp
80		32			15			Low Temperature				Low Temperature		Baixa temperatura			Baixa temp
81		32			15			Rectifier AC				AC			Alterna Retificadores			CA Retificador
82		32			15			Supervision Fail			Supervision Fail	Falha supervisão			Falha supervs
83		32			15			No					No			Não					Não
84		32			15			Yes					Yes			Sim					Sim
85		32			15			Phase A Mains Failure Counter		A Mains Fail Cnt	Contador Falhas fase R			Cont Falhas R
86		32			15			Phase B Mains Failure Counter		B Mains Fail Cnt	Contador Falhas fase S			Cont Falhas S
87		32			15			Phase C Mains Failure Counter		C Mains Fail Cnt	Contador Falhas fase T			Cont Falhas T
88		32			15			Frequency Failure Counter		F Fail Cnt		Contador Falhas frequencia		Cont FalhasFrec
89		32			15			Reset Phase A Mains Fail Counter	Reset A FailCnt		Iniciar cont Falhas fase R		Inic Falhas R
90		32			15			Reset Phase B Mains Fail Counter	Reset B FailCnt		Iniciar cont Falhas fase S		Inic Falhas S
91		32			15			Reset Phase C Mains Fail Counter	Reset C FailCnt		Iniciar cont Falhas fase T		Inic Falhas T
92		32			15			Reset Frequency Counter			Reset F FailCnt		Iniciar cont Falhas frequencia		Inic FalhasFrec
93		32			15			Current Alarm Threshold			Curr Alarm Limit	Limiar alarme de Corrente		Lim alarm corr
94		32			15			Phase A High Current			A High Current		Alta Corrente fase R			Alta I fase R
95		32			15			Phase B High Current			B High Current		Alta Corrente fase S			Alta I fase S
96		32			15			Phase C High Current			C High Current		Alta Corrente fase T			Alta I fase T
97		32			15			Min Phase Voltage			Min Phase Volt		Tensão mínima de fase			Tens min fase
98		32			15			Max Phase Voltage			Max Phase Volt		Tensão máxima de fase			Tens max fase
99		32			15			Raw Data 1				Raw Data 1		Dados brutos 1				Dados brutos 1
100		32			15			Raw Data 2				Raw Data 2		Dados brutos 2				Dados brutos 2
101		32			15			Raw Data 3				Raw Data 3		Dados brutos 3				Dados brutos 3
102		32			15			Ref Voltage				Ref Voltage		Tensão de referencia			Tensão ref
103		32			15			State					State			Estado					Estado
104		32			15			Off					Off			Desconectado				Desconectado
105		32			15			On					on			Conectado				Conectado
106		32			15			High Phase Voltage			High Ph-Volt		Alta Tensão de Fase			Alta Tens Fase
107		32			15			Very High Phase Voltage			VHigh Ph-Volt		Mui alta Tensão de fase		M alta Tens Fas
108		32			15			Low Phase Voltage			Low Ph-Volt		Baixa Tensão de fase			Baixa Tens Fase
109		32			15			Very Low Phase Voltage			VLow Ph-Volt		Mui Baixa Tensão de fase		M Baix Tens Fase
110		32			15			All Rectifiers Not Responding		Rects No Resp		Ninhún retificador responde		Rets Não Resp
