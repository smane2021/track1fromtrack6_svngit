﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
zh

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1	32		15		Temperature 1		Temperature 1		Temperatura 1			Temperatura 1
2	32		15		Temperature 2		Temperature 2		Temperatura 2			Temperatura 2
3	32		15		Temperature 3		Temperature 3		Temperatura 3			Temperatura 3
4	32		15		Humidity		Humidity		Umidade			Umidade
5	32		15		Temp 1 Alm		Temp 1 Alm		Alarme Temp 1		Alrm Temp 1
6	32		15		Temp 2 Alm		Temp 2 Alm		Alarme Temp 1		Alrm Temp 1
7	32		15		Temp 3 Alm		Temp 3 Alm		Alarme Temp 1		Alrm Temp 1
8	32		15		Humidity Alm		Humidity Alm		Alarme Umidade		Alrm Umidade
9	32		15		Fan 1 Alm		Fan 1 Alm		Alarme Vent. 1		Alrm Vent. 1
10	32		15		Fan 2 Alm		Fan 2 Alm		Alarme Vent. 2		Alrm Vent. 2
11	32		15		Fan 3 Alm		Fan 3 Alm		Alarme Vent. 3		Alrm Vent. 3
12	32		15		Fan 4 Alm		Fan 4 Alm		Alarme Vent. 4		Alrm Vent. 4
13	32		15		Fan 5 Alm		Fan 5 Alm		Alarme Vent. 5		Alrm Vent. 5
14	32		15		Fan 6 Alm		Fan 6 Alm		Alarme Vent. 6		Alrm Vent. 6
15	32		15		Fan 7 Alm		Fan 7 Alm		Alarme Vent. 7		Alrm Vent. 7
16	32		15		Fan 8 Alm		Fan 8 Alm		Alarme Vent. 8		Alrm Vent. 8
17	32		15		DI 1 Alm		DI 1 Alm		Alarme DI 1		Alarme DI 1
18	32		15		DI 2 Alm		DI 2 Alm		Alarme DI 2		Alarme DI 2
19	32		15		Fan Type		Fan Type		Tipo Vent.		Tipo Vent.
20	32		15		With Fan 3		With Fan 3		Tipo Vent.		Tipo Vent.
21	32		15		Fan 3 Ctrl Logic	F3 Ctrl Logic		 Ctrl Lógico Vent. 3		 Ctrl Lógico Vent. 3
22	32		15		With Heater		With Heater		Com Aquecedor		ComAquec
23	32		15		With Humiture Sensor	With Hum Sensor	Com Sensor Umidade		ComSensUmid
24	32		15		Fan 1 State		Fan 1 State		Estado Vent. 1		Est. V1
25	32		15		Fan 2 State		Fan 2 State		Estado Vent. 2		Est. V2
26	32		15		Fan 3 State		Fan 3 State		Estado Vent. 3		Est. V3
27	32		15		Temp Sensor Fail	T Sensor Fail		Falha Sensor Temp.	FalhaSensorT
28	32		15		Heat Change		Heat Change		Mudar Aquecedor		MudarAquec
29	32		15		Forced Vent		Forced Vent		Ventilação Forçada	Vent.Forç.
30	32		15		Not Exist		Not Exist		Não Presente		Não Pres.
31	32		15		Exist		Exist	Presente		Presente
32	32		15		Heater Logic		Heater Logic		Lógica Aquecedor		Lóg.Aquec.
33	32		15		ETC Logic		ETC Logic		Lógica ETC			Lógica ETC
34	32		15		Stop			Stop			Parar			Parar
35	32		15		Start			Start			Começar			Começar
36	32		15		Temp1 Over		Temp1 Over		Alta Temperatura 1	AltaTemp.1
37	32		15		Temp1 Under		Temp1 Under		Baixa Temperatura 1	BaixaTemp.1
38	32		15		Temp2 Over		Temp2 Over		Alta Temperatura 2	AltaTemp.2
39	32		15		Temp2 Under		Temp2 Under		Baixa Temperatura 2	BaixaTemp.2
40	32		15		Temp3 Over		Temp3 Over		Alta Temperatura 3	AltaTemp.3
41	32		15		Temp3 Under		Temp3 Under		Baixa Temperatura 3	BaixaTemp.3
42	32		15		Humidity Over		Humidity Over		Alta Umidade		AltaUmid.
43	32		15		Humidity Under		Humidity Under		Baixa Umidade		BaixaUmid.
44	32		15		Temp1 Sensor		Temp1 Sensor		Sensor Temp1		SensorTemp1
45	32		15		Temp2 Sensor		Temp2 Sensor		Sensor Temp2		SensorTemp2
46	32		15		DI1 Alm Type		DI1 Alm Type		Tipo Alarme DI1		TipoAlrmDI1
47	32		15		DI2 Alm Type		DI2 Alm Type		Tipo Alarme DI2		TipoAlrmDI2
48	32		15		No. of Fans in Fan Group 1		Num of FanG1		Num. de Vent. G1		Num.Vent.G1
49	32		15		No. of Fans in Fan Group 2		Num of FanG2		Num. de Vent. G2		Num.Vent.G2
50	32		15		No. of Fans in Fan Group 3		Num of FanG3		Num. de Vent. G3		Num.Vent.G3
51	32		15		T Sensor of F1		T Sensor of F1		Sensor Temp. de V1	SensorTemp.V1
52	32		15		T Sensor of F2		T Sensor of F2		Sensor Temp. de V1	SensorTemp.V1
53	32		15		T Sensor of F3		T Sensor of F3		Sensor Temp. de V1	SensorTemp.V1
54	32		15		T Sensor of Heater1	T Sensor of H1	Sensor Temp. de Aquec.1		SensorT.Aquec.1
55	32		15		T Sensor of Heater2	T Sensor of H2	Sensor Temp. de Aquec.2		SensorT.Aquec.2
56	32		15		HEX Fan Group 1 50% Speed Temp		H1 50%Speed Temp	Temp. Média AquecedorG 1		Temp.MédiaAquecG1
57	32		15		HEX Fan Group 1 100% Speed Temp		H1 100%Speed Temp	Temp. Máxima AquecedorG 1	Temp.Máx.AquecG1
58	32		15		HEX Fan Group 2 Start Temp			H2 Start Temp		Temp. Começar AquecedorG 2	Temp.ComeçarAquec2
59	32		15		HEX Fan Group 2 100% Speed Temp		H2 100%Speed Temp	Temp. Máxima AquecedorG 2		Temp.Máx.Aquec2	
60	32		15		HEX Fan Group 2 Stop Temp			H2 Stop Temp		Temp. Desl. AquecedorG 2	 TempGDeslGAquecG2
61	32		15		Fan Filter Fan G1 Start Temp		FV1 Start Temp		Temp. Começar Vent. ForçadaG 1		Temp.ComeçarVF1
62	32		15		Fan Filter Fan G1 Max Speed Temp	FV1 Full Temp		Temp. Máxima Vent. ForçadaG 1	Temp.MáxVF1
63	32		15		Fan Filter Fan G1 Stop Temp			FV1 Stop Temp		Temp. Desl. Vent. ForçadaG 1	Temp.Desl.VF1
64	32		15		Fan Filter Fan G2 Start Temp		FV2 Start Temp		Temp. Começar Vent. ForçadaG 2		Temp.ComeçarVF2
65	32		15		Fan Filter Fan G2 max speed Temp	FV2 Full Temp		Temp. Máxima Vent. ForçadaG 2	Temp.MáxVF2
66	32		15		Fan Filter Fan G2 Stop Temp			FV2 Stop Temp		Temp. Desl. Vent. ForçadaG 2	Temp.Desl.VF2
67	32		15		Fan Filter Fan G3 Start Temp		F3 Start Temp		Temp. Começar Vent. ForçadaG 3		Temp.ComeçarVF3
68	32		15		Fan Filter Fan G3 max speed Temp	F3 Full Temp		Temp. Máxima Vent. ForçadaG 3	Temp.MáxVF3
69	32		15		Fan Filter Fan G3 Stop Temp			F3 Stop Temp		Temp. Desl. Vent. ForçadaG 3	Temp.Desl.VF3
70	32		15		Heater1 Start Temp	H1 Start Temp	Temp. Começar Aquecedor 1		Temp.ComeçarAquec.1
71	32		15		Heater1 Stop Temp	H1 Stop Temp	Temp. Desl. Aquecedor 1		Temp.Desl.Aquec.1
72	32		15		Heater2 Start Temp	H2 Start Temp	Temp. Começar Aquecedor 2		Temp.ComeçarAquec.2
73	32		15		Heater2 Stop Temp	H2 Stop Temp	Temp. Desl. Aquecedor 2		Temp.Desl.Aquec.2
74	32		15		Fan Group 1 Max Speed		FG1 Max Speed		Velocidade Máx. VG1		Veloc.Máx.VG1
75	32		15		Fan Group 2 Max Speed		FG2 Max Speed		Velocidade Máx. VG2		Veloc.Máx.VG2
76	32		15		Fan Group 3 Max Speed		FG3 Max Speed		Velocidade Máx. VG3		Veloc.Máx.VG3
77	32		15		Fan Min Speed		Fan Min Speed		Velocidade Mín. Vent.		Velocidade Mín. Vent.
78	32		15		Close			Close			Fechar		Fechar
79	32		15		Open			Open			Abrir		Abrir
80	32		15		Self Rect Alm		Self Rect Alm		Alarme Retif. Auto		Alarme Retif. Auto
81	32		15		With FCUP		With FCUP		Com FCUP		Com FCUP
82	32		15		Normal			Normal			Normal			Normal
83	32		15		Low				Low			Baixo		Baixo
84	32		15		High				High			Alto		Alto
85	32		15		Alarm				Alarm			Alarme		Alarme
86	32		15		Times of Communication Fail	Times Comm Fail		Tempos de Falha de Comunicação	TemposFalhaCom 
87	32		15		Existence State			Existence State		Estado Presente	Est.Presente
88	32		15		Comm OK				Comm OK			Comunicação OK	Com.OK
89	32		15		All Batteries Comm Fail		AllBattCommFail		Falha COM. Todas as Baterias	FalhaCOM.TodasBat.
90	32		15		Communication Fail		Comm Fail		Falha de Comunicação	FalhaCom.
91	32		15		FCUPLUS				FCUP			FCUPLUS		FCUP
92	32		15		Heater1 State			Heater1 State			Estado Heater1			Estado Heater1
93	32		15		Heater2 State			Heater2 State			Estado Heater2			Estado Heater2
94	32		15		Temperature 1 Low		Temp 1 Low			Temperatura 1 Baixa		Temp1 Baixa
95	32		15		Temperature 2 Low		Temp 2 Low			Temperatura 2 Baixa		Temp2 Baixa
96	32		15		Temperature 3 Low		Temp 3 Low			Temperatura 3 Baixa		Temp3 Baixa
97	32		15		Humidity Low			Humidity Low			Umidade baixa		Umidade baixa
98	32		15		Temperature 1 High		Temp 1 High			Temperatura 1 Alta		Temp 1 Alta
99	32		15		Temperature 2 High		Temp 2 High			Temperatura 2 Alta		Temp 2 Alta
100	32		15		Temperature 3 High		Temp 3 High			Temperatura 3 Alta		Temp 3 Alta
101	32		15		Humidity High			Humidity High			Umidade Alta		Umidade Alta
102	32		15		Temperature 1 Sensor Fail		T1 Sensor Fail		Falha Sensor Temp1		T1 Falha Sensor
103	32		15		Temperature 2 Sensor Fail		T2 Sensor Fail		Falha Sensor Temp2		T2 Falha Sensor	
104	32		15		Temperature 3 Sensor Fail		T3 Sensor Fail		Falha Sensor Temp3		T3 Falha Sensor	
105	32		15		Humidity Sensor Fail			Hum Sensor Fail		FalhaSensorUmidade		FalhaSensorUmid
106	32		15		0					0			0			0
107	32		15		1					1			1			1
108	32		15		2					2			2			2
109	32		15		3					3			3			3
110	32		15		4					4			4			4

111	32		15		Enter Test Mode			Enter Test Mode				EntreModoTeste			EntreModoTeste		
112	32		15		Exit Test Mode			Exit Test Mode				ModoTesteSaída			ModoTesteSaída		
113	32		15		Start Fan Group 1 Test		StartFanG1Test				Iniciar teste FanG1		IniciaTestFanG1		
114	32		15		Start Fan Group 2 Test		StartFanG2Test				Iniciar teste FanG2		IniciaTestFanG2		
115	32		15		Start Fan Group 3 Test		StartFanG3Test				Iniciar teste FanG3		IniciaTestFanG3		
116	32		15		Start Heater1 Test		StartHeat1Test				IniciarTesteHeater1		IniciaTestHeat1		
117	32		15		Start Heater2 Test		StartHeat2Test				IniciarTesteHeater2		IniciaTestHeat2			
118	32		15		Clear					Clear						Claro				Claro				
120	32		15		Clear Fan1 Run Time		ClrFan1RunTime				Limpar Fan1 Run Time	LimpFan1RunTime		
121	32		15		Clear Fan2 Run Time		ClrFan2RunTime				Limpar Fan2 Run Time	LimpFan2RunTime		
122	32		15		Clear Fan3 Run Time		ClrFan3RunTime				Limpar Fan3 Run Time	LimpFan3RunTime		
123	32		15		Clear Fan4 Run Time		ClrFan4RunTime				Limpar Fan4 Run Time	LimpFan4RunTime		
124	32		15		Clear Fan5 Run Time		ClrFan5RunTime				Limpar Fan5 Run Time	LimpFan5RunTime		
125	32		15		Clear Fan6 Run Time		ClrFan6RunTime				Limpar Fan6 Run Time	LimpFan6RunTime		
126	32		15		Clear Fan7 Run Time		ClrFan7RunTime				Limpar Fan7 Run Time	LimpFan7RunTime		
127	32		15		Clear Fan8 Run Time		ClrFan8RunTime				Limpar Fan8 Run Time	LimpFan8RunTime		
128	32		15		Clear Heater1 Run Time	ClrHtr1RunTime				Limpar Heater1 Run Time	LimHeat1RunTime		
129	32		15		Clear Heater2 Run Time	ClrHtr2RunTime				Limpar Heater2 Run Time	LimHeat2RunTime		
130	32		15		Fan1 Speed Tolerance		Fan1 Spd Toler			Tolerân Fan1 veloci		TolerFan1Veloci	
131	32		15		Fan2 Speed Tolerance		Fan2 Spd Toler			Tolerân Fan2 veloci		TolerFan2Veloci	
132	32		15		Fan3 Speed Tolerance		Fan3 Spd Toler			Tolerân Fan3 veloci		TolerFan3Veloci	
133	32		15		Fan1 Rated Speed			Fan1 Rated Spd			Fan1 VelocNominal			Fan1VelocNomin	
134	32		15		Fan2 Rated Speed			Fan2 Rated Spd			Fan2 VelocNominal			Fan2VelocNomin	
135	32		15		Fan3 Rated Speed			Fan3 Rated Spd			Fan3 VelocNominal			Fan3VelocNomin	
136	32		15		Heater Test Time			HeaterTestTime			HeaterTempoTeste			HeaterTempTest
137	32		15		Heater Temperature Delta	HeaterTempDelta			HeaterDeltaTemp			HeaterDeltaTemp
140	32		15		Running Mode				Running Mode			Modo Corrida				Modo Corrida	
141	32		15		FAN1 Status					FAN1Status				FAN1 Estado				FAN1 Estado		
142	32		15		FAN2 Status					FAN2Status				FAN2 Estado				FAN2 Estado		
143	32		15		FAN3 Status					FAN3Status				FAN3 Estado				FAN3 Estado		
144	32		15		FAN4 Status					FAN4Status				FAN4 Estado				FAN4 Estado			
145	32		15		FAN5 Status					FAN5Status				FAN5 Estado				FAN5 Estado			
146	32		15		FAN6 Status					FAN6Status				FAN6 Estado				FAN6 Estado			
147	32		15		FAN7 Status					FAN7Status				FAN7 Estado				FAN7 Estado			
148	32		15		FAN8 Status					FAN8Status				FAN8 Estado				FAN8 Estado			
149	32		15		Heater1 Test Status			Heater1Status			Estado Heater1 Teste		Estado Heater1		
150	32		15		Heater2 Test Status			Heater2Status			Estado Heater2 Teste		Estado Heater2		
151	32		15		FAN1 Test Result			FAN1TestResult			Resultado FAN1 Teste		ResultFAN1Teste	
152	32		15		FAN2 Test Result			FAN2TestResult			Resultado FAN2 Teste		ResultFAN2Teste	
153	32		15		FAN3 Test Result			FAN3TestResult			Resultado FAN3 Teste		ResultFAN3Teste	
154	32		15		FAN4 Test Result			FAN4TestResult			Resultado FAN4 Teste		ResultFAN4Teste	
155	32		15		FAN5 Test Result			FAN5TestResult			Resultado FAN5 Teste		ResultFAN5Teste	
156	32		15		FAN6 Test Result			FAN6TestResult			Resultado FAN6 Teste		ResultFAN6Teste	
157	32		15		FAN7 Test Result			FAN7TestResult			Resultado FAN7 Teste		ResultFAN7Teste	
158	32		15		FAN8 Test Result			FAN8TestResult			Resultado FAN8 Teste		ResultFAN8Teste	
159	32		15		Heater1 Test Result			Heater1TestRslt			ResultHeater1Teste			ResultHeat1Test	
160	32		15		Heater2 Test Result			Heater2TestRslt			ResultHeater2Teste			ResultHeat1Test
171	32		15		FAN1 Run Time				FAN1RunTime				FAN1 Run Time				FAN1RunTime		
172	32		15		FAN2 Run Time				FAN2RunTime				FAN2 Run Time				FAN2RunTime		
173	32		15		FAN3 Run Time				FAN3RunTime				FAN3 Run Time				FAN3RunTime		
174	32		15		FAN4 Run Time				FAN4RunTime				FAN4 Run Time				FAN4RunTime		
175	32		15		FAN5 Run Time				FAN5RunTime				FAN5 Run Time				FAN5RunTime		
176	32		15		FAN6 Run Time				FAN6RunTime				FAN6 Run Time				FAN6RunTime		
177	32		15		FAN7 Run Time				FAN7RunTime				FAN7 Run Time				FAN7RunTime		
178	32		15		FAN8 Run Time				FAN8RunTime				FAN8 Run Time				FAN8RunTime		
179	32		15		Heater1 Run Time			Heater1RunTime			Heater1 Run Time			Heater1RunTime	
180	32		15		Heater2 Run Time			Heater2RunTime			Heater2	Run Time			Heater2RunTime	

181	32		15		Normal						Normal					Normal						Normal	
182	32		15		Test						Test					Teste						Teste	
183	32		15		NA							NA						NA							NA		
184	32		15		Stop						Stop					Pare						Pare	
185	32		15		Run							Run						Run							Run		
186	32		15		Test						Test					Teste						Teste	

190	32		15		FCUP is Testing				FCUPIsTesting			FCUPEstáTestan				FCUPEstáTestan	
191	32		15		FAN1 Test Fail				FAN1TestFail			FAN1TesteFalha				FAN1TesteFalha	
192	32		15		FAN2 Test Fail				FAN2TestFail			FAN2TesteFalha				FAN2TesteFalha	
193	32		15		FAN3 Test Fail				FAN3TestFail			FAN3TesteFalha				FAN3TesteFalha	
194	32		15		FAN4 Test Fail				FAN4TestFail			FAN4TesteFalha				FAN4TesteFalha	
195	32		15		FAN5 Test Fail				FAN5TestFail			FAN5TesteFalha				FAN5TesteFalha	
196	32		15		FAN6 Test Fail				FAN6TestFail			FAN6TesteFalha				FAN6TesteFalha	
197	32		15		FAN7 Test Fail				FAN7TestFail			FAN7TesteFalha				FAN7TesteFalha	
198	32		15		FAN8 Test Fail				FAN8TestFail			FAN8TesteFalha				FAN8TesteFalha	
199	32		15		Heater1 Test Fail			Heater1TestFail			Heater1TesteFalha			Heat1TestFalha
200	32		15		Heater2 Test Fail			Heater2TestFail			Heater2TesteFalha			Heat2TestFalha	
201	32		15		Fan is Test					Fan is Test				Fan é teste					Fan é teste		
202	32		15		Heater is Test				Heater is Test			Heater é teste				Heater é teste	
203	32		15		Version 106					Version 106				Versão 106					Versão 106		
204	32		15		Fan1 DownLimit Speed			Fan1 DnLmt Spd			Fan1 DownLimit Speed		Fan1 DnLmt spd		
205	32		15		Fan2 DownLimit Speed			Fan2 DnLmt Spd			Fan2 DownLimit Speed		Fan2 DnLmt spd		
206	32		15		Fan3 DownLimit Speed			Fan3 DnLmt Spd			Fan3 DownLimit Speed		Fan3 DnLmt spd		
