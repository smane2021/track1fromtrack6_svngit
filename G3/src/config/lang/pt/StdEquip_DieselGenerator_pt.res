﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Diesel Battery Voltage			Dsl Batt Volt		Grupo Tensão de Bateria		Grupo Tens Bat
2		32			15			Diesel Running				Diesel Running		Grupo Operando				Grupo Operando
3		32			15			Relay 2 Status				Relay2 Status		Estado Relé 2				Estado Relé 2
4		32			15			Relay 3 Status				Relay3 Status		Estado Relé 3				Estado Relé 3
5		32			15			Relay 4 Status				Relay4 Status		Estado Relé 4				Estado Relé 4
6		32			15			Diesel Failure Status			Diesel Fail		FalhGrupDiesel		FalhGrupDiesel
7		32			15			Diesel Connected Status			Diesel Cnnected		Grupo E. Ligado				Grupo Ligado
8		32			15			Low Fuel Level Status			Low Fuel Level		Baixo nivel combustivel			Baixo nivel comb
9		32			15			High Water Temp Status			High Water Temp		Alta temperatura de agua		Alta Temp agua
10		32			15			Low Oil Pressure Status			Low Oil Pressur		Baixa pressão óleo			Baixa pres óleo
11		32			15			Start Diesel				Start Diesel		Ligar Grupo Elect.			Ligar Grupo
12		32			15			Relay 2 On/Off				Relay 2 On/Off		Relé 2 Ligado				Relé 2 conect
13		32			15			Relay 3 On/Off				Relay 3 On/Off		Relé 3 Ligado				Relé 3 conect
14		32			15			Relay 4 On/Off				Relay 4 On/Off		Relé 4 Ligado				Rele 4 conect
15		32			15			Battery Voltage Limit			Batt Volt Limit		Límite Tensão Bateria			Lim Tens Bat
16		32			15			Relay 1 Pulse Time			RLY 1 Puls Time		Tempo de pulso Relé 1			T pulso rele1
17		32			15			Relay 2 Pulse Time			RLY 2 Puls Time		Tempo de pulso Relé 2			T pulso rele2
18		32			15			Relay 1 Pulse Time			RLY 1 Puls Time		Tempo de pulso Relé 1			T pulso rele1
19		32			15			Relay 2 Pulse Time			RLY 2 Puls Time		Tempo de pulso Relé 2			T pulso rele2
20		32			15			Low DC Voltage				Low DC Voltage		Baixa Tensão DC			Baixa Tens DC
21		32			15			DG Supervision Fail			Supervision Fail	Falha supervisão DG			Falha Sup.DG
22		32			15			Diesel Generator Failure		Generator Fail		Falha GE		                Falha GE
23		32			15			Diesel Generator Connected		Gen Connected		GE Ligado		                GE Ligado
24		32			15			Not Running				Not Running		Não Operando				Não operando
25		32			15			Running					Running			Operando				Operando
26		32			15			Off					Off			Desligado				Desligado
27		32			15			On					On			Ligado					Ligado
28		32			15			Off					Off			Desligado				Desligado
29		32			15			On					On			Ligado					Ligado
30		32			15			Off					Off			Desligado				Desligado
31		32			15			On					On			Ligado					Ligado
32		32			15			No					No			Não					Não
33		32			15			Yes					Yes			Sim					Sim
34		32			15			No					No			Não					Não
35		32			15			Yes					Yes			Sim					Sim
36		32			15			Off					Off			Desligado				Desligado
37		32			15			On					On			Ligado					Ligado
38		32			15			Off					Off			Desligado				Desligado
39		32			15			On					On			Ligado					Ligado
40		32			15			Off					Off			Desligado				Desligado
41		32			15			On					On			Ligado					Ligado
42		32			15			Diesel Generator			Diesel Genrator		G.Electr			        G.Electr
43		32			15			Mains Connected				Mains Connected		Disjuntor Red Ligado			Red conec
44		32			15			Diesel Shutdown				Diesel Shutdown		Desconexão G.E.			Desc. G.E.
45		32			15			Low Fuel Level				Low Fuel Level		Baixo nivel de combustivel		Baixo comb
46		32			15			High Water Temperature			High Water Temp		Alta temperatura agua			Alta temp agua
47		32			15			Low Oil Pressure			Low Oil Press		Baixa pressão de óleo		Baixa P óleo
48		32			15			Supervision Fail			SMAC Fail		Falha supervisão SM-AC			Falha SM-AC
49		32			15			Low Oil Pressure Alarm Status		Low Oil Press		Alarme Baixa pressão óleo		Baixa P óleo
50		32			15			Reset Low Oil Pressure Alarm		Clr OilPressArm		Limpar Alarme Baixa pres óleo		Limpar BP óleo
51		32			15			State					State			Estado					Estado
52		32			15			Existence State				Existence State		Estado Existente			Est Existente
53		32			15			Existent				Existent		Existente				Existente
54		32			15			Non-Existent				Non-Existent		Inexistente				Inexistente
55		32			15			Total Run Time				Total Run Time		Tempo Total de Funcionamento		TempoTotalFunc.
56		32			15			Maintenance Time Limit			Mtn Time Limit		Limite de Tempo de Manutenção		Lim.TempoManut.
57		32			15			Clear Total Run Time			Clr Run Time		Apagar Tempo Total de Funcionamento	ApagarTempoFunc.
58		32			15			Periodical Maintenance Required		Mtn Required		Manutenção periódicoObrigatório	Man.periódObrig
59		32			15			Maintenance Countdown Timer		Mtn DownTimer		Contagem Regressiva de Manutenção	Cont.Reg.Manut.
