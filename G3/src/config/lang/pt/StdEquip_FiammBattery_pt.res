﻿#
#  Locale language support:pt
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
pt


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			Battery Current				Batt Current		Corrente Bateria		Corrente Bat
2	32			15			Battery Rating (Ah)     		Batt Rating(Ah)		Classificação da bateria	Classifi Bat
3	32			15			Bus Voltage				Bus Voltage		Tensão Bus		Tensão Bus
5	32			15			Battery Over Current			Batt Over Curr		Bateria Sobre corrente		Bat Sobre Corr
6	32			15			Battery Capacity (%)			Batt Cap (%)		Capacidade da bateria		Cap bateria
7	32			15			Battery Voltage				Batt Voltage		Tensão da bateria		Tensão Bat
18	32			15			SoNick Battery				SoNick Batt		SoNick bateria		SoNick bat
29	32			15			Yes					Yes			sim			sim
30	32			15			No					No			Não			Não
31	32			15			On					On			Em			Em
32	32			15			Off					Off			Fora			Fora
33	32			15			State					State			State			State
87	32			15			No					No			Não			Não
96	32			15			Rated Capacity				Rated Capacity		Capacidade Nominal	Cap Nominal
99	32			15			Battery Temperature(AVE)		Batt Temp(AVE)		Temperatura bateria (AVE)	Temp Bat (AVE)
100	32			15			Board Temperature			Board Temp		Temperatura Board		Temp Board
101	32			15			Tc Center Temperature			Tc Center Temp		Tc Temperatura central		TcTempCentral
102	32			15			Tc Left Temperature			Tc Left Temp		Tc Temperatura esquerda		TcTempEsquerda
103	32			15			Tc Right Temperature			Tc Right Temp		Tc Temperatura Direita		TcTempDireita
114	32			15			Battery Communication Fail		Batt Comm Fail		Falha com bateria		Falha Com Bat
129	32			15			Low Ambient Temperature			Low Amb Temp		Temperatura ambiente baixa	TempAmbBaixa
130	32			15			High Ambient Temperature Warning	High Amb Temp W 	Alto Aviso Temperatura Ambiente		AltoAvisTempAmb
131	32			15			High Ambient Temperature		High Amb Temp		Alto Temperatura Ambiente		AltoTempAmb
132	32			15			Low Battery Internal Temperature	Low Int Temp		Temperatura interna bateria baixa	TempIntBaixa
133	32			15			High Batt Internal Temp Warning		Hi Int Temp W		Alta Batt Interno Temp Aviso		AltaIntTempAvis
134	32			15			High Batt Internal Temperature		Hi Bat Int Temp		Alta Batt Interno Temp		AltaIntTemp
135	32			15			Bus Voltage Below 40V			Bus V Below 40V		Tensão Bus Abaixo 40V		V BusAbaixo40V
136	32			15			Bus Voltage Below 39V			Bus V Below 39V		Tensão Bus Abaixo 39V		V BusAbaixo39V
137	32			15			Bus Voltage Above 60V			Bus V Above 60V		Tensão BusAcima 60V		V BusAcima60V
138	32			15			Bus Voltage Above 65V			Bus V Above 65V		Tensão BusAcima 65V		V BusAcima65V
139	32			15			High Discharge Current Warning		Hi Disch Curr W		Aviso Corrente Alta descarga	W CorAltaDescar
140	32			15			High Discharge Current			High Disch Curr		Corrente Alta descarga	CorAltaDescar
141	32			15			Main Switch Error			Main Switch Err		Erro comutador principal	ErroComutPrinc
142	32			15			Fuse Blown				Fuse Blown		Fusível soprado			FusívelSoprado
143	32			15			Heaters Failure				Heaters Failure		Falha Aquecedores		Falha Aqueced
144	32			15			Thermocouple Failure			Thermocple Fail		Falha termopar		Falha Termopar
145	32			15			Voltage Measurement Circuit Fail	V M Cir Fail		CircuitomediçãoTensãoFalha		Cir Med V Falha
146	32			15			Current Measurement Circuit Fail	C M Cir Fail		CircuitomediçãoAtualFalha		Cir Med C Falha
147	32			15			BMS Hardware Failure			BMS Hdw Failure		BMS Falha hardware			BMS Falha HW
148	32			15			Hardware Protection Sys Active		HdW Protect Sys		Sistema Proteção Hardware Ativo		SisProtHW
149	32			15			High Heatsink Temperature		Hi Heatsink Tmp		Temperatura do dissipador alta		TempDissipAlta
150	32			15			Battery Voltage Below 39V		Bat V Below 39V		Tensão BatAbaixo 39V		V Bat Abaixo39V
151	32			15			Battery Voltage Below 38V		Bat V Below 38V		Tensão BatAbaixo 38V		V Bat Abaixo38V
152	32			15			Battery Voltage Above 53.5V		Bat V Abv 53.5V		Tensão Bateria acima53.5V	VBat Acima53.5V
153	32			15			Battery Voltage Above 53.6V		Bat V Abv 53.6V		Tensão Bateria acima53.6V	VBat Acima53.6V
154	32			15			High Charge Current Warning		Hi Chrge Curr W		Aviso Alta Carga corrente	W AltaCargaCorr
155	32			15			High Charge Current			Hi Charge Curr		Alta Carga corrente	AltaCargaCorr
156	32			15			High Discharge Current Warning		Hi Disch Curr W		Aviso Alta Descarga corrente	W AltaDescaCorr
157	32			15			High Discharge Current			Hi Disch Curr		Alta Descarga corrente	AltaDescargCorr
158	32			15			Voltage Unbalance Warning		V unbalance W		Aviso Desequilíbrio Tensão	W.desequil V
159	32			15			Voltages Unbalance			Volt Unbalance		Aviso Desequilíbrio Tensão	Desequil V
160	32			15			Dc Bus Pwr Too Low for Charging		DC Low for Chrg		Dc Bus Pwr Too Low para carregar	DCLow for Chrg
161	32			15			Charge Regulation Failure		Charge Reg Fail		Regulamento carga Falha		RegCargaFalha
162	32			15			Capacity Below 12.5%			Cap Below 12.5%		Capacidade abaixo 12.5%		Cap abaixo12.5%
163	32			15			Thermocouples Mismatch			Tcples Mismatch		Incompatibilidade termopares	Incomp Termopar
164	32			15			Heater Fuse Blown			Heater FA		Fusível do aquecedor soprado	FusAqueSoprado