﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Analog Input 1				Analog Input 1		模擬量輸入1				模擬量輸入1
2		32			15			Analog Input 2				Analog Input 2		模擬量輸入2				模擬量輸入2
3		32			15			Analog Input 3				Analog Input 3		模擬量輸入3				模擬量輸入3
4		32			15			Analog Input 4				Analog Input 4		模擬量輸入4				模擬量輸入4
5		32			15			Analog Input 5				Analog Input 5		模擬量輸入5				模擬量輸入5
6		32			15			Frequency Input				Frequency Input		頻率輸入				頻率輸入
7		32			15			Digital Input 1				Digital Input 1		數字量輸入1				數字量輸入1
8		32			15			Digital Input 2				Digital Input 2		數字量輸入2				數字量輸入2
9		32			15			Digital Input 3				Digital Input 3		數字量輸入3				數字量輸入3
10		32			15			Digital Input 4				Digital Input 4		數字量輸入4				數字量輸入4
11		32			15			Digital Input 5				Digital Input 5		數字量輸入5				數字量輸入5
12		32			15			Digital Input 6				Digital Input 6		數字量輸入6				數字量輸入6
13		32			15			Digital Input 7				Digital Input 7		數字量輸入7				數字量輸入7
14		32			15			Relay 1 Status				Relay 1 Status		繼電器1状態				繼電器1状態
15		32			15			Relay 2 Status				Relay 2 Status		繼電器2状態				繼電器2状態
16		32			15			Relay 3 Status				Relay 3 Status		繼電器3状態				繼電器3状態
17		32			15			Relay 1 On/Off				Relay 1 On/Off		繼電器1開關				繼電器1開關
18		32			15			Relay 2 On/Off				Relay 2 On/Off		繼電器2開關				繼電器2開關
19		32			15			Relay 3 On/Off				Relay 3 On/Off		繼電器3開關				繼電器3開關
23		32			15			High Analog Input 1 Limit		Hi AI 1 Limit		高模擬量輸入1限點			高模量輸入1限點
24		32			15			Low Analog Input 1 Limit		Low AI 1 Limit		低模擬量輸入1限點			低模量輸入1限點
25		32			15			High Analog Input 2 Limit		Hi AI 2 Limit		高模擬量輸入2限點			高模量輸入2限點
26		32			15			Low Analog Input 2 Limit		Low AI 2 Limit		低模擬量輸入2限點			低模量輸入2限點
27		32			15			High Analog Input 3 Limit		Hi AI 3 Limit		高模擬量輸入3限點			高模量輸入3限點
28		32			15			Low Analog Input 3 Limit		Low AI 3 Limit		低模擬量輸入3限點			低模量輸入3限點
29		32			15			High Analog Input 4 Limit		Hi AI 4 Limit		高模擬量輸入4限點			高模量輸入4限點
30		32			15			Low Analog Input 4 Limit		Low AI 4 Limit		低模擬量輸入4限點			低模量輸入4限點
31		32			15			High Analog Input 5 Limit		Hi AI 5 Limit		高模擬量輸入5限點			高模量輸入5限點
32		32			15			Low Analogu Input 5 Limit		Low AI 5 Limit		低模擬量輸入5限點			低模量輸入5限點
33		32			15			High Frequency Limit			High Freq Limit		高頻點					高頻點
34		32			15			Low Frequency Limit			Low Freq Limit		低頻點					低頻點
35		32			15			High Analog Input 1 Alarm		Hi AI 1 Alarm		高模擬量輸入1告警			高模量輸入1告警
36		32			15			Low Analog Input 1 Alarm		Low AI 1 Alarm		低模擬量輸入1告警			低模量輸入1告警
37		32			15			High Analog Input 2 Alarm		Hi AI 2 Alarm		高模擬量輸入2告警			高模量輸入2告警
38		32			15			Low Analog Input 2 Alarm		Low AI 2 Alarm		低模擬量輸入2告警			低模量輸入2告警
39		32			15			High Analog Input 3 Alarm		Hi AI 3 Alarm		高模擬量輸入3告警			高模量輸入3告警
40		32			15			Low Analog Input 3 Alarm		Low AI 3 Alarm		低模擬量輸入3告警			低模量輸入3告警
41		32			15			High Analog Input 4 Alarm		Hi AI 4 Alarm		高模擬量輸入4告警			高模量輸入4告警
42		32			15			Low Analog Input 4 Alarm		Low AI 4 Alarm		低模擬量輸入4告警			低模量輸入4告警
43		32			15			High Analog Input 5 Alarm		Hi AI 5 Alarm		高模擬量輸入5告警			高模量輸入5告警
44		32			15			Low Analog Input 5 Alarm		Low AI 5 Alarm		低模擬量輸入5告警			低模量輸入5告警
45		32			15			High Frequency Input Alarm		Hi Freq In Alm		高頻輸入告警				高頻輸入告警
46		32			15			Low Frequency Input Alarm		Low Freq In Alm		低頻輸入告警				低頻輸入告警
47		32			15			Off					Off			關					關
48		32			15			On					On			開					開
49		32			15			Off					Off			關					關
50		32			15			On					On			開					開
51		32			15			Off					Off			關					關
52		32			15			On					On			開					開
53		32			15			Off					Off			關					關
54		32			15			On					On			開					開
55		32			15			Off					Off			關					關
56		32			15			On					On			開					開
57		32			15			Off					Off			關					關
58		32			15			On					On			開					開
59		32			15			Off					Off			關					關
60		32			15			On					On			開					開
61		32			15			Off					Off			關					關
62		32			15			On					On			開					開
63		32			15			Off					Off			關					關
64		32			15			On					On			開					開
65		32			15			Off					Off			關					關
66		32			15			On					On			開					開
67		32			15			Off					Off			關					關
68		32			15			On					On			開					開
69		32			15			Off					Off			關					關
70		32			15			On					On			開					開
71		32			15			Off					Off			關					關
72		32			15			On					On			開					開
73		32			15			SMIO 3					SMIO 3			SMIO 3					SMIO 3
74		32			15			SMIO Failure				SMIO Fail		SMIO通訊失敗				SMIO通訊失敗
75		32			15			SMIO Failure				SMIO Fail		SMIO通訊失敗				SMIO通訊失敗
76		32			15			No					No			否					否
77		32			15			Yes					Yes			是					是
78		32			15			Testing Relay 1				Testing Relay 1		測試繼電器1				測試繼電器1
79		32			15			Testing Relay 2				Testing Relay 2		測試繼電器2				測試繼電器2
80		32			15			Testing Relay 3				Testing Relay 3		測試繼電器3				測試繼電器3
