﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1	32		15		Temperature 1		Temperature 1		溫度1			溫度1
2	32		15		Temperature 2		Temperature 2		溫度2			溫度2
3	32		15		Temperature 3		Temperature 3		溫度3			溫度3
4	32		15		Humidity		Humidity		濕度			濕度
5	32		15		Temperature 1 Alarm		Temp 1 Alm		溫度1告警		溫度1告警
6	32		15		Temperature 2 Alarm		Temp 2 Alm		溫度2告警		溫度2告警
7	32		15		Temperature 3 Alarm		Temp 3 Alm		溫度3告警		溫度3告警
8	32		15		Humidity Alarm		Humidity Alm		濕度告警		濕度告警
9	32		15		Fan 1 Alarm		Fan 1 Alm		風機1告警		風機1告警
10	32		15		Fan 2 Alarm		Fan 2 Alm		風機2告警		風機2告警
11	32		15		Fan 3 Alarm		Fan 3 Alm		風機3告警		風機3告警
12	32		15		Fan 4 Alarm		Fan 4 Alm		風機4告警		風機4告警
13	32		15		Fan 5 Alarm		Fan 5 Alm		風機5告警		風機5告警
14	32		15		Fan 6 Alarm		Fan 6 Alm		風機6告警		風機6告警
15	32		15		Fan 7 Alarm		Fan 7 Alm		風機7告警		風機7告警
16	32		15		Fan 8 Alarm		Fan 8 Alm		風機8告警		風機8告警
17	32		15		DI 1 Alarm		DI 1 Alm		DI1告警		DI1告警
18	32		15		DI 2 Alarm		DI 2 Alm		DI2告警		DI2告警
19	32		15		Fan Type		Fan Type		風機類型		風機類型
20	32		15		With Fan 3		With Fan 3		風機3存在狀態		風機3存在狀態
21	32		15		Fan 3 Ctrl Logic	F3 Ctrl Logic		風機組3控制邏輯		風機組3控制邏輯
22	32		15		With Heater		With Heater		加熱器存在狀態		加熱器存在狀態
23	32		15		With Temp and Humidity Sensor	With T Hum Sen	溫濕度感測器		溫濕度感測器
24	32		15		Fan 1 State		Fan 1 State		風機1狀態		風機1狀態
25	32		15		Fan 2 State		Fan 2 State		風機2狀態		風機2狀態
26	32		15		Fan 3 State		Fan 3 State		風機3狀態		風機3狀態
27	32		15		Temperature Sensor Fail	T Sensor Fail		溫度感測器故障		溫度感測器故障
28	32		15		Heat Change		Heat Change		熱交換型		熱交換型
29	32		15		Forced Vent		Forced Vent		直通風型		直通風型
30	32		15		Not Existence		Not Exist		不存在			不存在
31	32		15		Existence		Exist		存在			存在
32	32		15		Heater Logic		Heater Logic		加熱器邏輯		加熱器邏輯
33	32		15		ETC Logic		ETC Logic		ETC邏輯			ETC邏輯
34	32		15		Stop			Stop			停止			停止
35	32		15		Start			Start			啟動			啟動
36	32		15		Temperature 1 Over		Temp1 Over		溫度1過溫點		溫度1過溫點
37	32		15		Temperature 1 Under		Temp1 Under		溫度1欠溫點		溫度1欠溫點
38	32		15		Temperature 2 Over		Temp2 Over		溫度2過溫點		溫度2過溫點
39	32		15		Temperature 2 Under		Temp2 Under		溫度2欠溫點		溫度2欠溫點
40	32		15		Temperature 3 Over		Temp3 Over		溫度3過溫點		溫度3過溫點
41	32		15		Temperature 3 Under		Temp3 Under		溫度3欠溫點		溫度3欠溫點
42	32		15		Humidity Over		Humidity Over		濕度過濕點		濕度過濕點
43	32		15		Humidity Under		Humidity Under		濕度欠濕點		濕度欠濕點
44	32		15		Temperature 1 Sensor State		Temp1 Sensor		溫度1感測器狀態		溫度1感測器狀態
45	32		15		Temperature 2 Sensor State		Temp2 Sensor		溫度2感測器狀態		溫度2感測器狀態
46	32		15		DI1 Alarm Type		DI1 Alm Type		DI1告警類型		DI1告警類型
47	32		15		DI2 Alarm Type		DI2 Alm Type		DI2告警類型		DI2告警類型
48	32		15		No. of Fans in Fan Group 1		Num of FanG1		風機組1風機數		風機組1風機數
49	32		15		No. of Fans in Fan Group 2		Num of FanG2		風機組2風機數		風機組2風機數
50	32		15		No. of Fans in Fan Group 3		Num of FanG3		風機組3風機數		風機組3風機數
51	32		15		Temperature Sensor of Fan1		T Sensor of F1		風機1溫度感測器		風機1溫度感測器
52	32		15		Temperature Sensor of Fan2		T Sensor of F2		風機2溫度感測器		風機2溫度感測器
53	32		15		Temperature Sensor of Fan3		T Sensor of F3		風機3溫度感測器		風機3溫度感測器
54	32		15		Temperature Sensor of Heater1	T Sensor of H1	1加熱溫度感測器		1加熱溫度感測器
55	32		15		Temperature Sensor of Heater2	T Sensor of H2	2加熱溫度感測器		2加熱溫度感測器
56	32		15		HEX Fan Group 1 50% Speed Temp		H1 50%Speed Temp		熱風機1半速溫度		熱風機1半速溫度
57	32		15		HEX Fan Group 1 100% Speed Temp		H1 100%Speed Temp		熱風機1全速溫度		熱風機1全速溫度
58	32		15		HEX Fan Group 2 Start Temp			H2 Start Temp			熱風機2啟動溫度		熱風機2啟動溫度
59	32		15		HEX Fan Group 2 100% Speed Temp		H2 100%Speed Temp		熱風機2全速溫度		熱風機2全速溫度	
60	32		15		HEX Fan Group 2 Stop Temp			H2 Stop Temp			熱風機2停止溫度		熱風機2停止溫度
61	32		15		Fan Filter Fan G1 Start Temp		FV1 Start Temp		直通風機1開始溫度		直通風機1開始溫度
62	32		15		Fan Filter Fan G1 Max Speed Temp	FV1 Full Temp		直通風機1全速溫度		直通風機1全速溫度
63	32		15		Fan Filter Fan G1 Stop Temp			FV1 Stop Temp		直通風機1停止溫度		直通風機1停止溫度
64	32		15		Fan Filter Fan G2 Start Temp		FV2 Start Temp		直通風機2開始溫度		直通風機2開始溫度
65	32		15		Fan Filter Fan G2 max speed Temp	FV2 Full Temp		直通風機2全速溫度		直通風機2全速溫度
66	32		15		Fan Filter Fan G2 Stop Temp			FV2 Stop Temp		直通風機2停止溫度		直通風機2停止溫度
67	32		15		Fan Filter Fan G3 Start Temp		F3 Start Temp		風機3開始溫度		風機3開始溫度
68	32		15		Fan Filter Fan G3 max speed Temp	F3 Full Temp		風機3全速溫度		風機3全速溫度
69	32		15		Fan Filter Fan G3 Stop Temp			F3 Stop Temp		風機3停止溫度		風機3停止溫度
70	32		15		Heater1 Start Temperature	Heater1 Start Temp	加熱器1開始溫度		加熱器1開始溫度
71	32		15		Heater1 Stop Temperature	Heater1 Stop Temp	加熱器1停止溫度		加熱器1停止溫度
72	32		15		Heater2 Start Temperature	Heater2 Start Temp	加熱器2開始溫度		加熱器2開始溫度
73	32		15		Heater2 Stop Temperature	Heater2 Stop Temp	加熱器2停止溫度		加熱器2停止溫度
74	32		15		Fan Group 1 Max Speed		FG1 Max Speed		風機組1最大速度		風機組1最大速度
75	32		15		Fan Group 2 Max Speed		FG2 Max Speed		風機組2最大速度		風機組2最大速度
76	32		15		Fan Group 3 Max Speed		FG3 Max Speed		風機組3最大速度		風機組3最大速度
77	32		15		Fan Minimum Speed			Fan Min Speed		風機組最小速度		風機組最小速度
78	32		15		Close			Close			閉合告警		閉合告警
79	32		15		Open			Open			斷開告警		斷開告警
80	32		15		Self Rectifier Alarm		Self Rect Alm		自成模組告警		自成模組告警
81	32		15		With FCUP		With FCUP		存在風機控制板		存在風機控制板
82	32		15		Normal			Normal			正常			正常
83	32		15		Low				Low			低		低
84	32		15		High				High			高		高
85	32		15		Alarm				Alarm			告警		告警
86	32		15		Times of Communication Fail	Times Comm Fail		通信失敗次數	通信失敗次數
87	32		15		Existence State			Existence State		是否存在	是否存在
88	32		15		Comm OK				Comm OK			通訊正常	通訊正常
89	32		15		All Batteries Comm Fail		AllBattCommFail		都通訊中斷	都通訊中斷
90	32		15		Communication Fail		Comm Fail		通訊中斷	通訊中斷
91	32		15		FCUPLUS				FCUPLUS			風機控制器	風機控制器
92	32		15		Heater1 State			Heater1 State			加熱器1狀態	加熱器1狀態
93	32		15		Heater2 State			Heater2 State			加熱器2狀態	加熱器2狀態
94	32		15		Temperature 1 Low		Temp 1 Low			溫度1低		溫度1低
95	32		15		Temperature 2 Low		Temp 2 Low			溫度2低		溫度2低
96	32		15		Temperature 3 Low		Temp 3 Low			溫度3低		溫度3低
97	32		15		Humidity Low			Humidity Low			濕度過低	濕度過低
98	32		15		Temperature 1 High		Temp 1 High			溫度1高		溫度1高
99	32		15		Temperature 2 High		Temp 2 High			溫度2高		溫度2高
100	32		15		Temperature 3 High		Temp 3 High			溫度3高		溫度3高
101	32		15		Humidity High			Humidity High			濕度過高	濕度過高
102	32		15		Temperature 1 Sensor Fail		T1 Sensor Fail		溫度感測器1故障		溫度1故障
103	32		15		Temperature 2 Sensor Fail		T2 Sensor Fail		溫度感測器2故障		溫度2故障
104	32		15		Temperature 3 Sensor Fail		T3 Sensor Fail		溫度感測器3故障		溫度3故障
105	32		15		Humidity Sensor Fail			Hum Sensor Fail		濕度感測器故障		濕度感測器故障
106	32		15		0					0			0			0
107	32		15		1					1			1			1
108	32		15		2					2			2			2
109	32		15		3					3			3			3
110	32		15		4					4			4			4
111	32		15		Enter Test Mode				Enter Test Mode				進入測試模式		進入測試模式
112	32		15		Exit Test Mode				Exit Test Mode				退出測試模式		退出測試模式
113	32		15		Start Fan Group 1 Test		StartFanG1Test				開始風機1測試		風機1測試	
114	32		15		Start Fan Group 2 Test		StartFanG2Test				開始風機2測試		風機2測試	
115	32		15		Start Fan Group 3 Test		StartFanG3Test				開始風機3測試		風機3測試
116	32		15		Start Heater1 Test			StartHeat1Test				開始加熱器1測試		加熱器1測試
117	32		15		Start Heater2 Test			StartHeat2Test				開始加熱器2測試		加熱器2測試
118	32		15		Clear						Clear						清除				清除
120	32		15		Clear Fan1 Run Time			ClrFan1RunTime				清風扇1執行時間		清風扇1時間
121	32		15		Clear Fan2 Run Time			ClrFan2RunTime				清風扇2執行時間		清風扇2時間
122	32		15		Clear Fan3 Run Time			ClrFan3RunTime				清風扇3執行時間		清風扇3時間
123	32		15		Clear Fan4 Run Time			ClrFan4RunTime				清風扇4執行時間		清風扇4時間
124	32		15		Clear Fan5 Run Time			ClrFan5RunTime				清風扇5執行時間		清風扇5時間
125	32		15		Clear Fan6 Run Time			ClrFan6RunTime				清風扇6執行時間		清風扇6時間
126	32		15		Clear Fan7 Run Time			ClrFan7RunTime				清風扇7執行時間		清風扇7時間
127	32		15		Clear Fan8 Run Time			ClrFan8RunTime				清風扇8執行時間		清風扇8時間
128	32		15		Clear Heater1 Run Time		ClrHtr1RunTime				清加熱器1執行時間	清加熱1時間
129	32		15		Clear Heater2 Run Time		ClrHtr2RunTime				清加熱器2執行時間	清加熱2時間
130	32		15		Fan1 Speed Tolerance		Fan1 Spd Toler			風扇1公差			風扇1公差
131	32		15		Fan2 Speed Tolerance		Fan2 Spd Toler			風扇2公差			風扇2公差
132	32		15		Fan3 Speed Tolerance		Fan3 Spd Toler			風扇3公差			風扇3公差
133	32		15		Fan1 Rated Speed			Fan1 Rated Spd			風扇1額定速度		風扇1額速
134	32		15		Fan2 Rated Speed			Fan2 Rated Spd			風扇2額定速度		風扇2額速
135	32		15		Fan3 Rated Speed			Fan3 Rated Spd			風扇3額定速度		風扇3額速
136	32		15		Heater Test Time			HeaterTestTime			加熱器測試時間		加熱測時間
137	32		15		Heater Test Temp Delta		HeaterTempDelta			加熱器溫差			加熱器溫差
140	32		15		Running Mode				Running Mode			運行模式			運行模式
141	32		15		FAN1 Status					FAN1Status				風扇1狀態			風扇1狀態
142	32		15		FAN2 Status					FAN2Status				風扇2狀態			風扇2狀態	
143	32		15		FAN3 Status					FAN3Status				風扇3狀態			風扇3狀態	
144	32		15		FAN4 Status					FAN4Status				風扇4狀態			風扇4狀態		
145	32		15		FAN5 Status					FAN5Status				風扇5狀態			風扇5狀態		
146	32		15		FAN6 Status					FAN6Status				風扇6狀態			風扇6狀態		
147	32		15		FAN7 Status					FAN7Status				風扇7狀態			風扇7狀態		
148	32		15		FAN8 Status					FAN8Status				風扇8狀態			風扇8狀態		
149	32		15		Heater1 Test Status			Heater1Status			加熱器1狀態			加熱器1狀態	
150	32		15		Heater2 Test Status			Heater2Status			加熱器2狀態			加熱器2狀態	
151	32		15		FAN1 Test Result			FAN1TestResult			風扇1測試結果		測風1結果
152	32		15		FAN2 Test Result			FAN2TestResult			風扇2測試結果		測風2結果
153	32		15		FAN3 Test Result			FAN3TestResult			風扇3測試結果		測風3結果
154	32		15		FAN4 Test Result			FAN4TestResult			風扇4測試結果		測風4結果
155	32		15		FAN5 Test Result			FAN5TestResult			風扇5測試結果		測風5結果
156	32		15		FAN6 Test Result			FAN6TestResult			風扇6測試結果		測風6結果
157	32		15		FAN7 Test Result			FAN7TestResult			風扇7測試結果		測風7結果
158	32		15		FAN8 Test Result			FAN8TestResult			風扇8測試結果		測風8結果
159	32		15		Heater1 Test Result			Heater1TestRslt			加熱器1測試結果		測加熱1結果
160	32		15		Heater2 Test Result			Heater2TestRslt			加熱器2測試結果		測加熱2結果
171	32		15		FAN1 Run Time				FAN1RunTime				風扇1執行時間		風扇1時間
172	32		15		FAN2 Run Time				FAN2RunTime				風扇2執行時間		風扇2時間
173	32		15		FAN3 Run Time				FAN3RunTime				風扇3執行時間		風扇3時間
174	32		15		FAN4 Run Time				FAN4RunTime				風扇4執行時間		風扇4時間
175	32		15		FAN5 Run Time				FAN5RunTime				風扇5執行時間		風扇5時間
176	32		15		FAN6 Run Time				FAN6RunTime				風扇6執行時間		風扇6時間
177	32		15		FAN7 Run Time				FAN7RunTime				風扇7執行時間		風扇7時間
178	32		15		FAN8 Run Time				FAN8RunTime				風扇8執行時間		風扇8時間
179	32		15		Heater1 Run Time			Heater1RunTime			加熱器1執行時間		加熱器1時間
180	32		15		Heater2 Run Time			Heater2RunTime			加熱器2執行時間		加熱器2時間

181	32		15		Normal						Normal					正常				正常
182	32		15		Test						Test					測試				測試
183	32		15		NA							NA						未初始化			未初始化
184	32		15		Stop						Stop					停止				停止
185	32		15		Run							Run						運行				運行
186	32		15		Test						Test					測試				測試

190	32		15		FCUP is Testing				FCUPIsTesting			FCUP正在測試		FCUP正在測
191	32		15		FAN1 Test Fail				FAN1TestFail			風扇1測試失敗		風1測失敗
192	32		15		FAN2 Test Fail				FAN2TestFail			風扇2測試失敗		風2測失敗
193	32		15		FAN3 Test Fail				FAN3TestFail			風扇3測試失敗		風3測失敗
194	32		15		FAN4 Test Fail				FAN4TestFail			風扇4測試失敗		風4測失敗
195	32		15		FAN5 Test Fail				FAN5TestFail			風扇5測試失敗		風5測失敗
196	32		15		FAN6 Test Fail				FAN6TestFail			風扇6測試失敗		風6測失敗
197	32		15		FAN7 Test Fail				FAN7TestFail			風扇7測試失敗		風7測失敗
198	32		15		FAN8 Test Fail				FAN8TestFail			風扇8測試失敗		風8測失敗
199	32		15		Heater1 Test Fail			Heater1TestFail			加熱器1測試失敗		加熱1測失敗
200	32		15		Heater2 Test Fail			Heater2TestFail			加熱器2測試失敗		加熱2測失敗
201	32		15		Fan is Test					Fan is Test				風扇在測試			風扇在測試
202	32		15		Heater is Test				Heater is Test			加熱器在測試		加熱器在測
203	32		15		Version 106					Version 106				106版本				106版本
204	32		15		Fan1 DownLimit Speed			Fan1 DnLmt Spd			Fan1下限速度		Fan1 DnLmt Spd
205	32		15		Fan2 DownLimit Speed			Fan2 DnLmt Spd			Fan2下限速度		Fan2 DnLmt Spd
206	32		15		Fan3 DownLimit Speed			Fan3 DnLmt Spd			Fan3下限速度		Fan3 DnLmt Spd
