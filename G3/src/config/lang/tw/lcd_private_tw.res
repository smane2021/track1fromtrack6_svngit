#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# ABBR_IN_EN: Abbreviated English name
# ABBR_IN_LOCALE: Abbreviated locale name
# ITEM_DESCRIPTION: The description of the resource item
#
[LOCALE_LANGUAGE]
tw

#1. Define the number of the self define multi language display items
[SELF_DEFINE_LANGUAGE_ITEM_NUMBER]
58

[SELF_DEFINE_LANGUAGE_ITEM_INFO]
#Sequence ID	#RES_ID		MAX_LEN_OF_BYTE_ABBR	ABBR_IN_EN		ABBR_IN_LOCALE		ITEM_DESCRIPTION
1		1		32			Main Menu		主菜單			Main Menu
2		2		32			Status			狀態		Running Info
3		3		32			Manual			手冊		Maintain
4		4		32			Settings		設定值		Parameter Set
5		5		32			ECO Mode		節能模式		Energy Saving Parameter Set
6		6		32			Quick Settings		快速設定		Quick Settings Menu
7		7		32			Quick Settings		快速設定		Quick Settings Menu
8		8		32			Test Menu 1		測試菜單1		Menu for self test
9		9		32			Test Menu 2		測試菜單2		Menu for self test
10		10		32			Man/Auto Set		手動/自動設定		Man/Auto Set in Maintain SubMenu
#
11		11		32			Select User		選擇用戶		Select user in password input screen
12		12		32			Enter Password		輸入密碼		Enter password in password input screen
#
13		13		32			Slave Settings		從站設置		Slave Parameter Set
#
21		21		32			Active Alarms		主動警報		Active Alarms
22		22		32			Alarm History		報警記錄		Alarm History
23		23		32			No Active Alarm		無活動警報		No Active Alarm
24		24		32			No Alarm History	無報警記錄		No Alarm History
#
31		31		32			Acknowledge Info	確認信息		Acknowledge Info
32		32		32			ENT Confirm		ENT確認		ENT to run
33		33		32			ESC Cancel		ESC取消		ESC Quit
34		34		32			Prompt Info		提示信息		Prompt Info
35		35		32			Password Error		密碼錯誤		Password Error!
36		36		32			ESC or ENT Ret		ESC或ENT Ret		ESC or ENT Ret
37		37		32			No Privilege		無特權		No Privilege
38		38		32			No Item Info		沒有項目信息		No Item Info
39		39		32			Switch to Next		切換到下一個		Switch To Next equip
40		40		32			Switch to Prev		切換到上一個		Switch To Previous equip
41		41		32			Disabled Set		禁用集		Disabled Set
42		42		32			Disabled Ctrl		禁用Ctrl		Disabled Ctrl
43		43		32			Conflict Setting	衝突設定		Conflict setting of signal relationship
44		44		32			Failed to Set		設置失敗		Failed to Control or set
45		45		32			HW Protect		硬件保護		Hardware Protect status
46		46		32			Reboot System		重啟系統		Reboot System
47		47		32			App is Auto		應用為自動		App is Auto configing
48		48		32			Configuring		配置中		App is Auto configing
49		49		32			Copying File		複製文件		Copy config file
50		50		32			Please wait...		請稍候...		Please Wait...
51		51		32			Switch to Set		切換至設定			Switch to Set Alarm Level or Grade
52		52		32			DHCP is Open		DHCP已打開
53		53		32			Cannot set.		無法設定。
54		54		32			Download Entire	下載整個		Download Config File completely
55		55		32			Reboot Validate	重新啟動驗證		Validate after reboot
#
#
#����Barcode��̖����ID���ô��256
61		61		32			Sys Inventory		系統庫存		Product info of Devices
62		62		32			Device Name		設備名稱		Device Name
63		63		32			Part Number		零件號		Part Number
64		64		32			Product Ver		產品版本		HW Version
65		65		32			SW Version		軟件版本		SW Version
66		66		32			Serial Number		序列號		Serial Number
#
#
80		80		32			Traditional Chinese	繁體中文		Traditional Chinese
81		81		32			Reboot Validate		重新啟動驗證		Reboot Validate
#
82		82		32			Alm Severity		嚴重程度		Alarm Grade
83		83		32			None			沒有			No Alarm
84		84		32			Observation		觀察		Observation alarm
85		85		32			Major			重大的		Major alarm
86		86		32			Critical		危急		Critical alarm
#
87		87		32			Alarm Relay		報警繼電器		Alarm Relay Settings
88		88		32			None			沒有			No Relay Output
89		89		32			Relay 1			繼電器1			Relay Output 1 of IB
90		90		32			Relay 2			繼電器2			Relay Output 2 of IB
91		91		32			Relay 3			繼電器3			Relay Output 3 of IB
92		92		32			Relay 4			繼電器4			Relay Output 4 of IB
93		93		32			Relay 5			繼電器5			Relay Output 5 of IB
94		94		32			Relay 6			繼電器6			Relay Output 6 of IB
95		95		32			Relay 7			繼電器7			Relay Output 7 of IB
96		96		32			Relay 8			繼電器8			Relay Output 8 of IB
97		97		32			Relay 9			繼電器9			Relay Output 1 of EIB
98		98		32			Relay 10		繼電器10		Relay Output 2 of EIB
99		99		32			Relay 11		繼電器11		Relay Output 3 of EIB
100		100		32			Relay 12		繼電器12		Relay Output 4 of EIB
101		101		32			Relay 13		繼電器13		Relay Output 5 of EIB
#
102		102		32			Alarm Param		報警參數		Alarm Param
103		103		32			Alarm Voice		報警聲音		Alarm Voice
104		104		32			Block Alarm		阻止警報		Block Alarm
105		105		32			Clr Alm Hist		Clr Alm Hist		Clear History alarm
106		106		32			Yes			是			Yes
107		107		32			No			沒有			No
#
108		108		32			Alarm Voltage		報警電壓		Alarm Voltage Level of IB
#
#
121		121		32			Sys Settings		系統設置		System Param
122		122		32			Language		語言		Current language displayed in LCD screen
123		123		32			Time Zone		時區		Time Zone
124		124		32			Date			日期		Set ACU+ Date, according to time zone
125		125		32			Time			時間		Set Time, accoring to time zone
126		126		32			Reload Config		重新加載配置		Reload Default Configuration
127		127		32			Keypad Voice		鍵盤語音		Keypad Voice
128		128		32			Download Config		下載配置		Download config file
129		129		32			Auto Config		自動配置		Auto config
#
#
141		141		32			Communication		通訊		Communication Parameter
#
142		142		32			DHCP			DHCP服務器		DHCP Function
143		143		32			IP Address		IP地址			IP Address of ACU+
144		144		32			Subnet Mask		子網掩碼		Subnet Mask of ACU+
145		145		32			Default Gateway		默認網關		Default Gateway of ACU+
#
146		146		32			Self Address		自我地址		Self Addr
147		147		32			Port Type		端口類型		Connection Mode
148		148		32			Port Param		港口參數		Port Parameter
149		149		32			Alarm Report 		報警報告		Alarm Report 
150		150		32			Dial Times 		撥號時間		Dialing Attempt Times
151		151		32			Dial Interval		撥號間隔		Dialing Interval
152		152		32			1st Phone Num		第一電話號碼		First Call Back Phone  Num
153		153		32			2nd Phone Num		第二電話號碼		Second Call Back Phone  Num
154		154		32			3rd Phone Num		第三電話號碼		Third Call Back Phone  Num
#
161		161		32			Enabled			已啟用			Enable DHCP
162		162		32			Disabled		殘障人士			Disable DHCP
163		163		32			Error			錯誤			DHCP function error
164		164		32			RS-232			RS-232			YDN23 Connection Mode RS-232
165		165		32			Modem			Modem			YDN23 Connection Mode MODEM
166		166		32			Ethernet		Ethernet		YDN23 Connection Mode Ethernet
167		167		32			5050			5050			Ethernet Port Number
168		168		32			2400,n,8,1		2400,n,8,1		Serial Port Parameter
169		169		32			4800,n,8,1		4800,n,8,1		Serial Port Parameter
170		170		32			9600,n,8,1		9600,n,8,1		Serial Port Parameter
171		171		32			19200,n,8,1		19200,n,8,1		Serial Port Parameter
172		172		32			38400,n,8,1		38400,n,8,1		Serial Port Parameter
#
# The next level of Battery Group
201		201		32			Basic			基本的		Sub Menu Resouce of BattGroup Para Setting
202		202		32			Charge			收費		Sub Menu Resouce of BattGroup Para Setting
203		203		32			Test			늳؜yԇ		Sub Menu Resouce of BattGroup Para Setting
204		204		32			Temp Comp		溫度補償		Sub Menu Resouce of BattGroup Para Setting
205		205		32			Capacity		容量		Sub Menu Resouce of BattGroup Para Setting
# The next level of Power System
206		206		32			General			一般		Sub Menu Resouce of PowerSystem Para Setting
207		207		32			Power Split		功率分配		Sub Menu Resouce of PowerSystem Para Setting
208		208		32			Temp Probe(s)		溫度探頭		Sub Menu Resouce of PowerSystem Para Setting
# ENERGY SAVING
209		209		32			ECO Mode		節能模式		Sub Menu Resouce of ENERGY SAVING
#
#������춣����A�O����ESC���@ʾ�b�ü������YӍ
301		301		32			Serial Num		序列號		Serial Number
302		302		32			HW Ver			硬件版本		Hardware Version
303		303		32			SW Ver			軟件版		Software Version
304		304		32			MAC Addr		MAC地址			MAC Addr
305		305		32			File Sys		文件系統		File System Revision
306		306		32			Device Name		設備名稱		Product Model
307		307		32			Config			設定檔			Solution Config File Version
#
#
501		501		32			LCD Size		LCD尺寸		Set the LCD Height
502		502		32			128x64			128x64			Set the LCD Height to 128 X 64
503		503		32			128x128			128x128			Set the LCD Height to 128 X 128
504		504		32			LCD Rotation		LCD旋轉		Set the LCD Rotation
505		505		32			0 deg			0度			Set the LCD Rotation to 0 degree
506		506		32			90 deg			90度			Set the LCD Rotation to 90 degree
507		507		32			180 deg			180度			Set the LCD Rotation to 180 degree
508		508		32			270 deg			270度			Set the LCD Rotation to 270 degree
#
#
601		601		32			All Rect Ctrl		全部Rect Ctrl
602		602		32			All Rect Set		所有直腸套
#
621		621		32			Rectifier		整流器
622		622		32			Battery			電池
623		623		32			LVD			LVD
624		624		32			Rect AC			矩形交流
625		625		32			Converter		轉換器
626		626		32			SMIO			斯米歐
627		627		32			Diesel			柴油機
628		628		32			Rect Group 2		矩形組2
629		629		32			Rect Group 3		矩形組3
630		630		32			Rect Group 4		矩形組4
631		631		32			All Conv Ctrl		全部轉換Ctrl
632		632		32			All Conv Set		所有轉換集
633		633		32			SMDU			SMDU
#
1001		1001		32			Auto/Manual		自動/手動
1002		1002		32			ECO Mode Set		ECO模式設定
1003		1003		32			FLT/EQ Voltage		FLT / EQ電壓
1004		1004		32			FLT/EQ Change		FLT / EQ變化
1005		1005		32			Temp Comp		溫度補償
1006		1006		32			Work Mode Set		工作模式設定
1007		1007		32			Maintenance		保養
1008		1008		32			Energy Saving		節約能源
1009		1009		32			Alarm Settings		警報設定
1010		1010		32			Rect Settings		矩形設置
1011		1011		32			Batt Settings		電池設置
1012		1012		32			Batt1 Settings		Batt1設置
1013		1013		32			Batt2 Settings		Batt2設置
1014		1014		32			LVD Settings		LVD設置
1015		1015		32			AC Settings		AC設定
1016		1016		32			Template 1		範本1
1017		1017		32			Template 2		範本1
1018		1018		32			Template N		模板N
#
1101		1101		32			Batt1			電池1
1102		1102		32			Batt2			電池2
1103		1103		32			Comp			補償
1104		1104		32			Amb			安布
1105		1105		32			Remain			保持
1106		1106		32			RectNum			矩形


