﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Inverter Group				Invt Group			逆变模块组				逆变模块组
2		32			15			Total Current				Tot Invt Curr		模块总电流				逆变总电流

4		32			15			Inverter Capacity Used			Sys Cap Used		模块组使用容量				系统使用容量
5		32			15			Maximum Capacity Used			Max Cap Used		最大使用容量				最大使用容量
6		32			15			Minimum Capacity Used			Min Cap Used		最小使用容量				最小使用容量
7		32			15			Total Inverters Communicating	Num Invts Comm			通讯正常模块数				通讯正常模块数
8		32			15			Valid Inverters					Valid Inverters			有效模块数					有效模块数
9		32			15			Number of Inverters				Num of Invts			逆变模块数					逆变模块数
10		32			15			Number of Phase1				Number of Phase1		L1数目						L1数目
11		32			15			Number of Phase2				Number of Phase2		L2数目						L2数目
12		32			15			Number of Phase3				Number of Phase3		L3数目						L3数目
13		32			15			Current of Phase1				Current of Phase1		L1电流						L1电流
14		32			15			Current of Phase2				Current of Phase2		L2电流						L2电流
15		32			15			Current of Phase3				Current of Phase3		L3电流						L3电流
16		32			15			Power_kW of Phase1				Power_kW of Phase1		L1功率Kw					L1功率Kw
17		32			15			Power_kW of Phase2				Power_kW of Phase2		L2功率Kw					L2功率Kw
18		32			15			Power_kW of Phase3				Power_kW of Phase3		L3功率Kw					L3功率Kw
19		32			15			Power_kVA of Phase1				Power_kVA of Phase1		L1功率KVA					L1功率KVA
20		32			15			Power_kVA of Phase2				Power_kVA of Phase2		L2功率KVA					L2功率KVA
21		32			15			Power_kVA of Phase3				Power_kVA of Phase3		L3功率KVA					L3功率KVA
22		32			15			Rated Current					Rated Current			额定电流					额定电流
23		32			15			Input Total Energy				Input Energy			输入总电量					输入总电量
24		32			15			Output Total Energy				Output Energy			输出总电量					输出总电量	
25		32			15			Inverter Sys Set ASYNC			Sys Set ASYNC			系统设置异步				系统设置异步
26		32			15			Inverter AC Phase Abnormal		ACPhaseAbno				交流相位异常				交流相位异常
27		32			15			Inverter REPO					REPO					REPO						REPO
28		32			15			Inverter Output Freq ASYNC		OutputFreqASYNC			输出频率异步				输出频率异步
29		32			15			Output On/Off			Output On/Off		模块输出开关机				模块输出开关机
30		32			15			Inverters LED Control			Invt LED Ctrl			模块LED灯控制				模块LED灯控制
31		32			15			Fan Speed				Fan Speed			风扇运行速度				风扇运行速度
32		32			15			Invt Mode AC/DC					Invt Mode AC/DC			交流工作交流/直流				交流工作交流/直流
33		32			15			Output Voltage Level			Output Voltage Level	输出电压等级				输出电压等级
34		32			15			Output Frequency					Output Freq			输出频率				输出频率
35		32			15			Source Ratio					Source Ratio			输入比例					输入比例
36		32			15			Normal					Normal			正常					正常
37		32			15			Fail					Fail			故障					故障
38		32			15			Switch Off All				Switch Off All		关所有模块				关所有模块
39		32			15			Switch On All				Switch On All		开所有模块				开所有模块
42		32			15			All Flashing				All Flashing		全部灯闪				全部灯闪
43		32			15			Stop Flashing				Stop Flashing		全部灯不闪				全部灯不闪
44		32			15			Full Speed				Full Speed		全速					全速
45		32			15			Automatic Speed				Auto Speed		自动调速				自动调速
54		32			15			Disabled				Disabled		否					否
55		32			15			Enabled					Enabled			是					是
56		32			15			100V					100V			100V				100V
57		32			15			110V					110V			110V				110V
58		32			15			115V					115V			115V				115V
59		32			15			120V					120V			120V				120V
60		32			15			125V					125V			125V				125V
61		32			15			200V					200V			200V				200V
62		32			15			208V					208V			208V				208V
63		32			15			220V					220V			220V				220V
64		32			15			230V					230V			230V				230V
65		32			15			240V					240V			240V				240V
66		32			15			Auto					Auto			Auto				Auto
67		32			15			50Hz					50Hz			50Hz				50Hz
68		32			15			60Hz					60Hz			60Hz				60Hz
69		32			15			Input DC Current		Input DC Current		输入直流电流				输入直流电流
70		32			15			Input AC Current		Input AC Current		输入交流电流				输入交流电流
71		32			15			Inverter Work Status	InvtWorkStatus			逆变工作状态				逆变工作状态
72		32			15			Off					Off						关							关
73		32			15			No					No						否							否
74		32			15			Yes					Yes						是							是
75		32			15			Part On				Part On					部分开						部分开
76		32			15			All On				All On					全开						全开

77		32			15			DC Low Voltage Off		DCLowVoltOff		直流低压关			直流低压关
78		32			15			DC Low Voltage On		DCLowVoltOn		直流低压开启			直流低压开启
79		32			15			DC High Voltage Off	DCHiVoltOff		直流高压关			直流高压关
80		32			15			DC High Voltage On	DCHighVoltOn		直流高压开启			直流高压开启
81		32			15			Last Inverters Quantity		Last Invts Qty		原模块数				原模块数
82		32			15			Inverter Lost				Inverter Lost		模块丢失				模块丢失
83		32			15			Inverter Lost				Inverter Lost		模块丢失				模块丢失
84		32			15			Clear Inverter Lost Alarm		Clear Invt Lost		清除模块丢失告警			清模块丢失告警
86		32			15			Confirm Inverter ID/Phase			Confirm ID/PH			确认模块位置/阶段			确认位置/PH
92		32			15			E-Stop Function				E-Stop Function		E-Stop功能				E-Stop功能
93		32			15			AC Phases				AC Phases		交流相数				交流相数
94		32			15			Single Phase				Single Phase		单相					单相
95		32			15			Three Phases				Three Phases		三相					三相
101		32			15			Sequence Start Interval			Start Interval		顺序开机间隔				顺序开机间隔
104		64			15			All Inverters Communication Failure		AllInvtCommFail		所有模块通信中断			所有模块通信断
113		32			15			Invt Info Change (M/S Internal Use)	InvtInfo Change		逆变信息发生改变			逆变信息已改变
119		32			15			Clear Inverter Comm Fail Alarm		ClrInvtCommFail		清除逆变通信中断			清逆变通信中断
126		32			15			None					None			无					无
143		32			15			Existence State				Existence State		是否存在				是否存在
144		32			15			Existent				Existent		存在					存在
145		32			15			Not Existent				Not Existent		不存在					不存在
146		32			15			Total Output Power			Output Power		总输出功率				总输出功率
147		32			15			Total Slave Rated Current		Total RatedCurr		总额定电流				总额定电流
148		32			15			Reset Inverter IDs			Reset Invt IDs		清除模块位置号				清除模块位置号
149		32			15			HVSD Voltage Difference (24V)		HVSD Volt Diff		HVSD压差(24V)				HVSD压差
150		32			15			Normal Update				Normal Update		普通升级				普通升级
151		32			15			Force Update				Force Update		强制升级				强制升级
152		32			15			Update OK Number			Update OK Num		升级成功数				升级成功数
153		32			15			Update State				Update State		升级状态				升级状态
154		32			15			Updating				Updating		升级中					升级中
155		32			15			Normal Successful			Normal Success		普通升级成功				普通升级成功
156		32			15			Normal Failed				Normal Fail		普通升级失败				普通升级失败
157		32			15			Force Successful			Force Success		强制升级成功				强制升级成功
158		32			15			Force Failed				Force Fail		强制升级失败				强制升级失败
159		32			15			Communication Time-Out			Comm Time-Out		通讯超时				通讯超时
160		32			15			Open File Failed			Open File Fail		打开文件失败				打开文件失败
161		32			15			AC mode						AC mode					交流模式				交流模式
162		32			15			DC mode						DC mode					直流模式				直流模式

#163		32			15			Safety mode					Safety mode				安全模式				安全模式
#164		32			15			Idle  mode					Idle  mode				空闲模式				空闲模式
165		32			15			Max used capacity			Max used capacity		最大容量				最大容量
166		32			15			Min used capacity			Min used capacity		最小容量				最小容量
167		32			15			Average Voltage				Invt Voltage			平均电压				平均电压
168		32			15			Invt Redundancy Number		Invt Redu Num			逆变冗余个数			逆变冗余个数
169		32			15			Inverter High Load			Inverter High Load		逆变负载高				逆变负载高
170		32			15			Rated Power					Rated Power				额定功率				额定功率

171		32			15			Clear Fault					Clear Fault				清除告警				清除告警
172		32			15			Reset Energy				Reset Energy			复位电能				复位电能

173		32			15			Syncronization Phase Failure			SynErrPhaType			相位配置不同步			相位配置不同步
174		32			15			Syncronization Voltage Failure			SynErrVoltType			电压等级不同步			电压等级不同步
175		32			15			Syncronization Frequency Failure			SynErrFreqLvl			频率等级不同步			频率等级不同步
176		32			15			Syncronization Mode Failure			SynErrWorkMode			运行模式不同步			运行模式不同步
177		32			15			Syn Err Low Volt TR			SynErrLVoltTR			低压转移不同步			低压转移不同步
178		32			15			Syn Err Low Volt CB			SynErrLVoltCB			低压回归不同步			低压回归不同步
179		32			15			Syn Err High Volt TR		SynErrHVoltTR			高压转移不同步			高压转移不同步
180		32			15			Syn Err High Volt CB		SynErrHVoltCB			高压回归不同步			高压回归不同步
181		32			15			Set Para To Invt			SetParaToInvt			向逆变同步参数			向逆变设参数
182		32			15			Get Para From Invt			GetParaFromInvt			从逆变读取参数			从逆变读参数
183		32			15			Power Used					Power Used				功率百分比				功率百分比
184		32			15			Input AC Voltage			Input AC Volt			输入交流电压 			输入交流电压	
#185     32          15          Input AC Voltage Phase A       InpVoltPhA           逆变器输入相电压A      逆变器输入相电压A
185		32			15			Input AC Phase A			Input AC VolPhA			输入交流电压 			输入交流电压	
186		32			15			Input AC Phase B			Input AC VolPhB			输入交流电压 			输入交流电压
187		32			15			Input AC Phase C			Input AC VolPhC			输入交流电压 			输入交流电压		
188		32			15			Total output voltage			Total output V			总输出V			总输出V
189		32			15			Power in kW					PowerkW				上电kW				上电kW
190		32			15			Power in kVA					PowerkVA				上电kVA				上电kVA
197		32			15			Maximum Power Capacity					Max Power Cap				最大功率容量				最大功率容量
