﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			SMBRC Group				SMBRC Group		SMBRC组					SMBRC组
2		32			15			Standby					Standby			待命					待命
3		32			15			Refresh					Refresh			刷新					刷新
4		32			15			Setting Refresh				Setting Refresh		設置信號刷新				設置信號刷新
5		32			15			E-Stop					E-Stop			E-Stop					E-Stop
6		32			15			Yes					Yes			是					是
7		32			15			Existence State				Existence State		是否存在				是否存在
8		32			15			Existent				Existent		存在					存在
9		32			15			Not Existent				Not Existent		不存在					不存在
10		32			15			Num of SM BRCs				Num of SM BRCs		SM-BRC數量				SM-BRC數量
11		32			15			SMBRC Config Changed			Cfg Changed		配置是否改變				配置是否改變
12		32			15			Not Changed				Not Changed		未改變					未改變
13		32			15			Changed					Changed			已改變					已改變
14		32			15			SMBRC Configuration			SMBRC Cfg		SM-BRC配置				SM-BRC配置
15		32			15			SMBRC Configuration Number		SMBRC Cfg Num		SMBRC配置值				SMBRC配置值
16		32			15			SMBRC Interval				SMBRC Interval		SMBRC阻抗間隔				SMBRC阻抗間隔
17		32			15			SMBRC Reset Test			SMBRC Rst Test		SMBRC阻抗測試				SMBRC阻抗測試
18		32			15			Start					Start			開始					開始
19		32			15			Stop					Stop			停止					停止
20		32			15			Cell Voltage High			Cell Volt High		節電壓上限				節電壓上限
21		32			15			Cell Voltage Low			Cell Volt Low		節電壓下限				節電壓下限
22		32			15			Cell Temperature High			Cell Temp High		節溫度上限				節溫度上限
23		32			15			Cell Temperature Low			Cell Temp Low		節溫度下限				節溫度下限
24		32			15			String Current High			String Curr Hi		串電流上限				串電流上限
25		32			15			Ripple Current High			Ripple Curr Hi		紋波電流上限				紋波電流上限
26		32			15			High Cell Resistance			Hi Cell Resist		阻抗上限				阻抗上限
27		32			15			Low Cell Resistance			Low Cell Resist		阻抗下限				阻抗下限
28		32			15			High Intercell Resistance		Hi InterResist		內阻上限				內阻上限
29		32			15			Low Intercell Resistance		Low InterResist		內阻下限				內阻下限
30		32			15			String Current Low			String Curr Low		串電流下限				串電流下限
31		32			15			Ripple Current Low			Ripple Curr Low		紋波電流下限				紋波電流下限
