﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Solar Converter Group			Solar ConvGrp		太陽能模塊组				太陽能模塊组
2		32			15			Total Current				Total Current		模塊總電流				模塊總電流
3		32			15			Average Voltage				Average Voltage		輸出電壓平均值				輸出電壓平均值
4		32			15			System Capacity Used			Sys Cap Used		模塊组使用容量				模塊组使用容量
5		32			15			Maximum Used Capacity			Max Cap Used		最大使用容量				最大使用容量
6		32			15			Minimum Used Capacity			Min Cap Used		最小使用容量				最小使用容量
7		36			15			Total Solar Converters Communicating	Num Solar Convs		通訊正常模塊數				通訊正常模塊數
8		32			15			Valid Solar Converter			Valid SolarConv		有效模塊數				有效模塊數
9		32			15			Number of Solar Converters		SolConv Num		整流模塊數				太陽能模塊數
10		32			15			Solar Converter AC Failure State	SolConv ACFail		交流失電状態				交流失電状態
11		34			15			Solar Converter(s) Failure Status	SolarConv Fail		多模塊故障				多模塊故障
12		32			15			Solar Converter Current Limit		SolConvCurrLmt		模塊限流				模塊限流
13		32			15			Solar Converter Trim			Solar Conv Trim		模塊調壓				模塊調壓
14		32			15			DC On/Off Control			DC On/Off Ctrl		模塊直流開關機				模塊直流開關機
16		32			15			Solar Converter LED Control		SolConv LEDCtrl		模塊LED燈控制				模塊LED燈控制
17		32			15			Fan Speed Control			Fan Speed Ctrl		風扇運行速度				風扇運行速度
18		32			15			Rated Voltage				Rated Voltage		額定輸出電壓				額定輸出電壓
19		32			15			Rated Current				Rated Current		額定輸出電流				額定輸出電流
20		32			15			High Voltage Shutdown Limit		HVSD Limit		直流輸出過壓點				直流輸出過壓點
21		32			15			Low Voltage Limit			Low Volt Limit		直流輸出欠壓點				直流輸出欠壓點
22		32			15			High Temperature Limit			High Temp Limit		模塊過溫點				模塊過溫點
24		32			15			HVSD Restart Time			HVSD Restart T		過壓重啟時間				過壓重啟時間
25		32			15			Walk-In Time				Walk-In Time		帶載軟啟動時間				帶載軟啟動時間
26		32			15			Walk-In					Walk-In			帶載軟啟動允許				帶載軟啟動允許
27		32			15			Minimum Redundancy			Min Redundancy		最小冗余				最小冗余
28		32			15			Maximum Redundancy			Max Redundancy		最大冗余				最大冗余
29		32			15			Turn Off Delay				Turn Off Delay		關機延時				關機延時
30		32			15			Cycle Period				Cycle Period		循環開關機周期				循環開關機周期
31		32			15			Cycle Activation Time			Cyc Active Time		循環開關機執行時刻			開關機執行時刻
33		32			15			Multiple Solar Converter Failure	MultSolConvFail		多模塊故障				多模塊故障
36		32			15			Normal					Normal			正常					正常
37		32			15			Failure					Failure			故障					故障
38		32			15			Switch Off All				Switch Off All		關所有模塊				關所有模塊
39		32			15			Switch On All				Switch On All		開所有模塊				開所有模塊
42		32			15			All Flashing				All Flashing		全部燈閃				全部燈閃
43		32			15			Stop Flashing				Stop Flashing		全部燈不閃				全部燈不閃
44		32			15			Full Speed				Full Speed		全速					全速
45		32			15			Automatic Speed				Auto Speed		自動調速				自動調速
46		32			32			Current Limit Control			Curr Limit Ctrl		限流點控制				限流點控制
47		32			32			Full Capability Control			Full Cap Ctrl		滿容量運行				滿容量運行
54		32			15			Disabled				Disabled		否					否
55		32			15			Enabled					Enabled			是					是
68		32			15			ECO Mode				ECO Mode		ECO模式使能				ECO模式使能
72		32			15			Turn On when AC Over Voltage		Turn On ACOverV		交流過壓開機				交流過壓開機
73		32			15			No					No			否					否
74		32			15			Yes					Yes			是					是
77		32			15			Pre-CurrLimit Turn-On			Pre-Curr Limit		開模塊預限流				開模塊預限流
78		32			15			Solar Converter Power Type		SolConv PwrType		模塊供電類型				模塊供電類型
79		32			15			Double Supply				Double Supply		雙供電					雙供電
80		32			15			Single Supply				Single Supply		單供電					單供電
81		32			15			Last Solar Converter Quantity		LastSolConvQty		原模塊數				原模塊數
83		32			15			Solar Converter Lost			Solar Conv Lost		太陽能模塊丢失				太陽能模塊丢失
84		32			15			Clear Solar Converter Lost Alarm	ClearSolarLost		清太陽能模塊丢失告警			清Solar模塊丢失
85		32			15			Clear					Clear			清除					清除
86		32			15			Confirm Solar Converter ID		Confirm ID		確認模塊位置				確認位置
87		32			15			Confirm					Confirm			確認位置				確認位置
88		32			15			Best Operating Point			Best Oper Point		最佳工作點				最佳工作點
89		32			15			Solar Converter Redundancy		SolConv Redund		節能運行				節能運行
90		32			15			Load Fluctuation Range			Fluct Range		負載波動率				負載波動率
91		32			15			System Energy Saving Point		Energy Save Pt		系統節能點				系統節能點
92		32			15			E-Stop Function				E-Stop Function		E-Stop功能				E-Stop功能
94		32			15			Single Phase				Single Phase		單相					單相
95		32			15			Three Phases				Three Phases		三相					三相
96		32			15			Input Current Limit			Input Curr Lmt		輸入電流限值				輸入電流限值
97		32			15			Double Supply				Double Supply		雙供電					雙供電
98		32			15			Single Supply				Single Supply		單供電					單供電
99		32			15			Small Supply				Small Supply		Small模塊供電方式			Small供電方式
100		32			15			Restart on HVSD				Restart on HVSD		直流過壓復位				直流過壓復位
101		32			15			Sequence Start Interval			Start Interval		順序開機間隔				順序開機間隔
102		32			15			Rated Voltage				Rated Voltage		額定輸出電壓				額定輸出電壓
103		32			15			Rated Current				Rated Current		額定輸出電流				額定輸出電流
104		32			15			All Solar Converters Comm Fail		SolarsComFail		所有模塊通信中斷			所有模塊通信斷
105		32			15			Inactive				Inactive		否					否
106		32			15			Active					Active			是					是
112		32			15			Rated Voltage (Internal Use)		Rated Voltage		額定輸出電壓(Internal Use)		額定輸出電壓
113		32			15			Solar Converter Info Change (M/S Internal Use)	SolarConvInfoChange	模塊信息發生改變			模塊信息已改變
114		32			15			MixHE Power				MixHE Power		高功率模塊是否降額			高功率模塊降額
115		32			15			Derated					Derated			降額					降額
116		32			15			Non-Derated				Non-Derated		不降					不降
117		32			15			All Solar Converter ON Time		SolarConvONTime		模塊烘幹時間				模塊烘幹時間
118		32			15			All Solar Converter are On		AllSolarConvOn		模塊正在烘幹				模塊正在烘幹
119		39			15			Clear Solar Converter Comm Fail Alarm	ClrSolCommFail		清模塊通訊中斷告警			清模塊通訊中斷
120		32			15			HVSD					HVSD			HVSD使能				HVSD使能
121		32			15			HVSD Voltage Difference			HVSD Volt Diff		HVSD壓差				HVSD壓差
122		32			15			Total Rated Current			Total Rated Cur		工作模塊總額定電流			工作模塊總額定
123		32			15			Diesel Generator Power Limit		DG Pwr Lmt		油機限功率使能				油機限功率使能
124		32			15			Diesel Generator Digital Input		Diesel DI Input		油機幹接點輸入				油機幹接點輸入
125		32			15			Diesel Gen Power Limit Point Set	DG Pwr Lmt Pt		油機限功率點				油機限功率點
126		32			15			None					None			無					無
140		32			15			Solar Converter Delta Voltage		SolConvDeltaVol		MPPT状態電壓差				MPPT電壓差
141		32			15			Solar Status				Solar Status		光照状態				光照状態
142		32			15			Day					Day			白天					白天
143		32			15			Night					Night			黑夜					黑夜
144		32			15			Existence State				Existence State		是否存在				是否存在
145		32			15			Existent				Existent		存在					存在
146		32			15			Not Existent				Not Existent		不存在					不存在
147		32			15			Total Input Current			Input Current		總輸入電流				總輸入電流
148		32			15			Total Input Power			Input Power		輸入總功率				輸入總功率
149		32			15			Total Rated Current			Total Rated Cur		工作模塊總額定電流			工作模塊總額定
150		32			15			Total Output Power			Output Power		總輸出功率				總輸出功率
151		32			15			Reset Solar Converter IDs		Reset Solar IDs		清除模塊位置號				清除模塊位置號
152		32			15			Clear Solar Converter Comm Fail		ClrSolarCommFail	清太陽能模塊通訊中斷			清Solar通訊中斷
