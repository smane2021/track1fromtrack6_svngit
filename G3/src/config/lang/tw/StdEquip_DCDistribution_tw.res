﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			DC Distribution				DC Distr		直流配電				直流配電
2		32			15			DC Voltage				DC Voltage		直流電壓				直流電壓
3		32			15			Load Current				Load Current		負載電流				負載電流
4		32			15			Load Shunt				Load Shunt		負載分流器使能				負載分流器使能
5		32			15			Disabled				Disabled		禁止					禁止
6		32			15			Enabled					Enabled			使能					使能
7		32			15			Shunt Current				Shunt Current		分流器額定電流				分流器額定電流
8		32			15			Shunt Voltage				Shunt Voltage		分流器額定電壓				分流器額定電壓
9		32			15			Load Shunt Exist			LoadShuntExist		測量負載電流				測量負載電流
10		32			15			Yes					Yes			是					是
11		32			15			No					No			否					否
12		32			15			Over Voltage 1				Over Voltage 1		直流電壓高				直流電壓高
13		32			15			Over Voltage 2				Over Voltage 2		直流過壓				直流過壓
14		32			15			Under Voltage 1				Under Voltage 1		直流電壓低				直流電壓低
15		32			15			Under Voltage 2				Under Voltage 2		直流欠壓				直流欠壓
16		32			15			Over Voltage 1 (24V)			24V Over Volt1		直流電壓高(24V)				直流電壓高(24V)
17		32			15			Over Voltage 2 (24V)			24V Over Volt2		直流過壓(24V)				直流過壓(24V)
18		32			15			Under Voltage 1 (24V)			24V Under Volt1		直流電壓低(24V)				直流電壓低(24V)
19		32			15			Under Voltage 2 (24V)			24V Under Volt2		直流欠壓(24V)				直流欠壓(24V)
20		32			15			Total Load Current			TotalLoadCurr		總負載電流				總負載電流
21		32			15			Load Alarm Flag				Load Alarm Flag		負載告警標誌				負載告警標誌
22		32			15			Current High Current			Curr Hi			電流過流				電流過流
23		32			15			Current Very High Current		Curr Very Hi		電流過過流				電流過過流
500		32			15			Current Break Size				Curr1 Brk Size		Current Break Size				Curr1 Brk Size			
501		32			15			Current High 1 Current Limit	Curr1 Hi1 Limit		Current High 1 Current Limit	Curr1 Hi1 Limit			
502		32			15			Current High 2 Current Limit	Curr1 Hi2 Lmt		Current High 2 Current Limit	Curr1 Hi2 Lmt			
503		32			15			Current High 1 Curr				Curr Hi1Cur			Current High 1 Curr				Curr Hi1Cur			
504		32			15			Current High 2 Curr				Curr Hi2Cur			Current High 2 Curr				Curr Hi2Cur		
