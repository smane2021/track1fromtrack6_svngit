﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			LVD Group				LVD Group		LVD组					LVD组
2		32			15			Battery LVD				Battery LVD		電池下電				電池下電
3		32			15			None					None			無					無
4		32			15			LVD 1					LVD 1			LVD1					LVD1
5		32			15			LVD 2					LVD 2			LVD2					LVD2
6		32			15			LVD 3					LVD 3			LVD3					LVD3
7		32			15			AC Fail Required			ACFail Required		交流停電才下電				交流停電才下電
8		32			15			Disabled				Disabled		否					否
9		32			15			Enabled					Enabled			是					是
10		32			15			Number of LVD				Number of LVD		LVD數量					LVD數量
11		32			15			0					0			0					0
12		32			15			1					1			1					1
13		32			15			2					2			2					2
14		32			15			3					3			3					3
15		32			15			HTD Point				HTD Point		高溫下電點				高溫下電點
16		32			15			HTD Reconnect Point			HTD Recon Point		高溫上電點				高溫上電點
17		32			15			HTD Temperature Sensor			HTD Temp Sensor		溫度采集點				溫度采集點
18		32			15			Ambient Temperature			Ambient Temp		環境溫度				環境溫度
19		32			15			Battery Temperature			Battery Temp		電池溫度				電池溫度
20		32			15			Temperature 1				Temperature 1		溫度1					溫度1
21		32			15			Temperature 2				Temperature 2		溫度2					溫度2
22		32			15			Temperature 3				Temperature 3		溫度3					溫度3
23		32			15			Temperature 4				Temperature 4		溫度4					溫度4
24		32			15			Temperature 5				Temperature 5		溫度5					溫度5
25		32			15			Temperature 6				Temperature 6		溫度6					溫度6
26		32			15			Temperature 7				Temperature 7		溫度7					溫度7
27		32			15			Existence State				Existence State		是否存在				是否存在
28		32			15			Existent				Existent		存在					存在
29		32			15			Not Existent				Not Existent		不存在					不存在
31		32			15			LVD 1					LVD 1			LVD1允許				LVD1允許
32		32			15			LVD 1 Mode				LVD 1 Mode		LVD1方式				LVD1方式
33		32			15			LVD 1 Voltage				LVD 1 Voltage		LVD1電壓				LVD1電壓
34		32			15			LVD 1 Reconnect Voltage			LVD1 Recon Volt		LVD1上電電壓				LVD1上電電壓
35		32			15			LVD 1 Reconnect Delay			LVD1 ReconDelay		LVD1上電延遲				LVD1上電延遲
36		32			15			LVD 1 Time				LVD 1 Time		LVD1時間				LVD1時間
37		32			15			LVD 1 Dependency			LVD1 Dependency		LVD1依賴關系				LVD1依賴關系
41		32			15			LVD 2					LVD 2			LVD2允許				LVD2允許
42		32			15			LVD 2 Mode				LVD 2 Mode		LVD2方式				LVD2方式
43		32			15			LVD 2 Voltage				LVD 2 Voltage		LVD2電壓				LVD2電壓
44		32			15			LVD 2 Reconnect Voltage			LVD2 Recon Volt		LVD2上電電壓				LVD2上電電壓
45		32			15			LVD 2 Reconnect Delay			LVD2 ReconDelay		LVD2上電延遲				LVD2上電延遲
46		32			15			LVD 2 Time				LVD 2 Time		LVD2時間				LVD2時間
47		32			15			LVD 2 Dependency			LVD2 Dependency		LVD2依賴關系				LVD2依賴關系
51		32			15			Disabled				Disabled		禁止					禁止
52		32			15			Enabled					Enabled			允許					允許
53		32			15			Voltage					Voltage			電壓方式				電壓方式
54		32			15			Time					Time			時間方式				時間方式
55		32			15			None					None			無					無
56		32			15			LVD 1					LVD 1			LVD1					LVD1
57		32			15			LVD 2					LVD 2			LVD2					LVD2
103		32			15			High Temp Disconnect 1			HTD 1			HTD1高溫下電允許			HTD1下電允許
104		32			15			High Temp Disconnect 2			HTD 2			HTD2高溫下電允許			HTD2下電允許
105		32			15			Battery LVD				Battery LVD		電池下電				電池下電
106		32			15			No Battery				No Battery		無電池					無電池
107		32			15			LVD 1					LVD 1			LVD1					LVD1
108		32			15			LVD 2					LVD 2			LVD2					LVD2
109		32			15			Battery Always On			Batt Always On		有電池但不下電				有電池但不下電
110		32			15			LVD Contactor Type			LVD Type		下電接觸器類型				接觸器類型
111		32			15			Bistable				Bistable		雙穩					雙穩
112		32			15			Mono-Stable				Mono-Stable		單穩					單穩
113		32			15			Mono w/Sample				Mono w/Sample		單穩帶回采				單穩帶回采
116		32			15			LVD 1 Disconnect			LVD1 Disconnect		LVD1下電				LVD1下電
117		32			15			LVD 2 Disconnect			LVD2 Disconnect		LVD2下電				LVD2下電
118		32			15			LVD 1 Mono w/Sample			LVD1 Mono Sampl		LVD1單穩帶回采				LVD1單穩帶回采
119		32			15			LVD 2 Mono w/Sample			LVD2 Mono Sampl		LVD2單穩帶回采				LVD2單穩帶回采
125		32			15			State					State			状態					状態
126		32			15			LVD 1 Voltage (24V)			LVD 1 Voltage		LVD1電壓(24V)				LVD1電壓
127		32			15			LVD 1 Reconnect Voltage (24V)		LVD1 Recon Volt		LVD1上電電壓(24V)			LVD1上電電壓
128		32			15			LVD 2 Voltage (24V)			LVD 2 Voltage		LVD2電壓(24V)				LVD2電壓
129		32			15			LVD 2 Reconnect Voltage (24V)		LVD2 Recon Volt		LVD2上電電壓(24V)			LVD2上電電壓
130		32			15			LVD 1 Control				LVD 1			LVD1控制				LVD1控制
131		32			15			LVD 2 Control				LVD 2			LVD2控制				LVD2控制
132		32			15			LVD 1 Reconnect				LVD 1 Reconnect		LVD1上電				LVD1上電
133		32			15			LVD 2 Reconnect				LVD 2 Reconnect		LVD2上電				LVD2上電
134		32			15			HTD Point				HTD Point		高溫下電點				高溫下電點
135		32			15			HTD Reconnect Point			HTD Recon Point		高溫上電點				高溫上電點
136		32			15			HTD Temperature Sensor			HTD Temp Sensor		溫度采集點				溫度采集點
137		32			15			Ambient Temperature			Ambient Temp		環境溫度				環境溫度
138		32			15			Battery Temperature			Battery Temp		電池溫度				電池溫度
139		32			15			LVD Synchronize				LVD Synchronize		同步LVD板				同步LVD板
140		32			15			Relay for LVD3				Relay for LVD3		LVD3繼電器				LVD3繼電器
141		32			15			Relay Output 1				Relay Output 1		繼電器1					繼電器1
142		32			15			Relay Output 2				Relay Output 2		繼電器2					繼電器2
143		32			15			Relay Output 3				Relay Output 3		繼電器3					繼電器3
144		32			15			Relay Output 4				Relay Output 4		繼電器4					繼電器4
145		32			15			Relay Output 5				Relay Output 5		繼電器5					繼電器5
146		32			15			Relay Output 6				Relay Output 6		繼電器6					繼電器6
147		32			15			Relay Output 7				Relay Output 7		繼電器7					繼電器7
148		32			15			Relay Output 8				Relay Output 8		繼電器8					繼電器8
149		32			15			Relay Output 14				Relay Output 14		繼電器14				繼電器14
150		32			15			Relay Output 15				Relay Output 15		繼電器15				繼電器15
151		32			15			Relay Output 16				Relay Output 16		繼電器16				繼電器16
152		32			15			Relay Output 17				Relay Output 17		繼電器17				繼電器17
153		32			15			LVD 3 Enable				LVD 3 Enable		LVD3允許				LVD3允許
154		32			15			LVD Threshold				LVD Threshold		LVD下電閾值				LVD閾值
