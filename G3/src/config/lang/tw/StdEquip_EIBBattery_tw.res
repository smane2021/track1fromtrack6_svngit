﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			EIB Battery				EIB Battery		EIB電池					EIB電池
2		32			15			Battery Current				Battery Current		電池電流				電池電流
3		32			15			Battery Voltage				Battery Voltage		電池電壓				電池電壓
4		32			15			Battery Rating(Ah)			Batt Rating(Ah)		電池容量(Ah)				電池容量(Ah)
5		32			15			Battery Capacity (%)			Batt Cap (%)		電池容量(%)				電池容量(%)
6		32			15			Battery Current Limit Exceeded		Ov Bat Curr Lmt		超過電池限流點				超過電池限流點
7		32			15			Battery Over Current			Bat Over Curr		電池充電過流				電池充電過流
8		32			15			Battery Low Capacity			Low Batt Cap		容量低					容量低
9		32			15			Yes					Yes			是					是
10		32			15			No					No			否					否
26		32			15			State					State			状態					状態
27		32			15			Battery Bloak 1 Voltage			Bat Block1 Volt		電池塊1電壓				電池塊1電壓
28		32			15			Battery Bloak 2 Voltage			Bat Block2 Volt		電池塊2電壓				電池塊2電壓
29		32			15			Battery Bloak 3 Voltage			Bat Block3 Volt		電池塊3電壓				電池塊3電壓
30		32			15			Battery Bloak 4 Voltage			Bat Block4 Volt		電池塊4電壓				電池塊4電壓
31		32			15			Battery Bloak 5 Voltage			Bat Block5 Volt		電池塊5電壓				電池塊5電壓
32		32			15			Battery Bloak 6 Voltage			Bat Block6 Volt		電池塊6電壓				電池塊6電壓
33		32			15			Battery Bloak 7 Voltage			Bat Block7 Volt		電池塊7電壓				電池塊7電壓
34		32			15			Battery Bloak 8 Voltage			Bat Block8 Volt		電池塊8電壓				電池塊8電壓
35		32			15			Battery Management			Batt Management		參與電池管理				參與電池管理
36		32			15			Enabled					Enabled			允許					允許
37		32			15			Disabled				Disabled		不允許					不允許
38		32			15			Communication Fail			Comm Fail		通信中斷				通信中斷
39		32			15			Shunt Full Current			Shunt Full Curr		分流器額定電流				分流器額定電流
40		32			15			Shunt Full Voltage			Shunt Full Volt		分流器額定電壓				分流器額定電壓
41		32			15			On					On			開					開
42		32			15			Off					Off			關					關
43		32			15			Communication Fail			Comm Fail		通信中斷				通信中斷
44		32			15			Battery Temperature Probe Number	BattTempPrbNum		電池用溫度路數				電池用溫度路數
87		32			15			No					No			否					否
91		32			15			Temperature 1				Temperature 1		溫度1					溫度1
92		32			15			Temperature 2				Temperature 2		溫度2					溫度2
93		32			15			Temperature 3				Temperature 3		溫度3					溫度3
94		32			15			Temperature 4				Temperature 4		溫度4					溫度4
95		32			15			Temperature 5				Temperature 5		溫度5					溫度5
96		32			15			Rated Capacity				Rated Capacity		標稱容量				標稱容量
97		32			15			Battery Temperature			Battery Temp		電池溫度				電池溫度
98		32			15			Battery Tempurature Sensor		Bat Temp Sensor		電池溫度傳感器				電池溫度傳感器
99		32			15			None					None			無					無
100		32			15			Temperature 1				Temperature 1		溫度1					溫度1
101		32			15			Temperature 2				Temperature 2		溫度2					溫度2
102		32			15			Temperature 3				Temperature 3		溫度3					溫度3
103		32			15			Temperature 4				Temperature 4		溫度4					溫度4
104		32			15			Temperature 5				Temperature 5		溫度5					溫度5
105		32			15			Temperature 6				Temperature 6		溫度6					溫度6
106		32			15			Temperature 7				Temperature 7		溫度7					溫度7
107		32			15			Temperature 8				Temperature 8		溫度8					溫度8
108		32			15			Temperature 9				Temperature 9		溫度9					溫度9
109		32			15			Temperature 10				Temperature 10		溫度10					溫度10
110		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		Battery Current Imbalance Alarm		BattCurrImbalan	
111		32			15			EIB1 Battery2				EIB1 Battery2				EIB1 Battery2				EIB1 Battery2
112		32			15			EIB1 Battery3				EIB1 Battery3				EIB1 Battery3				EIB1 Battery3
113		32			15			EIB2 Battery1				EIB2 Battery1				EIB2 Battery1				EIB2 Battery1
114		32			15			EIB2 Battery2				EIB2 Battery2				EIB2 Battery2				EIB2 Battery2
115		32			15			EIB2 Battery3				EIB2 Battery3				EIB2 Battery3				EIB2 Battery3
116		32			15			EIB3 Battery1				EIB3 Battery1				EIB3 Battery1				EIB3 Battery1
117		32			15			EIB3 Battery2				EIB3 Battery2				EIB3 Battery2				EIB3 Battery2
118		32			15			EIB3 Battery3				EIB3 Battery3				EIB3 Battery3				EIB3 Battery3
119		32			15			EIB4 Battery1				EIB4 Battery1				EIB4 Battery1				EIB4 Battery1
120		32			15			EIB4 Battery2				EIB4 Battery2				EIB4 Battery2				EIB4 Battery2
121		32			15			EIB4 Battery3				EIB4 Battery3				EIB4 Battery3				EIB4 Battery3
122		32			15			EIB1 Battery1				EIB1 Battery1				EIB1 Battery1				EIB1 Battery1

150		32			15		Battery 1					Batt 1		Battery 1					Batt 1
151		32			15		Battery 2					Batt 2		Battery 2					Batt 2
152		32			15		Battery 3					Batt 3		Battery 3					Batt 3
