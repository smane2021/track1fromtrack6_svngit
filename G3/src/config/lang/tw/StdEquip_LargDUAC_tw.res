﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Manual					Manual			手動					手動
2		32			15			Auto					Auto			自動					自動
3		32			15			Off					Off			關					關
4		32			15			On					On			開					開
5		32			15			No Input				No Input		無輸入					無輸入
6		32			15			Input 1					Input 1			輸入1					輸入1
7		32			15			Input 2					Input 2			輸入2					輸入2
8		32			15			Input 3					Input 3			輸入3					輸入3
9		32			15			No Input				No Input		無輸入					無輸入
10		32			15			Input					Input			有輸入					有輸入
11		32			15			Closed					Closed			閉合					閉合
12		32			15			Open					Open			斷開					斷開
13		32			15			Closed					Closed			閉合					閉合
14		32			15			Open					Open			斷開					斷開
15		32			15			Closed					Closed			閉合					閉合
16		32			15			Open					Open			斷開					斷開
17		32			15			Closed					Closed			閉合					閉合
18		32			15			Open					Open			斷開					斷開
19		32			15			Closed					Closed			閉合					閉合
20		32			15			Open					Open			斷開					斷開
21		32			15			Closed					Closed			閉合					閉合
22		32			15			Open					Open			斷開					斷開
23		32			15			Closed					Closed			閉合					閉合
24		32			15			Open					Open			斷開					斷開
25		32			15			Closed					Closed			閉合					閉合
26		32			15			Open					Open			斷開					斷開
27		32			15			1-Phase					1-Phase			單相					單相
28		32			15			3-Phase					3-Phase			三相					三相
29		32			15			No Measurement				No Measurement		不測					不測
30		32			15			1-Phase					1-Phase			單相					單相
31		32			15			3-Phase					3-Phase			三相					三相
32		32			15			Communication OK			Comm OK			有響應					有響應
33		32			15			Communication Fail			Comm Fail		無響應					無響應
34		32			15			AC Distribution				AC Distribution		交流屏					交流屏
35		32			15			Mains 1 Uab/Ua				Mains 1 Uab/Ua		第壹路Uab/Ua電壓			第壹路Uab/Ua
36		32			15			Mains 1 Ubc/Ub				Mains 1 Ubc/Ub		第壹路Ubc/Ub電壓			第壹路Ubc/Ub
37		32			15			Mains 1 Uca/Uc				Mains 1 Uca/Uc		第壹路Uca/Uc電壓			第壹路Uca/Uc
38		32			15			Mains 2 Uab/Ua				Mains 2 Uab/Ua		第二路Uab/Ua電壓			第二路Uab/Ua
39		32			15			Mains 2 Ubc/Ub				Mains 2 Ubc/Ub		第二路Ubc/Ub電壓			第二路Ubc/Ub
40		32			15			Mains 2 Uca/Uc				Mains 2 Uca/Uc		第二路Uca/Uc電壓			第二路Uca/Uc
41		32			15			Mains 3 Uab/Ua				Mains 3 Uab/Ua		第三路Uab/Ua電壓			第三路Uab/Ua
42		32			15			Mains 3 Ubc/Ub				Mains 3 Ubc/Ub		第三路Ubc/Ub電壓			第三路Ubc/Ub
43		32			15			Mains 3 Uca/Uc				Mains 3 Uca/Uc		第三路Uca/Uc電壓			第三路Uca/Uc
53		32			15			Working Phase A Current			Phase A Curr		工作路A相電流				工作路A相電流
54		32			15			Working Phase B Current			Phase B Curr		工作路B相電流				工作路B相電流
55		32			15			Working Phase C Current			Phase C Curr		工作路C相電流				工作路C相電流
56		32			15			AC Input Frequency			AC Input Freq		交流輸入頻率				交流輸入頻率
57		32			15			AC Input Switch Mode			AC Switch Mode		交流輸入切換方式			輸入切換方式
58		32			15			Fault Lighting Status			Fault Lighting		故障照明状態				故障照明状態
59		32			15			Mains 1 Input Status			1 Input Status		交流1路輸入				交流1路輸入
60		32			15			Mains 2 Input Status			2 Input Status		交流2路輸入				交流2路輸入
61		32			15			Mains 3 Input Status			3 Input Status		交流3路輸入				交流3路輸入
62		32			15			AC Output 1 Status			Output 1 Status		交流輸出1路状態				輸出1路状態
63		32			15			AC Output 2 Status			Output 2 Status		交流輸出2路状態				輸出2路状態
64		32			15			AC Output 3 Status			Output 3 Status		交流輸出3路状態				輸出3路状態
65		32			15			AC Output 4 Status			Output 4 Status		交流輸出4路状態				輸出4路状態
66		32			15			AC Output 5 Status			Output 5 Status		交流輸出5路状態				輸出5路状態
67		32			15			AC Output 6 Status			Output 6 Status		交流輸出6路状態				輸出6路状態
68		32			15			AC Output 7 Status			Output 7 Status		交流輸出7路状態				輸出7路状態
69		32			15			AC Output 8 Status			Output 8 Status		交流輸出8路状態				輸出8路状態
70		32			15			AC Input Frequency High			Frequency High		頻率過高				頻率過高
71		32			15			AC Input Frequency Low			Frequency Low		頻率過低				頻率過低
72		32			15			AC Input MCCB Trip			Input MCCB Trip		交流輸入空開跳				輸入空開跳
73		32			15			SPD Trip				SPD Trip		防雷器故障				防雷器故障
74		32			15			AC Output MCCB Trip			OutputMCCB Trip		交流輸出空開跳				輸出空開跳
75		32			15			AC Input 1 Failure			Input 1 Failure		交流輸入1路停電				輸入1路停電
76		32			15			AC Input 2 Failure			Input 2 Failure		交流輸入2路停電				輸入2路停電
77		32			15			AC Input 3 Failure			Input 3 Failure		交流輸入3路停電				輸入3路停電
78		32			15			Mains 1 Uab/Ua Under Voltage		M1Uab/Ua UnderV		交流輸入1Uab/Ua欠壓			1路Uab/Ua欠壓
79		32			15			Mains 1 Ubc/Ub Under Voltage		M1Ubc/Ub UnderV		交流輸入1Ubc/Ub欠壓			1路Ubc/Ub欠壓
80		32			15			Mains 1 Uca/Uc Under Voltage		M1Uca/Uc UnderV		交流輸入1Uca/Uc欠壓			1路Uca/Uc欠壓
81		32			15			Mains 2 Uab/Ua Under Voltage		M2Uab/Ua UnderV		交流輸入2Uab/Ua欠壓			2路Uab/Ua欠壓
82		32			15			Mains 2 Ubc/Ub Under Voltage		M2Ubc/Ub UnderV		交流輸入2Ubc/Ub欠壓			2路Ubc/Ub欠壓
83		32			15			Mains 2 Uca/Uc Under Voltage		M2Uca/Uc UnderV		交流輸入2Uca/Uc欠壓			2路Uca/Uc欠壓
84		32			15			Mains 3 Uab/Ua Under Voltage		M3Uab/Ua UnderV		交流輸入3Uab/Ua欠壓			3路Uab/Ua欠壓
85		32			15			Mains 3 Ubc/Ub Under Voltage		M3Ubc/Ub UnderV		交流輸入3Ubc/Ub欠壓			3路Ubc/Ub欠壓
86		32			15			Mains 3 Uca/Uc Under Voltage		M3Uca/Uc UnderV		交流輸入3Uca/Uc欠壓			3路Uca/Uc欠壓
87		32			15			Mains 1 Uab/Ua Over Voltage		M1 Uab/Ua OverV		交流輸入1Uab/Ua過壓			1路Uab/Ua過壓
88		32			15			Mains 1 Ubc/Ub Over Voltage		M1 Ubc/Ub OverV		交流輸入1Ubc/Ub過壓			1路Ubc/Ub過壓
89		32			15			Mains 1 Uca/Uc Over Voltage		M1 Uca/Uc OverV		交流輸入1Uca/Uc過壓			1路Uca/Uc過壓
90		32			15			Mains 2 Uab/Ua Over Voltage		M2 Uab/Ua OverV		交流輸入2Uab/Ua過壓			2路Uab/Ua過壓
91		32			15			Mains 2 Ubc/Ub Over Voltage		M2 Ubc/Ub OverV		交流輸入2Ubc/Ub過壓			2路Ubc/Ub過壓
92		32			15			Mains 2 Uca/Uc Over Voltage		M2 Uca/Uc OverV		交流輸入2Uca/Uc過壓			2路Uca/Uc過壓
93		32			15			Mains 3 Uab/Ua Over Voltage		M3 Uab/Ua OverV		交流輸入3Uab/Ua過壓			3路Uab/Ua過壓
93		32			15			Mains 3 Ubc/Ub Over Voltage		M3 Ubc/Ub OverV		交流輸入3Ubc/Ub過壓			3路Ubc/Ub過壓
94		32			15			Mains 3 Uca/Uc Over Voltage		M3 Uca/Uc OverV		交流輸入3Uca/Uc過壓			3路Uca/Uc過壓
95		32			15			Mains 1 Uab/Ua Failure			M1 Uab/Ua Fail		交流輸入1Uab/Ua停電			1路Uab/Ua停電
96		32			15			Mains 1 Ubc/Ub Failure			M1 Ubc/Ub Fail		交流輸入1Ubc/Ub停電			1路Ubc/Ub停電
97		32			15			Mains 1 Uca/Uc Failure			M1 Uca/Uc Fail		交流輸入1Uca/Uc停電			1路Uca/Uc停電
98		32			15			Mains 2 Uab/Ua Failure			M2 Uab/Ua Fail		交流輸入2Uab/Ua停電			2路Uab/Ua停電
99		32			15			Mains 2 Ubc/Ub Failure			M2 Ubc/Ub Fail		交流輸入2Ubc/Ub停電			2路Ubc/Ub停電
100		32			15			Mains 2 Uca/Uc Failure			M2 Uca/Uc Fail		交流輸入2Uca/Uc停電			2路Uca/Uc停電
101		32			15			Mains 3 Uab/Ua Failure			M3 Uab/Ua Fail		交流輸入3Uab/Ua停電			3路Uab/Ua停電
102		32			15			Mains 3 Ubc/Ub Failure			M3 Ubc/Ub Fail		交流輸入3Ubc/Ub停電			3路Ubc/Ub停電
103		32			15			Mains 3 Uca/Uc Failure			M3 Uca/Uc Fail		交流輸入3Uca/Uc停電			3路Uca/Uc停電
104		32			15			Communication Fail			Comm Fail		交流屏通訊中斷				交流屏通訊中斷
105		32			15			Over Voltage Limit			Over Volt Limit		輸入過壓告警點				輸入過壓點
106		32			15			Under Voltage Limit			Under Volt Limit	輸入欠壓告警點				輸入欠壓點
107		32			15			Phase Failure Voltage			Phase Fail Volt		輸入缺相告警點				輸入缺相點
108		32			15			Over Frequency Limit			Over Freq Limit		頻率過高告警點				頻率過高點
109		32			15			Under Frequency Limit			Under Freq Limit	頻率過低告警點				頻率過低點
110		32			15			Current Transformer Coef		Curr Trans Coef		交流互感器系數				交流互感系數
111		32			15			Input Type				Input Type		交流輸入類型				交流輸入類型
112		32			15			Input Number				Input Number		交流輸入路數				交流輸入路數
113		32			15			Current Measurement			Cur Measurement		交流電流測量方式			電流測量方式
114		32			15			Output Number				Output Number		交流輸出路數				交流輸出路數
115		32			15			Distribution Address			Distri Address		交流屏地址				交流屏地址
116		32			15			Mains 1 Failure				Mains 1 Fail		交流1路輸入停電				交流1路輸入停電
117		32			15			Mains 2 Failure				Mains 2 Fail		交流2路輸入停電				交流2路輸入停電
118		32			15			Mains 3 Failure				Mains 3 Fail		交流3路輸入停電				交流3路輸入停電
119		32			15			Mains 1 Uab/Ua Failure			M1 Uab/Ua Fail		交流1路Uab/Ua缺相			1路Uab/Ua缺相
120		32			15			Mains 1 Ubc/Ub Failure			M1 Ubc/Ub Fail		交流1路Ubc/Ub缺相			1路Ubc/Ub缺相
121		32			15			Mains 1 Uca/Uc Failure			M1 Uca/Uc Fail		交流1路Uca/Uc缺相			1路Uca/Uc缺相
122		32			15			Mains 2 Uab/Ua Failure			M2 Uab/Ua Fail		交流2路Uab/Ua缺相			2路Uab/Ua缺相
123		32			15			Mains 2 Ubc/Ub Failure			M2 Ubc/Ub Fail		交流2路Ubc/Ub缺相			2路Ubc/Ub缺相
124		32			15			Mains 2 Uca/Uc Failure			M2 Uca/Uc Fail		交流2路Uca/Uc缺相			2路Uca/Uc缺相
125		32			15			Mains 3 Uab/Ua Failure			M3 Uab/Ua Fail		交流3路Uab/Ua缺相			3路Uab/Ua缺相
126		32			15			Mains 3 Ubc/Ub Failure			M3 Ubc/Ub Fail		交流3路Ubc/Ub缺相			3路Ubc/Ub缺相
127		32			15			Mains 3 Uca/Uc Failure			M3 Uca/Uc Fail		交流3路Uca/Uc缺相			3路Uca/Uc缺相
128		32			15			Over Frequency				Over Frequency		交流頻率過高				交流頻率過高
129		32			15			Under Frequency				Under Frequency		交流頻率過低				交流頻率過低
130		32			15			Mains 1 Uab/Ua Under Voltage		M1Uab/Ua UnderV		交流輸入1Uab/Ua欠壓			1路Uab/Ua欠壓
131		32			15			Mains 1 Ubc/Ub Under Voltage		M1Ubc/Ub UnderV		交流輸入1Ubc/Ub欠壓			1路Ubc/Ub欠壓
132		32			15			Mains 1 Uca/Uc Under Voltage		M1Uca/Uc UnderV		交流輸入1Uca/Uc欠壓			1路Uca/Uc欠壓
133		32			15			Mains 2 Uab/Ua Under Voltage		M2Uab/Ua UnderV		交流輸入2Uab/Ua欠壓			2路Uab/Ua欠壓
134		32			15			Mains 2 Ubc/Ub Under Voltage		M2Ubc/Ub UnderV		交流輸入2Ubc/Ub欠壓			2路Ubc/Ub欠壓
135		32			15			Mains 2 Uca/Uc Under Voltage		M2Uca/Uc UnderV		交流輸入2Uca/Uc欠壓			2路Uca/Uc欠壓
136		32			15			Mains 3 Uab/Ua Under Voltage		M3Uab/Ua UnderV		交流輸入3Uab/Ua欠壓			3路Uab/Ua欠壓
137		32			15			Mains 3 Ubc/Ub Under Voltage		M3Ubc/Ub UnderV		交流輸入3Ubc/Ub欠壓			3路Ubc/Ub欠壓
138		32			15			Mains 3 Uca/Uc Under Voltage		M3Uca/Uc UnderV		交流輸入3Uca/Uc欠壓			3路Uca/Uc欠壓
139		32			15			Mains 1 Uab/Ua Over Voltage		M1 Uab/Ua OverV		交流輸入1Uab/Ua過壓			1路Uab/Ua過壓
140		32			15			Mains 1 Ubc/Ub Over Voltage		M1 Ubc/Ub OverV		交流輸入1Ubc/Ub過壓			1路Ubc/Ub過壓
141		32			15			Mains 1 Uca/Uc Over Voltage		M1 Uca/Uc OverV		交流輸入1Uca/Uc過壓			1路Uca/Uc過壓
142		32			15			Mains 2 Uab/Ua Over Voltage		M2 Uab/Ua OverV		交流輸入2Uab/Ua過壓			2路Uab/Ua過壓
143		32			15			Mains 2 Ubc/Ub Over Voltage		M2 Ubc/Ub OverV		交流輸入2Ubc/Ub過壓			2路Ubc/Ub過壓
144		32			15			Mains 2 Uca/Uc Over Voltage		M2 Uca/Uc OverV		交流輸入2Uca/Uc過壓			2路Uca/Uc過壓
145		32			15			Mains 3 Uab/Ua Over Voltage		M3 Uab/Ua OverV		交流輸入3Uab/Ua過壓			3路Uab/Ua過壓
146		32			15			Mains 3 Ubc/Ub Over Voltage		M3 Ubc/Ub OverV		交流輸入3Ubc/Ub過壓			3路Ubc/Ub過壓
147		32			15			Mains 3 Uca/Uc Over Voltage		M3 Uca/Uc OverV		交流輸入3Uca/Uc過壓			3路Uca/Uc過壓
148		32			15			AC Input MCCB Trip			Input MCCB Trip		輸入空開跳				輸入空開跳
149		32			15			AC Output MCCB Trip			OutputMCCB Trip		輸出空開跳				輸出空開跳
150		32			15			SPD Trip				SPD Trip		防雷器故障				防雷器故障
169		32			15			Communication Fail			Comm Fail		交流屏通訊中斷				交流屏通訊中斷
170		32			15			Mains Failure				Mains Failure		交流停電				交流停電
171		32			15			LargeDU AC Distribution			AC Distribution		LargeDU交流屏				交流屏
172		32			15			No Alarm				No Alarm		無告警					無告警
173		32			15			Over Voltage				Over Volt		過壓					過壓
174		32			15			Under Voltage				Under Volt		欠壓					欠壓
175		32			15			AC Phase Failure			AC Phase Fail		交流缺相				交流缺相
176		32			15			No Alarm				No Alarm		無告警					無告警
177		32			15			Over Frequency				Over Frequency		交流頻率過高				交流頻率過高
178		32			15			Under Frequency				Under Frequency		交流頻率過低				交流頻率過低
179		32			15			No Alarm				No Alarm		無告警					無告警
180		32			15			AC Over Voltage				AC Over Volt		交流過壓				交流過壓
181		32			15			AC Under Voltage			AC Under Volt		交流欠壓				交流欠壓
182		32			15			AC Phase Failure			AC Phase Fail		交流缺相				交流缺相
183		32			15			No Alarm				No Alarm		無告警					無告警
184		32			15			AC Over Voltage				AC Over Volt		交流過壓				交流過壓
185		32			15			AC Under Voltage			AC Under Volt		交流欠壓				交流欠壓
186		32			15			AC Phase Failure			AC Phase Fail		交流缺相				交流缺相
187		32			15			Mains 1 Uab/Ua Alarm			M1 Uab/Ua Alarm		1路Uab/Ua電壓告警			1路Uab/Ua告警
188		32			15			Mains 1 Ubc/Ub Alarm			M1 Ubc/Ub Alarm		1路Ubc/Ub電壓告警			1路Ubc/Ub告警
189		32			15			Mains 1 Uca/Uc Alarm			M1 Uca/Uc Alarm		1路Uca/Uc電壓告警			1路Uca/Uc告警
190		32			15			Frequency Alarm				Frequency Alarm		頻率告警				頻率告警
191		32			15			Communication Fail			Comm Fail		交流屏通訊中斷				交流屏通訊中斷
192		32			15			Normal					Normal			正常					正常
193		32			15			Failure					Failure			中斷					中斷
194		32			15			Communication Fail			Comm Fail		交流屏通訊中斷				交流屏通訊中斷
195		32			15			Mains Input Number			Mains Input Num		交流輸入路號				交流輸入路號
196		32			15			Number 1				Number 1		第壹路					第壹路
197		32			15			Number 2				Number 2		第二路					第二路
198		32			15			Number 3				Number 3		第三路					第三路
199		32			15			None					None			無					無
200		32			15			Emergency Light				Emergency Light		故障照明燈状態				故障照明燈状態
201		32			15			Closed					Closed			關閉					關閉
202		32			15			Open					Open			打開					打開
203		32			15			Mains 2 Uab/Ua Alarm			M2 Uab/Ua Alarm		2路Uab/Ua電壓告警			2路Uab/Ua告警
204		32			15			Mains 2 Ubc/Ub Alarm			M2 Ubc/Ub Alarm		2路Ubc/Ub電壓告警			2路Ubc/Ub告警
205		32			15			Mains 2 Uca/Uc Alarm			M2 Uca/Uc Alarm		2路Uca/Uc電壓告警			2路Uca/Uc告警
206		32			15			Mains 3 Uab/Ua Alarm			M3 Uab/Ua Alarm		3路Uab/Ua電壓告警			3路Uab/Ua告警
207		32			15			Mains 3 Ubc/Ub Alarm			M3 Ubc/Ub Alarm		3路Ubc/Ub電壓告警			3路Ubc/Ub告警
208		32			15			Mains 3 Uca/Uc Alarm			M3 Uca/Uc Alarm		3路Uca/Uc電壓告警			3路Uca/Uc告警
209		32			15			Normal					Normal			正常					正常
210		32			15			Alarm					Alarm			告警					告警
211		32			15			AC Fuse Number				AC Fuse Num		交流熔絲數量				交流熔絲數量
212		32			15			Existence State				Existence State		是否存在				是否存在
213		32			15			Existent				Existent		存在					存在
214		32			15			Not Existent				Not Existent		不存在					不存在
