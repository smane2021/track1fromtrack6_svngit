﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Phase A Voltage				R			A相電壓					R
2		32			15			Phase B Voltage				S			B相電壓					S
3		32			15			Phase C Voltage				T			C相電壓					T
4		32			15			Line AB Voltage				R			AB線電壓				R
5		32			15			Line BC Voltage				S			BC線電壓				S
6		32			15			Line CA Voltage				T			CA線電壓				T
7		32			15			Phase A Current				Phase A Current		A相電流					A相電流
8		32			15			Phase B Current				Phase B Current		B相電流					B相電流
9		32			15			Phase C Current				Phase C Current		C相電流					C相電流
10		32			15			AC Frequency				AC Frequency		交流頻率				交流頻率
11		32			15			Total Real Power			Total Real Pwr		總有功功率				總有功功率
12		32			15			Phase A Real Power			PH-A Real Power		A相有功功率				A相有功功率
13		32			15			Phase B Real Power			PH-B Real Power		B相有功功率				B相有功功率
14		32			15			Phase C Real Power			PH-C Real Power		C相有功功率				C相有功功率
15		32			15			Total Reactive Power			Total React Pwr		總無功功率				總無功功率
16		32			15			Phase A Reactive Power			PH-A React Pwr		A相無功功率				A相無功功率
17		32			15			Phase B Reactive Power			PH-B React Pwr		B相無功功率				B相無功功率
18		32			15			Phase C Reactive Power			PH-C React Pwr		C相無功功率				C相無功功率
19		32			15			Total Apparent Power			Total Appar Pwr		總視在功率				總視在功率
20		32			15			Phase A Apparent Power			PH-A Appar Pwr		A相視在功率				A相視在功率
21		32			15			Phase B Apparent Power			PH-B Appar Pwr		B相視在功率				B相視在功率
22		32			15			Phase C Apparent Power			PH-C Appar Pwr		C相視在功率				C相視在功率
23		32			15			Power Factor				Power Factor		功率因數				功率因數
24		32			15			Phase A Power Factor			PH-A Pwr Factor		A相功率因數				A相功率因數
25		32			15			Phase B Power Factor			PH-B Pwr Factor		B相功率因數				B相功率因數
26		32			15			Phase C Power Factor			PH-C Pwr Factor		C相功率因數				C相功率因數
27		32			15			Phase A Current Crest Factor		Ia Crest Factor		Ia波峰因數				Ia波峰因數
28		32			15			Phase B Current Crest Factor		Ib Crest Factor		Ib波峰因數				Ib波峰因數
29		32			15			Phase C Current Crest Factor		Ic Crest Factor		Ic波峰因數				Ic波峰因數
30		32			15			Phase A Current THD			PH-A Curr THD		A相電流THD				A相電流THD
31		32			15			Phase B Current THD			PH-B Curr THD		B相電流THD				B相電流THD
32		32			15			Phase C Current THD			PH-C Curr THD		C相電流THD				C相電流THD
33		32			15			Phase A Voltage THD			PH-A Volt THD		A相電壓THD				A相電壓THD
34		32			15			Phase B Voltage THD			PH-B Volt THD		B相電壓THD				B相電壓THD
35		32			15			Phase C Voltage THD			PH-C Volt THD		C相電壓THD				C相電壓THD
36		32			15			Total Real Energy			TotalRealEnergy		總有功能量				總有功能量
37		32			15			Total Reactive Energy			TotalReacEnergy		總無功能量				總無功能量
38		32			15			Total Apparent Energy			TotalAppaEnergy		總視在能量				總視在能量
39		32			15			Ambient Temperature			Ambient Temp		環境溫度				環境溫度
40		32			15			Nominal Line Voltage			NominalLineVolt		標稱線電壓				標稱線電壓
41		32			15			Nominal Phase Voltage			Nominal PH-Volt		標稱相電壓				標稱相電壓
42		32			15			Nominal Frequency			Nominal Freq		標稱頻率				標稱頻率
43		32			15			Mains Failure Alarm Limit 1		Mains Fail Alm1		電壓告警門限1				電壓告警門限1
44		32			15			Mains Failure Alarm Limit 2		Mains Fail Alm2		電壓告警門限2				電壓告警門限2
45		32			15			Voltage Alarm Limit 1			Volt Alarm Lmt1		電壓告警門限1				電壓告警門限1
46		32			15			Voltage Alarm Limit 2			Volt Alarm Lmt2		電壓告警門限2				電壓告警門限2
47		32			15			Frequency Alarm Limit			Freq Alarm Lmt		頻率告警門限				頻率告警門限
48		32			15			High Temperature Limit			High Temp Limit		高溫點					高溫點
49		32			15			Low Temperature Limit			Low Temp Limit		低溫點					低溫點
50		32			15			Supervision Fail			SupervisionFail		監控失敗				監控失敗
51		32			15			Line AB Over Voltage 1			L-AB Over Volt1		線電壓AB高壓				線電壓AB高壓
52		32			15			Line AB Over Voltage 2			L-AB Over Volt2		線電壓AB超高				線電壓AB超高
53		32			15			Line AB Under Voltage 1			L-AB UnderVolt1		線電壓AB低壓				線電壓AB低壓
54		32			15			Line AB Under Voltage 2			L-AB UnderVolt2		線電壓AB超低				線電壓AB超低
55		32			15			Line BC Over Voltage 1			L-BC Over Volt1		線電壓BC高壓				線電壓BC高壓
56		32			15			Line BC Over Voltage 2			L-BC Over Volt2		線電壓BC超高				線電壓BC超高
57		32			15			Line BC Under Voltage 1			L-BC UnderVolt1		線電壓BC低壓				線電壓BC低壓
58		32			15			Line BC Under Voltage 2			L-BC UnderVolt2		線電壓BC超低				線電壓BC超低
59		32			15			Line CA Over Voltage 1			L-CA Over Volt1		線電壓CA高壓				線電壓CA高壓
60		32			15			Line CA Over Voltage 2			L-CA Over Volt2		線電壓CA超高				線電壓CA超高
61		32			15			Line CA Under Voltage 1			L-CA UnderVolt1		線電壓CA低壓				線電壓CA低壓
62		32			15			Line CA Under Voltage 2			L-CA UnderVolt2		線電壓CA超低				線電壓CA超低
63		32			15			Phase A Over Voltage 1			PH-A Over Volt1		A相電壓高				A相電壓高
64		32			15			Phase A Over Voltage 2			PH-A Over Volt2		A相電壓超高				A相電壓超高
65		32			15			Phase A Under Voltage 1			PH-A UnderVolt1		A相電壓低				A相電壓低
66		32			15			Phase A Under Voltage 2			PH-A UnderVolt2		A相電壓超低				A相電壓超低
67		32			15			Phase B Over Voltage 1			PH-B Over Volt1		B相電壓高				B相電壓高
68		32			15			Phase B Over Voltage 2			PH-B Over Volt2		B相電壓超高				B相電壓超高
69		32			15			Phase B Under Voltage 1			PH-B UnderVolt1		B相電壓低				B相電壓低
70		32			15			Phase B Under Voltage 2			PH-B UnderVolt2		B相電壓超低				B相電壓超低
71		32			15			Phase C Over Voltage 1			PH-C Over Volt1		C相電壓高				C相電壓高
72		32			15			Phase C Over Voltage 2			PH-C Over Volt2		C相電壓超高				C相電壓超高
73		32			15			Phase C Under Voltage 1			PH-C UnderVolt1		C相電壓低				C相電壓低
74		32			15			Phase C Under Voltage 2			PH-C UnderVolt2		C相電壓超低				C相電壓超低
75		32			15			Mains Failure				Mains Failure		交流停電				交流停電
76		32			15			Severe Mains Failure			SevereMainsFail		嚴重市電失敗				嚴重市電失敗
77		32			15			High Frequency				High Frequency		高頻					高頻
78		32			15			Low Frequency				Low Frequency		低頻					低頻
79		32			15			High Temperature			High Temp		高溫					高溫
80		32			15			Low Temperature				Low Temperature		低溫					低溫
81		32			15			Rectifier AC				Rectifier AC		模塊交流				模塊交流
82		32			15			Supervision Fail			SupervisionFail		監控失敗				監控失敗
83		32			15			No					No			否					否
84		32			15			Yes					Yes			是					是
85		32			15			Phase A Mains Failure Counter		PH-A Fail Count		A相故障計數				A相故障計數
86		32			15			Phase B Mains Failure Counter		PH-B Fail Count		B相故障計數				B相故障計數
87		32			15			Phase C Mains Failure Counter		PH-C Fail Count		C相故障計數				C相故障計數
88		32			15			Frequency Failure Counter		FreqFailCounter		頻率故障計數				頻率故障計數
89		32			15			Reset Phase A Mains Fail Counter	RstPH-AFailCnt		A相故障復位				A相故障復位
90		32			15			Reset Phase B Mains Fail Counter	RstPH-BFailCnt		B相故障復位				B相故障復位
91		32			15			Reset Phase C Mains Fail Counter	RstPH-CFailCnt		C相故障復位				C相故障復位
92		32			15			Reset Frequency Fail Counter		RstFreqFailCnt		頻率故障復位				頻率故障復位
93		32			15			Current Alarm Limit			Curr Alm Limit		AC過流告警點				AC過流告警點
94		32			15			Phase A High Current			PH-A High Curr		A相過流					A相過流
95		32			15			Phase B High Current			PH-B High Curr		B相過流					B相過流
96		32			15			Phase C High Current			PH-C High Curr		C相過流					C相過流
97		32			15			Minimum Phase Voltage			Min Phase Volt		相電壓最小值				相電壓最小值
98		32			15			Maximum Phase Voltage			Max Phase Volt		相電壓最大值				相電壓最大值
99		32			15			Raw Data 1				Raw Data 1		原始數據1				原始數據1
100		32			15			Raw Data 2				Raw Data 2		原始數據2				原始數據2
101		32			15			Raw Data 3				Raw Data 3		原始數據3				原始數據3
102		32			15			Reference Voltage			Reference Volt		基准電壓				基准電壓
103		32			15			State					State			State					State
104		32			15			Off					Off			off					off
105		32			15			On					On			on					on
106		32			15			High Phase Voltage			High Ph-Volt		相電壓高				相電壓高
107		32			15			Very High Phase Voltage			VHigh Ph-Volt		相電壓超高				相電壓超高
108		32			15			Low Phase Voltage			Low Ph-Volt		相電壓低				相電壓低
109		32			15			Very Low Phase Voltage			VLow Ph-Volt		相電壓超低				相電壓超低
110		32			15			All Rectifiers Comm Fail		AllRectCommFail		所有模塊通信中斷			所有模塊通信斷
