﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Current				Batt Current		電池電流				電池電流
2		32			15			Battery Rating(Ah)			Batt Rating(Ah)		電池容量(Ah)				電池容量(Ah)
3		32			15			Exceed Current Limit			Exceed Curr Lmt		超過電池限流點				超過電池限流點
4		32			15			Battery					Battery			電池					電池
5		32			15			Over Battery Current			Over Current		電池充電過流				電池充電過流
6		32			15			Battery Capacity (%)			Batt Cap (%)		電池容量(%)				電池容量(%)
7		32			15			Battery Voltage				Batt Voltage		電池電壓				電池電壓
8		32			15			Low Capacity				Low Capacity		容量低					容量低
9		32			15			On					On			正常					正常
10		32			15			Off					Off			斷開					斷開
11		32			15			Battery Fuse Voltage			Fuse Voltage		電池熔絲電壓				電池熔絲電壓
12		32			15			Battery Fuse Status			Fuse Status		電池熔絲状態				電池熔絲状態
13		32			15			Fuse Alarm				Fuse Alarm		熔絲告警				熔絲告警
14		32			15			SMDU Battery				SMDU Battery		SMDU電池				SMDU電池
15		32			15			State					State			状態					状態
16		32			15			Off					Off			斷開					斷開
17		32			15			On					On			閉合					閉合
18		32			15			Switch					Switch			Swich					Swich
19		32			15			Battery Over Current			Batt Over Curr		電池過流				電池過流
20		32			15			Battery Management			Batt Management		參與電池管理				參與電池管理
21		32			15			Yes					Yes			是					是
22		32			15			No					No			否					否
23		32			15			Over Voltage Limit			Over Volt Limit		電池過壓點				電池過壓點
24		32			15			Low Voltage Limit			Low Volt Limit		電池欠壓點				電池欠壓點
25		32			15			Battery Over Voltage			Over Voltage		過壓					過壓
26		32			15			Battery Under Voltage			Under Voltage		欠壓					欠壓
27		32			15			Over Current				Over Current		過流點					過流點
28		32			15			Communication Fail			Comm Fail		通訊中斷				通訊中斷
29		32			15			Times of Communication Fail		Times Comm Fail		通信失敗次數				通信失敗次數
44		32			15			Battery Temp Number			Batt Temp Num		電池用溫度路數				電池用溫度路數
87		32			15			No					No			否					否
91		32			15			Temperature 1				Temperature 1		溫度1					溫度1
92		32			15			Temperature 2				Temperature 2		溫度2					溫度2
93		32			15			Temperature 3				Temperature 3		溫度3					溫度3
94		32			15			Temperature 4				Temperature 4		溫度4					溫度4
95		32			15			Temperature 5				Temperature 5		溫度5					溫度5
96		32			15			Rated Capacity				Rated Capacity		標稱容量				標稱容量
97		32			15			Battery Temperature			Battery Temp		電池溫度				電池溫度
98		32			15			Battery Temperature Sensor		BattTemp Sensor		電池溫度傳感器				電池溫度傳感器
99		32			15			None					None			無					無
100		32			15			Temperature 1				Temperature 1		溫度1					溫度1
101		32			15			Temperature 2				Temperature 2		溫度2					溫度2
102		32			15			Temperature 3				Temperature 3		溫度3					溫度3
103		32			15			Temperature 4				Temperature 4		溫度4					溫度4
104		32			15			Temperature 5				Temperature 5		溫度5					溫度5
105		32			15			Temperature 6				Temperature 6		溫度6					溫度6
106		32			15			Temperature 7				Temperature 7		溫度7					溫度7
107		32			15			Temperature 8				Temperature 8		溫度8					溫度8
108		32			15			Temperature 9				Temperature 9		溫度9					溫度9
109		32			15			Temperature 10				Temperature 10		溫度10					溫度10
110		32			15			SMDU1 Battery1			SMDU1 Battery1		SMDU1 Battery1			SMDU1 Battery1
111		32			15			SMDU1 Battery2			SMDU1 Battery2		SMDU1 Battery2			SMDU1 Battery2
112		32			15			SMDU1 Battery3			SMDU1 Battery3		SMDU1 Battery3			SMDU1 Battery3
113		32			15			SMDU1 Battery4			SMDU1 Battery4		SMDU1 Battery4			SMDU1 Battery4
114		32			15			SMDU1 Battery5			SMDU1 Battery5		SMDU1 Battery5			SMDU1 Battery5
115		32			15			SMDU2 Battery1			SMDU2 Battery1		SMDU2 Battery1			SMDU2 Battery1

116		32		15				Battery Current Imbalance Alarm		BattCurrImbalan		Battery Current Imbalance Alarm		BattCurrImbalan

117		32			15			SMDU2 Battery3			SMDU2 Battery3		SMDU2 Battery3			SMDU2 Battery3
118		32			15			SMDU2 Battery4			SMDU2 Battery4		SMDU2 Battery4			SMDU2 Battery4
119		32			15			SMDU2 Battery5			SMDU2 Battery5		SMDU2 Battery5			SMDU2 Battery5
120		32			15			SMDU3 Battery1			SMDU3 Battery1		SMDU3 Battery1			SMDU3 Battery1
121		32			15			SMDU3 Battery2			SMDU3 Battery2		SMDU3 Battery2			SMDU3 Battery2
122		32			15			SMDU3 Battery3			SMDU3 Battery3		SMDU3 Battery3			SMDU3 Battery3
123		32			15			SMDU3 Battery4			SMDU3 Battery4		SMDU3 Battery4			SMDU3 Battery4
124		32			15			SMDU3 Battery5			SMDU3 Battery5		SMDU3 Battery5			SMDU3 Battery5
125		32			15			SMDU4 Battery1			SMDU4 Battery1		SMDU4 Battery1			SMDU4 Battery1
126		32			15			SMDU4 Battery2			SMDU4 Battery2		SMDU4 Battery2			SMDU4 Battery2
127		32			15			SMDU4 Battery3			SMDU4 Battery3		SMDU4 Battery3			SMDU4 Battery3
128		32			15			SMDU4 Battery4			SMDU4 Battery4		SMDU4 Battery4			SMDU4 Battery4
129		32			15			SMDU4 Battery5			SMDU4 Battery5		SMDU4 Battery5			SMDU4 Battery5
130		32			15			SMDU5 Battery1			SMDU5 Battery1		SMDU5 Battery1			SMDU5 Battery1
131		32			15			SMDU5 Battery2			SMDU5 Battery2		SMDU5 Battery2			SMDU5 Battery2
132		32			15			SMDU5 Battery3			SMDU5 Battery3		SMDU5 Battery3			SMDU5 Battery3
133		32			15			SMDU5 Battery4			SMDU5 Battery4		SMDU5 Battery4			SMDU5 Battery4
134		32			15			SMDU5 Battery5			SMDU5 Battery5		SMDU5 Battery5			SMDU5 Battery5
135		32			15			SMDU6 Battery1			SMDU6 Battery1		SMDU6 Battery1			SMDU6 Battery1
136		32			15			SMDU6 Battery2			SMDU6 Battery2		SMDU6 Battery2			SMDU6 Battery2
137		32			15			SMDU6 Battery3			SMDU6 Battery3		SMDU6 Battery3			SMDU6 Battery3
138		32			15			SMDU6 Battery4			SMDU6 Battery4		SMDU6 Battery4			SMDU6 Battery4
139		32			15			SMDU6 Battery5			SMDU6 Battery5		SMDU6 Battery5			SMDU6 Battery5
140		32			15			SMDU7 Battery1			SMDU7 Battery1		SMDU7 Battery1			SMDU7 Battery1
141		32			15			SMDU7 Battery2			SMDU7 Battery2		SMDU7 Battery2			SMDU7 Battery2
142		32			15			SMDU7 Battery3			SMDU7 Battery3		SMDU7 Battery3			SMDU7 Battery3
143		32			15			SMDU7 Battery4			SMDU7 Battery4		SMDU7 Battery4			SMDU7 Battery4
144		32			15			SMDU7 Battery5			SMDU7 Battery5		SMDU7 Battery5			SMDU7 Battery5
145		32			15			SMDU8 Battery1			SMDU8 Battery1		SMDU8 Battery1			SMDU8 Battery1
146		32			15			SMDU8 Battery2			SMDU8 Battery2		SMDU8 Battery2			SMDU8 Battery2
147		32			15			SMDU8 Battery3			SMDU8 Battery3		SMDU8 Battery3			SMDU8 Battery3
148		32			15			SMDU8 Battery4			SMDU8 Battery4		SMDU8 Battery4			SMDU8 Battery4
149		32			15			SMDU8 Battery5			SMDU8 Battery5		SMDU8 Battery5			SMDU8 Battery5
150		32			15			SMDU2 Battery2			SMDU2 Battery2		SMDU2 Battery2			SMDU2 Battery2


151		32			15			Battery 1			Batt 1			Battery 1			Batt 1
152		32			15			Battery 2			Batt 2			Battery 2			Batt 2
153		32			15			Battery 3			Batt 3			Battery 3			Batt 3
154		32			15			Battery 4			Batt 4			Battery 4			Batt 4
155		32			15			Battery 5			Batt 5			Battery 5			Batt 5
