﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Voltage				Battery Voltage		電池電壓				電池電壓
2		32			15			Total Battery Current			Total Batt Curr		電池總電流				電池總電流
3		32			15			Battery Temperature			Battery Temp		電池溫度				電池溫度
4		32			15			Short BOD Total Time			Short BOD Time		淺放電總時間				淺放電總時間
5		32			15			Long BOD Total Time			Long BOD Time		深放電總時間				深放電總時間
6		32			15			Full BOD Total Time			Full BOD Time		過放電總時間				過放電總時間
7		32			15			Short BOD Counter			ShortBODCounter		淺放電總次數				淺放電總次數
8		32			15			Long BOD Counter			LongBODCounter		深放電總次數				深放電總次數
9		32			15			Full BOD Counter			FullBODCounter		過放電總次數				過放電總次數
14		32			15			End Test on Voltage			End Test Volt		電壓測試結束				電壓測試結束
15		32			15			Discharge Current Imbalance		Dsch Curr Imb		放電電流不平衡				放電電流不平衡
19		32			15			Abnormal Battery Current		AbnormalBatCurr		電池電流異常				電池電流異常
21		32			15			System Current Limit Active		SystemCurrLimit		系統限流				系統限流
23		32			15			Equalize/Float Charge Control		EQ/FLT Control		均浮充控制				均浮充控制
25		32			15			Battery Test Control			BattTestControl		電池測試啟停				電池測試啟停
30		32			15			Number of Battery Blocks		Num Batt Blocks		每组電池的塊數				每组電池的塊數
31		32			15			End Test Time				End Test Time		電池測試時間				電池測試時間
32		32			15			End Test Voltage			End Test Volt		測試結束電壓				測試結束電壓
33		32			15			End Test Capacity			EndTestCapacity		測試結束容量				測試結束容量
34		32			15			Constant Current Test			Const Curr Test		恒流測試允許				恒流測試允許
35		32			15			Constant Current Test Current		ConstCurrT Curr		恒流測試電流				恒流測試電流
37		32			15			AC Fail Test				AC Fail Test		停電測試允許				停電測試允許
38		32			15			Short Test				Short Test		短測試允許				短測試允許
39		32			15			Short Test Cycle			ShortTest Cycle		短測試啟動條件				短測試啟動條件
40		32			15			Short Test Max Difference Curr		Max Diff Curr		短測試最大流差				短測試最大流差
41		32			15			Short Test Time				Short Test Time		短測試時間				短測試時間
42		32			15			Float Charge Voltage			Float Voltage		浮充電壓				浮充電壓
43		32			15			Equalize Charge Voltage			EQ Voltage		均充電壓				均充電壓
44		32			15			Maximum Equalize Charge Time		Maximum EQ Time		均充保護時間				均充保護時間
45		32			15			Equalize Stop Current			EQ Stop Curr		穩流均充電流				穩流均充電流
46		32			15			Equalize Stop Delay Time		EQ Stop Delay		穩流均充時間				穩流均充時間
47		32			15			Automatic Equalize			Auto EQ			自動均充允許				自動均充允許
48		32			15			Equalize Start Current			EQ Start Curr		轉均充電流				轉均充電流
49		32			15			Equalize Start Capacity			EQ Start Cap		轉均充容量				轉均充容量
50		32			15			Cyclic Equalize				Cyc EQ			定時均充允許				定時均充允許
51		32			15			Cyclic Equalize Interval		Cyc EQ Interval		定時均充周期				定時均充周期
52		32			15			Cyclic Equalize Duration		Cyc EQ Duration		均充持續時間				均充持續時間
53		32			15			Temperature Compensation Center		TempComp Center		溫補中心點				溫補中心點
54		32			15			Temp Comp Coefficient (slope)		Temp Comp Coeff		溫補系數				溫補系數
55		32			15			Battery Current Limit			Batt Curr Limit		電池限流點				電池限流點
56		32			15			Battery Type Number			Batt Type Num		電池類型				電池類型
57		32			15			Rated Capacity				Rated Capacity		標稱容量				標稱容量
58		32			15			Charging Efficiency			Charging Eff		充電效率				充電效率
59		32			15			Time to 0.1C10 Discharge Current	Time 0.1C10		0.1C10放電時間				0.1C10放電時間
60		32			15			Time to 0.2C10 Discharge Current	Time 0.2C10		0.2C10放電時間				0.2C10放電時間
61		32			15			Time to 0.3C10 Discharge Current	Time 0.3C10		0.3C10放電時間				0.3C10放電時間
62		32			15			Time to 0.4C10 Discharge Current	Time 0.4C10		0.4C10放電時間				0.4C10放電時間
63		32			15			Time to 0.5C10 Discharge Current	Time 0.5C10		0.5C10放電時間				0.5C10放電時間
64		32			15			Time to 0.6C10 Discharge Current	Time 0.6C10		0.6C10放電時間				0.6C10放電時間
65		32			15			Time to 0.7C10 Discharge Current	Time 0.7C10		0.7C10放電時間				0.7C10放電時間
66		32			15			Time to 0.8C10 Discharge Current	Time 0.8C10		0.8C10放電時間				0.8C10放電時間
67		32			15			Time to 0.9C10 Discharge Current	Time 0.9C10		0.9C10放電時間				0.9C10放電時間
68		32			15			Time to 1.0C10 Discharge Current	Time 1.0C10		1.0C10放電時間				1.0C10放電時間
70		32			15			Temperature Sensor Failure		TempSensorFail		溫度傳感器故障				溫度傳感器故障
71		32			15			High Temperature 1			High Temp 1		高溫					高溫
72		32			15			High Temperature 2			High Temp 2		超高溫					超高溫
73		32			15			Low Temperature				Low Temperature		低溫					低溫
74		32			15			Planned Battery Test Running		PlanBattTestRun		計劃電池測試				計劃電池測試
77		32			15			Short Battery Test Running		ShortBatTestRun		短測試					短測試
81		32			15			Automatic Equalize			Auto EQ			自動均充				自動均充
83		32			15			Abnormal Battery Current		Abnl Batt Curr		電池電流異常				電池電流異常
84		32			15			Temperature Compensation Active		TempComp Active		溫補激活				溫補激活
85		32			15			Battery Current Limit Active		Batt Curr Limit		電池限流				電池限流
86		32			15			Battery Charge Prohibited Alarm		BattChgProhiAlm		禁止電池充電				禁止電池充電
87		32			15			No					No			否					否
88		32			15			Yes					Yes			是					是
90		32			15			None					None			無					無
91		32			15			Temperature 1				Temperature 1		溫度1					溫度1
92		32			15			Temperature 2				Temperature 2		溫度2					溫度2
93		32			15			Temperature 3				Temperature 3		溫度3					溫度3
94		32			15			Temperature 4				Temperature 4		溫度4					溫度4
95		32			15			Temperature 5				Temperature 5		溫度5					溫度5
96		32			15			Temperature 6				Temperature 6		溫度6					溫度6
97		32			15			Temperature 7				Temperature 7		溫度7					溫度7
98		32			15			0					0			0					0
99		32			15			1					1			1					1
100		32			15			2					2			2					2
113		32			15			Float Charge				Float Charge		浮充					浮充
114		32			15			Equalize Charge				EQ Charge		均充					均充
121		32			15			Disabled				Disabled		否					否
122		32			15			Enabled					Enabled			是					是
136		32			15			Record Threshold			RecordThreshold		測試記錄閾值				測試記錄閾值
137		32			15			Remaining Time				Remaining		預計放電時間				剩余
138		32			15			Battery Management State		Batt Management		電池管理状態				電池管理状態
139		32			15			Float Charge				Float Charge		浮充					浮充
140		32			15			Short Test				Short Test		短測試					短測試
141		32			15			Equalize for Test			EQ for Test		預均充					預均充
142		32			15			Manual Test				Manual Test		手動測試				手動測試
143		32			15			Planned Test				Planned Test		計劃測試				計劃測試
144		32			15			AC Fail Test				AC Fail Test		交流停電測試				交流停電測試
145		32			15			AC Fail					AC Fail			交流停電				交流停電
146		32			15			Manual Equalize				Manual EQ		手動均充				手動均充
147		32			15			Auto Equalize				Auto EQ			自動均充				自動均充
148		32			15			Cyclic Equalize				Cyclic EQ		定時均充				定時均充
152		32			15			Over Current Limit			Over Curr Lmt		充電過流點				充電過流點
153		32			15			Stop Battery Test			Stop Batt Test		停止電池測試				電池測試結束
154		32			15			Battery Group				Battery Group		電池串组				電池串组
157		32			15			Master Battery Test			Master Bat Test		跟隨主機測試				跟隨主機測試
158		32			15			Master Equalize				Master EQ		跟隨主機均充				跟隨主機均充
165		32			15			Test Voltage Level			Test Volt Level		電池測試電壓				電池測試電壓
166		32			15			Bad Battery				Bad Battery		壞電池					壞電池
168		32			15			Clear Bad Battery Alarm			Clr Bad Bat Alm		清除壞電池告警				清除壞電池告警
172		32			15			Start Battery Test			Start Batt Test		啟動電池測試				開始電池測試
173		32			15			Stop					Stop			停止					停止
174		32			15			Start					Start			開始					開始
175		32			15			Number of Planned Tests per Year	Planned Tests		年電池測試次數				年電池測試次數
176		32			15			Planned Test 1 (MM-DD Hr)		Test1 (M-D Hr)		電池測試時間1(MM-DD HH)			時間1(M-D H)
177		32			15			Planned Test 2 (MM-DD Hr)		Test2 (M-D Hr)		電池測試時間2(MM-DD HH)			時間2(M-D H)
178		32			15			Planned Test 3 (MM-DD Hr)		Test3 (M-D Hr)		電池測試時間3(MM-DD HH)			時間3(M-D H)
179		32			15			Planned Test 4 (MM-DD Hr)		Test4 (M-D Hr)		電池測試時間4(MM-DD HH)			時間4(M-D H)
180		32			15			Planned Test 5 (MM-DD Hr)		Test5 (M-D Hr)		電池測試時間5(MM-DD HH)			時間5(M-D H)
181		32			15			Planned Test 6 (MM-DD Hr)		Test6 (M-D Hr)		電池測試時間6(MM-DD HH)			時間6(M-D H)
182		32			15			Planned Test 7 (MM-DD Hr)		Test7 (M-D Hr)		電池測試時間7(MM-DD HH)			時間7(M-D H)
183		32			15			Planned Test 8 (MM-DD Hr)		Test8 (M-D Hr)		電池測試時間8(MM-DD HH)			時間8(M-D H)
184		32			15			Planned Test 9 (MM-DD Hr)		Test9 (M-D Hr)		電池測試時間9(MM-DD HH)			時間9(M-D H)
185		32			15			Planned Test 10 (MM-DD Hr)		Test10 (M-D Hr)		電池測試時間10(MM-DD HH)		時間10(M-D H)
186		32			15			Planned Test 11 (MM-DD Hr)		Test11 (M-D Hr)		電池測試時間11(MM-DD HH)		時間11(M-D H)
187		32			15			Planned Test 12 (MM-DD Hr)		Test12 (M-D Hr)		電池測試時間12(MM-DD HH)		時間12(M-D H)
188		32			15			Reset Battery Capacity			Reset Batt Cap		復位電池容量				復位電池容量
191		32			15			Clear Abnormal Bat Current Alarm	Clr AbnlCur Alm		清除電流異常告警			清電流異常告警
192		32			15			Clear Discharge Curr Imbalance		Clr Cur Imb Alm		清除放電不平衡告警			清放電不平衡
193		32			15			Expected Current Limit			ExpectedCurrLmt		當前限流點				當前限流點
194		32			15			Battery Test Running			BatTestRunning		電池測試進行中				電池測試進行中
195		32			15			Low Capacity Point			Low Capacity Pt		低容量告警點				低容量告警點
196		32			15			Battery Discharge			Battery Disch		電池放電				電池放電
197		32			15			Over Voltage				Over Voltage		電池過壓點				電池過壓點
198		32			15			Low Voltage				Low Voltage		電池欠壓點				電池欠壓點
200		32			15			Number of Battery Shunts		Num Batt Shunts		電池分流器個數				電池分流器個數
201		32			15			Imbalance Protection			Imb Protection		不平衡保護				不平衡保護
202		32			15			Temp Compensation Probe Number		TempComp Sensor		溫補用溫度				溫補用溫度
203		32			15			EIB Battery Number			EIB Battery Num		EIB電池數				EIB電池數
204		32			15			Normal					Normal			正常					正常
205		32			15			Special for NA				Special for NA		特殊(北美)				特殊
206		32			15			Battery Voltage Type			Batt Volt Type		電池電壓類型				電池電壓類型
207		32			15			Voltage on Very High Batt Temp		VoltVHiBatTemp		超高溫電壓				超高溫電壓
209		32			15			Battery Temperature Probe Number	BatTempProbeNum		電池用溫度路數				電池用溫度路數
212		32			15			Action on Very High Battery Temp	Action on VHBT		電池高高溫動作				電池高高溫動作
213		32			15			Disabled				Disabled		禁止					禁止
214		32			15			Lowering Voltage			Lowering Volt		調低輸出電壓				調低輸出電壓
215		32			15			Disconnection				Disconnection		電池下電				電池下電
216		32			15			Reconnection Temperature		ReconnectTemp		電池上電溫度				電池上電溫度
217		32			15			Very Hi Temp Volt Setpoint (24V)	VHigh Temp Volt		超高溫電壓(24V)				超高溫電壓
218		32			15			Float Charge Voltage (24V)		Float Voltage		浮充電壓(24V)				浮充電壓
219		32			15			Equalize Charge Voltage (24V)		EQ Voltage		均充電壓(24V)				均充電壓
220		32			15			Test Voltage Limit (24V)		Test Volt Lmt		電池測試電壓(24V)			電池測試電壓
221		32			15			Test End Voltage (24V)			Test End Volt		測試結束電壓(24V)			測試結束電壓
222		32			15			Current Limited				Current Limited		自動限流允許				自動限流允許
223		32			15			Batt Voltage for North America		Batt Volt NA		北美電壓測量模式			北美模式
224		32			15			Battery Changed				Battery Changed		電池改變				電池改變
225		32			15			Battery Test Lowest Capacity		BattTestLowCap		電池測試最低容量			測試需最低容量
226		32			15			Temperature 8				Temperature 8		溫度8					溫度8
227		32			15			Temperature 9				Temperature 9		溫度9					溫度9
228		32			15			Temperature 10				Temperature 10		溫度10					溫度10
229		32			15			LargeDU Rated Capacity			Rated Capacity		LargeDU標稱容量				標稱容量
230		32			15			Clear Battery Test Fail Alarm		ClrBatTestFail		清除電池測試失敗告警			清電池測試失敗
231		32			15			Battery Test Fail			BatteryTestFail		電池測試失敗				電池測試失敗
232		32			15			Maximum					Maximum			最大溫度				最大溫度
233		32			15			Average					Average			平均溫度				平均溫度
234		32			15			Average SMBRC				Average SMBRC		SMBRC平均溫度				SMBRC平均溫度
235		32			15			Compensation Temperature		Comp Temp		溫補溫度				溫補溫度
236		32			15			Comp Temp High1				Comp Temp Hi1		溫補溫度高溫點				溫補高溫點
237		32			15			Comp Temp Low				Comp Temp Low		溫補溫度低溫點				溫補低溫點
238		32			15			Comp Temp High1				Comp Temp Hi1		溫補溫度高				溫補溫度高
239		32			15			Comp Temp Low				Comp Temp Low		溫補溫度低				溫補溫度低
240		32			15			Comp Temp High2				Comp Temp Hi2		溫補溫度過溫點				溫補過溫點
241		32			15			Comp Temp High2				Comp Temp Hi2		溫補過溫				溫補過溫
242		32			15			Compensation Sensor Fault		CompTempFail		溫補溫度故障				溫補溫度故障
243		32			15			Calculate Battery Current		Calc Batt Curr		計算電池電流				計算電池電流
244		32			15			Start Equalize Charge Control(for EEM)	Star EQ(EEM)		均充開始(EEM)				均充開始(EEM)
245		32			15			Start Float Charge Control(for EEM)	Star FLT(EEM)		浮充開始(EEM)				浮充開始(EEM)
246		32			15			Start Resistance Test(for EEM)		Star Res Test(EEM)	開始電阻測試(EEM)			開始電阻測試(EEM)
247		32			15			Stop Resistance Test(for EEM)		Stop Res Test(EEM)	停止電阻測試(EEM)			停止電阻測試(EEM)
248		32			15			Reset Battery Capacity(Internal Use)	Reset Cap(Intuse)	復位電池容量(內部使用)			復位容量(內部用)
249		32			15			Reset Battery Capacity(for EEM)		Reset Cap(EEM)		復位電池容量(EEM)			復位容量(EEM)
250		32			15			Clear Bad Battery Alarm(for EEM)	Clr BadBatAlm(EEM)	清除壞電池告警(EEM)			清除壞電池告警(EEM)
251		32			15			System Temp1				System T1		系統溫度1				系統溫度1
252		32			15			System Temp2				System T2		系統溫度2				系統溫度2
253		32			15			System Temp3				System T3		系統溫度3				系統溫度3
254		32			15			IB2 Temp1				IB2 T1			IB2溫度1				IB2溫度1
255		32			15			IB2 Temp2				IB2 T2			IB2溫度2				IB2溫度2
256		32			15			EIB Temp1				EIB T1			EIB溫度1				EIB溫度1
257		32			15			EIB Temp2				EIB T2			EIB溫度2				EIB溫度2
258		32			15			SMTemp1 Temp1				SMTemp1 T1		SMTemp1溫度1				SMTemp1溫度1
259		32			15			SMTemp1 Temp2				SMTemp1 T2		SMTemp1溫度2				SMTemp1溫度2
260		32			15			SMTemp1 Temp3				SMTemp1 T3		SMTemp1溫度3				SMTemp1溫度3
261		32			15			SMTemp1 Temp4				SMTemp1 T4		SMTemp1溫度4				SMTemp1溫度4
262		32			15			SMTemp1 Temp5				SMTemp1 T5		SMTemp1溫度5				SMTemp1溫度5
263		32			15			SMTemp1 Temp6				SMTemp1 T6		SMTemp1溫度6				SMTemp1溫度6
264		32			15			SMTemp1 Temp7				SMTemp1 T7		SMTemp1溫度7				SMTemp1溫度7
265		32			15			SMTemp1 Temp8				SMTemp1 T8		SMTemp1溫度8				SMTemp1溫度8
266		32			15			SMTemp2 Temp1				SMTemp2 T1		SMTemp2溫度1				SMTemp2溫度1
267		32			15			SMTemp2 Temp2				SMTemp2 T2		SMTemp2溫度2				SMTemp2溫度2
268		32			15			SMTemp2 Temp3				SMTemp2 T3		SMTemp2溫度3				SMTemp2溫度3
269		32			15			SMTemp2 Temp4				SMTemp2 T4		SMTemp2溫度4				SMTemp2溫度4
270		32			15			SMTemp2 Temp5				SMTemp2 T5		SMTemp2溫度5				SMTemp2溫度5
271		32			15			SMTemp2 Temp6				SMTemp2 T6		SMTemp2溫度6				SMTemp2溫度6
272		32			15			SMTemp2 Temp7				SMTemp2 T7		SMTemp2溫度7				SMTemp2溫度7
273		32			15			SMTemp2 Temp8				SMTemp2 T8		SMTemp2溫度8				SMTemp2溫度8
274		32			15			SMTemp3 Temp1				SMTemp3 T1		SMTemp3溫度1				SMTemp3溫度1
275		32			15			SMTemp3 Temp2				SMTemp3 T2		SMTemp3溫度2				SMTemp3溫度2
276		32			15			SMTemp3 Temp3				SMTemp3 T3		SMTemp3溫度3				SMTemp3溫度3
277		32			15			SMTemp3 Temp4				SMTemp3 T4		SMTemp3溫度4				SMTemp3溫度4
278		32			15			SMTemp3 Temp5				SMTemp3 T5		SMTemp3溫度5				SMTemp3溫度5
279		32			15			SMTemp3 Temp6				SMTemp3 T6		SMTemp3溫度6				SMTemp3溫度6
280		32			15			SMTemp3 Temp7				SMTemp3 T7		SMTemp3溫度7				SMTemp3溫度7
281		32			15			SMTemp3 Temp8				SMTemp3 T8		SMTemp3溫度8				SMTemp3溫度8
282		32			15			SMTemp4 Temp1				SMTemp4 T1		SMTemp4溫度1				SMTemp4溫度1
283		32			15			SMTemp4 Temp2				SMTemp4 T2		SMTemp4溫度2				SMTemp4溫度2
284		32			15			SMTemp4 Temp3				SMTemp4 T3		SMTemp4溫度3				SMTemp4溫度3
285		32			15			SMTemp4 Temp4				SMTemp4 T4		SMTemp4溫度4				SMTemp4溫度4
286		32			15			SMTemp4 Temp5				SMTemp4 T5		SMTemp4溫度5				SMTemp4溫度5
287		32			15			SMTemp4 Temp6				SMTemp4 T6		SMTemp4溫度6				SMTemp4溫度6
288		32			15			SMTemp4 Temp7				SMTemp4 T7		SMTemp4溫度7				SMTemp4溫度7
289		32			15			SMTemp4 Temp8				SMTemp4 T8		SMTemp4溫度8				SMTemp4溫度8
290		32			15			SMTemp5 Temp1				SMTemp5 T1		SMTemp5溫度1				SMTemp5溫度1
291		32			15			SMTemp5 Temp2				SMTemp5 T2		SMTemp5溫度2				SMTemp5溫度2
292		32			15			SMTemp5 Temp3				SMTemp5 T3		SMTemp5溫度3				SMTemp5溫度3
293		32			15			SMTemp5 Temp4				SMTemp5 T4		SMTemp5溫度4				SMTemp5溫度4
294		32			15			SMTemp5 Temp5				SMTemp5 T5		SMTemp5溫度5				SMTemp5溫度5
295		32			15			SMTemp5 Temp6				SMTemp5 T6		SMTemp5溫度6				SMTemp5溫度6
296		32			15			SMTemp5 Temp7				SMTemp5 T7		SMTemp5溫度7				SMTemp5溫度7
297		32			15			SMTemp5 Temp8				SMTemp5 T8		SMTemp5溫度8				SMTemp5溫度8
298		32			15			SMTemp6 Temp1				SMTemp6 T1		SMTemp6溫度1				SMTemp6溫度1
299		32			15			SMTemp6 Temp2				SMTemp6 T2		SMTemp6溫度2				SMTemp6溫度2
300		32			15			SMTemp6 Temp3				SMTemp6 T3		SMTemp6溫度3				SMTemp6溫度3
301		32			15			SMTemp6 Temp4				SMTemp6 T4		SMTemp6溫度4				SMTemp6溫度4
302		32			15			SMTemp6 Temp5				SMTemp6 T5		SMTemp6溫度5				SMTemp6溫度5
303		32			15			SMTemp6 Temp6				SMTemp6 T6		SMTemp6溫度6				SMTemp6溫度6
304		32			15			SMTemp6 Temp7				SMTemp6 T7		SMTemp6溫度7				SMTemp6溫度7
305		32			15			SMTemp6 Temp8				SMTemp6 T8		SMTemp6溫度8				SMTemp6溫度8
306		32			15			SMTemp7 Temp1				SMTemp7 T1		SMTemp7溫度1				SMTemp7溫度1
307		32			15			SMTemp7 Temp2				SMTemp7 T2		SMTemp7溫度2				SMTemp7溫度2
308		32			15			SMTemp7 Temp3				SMTemp7 T3		SMTemp7溫度3				SMTemp7溫度3
309		32			15			SMTemp7 Temp4				SMTemp7 T4		SMTemp7溫度4				SMTemp7溫度4
310		32			15			SMTemp7 Temp5				SMTemp7 T5		SMTemp7溫度5				SMTemp7溫度5
311		32			15			SMTemp7 Temp6				SMTemp7 T6		SMTemp7溫度6				SMTemp7溫度6
312		32			15			SMTemp7 Temp7				SMTemp7 T7		SMTemp7溫度7				SMTemp7溫度7
313		32			15			SMTemp7 Temp8				SMTemp7 T8		SMTemp7溫度8				SMTemp7溫度8
314		32			15			SMTemp8 Temp1				SMTemp8 T1		SMTemp8溫度1				SMTemp8溫度1
315		32			15			SMTemp8 Temp2				SMTemp8 T2		SMTemp8溫度2				SMTemp8溫度2
316		32			15			SMTemp8 Temp3				SMTemp8 T3		SMTemp8溫度3				SMTemp8溫度3
317		32			15			SMTemp8 Temp4				SMTemp8 T4		SMTemp8溫度4				SMTemp8溫度4
318		32			15			SMTemp8 Temp5				SMTemp8 T5		SMTemp8溫度5				SMTemp8溫度5
319		32			15			SMTemp8 Temp6				SMTemp8 T6		SMTemp8溫度6				SMTemp8溫度6
320		32			15			SMTemp8 Temp7				SMTemp8 T7		SMTemp8溫度7				SMTemp8溫度7
321		32			15			SMTemp8 Temp8				SMTemp8 T8		SMTemp8溫度8				SMTemp8溫度8
351		32			15			System Temp1 High 2			System T1 Hi2		系統溫度1過過溫				系統溫度1過過溫
352		32			15			System Temp1 High 1			System T1 Hi1		系統溫度1過溫				系統溫度1過溫
353		32			15			System Temp1 Low			System T1 Low		系統溫度1低溫				系統溫度1低溫
354		32			15			System Temp2 High 2			System T2 Hi2		系統溫度2過過溫				系統溫度2過過溫
355		32			15			System Temp2 High 1			System T2 Hi1		系統溫度2過溫				系統溫度2過溫
356		32			15			System Temp2 Low			System T2 Low		系統溫度2低溫				系統溫度2低溫
357		32			15			System Temp3 High 2			System T3 Hi2		系統溫度3過過溫				系統溫度3過過溫
358		32			15			System Temp3 High 1			System T3 Hi1		系統溫度3過溫				系統溫度3過溫
359		32			15			System Temp3 Low			System T3 Low		系統溫度3低溫				系統溫度3低溫
360		32			15			IB2 Temp1 High 2			IB2 T1 Hi2		IB2溫度1過過溫				IB2溫度1過過溫
361		32			15			IB2 Temp1 High 1			IB2 T1 Hi1		IB2溫度1過溫				IB2溫度1過溫
362		32			15			IB2 Temp1 Low				IB2 T1 Low		IB2溫度1低溫				IB2溫度1低溫
363		32			15			IB2 Temp2 High 2			IB2 T2 Hi2		IB2溫度2過過溫				IB2溫度2過過溫
364		32			15			IB2 Temp2 High 1			IB2 T2 Hi1		IB2溫度2過溫				IB2溫度2過溫
365		32			15			IB2 Temp2 Low				IB2 T2 Low		IB2溫度2低溫				IB2溫度2低溫
366		32			15			EIB Temp1 High 2			EIB T1 Hi2		EIB溫度1過過溫				EIB溫度1過過溫
367		32			15			EIB Temp1 High 1			EIB T1 Hi1		EIB溫度1過溫				EIB溫度1過溫
368		32			15			EIB Temp1 Low				EIB T1 Low		EIB溫度1低溫				EIB溫度1低溫
369		32			15			EIB Temp2 High 2			EIB T2 Hi2		EIB溫度2過過溫				EIB溫度2過過溫
370		32			15			EIB Temp2 High 1			EIB T2 Hi1		EIB溫度2過溫				EIB溫度2過溫
371		32			15			EIB Temp2 Low				EIB T2 Low		EIB溫度2低溫				EIB溫度2低溫
372		32			15			SMTemp1 Temp1 High 2			SMTemp1 T1 Hi2		SMTemp1 T1過過溫			SMTemp1T1過過溫
373		32			15			SMTemp1 Temp1 High 1			SMTemp1 T1 Hi1		SMTemp1 T1過溫				SMTemp1T1過溫
374		32			15			SMTemp1 Temp1 Low			SMTemp1 T1 Low		SMTemp1 T1低溫				SMTemp1T1低溫
375		32			15			SMTemp1 Temp2 High 2			SMTemp1 T2 Hi2		SMTemp1 T2過過溫			SMTemp1T2過過溫
376		32			15			SMTemp1 Temp2 High 1			SMTemp1 T2 Hi1		SMTemp1 T2過溫				SMTemp1T2過溫
377		32			15			SMTemp1 Temp2 Low			SMTemp1 T2 Low		SMTemp1 T2低溫				SMTemp1T2低溫
378		32			15			SMTemp1 Temp3 High 2			SMTemp1 T3 Hi2		SMTemp1 T3過過溫			SMTemp1T3過過溫
379		32			15			SMTemp1 Temp3 High 1			SMTemp1 T3 Hi1		SMTemp1 T3過溫				SMTemp1T3過溫
380		32			15			SMTemp1 Temp3 Low			SMTemp1 T3 Low		SMTemp1 T3低溫				SMTemp1T3低溫
381		32			15			SMTemp1 Temp4 High 2			SMTemp1 T4 Hi2		SMTemp1 T4過過溫			SMTemp1T4過過溫
382		32			15			SMTemp1 Temp4 High 1			SMTemp1 T4 Hi1		SMTemp1 T4過溫				SMTemp1T4過溫
383		32			15			SMTemp1 Temp4 Low			SMTemp1 T4 Low		SMTemp1 T4低溫				SMTemp1T4低溫
384		32			15			SMTemp1 Temp5 High 2			SMTemp1 T5 Hi2		SMTemp1 T5過過溫			SMTemp1T5過過溫
385		32			15			SMTemp1 Temp5 High 1			SMTemp1 T5 Hi1		SMTemp1 T5過溫				SMTemp1T5過溫
386		32			15			SMTemp1 Temp5 Low			SMTemp1 T5 Low		SMTemp1 T5低溫				SMTemp1T5低溫
387		32			15			SMTemp1 Temp6 High 2			SMTemp1 T6 Hi2		SMTemp1 T6過過溫			SMTemp1T6過過溫
388		32			15			SMTemp1 Temp6 High 1			SMTemp1 T6 Hi1		SMTemp1 T6過溫				SMTemp1T6過溫
389		32			15			SMTemp1 Temp6 Low			SMTemp1 T6 Low		SMTemp1 T6低溫				SMTemp1T6低溫
390		32			15			SMTemp1 Temp7 High 2			SMTemp1 T7 Hi2		SMTemp1 T7過過溫			SMTemp1T7過過溫
391		32			15			SMTemp1 Temp7 High 1			SMTemp1 T7 Hi1		SMTemp1 T7過溫				SMTemp1T7過溫
392		32			15			SMTemp1 Temp7 Low			SMTemp1 T7 Low		SMTemp1 T7低溫				SMTemp1T7低溫
393		32			15			SMTemp1 Temp8 High 2			SMTemp1 T8 Hi2		SMTemp1 T8過過溫			SMTemp1T8過過溫
394		32			15			SMTemp1 Temp8 High 1			SMTemp1 T8 Hi1		SMTemp1 T8過溫				SMTemp1T8過溫
395		32			15			SMTemp1 Temp8 Low			SMTemp1 T8 Low		SMTemp1 T8低溫				SMTemp1T8低溫
396		32			15			SMTemp2 Temp1 High 2			SMTemp2 T1 Hi2		SMTemp2 T1過過溫			SMTemp2T1過過溫
397		32			15			SMTemp2 Temp1 High 1			SMTemp2 T1 Hi1		SMTemp2 T1過溫				SMTemp2T1過溫
398		32			15			SMTemp2 Temp1 Low			SMTemp2 T1 Low		SMTemp2 T1低溫				SMTemp2T1低溫
399		32			15			SMTemp2 Temp2 High 2			SMTemp2 T2 Hi2		SMTemp2 T2過過溫			SMTemp2T2過過溫
400		32			15			SMTemp2 Temp2 High 1			SMTemp2 T2 Hi1		SMTemp2 T2過溫				SMTemp2T2過溫
401		32			15			SMTemp2 Temp2 Low			SMTemp2 T2 Low		SMTemp2 T2低溫				SMTemp2T2低溫
402		32			15			SMTemp2 Temp3 High 2			SMTemp2 T3 Hi2		SMTemp2 T3過過溫			SMTemp2T3過過溫
403		32			15			SMTemp2 Temp3 High 1			SMTemp2 T3 Hi1		SMTemp2 T3過溫				SMTemp2T3過溫
404		32			15			SMTemp2 Temp3 Low			SMTemp2 T3 Low		SMTemp2 T3低溫				SMTemp2T3低溫
405		32			15			SMTemp2 Temp4 High 2			SMTemp2 T4 Hi2		SMTemp2 T4過過溫			SMTemp2T4過過溫
406		32			15			SMTemp2 Temp4 High 1			SMTemp2 T4 Hi1		SMTemp2 T4過溫				SMTemp2T4過溫
407		32			15			SMTemp2 Temp4 Low			SMTemp2 T4 Low		SMTemp2 T4低溫				SMTemp2T4低溫
408		32			15			SMTemp2 Temp5 High 2			SMTemp2 T5 Hi2		SMTemp2 T5過過溫			SMTemp2T5過過溫
409		32			15			SMTemp2 Temp5 High 1			SMTemp2 T5 Hi1		SMTemp2 T5過溫				SMTemp2T5過溫
410		32			15			SMTemp2 Temp5 Low			SMTemp2 T5 Low		SMTemp2 T5低溫				SMTemp2T5低溫
411		32			15			SMTemp2 Temp6 High 2			SMTemp2 T6 Hi2		SMTemp2 T6過過溫			SMTemp2T6過過溫
412		32			15			SMTemp2 Temp6 High 1			SMTemp2 T6 Hi1		SMTemp2 T6過溫				SMTemp2T6過溫
413		32			15			SMTemp2 Temp6 Low			SMTemp2 T6 Low		SMTemp2 T6低溫				SMTemp2T6低溫
414		32			15			SMTemp2 Temp7 High 2			SMTemp2 T7 Hi2		SMTemp2 T7過過溫			SMTemp2T7過過溫
415		32			15			SMTemp2 Temp7 High 1			SMTemp2 T7 Hi1		SMTemp2 T7過溫				SMTemp2T7過溫
416		32			15			SMTemp2 Temp7 Low			SMTemp2 T7 Low		SMTemp2 T7低溫				SMTemp2T7低溫
417		32			15			SMTemp2 Temp8 High 2			SMTemp2 T8 Hi2		SMTemp2 T8過過溫			SMTemp2T8過過溫
418		32			15			SMTemp2 Temp8 High 1			SMTemp2 T8 Hi1		SMTemp2 T8過溫				SMTemp2T8過溫
419		32			15			SMTemp2 Temp8 Low			SMTemp2 T8 Low		SMTemp2 T8低溫				SMTemp2T8低溫
420		32			15			SMTemp3 Temp1 High 2			SMTemp3 T1 Hi2		SMTemp3 T1過過溫			SMTemp3T1過過溫
421		32			15			SMTemp3 Temp1 High 1			SMTemp3 T1 Hi1		SMTemp3 T1過溫				SMTemp3T1過溫
422		32			15			SMTemp3 Temp1 Low			SMTemp3 T1 Low		SMTemp3 T1低溫				SMTemp3T1低溫
423		32			15			SMTemp3 Temp2 High 2			SMTemp3 T2 Hi2		SMTemp3 T2過過溫			SMTemp3T2過過溫
424		32			15			SMTemp3 Temp2 High 1			SMTemp3 T2 Hi1		SMTemp3 T2過溫				SMTemp3T2過溫
425		32			15			SMTemp3 Temp2 Low			SMTemp3 T2 Low		SMTemp3 T2低溫				SMTemp3T2低溫
426		32			15			SMTemp3 Temp3 High 2			SMTemp3 T3 Hi2		SMTemp3 T3過過溫			SMTemp3T3過過溫
427		32			15			SMTemp3 Temp3 High 1			SMTemp3 T3 Hi1		SMTemp3 T3過溫				SMTemp3T3過溫
428		32			15			SMTemp3 Temp3 Low			SMTemp3 T3 Low		SMTemp3 T3低溫				SMTemp3T3低溫
429		32			15			SMTemp3 Temp4 High 2			SMTemp3 T4 Hi2		SMTemp3 T4過過溫			SMTemp3T4過過溫
430		32			15			SMTemp3 Temp4 High 1			SMTemp3 T4 Hi1		SMTemp3 T4過溫				SMTemp3T4過溫
431		32			15			SMTemp3 Temp4 Low			SMTemp3 T4 Low		SMTemp3 T4低溫				SMTemp3T4低溫
432		32			15			SMTemp3 Temp5 High 2			SMTemp3 T5 Hi2		SMTemp3 T5過過溫			SMTemp3T5過過溫
433		32			15			SMTemp3 Temp5 High 1			SMTemp3 T5 Hi1		SMTemp3 T5過溫				SMTemp3T5過溫
434		32			15			SMTemp3 Temp5 Low			SMTemp3 T5 Low		SMTemp3 T5低溫				SMTemp3T5低溫
435		32			15			SMTemp3 Temp6 High 2			SMTemp3 T6 Hi2		SMTemp3 T6過過溫			SMTemp3T6過過溫
436		32			15			SMTemp3 Temp6 High 1			SMTemp3 T6 Hi1		SMTemp3 T6過溫				SMTemp3T6過溫
437		32			15			SMTemp3 Temp6 Low			SMTemp3 T6 Low		SMTemp3 T6低溫				SMTemp3T6低溫
438		32			15			SMTemp3 Temp7 High 2			SMTemp3 T7 Hi2		SMTemp3 T7過過溫			SMTemp3T7過過溫
439		32			15			SMTemp3 Temp7 High 1			SMTemp3 T7 Hi1		SMTemp3 T7過溫				SMTemp3T7過溫
440		32			15			SMTemp3 Temp7 Low			SMTemp3 T7 Low		SMTemp3 T7低溫				SMTemp3T7低溫
441		32			15			SMTemp3 Temp8 High 2			SMTemp3 T8 Hi2		SMTemp3 T8過過溫			SMTemp3T8過過溫
442		32			15			SMTemp3 Temp8 High 1			SMTemp3 T8 Hi1		SMTemp3 T8過溫				SMTemp3T8過溫
443		32			15			SMTemp3 Temp8 Low			SMTemp3 T8 Low		SMTemp3 T8低溫				SMTemp3T8低溫
444		32			15			SMTemp4 Temp1 High 2			SMTemp4 T1 Hi2		SMTemp4 T1過過溫			SMTemp4T1過過溫
445		32			15			SMTemp4 Temp1 High 1			SMTemp4 T1 Hi1		SMTemp4 T1過溫				SMTemp4T1過溫
446		32			15			SMTemp4 Temp1 Low			SMTemp4 T1 Low		SMTemp4 T1低溫				SMTemp4T1低溫
447		32			15			SMTemp4 Temp2 High 2			SMTemp4 T2 Hi2		SMTemp4 T2過過溫			SMTemp4T2過過溫
448		32			15			SMTemp4 Temp2 High 1			SMTemp4 T2 Hi1		SMTemp4 T2過溫				SMTemp4T2過溫
449		32			15			SMTemp4 Temp2 Low			SMTemp4 T2 Low		SMTemp4 T2低溫				SMTemp4T2低溫
450		32			15			SMTemp4 Temp3 High 2			SMTemp4 T3 Hi2		SMTemp4 T3過過溫			SMTemp4T3過過溫
451		32			15			SMTemp4 Temp3 High 1			SMTemp4 T3 Hi1		SMTemp4 T3過溫				SMTemp4T3過溫
452		32			15			SMTemp4 Temp3 Low			SMTemp4 T3 Low		SMTemp4 T3低溫				SMTemp4T3低溫
453		32			15			SMTemp4 Temp4 High 2			SMTemp4 T4 Hi2		SMTemp4 T4過過溫			SMTemp4T4過過溫
454		32			15			SMTemp4 Temp4 High 1			SMTemp4 T4 Hi1		SMTemp4 T4過溫				SMTemp4T4過溫
455		32			15			SMTemp4 Temp4 Low			SMTemp4 T4 Low		SMTemp4 T4低溫				SMTemp4T4低溫
456		32			15			SMTemp4 Temp5 High 2			SMTemp4 T5 Hi2		SMTemp4 T5過過溫			SMTemp4T5過過溫
457		32			15			SMTemp4 Temp5 High 1			SMTemp4 T5 Hi1		SMTemp4 T5過溫				SMTemp4T5過溫
458		32			15			SMTemp4 Temp5 Low			SMTemp4 T5 Low		SMTemp4 T5低溫				SMTemp4T5低溫
459		32			15			SMTemp4 Temp6 High 2			SMTemp4 T6 Hi2		SMTemp4 T6過過溫			SMTemp4T6過過溫
460		32			15			SMTemp4 Temp6 High 1			SMTemp4 T6 Hi1		SMTemp4 T6過溫				SMTemp4T6過溫
461		32			15			SMTemp4 Temp6 Low			SMTemp4 T6 Low		SMTemp4 T6低溫				SMTemp4T6低溫
462		32			15			SMTemp4 Temp7 High 2			SMTemp4 T7 Hi2		SMTemp4 T7過過溫			SMTemp4T7過過溫
463		32			15			SMTemp4 Temp7 High 1			SMTemp4 T7 Hi1		SMTemp4 T7過溫				SMTemp4T7過溫
464		32			15			SMTemp4 Temp7 Low			SMTemp4 T7 Low		SMTemp4 T7低溫				SMTemp4T7低溫
465		32			15			SMTemp4 Temp8 High 2			SMTemp4 T8 Hi2		SMTemp4 T8過過溫			SMTemp4T8過過溫
466		32			15			SMTemp4 Temp8 Hi1			SMTemp4 T8 Hi1		SMTemp4 T8過溫				SMTemp4T8過溫
467		32			15			SMTemp4 Temp8 Low			SMTemp4 T8 Low		SMTemp4 T8低溫				SMTemp4T8低溫
468		32			15			SMTemp5 Temp1 High 2			SMTemp5 T1 Hi2		SMTemp5 T1過過溫			SMTemp5T1過過溫
469		32			15			SMTemp5 Temp1 High 1			SMTemp5 T1 Hi1		SMTemp5 T1過溫				SMTemp5T1過溫
470		32			15			SMTemp5 Temp1 Low			SMTemp5 T1 Low		SMTemp5 T1低溫				SMTemp5T1低溫
471		32			15			SMTemp5 Temp2 High 2			SMTemp5 T2 Hi2		SMTemp5 T2過過溫			SMTemp5T2過過溫
472		32			15			SMTemp5 Temp2 High 1			SMTemp5 T2 Hi1		SMTemp5 T2過溫				SMTemp5T2過溫
473		32			15			SMTemp5 Temp2 Low			SMTemp5 T2 Low		SMTemp5 T2低溫				SMTemp5T2低溫
474		32			15			SMTemp5 Temp3 High 2			SMTemp5 T3 Hi2		SMTemp5 T3過過溫			SMTemp5T3過過溫
475		32			15			SMTemp5 Temp3 High 1			SMTemp5 T3 Hi1		SMTemp5 T3過溫				SMTemp5T3過溫
476		32			15			SMTemp5 Temp3 Low			SMTemp5 T3 Low		SMTemp5 T3低溫				SMTemp5T3低溫
477		32			15			SMTemp5 Temp4 High 2			SMTemp5 T4 Hi2		SMTemp5 T4過過溫			SMTemp5T4過過溫
478		32			15			SMTemp5 Temp4 High 1			SMTemp5 T4 Hi1		SMTemp5 T4過溫				SMTemp5T4過溫
479		32			15			SMTemp5 Temp4 Low			SMTemp5 T4 Low		SMTemp5 T4低溫				SMTemp5T4低溫
480		32			15			SMTemp5 Temp5 High 2			SMTemp5 T5 Hi2		SMTemp5 T5過過溫			SMTemp5T5過過溫
481		32			15			SMTemp5 Temp5 High 1			SMTemp5 T5 Hi1		SMTemp5 T5過溫				SMTemp5T5過溫
482		32			15			SMTemp5 Temp5 Low			SMTemp5 T5 Low		SMTemp5 T5低溫				SMTemp5T5低溫
483		32			15			SMTemp5 Temp6 High 2			SMTemp5 T6 Hi2		SMTemp5 T6過過溫			SMTemp5T6過過溫
484		32			15			SMTemp5 Temp6 High 1			SMTemp5 T6 Hi1		SMTemp5 T6過溫				SMTemp5T6過溫
485		32			15			SMTemp5 Temp6 Low			SMTemp5 T6 Low		SMTemp5 T6低溫				SMTemp5T6低溫
486		32			15			SMTemp5 Temp7 High 2			SMTemp5 T7 Hi2		SMTemp5 T7過過溫			SMTemp5T7過過溫
487		32			15			SMTemp5 Temp7 High 1			SMTemp5 T7 Hi1		SMTemp5 T7過溫				SMTemp5T7過溫
488		32			15			SMTemp5 Temp7 Low			SMTemp5 T7 Low		SMTemp5 T7低溫				SMTemp5T7低溫
489		32			15			SMTemp5 Temp8 High 2			SMTemp5 T8 Hi2		SMTemp5 T8過過溫			SMTemp5T8過過溫
490		32			15			SMTemp5 Temp8 High 1			SMTemp5 T8 Hi1		SMTemp5 T8過溫				SMTemp5T8過溫
491		32			15			SMTemp5 Temp8 Low			SMTemp5 T8 Low		SMTemp5 T8低溫				SMTemp5T8低溫
492		32			15			SMTemp6 Temp1 High 2			SMTemp6 T1 Hi2		SMTemp6 T1過過溫			SMTemp6T1過過溫
493		32			15			SMTemp6 Temp1 High 1			SMTemp6 T1 Hi1		SMTemp6 T1過溫				SMTemp6T1過溫
494		32			15			SMTemp6 Temp1 Low			SMTemp6 T1 Low		SMTemp6 T1低溫				SMTemp6T1低溫
495		32			15			SMTemp6 Temp2 High 2			SMTemp6 T2 Hi2		SMTemp6 T2過過溫			SMTemp6T2過過溫
496		32			15			SMTemp6 Temp2 High 1			SMTemp6 T2 Hi1		SMTemp6 T2過溫				SMTemp6T2過溫
497		32			15			SMTemp6 Temp2 Low			SMTemp6 T2 Low		SMTemp6 T2低溫				SMTemp6T2低溫
498		32			15			SMTemp6 Temp3 High 2			SMTemp6 T3 Hi2		SMTemp6 T3過過溫			SMTemp6T3過過溫
499		32			15			SMTemp6 Temp3 High 1			SMTemp6 T3 Hi1		SMTemp6 T3過溫				SMTemp6T3過溫
500		32			15			SMTemp6 Temp3 Low			SMTemp6 T3 Low		SMTemp6 T3低溫				SMTemp6T3低溫
501		32			15			SMTemp6 Temp4 High 2			SMTemp6 T4 Hi2		SMTemp6 T4過過溫			SMTemp6T4過過溫
502		32			15			SMTemp6 Temp4 High 1			SMTemp6 T4 Hi1		SMTemp6 T4過溫				SMTemp6T4過溫
503		32			15			SMTemp6 Temp4 Low			SMTemp6 T4 Low		SMTemp6 T4低溫				SMTemp6T4低溫
504		32			15			SMTemp6 Temp5 High 2			SMTemp6 T5 Hi2		SMTemp6 T5過過溫			SMTemp6T5過過溫
505		32			15			SMTemp6 Temp5 High 1			SMTemp6 T5 Hi1		SMTemp6 T5過溫				SMTemp6T5過溫
506		32			15			SMTemp6 Temp5 Low			SMTemp6 T5 Low		SMTemp6 T5低溫				SMTemp6T5低溫
507		32			15			SMTemp6 Temp6 High 2			SMTemp6 T6 Hi2		SMTemp6 T6過過溫			SMTemp6T6過過溫
508		32			15			SMTemp6 Temp6 High 1			SMTemp6 T6 Hi1		SMTemp6 T6過溫				SMTemp6T6過溫
509		32			15			SMTemp6 Temp6 Low			SMTemp6 T6 Low		SMTemp6 T6低溫				SMTemp6T6低溫
510		32			15			SMTemp6 Temp7 High 2			SMTemp6 T7 Hi2		SMTemp6 T7過過溫			SMTemp6T7過過溫
511		32			15			SMTemp6 Temp7 High 1			SMTemp6 T7 Hi1		SMTemp6 T7過溫				SMTemp6T7過溫
512		32			15			SMTemp6 Temp7 Low			SMTemp6 T7 Low		SMTemp6 T7低溫				SMTemp6T7低溫
513		32			15			SMTemp6 Temp8 High 2			SMTemp6 T8 Hi2		SMTemp6 T8過過溫			SMTemp6T8過過溫
514		32			15			SMTemp6 Temp8 High 1			SMTemp6 T8 Hi1		SMTemp6 T8過溫				SMTemp6T8過溫
515		32			15			SMTemp6 Temp8 Low			SMTemp6 T8 Low		SMTemp6 T8低溫				SMTemp6T8低溫
516		32			15			SMTemp7 Temp1 High 2			SMTemp7 T1 Hi2		SMTemp7 T1過過溫			SMTemp7T1過過溫
517		32			15			SMTemp7 Temp1 High 1			SMTemp7 T1 Hi1		SMTemp7 T1過溫				SMTemp7T1過溫
518		32			15			SMTemp7 Temp1 Low			SMTemp7 T1 Low		SMTemp7 T1低溫				SMTemp7T1低溫
519		32			15			SMTemp7 Temp2 High 2			SMTemp7 T2 Hi2		SMTemp7 T2過過溫			SMTemp7T2過過溫
520		32			15			SMTemp7 Temp2 High 1			SMTemp7 T2 Hi1		SMTemp7 T2過溫				SMTemp7T2過溫
521		32			15			SMTemp7 Temp2 Low			SMTemp7 T2 Low		SMTemp7 T2低溫				SMTemp7T2低溫
522		32			15			SMTemp7 Temp3 High 2			SMTemp7 T3 Hi2		SMTemp7 T3過過溫			SMTemp7T3過過溫
523		32			15			SMTemp7 Temp3 High 1			SMTemp7 T3 Hi1		SMTemp7 T3過溫				SMTemp7T3過溫
524		32			15			SMTemp7 Temp3 Low			SMTemp7 T3 Low		SMTemp7 T3低溫				SMTemp7T3低溫
525		32			15			SMTemp7 Temp4 High 2			SMTemp7 T4 Hi2		SMTemp7 T4過過溫			SMTemp7T4過過溫
526		32			15			SMTemp7 Temp4 High 1			SMTemp7 T4 Hi1		SMTemp7 T4過溫				SMTemp7T4過溫
527		32			15			SMTemp7 Temp4 Low			SMTemp7 T4 Low		SMTemp7 T4低溫				SMTemp7T4低溫
528		32			15			SMTemp7 Temp5 High 2			SMTemp7 T5 Hi2		SMTemp7 T5過過溫			SMTemp7T5過過溫
529		32			15			SMTemp7 Temp5 High 1			SMTemp7 T5 Hi1		SMTemp7 T5過溫				SMTemp7T5過溫
530		32			15			SMTemp7 Temp5 Low			SMTemp7 T5 Low		SMTemp7 T5低溫				SMTemp7T5低溫
531		32			15			SMTemp7 Temp6 High 2			SMTemp7 T6 Hi2		SMTemp7 T6過過溫			SMTemp7T6過過溫
532		32			15			SMTemp7 Temp6 High 1			SMTemp7 T6 Hi1		SMTemp7 T6過溫				SMTemp7T6過溫
533		32			15			SMTemp7 Temp6 Low			SMTemp7 T6 Low		SMTemp7 T6低溫				SMTemp7T6低溫
534		32			15			SMTemp7 Temp7 High 2			SMTemp7 T7 Hi2		SMTemp7 T7過過溫			SMTemp7T7過過溫
535		32			15			SMTemp7 Temp7 High 1			SMTemp7 T7 Hi1		SMTemp7 T7過溫				SMTemp7T7過溫
536		32			15			SMTemp7 Temp7 Low			SMTemp7 T7 Low		SMTemp7 T7低溫				SMTemp7T7低溫
537		32			15			SMTemp7 Temp8 High 2			SMTemp7 T8 Hi2		SMTemp7 T8過過溫			SMTemp7T8過過溫
538		32			15			SMTemp7 Temp8 High 1			SMTemp7 T8 Hi1		SMTemp7 T8過溫				SMTemp7T8過溫
539		32			15			SMTemp7 Temp8 Low			SMTemp7 T8 Low		SMTemp7 T8低溫				SMTemp7T8低溫
540		32			15			SMTemp8 Temp1 High 2			SMTemp8 T1 Hi2		SMTemp8 T1過過溫			SMTemp8T1過過溫
541		32			15			SMTemp8 Temp1 High 1			SMTemp8 T1 Hi1		SMTemp8 T1過溫				SMTemp8T1過溫
542		32			15			SMTemp8 Temp1 Low			SMTemp8 T1 Low		SMTemp8 T1低溫				SMTemp8T1低溫
543		32			15			SMTemp8 Temp2 High 2			SMTemp8 T2 Hi2		SMTemp8 T2過過溫			SMTemp8T2過過溫
544		32			15			SMTemp8 Temp2 High 1			SMTemp8 T2 Hi1		SMTemp8 T2過溫				SMTemp8T2過溫
545		32			15			SMTemp8 Temp2 Low			SMTemp8 T2 Low		SMTemp8 T2低溫				SMTemp8T2低溫
546		32			15			SMTemp8 Temp3 High 2			SMTemp8 T3 Hi2		SMTemp8 T3過過溫			SMTemp8T3過過溫
547		32			15			SMTemp8 Temp3 High 1			SMTemp8 T3 Hi1		SMTemp8 T3過溫				SMTemp8T3過溫
548		32			15			SMTemp8 Temp3 Low			SMTemp8 T3 Low		SMTemp8 T3低溫				SMTemp8T3低溫
549		32			15			SMTemp8 Temp4 High 2			SMTemp8 T4 Hi2		SMTemp8 T4過過溫			SMTemp8T4過過溫
550		32			15			SMTemp8 Temp4 High 1			SMTemp8 T4 Hi1		SMTemp8 T4過溫				SMTemp8T4過溫
551		32			15			SMTemp8 Temp4 Low			SMTemp8 T4 Low		SMTemp8 T4低溫				SMTemp8T4低溫
552		32			15			SMTemp8 Temp5 High 2			SMTemp8 T5 Hi2		SMTemp8 T5過過溫			SMTemp8T5過過溫
553		32			15			SMTemp8 Temp5 High 1			SMTemp8 T5 Hi1		SMTemp8 T5過溫				SMTemp8T5過溫
554		32			15			SMTemp8 Temp5 Low			SMTemp8 T5 Low		SMTemp8 T5低溫				SMTemp8T5低溫
555		32			15			SMTemp8 Temp6 High 2			SMTemp8 T6 Hi2		SMTemp8 T6過過溫			SMTemp8T6過過溫
556		32			15			SMTemp8 Temp6 High 1			SMTemp8 T6 Hi1		SMTemp8 T6過溫				SMTemp8T6過溫
557		32			15			SMTemp8 Temp6 Low			SMTemp8 T6 Low		SMTemp8 T6低溫				SMTemp8T6低溫
558		32			15			SMTemp8 Temp7 High 2			SMTemp8 T7 Hi2		SMTemp8 T7過過溫			SMTemp8T7過過溫
559		32			15			SMTemp8 Temp7 High 1			SMTemp8 T7 Hi1		SMTemp8 T7過溫				SMTemp8T7過溫
560		32			15			SMTemp8 Temp7 Low			SMTemp8 T7 Low		SMTemp8 T7低溫				SMTemp8T7低溫
561		32			15			SMTemp8 Temp8 High 2			SMTemp8 T8 Hi2		SMTemp8 T8過過溫			SMTemp8T8過過溫
562		32			15			SMTemp8 Temp8 High 1			SMTemp8 T8 Hi1		SMTemp8 T8過溫				SMTemp8T8過溫
563		32			15			SMTemp8 Temp8 Low			SMTemp8 T8 Low		SMTemp8 T8低溫				SMTemp8T8低溫
564		32			15			Temp Comp Voltage Clamp			Temp Comp Clamp		北美溫補模式				北美溫補模式
565		32			15			Temp Comp Max Voltage			Temp Comp Max V		北美溫補高壓限				北美溫補高壓限
566		32			15			Temp Comp Min Voltage			Temp Comp Min V		北美溫補低壓限				北美溫補低壓限
567		32			15			BTRM Temperature			BTRM Temp		BTRM溫度傳感器				BTRM溫度
568		32			15			BTRM Temp High 2			BTRM Temp High2		BTRM溫度過過溫				BTRM過過溫
569		32			15			BTRM Temp High 1			BTRM Temp High1		BTRM溫度過溫				BTRM過溫
570		32			15			Temp Comp Max Voltage (24V)		Temp Comp Max V		北美溫補高壓限(24V)			北美溫補高壓限
571		32			15			Temp Comp Min Voltage (24V)		Temp Comp Min V		北美溫補低壓限(24V)			北美溫補低壓限
572		32			15			BTRM Temp Sensor			BTRM TempSensor		BTRM溫度傳感器				BTRM溫度
573		32			15			BTRM Sensor Fault			BTRM TempFault		BTRM傳感器故障				BTRM故障
574		32			15			Li-Ion Group Avg Temperature		LiBatt AvgTemp		Li電池组平均溫度			Li電池平均溫度
575		32			15			Number of Installed Batteries		Num Installed		已安装的電池數目			已安装電池數
576		32			15			Number of Disconnected Batteries	NumDisconnected		未連接的電池數目			未連接電池數
577		32			15			Inventory Update In Process		InventUpdating		鋰電池模塊地址重排			電池地址重排
578		32			15			Number of Communication Fail Batteries	Num Comm Fail		通信失敗電池數目			通信失敗電池數
579		32			15			Li-Ion Battery Lost			LiBatt Lost		Li電池丢失				Li電池丢失
580		32			15			System Battery Type			Sys Batt Type		系統電池類型				系統電池類型
581		32			15			1 Li-Ion Battery Disconnect		1 LiBattDiscon		1個Li電池未連接				1Li電池未連接
582		32			15			2+Li-Ion Battery Disconnect		2+LiBattDiscon		2個以上Li電池未連接			多Li電池未連接
583		32			15			1 Li-Ion Battery No Reply		1 LiBattNoReply		1個Li電池通信失敗			1Li電池無響應
584		32			15			2+Li-Ion Battery No Reply		2+LiBattNoReply		2個以上Li電池通信失敗			多Li電池無響應
585		32			15			Clear Li-Ion Battery Lost		Clr LiBatt Lost		清除Li電池丢失				清除Li電池丢失
586		32			15			Clear					Clear			清除					清除
587		32			15			Float Charge Voltage(Solar)		Float Volt(S)		浮充電壓(太陽能)			浮充電壓(S)
588		32			15			Equalize Charge Voltage(Solar)		EQ Voltage(S)		均充電壓(太陽能)			均充電壓(S)
589		32			15			Float Charge Voltage(RECT)		Float Volt(R)		浮充電壓(RECT)				浮充電壓(R)
590		32			15			Equalize Charge Voltage(RECT)		EQ Voltage(R)		均充電壓(RECT)				均充電壓(R)
591		32			15			Active Battery Current Limit		ABCL Point		活動Li電池限流點			ABCL點
592		32			15			Clear Li Battery CommInterrupt		ClrLiBatComFail		清除Li電池通信中斷告警			清Li電池通信斷
593		32			15			ABCL is active				ABCL Active		ABCL模式開啟				ABCL模式開啟
594		32			15			Last SMBAT Battery Number		Last SMBAT Num		尾SMBAT電池數				尾SMBAT電池數
595		32			15			Voltage Adjust Gain			VoltAdjustGain		調壓系數				調壓系數
596		32			15			Curr Limited Mode			Curr Limit Mode		限流方式				限流方式
597		32			15			Current					Current			電流方式				電流方式
598		32			15			Voltage					Voltage			電壓方式				電壓方式
599		32			15			Battery Charge Prohibited Status	Charge Prohibit		禁止電池充電				禁止電池充電
600		32			15			Battery Charge Prohibited Alarm		Charge Prohibit		禁止電池充電				禁止電池充電
601		32			15			Battery Lower Capacity			Lower Capacity		最小當前容量				最小當前容量
602		32			15			Charge Current				Charge Current		充電電流				充電電流
603		32			15			Upper Limit				Upper Lmt		電池電流上限				電池電流上限
604		32			15			Stable Range Upper Limit		Stable Up Lmt		电流范围上限				电流范围上限
605		32			15			Stable Range Lower Limit		Stable Low Lmt		電流範圍上限				電流範圍上限
606		32			15			Speed Set Point				Speed Set Point		調速點					調速點
607		32			15			Slow Speed Coefficient			Slow Coeff		慢速系數				慢速系數
608		32			15			Fast Speed Coefficient			Fast Coeff		快速系數				快速系數
609		32			15			Min Amplitude				Min Amplitude		最小幅值				最小幅值
610		32			15			Max Amplitude				Max Amplitude		最大幅值				最大幅值
611		32			15			Cycle Number				Cycle Num		周期個數				周期個數
613		32			15			EQTemp Comp Coefficient			EQCompCoeff		EQ溫補系數				EQ溫補系數	
614		32			15			Mid-Point Volt(MB)			Mid-Point(MB)		Mid-Point Volt(MB)			Mid-Point(MB)
615		32			15			Bad Battery Block			Bad BattBlock		Bad Battery Block			Bad BattBlock
616		32			15			1					1			1					1
617		32			15			2					2			2					2
618		32			15			Voltage Diff(Mid)			Voltage Diff(Mid)	Voltage Diff(Mid)			Voltage Diff(Mid)
619		32			15			Bad Battery Block2			Bad BattBlock2		Bad Battery Block2			Bad BattBlock2
620		32			15			Monitor BattCurrImbal		BattCurrImbal			Monitor BattCurrImbal		BattCurrImbal	
621		32			15			Diviation Limit				Diviation Lmt			Diviation Limit				Diviation Lmt	
622		32			15			Allowed Diviation Length	AllowDiviatLen			Allowed Diviation Length	AllowDiviatLen	
623		32			15			Alarm Clear Time			AlarmClrTime			Alarm Clear Time			AlarmClrTime	
624		32			15			Battery Test End 10Min		BT End 10Min			Battery Test End 10Min		BT End 10Min

700		32			15			Clear Batt Curr Pos Threshold		Pos Threshold		電池電流清零正閾值			正閾值
701		32			15			Clear Batt Curr Neg Threshold		Neg Threshold		電池電流清零負閾值			負閾值
702		32			15			Battery test Multiple Abort				BT MultiAbort		Battery test Multiple Abort				BT MultiAbort
703		32			15			Clear Battery test Multiple Abort		ClrBTMultiAbort		Clear Battery test Multiple Abort		ClrBTMultiAbort
704		32			15			BT Interrupted-Main Failure				BT Intrp-MF			BT Interrupted-Main Failure				BT Intrp-MF
705		32			15			Min Voltage for BCL					Min BCL Volt				調壓限流最小電壓		最小電壓
706		32			15			Action of BattFuseAlm				ActBattFuseAlm				電池熔丝告警后的措施	熔丝断措施
707		32			15			Adjust to Min Voltage				AdjustMinVolt				調至最小電压			最小電压
708		32			15			Adjust to Default Voltage			AdjustDefltVolt				調至默认電压			默认電压
