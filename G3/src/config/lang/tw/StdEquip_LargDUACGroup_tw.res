﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			AC Distribution Number			AC Distri Num		交流屏數				交流屏數
2		32			15			LargeDU AC Distribution Group		LargeDUACDistGr		LargDU交流屏组				交流屏组
3		32			15			Over Voltage Limit			Over Volt Limit		輸入過壓告警點				輸入過壓點
4		32			15			Under Voltage Limit			Under Volt Lmt		輸入欠壓告警點				輸入欠壓點
5		32			15			Phase Failure Voltage			Phase Fail Volt		輸入缺相告警點				輸入缺相點
6		32			15			Over Frequency Limit			Over Freq Limit		頻率過高告警點				頻率過高點
7		32			15			Under Frequency Limit			Under Freq Lmt		頻率過低告警點				頻率過低點
8		32			15			Mains Failure				Mains Failure		交流停電				交流停電
9		32			15			Noraml					Normal			正常					正常
10		32			15			Alarm					Alarm			告警					告警
11		32			15			Mains Failure				Mains Failure		交流停電				交流停電
12		32			15			Existence State				Existence State		是否存在				是否存在
13		32			15			Existent				Existent		存在					存在
14		32			15			Not Existent				Not Existent		不存在					不存在
