﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			LVD Unit				LVD Unit		LVD					LVD
11		32			15			Connected				Connected		連接					連接
12		32			15			Disconnected				Disconnected		斷開					斷開
13		32			15			No					No			否					否
14		32			15			Yes					Yes			是					是
21		32			15			LVD 1 Status				LVD 1 Status		LVD1状態				LVD1状態
22		32			15			LVD 2 Status				LVD 2 Status		LVD2状態				LVD2状態
23		32			15			LVD 1 Failure				LVD 1 Failure		LVD1控制失敗				LVD1控制失敗
24		32			15			LVD 2 Failure				LVD 2 Failure		LVD2控制失敗				LVD2控制失敗
25		32			15			Communication Failure			Comm Failure		通信中斷				通信中斷
26		32			15			State					State			工作状態				工作状態
27		32			15			LVD 1 Control				LVD 1 Control		LVD1控制				LVD1控制
28		32			15			LVD 2 Control				LVD 2 Control		LVD2控制				LVD2控制
29		32			15			LVD Disabled				LVD Disabled		LVD Disabled				LVD Disabled
31		32			15			LVD 1					LVD 1			LVD1允許				LVD1允許
32		32			15			LVD 1 Mode				LVD 1 Mode		LVD1方式				LVD1方式
33		32			15			LVD 1 Voltage				LVD 1 Voltage		LVD1電壓				LVD1電壓
34		32			15			LVD 1 Reconnect Voltage			LVD1 Recon Volt		LVD1上電電壓				LVD1上電電壓
35		32			15			LVD 1 Reconnect Delay			LVD1 ReconDelay		LVD1上電延遲				LVD1上電延遲
36		32			15			LVD 1 Time				LVD 1 Time		LVD1時間				LVD1時間
37		32			15			LVD 1 Dependency			LVD1 Dependency		LVD1依賴關系				LVD1依賴關系
41		32			15			LVD 2					LVD 2			LVD2允許				LVD2允許
42		32			15			LVD 2 Mode				LVD 2 Mode		LVD2方式				LVD2方式
43		32			15			LVD 2 Voltage				LVD 2 Voltage		LVD2電壓				LVD2電壓
44		32			15			LVD 2 Reconnect Voltage			LVD2 Recon Volt		LVD2上電電壓				LVD2上電電壓
45		32			15			LVD 2 Reconnect Delay			LVD2 ReconDelay		LVD2上電延遲				LVD2上電延遲
46		32			15			LVD 2 Time				LVD 2 Time		LVD2時間				LVD2時間
47		32			15			LVD 2 Dependency			LVD2 Dependency		LVD2依賴關系				LVD2依賴關系
51		32			15			Disabled				Disabled		禁止					禁止
52		32			15			Enabled					Enabled			允許					允許
53		32			15			Voltage					Voltage			電壓方式				電壓方式
54		32			15			Time					Time			時間方式				時間方式
55		32			15			None					None			無					無
56		32			15			LVD 1					LVD 1			LVD1					LVD1
57		32			15			LVD 2					LVD 2			LVD2					LVD2
103		32			15			High Temp Disconnect 1			HTD 1			HTD1高溫下電允許			HTD1下電允許
104		32			15			High Temp Disconnect 2			HTD 2			HTD2高溫下電允許			HTD2下電允許
105		32			15			Battery LVD				Battery LVD		電池下電				電池下電
106		32			15			No Battery				No Battery		無電池					無電池
107		32			15			LVD 1					LVD 1			LVD1					LVD1
108		32			15			LVD 2					LVD 2			LVD2					LVD2
109		32			15			Battery Always On			Batt Always On		有電池但不下電				有電池但不下電
110		32			15			LVD Contactor Type			LVD Type		下電接觸器類型				接觸器類型
111		32			15			Bistable				Bistable		雙穩					雙穩
112		32			15			Mono-Stable				Mono-Stable		單穩					單穩
113		32			15			Mono w/Sample				Mono w/Sample		單穩帶回采				單穩帶回采
116		32			15			LVD 1 Disconnect			LVD1 Disconnect		LVD1下電				LVD1下電
117		32			15			LVD 2 Disconnect			LVD2 Disconnect		LVD2下電				LVD2下電
118		32			15			LVD 1 Mono w/Sample			LVD1 Mono Sampl		LVD1單穩帶回采				LVD1單穩帶回采
119		32			15			LVD 2 Mono w/Sample			LVD2 Mono Sampl		LVD2單穩帶回采				LVD2單穩帶回采
125		32			15			State					State			State					State
126		32			15			LVD 1 Voltage (24V)			LVD 1 Voltage		LVD1電壓(24V)				LVD1電壓
127		32			15			LVD 1 Reconnect Voltage (24V)		LVD1 Recon Volt		LVD1上電電壓(24V)			LVD1上電電壓
128		32			15			LVD 2 Voltage (24V)			LVD 2 Voltage		LVD2電壓(24V)				LVD2電壓
129		32			15			LVD 2 Reconnect Voltage (24V)		LVD2 Recon Volt		LVD2上電電壓(24V)			LVD2上電電壓
130		32			15			LVD 3					LVD 3			LVD 3					LVD 3
