/*==========================================================================*
*    Copyright(c) 2021, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : NSC3.0(Net Sure Controller)
*
*  FILENAME : web_user.c
*  CREATOR  : Zhao Zicheng              DATE: 2013-07-22
*  VERSION  : V1.00
*  PURPOSE  : This file is the main web user task codes.
*
*  HISTORY  :
*
*==========================================================================*/


#include <signal.h>
#include "stdsys.h"  


#include <netinet/in.h>			/* sockaddr_in, sockaddr */
#include <arpa/inet.h>			/* inet_ntoa */
//#include <ipv6.h>
//#include <in6.h>
#include <sys/resource.h>		/* setrlimit */
#include <sys/types.h>			/* socket, bind, accept */
#include <sys/socket.h>			/* socket, bind, accept, setsockopt, */
#include <netdb.h>
#include <net/route.h>			/*rtentry*/
#include <net/if.h>			/*ifreq */
#include <time.h>

#include <sys/dir.h> 
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "web_user.h"

#include "../eem_soc/esr.h"
#include "../ydn23/ydn.h"
#include "../modbus/modbus.h"
//#include "iconv.h"


#include "cgi_pub.h"
//#include "cgi_query_setting.h"
#include "../../mainapp/config_mgmt/cfg_mgmt.h"

#define SET_PARA_tar_FILE	"/var/download/SettingParam.tar"
#define RADIUS_SERVER_SETTINGS "/app/radius/radius_config/rserver_param.txt"
#define RADIUSCLIENT_CONFIG	 "/app/radius/radius_config/radiusclient.conf"
#define RADIUSCLENT_SERVER 	 "/app/radius/radius_config/servers"
char *strptime(const char *s, const char *format, struct tm *tm);
typedef struct tagWEB_GENERAL_RECT_LANG_INFO
{
	char szEngLang[32];
	char szLocLang[32];
}WEB_GENERAL_RECT_LANG_INFO;


typedef struct tagWebGeneralResourceInfo
{
	char		szResourceID[33];
	char		*szLocal;
	char		*szDefault;
}WEB_GENERAL_RESOURCE_INFO;

typedef struct tagWebTotalResourceInfo
{
	int			iNumber;
	char		szFileName[64];
	WEB_GENERAL_RESOURCE_INFO	*stWebPrivate;
}WEB_PRIVATE_RESOURCE_INFO;

typedef struct stWEB_VERSION_INFO
{
	float	fVersion;
	char	szLanguage[32];
}WEB_VERSION_INFO;

#define	ERR_WEB_OK		0
#define CONFIG_FILE_WEB_PRIVATE		"Web_ResourceUser.res"
#define	WEB_LOCAL_LANGUAGE_VERSION	"[LOCAL_LANGUAGE_VERSION]"
#define	WEB_LOCAL_LANGUAGE		"[LOCAL_LANGUAGE]"
//changed by Frank Wu,15/17/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
//#define WEB_MAX_HTML_PAGE_NUM	45
//changed by Frank Wu,24/15/30,20140527, for add single converter and single solar settings pages
//#define WEB_MAX_HTML_PAGE_NUM	47
//changed by Frank Wu,23/N/35,20140527, for adding the the web setting tab page 'DI'
//#define WEB_MAX_HTML_PAGE_NUM	52
//changed by Frank Wu,5/N/14,20140527, for system log
//#define WEB_MAX_HTML_PAGE_NUM	53
//changed by Frank Wu,11/N/27,20140527, for power split
//#define WEB_MAX_HTML_PAGE_NUM	54
//changed by Frank Wu,N/N/N,20140613, for two tabs
//#define WEB_MAX_HTML_PAGE_NUM	58
//#define WEB_MAX_HTML_PAGE_NUM	63
//changed by John,N/N/N,20171113, for T2S&Inverter  tabs
#define WEB_MAX_HTML_PAGE_NUM	72

#define CONFIG_FILE_WEB_PRIVATE_R	"private/web_user/cfg_ui_function.cfg"
#define WEB_DEFAULT_DIR		"/app/www_user/html/eng"		/*save english language html file*/
#define WEB_LOCAL_DIR		"/app/www_user/html/loc"		/*save local language html file*/
#define WEB_TEMPLATE_DIR	"/app/www_user/html/template"			/*store template file*/
#define WEB_PAGES_VERSION_FILE		"/app/config/private/web_user/cfg_ui_function.cfg"

//for RADIUS setting
#define PRIMARY_SEL	1
#define SECONDARY_SEL 2
#define BUFFER_SIZE 1000
//////////////////////////////////////////////////////////////////////////
//changed by Frank Wu,12/N/27,20140527, for power split
//Added by wj for PLC Config 2006.5.15

//for PLC Config
#define MAX_TIME_WAITING_LOAD_PLC_CONFIG 1000
#define MAX_BUFFER  300000
#define MAX_ONE_EQUIP_LEN			(100*1024)	//100K Bytes
#define MAX_SPRINTF_LINE_LEN		256
//////////////////////////////////////////////////////////////////////////

//static int	iVarSubID = 0;
//static int	iBufLen = 0;
//static int	iTimeOut = 0;

//int	iPagesNumber = 0;
int	StartFlage = 0;
int	iRollFlage = 0;
int	iAutoCFGStartFlage = 0;
int	iSlaveMode = 0;
HANDLE g_hMutexLoadCurrent = NULL;
HANDLE g_hMutexSetDataFile = NULL;

WEB_PRIVATE_RESOURCE_INFO	*pWebCfgInfo = NULL;
WEB_VERSION_INFO	stWebCompareVersion;
//WEB_STATUS_CONFIG	*stStatusConfig;

static const char szTemplateFile[][64] = {"login.html:Number","login.html",
"index.html:Number","index.html",
"tmp.system_rectifier.html:Number","tmp.system_rectifier.html",
"tmp.system_battery.html:Number","tmp.system_battery.html",
"tmp.system_dc.html:Number","tmp.system_dc.html",
"tmp.history_alarmlog.html:Number","tmp.history_alarmlog.html",
"tmp.history_testlog.html:Number","tmp.history_testlog.html",
"tmp.history_eventlog.html:Number","tmp.history_eventlog.html",
"tmp.history_datalog.html:Number","tmp.history_datalog.html",
"tmp.setting_charge.html:Number","tmp.setting_charge.html",
"tmp.index.html:Number","tmp.index.html",
"tmp.hybrid.html:Number","tmp.hybrid.html",
"tmp.system_converter.html:Number","tmp.system_converter.html",
"tmp.system_solar.html:Number","tmp.system_solar.html",
"tmp.setting_eco.html:Number","tmp.setting_eco.html",
"tmp.setting_lvd.html:Number","tmp.setting_lvd.html",
"tmp.setting_rectifiers.html:Number","tmp.setting_rectifiers.html",
"tmp.setting_batteryTest.html:Number","tmp.setting_batteryTest.html",
"tmp.install_wizard.html:Number","tmp.install_wizard.html",
"tmp.setting_user.html:Number","tmp.setting_user.html",
"tmp.setting_ipv4.html:Number","tmp.setting_ipv4.html",
//"tmp.setting_site_info.html:Number","tmp.setting_site_info.html",
"tmp.setting_time_sync.html:Number","tmp.setting_time_sync.html",
//"tmp.setting_auto_configuration.html:Number","tmp.setting_auto_configuration.html",
"tmp.system_inventory.html:Number","tmp.system_inventory.html",
"forgot_password.html:Number","forgot_password.html",
"tmp.setting_other.html:Number","tmp.setting_other.html",
"tmp.setting_hlms_configuration.html:Number","tmp.setting_hlms_configuration.html",
"tmp.system_rectifierS1.html:Number","tmp.system_rectifierS1.html",
"tmp.setting_temp.html:Number","tmp.setting_temp.html",
"tmp.setting_hybrid.html:Number","tmp.setting_hybrid.html",
"tmp.setting_nms.html:Number","tmp.setting_nms.html",
"tmp.setting_system_rect.html:Number","tmp.setting_system_rect.html",
"tmp.system_dg.html:Number","tmp.system_dg.html",
"tmp.system_ac.html:Number","tmp.system_ac.html",
"tmp.system_smio.html:Number","tmp.system_smio.html",
"tmp.setting_alarm_equipment.html:Number","tmp.setting_alarm_equipment.html",
"tmp.setting_alarm_content.html:Number","tmp.setting_alarm_content.html",
"tmp.setting_converter.html:Number","tmp.setting_converter.html",
"tmp.setting_clear_data.html:Number","tmp.setting_clear_data.html",
"tmp.setting_upload_download.html:Number","tmp.setting_upload_download.html",
"tmp.setting_power_system.html:Number","tmp.setting_power_system.html",
"tmp.setting_language.html:Number","tmp.setting_language.html",
"tmp.system_udef.html:Number","tmp.system_udef.html",
"tmp.udef_setting.html:Number","tmp.udef_setting.html",
"tmp.udef_setting_2.html:Number","tmp.udef_setting_2.html",
"tmp.udef_setting_3.html:Number","tmp.udef_setting_3.html"
//changed by Frank Wu,16/18/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
 , WEB_PAGES_SETTING_MPPT_STR_NUM, WEB_PAGES_SETTING_MPPT_STR
 , WEB_PAGES_SETTING_SHUNT_STR_NUM, WEB_PAGES_SETTING_SHUNT_STR
 //changed by Frank Wu,25/16/30,20140527, for add single converter and single solar settings pages
 ,WEB_PAGES_SETTING_SINGLE_CONVERTER_STR_NUM, WEB_PAGES_SETTING_SINGLE_CONVERTER_STR
 ,WEB_PAGES_SETTING_SINGLE_SOLAR_STR_NUM, WEB_PAGES_SETTING_SINGLE_SOLAR_STR,
 "tmp.cabinet_branch.html:Number","tmp.cabinet_branch.html",
 "tmp.cabinet_map.html:Number","tmp.cabinet_map.html",
 "tmp.cabinet_set.html:Number","tmp.cabinet_set.html"
 //changed by Frank Wu,24/N/35,20140527, for adding the the web setting tab page 'DI'
 ,WEB_PAGES_SETTING_DI_STR_NUM, WEB_PAGES_SETTING_DI_STR
 //changed by Frank Wu,6/N/14,20140527, for system log
 ,WEB_PAGES_SETTING_SYSTEM_LOG_STR_NUM, WEB_PAGES_SETTING_SYSTEM_LOG_STR
 //changed by Frank Wu,13/N/27,20140527, for power split
 ,WEB_PAGES_SETTING_POWER_SPLIT_STR_NUM, WEB_PAGES_SETTING_POWER_SPLIT_STR,
 "tmp.batt_setting.html:Number","tmp.batt_setting.html",
 "tmp.system_lvd_fuse.html:Number","tmp.system_lvd_fuse.html",
 "tmp.system_battery_tabs.html:Number","tmp.system_battery_tabs.html"
 //changed by Frank Wu,N/N/N,20140613, for two tabs
 ,WEB_PAGES_SETTING_TABS_STR_NUM, WEB_PAGES_SETTING_TABS_STR
 ,WEB_PAGES_SETTING_TABS_A_STR_NUM, WEB_PAGES_SETTING_TABS_A_STR,
    "tmp.consumption_map.html:Number","tmp.consumption_map.html",
    "tmp.system_dc_smdup.html:Number","tmp.system_dc_smdup.html",
    "tmp.adv_setting_tabs.html:Number","tmp.adv_setting_tabs.html",
    "tmp.system_T2S.html:Number","tmp.system_T2S.html",
	"tmp.system_inverter_output.html:Number","tmp.system_inverter_output.html",
	"tmp.efficiency_tracker.html:Number","tmp.efficiency_tracker.html",
	"tmp.shunts_data_content.html:Number","tmp.shunts_data_content.html",
	"tmp.setting_shunts_content.html:Number","tmp.setting_shunts_content.html",
	//changed by Stone Song ,20160525, for adding the the web setting tab page 'FUSE'
	WEB_PAGES_SETTING_FUSE_STR_NEW_NUM, WEB_PAGES_SETTING_FUSE_STR_NEW,
	WEB_PAGES_SETTING_STR_INVT_NUM, WEB_PAGES_SETTING_STR_INVT,
	WEB_PAGES_SYSTEM_STR_INVT_NUM,WEB_PAGES_SYSTEM_STR_INVT,
	WEB_PAGES_SETTING_SWSWITCH_STR_NEW_NUM, WEB_PAGES_SETTING_SWSWITCH_STR_NEW,
};

const char szWebLangType[LANGUAGE_NUM][8] = {"de","es","fr","it","ru","pt","tw","zh","tr"};

static const char szUserInfo[4][32] = {"User","HLMS","NMS","Internal Service"};

static char szClearName[][64]= {HIST_ALARM_LOG,
HIS_DATA_LOG,
CTRL_CMD_LOG,
BATT_TEST_LOG_FILE,
DSL_TEST_LOG
};

//changed by Frank Wu,7/N/14,20140527, for system log
/*for querying log*/
//#define BASIC_LOG_FILE			"ACU.log"
//#define BASIC_BAK_LOG_FILE		"ACU.bak.log"
//#define SUB_DIR_LOG_FILE		"log"

//#ifndef ENV_ACU_ROOT_DIR
//#define ENV_ACU_ROOT_DIR		"ACU_ROOT_DIR"	// to use shell env variable.
//#endif
//static char APP_LOG_FILE[MAX_FILE_PATH] = BASIC_LOG_FILE;


//for setting param
static int Web_MakeGetSetParamWebPage(int readable_param);
static int Web_GetSetParamNum(void);

static char *CreateLangResourceFileName(const char *szFileName, const char *szLangCode);

//changed by Frank Wu,14/N/27,20140527, for power split
//for GC Config
static HANDLE s_hMutexLoadGCCFGFile = NULL;
#define WEB_GC_CFG_FILE_PATH "/app/config/private/gen_ctl/gen_ctl.cfg"
#define GC_PS_MODE     "[POWER_SPLIT_MODE]"
#define GC_PS_INFO      "[POWER_SPLIT_INPUT]"

//for GC Config
HANDLE GetMutexLoadGCConfig(void);
static int Web_MakeGCWebPage(OUT char **ppszReturn, IN int iLanguage);
static int Web_LoadGCConfigFile(char **ppszReturn);
//static int Web_LoadGCPSMode(char **ppszReturn);
static int ParseGCTableProc(IN char *szBuf, OUT WEB_GC_CFG_INFO_LINE *pStructData);
static int LoadGCConfigFile(IN void *pCfg, OUT void *pLoadToBuf);
static int Web_GetGCPSMode(OUT char **ppszGCPSMode);
static int Web_ModifyGCPSMode(IN char *pszMode);
static int Web_ModifyGCPSInfo(IN char *pszGCPSInfo);
static int Radius_Web_GetAuthorityByUser(char *szUser,char *szPasswordRadius);
static int SetRadiusInfo(char *ptr);
static int SetRadiusConfig(int IpSelection, char *serverIP, char *Port, char *skey);
static int UpdateRadiusConfigFile(int lineNo, char *newline, char *filename);

/*==========================================================================*
* FUNCTION :    ParsedWebLangFileTableProc
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   IN char *szBuf:
OUT WEB_GENERAL_RESOURCE_INFO *pStructData:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
*==========================================================================*/
static int ParsedWebLangFileTableProc(IN char *szBuf, 
				      OUT WEB_GENERAL_RESOURCE_INFO *pStructData)
{
	char *pField = NULL;
	int		iMaxLen = 0;
	/* used as buffer */

	ASSERT(szBuf);
	ASSERT(pStructData);

	/*0.Jump sequence ID*/
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if(pField != NULL)
	{
		//DELETE(pField);
		//pField = NULL;
	}

	/* 1.RES ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField == NULL)
	{
		return 1;    
	}

	////TRACE("pStructData->szResourceID : %s\n", pField);
	strncpyz(pStructData->szResourceID, pField, sizeof(pStructData->szResourceID));

	/*2.iMaxFullLen*/
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField == NULL)
	{
		return 1;    
	}
	////TRACE("iMaxLen : %s\n", pField);
	iMaxLen = atoi(pField);

	/* 4.English full language */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField == NULL)
	{
		return 2;    
	}
	////TRACE("pStructData->szDefault : %s\n", pField);
	pStructData->szDefault = NEW(char, iMaxLen + 1);
	//TRACE_WEB_USER("pStructData->szDefault : %s\n", pField);
	strncpyz(pStructData->szDefault, pField, iMaxLen + 1);

	/* 6.local full language */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField == NULL)
	{
		return 3;    
	}
	pStructData->szLocal = NEW(char, iMaxLen + 1);
	strncpyz(pStructData->szLocal, pField, iMaxLen + 1);

	return ERR_WEB_OK;
}

/*==========================================================================*
* FUNCTION :  WEB_ReadConfig
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
*==========================================================================*/
static char *CreateLangResourceFileName(const char *szFileName, const char *szLangCode)
{
	static char szLRFileName[256]; /* the returned lang resource file name */
	const char *p;   
	size_t iLen;

	if (szFileName == NULL || szLangCode == NULL)
	{
		return NULL;
	}

	/* init */
	szLRFileName[0] = '\0';

	/* get the head: config/lang/xx/ */
	strcpy(szLRFileName, "lang/");
	strcat(szLRFileName, szLangCode);
	strcat(szLRFileName, "/");

	/* get the name: stdxx_xx.res */
	p = szFileName;
	iLen = 0;
	while (*p != '\0' && *p != '.')
	{
		p++;
		iLen++;
	}

	if (*p == '\0')				/* lack of '.', invalid format */
	{
		return NULL;
	}

	strncat(szLRFileName, szFileName, iLen);
	strcat(szLRFileName, "_");
	strcat(szLRFileName, szLangCode);
	strcat(szLRFileName, ".res");

	return szLRFileName;
}

/*==========================================================================*
* FUNCTION :    LoadWebPrivateConfigProc
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   IN int iPages:
IN void *pCfg:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
* MODIFY:    Zhao Zicheng	       DATE: 2013-7-22 ----Load the web user webpage languages resources <Web_ResourceUser_**.res>
*==========================================================================*/
static int LoadWebPrivateConfigProc(IN void *pCfg)
{
	//changed by Frank Wu,19/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
#define PRIVATE_CONFIG_PROC_DEF_ITEM(iArgSeqId, iArgFieldStr)		\
	{	\
		DEF_LOADER_ITEM(&loader[iArgSeqId],				\
			NULL,	\
			&(pWebCfgInfo[iArgSeqId].iNumber),			\
			iArgFieldStr,	\
			&(pWebCfgInfo[iArgSeqId].stWebPrivate),	\
			ParsedWebLangFileTableProc);	\
	}



	//UNUSED(iPages); //iPages = 11;
	int			i = 0, k = 0;
	CONFIG_TABLE_LOADER loader[WEB_MAX_HTML_PAGE_NUM];
	static WEB_PRIVATE_RESOURCE_INFO	s_aWebCfgInfo[WEB_MAX_HTML_PAGE_NUM];

	pWebCfgInfo = s_aWebCfgInfo;

	k = 1;
	for(i = 1; i < WEB_MAX_HTML_PAGE_NUM + 1; i++)
	{
		strncpyz(pWebCfgInfo[i-1].szFileName,szTemplateFile[k] , sizeof(pWebCfgInfo[i-1].szFileName));
		k = k + 2;
	}

	DEF_LOADER_ITEM(&loader[0],
		NULL,
		&(pWebCfgInfo[0].iNumber), 
		WEB_PAGES_LOGIN,
		&(pWebCfgInfo[0].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[1],
		NULL,
		&(pWebCfgInfo[1].iNumber), 
		WEB_PAGES_HOMEPAGE,
		&(pWebCfgInfo[1].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[2],
		NULL,
		&(pWebCfgInfo[2].iNumber), 
		WEB_PAGES_RECT,
		&(pWebCfgInfo[2].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[3],
		NULL,
		&(pWebCfgInfo[3].iNumber), 
		WEB_PAGES_BATT,
		&(pWebCfgInfo[3].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[4],
		NULL,
		&(pWebCfgInfo[4].iNumber), 
		WEB_PAGES_DC,
		&(pWebCfgInfo[4].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[5],
		NULL,
		&(pWebCfgInfo[5].iNumber), 
		WEB_PAGES_HISALARM,
		&(pWebCfgInfo[5].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[6],
		NULL,
		&(pWebCfgInfo[6].iNumber), 
		WEB_PAGES_BATTLOG,
		&(pWebCfgInfo[6].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[7],
		NULL,
		&(pWebCfgInfo[7].iNumber), 
		WEB_PAGES_HISEVENT,
		&(pWebCfgInfo[7].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[8],
		NULL,
		&(pWebCfgInfo[8].iNumber), 
		WEB_PAGES_HISDATA,
		&(pWebCfgInfo[8].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[9],
		NULL,
		&(pWebCfgInfo[9].iNumber), 
		WEB_PAGES_SETTING_CHARGE,
		&(pWebCfgInfo[9].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[10],
		NULL,
		&(pWebCfgInfo[10].iNumber), 
		WEB_PAGES_TMP_INDEX,
		&(pWebCfgInfo[10].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[11],
		NULL,
		&(pWebCfgInfo[11].iNumber), 
		WEB_PAGES_TMP_HYBRID,
		&(pWebCfgInfo[11].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[12],
		NULL,
		&(pWebCfgInfo[12].iNumber), 
		WEB_PAGES_CONVERTER,
		&(pWebCfgInfo[12].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[13],
		NULL,
		&(pWebCfgInfo[13].iNumber), 
		WEB_PAGES_SOLAR,
		&(pWebCfgInfo[13].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[14],
		NULL,
		&(pWebCfgInfo[14].iNumber), 
		WEB_PAGES_SETTING_ECO,
		&(pWebCfgInfo[14].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[15],
		NULL,
		&(pWebCfgInfo[15].iNumber), 
		WEB_PAGES_SETTING_LVD,
		&(pWebCfgInfo[15].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[16],
		NULL,
		&(pWebCfgInfo[16].iNumber), 
		WEB_PAGES_SETTING_RECT,
		&(pWebCfgInfo[16].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[17],
		NULL,
		&(pWebCfgInfo[17].iNumber), 
		WEB_PAGES_SETTING_BATT_TEST,
		&(pWebCfgInfo[17].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[18],
		NULL,
		&(pWebCfgInfo[18].iNumber), 
		WEB_PAGES_TMP_WIZARD,
		&(pWebCfgInfo[18].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[19],
		NULL,
		&(pWebCfgInfo[19].iNumber), 
		WEB_PAGES_TMP_USER,
		&(pWebCfgInfo[19].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[20],
		NULL,
		&(pWebCfgInfo[20].iNumber), 
		WEB_PAGES_TMP_IPV4,
		&(pWebCfgInfo[20].stWebPrivate), 
		ParsedWebLangFileTableProc);

	/*DEF_LOADER_ITEM(&loader[21],
		NULL,
		&(pWebCfgInfo[21].iNumber), 
		WEB_PAGES_TMP_SITE,
		&(pWebCfgInfo[21].stWebPrivate), 
		ParsedWebLangFileTableProc);*/

	DEF_LOADER_ITEM(&loader[21],
		NULL,
		&(pWebCfgInfo[21].iNumber), 
		WEB_PAGES_TMP_TIME,
		&(pWebCfgInfo[21].stWebPrivate), 
		ParsedWebLangFileTableProc);

	/*DEF_LOADER_ITEM(&loader[23],
		NULL,
		&(pWebCfgInfo[23].iNumber), 
		WEB_PAGES_TMP_CONF,
		&(pWebCfgInfo[23].stWebPrivate), 
		ParsedWebLangFileTableProc);*/

	DEF_LOADER_ITEM(&loader[22],
		NULL,
		&(pWebCfgInfo[22].iNumber), 
		WEB_PAGES_INVENTORY,
		&(pWebCfgInfo[22].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[23],
		NULL,
		&(pWebCfgInfo[23].iNumber), 
		WEB_PAGES_FIND_PASSWD,
		&(pWebCfgInfo[23].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[24],
		NULL,
		&(pWebCfgInfo[24].iNumber), 
		WEB_PAGES_SET_OTHER,
		&(pWebCfgInfo[24].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[25],
		NULL,
		&(pWebCfgInfo[25].iNumber), 
		WEB_PAGES_HLMS_CONFIG,
		&(pWebCfgInfo[25].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[26],
	    NULL,
	    &(pWebCfgInfo[26].iNumber), 
	    WEB_PAGES_RECTS1,
	    &(pWebCfgInfo[26].stWebPrivate),
	    ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[27],
	    NULL,
	    &(pWebCfgInfo[27].iNumber), 
	    WEB_PAGES_SETTING_TEMP,
	    &(pWebCfgInfo[27].stWebPrivate), 
	    ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[28],
	    NULL,
	    &(pWebCfgInfo[28].iNumber), 
	    WEB_PAGES_SETTING_HYBRID,
	    &(pWebCfgInfo[28].stWebPrivate), 
	    ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[29],
	    NULL,
	    &(pWebCfgInfo[29].iNumber), 
	    WEB_PAGES_NMS_CONFIG,
	    &(pWebCfgInfo[29].stWebPrivate), 
	    ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[30],
	    NULL,
	    &(pWebCfgInfo[30].iNumber), 
	    WEB_PAGES_RECT_SET,
	    &(pWebCfgInfo[30].stWebPrivate), 
	    ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[31],
	    NULL,
	    &(pWebCfgInfo[31].iNumber), 
	    WEB_PAGES_DG,
	    &(pWebCfgInfo[31].stWebPrivate), 
	    ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[32],
	    NULL,
	    &(pWebCfgInfo[32].iNumber), 
	    WEB_PAGES_AC,
	    &(pWebCfgInfo[32].stWebPrivate), 
	    ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[33],
	    NULL,
	    &(pWebCfgInfo[33].iNumber), 
	    WEB_PAGES_SMIO,
	    &(pWebCfgInfo[33].stWebPrivate), 
	    ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[34],
	    NULL,
	    &(pWebCfgInfo[34].iNumber), 
	    WEB_PAGES_ALARM_EQUIP,
	    &(pWebCfgInfo[34].stWebPrivate), 
	    ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[35],
	    NULL,
	    &(pWebCfgInfo[35].iNumber), 
	    WEB_PAGES_ALARM_CONT,
	    &(pWebCfgInfo[35].stWebPrivate), 
	    ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[36],
	    NULL,
	    &(pWebCfgInfo[36].iNumber), 
	    WEB_PAGES_SETTING_CONV,
	    &(pWebCfgInfo[36].stWebPrivate), 
	    ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[37],
	    NULL,
	    &(pWebCfgInfo[37].iNumber), 
	    WEB_PAGES_CLEAR_DATA,
	    &(pWebCfgInfo[37].stWebPrivate), 
	    ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[38],
	    NULL,
	    &(pWebCfgInfo[38].iNumber), 
	    WEB_PAGES_DOWNLOAD,
	    &(pWebCfgInfo[38].stWebPrivate), 
	    ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[39],
	    NULL,
	    &(pWebCfgInfo[39].iNumber), 
	    WEB_PAGES_SYS_SET,
	    &(pWebCfgInfo[39].stWebPrivate), 
	    ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[40],
	    NULL,
	    &(pWebCfgInfo[40].iNumber), 
	    WEB_PAGES_LANG_SET,
	    &(pWebCfgInfo[40].stWebPrivate), 
	    ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[41],
	    NULL,
	    &(pWebCfgInfo[41].iNumber), 
	    WEB_PAGES_USER_DEF,
	    &(pWebCfgInfo[41].stWebPrivate), 
	    ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[42],
	    NULL,
	    &(pWebCfgInfo[42].iNumber), 
	    WEB_PAGES_USER_SET1,
	    &(pWebCfgInfo[42].stWebPrivate), 
	    ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[43],
	    NULL,
	    &(pWebCfgInfo[43].iNumber), 
	    WEB_PAGES_USER_SET2,
	    &(pWebCfgInfo[43].stWebPrivate), 
	    ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[44],
	    NULL,
	    &(pWebCfgInfo[44].iNumber), 
	    WEB_PAGES_USER_SET3,
	    &(pWebCfgInfo[44].stWebPrivate), 
	    ParsedWebLangFileTableProc);
		
	//changed by Frank Wu,17/20/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
	PRIVATE_CONFIG_PROC_DEF_ITEM( 45, WEB_PAGES_SETTING_MPPT);
	PRIVATE_CONFIG_PROC_DEF_ITEM( 46, WEB_PAGES_SETTING_SHUNT);		
	//changed by Frank Wu,26/17/30,20140527, for add single converter and single solar settings pages
	PRIVATE_CONFIG_PROC_DEF_ITEM( 47, WEB_PAGES_SETTING_SINGLE_CONVERTER);
	PRIVATE_CONFIG_PROC_DEF_ITEM( 48, WEB_PAGES_SETTING_SINGLE_SOLAR);

	DEF_LOADER_ITEM(&loader[49],
	    NULL,
	    &(pWebCfgInfo[49].iNumber), 
	    WEB_PAGES_CAB_BRANCH,
	    &(pWebCfgInfo[49].stWebPrivate), 
	    ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[50],
	    NULL,
	    &(pWebCfgInfo[50].iNumber), 
	    WEB_PAGES_CAB_MAP,
	    &(pWebCfgInfo[50].stWebPrivate), 
	    ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[51],
	    NULL,
	    &(pWebCfgInfo[51].iNumber), 
	    WEB_PAGES_CAB_SET,
	    &(pWebCfgInfo[51].stWebPrivate), 
	    ParsedWebLangFileTableProc);
			
	//changed by Frank Wu,25/N/35,20140527, for adding the the web setting tab page 'DI'
	int len = 52;
	PRIVATE_CONFIG_PROC_DEF_ITEM( len, WEB_PAGES_SETTING_DI);
	len++;
	//changed by Frank Wu,8/N/14,20140527, for system log
	PRIVATE_CONFIG_PROC_DEF_ITEM( len, WEB_PAGES_SETTING_SYSTEM_LOG);
	len++;
	//changed by Frank Wu,15/N/27,20140527, for power split
	PRIVATE_CONFIG_PROC_DEF_ITEM( len, WEB_PAGES_SETTING_POWER_SPLIT);
	len++;

	DEF_LOADER_ITEM(&loader[55],
	    NULL,
	    &(pWebCfgInfo[55].iNumber), 
	    WEB_PAGES_BATT_SET,
	    &(pWebCfgInfo[55].stWebPrivate), 
	    ParsedWebLangFileTableProc);
	len++;

	DEF_LOADER_ITEM(&loader[56],
	    NULL,
	    &(pWebCfgInfo[56].iNumber), 
	    WEB_PAGES_LVD_FUSE,
	    &(pWebCfgInfo[56].stWebPrivate), 
	    ParsedWebLangFileTableProc);
	len++;

	DEF_LOADER_ITEM(&loader[57],
	    NULL,
	    &(pWebCfgInfo[57].iNumber), 
	    WEB_PAGES_BATT_TAB,
	    &(pWebCfgInfo[57].stWebPrivate), 
	    ParsedWebLangFileTableProc);
	len++;

	//changed by Frank Wu,N/N/N,20140613, for two tabs
	PRIVATE_CONFIG_PROC_DEF_ITEM( len, WEB_PAGES_SETTING_TABS);
	len++;
	PRIVATE_CONFIG_PROC_DEF_ITEM( len, WEB_PAGES_SETTING_TABS_A);
	len++;

	DEF_LOADER_ITEM(&loader[60],
	    NULL,
	    &(pWebCfgInfo[60].iNumber), 
	    WEB_PAGES_CONSUM_MAP,
	    &(pWebCfgInfo[60].stWebPrivate), 
	    ParsedWebLangFileTableProc);
	len++;

	DEF_LOADER_ITEM(&loader[61],
	    NULL,
	    &(pWebCfgInfo[61].iNumber), 
	    WEB_PAGES_DC_SMDUP,
	    &(pWebCfgInfo[61].stWebPrivate), 
	    ParsedWebLangFileTableProc);
	len++;

	DEF_LOADER_ITEM(&loader[62],
	    NULL,
	    &(pWebCfgInfo[62].iNumber), 
	    WEB_PAGES_ADV_SET_TAB,
	    &(pWebCfgInfo[62].stWebPrivate), 
	    ParsedWebLangFileTableProc);
	len++;

	DEF_LOADER_ITEM(&loader[63],
	    NULL,
	    &(pWebCfgInfo[63].iNumber), 
	    WEB_PAGES_T2S_TAB,
	    &(pWebCfgInfo[63].stWebPrivate), 
	    ParsedWebLangFileTableProc);
	len++;

	
	DEF_LOADER_ITEM(&loader[64],
	    NULL,
	    &(pWebCfgInfo[64].iNumber), 
	    WEB_PAGES_INVERTER_OUTPUT,
	    &(pWebCfgInfo[64].stWebPrivate), 
	    ParsedWebLangFileTableProc);
	len++;

	PRIVATE_CONFIG_PROC_DEF_ITEM( len, WEB_PAGES_ET_TAB);
	//DEF_LOADER_ITEM(&loader[64],
	//    NULL,
	//    &(pWebCfgInfo[64].iNumber), 
	//    WEB_PAGES_ET_TAB,
	//    &(pWebCfgInfo[64].stWebPrivate), 
	//    ParsedWebLangFileTableProc);
	len++;

	PRIVATE_CONFIG_PROC_DEF_ITEM( len, WEB_PAGES_SHUNT_PAGE_TAB);
	len++;
	PRIVATE_CONFIG_PROC_DEF_ITEM( len, WEB_PAGES_SET_SHUNT_PAGE_TAB);
	len++;
	PRIVATE_CONFIG_PROC_DEF_ITEM( len, WEB_PAGES_SET_FUSE_PAGE_TAB);
	len++;

	PRIVATE_CONFIG_PROC_DEF_ITEM( len, WEB_PAGES_SETTING_INVT);
	len++;
	PRIVATE_CONFIG_PROC_DEF_ITEM( len, WEB_PAGES_INVERTER);
	len++;
	
	PRIVATE_CONFIG_PROC_DEF_ITEM( len, WEB_PAGES_SETTING_SWSWITCH_NEW);
	len++;
	
	//if (Cfg_LoadTables(pCfg, WEB_MAX_HTML_PAGE_NUM, loader) != ERR_CFG_OK)
	if (Cfg_LoadTables(pCfg, len, loader) != ERR_CFG_OK)
	{
		return ERR_LCD_FAILURE;
	}

	return ERR_CFG_OK;
}


/*==========================================================================*
* FUNCTION :  WEB_ReadConfig
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
*==========================================================================*/
static int WEB_ReadConfig(void)
{
	int		iRst;
	void	*pProf = NULL;  
	FILE	*pFile = NULL;
	char	*szInFile = NULL;
	size_t	ulFileLen;
	char	*pLangCode = NULL;
	int	iBufLen = 0;

	char szCfgFileName[MAX_FILE_PATH];

	DxiGetData(VAR_ACU_PUBLIC_CONFIG,
		LOCAL_LANGUAGE_CODE, 
		0, 
		&iBufLen,
		&(pLangCode),
		0);

	TRACE_WEB_USER_NOT_CYCLE("Now the language is %s\n", pLangCode);

	Cfg_GetFullConfigPath(CreateLangResourceFileName(CONFIG_FILE_WEB_PRIVATE, pLangCode),
		szCfgFileName, 
		MAX_FILE_PATH);

	pFile = fopen(szCfgFileName, "r");
	if (pFile == NULL)
	{
		TRACE("Failure to open %s",szCfgFileName);
		return FALSE;
	}

	ulFileLen = GetFileLength(pFile);

	szInFile = NEW(char, ulFileLen + 1);
	if (szInFile == NULL)
	{
		fclose(pFile);
		return FALSE;
	}

	ulFileLen = fread(szInFile, sizeof(char), ulFileLen, pFile);
	fclose(pFile);

	if (ulFileLen == 0) 
	{
		/* clear the memory */
		DELETE(szInFile);

		return FALSE;
	}
	szInFile[ulFileLen] = '\0';  /* end with NULL */

	/* create SProfile */
	pProf = Cfg_ProfileOpen(szInFile, (int)ulFileLen);

	if (pProf == NULL)
	{
		DELETE(szInFile);
		return FALSE;
	}

	//1.Read Number of pages
	/*iRst = Cfg_ProfileGetInt(pProf,
		WEB_PAGES_NUMBER, 
		&iPagesNumber); 
	TRACE("Successfully to  Cfg_ProfileGetInt  %d\n", iPagesNumber);*/
	/*if (iRst != 1)
	{
		AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
			"There are no pages number in Resource.cfg.");
	}*/

	//2. Get local language
	char	szVersion[32];
	iRst = Cfg_ProfileGetString(pProf,
		WEB_LOCAL_LANGUAGE_VERSION,
		szVersion, 
		sizeof(szVersion)); 
	stWebCompareVersion.fVersion = atof(szVersion);

	//3. Get local language version
	iRst = Cfg_ProfileGetString(pProf,
		WEB_LOCAL_LANGUAGE, 
		stWebCompareVersion.szLanguage,
		sizeof(stWebCompareVersion.szLanguage));

	iRst = LoadWebPrivateConfigProc(pProf);

	if (iRst != ERR_WEB_OK)
	{
		AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
			"Fail to get resource table.");

		DELETE(pProf);
		DELETE(szInFile);
		return iRst;
	}

	DELETE(pProf); 
	DELETE(szInFile);
	return ERR_WEB_OK;
}

/*==========================================================================*
* FUNCTION :  Web_GetVersionOfWebPages_r
* PURPOSE  :  Get the web page version
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  stWebVersionInfo_R: The struct saved the web page information
* RETURN   :  int: success or not
* COMMENTS :  This function is from <communicate.c>
* CREATOR  :  Unknown               DATE: 2013-07-20
*==========================================================================*/
static int Web_GetVersionOfWebPages_r(WEB_VERSION_INFO  *stWebVersionInfo_R)
{
	int		iRst;
	void	*pProf = NULL;  
	FILE	*pFile = NULL;
	char	*szInFile = NULL;
	size_t	ulFileLen;

	char szCfgFileName[MAX_FILE_PATH]; 

	Cfg_GetFullConfigPath(CONFIG_FILE_WEB_PRIVATE_R, szCfgFileName, MAX_FILE_PATH);

	pFile = fopen(szCfgFileName, "r");
	if (pFile == NULL)
	{	
		return FALSE;
	}

	ulFileLen = GetFileLength(pFile);

	szInFile = NEW(char, ulFileLen + 1);
	if (szInFile == NULL)
	{
		fclose(pFile);
		return FALSE;
	}

	/* read file */
	ulFileLen = fread(szInFile, sizeof(char), ulFileLen, pFile);
	fclose(pFile);

	if (ulFileLen == 0) 
	{
		/* clear the memory */
		DELETE(szInFile);
		return FALSE;
	}
	szInFile[ulFileLen] = '\0';  /* end with NULL */

	/* create SProfile */
	pProf = Cfg_ProfileOpen(szInFile, (int)ulFileLen);

	if (pProf == NULL)
	{
		DELETE(szInFile);
		return FALSE;
	}

	char	szVersion[32];
	//0.Read Local language version
	iRst = Cfg_ProfileGetString(pProf,
	    WEB_LOCAL_LANGUAGE_VERSION, 
	    szVersion,
	    sizeof(szVersion)); 
	stWebVersionInfo_R->fVersion = atof(szVersion);

	//TRACE("Successfully to  Cfg_ProfileGetInt  %d\n", iPagesNumber);
	if (iRst != 1)
	{
	    TRACE_WEB_USER_NOT_CYCLE("Can't find the [LOCAL_LANGUAGE_VERSION]! %d\n", iRst);
	    AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
		"There are no pages number in Resource.cfg.");
	}

	//1.Read Local language version
	iRst = Cfg_ProfileGetString(pProf,
	    WEB_LOCAL_LANGUAGE, 
	    stWebVersionInfo_R->szLanguage,
	    sizeof(stWebVersionInfo_R->szLanguage)); 
	//TRACE("Successfully to  Cfg_ProfileGetInt  %d\n", iPagesNumber);
	if (iRst != 1)
	{
	    TRACE_WEB_USER_NOT_CYCLE("Can't find the [LOCAL_LANGUAGE]! %d\n", iRst);
	    AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
		"There are no pages number in Resource.cfg.");
	}

	DELETE(pProf); 
	DELETE(szInFile);
	return ERR_WEB_OK;
}

/*==========================================================================*
* FUNCTION :  MakeFilePath
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN char *szFileName:
IN int iLanguage:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
*==========================================================================*/
static char *MakeFilePath(IN char *szFileName, IN int iLanguage)
{
	char			*szReturnFile = NULL;

	ASSERT(szFileName);

	szReturnFile = NEW(char, 128);
	if(szReturnFile == NULL)
	{
		return NULL;
	}

	if(iLanguage ==0)			//Eng
	{
		sprintf(szReturnFile,"%s/%s", WEB_DEFAULT_DIR, szFileName);
	}
	else if(iLanguage == 1)		//Loc
	{
		sprintf(szReturnFile,"%s/%s", WEB_LOCAL_DIR, szFileName);
	}
	else						//Template
	{
		sprintf(szReturnFile,"%s/%s", WEB_TEMPLATE_DIR, szFileName);
	}
	return szReturnFile;
}

/*==========================================================================*
* FUNCTION :  MakeVarField
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN char *szBuffer:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
*==========================================================================*/
static char *MakeVarField(IN char *szBuffer)
{
	char	*szReturnBuffer;
	szReturnBuffer = NEW(char, 128);

	if(szReturnBuffer != NULL)
	{
		sprintf(szReturnBuffer, "/*[%s]*/", szBuffer);
		return szReturnBuffer;
	}
	return NULL;
}

/*==========================================================================*
* FUNCTION :  Web_WriteVersionOfWebPages_r
* PURPOSE  :  
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  fVersion:
	      szLanguage:
* RETURN   :  int:
* COMMENTS :  This function is from <communicate.c>
* CREATOR  :  Unknown               DATE: 2013-07-20
*==========================================================================*/
static int Web_WriteVersionOfWebPages_r(IN double fVersion, IN char *szLanguage)
{
	char			*pSearchValue = NULL, *str_string = NULL, szReadString[130];
	FILE			*fp;
#define FILE_CFG_VERSION			"[LOCAL_LANGUAGE_VERSION]"
#define FILE_CFG_LANGUAGE			"[LOCAL_LANGUAGE]"
#define MAX_READ_LINE_LEN			129

	if((fp = fopen(WEB_PAGES_VERSION_FILE, "rb+")) != NULL)
	{
		while(fgets(szReadString, MAX_READ_LINE_LEN, fp) != NULL)
		{
			if((pSearchValue = strstr(szReadString, FILE_CFG_LANGUAGE)) != NULL)
			{
				if(fgets(szReadString, MAX_READ_LINE_LEN, fp) != NULL)
				{
					fseek(fp,(long)-strlen(szReadString), SEEK_CUR);
					if(szLanguage != NULL)
					{
						str_string = NEW(char, strlen(szReadString) + 1);
						if(str_string != NULL)
						{
						    memset(str_string,0, 4);
						    sprintf(str_string,"%3s\n",Cfg_RemoveWhiteSpace(szLanguage));
						    //TRACE("szLanguage[%s]\n", szLanguage);
						    //strncpyz(str_string,Cfg_RemoveWhiteSpace(szLanguage),strlen(szReadString) + 1);
						    fputs(str_string, fp);

						    DELETE(str_string);
						    str_string = NULL;
						}
					}

				}
			}
			else if((pSearchValue = strstr(szReadString,FILE_CFG_VERSION)) != NULL)
			{
				if(fgets(szReadString, MAX_READ_LINE_LEN, fp) != NULL)
				{
					fseek(fp,(long)-strlen(szReadString),SEEK_CUR);
					str_string = NEW(char, 128);
					if(str_string != NULL)
					{
					    sprintf(str_string,"%8.2f\n",fVersion);
					    fputs(str_string, fp);
					    DELETE(str_string);
					    str_string = NULL;
					}
				}	
			}

		}
		fclose(fp);
	}
	return TRUE;
}

/*==========================================================================*
* FUNCTION :  StartTranslateFile
* PURPOSE  :  To fill in the webpages with the languages string in <Web_ResourceUser_*.res>
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN int *iQuitCommand:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
* MODIFY:    Zhao Zicheng	       DATE: 2013-7-22
*==========================================================================*/
static int StartTranslateFile(IN int *iQuitCommand)
{
	int			i = 0, k = 0;
	FILE		*fp1 ;
	char		*pHtml1 = NULL, *pHtml2 = NULL;
	int			iError = 0;
	int			iReturn1 = 0, iReturn2 = 0; 
	//int         iReturn3 = 0;
	char		*szFile = NULL;
	char		*szMakeVar = NULL;
	double		fVersion;
	char	szCommand[128];

	iError = WEB_ReadConfig();
	if(iError == ERR_WEB_OK)
	{
		fVersion = stWebCompareVersion.fVersion;

		WEB_VERSION_INFO	stVersion;
		Web_GetVersionOfWebPages_r(&stVersion);

		TRACE_WEB_USER_NOT_CYCLE("[Web_user][Web_ResourceUser.res] version is %f, language is %s\n", fVersion, stWebCompareVersion.szLanguage);
		TRACE_WEB_USER_NOT_CYCLE("[cfg_ui_function.cfg] version is %f, language is %s\n", stVersion.fVersion, stVersion.szLanguage);
		if(fVersion >  stVersion.fVersion || fVersion <  stVersion.fVersion || strcmp(Cfg_RemoveWhiteSpace(stWebCompareVersion.szLanguage), Cfg_RemoveWhiteSpace(stVersion.szLanguage)) != 0)
		{
			strcpy(szCommand, "cp -rf /app/www_user/html/template/* /app/www_user/html/eng/");
			_SYSTEM(szCommand);
			strcpy(szCommand, "cp -rf /app/www_user/html/template/* /app/www_user/html/loc/");
			_SYSTEM(szCommand);
			for(i = 0; i < WEB_MAX_HTML_PAGE_NUM; i++)
			{
				szFile = MakeFilePath(pWebCfgInfo[i].szFileName, 3);
				ASSERT(szFile);
				if(szFile != NULL)
				{
					iReturn1 = LoadHtmlFile(szFile, &pHtml1);
					iReturn2 = LoadHtmlFile(szFile, &pHtml2);
					DELETE(szFile);
					szFile = NULL;
				}
				if((iReturn1 > 0) && (iReturn2 > 0))
				{
					for(k = 0; k < pWebCfgInfo[i].iNumber; k++)
					{
						/*Eng*/
						szMakeVar = MakeVarField(pWebCfgInfo[i].stWebPrivate[k].szResourceID);
						if(szMakeVar != NULL)
						{
							ReplaceString(&pHtml1, 
								szMakeVar,
								pWebCfgInfo[i].stWebPrivate[k].szDefault);
							DELETE(szMakeVar);
							szMakeVar = NULL;
						}


						/*Loc*/
						szMakeVar = MakeVarField(pWebCfgInfo[i].stWebPrivate[k].szResourceID);
						if(szMakeVar != NULL)
						{
							ReplaceString(&pHtml2, 
								szMakeVar, 
								pWebCfgInfo[i].stWebPrivate[k].szLocal);
							DELETE(szMakeVar);
							szMakeVar = NULL;
						}
					}
				}
				//Save Eng File 
				szFile = MakeFilePath(pWebCfgInfo[i].szFileName, 0); 
				if(szFile != NULL && (fp1 = fopen(szFile,"wb")) != NULL &&
					pHtml1 != NULL)
				{
					fwrite(pHtml1,strlen(pHtml1), 1, fp1);
					fclose(fp1);
				}
				if(szFile != NULL)
				{
					DELETE(szFile);
					szFile = NULL;
				}
				//Save Loc File 
				szFile = MakeFilePath(pWebCfgInfo[i].szFileName, 1); 
				if(szFile != NULL && (fp1 = fopen(szFile,"wb")) != NULL &&
					pHtml1 != NULL)
				{
					fwrite(pHtml2,strlen(pHtml2), 1, fp1);
					fclose(fp1);
				}
				if(szFile != NULL)
				{
				    DELETE(szFile);
				    szFile = NULL;
				}
				if(pHtml1 != NULL)
				{
					DELETE(pHtml1);
					pHtml1 = NULL;
				}

				if(pHtml2 !=  NULL)
				{
					DELETE(pHtml2);
					pHtml2 = NULL;
				}
				RunThread_Heartbeat(RunThread_GetId(NULL));
				if(*iQuitCommand == SERVICE_STOP_RUNNING)
				{
					break;
				}
			}
			TRACE("Web_WriteVersionOfWebPages_r((double)fVersion) \n");
			Web_WriteVersionOfWebPages_r((double)fVersion, stWebCompareVersion.szLanguage);
			sprintf(szCommand,"chmod 777 %s/*", WEB_DEFAULT_DIR);
			_SYSTEM(szCommand);

			//szCommand[0] = NULL;
			sprintf(szCommand,"chmod 777 %s/*", WEB_LOCAL_DIR);
			_SYSTEM(szCommand);
		}

		WEB_PRIVATE_RESOURCE_INFO	*pDelete = (WEB_PRIVATE_RESOURCE_INFO *)pWebCfgInfo;
		WEB_GENERAL_RESOURCE_INFO	*pDeleteWebPrivate = NULL;
		for(i = 0; i < WEB_MAX_HTML_PAGE_NUM && pDelete != NULL; i++, pDelete++)
		{
			pDeleteWebPrivate = pDelete->stWebPrivate;
			for(k = 0; k < pDelete->iNumber && pDelete->stWebPrivate != NULL; k++, pDelete->stWebPrivate++)
			{
				if(pDelete->stWebPrivate != NULL)
				{
					if(pDelete->stWebPrivate->szDefault != NULL)
					{
						DELETE(pDelete->stWebPrivate->szDefault);
						pDelete->stWebPrivate->szDefault = NULL;
					}

					if(pDelete->stWebPrivate->szLocal != NULL)
					{
						DELETE(pDelete->stWebPrivate->szLocal);
						pDelete->stWebPrivate->szLocal = NULL;
					}
				}
			}
			if(pDeleteWebPrivate != NULL)
			{
				DELETE(pDeleteWebPrivate);
				pDeleteWebPrivate = NULL;
			}
		}
		if(pWebCfgInfo != NULL)
		{
			//DELETE(pWebCfgInfo);
			pWebCfgInfo = NULL;
		}
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

/*==========================================================================*
* FUNCTION :  Web_ParseWebStatusInfo
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN char *szBuf:
OUT WEB_STATUS_CONFIG_INFO *pStructData:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-6 15:01
*==========================================================================*/
//static int Web_ParseWebStatusInfo(IN char *szBuf, 
//				  OUT WEB_STATUS_CONFIG_INFO *pStructData)
//{
//	char	*pField = NULL;
//	char	szBuffer[33];
//	/* used as buffer */
//	ASSERT(szBuf);
//	ASSERT(pStructData);
//	/* EquipID */
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
//	if (pField == NULL)
//	{
//		return 1;    
//	}
//	strncpyz(szBuffer, pField, sizeof(szBuffer));
//	//TRACE("pStructData->iEquipID : %s\n", szBuffer);
//	pStructData->iEquipID = atoi(szBuffer);
//
//	/* SignalType */
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
//	if (pField == NULL)
//	{
//		return 2;    
//	}
//	strncpyz(szBuffer, pField, sizeof(szBuffer));
//	//TRACE("pStructData->iSignalType : %s\n", szBuffer);
//	pStructData->iSignalType = atoi(szBuffer);
//
//	/* SignalID */
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
//	if (pField == NULL)
//	{
//		return 3;    
//	}
//	strncpyz(szBuffer, pField, sizeof(szBuffer));
//	//TRACE("pStructData->iSignalID : %s\n", szBuffer);
//
//	pStructData->iSignalID = atoi(szBuffer);
//
//	return ERR_WEB_OK;
//}

/*==========================================================================*
* FUNCTION :  Web_GetShowStatusData
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN void *pCfg:
OUT WEB_STATUS_CONFIG *stStatusConfig:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-6 15:01
*==========================================================================*/
//static int Web_GetShowStatusData(IN void *pCfg, 
//				 OUT WEB_STATUS_CONFIG *stStatusConfig)
//{
//#define WEB_NUMBER_OF_STATUS_CONFIG_INFORMATION		"[NUMBER_OF_CONFIG_SHOW]"
//#define	WEB_STATUS_CONFIG_INFORMATION			"[CONFIG_STATUS_SHOW]"
//
//
//	CONFIG_TABLE_LOADER			loader[1];
//
//	DEF_LOADER_ITEM(loader,
//		WEB_NUMBER_OF_STATUS_CONFIG_INFORMATION,
//		&(stStatusConfig->iNumber), 
//		WEB_STATUS_CONFIG_INFORMATION, 
//		&(stStatusConfig->stWebStatusConfig), 
//		Web_ParseWebStatusInfo);
//
//	if (Cfg_LoadTables(pCfg, 1, loader) != ERR_CFG_OK)
//	{
//		TRACE("Fail to Cfg_LoadTables\n");
//		return ERR_LCD_FAILURE;
//	}
//
//	return ERR_CFG_OK;
//}

/*==========================================================================*
* FUNCTION :  Web_ReadStatusConfig
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	OUT WEB_STATUS_CONFIG **stReturnStatusConfig:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-6 15:01
*==========================================================================*/
//static int Web_ReadStatusConfig(OUT WEB_STATUS_CONFIG **stReturnStatusConfig )
//{
//	int				iRst;
//	void			*pProf = NULL;
//	FILE			*pFile = NULL;
//	char			*szInFile = NULL;
//	size_t			ulFileLen;
//
//	char			szCfgFileName[MAX_FILE_PATH]; 
//
//	Cfg_GetFullConfigPath(CONFIG_FILE_WEB_PRIVATE_R, szCfgFileName, MAX_FILE_PATH);
//
//	/* open file */
//	pFile = fopen(szCfgFileName, "r");
//	if (pFile == NULL)
//	{		
//		return FALSE;
//	}
//
//	ulFileLen = GetFileLength(pFile);
//
//	szInFile = NEW(char, ulFileLen + 1);
//	if (szInFile == NULL)
//	{
//		fclose(pFile);
//		return FALSE;
//	}
//
//	/* read file */
//	ulFileLen = fread(szInFile, sizeof(char), ulFileLen, pFile);
//	fclose(pFile);
//
//	if (ulFileLen == 0) 
//	{
//		/* clear the memory */
//		DELETE(szInFile);
//		return FALSE;
//	}
//	szInFile[ulFileLen] = '\0';  /* end with NULL */
//
//	/* create SProfile */
//	pProf = Cfg_ProfileOpen(szInFile, (int)ulFileLen);
//
//	if (pProf == NULL)
//	{
//		DELETE(szInFile);
//		return FALSE;
//	}
//
//	//2.Read current all command content and command numbers
//	WEB_STATUS_CONFIG		*stStatusConfig;
//	stStatusConfig = NEW(WEB_STATUS_CONFIG, 1);
//
//	iRst = Web_GetShowStatusData(pProf, stStatusConfig);
//
//	if (iRst != ERR_WEB_OK)
//	{
//		AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
//			"Fail to get resource table.");
//
//		DELETE(pProf); 
//		DELETE(szInFile);
//		if(stStatusConfig != NULL)
//		{
//		    DELETE(stStatusConfig);
//		    stStatusConfig = NULL;
//		}
//		return iRst;
//	}
//
//	*stReturnStatusConfig = stStatusConfig;
//
//	DELETE(pProf); 
//	DELETE(szInFile);
//	if(stStatusConfig != NULL)
//	{
//	    DELETE(stStatusConfig);
//	    stStatusConfig = NULL;
//	}
//	return ERR_WEB_OK;
//}

/*==========================================================================*
* FUNCTION :    Web_QueryHisAlarm
* PURPOSE  :	 Query His alarm
* CALLS    : 
* CALLED BY:	
* ARGUMENTS:   IN time_t fromTime:
IN time_t toTime:
IN int iEquipID:
OUT char **ppBuf:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
*==========================================================================*/
static int Web_QueryHisAlarm(IN time_t fromTime,
			     IN time_t toTime,
			     IN int iEquipID,
			     OUT void **ppBuf)
{
	/*query history alarm record*/
	/*query parameter equip,data type (his, statistics,from time to time)*/
	int			iRecords = MAX_HIS_ALARM_COUNT;//400;
	int			iResult = 0,iStartRecordNo = -1;		//record number
	HANDLE		hAlarmData;
	int			iBufLen	= MAX_HIS_ALARM_COUNT;//400;
	int			iMatchRecord = 0, i;
	HIS_ALARM_RECORD	*pTempHisAlarmRecord =NULL, *pTempFullHisAlarmRecord = NULL;


	hAlarmData =	DAT_StorageOpen(HIST_ALARM_LOG);

	if(hAlarmData == NULL)
	{
		AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_WARNING,"Fail to open hAlarmData");
		TRACE_WEB_USER("DAT_StorageOpen fail\n");
		return FALSE;
	}

	HIS_ALARM_RECORD *pHisAlarmRecord	= NEW(HIS_ALARM_RECORD,iBufLen);
	if(pHisAlarmRecord == NULL)
	{
	    return FALSE;
	}
	HIS_GENERAL_CONDITION *pCondition = NEW(HIS_GENERAL_CONDITION,1);
	if(pCondition == NULL)
	{
	    if(pHisAlarmRecord != NULL)
	    {
		DELETE(pHisAlarmRecord);
		pHisAlarmRecord = NULL;
	    }
	    return FALSE;
	}

	ASSERT(pCondition);
	//TRACE("In Web_QueryHisAlarm toTime is %d, fromTime is %d.\n",toTime,fromTime);
	pCondition->tmToTime			= toTime;
	pCondition->tmFromTime			= fromTime;
	pCondition->iEquipID			= iEquipID;
	pCondition->iDataType			= QUERY_HISALARM_DATA;


	if(pHisAlarmRecord == NULL )
	{
		DAT_StorageClose(HIS_ALARM_LOG);

		DELETE(pCondition);
		pCondition = NULL;
		DELETE(pHisAlarmRecord);
		pHisAlarmRecord = NULL;


		return FALSE;
	}
	//time_t the_time = time((time_t *)0);
	//TRACE_WEB_USER("\n__The Time 1 is:%ld___\n",the_time);
	iRecords = MAX_HIS_ALARM_COUNT;
	iStartRecordNo = 1;
	iResult =DAT_StorageReadRecords(hAlarmData,
		&iStartRecordNo, 
		&iRecords, 
		(void*)pHisAlarmRecord, 
		0,
		FALSE);

	DELETE(pCondition);
	pCondition = NULL;
	//TRACE("Current Alarm Records is %d.iResult is %d.\n\n",iRecords,iResult);


	TRACE_WEB_USER("iResult is %d, iRecords is %d\n", iResult, iRecords);
	if(iResult && iRecords > 0)
	{
		HIS_ALARM_RECORD *pFullHisAlarmRecord	= NEW(HIS_ALARM_RECORD,iRecords);
		ASSERT(pFullHisAlarmRecord);
		memset(pFullHisAlarmRecord, 0x0, sizeof(HIS_ALARM_RECORD) *  iRecords);
		pTempFullHisAlarmRecord = pFullHisAlarmRecord;
		pTempHisAlarmRecord = pHisAlarmRecord;
		if(pFullHisAlarmRecord != NULL && pHisAlarmRecord != NULL)
		{

			for(i = 0; i < iRecords && pHisAlarmRecord != NULL && pFullHisAlarmRecord != NULL; i++, pHisAlarmRecord++)
			{
				if((pHisAlarmRecord->tmStartTime - fromTime) > 0 && (toTime - pHisAlarmRecord->tmStartTime) > 0 && (pHisAlarmRecord->iEquipID == iEquipID || iEquipID < 0))
				{	
					memcpy(pFullHisAlarmRecord, pHisAlarmRecord, sizeof(HIS_ALARM_RECORD));
					iMatchRecord++;
					pFullHisAlarmRecord++;
				}


				if(i == iRecords/100)
				{
					RunThread_Heartbeat(RunThread_GetId(NULL));
				}
			}


		}

		pFullHisAlarmRecord = pTempFullHisAlarmRecord ;
		pHisAlarmRecord = pTempHisAlarmRecord ;
		*ppBuf = (void *)pFullHisAlarmRecord;
		DELETE(pHisAlarmRecord);

		DAT_StorageClose(hAlarmData);

		//TRACE("Current Alarm Records is %d.\n\n",iRecords);
		return iMatchRecord;
	}
	else
	{
		DELETE(pHisAlarmRecord);
		pHisAlarmRecord = NULL;
		DAT_StorageClose(hAlarmData);
		return FALSE;
	}
}

static int Web_GetEquipName(IN OUT char *szEquipName)
{
#define IS_NUMBER(c)								(((c)>=(0x30) && (c)<=(0x39)) || (c) == 0x23)
	char *p=szEquipName;
	/*char pfinal[30]="";*/
	int i=0;
	while(*p)
	{
		if(IS_NUMBER(*p))
		{
			p=p++;
			break;
		}
		else
		{
			//pfinal[i]=*p;
			p++;
			i++;
		}
	}
	//p=strdup(pfinal);
	return i;
}

static char *Web_GetEquipNameFromInterface_New(IN int iEquipID,IN int iLanguage,IN int iPosition)
{


	char	*szEquipName = NULL;
	int			iPos, iBufLen;
	EQUIP_INFO		*pEquipInfo = NULL;

	int iError = DxiGetData(VAR_A_EQUIP_INFO,			
		iEquipID,	
		0,
		&iBufLen,			
		&pEquipInfo,			
		0);

	/*szEquipName = NEW(char, 64);
	if(szEquipName == NULL)
	{
		return NULL;
	}
	memset(szEquipName, 0, 64);*/

	szEquipName = NEW(char, 64);


	if(szEquipName == NULL)
	{
		return NULL;
	}
	memset(szEquipName, 0, 64);


	if(iError == ERR_DXI_OK)
	{		
		if(iPosition != 0)
		{

			strncpyz(szEquipName, pEquipInfo->pEquipName->pFullName[iLanguage], 64);
			iPos = Web_GetEquipName(pEquipInfo->pEquipName->pFullName[iLanguage]);
			snprintf(szEquipName + iPos, 5, "#%d", iPosition);
			//TRACE_WEB_USER("iPosition = %d\n", iPosition);
		}				
		else
		{
			strncpyz(szEquipName, pEquipInfo->pEquipName->pFullName[iLanguage], 64);
		}


		return szEquipName;

	}
	else
	{
		SAFELY_DELETE(szEquipName);
		return NULL;
	}
}

/*==========================================================================*
* FUNCTION :    Web_GetASignalName
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   IN int iSignalID:
IN int iDataType:
IN int iEquipID:
IN int iLanguage:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
*==========================================================================*/
static char *Web_GetASignalName(IN int iSignalID, 
				IN int iDataType, 
				IN int iEquipID, 
				IN int iLanguage,
				OUT char **szUnit,
				IN int iStatue,
				IN char **szStatue,
				IN int *iValueType)
{
	int		iVarSubID, iError = 0;
	iVarSubID = DXI_MERGE_SIG_ID(iDataType,iSignalID);
	char	*szSignalName = NULL;
	int	iBufLen = 0;
	int	iTimeOut = 0;

	szSignalName = NEW(char, 33);
	if(szSignalName == NULL)
	{
		return NULL;
	}
	memset(szSignalName, 0x0, 33);

	if(iDataType == SIG_TYPE_SAMPLING )
	{
		SAMPLE_SIG_INFO		*pSigInfo = NULL;	
		iError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
			iEquipID,			
			iVarSubID,		
			&iBufLen,			
			(void *)&pSigInfo,			
			iTimeOut);

		if (iError == ERR_DXI_OK)
		{
			if(strcmp(pSigInfo->szSigUnit, "\0") != 0)
				//if(pSigInfo->szSigUnit != NULL)
			{
				*szUnit = NEW(char, 8);
				ASSERT(*szUnit);
				if(*szUnit != NULL)
				{
					strncpyz(*szUnit, pSigInfo->szSigUnit, 8);
				}
			}

			if((pSigInfo->iStateNum > 0) && (iStatue != -1) && (iStatue <= (int)pSigInfo->fMaxValidValue) && (iStatue >= (int)pSigInfo->fMinValidValue))
			{
				*szStatue = NEW(char, 32);
				iStatue = iStatue - (int)pSigInfo->fMinValidValue;
				strncpyz(*szStatue, pSigInfo->pStateText[iStatue]->pFullName[iLanguage], 32);
			}

			strncpyz(szSignalName, pSigInfo->pSigName->pFullName[iLanguage], 33);
			if(iValueType != NULL)
			{
				*iValueType = pSigInfo->iSigValueType;
			}
			return	szSignalName;
		}
		else
		{
		    if(szSignalName != NULL)
		    {
			DELETE(szSignalName);
			szSignalName = NULL;
		    }
			return NULL;
		}

	}
	else if(iDataType == SIG_TYPE_CONTROL)
	{
		CTRL_SIG_INFO	*pSigInfo = NULL;
		iError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
			iEquipID,			
			iVarSubID,		
			&iBufLen,			
			(void *)&pSigInfo,			
			iTimeOut);
		if (iError == ERR_DXI_OK)
		{
			if(strcmp(pSigInfo->szSigUnit, "\0") != 0)
				//if(pSigInfo->szSigUnit != NULL)
			{
				*szUnit = NEW(char, 8);
				ASSERT(*szUnit);
				if(*szUnit != NULL)
				{
					strncpyz(*szUnit, pSigInfo->szSigUnit, 8);
				}
			}


			if((pSigInfo->iStateNum > 0) && (iStatue != -1) && (iStatue <= (int)pSigInfo->fMaxValidValue) && (iStatue >= (int)pSigInfo->fMinValidValue))
			{
				*szStatue = NEW(char, 32);
				iStatue = iStatue - (int)pSigInfo->fMinValidValue;
				strncpyz(*szStatue, pSigInfo->pStateText[iStatue]->pFullName[iLanguage], 32);
			}

			if(iValueType != NULL)
			{
				*iValueType = pSigInfo->iSigValueType;
			}

			strncpyz(szSignalName, pSigInfo->pSigName->pFullName[iLanguage], 33);
			return	szSignalName;
		}
		else
		{
		    if(szSignalName != NULL)
		    {
			DELETE(szSignalName);
			szSignalName = NULL;
		    }
			return NULL;
		}

	}
	else if(iDataType == SIG_TYPE_SETTING)
	{
		SET_SIG_INFO	*pSigInfo = NULL;
		iError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
			iEquipID,			
			iVarSubID,		
			&iBufLen,			
			(void *)&pSigInfo,			
			iTimeOut);
		if (iError == ERR_DXI_OK)
		{


			if(strcmp(pSigInfo->szSigUnit, "\0") != 0)
				//if(pSigInfo->szSigUnit != NULL)
			{
				*szUnit = NEW(char, 8);
				ASSERT(*szUnit);
				if(*szUnit != NULL)
				{
					strncpyz(*szUnit, pSigInfo->szSigUnit, 8);
				}
			}

			if((pSigInfo->iStateNum > 0) && (iStatue != -1) && (iStatue <= (int)pSigInfo->fMaxValidValue) && (iStatue >= (int)pSigInfo->fMinValidValue))
			{
				*szStatue = NEW(char, 32);
				iStatue = iStatue - (int)pSigInfo->fMinValidValue;
				strncpyz(*szStatue, pSigInfo->pStateText[iStatue]->pFullName[iLanguage], 32);
			}

			if(iValueType != NULL)
			{
				*iValueType = pSigInfo->iSigValueType;
			}

			strncpyz(szSignalName, pSigInfo->pSigName->pFullName[iLanguage], 33);
			return	szSignalName;
		}
		else
		{
		    if(szSignalName != NULL)
		    {
			DELETE(szSignalName);
			szSignalName = NULL;
		    }
			return NULL;
		}

	}
	else if(iDataType == SIG_TYPE_ALARM)
	{
		ALARM_SIG_INFO	*pSigInfo = NULL;

		iError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
			iEquipID,			
			iVarSubID,		
			&iBufLen,			
			(void *)&pSigInfo,			
			iTimeOut);

		if (iError == ERR_DXI_OK)
		{
			strncpyz(szSignalName, pSigInfo->pSigName->pFullName[iLanguage], 33);

			return	szSignalName;
		}
		else
		{
		    if(szSignalName != NULL)
		    {
			DELETE(szSignalName);
			szSignalName = NULL;
		    }
			return NULL;
		}	

	}
	else
	{
	    if(szSignalName != NULL)
	    {
		DELETE(szSignalName);
		szSignalName = NULL;
	    }
		return NULL;
	}
}

/*==========================================================================*
* FUNCTION :   Web_TransferAlarmDataIDToName 
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN void *pBuf:
IN int iDataLen:
IN int iLanguage:
OUT void **ppBuf:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
*==========================================================================*/
static int Web_TransferAlarmDataIDToName(IN void *pBuf,
					 IN int iDataLen,
					 IN int iLanguage,
					 OUT void **ppBuf)
{
	char						*pszEquipName	= NULL;
	char						*pszSignalName	= NULL;
	char						*szUnit = NULL;
	HIS_ALARM_TO_RETURN			*pDataReturn	= NULL;
	HIS_ALARM_RECORD			*pAlarmRecord	= NULL;
	//int							iValueType = 0;

	ASSERT(pBuf);
	if(pBuf == NULL)
	{
		return FALSE;
	}
	pAlarmRecord = (HIS_ALARM_RECORD *)pBuf;

	/*init*/
	pDataReturn = NEW(HIS_ALARM_TO_RETURN, iDataLen);
	if(pDataReturn == NULL)
	{
		return FALSE;
	}
	memset(pDataReturn, 0x0, (size_t)iDataLen);

	*ppBuf  = (void *)pDataReturn;
	while(pAlarmRecord != NULL && iDataLen > 0)
	{
		if(pAlarmRecord->szSerialNumber[0] == '\0')
		{
			pszEquipName = Web_GetEquipNameFromInterface_New(pAlarmRecord->iEquipID, iLanguage, pAlarmRecord->iPosition); 
			if(pszEquipName != NULL)
			{
				strncpyz(pDataReturn->szEquipName,pszEquipName,
					sizeof(pDataReturn->szEquipName));
				DELETE(pszEquipName);
				pszEquipName = NULL;
			}
			else
			{
				strncpyz(pDataReturn->szEquipName,"--",
					sizeof(pDataReturn->szEquipName));
			}
		}
		else
		{
#ifdef		WEB_SUPPORT_RECTID
			pszEquipName = Web_GetEquipNameFromInterface_New(pAlarmRecord->iEquipID, iLanguage, pAlarmRecord->iPosition); 
			if(pszEquipName)
			{			
				strncpyz(pDataReturn->szEquipName, pszEquipName,
					sizeof(pDataReturn->szEquipName));
				DELETE(pszEquipName);
				pszEquipName = NULL;
			}
			else
			{
				strncpyz(pDataReturn->szEquipName,"--",
					sizeof(pDataReturn->szEquipName));
			}
#else
			strncpyz(pDataReturn->szEquipName, pAlarmRecord->szSerialNumber,
				sizeof(pDataReturn->szEquipName));
			//TRACE_WEB_USER("\npAlarmRecord->szSerialNumber = %s\n", pAlarmRecord->szSerialNumber);
#endif
		}

		pszSignalName = Web_GetASignalName(pAlarmRecord->iAlarmID,		//Signal ID
			SIG_TYPE_ALARM,				//Signal type
			pAlarmRecord->iEquipID,
			iLanguage,
			&szUnit,
			-1,
			NULL,
			NULL);

		if(pszSignalName != NULL)
		{
			strncpyz(pDataReturn->szAlarmName, pszSignalName,
				sizeof(pDataReturn->szAlarmName));
			DELETE(pszSignalName);
			pszSignalName = NULL;
		}
		else
		{
			strncpyz(pDataReturn->szAlarmName, "--",
				sizeof(pDataReturn->szAlarmName));
		}

		pDataReturn->tmStartTime	= pAlarmRecord->tmStartTime;
		pDataReturn->tmEndTime		= pAlarmRecord->tmEndTime;		
		pDataReturn->fTriggerValue	= pAlarmRecord->varTrigValue.fValue;
		pDataReturn->byLevel		= pAlarmRecord->byAlarmLevel;

		if(szUnit != NULL)
		{
			strncpyz(pDataReturn->szUnit, szUnit, sizeof(pDataReturn->szUnit));
			DELETE(szUnit);
			szUnit = NULL;
		}
		else
		{
			strncpyz(pDataReturn->szUnit, "--", sizeof(pDataReturn->szUnit));
		}
		pAlarmRecord++;
		pDataReturn++;
		iDataLen--;

	}
	return TRUE;
}

static int Compare_His_ALARM_SIG_VALUE(const HIS_ALARM_TO_RETURN *p1,
				       const HIS_ALARM_TO_RETURN *p2)
{
	if(p1->tmEndTime < p2->tmEndTime)
	{
		return 1;
	}

	if(p1->tmEndTime > p2->tmEndTime)
	{
		return -1;
	}

	return 0;

}

//static void Web_SortHisAlarmData(HIS_ALARM_TO_RETURN *pHisAlarmToReturn, int iNum)
//{
//
//	qsort(pHisAlarmToReturn, (size_t)iNum, sizeof(*pHisAlarmToReturn),
//		(int(*)(const void *, const void *))Compare_His_ALARM_SIG_VALUE);
//
//}

static char *Web_GetAEquipName(IN int iEquipID, IN int iLanguage)
{
	EQUIP_INFO* pEquipInfo;
	int			iEquipNum = 0;
	int			i = 0;
	int	iBufLen = 0;
	int iError = DxiGetData(VAR_ACU_EQUIPS_LIST,
		0,			
		0,		
		&iBufLen,			
		&pEquipInfo,			
		0);

	iEquipNum = iBufLen / sizeof(EQUIP_INFO);
	if(iError == ERR_DXI_OK)
	{
		for( i = 0; i < iEquipNum && pEquipInfo != NULL; i++)
		{
			if(pEquipInfo->iEquipID == iEquipID)
			{
				return pEquipInfo->pEquipName->pFullName[iLanguage];
			}
			pEquipInfo++;
		}
	}
	else
	{
		return NULL;
	}
	return NULL;
}

/*==========================================================================*
* FUNCTION :    Web_MakeQueryAlarmDataBuffer
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   IN int iEquipID:
IN time_t fromTime:
IN time_t toTime:
IN int iLanguage:
IN char **ppBuf:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
*==========================================================================*/
static int Web_MakeQueryAlarmDataBuffer(IN int iEquipID, 
					IN time_t fromTime, 
					IN time_t toTime, 
					IN int iLanguage, 
					IN char **ppBuf)
{
	int							iLen = 0, iFileLen = 0;
	HIS_ALARM_RECORD			*pReturnData		= NULL;
	HIS_ALARM_TO_RETURN			*pTraiReturnData	= NULL;	
	char						*pMakeBuf			= NULL, *pMakeFileBuf = NULL;
	int							iDataLen = 0;
	int							iBufLen = 0;
	void						*pDeletePtr = NULL;
	char						szTime1[32], szTime2[32];
	char						*szLevelName[] = {"NA", "OA", "MA", "CA"};
	int							nAlarms;
	int				iMatchNumber = 0;

	/*Query alarm*/
	//TRACE("In Web_MakeQueryAlarmDataBuffer, fromTime is %d,toTime is %d\n",fromTime,toTime);
	if(( iBufLen = Web_QueryHisAlarm(fromTime, toTime, iEquipID,
		(void *)&pReturnData)) <= 0)
	{
		if(pReturnData != NULL)
		{
			DELETE(pReturnData);
			pReturnData = NULL;
		}
		TRACE_WEB_USER("No history alarm\n");
		return FALSE;
	}
	TRACE("\n QueryAlarmData iBufLen: [%d]\n", iBufLen);
#ifdef _SHOW_WEB_INFO
	//TRACE("iBufLen: [%d]\n", iBufLen);
#endif 

	iDataLen = iBufLen * MAX_ALARM_DATA_LEN;
	pMakeBuf = NEW(char, iDataLen);
	if(pMakeBuf == NULL)
	{
		DELETE(pReturnData);
		pReturnData = NULL;
		return FALSE;
	}
	memset(pMakeBuf, 0, (size_t)iDataLen);

	//Special buffer for make file 
	
	pMakeFileBuf = NEW(char, (iBufLen * (MAX_ALARM_DATA_LEN + 50)));
	if(pMakeFileBuf == NULL)
	{
	    if(pMakeBuf != NULL)
	    {
		DELETE(pMakeBuf);
		pMakeBuf = NULL;
	    }
	    
	    DELETE(pReturnData);
	    pReturnData = NULL;
	    return FALSE;
	}
	memset(pMakeFileBuf, 0, sizeof(pMakeFileBuf));

	/*Transfer the match language*/
	if(Web_TransferAlarmDataIDToName((void *)pReturnData, iBufLen, iLanguage,
		(void *)&pTraiReturnData) == FALSE)
	{
		//TRACE("Web_TransferAlarmDataIDToName : FALSE");

		DELETE(pReturnData);
		pReturnData = NULL;

		DELETE(pMakeBuf);
		pMakeBuf = NULL;
		TRACE_WEB_USER("Web_TransferAlarmDataIDToName failed!\n");
		return FALSE;
	}

	DELETE(pReturnData);
	pReturnData = NULL;


	nAlarms = 0;
	pDeletePtr = pTraiReturnData;

	//sort
	//Web_SortHisAlarmData(pTraiReturnData, iBufLen);

	char szCommandStr[128];
	sprintf(szCommandStr, "rm  %s *.tar -f", SET_PARA_tar_FILE);
	_SYSTEM(szCommandStr);

	iLen += sprintf(pMakeBuf + iLen,"%2d", 1);
	//int		i = 1;
	iLen += sprintf(pMakeBuf + iLen, "[");
	while(pTraiReturnData != NULL && iBufLen > 0)
	{
		TimeToString(pTraiReturnData->tmStartTime, TIME_CHN_FMT, 
			szTime1, sizeof(szTime1));
		TimeToString(pTraiReturnData->tmEndTime, TIME_CHN_FMT, 
			szTime2, sizeof(szTime2));
		if(iMatchNumber < 500)
		{
			iLen += sprintf(pMakeBuf + iLen,"[\"%16s\",\"%16s\",%d,%ld,%ld],",
				pTraiReturnData->szEquipName,
				pTraiReturnData->szAlarmName,
				pTraiReturnData->byLevel,
				pTraiReturnData->tmStartTime,//szTime1,
				pTraiReturnData->tmEndTime);//szTime2); 
		}
		iMatchNumber++;
		// maofuhua changed the format. 2005-3-15
		nAlarms++;
		// removed the value column.

		iFileLen +=sprintf(pMakeFileBuf + iFileLen,"<tr><td>%5d</td> \t<td>%-32s</td> \t<td>%-32s</td> \t<td>%-5s</td> \t<td>%-15s</td> \t<td>%-15s</td></tr>\n",
		    nAlarms,
		    pTraiReturnData->szEquipName,
		    pTraiReturnData->szAlarmName,
		    szLevelName[pTraiReturnData->byLevel],
		    //pTraiReturnData->fTriggerValue,
		    szTime1,
		    szTime2);

		pTraiReturnData++;
		iBufLen--;
	}
	if(iLen >= 1)
	{
		iLen = iLen - 1;	// ȥ�����һ�������Է���JSON��ʽ
	}
	iLen += sprintf(pMakeBuf + iLen, "]");
	TRACE_WEB_USER("pMakeBuf is %s\n", pMakeBuf);
	*ppBuf = pMakeBuf;
#ifdef _SHOW_WEB_INFO
	//TRACE("********[Web_MakeQueryAlarmDataBuffer]******%s\n", pMakeBuf);
#endif 

	int iAccess;
	iAccess = access(WEB_LOG_DIR_HA, R_OK|W_OK);

	FILE *fp;
	if((fp = fopen(WEB_LOG_DIR_HA,"wb")) != NULL && iFileLen > 1)
	{

		char	szFileTitle[1024];

		TimeToString(fromTime, TIME_CHN_FMT, 
			szTime1, sizeof(szTime1));
		TimeToString(toTime, TIME_CHN_FMT, 
			szTime2, sizeof(szTime2));

		// removed the value column.
		sprintf(szFileTitle, "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" \
		    "<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /></head><body>\n" \
		    "<style>body,html{padding:10px;margin:0;font-size:14px;line-height:25px;font-family:Arial;}table{border-left:#ddd solid 1px;border-top:#ddd solid 1px;margin-top:20px;}table td{border-right:#ddd solid 1px;border-bottom:#ddd solid 1px;height:30px;line-height:30px;padding:0 0 0 10px;font-size:14px;color:#333}table tr:first-child td{font-weight:bold;height:40px;line-height:40px;}</style>\n"\
		    "Query Alarm History Log<br/>\nQuery EquipID: %s<br/>\n" \
			"Query Time: from %s to %s<br/>\n"	\
			"Total %d alarm(s) queried.<br/>\n\n" \
			"<table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n<tr>\n" \
			"<td>%5s</td> \t<td>%-32s</td> \t<td>%-32s</td> \t<td>%s</td> \t<td>%-15s</td> \t<td>%-15s</td>\n</tr>\n",
			(iEquipID > 0) ? Web_GetAEquipName(iEquipID, iLanguage) : "All Devices",
			szTime1,//ctime(&fromTime),
			szTime2,//ctime(&toTime),
			nAlarms,
			"Index",
			"Device Name",
			"Signal Name",
			"Alarm Level",
			//"Value",
			"Start Time",
			"End Time");

		fwrite(szFileTitle,strlen(szFileTitle), 1, fp);
		fwrite(pMakeFileBuf,strlen(pMakeFileBuf), 1, fp);
		memset(szFileTitle, 0, sizeof(szFileTitle));
		sprintf(szFileTitle, "</table></body>\n" \
		    "</html>\n");
		fwrite(szFileTitle,strlen(szFileTitle), 1, fp);
		fclose(fp);
		if(iAccess < 0)
		{
			char szCommandStr[128];
			sprintf(szCommandStr, "chmod 777 %s", WEB_LOG_DIR_HA);
			_SYSTEM(szCommandStr);
		}


	}

	DELETE(pMakeFileBuf);
	pMakeFileBuf = NULL;

	DELETE(pDeletePtr);
	pDeletePtr = NULL;

	//fclose(fp);

	return TRUE;
}

/*==========================================================================*
* FUNCTION :   Web_fnCompareQueryCondition1
* PURPOSE  :	Compare Condition	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   const void *pRecord:
const void *pTCondition:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
*==========================================================================*/
static int Web_fnCompareQueryCondition1(IN const void *pRecord, 
				       IN const void *pTCondition)
{
	HIS_GENERAL_CONDITION *pCondition = (HIS_GENERAL_CONDITION *)pTCondition;
	static unsigned int		lnHisDataCount = 0;
	static unsigned int		lnStatDataCount = 0;
	static unsigned int		lnHisAlarmCount = 0;
	static unsigned int		lnHisControlCount = 0;

	//TRACE("Come here compare data type is %d.\n", (int)pCondition->iDataType);

	if((pRecord == NULL) || (pTCondition == NULL))
	{
		return FALSE;
	}

	switch((int)pCondition->iDataType)
	{

	case QUERY_HIS_DATA:
		{	
			/*History data*/
			//TRACE("*********QUERY_HIS_DATA*************\n");
			HIS_DATA_RECORD *pRecordData	= (HIS_DATA_RECORD *)pRecord;
			lnHisDataCount++;
			if(lnHisDataCount % 200 == 0)
			{
				RunThread_Heartbeat(RunThread_GetId(NULL));
				lnHisDataCount = 0;
			}

			if(pRecordData->iEquipID == pCondition->iEquipID || 
				pCondition->iEquipID < 0)
			{
				if(((pRecordData->tmSignalTime - pCondition->tmFromTime) > 0) && 
					((pCondition->tmToTime - pRecordData->tmSignalTime) > 0))
				{

					return TRUE;
				}
				else
				{
					return FALSE;
				}
			}
			else
			{
				return FALSE;
			}
			break;
		}
	case QUERY_STAT_DATA:
		{
			/*Statical data*/
			//TRACE("*********QUERY_STAT_DATA*************\n");
			HIS_STAT_RECORD		*pRecordData = (HIS_STAT_RECORD *)pRecord;

			lnStatDataCount++;
			if(lnStatDataCount % 200 == 0)
			{
				RunThread_Heartbeat(RunThread_GetId(NULL));
				lnStatDataCount = 0;
			}

			if(pRecordData->iEquipID == pCondition->iEquipID || 
				pCondition->iEquipID < 0)
			{
				if((pRecordData->tmStatTime - pCondition->tmFromTime) > 0 && 
					( pCondition->tmToTime - pRecordData->tmStatTime) > 0)
				{	
					return TRUE;
				}
				else
				{
					return FALSE;
				}
			}
			else
			{
				return FALSE;
			}
			break;

		}
	case QUERY_HISALARM_DATA:
		{
			/*Query history alarm*/
			//TRACE("*********QUERY_HISALARM_DATA*************\n");
			HIS_ALARM_RECORD	*pRecordData = (HIS_ALARM_RECORD *)pRecord;

			lnHisAlarmCount++;
			if(lnHisAlarmCount % 200 == 0)
			{
				RunThread_Heartbeat(RunThread_GetId(NULL));
				lnHisAlarmCount = 0;
			}

			/*TRACE("pCondition->iEquipID is %d.pRecordData->tmStartTime is %d,pCondition->tmFromTime is %d, pCondition->tmFromTime is %d\n",
			pCondition->iEquipID,pRecordData->tmStartTime, pCondition->tmFromTime, pCondition->tmToTime);

			TRACE("pCondition->iEquipID is %d.Time Interval is %d,Timew is %d\n",
			pCondition->iEquipID,(pRecordData->tmStartTime - pCondition->tmFromTime), 
			(pCondition->tmToTime - pRecordData->tmStartTime));*/

			if(pRecordData->iEquipID == pCondition->iEquipID || 
				pCondition->iEquipID < 0)
			{
				if((pRecordData->tmStartTime - pCondition->tmFromTime) > 0 && 
					( pCondition->tmToTime - pRecordData->tmStartTime) > 0)
				{	
					/* TRACE("pCondition->iEquipID is %d.pRecordData->tmStartTime is %d,pCondition->tmFromTime is %d, pCondition->tmFromTime is %d\n",
					pCondition->iEquipID,pRecordData->tmStartTime, pCondition->tmFromTime, pCondition->tmToTime);*/
					return TRUE;
				}
				else
				{
					return FALSE;
				}
			}
			else
			{
				return FALSE;
			}
			break;

		}
	case QUERY_CONTROLCOMMAND_DATA:
		{

			/*query control command log*/
			//TRACE("*********QUERY_CONTROLCOMMAND_DATA*************\n");
			HIS_CONTROL_RECORD		*pRecordData = (HIS_CONTROL_RECORD *)pRecord;

			lnHisControlCount++;
			if(lnHisControlCount % 200 == 0)
			{
				RunThread_Heartbeat(RunThread_GetId(NULL));
				lnHisControlCount = 0;
			}


			/* TRACE("###########pCondition->iEquipID is %d.pRecordData->tmControlTime is %d,pCondition->tmFromTime is %d, pCondition->tmFromTime is %d\n",
			pCondition->iEquipID,pRecordData->tmControlTime, pCondition->tmFromTime, pCondition->tmToTime);*/

			if(pRecordData->iEquipID == pCondition->iEquipID || 
				pCondition->iEquipID < 0)
			{
				if((pRecordData->tmControlTime - pCondition->tmFromTime) > 0 && 
					( pCondition->tmToTime - pRecordData->tmControlTime) > 0)
				{	
					/*TRACE("@@@@@@@@@@@@@@@@pCondition->iEquipID is %d.pRecordData->tmControlTime is %d,pCondition->tmFromTime is %d, pCondition->tmFromTime is %d\n",
					pCondition->iEquipID,pRecordData->tmControlTime, pCondition->tmFromTime, pCondition->tmToTime);*/

					return TRUE;
				}
				else
				{
					return FALSE;
				}
			}
			else
			{
				return FALSE;
			}
			break;
		}
	case QUERY_DISEL_TEST:
		{
			WEB_GC_DSL_TEST_INFO	*pRecordData = (WEB_GC_DSL_TEST_INFO *)pRecord;

			if((pRecordData->tStartTime - pCondition->tmFromTime) > 0 && 
				( pCondition->tmToTime - pRecordData->tStartTime) > 0)
			{	
				return TRUE;
			}
			else
			{
				return FALSE;
			}

			break;

		}
	default:

		return FALSE;


	}

}

/*==========================================================================*
* FUNCTION :    Web_QueryHisData1
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN time_t toTime:
IN time_t fromTime:
IN int iEquipID:
IN int iDataType:
OUT char **ppBuf: 
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
*==========================================================================*/
static int Web_QueryHisData1(IN time_t toTime,
			    IN time_t fromTime,
			    IN int iEquipID,
			    IN int iDataType, 
			    OUT void **ppBuf)
{
	/*query parameter equip,data type (his, statistics,from time to time)*/
	int			iRecords = 6000, iResult,iStartRecordNo = -1;		//record number
	HANDLE		hHisData;
	int			iBufLen = 6000;


	/*Judge data type*/
	HIS_GENERAL_CONDITION *pCondition	= NEW(HIS_GENERAL_CONDITION,1);
	pCondition->tmToTime				= toTime;
	pCondition->tmFromTime				= fromTime;
	pCondition->iEquipID				= iEquipID;
	pCondition->iDataType				= iDataType;

	//TRACE("iDataType:[%d]\n", iDataType);
	if(iDataType == QUERY_STAT_DATA)
	{
		hHisData =	DAT_StorageOpen(STAT_DATA_LOG);
		if(hHisData == NULL)
		{
			return FALSE;
		}
		HIS_STAT_RECORD *pHisDataRecord = NEW(HIS_STAT_RECORD, iBufLen);
		ASSERT(pHisDataRecord);		
		memset(pHisDataRecord, 0x0, iBufLen * sizeof(HIS_STAT_RECORD));

		//TRACE("Start to DAT_StorageFindRecords\n");
		iResult = DAT_StorageFindRecords(hHisData,
			Web_fnCompareQueryCondition1, 
			(void *)pCondition,
			&iStartRecordNo, 
			&iRecords, 
			(void*)pHisDataRecord, 
			FALSE,
			TRUE);
		DELETE(pCondition);
		pCondition = NULL;
		//#ifdef _SHOW_WEB_INFO
		//TRACE("HIS_STAT_RECORD : iResult : [%d] iRecords[%d]\n", iResult, iRecords);
		//#endif

		if(iResult >= 1  && iRecords > 0)
		{
			//return (HIS_DATA_RECORD *)pHisDataRecord;
			*ppBuf = (void *)pHisDataRecord;
			DAT_StorageClose(hHisData);


			return iRecords;
		}
		else
		{
			DELETE(pHisDataRecord);
			pHisDataRecord = NULL;
			DAT_StorageClose(hHisData);
			return FALSE;

		}
	}
	else
	{
		hHisData =	DAT_StorageOpen( HIS_DATA_LOG);
		if(hHisData == NULL)
		{
			return FALSE;
		}

		HIS_DATA_RECORD *pHisDataRecord = NEW(HIS_DATA_RECORD,iBufLen);
		ASSERT(pHisDataRecord);	
		memset(pHisDataRecord, 0x0, iBufLen * sizeof(HIS_DATA_RECORD));

		iResult = DAT_StorageFindRecords(hHisData,
			Web_fnCompareQueryCondition1, 
			(void *)pCondition,
			&iStartRecordNo, 
			&iRecords, 
			(void*)pHisDataRecord, 
			0,
			FALSE);
#ifdef _SHOW_WEB_INFO
		//TRACE("HIS_DATA_RECORD : iResult[%d] : iRecords[%d]\n", iResult, iRecords);
#endif

		DELETE(pCondition);
		pCondition = NULL;

		if(iResult >= 1 && iRecords > 0)
		{
			//return (HIS_DATA_RECORD *)pHisDataRecord;
			*ppBuf = (void *)pHisDataRecord;
			DAT_StorageClose(hHisData);
			return iRecords;
		}
		else
		{
			DELETE(pHisDataRecord);
			pHisDataRecord = NULL;
			DAT_StorageClose(hHisData);
			return FALSE;

		}
	}

}

static int Web_GetRectSNIndex(void)
{
	SAMPLE_SIG_INFO		*pSampleSigInfo;
	int	iBufLen = 0;

	int	iError = DxiGetData(VAR_A_SIGNAL_INFO_THRO_STD_EQUIP,
		RECT_STD_EQUIP_ID,			
		SIG_ID_RECT_SN,		
		&iBufLen,			
		(void *)&pSampleSigInfo,			
		0);
	if (iError == ERR_DXI_OK)
	{
		return (pSampleSigInfo->iValueDisplayID - 1);
	}

	return -1;

}

static int Web_GetRectHighSNIndex(void)
{
	SAMPLE_SIG_INFO		*pSampleSigInfo;
	int	iBufLen = 0;

	int	iError = DxiGetData(VAR_A_SIGNAL_INFO_THRO_STD_EQUIP,
		RECT_STD_EQUIP_ID,			
		SIG_ID_RECT_HI_SN,		
		&iBufLen,			
		(void *)&pSampleSigInfo,			
		0);
	if (iError == ERR_DXI_OK)
	{
		return (pSampleSigInfo->iValueDisplayID - 1);
	}

	return -1;
}

/*==========================================================================*
* FUNCTION :  Web_GetEquipListFromInterface
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  OUT EQUIP_INFO *pEquipInfo
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:15
*==========================================================================*/
static int Web_GetEquipListFromInterface(OUT EQUIP_INFO **ppEquipInfo)
{
	int				iError = 0;
	EQUIP_INFO		*pEquipInfo = NULL;
	int				iSBufLen = 0;
	int				iLen = 0;

	iError = DxiGetData(VAR_ACU_EQUIPS_LIST,
		0,			
		0,		
		&iSBufLen,			
		&pEquipInfo,			
		0);

	iLen = iSBufLen/sizeof(EQUIP_INFO);

	if (iError == ERR_DXI_OK   )
	{
		*ppEquipInfo = pEquipInfo;

		return iLen;
	}
	else
	{
		return FALSE;
	}
}

/*==========================================================================*
* FUNCTION :  Web_GetEquipNameFromInterface
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN int iEquipID:
IN int iLanguage:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:35
*==========================================================================*/
static char *Web_GetEquipNameFromInterface(IN int iEquipID,IN int iLanguage)
{

	int		i, iEquipNum = 0;
	char	*szEquipName = NULL;
	EQUIP_INFO		*pEquipInfo = NULL;
	EQUIP_INFO		*pTEquipInfo = NULL;
	//char		szDisBuf[128];
	static	int			iRectSNIndex = -1;
	static	int			iRectHighSNIndex = -1;
	int	iVarSubID = 0;
	int	iBufLen = 0;
	int	iTimeOut = 0;

	if(iRectSNIndex == -1)
	{
		iRectSNIndex = Web_GetRectSNIndex();
	}

	if(iRectHighSNIndex == -1)
	{
		iRectHighSNIndex = Web_GetRectHighSNIndex();
	}

	if(Web_GetEquipListFromInterface(&pEquipInfo) <= 0)
	{
		return NULL;
	}
	int iError = DxiGetData(VAR_ACU_EQUIPS_NUM,
		0,			
		iVarSubID,		
		&iBufLen,			
		(void *)&iEquipNum,			
		iTimeOut);

	szEquipName = NEW(char, 64);


	if(szEquipName == NULL)
	{
		return NULL;
	}
	memset(szEquipName, 0, 64);

	pTEquipInfo = pEquipInfo;
	if(iError == ERR_DXI_OK)
	{
		for(i = 0; i < iEquipNum && pTEquipInfo != NULL; i++, pTEquipInfo++)
		{
			if(iEquipID == pTEquipInfo->iEquipID)
			{
				{
					strncpyz(szEquipName, pTEquipInfo->pEquipName->pFullName[iLanguage], 64);
				}
				return szEquipName;
			}
		}
	}
	else
	{
		SAFELY_DELETE(szEquipName);
		return NULL;
	}
	return NULL;
}

BOOL ConvertTime(time_t* pTime, BOOL bUTCToLocal)
{
	SIG_BASIC_VALUE* pSigValue;

	int nBufLen;

	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		DXI_GetEquipIDFromStdEquipID(STD_ID_ACU_SYSTEM_EQUIP),			
		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 25),
		&nBufLen,			
		&pSigValue,			
		0) != ERR_DXI_OK)
	{	
		//TRACE("FALSE\n");
		return FALSE;
	}

	ASSERT(pSigValue);	

	if(SIG_VALUE_IS_VALID(pSigValue))
	{
		int nTimeOffset = (pSigValue->varValue.lValue) * 3600 / 2;

		*pTime = 0;
	}
	else
	{
		return FALSE;
	}

	return TRUE;
}

/*==========================================================================*
* FUNCTION :    Web_TransferHisDataIDToName
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   IN void *pBuf:
IN int iDataLen:
IN int iLanguage:
OUT void **ppBuf:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
*==========================================================================*/
static int Web_TransferHisDataIDToName(IN void *pBuf,
				       IN int iDataLen,
				       IN int iLanguage,
				       OUT void **ppBuf)
{
	char					*pszEquipName	= NULL;
	char					*pszSignalName	= NULL;
	HIS_DATA_RECORD			*pHisDataRecord = NULL;
	HIS_DATA_TO_RETURN		*pDataReturn	= NULL;
	char					*szUnit = NULL, *szState = NULL;
	int						iValueType = 0;
	char					szTime1[32], *pTime = NULL;
	time_t					tmUTC;

	/*get equipment alarm info by ID*/
	if(pBuf == NULL)
	{
#ifdef _SHOW_WEB_INFO
		//TRACE("Web_TransferHisDataIDToName pBuf : FALSE\n");
#endif 

		return FALSE;
	}

	pHisDataRecord = (HIS_DATA_RECORD *)pBuf;

	pDataReturn = NEW(HIS_DATA_TO_RETURN, iDataLen);
	if(pDataReturn == NULL)
	{
		return FALSE;
	}
	memset(pDataReturn,0x0, (size_t)iDataLen);

	/*get signal name*/
	*ppBuf  = (void *)pDataReturn;
	while(pHisDataRecord != NULL && iDataLen > 0)
	{
#ifdef _SHOW_WEB_INFO
		//TRACE("iDataLen : %d\n", iDataLen);
#endif 

		pszEquipName = Web_GetEquipNameFromInterface((int)pHisDataRecord->iEquipID,
			iLanguage); 

#ifdef _SHOW_WEB_INFO
		//TRACE("pHisDataRecord->iEquipID : [%d]\n", pHisDataRecord->iEquipID);
#endif 

		if(pszEquipName != NULL)
		{
			strncpyz(pDataReturn->szEquipName,pszEquipName,sizeof(pDataReturn->szEquipName));
			DELETE(pszEquipName);
			pszEquipName = NULL;
		}
		else
		{
			strncpyz(pDataReturn->szEquipName,"--",sizeof(pDataReturn->szEquipName));
		}

		pszSignalName =  Web_GetASignalName(LOWORD(pHisDataRecord->iSignalID),
			HIWORD(pHisDataRecord->iSignalID),
			pHisDataRecord->iEquipID,
			iLanguage,
			&szUnit,
			pHisDataRecord->varSignalVal.enumValue,//-1,
			&szState,
			&iValueType);
		if(pszSignalName != NULL)
		{
			strncpyz(pDataReturn->szSignalName,pszSignalName,sizeof(pDataReturn->szSignalName));
			DELETE(pszSignalName);
			pszSignalName = NULL;
		}
		else
		{
			strncpyz(pDataReturn->szSignalName,"--",sizeof(pDataReturn->szSignalName));
		}

		pDataReturn->tSignalTime	= pHisDataRecord->tmSignalTime;

		//Get signal value
		if(iValueType == VAR_LONG)
		{
			sprintf(pDataReturn->szSignalVal,"%ld", pHisDataRecord->varSignalVal.lValue);
		}
		else if(iValueType == VAR_FLOAT)
		{
			sprintf(pDataReturn->szSignalVal,"%.2f", pHisDataRecord->varSignalVal.fValue);
		}
		else if(iValueType == VAR_UNSIGNED_LONG)
		{
			sprintf(pDataReturn->szSignalVal,"%lu", pHisDataRecord->varSignalVal.ulValue);
		}
		else if(iValueType == VAR_DATE_TIME)
		{
			//Conver to local time

			tmUTC = pHisDataRecord->varSignalVal.dtValue;
			ConvertTime(&tmUTC, TRUE);

			TimeToString(tmUTC, TIME_CHN_FMT, 
				szTime1, 32);
			//trim
			pTime = szTime1;
			pTime = pTime + 5;
			pTime[13] = '\0';

			//show
			strncpyz(pDataReturn->szSignalVal,pTime, sizeof(pDataReturn->szSignalVal));
		}
		else 
		{
			if(szState != NULL)
			{
				sprintf(pDataReturn->szSignalVal,"%-s", szState);//pHisDataRecord->varSignalVal.enumValue);

			}
			else
			{
				sprintf(pDataReturn->szSignalVal,"%s", "--");
			}
		}

		if(szState != NULL)
		{
			DELETE(szState);
			szState = NULL;
		}
		if(szUnit != NULL)
		{
			strncpyz(pDataReturn->szUnit, szUnit, sizeof(pDataReturn->szUnit));
			DELETE(szUnit);
			szUnit = NULL;
		}
		else
		{
			strncpyz(pDataReturn->szUnit, "--", sizeof(pDataReturn->szUnit));
		}

		pDataReturn->dwActAlarmID	= pHisDataRecord->iActAlarmID;

		pHisDataRecord++;
		pDataReturn++;

		iDataLen--;

	}


	return TRUE;
}

/*==========================================================================*
* FUNCTION :    Web_MakeQueryHisDataBuffer
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   IN int iEquipID:
IN time_t fromTime:
IN time_t toTime:
IN int iLanguage:
OUT char **ppBuf:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
*==========================================================================*/
static int Web_MakeQueryHisDataBuffer(IN int iEquipID, 
				      IN time_t fromTime, 
				      IN time_t toTime, 
				      IN int iLanguage, 
				      OUT char **ppBuf)
{
	int						iLen			= 0, iFileLen = 0;
	HIS_DATA_RECORD			*pReturnData	= NULL;
	HIS_DATA_TO_RETURN		*pTraiReturnData= NULL;
	HIS_DATA_TO_RETURN		*pDeletePtr		= NULL;
	char					*pMakeBuf		= NULL, *pMakeFileBuf = NULL;
	int						iBufLen			= 0;
	int						iDataLen		= 0;
	char					szTime[32], szTime2[32];

	if((iBufLen = Web_QueryHisData1(toTime,fromTime, iEquipID, 
		QUERY_HIS_DATA, (void *)&pReturnData)) <= 0 )
	{
#ifdef _SHOW_WEB_INFO
		//TRACE("Web_MakeQueryHisDataBuffer :iBufLen : %d\n", iBufLen);
#endif 
		DELETE(pReturnData);
		pReturnData = NULL;

		return FALSE;
	}

	iDataLen = iBufLen * (MAX_HIS_DATA_LEN + 50);

	pMakeBuf = NEW(char,iDataLen);
	if(pMakeBuf == NULL)
	{
		DELETE(pReturnData);
		pReturnData = NULL;
		return FALSE;
	}
	memset(pMakeBuf, 0x0, (size_t)iDataLen);

	//Special buffer for make file 
	pMakeFileBuf = NEW(char, iDataLen);
	if(pMakeFileBuf == NULL)
	{
		DELETE(pReturnData);
		pReturnData = NULL;
		return FALSE;
	}
	memset(pMakeFileBuf, 0, (size_t)iDataLen);


	if(Web_TransferHisDataIDToName(pReturnData,iBufLen, iLanguage,
		(void *)&pTraiReturnData) == FALSE)
	{
#ifdef _SHOW_WEB_INFO
		//TRACE("Web_TransferHisDataIDToName :FALSE\n");
#endif 
		DELETE(pReturnData);
		pReturnData = NULL;

		DELETE(pMakeBuf);
		pMakeBuf = NULL;

		return FALSE;
	}

	DELETE(pReturnData);
	pReturnData = NULL;

	pDeletePtr = pTraiReturnData;

	iLen += sprintf(pMakeBuf + iLen,"%2d", 1);
	int i = 1;
	int j = 500;

	//remove the /var/SettingParam.tar in case /var/ is not enough
	char szCommandStr[128];
	sprintf(szCommandStr, "rm  %s *.tar -f", SET_PARA_tar_FILE);
	_SYSTEM(szCommandStr);
	iLen += sprintf(pMakeBuf + iLen, "[");
	while(pTraiReturnData != NULL && iBufLen > 0)
	{

		TimeToString(pTraiReturnData->tSignalTime, TIME_CHN_FMT, 
			szTime, sizeof(szTime));
		if(j > 0)
		{
			iLen += sprintf(pMakeBuf + iLen, "[\"%16s\",\"%32s\",\"%32s\",\"%8s\",%ld],", 
				pTraiReturnData->szEquipName, 
				pTraiReturnData->szSignalName,
				pTraiReturnData->szSignalVal, 
				pTraiReturnData->szUnit,
				pTraiReturnData->tSignalTime);//,szTime,
			//pTraiReturnData->dwActAlarmID );
			j--;

		}

		iFileLen += sprintf(pMakeFileBuf + iFileLen, "\t<tr><td>%d</td> \t<td>%-32s</td> \t<td>%-32s</td>  \t<td>%-32s</td> \t<td>%-8s</td> \t<td>%-32s</td></tr>\t\n", 
			i,
			pTraiReturnData->szEquipName, 
			pTraiReturnData->szSignalName,
			pTraiReturnData->szSignalVal, 
			pTraiReturnData->szUnit,
			szTime);

		pTraiReturnData++;
		iBufLen--;
		i++;
	}
	if(iLen >= 1)
	{
		iLen = iLen - 1;	// ȥ�����һ�������Է���JSON��ʽ
	}
	iLen += sprintf(pMakeBuf + iLen, "]");

	TRACE_WEB_USER("pMakeBuf is %s\n", pMakeBuf);

	*ppBuf = pMakeBuf;
#ifdef _SHOW_WEB_INFO
	//TRACE("********[Web_MakeQueryHisDataBuffer]******%s\n", pMakeBuf);
#endif 
	int iAccess;
	iAccess = access(WEB_LOG_DIR_DL, R_OK|W_OK);

	FILE *fp;
	if((fp = fopen(WEB_LOG_DIR_DL,"wb")) != NULL && iFileLen > 1)
	{
		char	szFileTitle[1024];

		TimeToString(fromTime, TIME_CHN_FMT, 
			szTime, sizeof(szTime));

		TimeToString(toTime, TIME_CHN_FMT, 
			szTime2, sizeof(szTime2));

		sprintf(szFileTitle, "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"\
		    "<head><meta	http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /></head><body>\n"\
		    "<style>body,html{padding:10px;margin:0;font-size:14px;line-height:25px;font-family:Arial;}table{border-left:#ddd solid 1px;border-top:#ddd solid 1px;margin-top:20px;}table td{border-right:#ddd solid 1px;border-bottom:#ddd solid 1px;height:30px;line-height:30px;padding:0 0 0 10px;font-size:14px;color:#333}table tr:first-child td{font-weight:bold;height:40px;line-height:40px;}</style>\n"\
		    "Query Data History Log<br/>\n"\
		    "Query EquipID: %s <br/>\n"\
		    "Query Time: from %s to %s<br/>\n"\
		    "Total %d record(s) queried.<br/>\n"\
		    "<table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n"\
		    "<tr><td>%s</td> \t<td>%-32s</td> \t<td>%-32s</td> \t<td>%-32s</td> \t<td>%-8s</td> \t<td>%-32s</td></tr> \t\n",
			(iEquipID > 0) ? Web_GetAEquipName(iEquipID, iLanguage) : "All Devices",
			szTime,
			szTime2,
			i-1,
			"Index",
			"Device Name", 
			"Signal Name", 
			"Value", 
			"Unit",
			"Time");

		fwrite(szFileTitle,strlen(szFileTitle), 1, fp);
		fwrite(pMakeFileBuf,strlen(pMakeFileBuf), 1, fp);
		memset(szFileTitle, 0, sizeof(szFileTitle));
		sprintf(szFileTitle, "</table></body>\n" \
		    "</html>\n");
		fwrite(szFileTitle,strlen(szFileTitle), 1, fp);		
		fclose(fp);

		if(iAccess < 0)
		{
			char szCommandStr[128];
			sprintf(szCommandStr, "chmod 777 %s", WEB_LOG_DIR_DL);
			_SYSTEM(szCommandStr);
		}


	}

	//Web_MakeHisData();

	DELETE(pMakeFileBuf);
	pMakeFileBuf = NULL;

	DELETE(pDeletePtr);
	pDeletePtr = NULL;
	return TRUE;
}

/*==========================================================================*
* FUNCTION :    Web_QueryControlCommand
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN time_t fromTime:
IN time_t toTime:
OUT char **ppBuf:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
*==========================================================================*/
static int Web_QueryControlCommand(IN time_t fromTime,
				   IN time_t toTime, 
				   OUT void **ppBuf)
{

	int			iResult = 0, iStartRecordNo = -1;		//record number
	HANDLE		hHisCommand;
	int			iBufLen = MAX_CTRL_CMD_COUNT;//400;

	hHisCommand =	DAT_StorageOpen(CTRL_CMD_LOG);

	if(hHisCommand == NULL)
	{
		AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_WARNING,"Fail to open HisCommand");
		return FALSE;
	}

	HIS_CONTROL_RECORD *pHisCommandRecord	= NEW(HIS_CONTROL_RECORD, iBufLen);
	HIS_GENERAL_CONDITION stCondition;

	stCondition.tmToTime	= toTime;
	stCondition.tmFromTime	= fromTime;
	stCondition.iEquipID	= -1;	//all equipment 
	stCondition.iDataType	= QUERY_CONTROLCOMMAND_DATA;



	if(pHisCommandRecord == NULL )
	{
		DAT_StorageClose(hHisCommand);
		return FALSE;
	}
	int			iRecords = MAX_CTRL_CMD_COUNT;//400;
	iResult = DAT_StorageFindRecords(hHisCommand,
		Web_fnCompareQueryCondition1, 
		(void *)&stCondition,
		&iStartRecordNo, 
		&iRecords, 
		(void*)pHisCommandRecord, 
		0,
		FALSE);

#ifdef _SHOW_WEB_INFO
	//TRACE("iResult : %d  iRecords :[%d]  iStartRecordNo[%d]\n", iResult, iRecords,iStartRecordNo);
#endif

	if(iResult >= 1 && iRecords >= 1)
	{
		*ppBuf = (void *)pHisCommandRecord;
		DAT_StorageClose(hHisCommand);
		return iRecords;
	}
	else
	{
		DELETE(pHisCommandRecord);
		pHisCommandRecord = NULL;
		DAT_StorageClose(hHisCommand);
		return FALSE;
	}
}

static void Web_TransferGetValue(IN char *szCtrlCmd, 
				 OUT int *iChannel, 
				 OUT char *szValue, 
				 OUT int *iSave)
{
	ASSERT(szCtrlCmd);
	char	*pSearchValue = NULL;
	char	*pTempCtrlCmd = szCtrlCmd;
	char	szBuffer[32];
	int		iPosition = 0;

	if(pTempCtrlCmd != NULL)
	{
		pSearchValue = strchr(pTempCtrlCmd, ',');
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - pTempCtrlCmd;

			if(iPosition > 0)
			{
				strncpyz(szBuffer, pTempCtrlCmd, iPosition + 1);
				pTempCtrlCmd = pTempCtrlCmd + iPosition;
				*iChannel = atoi(szBuffer);
			}

		}
		pTempCtrlCmd =  pTempCtrlCmd + 1;

		pSearchValue = strchr(pTempCtrlCmd, ',');
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - pTempCtrlCmd;

			if(iPosition > 0)
			{
				strncpyz(szValue, pTempCtrlCmd, iPosition + 1);
				pTempCtrlCmd = pTempCtrlCmd + iPosition;

			}

		}
		pTempCtrlCmd =  pTempCtrlCmd + 1;


		//strncpyz(szBuffer, pTempCtrlCmd, (int)strlen(pTempCtrlCmd) + 1);
		strncpyz(szBuffer, pTempCtrlCmd, sizeof(szBuffer));//(int)strlen(pTempCtrlCmd) + 1);
		*iSave = atoi(szBuffer);


	}
}

/*==========================================================================*
* FUNCTION :   Web_TransferControlIDToName 
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   IN HIS_CONTROL_RECORD *pBuf:
IN int iDataLen:
IN int iLanguage,OUT void **ppBuf:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
*==========================================================================*/
static int Web_TransferControlIDToName(IN HIS_CONTROL_RECORD *pBuf,
				       IN int iDataLen,
				       IN int iLanguage,
				       OUT void **ppBuf)
{
	char						*pszEquipName	= NULL;
	char						*pszSignalName	= NULL;
	HIS_CONTROL_TO_RETURN		*pDataReturn	= NULL;
	HIS_CONTROL_RECORD			*pControlRecord = NULL;
	char						*szUnit = NULL;
	int							iChannel = -1, iValue = -1,iSave = -1;
	//char						szValue[32];
	char						*szStatue = NULL;
	char						szValue[64];
	int							iValueType = 0;
	time_t						tmUTC;

	char						szTime1[32], *pTime = NULL;
	ASSERT(pBuf);
	if(pBuf == NULL)
	{

		////TRACE("pBuf : false");
		return FALSE;
	}

	pControlRecord = (HIS_CONTROL_RECORD *)pBuf;

	/*init*/
	pDataReturn = NEW(HIS_CONTROL_TO_RETURN,iDataLen);
	if(pDataReturn == NULL)
	{
		////TRACE("pDataReturn : false");
		return FALSE;
	}
	memset(pDataReturn, 0x0, (size_t)iDataLen);

	*ppBuf  = (void *)pDataReturn;
	while(pControlRecord != NULL && iDataLen > 0)
	{

		/*Transfer equip name*/
		pszEquipName = Web_GetEquipNameFromInterface_New(pControlRecord->iEquipID, iLanguage, pControlRecord->iPositionID); 
		if(pszEquipName != NULL)
		{
			strncpyz(pDataReturn->szEquipName, pszEquipName,sizeof(pDataReturn->szEquipName));
			DELETE(pszEquipName);
			pszEquipName = NULL;
		}
		else
		{
			strncpyz(pDataReturn->szEquipName, "--",sizeof(pDataReturn->szEquipName));
		}


		//sscanf(pControlRecord->szCtrlCmd, "%d,%d,%d", &iChannel, &iValue, &iSave);
		Web_TransferGetValue(pControlRecord->szCtrlCmd, &iChannel,szValue,&iSave);

#ifdef _SHOW_WEB_INFO
		//TRACE("iChannel[%d] szValue[%s] iSave[%d]\n", iChannel, szValue, iSave);
#endif 

		/*Transfer signal name*/
		////{{ new code. maofuhua, 2005-5-10
		szStatue = NULL;
		iValue = atoi(szValue);

		pszSignalName = Web_GetASignalName(LOWORD(pControlRecord->iSignalID),		//Signal ID
			HIWORD(pControlRecord->iSignalID),		//Signal type
			pControlRecord->iEquipID,
			iLanguage,
			&szUnit,
			iValue,
			&szStatue,
			&iValueType);



		if(pszSignalName != NULL )
		{
			strncpyz(pDataReturn->szSignalName, 
				pszSignalName,
				sizeof(pDataReturn->szSignalName));
			DELETE(pszSignalName);
			pszSignalName = NULL;
		}
		else
		{
			strncpyz(pDataReturn->szSignalName, 
				"--",
				sizeof(pDataReturn->szSignalName));
		}

		pDataReturn->iValueType = 0; 
		if(szStatue != NULL)
		{
			strncpyz(pDataReturn->szControlVal, szStatue, sizeof(pDataReturn->szControlVal));
			strncpyz(pDataReturn->szControlValForSave, szStatue, sizeof(pDataReturn->szControlValForSave));
			DELETE(szStatue);
			szStatue = NULL;
		}
		else if(iValueType == VAR_DATE_TIME)
		{


			pDataReturn->iValueType = 1;

			tmUTC = (time_t)atoi(szValue);

			TimeToString(tmUTC, TIME_CHN_FMT, 
				szTime1, sizeof(szTime1));
			//trim
			pTime = szTime1;
			pTime = pTime + 5;
			pTime[8] = '\0';

			//show
			strncpyz(pDataReturn->szControlValForSave,pTime, sizeof(pDataReturn->szControlValForSave));
			strncpyz(pDataReturn->szControlVal,szValue, sizeof(pDataReturn->szControlVal));

		}
		else if(iValueType == VAR_FLOAT)
		{
			sprintf(pDataReturn->szControlValForSave, "%.2f", atof(szValue));
			sprintf(pDataReturn->szControlVal, "%8.2f", atof(szValue));
		}
		else
		{
			sprintf(pDataReturn->szControlValForSave, "%-s", szValue);
			sprintf(pDataReturn->szControlVal, "%s", szValue);
		}


		pDataReturn->tmControlTime	= pControlRecord->tmControlTime;
		//pDataReturn->fControlVal	= pControlRecord->varControlVal.fValue;	
		//strncpyz(pDataReturn->szControlVal, pControlRecord->szCtrlCmd, sizeof(pDataReturn->szControlVal));
		if(szUnit != NULL)
		{
			strncpyz(pDataReturn->szUnit, szUnit, sizeof(pDataReturn->szUnit));
			DELETE(szUnit);
			szUnit = NULL;
		}
		else
		{
			strncpyz(pDataReturn->szUnit, "--", sizeof(pDataReturn->szUnit));
		}

		/*redefine that store name , not ID */
		strncpyz(pDataReturn->szCtrlSenderName, 
			pControlRecord->szSenderName, 
			sizeof(pDataReturn->szCtrlSenderName));
		strncpyz(pDataReturn->szSenderType, 
			szUserInfo[pControlRecord->bySenderType - 1], 
			sizeof(pDataReturn->szSenderType));
		//strncpyz(pDataReturn->szControlResult, 
		//		szControlResult[pControlRecord->nControlResult], 
		//		 sizeof(pDataReturn->szControlResult));
		pDataReturn->iControlResult = pControlRecord->nControlResult;

		pControlRecord++;
		pDataReturn++;
		iDataLen--;

	}

	return TRUE;
}

/*==========================================================================*
* FUNCTION :    Web_MakeQueryControlDataBuffer
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   IN time_t fromTime:
IN int toTime:
IN int iLanguage:
OUT char **ppBuf:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
*==========================================================================*/
static int Web_MakeQueryControlDataBuffer(IN time_t fromTime , 
					  IN time_t toTime, 
					  IN int iLanguage, 
					  OUT char **ppBuf)
{
	int							iLen = 0, iFileLen = 0;
	HIS_CONTROL_RECORD			*pReturnData		= NULL;
	HIS_CONTROL_TO_RETURN		*pTraiReturnData	= NULL;
	char						*pMakeBuf			= NULL, *pMakeFileBuf = NULL;
	int							iBufLen = 0;
	int							iDataLen = 0;
	HIS_CONTROL_TO_RETURN		*pDeletePtr = NULL;
	char						szTime[32],szTime2[32];


	if((iBufLen = Web_QueryControlCommand(fromTime, toTime, 
		(void **)&pReturnData)) <= 0)
	{
		return FALSE;
	}

	iDataLen = iBufLen * (MAX_CONTROL_DATA_LEN + 50);	
	pMakeBuf = NEW(char,iDataLen);
	if(pMakeBuf == NULL)
	{
		DELETE(pReturnData);
		pReturnData = NULL;
		return FALSE;
	}

	memset(pMakeBuf, 0, (size_t)iDataLen);

	//Special buffer for make file 
	pMakeFileBuf = NEW(char, iDataLen);
	if(pMakeFileBuf == NULL)
	{
	    DELETE(pReturnData);
	    pReturnData = NULL;
	    DELETE(pMakeBuf);
	    pMakeBuf = NULL;
	    return FALSE;
	}
	memset(pMakeFileBuf, 0, (size_t)iDataLen);

	if(Web_TransferControlIDToName((void *)pReturnData, iBufLen, iLanguage, 
		(void *) &pTraiReturnData) == FALSE)
	{
		//TRACE("Web_TransferControlIDToName false\n");
		DELETE(pReturnData);
		pReturnData = NULL;
		DELETE(pMakeBuf);
		pMakeBuf = NULL;
		DELETE(pMakeFileBuf);
		pMakeFileBuf = NULL;
		return FALSE;
	}


	DELETE(pReturnData);
	pReturnData = NULL;

	pDeletePtr = pTraiReturnData;

	//remove the /var/SettingParam.tar in case /var/ is not enough
	char szCommandStr[128];
	sprintf(szCommandStr, "rm  %s *.tar -f", SET_PARA_tar_FILE);
	_SYSTEM(szCommandStr);

	iLen += sprintf(pMakeBuf + iLen,"%2d", 1);
	int		i = 1;
	iLen += sprintf(pMakeBuf + iLen, "[");
	while(pTraiReturnData != NULL && iBufLen > 0)
	{
		TimeToString(pTraiReturnData->tmControlTime, TIME_CHN_FMT, 
			szTime, sizeof(szTime));

		iLen += sprintf(pMakeBuf + iLen,"[\"%s\",\"%s\",\"%s\",\"%8s\",%ld,\"%s\",\"%-s\",%d],",
			pTraiReturnData->szEquipName,
			pTraiReturnData->szSignalName,
			//pTraiReturnData->iValueType,
			pTraiReturnData->szControlValForSave,
			pTraiReturnData->szUnit,
			pTraiReturnData->tmControlTime,//szTime,
			pTraiReturnData->szCtrlSenderName,
			pTraiReturnData->szSenderType,
			pTraiReturnData->iControlResult);

		iFileLen += sprintf(pMakeFileBuf + iFileLen,"<tr><td>%8d</td> \t<td>%-32s</td> \t<td>%-32s</td> \t<td>%-32s</td> \t<td>%-8s</td> \t<td>%-32s</td> \t<td>%-32s</td> \t<td>%-32s</td></tr> \n",
		    i,
		    pTraiReturnData->szEquipName,
		    pTraiReturnData->szSignalName,
		    pTraiReturnData->szControlValForSave,
		    pTraiReturnData->szUnit,
		    szTime,
		    pTraiReturnData->szCtrlSenderName,
		    pTraiReturnData->szSenderType/*,
		    (pTraiReturnData->iControlResult == 0) ? "Successful" :"Failure"*/);

		pTraiReturnData++;
		iBufLen--;
		i++;
	}
	if(iLen >= 1)
	{
		iLen = iLen - 1;	// ȥ�����һ�������Է���JSON��ʽ
	}
	iLen += sprintf(pMakeBuf + iLen, "]");

	TRACE_WEB_USER("pMakeBuf is %s\n", pMakeBuf);
	*ppBuf = pMakeBuf;
#ifdef _SHOW_WEB_INFO
	//TRACE("********[Web_MakeQueryControlDataBuffer]******%s\n", pMakeBuf);
#endif 

	int iAccess;
	iAccess = access(WEB_LOG_DIR_EL, R_OK|W_OK);

	FILE *fp;
	if((fp = fopen(WEB_LOG_DIR_EL,"wb")) != NULL && iFileLen > 1)
	{
		char	szFileTitle[1024];
		TimeToString(fromTime, TIME_CHN_FMT, 
			szTime, sizeof(szTime));

		TimeToString(toTime, TIME_CHN_FMT, 
			szTime2, sizeof(szTime2));

		sprintf(szFileTitle, "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" \
		    "<head><meta	http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /></head><body>\n" \
		    "<style>body,html{padding:10px;margin:0;font-size:14px;line-height:25px;font-family:Arial;}table{border-left:#ddd solid 1px;border-top:#ddd solid 1px;margin-top:20px;}table td{border-right:#ddd solid 1px;border-bottom:#ddd solid 1px;height:30px;line-height:30px;padding:0 0 0 10px;font-size:14px;color:#333}table tr:first-child td{font-weight:bold;height:40px;line-height:40px;}</style>\n"\
		    "Query Event Log<br/>\n" \
			"Query Time: from %s to %s<br/>\n"\
			"Total %d record(s) queried.<br/>\n " \
			"<table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n" \
			"\n<tr><td>%8s</td> \t<td>%-32s</td> \t<td>%-32s</td> \t<td>%-32s</td> \t<td>%-8s</td> \t<td>%-32s</td> \t<td>%-32s</td> \t<td>%-32s</td></tr>\n",
			szTime,
			szTime2,
			i-1,
			"Index",
			"Device Name", 
			"Signal Name", 
			"Value", 
			"Unit",
			"Time",
			"Sender Name",
			"Sender Type"/*,
			"Send Result"*/);

		fwrite(szFileTitle, strlen(szFileTitle),1, fp);
		fwrite(pMakeFileBuf,strlen(pMakeFileBuf), 1, fp);
		memset(szFileTitle, 0, sizeof(szFileTitle));
		sprintf(szFileTitle, "</table></body>\n" \
		    "</html>\n");
		fwrite(szFileTitle,strlen(szFileTitle), 1, fp);
		fclose(fp);
		if(iAccess < 0)
		{
			char szCommandStr[128];
			sprintf(szCommandStr, "chmod 777 %s", WEB_LOG_DIR_EL);
			_SYSTEM(szCommandStr);
		}
	}

	DELETE(pMakeFileBuf);
	pMakeFileBuf = NULL;


	DELETE(pDeletePtr);
	pDeletePtr = NULL;

	return TRUE;
}



//changed by Frank Wu,9/N/14,20140527, for system log
#define	LEN_INFO_LEVEL	6
#define	LEN_LOG_TIME	17
#define	LEN_RECORDER	12
#define	LEN_THREAD_ID	8

/*==========================================================================*
 * FUNCTION :  Web_AnalyzeHisLogLine
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN char *szReadLine:
				OUT HIS_READ_LOG *szAnalyzeString:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
 *==========================================================================*/


static time_t Web_AnalyzeHisLogLine(IN char *szReadLine, 
									OUT HIS_MATCH_LOG *szAnalyzeString)
{
	ASSERT(szReadLine);
	int			iPostition = 0;
	char		*pSearchValue = NULL;
	struct tm	p_tm;
	char		szTempBuf[10];
	char		szTimeStr[32];

	/*InfoLevel*/
	strncpyz(szAnalyzeString->szInfoLevel, szReadLine, LEN_INFO_LEVEL);
	szReadLine = szReadLine + LEN_INFO_LEVEL;

	/*time*/
	pSearchValue = strchr(szReadLine, ' ');
	pSearchValue = pSearchValue + 1;
	pSearchValue = strchr(pSearchValue, ' ');
	iPostition = pSearchValue - szReadLine;

	//printf("iPostition = %d\n", iPostition);
	if(iPostition == LEN_LOG_TIME)
	{
		//Transfer time
		strncpyz(szTimeStr, szReadLine, iPostition + 1);
		strptime(szTimeStr, "%y-%m-%d %H:%M:%S", &p_tm);
		szAnalyzeString->tmLogTime = mktime(&p_tm);
	}
	else
	{
		szAnalyzeString->tmLogTime = -1;	
	}
	szReadLine += LEN_LOG_TIME + 1;
	
	/*TaskName*/
	strncpyz(szAnalyzeString->szTaskName, szReadLine, LEN_RECORDER);
	szReadLine += LEN_RECORDER + 1;

	/*Run thread ID*/
	//strncpyz(szTempBuf, szReadLine, LEN_THREAD_ID + 1);
	//szAnalyzeString->RunThread_GetId = atol(szTempBuf);
	szReadLine += LEN_THREAD_ID + 1;

	while((pSearchValue = strchr(szReadLine, 34)) != NULL )//delete "
	{
		*pSearchValue = 39;// '
	}

	while((pSearchValue = strrchr(szReadLine, '\n')) != NULL || (pSearchValue = strrchr(szReadLine, '\r')) != NULL)
	{
		*pSearchValue = ' ';
	}
	 
	strncpyz(szAnalyzeString->szInformation, szReadLine,(int)sizeof(szAnalyzeString->szInformation));

	return TRUE;
}


/*==========================================================================*
* FUNCTION :    Web_QueryHisLog
* PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   IN time_t fromTime:
				IN time_t toTime:
				OUT void **ppHisLog:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
 *==========================================================================*/

static int Web_QueryHisLog(IN time_t fromTime, 
						   IN time_t toTime, 
						   OUT void **ppHisLog)
{
	//UNUSED(APP_LOG_FILE);
	char			*szReadLog = NULL, *szReadTempLog = NULL;
	int				iStartRecordNum = 1;
	int				iRecords = MAX_LOG_RECORD_COUNT;
	int				iRecordSize = (int)sizeof(LOG_RECORD_SIZE);
	char			szReadString[iRecordSize + 1];
	int				i, iMatchRecord = 0;
	time_t			tmReturn = 0;
	HIS_READ_LOG	pLog;
	HIS_MATCH_LOG	*pMatchLog = NULL, *pTMatchLog = NULL;

	szReadLog = NEW(char, MAX_LOG_RECORD_COUNT * iRecordSize);
	ASSERT(szReadLog);

	if(szReadLog == NULL)
	{
		return FALSE;
	}

	memset(szReadLog, 0x0, (size_t)MAX_LOG_RECORD_COUNT * iRecordSize);

	time_t		the_time,end_time;
	the_time = time((time_t *)0);
	TRACE("\n__The Time 1 is:%ld___\n",the_time);
	int		iResult = LOG_StorageReadRecords(&iStartRecordNum,
											&iRecords,
											(void*)szReadLog,
											FALSE,
											TRUE);
	end_time = time((time_t *)0);
	TRACE("\n__The Time 2 is:%ld___\n",end_time);
	TRACE("\n__Elapsed Time is:%lf__\n",difftime(end_time,the_time));

	pMatchLog = NEW(HIS_MATCH_LOG, iRecords);
	ASSERT(pMatchLog);

	the_time = time((time_t *)0);
	TRACE("\n__The Time 1 is:%ld___\n",the_time);
	if(pMatchLog != NULL)
	{
		pTMatchLog = pMatchLog;
		if(iResult == TRUE)
		{
			szReadTempLog = szReadLog;
			for(i = 0; i < iRecords && szReadTempLog != NULL; i++)
			{	
				memset(szReadString, 0x0, (size_t)iRecordSize + 1);
		
				memcpy(szReadString, szReadTempLog, (size_t)iRecordSize);
				szReadTempLog = szReadTempLog + iRecordSize;
				Web_AnalyzeHisLogLine(szReadString, pMatchLog);
				
				if((pMatchLog->tmLogTime - fromTime) > 0 && (toTime - pMatchLog->tmLogTime) > 0 )
				{	
					pMatchLog++;
					iMatchRecord++;
				}
			

				if(i == iRecords/100)
				{
					RunThread_Heartbeat(RunThread_GetId(NULL));
				}
			}
			*ppHisLog = (void *)pTMatchLog;
		}
	}
	TRACE("**********I'm here!!!\n");
	
	SAFELY_DELETE(szReadLog);
	
	if(iMatchRecord <= 0 && pMatchLog != NULL)
	{
		SAFELY_DELETE(pMatchLog);
	}

	/*end_time = time((time_t *)0);
	TRACE_WEB_USER("\n__The Time 2 is:%ld___\n",end_time);
	TRACE_WEB_USER("\n__Elapsed Time is:%lf__\n",difftime(end_time,the_time));*/

	return iMatchRecord;
}


/*==========================================================================*
 * FUNCTION :    Web_MakeQueryHisLogBuffer
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   IN time_t fromTime:
				IN time_t toTime:
				IN char **ppBuf:
 * RETURN   :	TRUE or FALSE
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
 *==========================================================================*/
static int Web_MakeQueryHisLogBuffer(IN time_t fromTime,
									 IN time_t toTime,
									 IN char **ppBuf)
{
	int							iLen = 0,iFileLen = 0;
	HIS_MATCH_LOG				*pReturnData	= NULL;
	HIS_MATCH_LOG				*pDeletePtr		= NULL;
	char						*pMakeBuf		= NULL, *pMakeFileBuf = NULL;
	int							iMatchRecord = 0;
	char						szTime[32],szTime2[32];
	int							i = 0;
	time_t		the_time,end_time;
	
	/*Get Log*/
	if((iMatchRecord = Web_QueryHisLog(fromTime, toTime,(void *)&pReturnData)) <= 0)
	{
		TRACE("Web_QueryHisLog : FALSE");

		return FALSE;
	}
	
	pMakeBuf = NEW(char,iMatchRecord * MAX_DATA_LOG_LEN);
	pMakeFileBuf = NEW(char, iMatchRecord * MAX_DATA_LOG_LEN);
	if(pMakeBuf == NULL || pMakeFileBuf == NULL || pReturnData == NULL)
	{
		SAFELY_DELETE(pReturnData);
		SAFELY_DELETE(pMakeBuf);
		SAFELY_DELETE(pMakeFileBuf);

		return FALSE;
	}
	//remove the /var/SettingParam.tar in case /var/ is not enough
	char szCommandStr[128];
	sprintf(szCommandStr, "rm  %s *.tar -f", SET_PARA_tar_FILE);
	_SYSTEM(szCommandStr);

	pDeletePtr = pReturnData;
	//pReturnData = &pReturnData[iMatchRecord - 1];//from the tail to head, from new records to old records
	/*the_time = time((time_t *)0);
	TRACE_WEB_USER("\n__The Time 1 is:%ld___\n",the_time);*/
	iLen += snprintf(pMakeBuf + iLen, MAX_SPRINTF_LINE_LEN, "%2d", 1);
	iLen += snprintf(pMakeBuf + iLen, MAX_SPRINTF_LINE_LEN, "[");
	while(pReturnData != NULL && iMatchRecord > 0 )
	{
		TimeToString(pReturnData->tmLogTime, TIME_CHN_FMT, 
				szTime, sizeof(szTime));

		if(iMatchRecord <= 500)
		//if(i < 500)
		{
			iLen += snprintf(pMakeBuf + iLen, MAX_SPRINTF_LINE_LEN, "[\"%-32s\",\"%-16s\",\"%-32s\",\"%s\"],",
				pReturnData->szTaskName,
				pReturnData->szInfoLevel,
				szTime, 
				Cfg_RemoveWhiteSpace(pReturnData->szInformation));
		}

		iFileLen += snprintf(pMakeFileBuf + iFileLen, MAX_SPRINTF_LINE_LEN, "<tr><td>%8d</td> \t<td>%-32s</td> \t<td>%-16s</td> \t<td>%32s</td> \t<td>%s</td> </tr> \n",
				i + 1,
				pReturnData->szTaskName,
				pReturnData->szInfoLevel,
				szTime, 
				Cfg_RemoveWhiteSpace(pReturnData->szInformation));

		iMatchRecord--;
		pReturnData++;
		//pReturnData--;
		i++;
	}
	if(',' == pMakeBuf[iLen - 1])
	{
		iLen--;// ȥ�����һ�������Է���JSON��ʽ
	}
	iLen += snprintf(pMakeBuf + iLen, MAX_SPRINTF_LINE_LEN, "]");

	TRACE_WEB_USER("pMakeBuf is %s\n", pMakeBuf);

	int iAccess;
	iAccess = access(WEB_LOG_DIR_SL, R_OK|W_OK);

	FILE *fp;
	if((fp = fopen(WEB_LOG_DIR_SL,"wb")) != NULL)
	{
		char	szFileTitle[1024];

		TimeToString(fromTime, TIME_CHN_FMT, 
				szTime, sizeof(szTime));

		TimeToString(toTime, TIME_CHN_FMT, 
				szTime2, sizeof(szTime2));

		snprintf(szFileTitle, sizeof(szFileTitle), "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" \
		    "<head><meta	http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /></head><body>\n" \
		    "<style>body,html{padding:10px;margin:0;font-size:14px;line-height:25px;font-family:Arial;}table{border-left:#ddd solid 1px;border-top:#ddd solid 1px;margin-top:20px;}table td{border-right:#ddd solid 1px;border-bottom:#ddd solid 1px;height:30px;line-height:30px;padding:0 0 0 10px;font-size:14px;color:#333}table tr:first-child td{font-weight:bold;height:40px;line-height:40px;}</style>\n"	\			
		    "Query System Log<br/>\n" \
			"Query Time: from %s to %s<br/>\n"\
			"Total %d record(s) queried.<br/>\n " \
			"<table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n" \
			"\n<tr><td>%8s</td> \t<td>%-32s</td> \t<td>%-16s</td> \t<td>%-32s</td> \t<td>%s</td></tr>\n",
			szTime,
			szTime2,
			i,
			"Index",
			"Task Name", 
			"Info Level", 
			"Log Time", 
			"Information");
		
		fwrite(szFileTitle,strlen(szFileTitle), 1, fp);
		fwrite(pMakeFileBuf,strlen(pMakeFileBuf), 1, fp);
		memset(szFileTitle, 0, sizeof(szFileTitle));
		snprintf(szFileTitle, MAX_SPRINTF_LINE_LEN, "</table></body>\n</html>\n");
		fwrite(szFileTitle,strlen(szFileTitle), 1, fp);
		fclose(fp);
		if(iAccess < 0)
		{
		
			char szCommandStr[128];
			sprintf(szCommandStr, "chmod 777 %s", WEB_LOG_DIR_SL);
			system(szCommandStr);
		}
	}

	*ppBuf = pMakeBuf;
	pMakeBuf = NULL;

#ifdef _SHOW_WEB_INFO
	//TRACE("********[Web_MakeQueryHisLogBuffer]**[%s]****\n", pMakeBuf);
#endif 
	

	SAFELY_DELETE(pDeletePtr);
    SAFELY_DELETE(pMakeFileBuf);

	/*end_time = time((time_t *)0);
	TRACE_WEB_USER("\n__The Time 2 is:%ld___\n",end_time);
	TRACE_WEB_USER("\n__Elapsed Time is:%lf__\n",difftime(the_time,end_time));*/

	return TRUE;
}



static int Web_fnCompareBatteryCondition(IN const void *pRecord, 
					 IN const void *pCondition)
{
	/*Compare head*/
	UNUSED(pCondition);		
	return (stricmp(((BATTERY_HEAD_INFO *)pRecord)->szHeadInfo, 
		"Head of a Battery Test") == 0) ? TRUE : FALSE;
}

/*==========================================================================*
* FUNCTION :  transferBuftoBatteryGeneralInfo
* PURPOSE  :  Transfer the char string of baBuf to the struct of BATTERY_GENERAL_INFO
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  BatteryGeneralInfo: The struct saving the battery information
	      baBuf: the raw char string
	      iQtyBatt: the battery number
* RETURN   :  void
* COMMENTS :  This function is from <communicate.c>
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
#define MAX_NUM_TEMPERATURE 7
#define MAX_BLOCK_NUM 24
#define MAX_NUM_SMBAT_OR_SMBRC 20
#define MAX_NUM_EIB_EQUIP 4
#define MAX_NUM_OF_BLK_PER_EIB 8
void transferBuftoBatteryGeneralInfo(BATTERY_GENERAL_INFO *BatteryGeneralInfo, BYTE *baBuf, int iQtyBatt)
{
	int i, j;

	//TRACE_WEB_USER("iQtyBatt is %d\n", iQtyBatt);
	for(i = 0; i < MAX_NUM_TEMPERATURE; i++)
	{
		BatteryGeneralInfo->byTemperatureInfo[i] = *(baBuf + i);  //7 temperature
	}
	for(i = 0; i < iQtyBatt; i++)  // battery
	{
		BatteryGeneralInfo->BatteryInfo[i].byBatteryCurrent = *(baBuf + MAX_NUM_TEMPERATURE + i * 3 + 0);
		BatteryGeneralInfo->BatteryInfo[i].byBatteryVolage = *(baBuf + MAX_NUM_TEMPERATURE + i * 3 + 1);
		BatteryGeneralInfo->BatteryInfo[i].byBatteryCapacity = *(baBuf + MAX_NUM_TEMPERATURE + i * 3 + 2);
	}
	for(i = 0; i < MAX_NUM_SMBAT_OR_SMBRC; i++) // block voltage
	{
		for(j = 0; j < MAX_BLOCK_NUM; j++)
		{
			BatteryGeneralInfo->stSmbatSmbrcBlock[i].bySMBlockVoltage[j] = *(baBuf + MAX_NUM_TEMPERATURE + iQtyBatt * 3 + i * MAX_BLOCK_NUM + j);
		}
	}
	for(i = 0; i < MAX_NUM_EIB_EQUIP; i++)// EIB block voltage
	{
		for(j = 0; j < MAX_NUM_OF_BLK_PER_EIB; j++)
		{
			BatteryGeneralInfo->stBatteryBlock[i].byBlockVoltage[j] = *(baBuf + MAX_NUM_TEMPERATURE + iQtyBatt * 3 + MAX_NUM_SMBAT_OR_SMBRC * MAX_BLOCK_NUM + i * MAX_NUM_OF_BLK_PER_EIB + j);
		}
	}
	return;
}

#define FLOAT_EQUAL01(f)			((f < 0.1 && f >-0.1) ? 1 : 0)
/*==========================================================================*
* FUNCTION :    Web_ManageBatteryData
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   IN int iRecordNum:
IN int iQtyBatt:			Quantity of  battery
IN int iQtyRecord:				Quantity of record
IN void *pGeneralInfo: 
IN void *pBatteryData:
OUT BATTERY_LOG_INFO **ppBatteryRealData:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
*==========================================================================*/
static void Web_ManageBatteryData(IN int iRecordNum,				/*total record num*/
				  IN int iQtyBatt,					/*Quantity of  battery*/
				  IN int iQtyRecord,					/*Quantity of record*/
				  BATTERY_GENERAL_INFO stGeneralInfo, 
				  IN void *pBatteryData, 
				  OUT BATTERY_LOG_INFO **ppBatteryRealData)
{
	BATTERY_LOG_INFO		*stBatteryData = NULL;   //the largest battery group is 18.
	int						i = 0, j = 0, k = 0;
	//int						iBatteryNum = 0;
	BYTE					*pfBatteryData = NULL, *pfTemBatteryData = NULL;
	BATTERY_DATA_RECORD		*pfBatteryDataInfo = (BATTERY_DATA_RECORD *) pBatteryData;
	int						iValue = 0;
	float					fValue;
	//int					m;
	BYTE					byTest[1024];

	pfBatteryData = NEW(BYTE, iRecordNum * 256);
	if(pfBatteryData == NULL)
	{
	    return;
	}
	ASSERT(pfBatteryData);

	//TRACE_WEB_USER("iRecordNum =%d iQtyBatt = %d,iQtyRecord = %d\n",iRecordNum, iQtyBatt,iQtyRecord);
	for(k = 0; k < iRecordNum && pfBatteryDataInfo != NULL; k++, pfBatteryDataInfo++)
	{
		memcpy(pfBatteryData + k * 256, pfBatteryDataInfo->byBatteryDataInfo, sizeof(BYTE) * 256);
	}

	pfTemBatteryData =(BYTE *) pfBatteryData;


	stBatteryData = NEW(BATTERY_LOG_INFO, iQtyRecord);
	ASSERT(stBatteryData);

	//	for(i = 0; i < iQtyBatt; i++)
	//	{
	//		
	////#ifdef _SHOW_WEB_INFO
	//		TRACE(" %d\n", stGeneralInfo.BatteryInfo[i].byBatteryCurrent);
	//		TRACE(" %d\n", stGeneralInfo.BatteryInfo[i].byBatteryVolage);
	//		TRACE(" %d\n", stGeneralInfo.BatteryInfo[i].byBatteryCapacity);
	//
	////#endif 
	//	}
	memset(byTest, 0x0, sizeof(byTest)); 
	memcpy(byTest, &stGeneralInfo, sizeof(byTest));

	/*for(i = 0; i < 1024; i++)
	{
	TRACE_WEB_USER("byTest[%d] = %d ", i, byTest[i]);
	}
	TRACE_WEB_USER("\n");*/

	for(k = 0; k < iQtyRecord; k++)
	{
		if(k > 0)
		{
			pfBatteryData = pfBatteryData + 4;			//?

		}
		/*get time*/
		memcpy((int*)&iValue, pfBatteryData, 4 * sizeof(BYTE));
		stBatteryData[k].tRecordTime = iValue;

		/*get system voltage*/
		pfBatteryData = pfBatteryData + 4;
		memcpy((void*)&fValue, pfBatteryData, 4 * sizeof(BYTE));
		stBatteryData[k].fVoltage = fValue;

		TRACE("k = %d: tRecordTime = %ul, fVoltage = %f\n", k, stBatteryData[k].tRecordTime, stBatteryData[k].fVoltage);
		/*get Temperature*/
		for(i = 0; i < 7; i++)
		{
			if(stGeneralInfo.byTemperatureInfo[i] != 0)
			{
				pfBatteryData = pfBatteryData + 4;
				memcpy((void*)&fValue, pfBatteryData, 4);
				stBatteryData[k].fTemp[i] = fValue;
			}
			else
			{
				stBatteryData[k].fTemp[i] = 0.0;
			}
		}



		/*stBatteryData[k].pBatteryRealData = NEW(BATTERY_REAL_DATA, iQtyBatt);
		ASSERT(stBatteryData[k].pBatteryRealData);*/

		for(i = 0; i < iQtyBatt; i++)
		{
			/*Battery Current*/
			if(stGeneralInfo.BatteryInfo[i].byBatteryCurrent != 0)
			{
				pfBatteryData = pfBatteryData + 4;
				memcpy((void*)&fValue, pfBatteryData, 4);
				//TRACE_WEB_USER("fValue is %f\n", fValue);
				if(FLOAT_EQUAL01(fValue))
				{
					stBatteryData[k].pBatteryRealData[i].fBatCurrent = 0.0;
				}
				else
				{
					stBatteryData[k].pBatteryRealData[i].fBatCurrent = fValue;
				}

				TRACE("stBatteryData[%d].pBatteryRealData[%d].fBatCurrent = %f",k,i, stBatteryData[k].pBatteryRealData[i].fBatCurrent);
			}
			else
			{
				stBatteryData[k].pBatteryRealData[i].fBatCurrent = 0.0;
			}

			/*Battery Voltage*/
			if(stGeneralInfo.BatteryInfo[i].byBatteryVolage != 0)
			{
				pfBatteryData = pfBatteryData + 4;
				memcpy((void*)&fValue, pfBatteryData, 4);
				//TRACE_WEB_USER("fValue is %f\n", fValue);
				stBatteryData[k].pBatteryRealData[i].fBatVoltage = fValue;
				if(FLOAT_EQUAL01(fValue))
				{
					stBatteryData[k].pBatteryRealData[i].fBatVoltage = 0.0;
				}
				else
				{
					stBatteryData[k].pBatteryRealData[i].fBatVoltage = fValue;
				}
				TRACE("stBatteryData[%d].pBatteryRealData[%d].fBatCurrent = %f",k,i, stBatteryData[k].pBatteryRealData[i].fBatCurrent);
			}
			else
			{
				stBatteryData[k].pBatteryRealData[i].fBatVoltage = 0.0;
			}

			/*Battery Capacity*/
			if(stGeneralInfo.BatteryInfo[i].byBatteryCapacity != 0)
			{
				pfBatteryData = pfBatteryData + 4;
				memcpy((void*)&fValue, pfBatteryData, 4);
				//TRACE_WEB_USER("fValue is %f\n", fValue);
				stBatteryData[k].pBatteryRealData[i].fBatCapacity = fValue;
				TRACE("stBatteryData[%d].pBatteryRealData[%d].fBatCurrent = %f",k,i, stBatteryData[k].pBatteryRealData[i].fBatCurrent);
			}
			else
			{
				stBatteryData[k].pBatteryRealData[i].fBatCapacity = 0.0;
			}

		}
		//TRACE_WEB_USER("Now the read block voltage is:\n");
		for(i = 0; i < 20; i++)
		{
			for(j = 0; j < 24; j++)
			{
				if(stGeneralInfo.stSmbatSmbrcBlock[i].bySMBlockVoltage[j])
				{				
					pfBatteryData = pfBatteryData + 4;
					memcpy((void *)&fValue, pfBatteryData, 4);
					//TRACE_WEB_USER("[%d][%d]fValue is %f\n", i, j, fValue);
					if(FLOAT_EQUAL01(fValue))
					{
						stBatteryData[k].fBatSMBlock[i].fSMBlockVoltage[j] = 0.0;
					}
					else
					{
						stBatteryData[k].fBatSMBlock[i].fSMBlockVoltage[j] = fValue;
					}
				}
				else
				{
					stBatteryData[k].fBatSMBlock[i].fSMBlockVoltage[j] = 0.0;
				}
			}
		}
		for(i = 0; i < 4; i++)
		{
			for(j = 0; j < 8; j++)
			{
				if(stGeneralInfo.stBatteryBlock[i].byBlockVoltage[j])
				{				
					pfBatteryData = pfBatteryData + 4;
					memcpy((void *)&fValue, pfBatteryData, 4);
					if(FLOAT_EQUAL01(fValue))
					{
						stBatteryData[k].fBatBlock[i].fBatBlockVoltage[j] = 0.0;
					}
					else
					{
						stBatteryData[k].fBatBlock[i].fBatBlockVoltage[j] = fValue;
					}
				}
				else
				{
					stBatteryData[k].fBatBlock[i].fBatBlockVoltage[j] = 0.0;
				}
			}
		}
		/*for(j = 0; j < 32; j++)
		{
		m = j/8;
		if(byTest[205 + j] != 0)
		{
		pfBatteryData = pfBatteryData + 4;
		memcpy((void *)&fValue, pfBatteryData, 4);
		stBatteryData[k].fBatBlock[m].fBatBlockVoltage[j] = fValue;
		TRACE("stBatteryData[%d].pBatteryRealData[%d].fBatCurrent = %f",k,i, stBatteryData[k].pBatteryRealData[i].fBatCurrent);
		}
		else
		{
		stBatteryData[k].fBatBlock[m].fBatBlockVoltage[j] = 0.0;
		}
		}*/
	}

	DELETE(pfTemBatteryData);
	pfTemBatteryData = NULL;
	*ppBatteryRealData = stBatteryData;
}


/*==========================================================================*
* FUNCTION :    Web_QueryBatteryTestLog
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   IN int iQueryTimes:
OUT BT_LOG_SUMMARY **ppGeneralInfo:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
*==========================================================================*/
static int Web_QueryBatteryTestLog(IN int iQueryTimes, 
				   OUT BATTERY_HEAD_INFO **ppGeneralInfo,
				   OUT BATTERY_LOG_INFO **ppBatteryRealData,
				   OUT  BATTERY_GENERAL_INFO	*stOutBatteryGeneralInfo)
{
	HANDLE					hBatteryTestLog = NULL;
	BATTERY_HEAD_INFO		*stBatteryHeadInfo = NULL;
	HIS_GENERAL_CONDITION	stCondition;
	BATTERY_DATA_RECORD		*pBatteryData = NULL;
	int						iBuf[10];
	int						iStartNO = -1;
	//int						iBuffLen = 0;
	int						iRecords = 10;
	int						iRecordNum = 0, iQtyBatt = 0, iQtyRecord = 0, iResult = -2;
	BATTERY_GENERAL_INFO	BatteryGeneralInfo;
	BYTE	baBuf[1024];
	int i, j;
	//int iRecordNumtemp, iStartNOtemp;


	/*for(i = 0; i < MAX_DATA_TYPES; i++)
	{
	TRACE_WEB_USER("The log name is %s\n", g_RECORD_SET_TRACK[i].DataName);
	if(strcmp(BATT_TEST_LOG_FILE, g_RECORD_SET_TRACK[i].DataName) == 0)
	{
	TRACE_WEB_USER("iTotalSectors is %d, iMaxRecords is %d, iRecordSize is %d, iStartRecordNo is %d, bSectorEffect is %d, "\ 
	"byCurrSectorNo is %d, iCurrWritePos is %d, iCurrReadPos is %d, byOldCurrSectorNo is %d, iDataRecordIDIndex is %d, "\
	"byCurrSectorNo is %d, byNewSectorFlag is %d, iMaxRecordsPerSector is %d, iWritableRecords is %d, dwCurrRecordID is %d, "\
	"byLogSectorNo is %d, byPhySectorNo is %d, byOldCurrSectorNo is %d\n", 
	g_RECORD_SET_TRACK[i].iTotalSectors, g_RECORD_SET_TRACK[i].iMaxRecords, g_RECORD_SET_TRACK[i].iRecordSize, 
	g_RECORD_SET_TRACK[i].iStartRecordNo, g_RECORD_SET_TRACK[i].bSectorEffect, g_RECORD_SET_TRACK[i].byCurrSectorNo, 
	g_RECORD_SET_TRACK[i].iCurrWritePos, g_RECORD_SET_TRACK[i].iCurrReadPos, g_RECORD_SET_TRACK[i].byOldCurrSectorNo, 
	g_RECORD_SET_TRACK[i].iDataRecordIDIndex, g_RECORD_SET_TRACK[i].byCurrSectorNo, g_RECORD_SET_TRACK[i].byNewSectorFlag,
	g_RECORD_SET_TRACK[i].iMaxRecordsPerSector, g_RECORD_SET_TRACK[i].iWritableRecords, g_RECORD_SET_TRACK[i].dwCurrRecordID,
	g_RECORD_SET_TRACK[i].pbyPhysicsSectorNo->byLogSectorNo, g_RECORD_SET_TRACK[i].pbyPhysicsSectorNo->byPhySectorNo,
	g_RECORD_SET_TRACK[i].byOldCurrSectorNo);
	for (j = 0; j < MAX_DATA_TYPES; j++)
	{
	if(g_HISDATA_QUEUE[j].iDataRecordIDIndex == g_RECORD_SET_TRACK[i].iDataRecordIDIndex)
	{
	TRACE_WEB_USER("g_HISDATA_QUEUE.bIsAlarm is %d, iHead is %d, iLength is %d, iRecordSize is %d, iMaxSize is %d\n",
	g_HISDATA_QUEUE[j].bIsAlarm, g_HISDATA_QUEUE[j].iHead, g_HISDATA_QUEUE[j].iLength, g_HISDATA_QUEUE[j].iRecordSize,
	g_HISDATA_QUEUE[j].iMaxSize);
	}
	}
	}
	}*/
	hBatteryTestLog = DAT_StorageOpen(BATT_TEST_LOG_FILE);

	if(hBatteryTestLog == NULL)
	{
		//TRACE_WEB_USER("Open BATT_TEST_LOG_FILE file!\n");
		return FALSE;
	}
	/*init*/

	stCondition.iEquipID	= -1;
	stCondition.iDataType	= -1;
	stCondition.tmFromTime	= 0;
	stCondition.tmToTime	= 0;

	/*find the match record*/
	//Find the battery test times
	iStartNO = -1;
	memset(iBuf, 0x0, iRecords * sizeof(int));

	iResult = DAT_StorageSearchRecords(hBatteryTestLog,
		Web_fnCompareBatteryCondition,
		NULL,
		&iStartNO,
		&iRecords,
		(void *)iBuf,
		FALSE,
		FALSE,
		TRUE);
	DAT_RecoverReadHandle(&hBatteryTestLog);

	/*for(i = 0; i < 10; i++)
	{
	TRACE_WEB_USER("iBuf[%d] is %d\n", i, iBuf[i]);
	}
	TRACE_WEB_USER("iQueryTimes is %d\n", iQueryTimes);*/

	if(iResult < 0 || iRecords <= 0 )
	{
		DAT_StorageClose(hBatteryTestLog);
		//TRACE_WEB_USER("iResult is %d, iRecords is %d\n", iResult, iRecords);
		//TRACE_WEB_USER("ERR_WEB_COMM_GET_RESULT_FAIL1!\n");

		return ERR_WEB_COMM_GET_RESULT_FAIL;
	}
	else if(iQueryTimes > iRecords)
	{
		DAT_StorageClose(hBatteryTestLog);
		//TRACE_WEB_USER("ERR_WEB_COMM_GET_RESULT_FAIL2!\n");
		return ERR_WEB_COMM_GET_RESULT_FAIL;//ERR_COMM_FALSE_QUERY_TIMES;
	}
	else
	{
#ifdef _DEBUG
		int		k;
		for(k = 0; k < iRecords; k++)
		{
			//TRACE("iBuf[%d] : %d\n",k, iBuf[k]);
		}
		//iStartNO = iBuf[0] ;
		iStartNO = iBuf[iQueryTimes - 1];

#else
		iStartNO = iBuf[iQueryTimes - 1];
		/*iStartNOtemp = iStartNO;
		TRACE_WEB_USER("iStartNOtemp is %d\n", iStartNOtemp);*/
#endif

	}


	/*get the match record head information*/
#ifdef _SHOW_WEB_INFO
	//TRACE("iStartNO:[%d]\n", iStartNO);
#endif

	stBatteryHeadInfo = NEW(BATTERY_HEAD_INFO,1);
	if(stBatteryHeadInfo == NULL)
	{
		DAT_StorageClose(hBatteryTestLog);
		//TRACE_WEB_USER("ERR_WEB_COMM_GET_RESULT_FAIL3!\n");
		return ERR_WEB_COMM_GET_RESULT_FAIL;
	}
	memset(stBatteryHeadInfo, 0x0, sizeof(BATTERY_HEAD_INFO));
	iRecords = 1;

	iResult = DAT_StorageReadRecords(hBatteryTestLog,
		&iStartNO,
		&iRecords,
		stBatteryHeadInfo,//(void*)stBatteryHeadInfo,
		FALSE,
		TRUE);
	DAT_RecoverReadHandle(&hBatteryTestLog);


#ifdef _SHOW_WEB_INFO
	//TRACE("[DAT_StorageReadRecords] iResult :[%d][%d]\n", iResult,iRecords);
#endif


	if(iResult == FALSE )//|| iRecords <= 0)
	{
		DAT_StorageClose(hBatteryTestLog);
		//TRACE_WEB_USER("ERR_WEB_COMM_GET_RESULT_FAIL4!\n");
		return ERR_WEB_COMM_GET_RESULT_FAIL;
	}

	/*Analyze the match record*/
	iQtyRecord	= stBatteryHeadInfo->btSummary.iQtyRecord;
	//changed by Yangguoxin,1/30/2007. Added 8 SMDU,extend the record space from 128 to 256
	iRecordNum	= (iQtyRecord * stBatteryHeadInfo->btSummary.iSizeRecord - 1)/256 + 1; //(iQtyRecord * stBatteryHeadInfo->btSummary.iSizeRecord - 1)/128 + 1; 
	/*iRecordNumtemp = iRecordNum;
	TRACE_WEB_USER("iRecordNumtemp is %d\n", iRecordNumtemp);*/
	iQtyBatt	= stBatteryHeadInfo->btSummary.iQtyBatt;

	TRACE_WEB_USER_NOT_CYCLE("iQtyRecord is %d iRecordNum is %d iQtyBatt is %d iSizeRecord is %d\n", iQtyRecord, iRecordNum, iQtyBatt, stBatteryHeadInfo->btSummary.iSizeRecord);

#ifdef _SHOW_WEB_INFO
	//TRACE("iRecordNum :%d iQtyBatt :%d iQtyRecord:%d \n", iRecordNum, iQtyBatt, iQtyRecord);
	//TRACE("stBatteryHeadInfo->szHeadInfo = [%s]\n",stBatteryHeadInfo->szHeadInfo);
	//TRACE("\t ,iStartReason %d, iEndReason %d, iTestResult %d, iSizeRecord : %d\n",
	//stBatteryHeadInfo->btSummary.iStartReason,
	//stBatteryHeadInfo->btSummary.iEndReason,
	//stBatteryHeadInfo->btSummary.iTestResult,
	//stBatteryHeadInfo->btSummary.iSizeRecord);
	//TRACE("read first record \n");
#endif 

	/*read first record*/

	/*for(i = 0; i < 10; i++)
	{
	TRACE_WEB_USER("iBuf[%d] is %d\n", i, iBuf[i]);
	}
	TRACE_WEB_USER("iQueryTimes is %d\n", iQueryTimes);*/
	iRecords = 4;
	iStartNO = iBuf[iQueryTimes - 1] + 1 ;
	/*TRACE_WEB_USER("iStartNO is %d\n", iStartNO);
	TRACE_WEB_USER("iRecordNum is %d\n", iRecordNum);*/

	//memset(&BatteryGeneralInfo, 0x0, 128);
	iResult = DAT_StorageReadRecords(hBatteryTestLog,
		&iStartNO,
		&iRecords,
		(void*)baBuf,
		FALSE,
		TRUE);
	for(i = 0; i < 1024; i++)
	{
	    TRACE_WEB_USER_NOT_CYCLE("[%d]%d ", i, baBuf[i]);
	}
	TRACE_WEB_USER_NOT_CYCLE("\n");

	transferBuftoBatteryGeneralInfo(&BatteryGeneralInfo, baBuf, iQtyBatt); //Ϊ�˽��ɾ���豸�������Ķ�ȡ��ز��Լ�¼��������
	DAT_RecoverReadHandle(&hBatteryTestLog);
#ifdef _SHOW_WEB_INFO
	//TRACE("[DAT_StorageReadRecords] iResult :[%d] iRecordNum [%d]\n", iResult,iRecords);
	int		mc;
	for(mc = 0; mc < iQtyBatt; mc++)
	{
		BATTERY_INFO	*pBI = &(BatteryGeneralInfo.BatteryInfo[mc]);

		//TRACE("byBatteryCapacity : [%c][%d]\n", pBI->byBatteryCapacity,pBI->byBatteryCapacity);
		//TRACE("byBatteryCurrent [%c][%d]\n", pBI->byBatteryCurrent,pBI->byBatteryCurrent);
		//TRACE("byBatteryNum:[%c][%d]\n", pBI->byBatteryNum, pBI->byBatteryNum);
		//TRACE("byBatteryTemp : [%c][%d]\n", pBI->byBatteryTemp, pBI->byBatteryTemp);
		//TRACE("byBatteryVolage : [%c][%d]\n", pBI->byBatteryVolage, pBI->byBatteryVolage);
	}
#endif

	/*TRACE_WEB_USER("iRecordNum is %d\n", iRecordNum);*/
	if(iResult <= 0)
	{
		DAT_StorageClose(hBatteryTestLog);

		DELETE(stBatteryHeadInfo);
		stBatteryHeadInfo = NULL;
		//TRACE_WEB_USER("ERR_WEB_COMM_GET_RESULT_FAIL5!\n");
		return ERR_WEB_COMM_GET_RESULT_FAIL;
	}

	/*read second record and next record*/
#ifdef _SHOW_WEB_INFO
	//TRACE("read second record and next record \n");
#endif 


	/*TRACE_WEB_USER("iRecordNum is %d\n", iRecordNum);*/
	pBatteryData = NEW(BATTERY_DATA_RECORD, iRecordNum );
	if(pBatteryData == NULL)
	{
		DAT_StorageClose(hBatteryTestLog);
		DELETE(stBatteryHeadInfo);
		stBatteryHeadInfo = NULL;
		//TRACE_WEB_USER("ERR_WEB_NO_MEMORY!\n");
		return ERR_WEB_NO_MEMORY;
	}
	/*TRACE_WEB_USER("iRecordNum is %d\n", iRecordNum);*/
	memset(pBatteryData, 0x0, iRecordNum  * sizeof(BATTERY_DATA_RECORD));

	/*for(i = 0; i < 10; i++)
	{
	TRACE_WEB_USER("iBuf[%d] is %d\n", i, iBuf[i]);
	}
	TRACE_WEB_USER("iQueryTimes is %d\n", iQueryTimes);*/
	iStartNO = iBuf[iQueryTimes - 1] + 5;//�ӵ�5��iRecord��ʼ��
	/*iRecordNum = iRecordNumtemp;
	iStartNO = iStartNOtemp + 3;
	TRACE_WEB_USER("iRecordNumtemp is %d iStartNOtemp is %d\n", iRecordNumtemp, iStartNOtemp);*/
	TRACE_WEB_USER_NOT_CYCLE("\niStartNO = %d iRecordNum = %d\n", iStartNO, iRecordNum);
	iResult = DAT_StorageReadRecords(hBatteryTestLog,
		&iStartNO,
		&iRecordNum,
		(void *)pBatteryData,
		0,
		TRUE);
	DAT_RecoverReadHandle(&hBatteryTestLog);
	//TRACE_WEB_USER("iRecordNum is %d\n", iRecordNum);

#ifdef _SHOW_WEB_INFO
	//TRACE("[DAT_StorageReadRecords] iResult :[%d] iRecordNum [%d]\n", iResult,iRecordNum);
#endif

	if(iResult > 0)
	{
		/*Analyze the battery data*/

		Web_ManageBatteryData(iRecordNum, iQtyBatt, iQtyRecord, BatteryGeneralInfo, 
			(void *)pBatteryData, ppBatteryRealData);

		DELETE(pBatteryData);
		pBatteryData = NULL;

		*ppGeneralInfo = stBatteryHeadInfo;
		memcpy(stOutBatteryGeneralInfo,&BatteryGeneralInfo, sizeof(BATTERY_GENERAL_INFO));
		/*free*/
		DAT_StorageClose(hBatteryTestLog);
		//TRACE_WEB_USER("TRUE!\n");

		return TRUE;
	}
	else
	{
		/*free*/
		DAT_StorageClose(hBatteryTestLog);
		DELETE(stBatteryHeadInfo);
		stBatteryHeadInfo = NULL;
		//TRACE_WEB_USER("FALSE!\n");
		return FALSE;
	}

}

/*==========================================================================*
* FUNCTION :  transferTostOutBatteryGeneralInfo1
* PURPOSE  :  Transfer the struct of BATTERY_GENERAL_INFO to BATTERY_GENERAL_INFO1
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  stOutBatteryGeneralInfo1: The struct of BATTERY_GENERAL_INFO1
	      stOutBatteryGeneralInfo: The struct of BATTERY_GENERAL_INFO
* RETURN   :  void 
* COMMENTS :  This function is from <communicate.c>
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
void transferTostOutBatteryGeneralInfo1(BATTERY_GENERAL_INFO1 *stOutBatteryGeneralInfo1, BATTERY_GENERAL_INFO stOutBatteryGeneralInfo)
{
	int i, j;

	TRACE_WEB_USER_NOT_CYCLE("g_pGcData->EquipInfo.iCfgQtyOfComBAT is %d\n", g_SiteInfo.stBattNum.iCfgQtyOfComBAT);
	TRACE_WEB_USER_NOT_CYCLE("g_pGcData->EquipInfo.iCfgQtyOfEIBBAT1 is %d\n", g_SiteInfo.stBattNum.iCfgQtyOfEIBBAT1);
	TRACE_WEB_USER_NOT_CYCLE("g_pGcData->EquipInfo.iCfgQtyOfSmduBAT1 is %d\n", g_SiteInfo.stBattNum.iCfgQtyOfSmduBAT1);
	TRACE_WEB_USER_NOT_CYCLE("g_pGcData->EquipInfo.iCfgQtyOfEIBBAT2 is %d\n", g_SiteInfo.stBattNum.iCfgQtyOfEIBBAT2);
	TRACE_WEB_USER_NOT_CYCLE("g_pGcData->EquipInfo.iCfgQtyOfSMBAT is %d\n", g_SiteInfo.stBattNum.iCfgQtyOfSMBAT);
	TRACE_WEB_USER_NOT_CYCLE("g_pGcData->EquipInfo.iCfgQtyOfLargeDUBAT is %d\n", g_SiteInfo.stBattNum.iCfgQtyOfLargeDUBAT);
	TRACE_WEB_USER_NOT_CYCLE("g_pGcData->EquipInfo.iCfgQtyOfSmduBAT2 is %d\n", g_SiteInfo.stBattNum.iCfgQtyOfSmduBAT2);
	TRACE_WEB_USER_NOT_CYCLE("g_pGcData->EquipInfo.iCfgQtyOfSMBRC is %d\n", g_SiteInfo.stBattNum.iCfgQtyOfSMBRC);

	j = 0;
	memset(stOutBatteryGeneralInfo1, 0x00, sizeof(stOutBatteryGeneralInfo1));
	for(i = 0; i < g_SiteInfo.stBattNum.iCfgQtyOfComBAT; i++)
	{
		stOutBatteryGeneralInfo1->BatteryInfo[i].byBatteryCurrent = stOutBatteryGeneralInfo.BatteryInfo[j].byBatteryCapacity;
		stOutBatteryGeneralInfo1->BatteryInfo[i].byBatteryVolage = stOutBatteryGeneralInfo.BatteryInfo[j].byBatteryVolage;
		stOutBatteryGeneralInfo1->BatteryInfo[i].byBatteryCapacity = stOutBatteryGeneralInfo.BatteryInfo[j].byBatteryCapacity;
		j++;
	}
	for(i = 0; i < g_SiteInfo.stBattNum.iCfgQtyOfEIBBAT1; i++)
	{
		stOutBatteryGeneralInfo1->EIBBatteryInfo1[i].byBatteryCurrent = stOutBatteryGeneralInfo.BatteryInfo[j].byBatteryCapacity;
		stOutBatteryGeneralInfo1->EIBBatteryInfo1[i].byBatteryVolage = stOutBatteryGeneralInfo.BatteryInfo[j].byBatteryVolage;
		stOutBatteryGeneralInfo1->EIBBatteryInfo1[i].byBatteryCapacity = stOutBatteryGeneralInfo.BatteryInfo[j].byBatteryCapacity;
		j++;
	}
	for(i = 0; i < g_SiteInfo.stBattNum.iCfgQtyOfSmduBAT1; i++)
	{
		stOutBatteryGeneralInfo1->SmduBatteryInfo1[i].byBatteryCurrent = stOutBatteryGeneralInfo.BatteryInfo[j].byBatteryCapacity;
		stOutBatteryGeneralInfo1->SmduBatteryInfo1[i].byBatteryVolage = stOutBatteryGeneralInfo.BatteryInfo[j].byBatteryVolage;
		stOutBatteryGeneralInfo1->SmduBatteryInfo1[i].byBatteryCapacity = stOutBatteryGeneralInfo.BatteryInfo[j].byBatteryCapacity;
		j++;
	}
	for(i = 0; i < g_SiteInfo.stBattNum.iCfgQtyOfEIBBAT2; i++)
	{
		stOutBatteryGeneralInfo1->EIBBatteryInfo2[i].byBatteryCurrent = stOutBatteryGeneralInfo.BatteryInfo[j].byBatteryCapacity;
		stOutBatteryGeneralInfo1->EIBBatteryInfo2[i].byBatteryVolage = stOutBatteryGeneralInfo.BatteryInfo[j].byBatteryVolage;
		stOutBatteryGeneralInfo1->EIBBatteryInfo2[i].byBatteryCapacity = stOutBatteryGeneralInfo.BatteryInfo[j].byBatteryCapacity;
		j++;
	}
	for(i = 0; i < g_SiteInfo.stBattNum.iCfgQtyOfSMBAT; i++)
	{
		stOutBatteryGeneralInfo1->SMBATInfo[i].byBatteryCurrent = stOutBatteryGeneralInfo.BatteryInfo[j].byBatteryCapacity;
		stOutBatteryGeneralInfo1->SMBATInfo[i].byBatteryVolage = stOutBatteryGeneralInfo.BatteryInfo[j].byBatteryVolage;
		stOutBatteryGeneralInfo1->SMBATInfo[i].byBatteryCapacity = stOutBatteryGeneralInfo.BatteryInfo[j].byBatteryCapacity;
		j++;
	}
	for(i = 0; i < g_SiteInfo.stBattNum.iCfgQtyOfLargeDUBAT; i++)
	{
		stOutBatteryGeneralInfo1->LargeDUBatteryInfo[i].byBatteryCurrent = stOutBatteryGeneralInfo.BatteryInfo[j].byBatteryCapacity;
		stOutBatteryGeneralInfo1->LargeDUBatteryInfo[i].byBatteryVolage = stOutBatteryGeneralInfo.BatteryInfo[j].byBatteryVolage;
		stOutBatteryGeneralInfo1->LargeDUBatteryInfo[i].byBatteryCapacity = stOutBatteryGeneralInfo.BatteryInfo[j].byBatteryCapacity;
		j++;
	}
	for(i = 0; i < g_SiteInfo.stBattNum.iCfgQtyOfSmduBAT2; i++)
	{
		stOutBatteryGeneralInfo1->SmduBatteryInfo2[i].byBatteryCurrent = stOutBatteryGeneralInfo.BatteryInfo[j].byBatteryCapacity;
		stOutBatteryGeneralInfo1->SmduBatteryInfo2[i].byBatteryVolage = stOutBatteryGeneralInfo.BatteryInfo[j].byBatteryVolage;
		stOutBatteryGeneralInfo1->SmduBatteryInfo2[i].byBatteryCapacity = stOutBatteryGeneralInfo.BatteryInfo[j].byBatteryCapacity;
		j++;
	}
	for(i = 0; i < g_SiteInfo.stBattNum.iCfgQtyOfSMBRC; i++)
	{
		stOutBatteryGeneralInfo1->SMBRCInfo[i].byBatteryCurrent = stOutBatteryGeneralInfo.BatteryInfo[j].byBatteryCapacity;
		stOutBatteryGeneralInfo1->SMBRCInfo[i].byBatteryVolage = stOutBatteryGeneralInfo.BatteryInfo[j].byBatteryVolage;
		stOutBatteryGeneralInfo1->SMBRCInfo[i].byBatteryCapacity = stOutBatteryGeneralInfo.BatteryInfo[j].byBatteryCapacity;
		j++;
	}

	//TRACE_WEB_USER("transferTostOutBatteryGeneralInfo1 j is %d\n", j);

	return;
}

/*==========================================================================*
* FUNCTION :  transferTostBatteryLogInfo1
* PURPOSE  :  Transfer the struct of BATTERY_LOG_INFO to BATTERY_LOG_INFO1
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  stBatteryLogInfo1: The struct of BATTERY_LOG_INFO1
	      pBatteryLogInfo: The struct of BATTERY_LOG_INFO
* RETURN   :  void 
* COMMENTS :  This function is from <communicate.c>
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
void transferTostBatteryLogInfo1(BATTERY_LOG_INFO1 *stBatteryLogInfo1, BATTERY_LOG_INFO pBatteryLogInfo)
{
	int i, j;

	j = 0;
	memset(stBatteryLogInfo1, 0x00, sizeof(stBatteryLogInfo1));
	for(i = 0; i < g_SiteInfo.stBattNum.iCfgQtyOfComBAT; i++)
	{
		stBatteryLogInfo1->BatteryData[i].fBatCurrent = pBatteryLogInfo.pBatteryRealData[j].fBatCurrent;
		stBatteryLogInfo1->BatteryData[i].fBatVoltage = pBatteryLogInfo.pBatteryRealData[j].fBatVoltage;
		stBatteryLogInfo1->BatteryData[i].fBatCapacity = pBatteryLogInfo.pBatteryRealData[j].fBatCapacity;
		j++;
	}
	for(i = 0; i < g_SiteInfo.stBattNum.iCfgQtyOfEIBBAT1; i++)
	{
		stBatteryLogInfo1->EIBBatteryData1[i].fBatCurrent = pBatteryLogInfo.pBatteryRealData[j].fBatCurrent;
		stBatteryLogInfo1->EIBBatteryData1[i].fBatVoltage = pBatteryLogInfo.pBatteryRealData[j].fBatVoltage;
		stBatteryLogInfo1->EIBBatteryData1[i].fBatCapacity = pBatteryLogInfo.pBatteryRealData[j].fBatCapacity;
		j++;
	}
	for(i = 0; i < g_SiteInfo.stBattNum.iCfgQtyOfSmduBAT1; i++)
	{
		stBatteryLogInfo1->SmduBatteryData1[i].fBatCurrent = pBatteryLogInfo.pBatteryRealData[j].fBatCurrent;
		stBatteryLogInfo1->SmduBatteryData1[i].fBatVoltage = pBatteryLogInfo.pBatteryRealData[j].fBatVoltage;
		stBatteryLogInfo1->SmduBatteryData1[i].fBatCapacity = pBatteryLogInfo.pBatteryRealData[j].fBatCapacity;
		j++;
	}
	for(i = 0; i < g_SiteInfo.stBattNum.iCfgQtyOfEIBBAT2; i++)
	{
		stBatteryLogInfo1->EIBBatteryData2[i].fBatCurrent = pBatteryLogInfo.pBatteryRealData[j].fBatCurrent;
		stBatteryLogInfo1->EIBBatteryData2[i].fBatVoltage = pBatteryLogInfo.pBatteryRealData[j].fBatVoltage;
		stBatteryLogInfo1->EIBBatteryData2[i].fBatCapacity = pBatteryLogInfo.pBatteryRealData[j].fBatCapacity;
		j++;
	}
	for(i = 0; i < g_SiteInfo.stBattNum.iCfgQtyOfSMBAT; i++)
	{
		stBatteryLogInfo1->SMBATData[i].fBatCurrent = pBatteryLogInfo.pBatteryRealData[j].fBatCurrent;
		stBatteryLogInfo1->SMBATData[i].fBatVoltage = pBatteryLogInfo.pBatteryRealData[j].fBatVoltage;
		stBatteryLogInfo1->SMBATData[i].fBatCapacity = pBatteryLogInfo.pBatteryRealData[j].fBatCapacity;
		j++;
	}
	for(i = 0; i < g_SiteInfo.stBattNum.iCfgQtyOfLargeDUBAT; i++)
	{
		stBatteryLogInfo1->LargeDUBatteryData[i].fBatCurrent = pBatteryLogInfo.pBatteryRealData[j].fBatCurrent;
		stBatteryLogInfo1->LargeDUBatteryData[i].fBatVoltage = pBatteryLogInfo.pBatteryRealData[j].fBatVoltage;
		stBatteryLogInfo1->LargeDUBatteryData[i].fBatCapacity = pBatteryLogInfo.pBatteryRealData[j].fBatCapacity;
		j++;
	}
	for(i = 0; i < g_SiteInfo.stBattNum.iCfgQtyOfSmduBAT2; i++)
	{
		stBatteryLogInfo1->SmduBatteryData2[i].fBatCurrent = pBatteryLogInfo.pBatteryRealData[j].fBatCurrent;
		stBatteryLogInfo1->SmduBatteryData2[i].fBatVoltage = pBatteryLogInfo.pBatteryRealData[j].fBatVoltage;
		stBatteryLogInfo1->SmduBatteryData2[i].fBatCapacity = pBatteryLogInfo.pBatteryRealData[j].fBatCapacity;
		j++;
	}
	for(i = 0; i < g_SiteInfo.stBattNum.iCfgQtyOfSMBRC; i++)
	{
		stBatteryLogInfo1->SMBRCData[i].fBatCurrent = pBatteryLogInfo.pBatteryRealData[j].fBatCurrent;
		stBatteryLogInfo1->SMBRCData[i].fBatVoltage = pBatteryLogInfo.pBatteryRealData[j].fBatVoltage;
		stBatteryLogInfo1->SMBRCData[i].fBatCapacity = pBatteryLogInfo.pBatteryRealData[j].fBatCapacity;
		j++;
	}

	//TRACE_WEB_USER("transferTostBatteryLogInfo1 j is %d\n", j);

	return;
}

char szBatterTitle[861][32] = {"Battery1 Current(A)",	
"Battery1 Voltage(V)",	
"Battery1 Capacity(Ah)",	
"Battery2 Current(A)",	
"Battery2 Voltage(V)",	
"Battery2 Capacity(Ah)",	
"EIB1Battery1 Current(A)",	
"EIB1Battery1 Voltage(V)",	
"EIB1Battery1 Capacity(Ah)",	
"EIB1Battery2 Current(A)",	
"EIB1Battery2 Voltage(V)",	
"EIB1Battery2 Capacity(Ah)",	
"EIB2Battery1 Current(A)",	
"EIB2Battery1 Voltage(V)",	
"EIB2Battery1 Capacity(Ah)",	
"EIB2Battery2 Current(A)",	
"EIB2Battery2 Voltage(V)",	
"EIB2Battery2 Capacity(Ah)",	
"EIB3Battery1 Current(A)",	
"EIB3Battery1 Voltage(V)",	
"EIB3Battery1 Capacity(Ah)",	
"EIB3Battery2 Current(A)",	
"EIB3Battery2 Voltage(V)",	
"EIB3Battery2 Capacity(Ah)",	
"EIB4Battery1 Current(A)",	
"EIB4Battery1 Voltage(V)",	
"EIB4Battery1 Capacity(Ah)",	
"EIB4Battery2 Current(A)",	
"EIB4Battery2 Voltage(V)",	
"EIB4Battery2 Capacity(Ah)",	
"SMDU1Battery1 Current(A)",	
"SMDU1Battery1 Voltage(V)",	
"SMDU1Battery1 Capacity(Ah)",	
"SMDU1Battery2 Current(A)",	
"SMDU1Battery2 Voltage(V)",	
"SMDU1Battery2 Capacity(Ah)",	
"SMDU1Battery3 Current(A)",	
"SMDU1Battery3 Voltage(V)",	
"SMDU1Battery3 Capacity(Ah)",	
"SMDU1Battery4 Current(A)",	
"SMDU1Battery4 Voltage(V)",	
"SMDU1Battery4 Capacity(Ah)",	
"SMDU2Battery1 Current(A)",	
"SMDU2Battery1 Voltage(V)",	
"SMDU2Battery1 Capacity(Ah)",	
"SMDU2Battery2 Current(A)",	
"SMDU2Battery2 Voltage(V)",	
"SMDU2Battery2 Capacity(Ah)",	
"SMDU2Battery3 Current(A)",	
"SMDU2Battery3 Voltage(V)",	
"SMDU2Battery3 Capacity(Ah)",	
"SMDU2Battery4 Current(A)",	
"SMDU2Battery4 Voltage(V)",	
"SMDU2Battery4 Capacity(Ah)",	
"SMDU3Battery1 Current(A)",	
"SMDU3Battery1 Voltage(V)",	
"SMDU3Battery1 Capacity(Ah)",
"SMDU3Battery2 Current(A)",	
"SMDU3Battery2 Voltage(V)",	
"SMDU3Battery2 Capacity(Ah)",	
"SMDU3Battery3 Current(A)",	
"SMDU3Battery3 Voltage(V)",	
"SMDU3Battery3 Capacity(Ah)",	
"SMDU3Battery4 Current(A)",	
"SMDU3Battery4 Voltage(V)",	
"SMDU3Battery4 Capacity(Ah)",	
"SMDU4Battery1 Current(A)",	
"SMDU4Battery1 Voltage(V)",	
"SMDU4Battery1 Capacity(Ah)",	
"SMDU4Battery2 Current(A)",	
"SMDU4Battery2 Voltage(V)",	
"SMDU4Battery2 Capacity(Ah)",	
"SMDU4Battery3 Current(A)",	
"SMDU4Battery3 Voltage(V)",	
"SMDU4Battery3 Capacity(Ah)",	
"SMDU4Battery4 Current(A)",	
"SMDU4Battery4 Voltage(V)",	
"SMDU4Battery4 Capacity(Ah)",	
"SMDU5Battery1 Current(A)",	
"SMDU5Battery1 Voltage(V)",	
"SMDU5Battery1 Capacity(Ah)",	
"SMDU5Battery2 Current(A)",	
"SMDU5Battery2 Voltage(V)",	
"SMDU5Battery2 Capacity(Ah)",	
"SMDU5Battery3 Current(A)",	
"SMDU5Battery3 Voltage(V)",	
"SMDU5Battery3 Capacity(Ah)",	
"SMDU5Battery4 Current(A)",	
"SMDU5Battery4 Voltage(V)",	
"SMDU5Battery4 Capacity(Ah)",	
"SMDU6Battery1 Current(A)",	
"SMDU6Battery1 Voltage(V)",	
"SMDU6Battery1 Capacity(Ah)",	
"SMDU6Battery2 Current(A)",	
"SMDU6Battery2 Voltage(V)",	
"SMDU6Battery2 Capacity(Ah)",	
"SMDU6Battery3 Current(A)",	
"SMDU6Battery3 Voltage(V)",	
"SMDU6Battery3 Capacity(Ah)",	
"SMDU6Battery4 Current(A)",	
"SMDU6Battery4 Voltage(V)",	
"SMDU6Battery4 Capacity(Ah)",	
"SMDU7Battery1 Current(A)",	
"SMDU7Battery1 Voltage(V)",	
"SMDU7Battery1 Capacity(Ah)",	
"SMDU7Battery2 Current(A)",	
"SMDU7Battery2 Voltage(V)",	
"SMDU7Battery2 Capacity(Ah)",	
"SMDU7Battery3 Current(A)",	
"SMDU7Battery3 Voltage(V)",	
"SMDU7Battery3 Capacity(Ah)",	
"SMDU7Battery4 Current(A)",	
"SMDU7Battery4 Voltage(V)",	
"SMDU7Battery4 Capacity(Ah)",	
"SMDU8Battery1 Current(A)",	
"SMDU8Battery1 Voltage(V)",	
"SMDU8Battery1 Capacity(Ah)",	
"SMDU8Battery2 Current(A)",	
"SMDU8Battery2 Voltage(V)",	
"SMDU8Battery2 Capacity(Ah)",	
"SMDU8Battery3 Current(A)",	
"SMDU8Battery3 Voltage(V)",	
"SMDU8Battery3 Capacity(Ah)",	
"SMDU8Battery4 Current(A)",	
"SMDU8Battery4 Voltage(V)",	
"SMDU8Battery4 Capacity(Ah)",	
"EIB1Battery3 Current(A)",	
"EIB1Battery3 Voltage(V)",	
"EIB1Battery3 Capacity(Ah)",	
"EIB2Battery3 Current(A)",	
"EIB2Battery3 Voltage(V)",	
"EIB2Battery3 Capacity(Ah)",	
"EIB3Battery3 Current(A)",	
"EIB3Battery3 Voltage(V)",	
"EIB3Battery3 Capacity(Ah)",	
"EIB4Battery3 Current(A)",	
"EIB4Battery3 Voltage(V)",	
"EIB4Battery3 Capacity(Ah)",//�����ӵ���豸begin
"SMBattery1 Current(A)",
"SMBattery1 Voltage(V)",
"SMBattery1 Capacity(Ah)",
"SMBattery2 Current(A)",
"SMBattery2 Voltage(V)",
"SMBattery2 Capacity(Ah)",
"SMBattery3 Current(A)",
"SMBattery3 Voltage(V)",
"SMBattery3 Capacity(Ah)",
"SMBattery4 Current(A)",
"SMBattery4 Voltage(V)",
"SMBattery4 Capacity(Ah)",
"SMBattery5 Current(A)",
"SMBattery5 Voltage(V)",
"SMBattery5 Capacity(Ah)",
"SMBattery6 Current(A)",
"SMBattery6 Voltage(V)",
"SMBattery6 Capacity(Ah)",
"SMBattery7 Current(A)",
"SMBattery7 Voltage(V)",
"SMBattery7 Capacity(Ah)",
"SMBattery8 Current(A)",
"SMBattery8 Voltage(V)",
"SMBattery8 Capacity(Ah)",
"SMBattery9 Current(A)",
"SMBattery9 Voltage(V)",
"SMBattery9 Capacity(Ah)",
"SMBattery10 Current(A)",
"SMBattery10 Voltage(V)",
"SMBattery10 Capacity(Ah)",
"SMBattery11 Current(A)",
"SMBattery11 Voltage(V)",
"SMBattery11 Capacity(Ah)",
"SMBattery12 Current(A)",
"SMBattery12 Voltage(V)",
"SMBattery12 Capacity(Ah)",
"SMBattery13 Current(A)",
"SMBattery13 Voltage(V)",
"SMBattery13 Capacity(Ah)",
"SMBattery14 Current(A)",
"SMBattery14 Voltage(V)",
"SMBattery14 Capacity(Ah)",
"SMBattery15 Current(A)",
"SMBattery15 Voltage(V)",
"SMBattery15 Capacity(Ah)",
"SMBattery16 Current(A)",
"SMBattery16 Voltage(V)",
"SMBattery16 Capacity(Ah)",
"SMBattery17 Current(A)",
"SMBattery17 Voltage(V)",
"SMBattery17 Capacity(Ah)",
"SMBattery18 Current(A)",
"SMBattery18 Voltage(V)",
"SMBattery18 Capacity(Ah)",
"SMBattery19 Current(A)",
"SMBattery19 Voltage(V)",
"SMBattery19 Capacity(Ah)",
"SMBattery20 Current(A)",
"SMBattery20 Voltage(V)",
"SMBattery20 Capacity(Ah)",//�����ӵ���豸end
"LargDUBattery1 Current(A)",	
"LargDUBattery1 Voltage(V)",	
"LargDUBattery1 Capacity(Ah)",
"LargDUBattery2 Current(A)",	
"LargDUBattery2 Voltage(V)",	
"LargDUBattery2 Capacity(Ah)",
"LargDUBattery3 Current(A)",	
"LargDUBattery3 Voltage(V)",	
"LargDUBattery3 Capacity(Ah)",
"LargDUBattery4 Current(A)",	
"LargDUBattery4 Voltage(V)",	
"LargDUBattery4 Capacity(Ah)",
"LargDUBattery5 Current(A)",	
"LargDUBattery5 Voltage(V)",	
"LargDUBattery5 Capacity(Ah)",
"LargDUBattery6 Current(A)",	
"LargDUBattery6 Voltage(V)",	
"LargDUBattery6 Capacity(Ah)",
"LargDUBattery7 Current(A)",	
"LargDUBattery7 Voltage(V)",	
"LargDUBattery7 Capacity(Ah)",
"LargDUBattery8 Current(A)",	
"LargDUBattery8 Voltage(V)",	
"LargDUBattery8 Capacity(Ah)",
"LargDUBattery9 Current(A)",	
"LargDUBattery9 Voltage(V)",	
"LargDUBattery9 Capacity(Ah)",
"LargDUBattery10 Current(A)",	
"LargDUBattery10 Voltage(V)",	
"LargDUBattery10 Capacity(Ah)",
"LargDUBattery11 Current(A)",	
"LargDUBattery11 Voltage(V)",	
"LargDUBattery11 Capacity(Ah)",
"LargDUBattery12 Current(A)",	
"LargDUBattery12 Voltage(V)",	
"LargDUBattery12 Capacity(Ah)",
"LargDUBattery13 Current(A)",	
"LargDUBattery13 Voltage(V)",	
"LargDUBattery13 Capacity(Ah)",
"LargDUBattery14 Current(A)",	
"LargDUBattery14 Voltage(V)",	
"LargDUBattery14 Capacity(Ah)",
"LargDUBattery15 Current(A)",	
"LargDUBattery15 Voltage(V)",	
"LargDUBattery15 Capacity(Ah)",
"LargDUBattery16 Current(A)",	
"LargDUBattery16 Voltage(V)",	
"LargDUBattery16 Capacity(Ah)",
"LargDUBattery17 Current(A)",	
"LargDUBattery17 Voltage(V)",	
"LargDUBattery17 Capacity(Ah)",
"LargDUBattery18 Current(A)",	
"LargDUBattery18 Voltage(V)",	
"LargDUBattery18 Capacity(Ah)",
"LargDUBattery19 Current(A)",	
"LargDUBattery19 Voltage(V)",	
"LargDUBattery19 Capacity(Ah)",
"LargDUBattery20 Current(A)",	
"LargDUBattery20 Voltage(V)",	
"LargDUBattery20 Capacity(Ah)",				//�����ӵ���豸begin
"SMDU1Battery5 Current(A)",
"SMDU1Battery5 Voltage(V)",
"SMDU1Battery5 Capacity(Ah)",
"SMDU2Battery5 Current(A)",
"SMDU2Battery5 Voltage(V)",
"SMDU2Battery5 Capacity(Ah)",
"SMDU3Battery5 Current(A)",
"SMDU3Battery5 Voltage(V)",
"SMDU3Battery5 Capacity(Ah)",
"SMDU4Battery5 Current(A)",
"SMDU4Battery5 Voltage(V)",
"SMDU4Battery5 Capacity(Ah)",
"SMDU5Battery5 Current(A)",
"SMDU5Battery5 Voltage(V)",
"SMDU5Battery5 Capacity(Ah)",
"SMDU6Battery5 Current(A)",
"SMDU6Battery5 Voltage(V)",
"SMDU6Battery5 Capacity(Ah)",
"SMDU7Battery5 Current(A)",
"SMDU7Battery5 Voltage(V)",
"SMDU7Battery5 Capacity(Ah)",
"SMDU8Battery5 Current(A)",
"SMDU8Battery5 Voltage(V)",
"SMDU8Battery5 Capacity(Ah)",
"SMBRCBattery1 Current(A)",
"SMBRCBattery1 Voltage(V)",
"SMBRCBattery1 Capacity(Ah)",
"SMBRCBattery2 Current(A)",
"SMBRCBattery2 Voltage(V)",
"SMBRCBattery2 Capacity(Ah)",
"SMBRCBattery3 Current(A)",
"SMBRCBattery3 Voltage(V)",
"SMBRCBattery3 Capacity(Ah)",
"SMBRCBattery4 Current(A)",
"SMBRCBattery4 Voltage(V)",
"SMBRCBattery4 Capacity(Ah)",
"SMBRCBattery5 Current(A)",
"SMBRCBattery5 Voltage(V)",
"SMBRCBattery5 Capacity(Ah)",
"SMBRCBattery6 Current(A)",
"SMBRCBattery6 Voltage(V)",
"SMBRCBattery6 Capacity(Ah)",
"SMBRCBattery7 Current(A)",
"SMBRCBattery7 Voltage(V)",
"SMBRCBattery7 Capacity(Ah)",
"SMBRCBattery8 Current(A)",
"SMBRCBattery8 Voltage(V)",
"SMBRCBattery8 Capacity(Ah)",
"SMBRCBattery9 Current(A)",
"SMBRCBattery9 Voltage(V)",
"SMBRCBattery9 Capacity(Ah)",
"SMBRCBattery10 Current(A)",
"SMBRCBattery10 Voltage(V)",
"SMBRCBattery10 Capacity(Ah)",
"SMBRCBattery11 Current(A)",
"SMBRCBattery11 Voltage(V)",
"SMBRCBattery11 Capacity(Ah)",
"SMBRCBattery12 Current(A)",
"SMBRCBattery12 Voltage(V)",
"SMBRCBattery12 Capacity(Ah)",
"SMBRCBattery13 Current(A)",
"SMBRCBattery13 Voltage(V)",
"SMBRCBattery13 Capacity(Ah)",
"SMBRCBattery14 Current(A)",
"SMBRCBattery14 Voltage(V)",
"SMBRCBattery14 Capacity(Ah)",
"SMBRCBattery15 Current(A)",
"SMBRCBattery15 Voltage(V)",
"SMBRCBattery15 Capacity(Ah)",
"SMBRCBattery16 Current(A)",
"SMBRCBattery16 Voltage(V)",
"SMBRCBattery16 Capacity(Ah)",
"SMBRCBattery17 Current(A)",
"SMBRCBattery17 Voltage(V)",
"SMBRCBattery17 Capacity(Ah)",
"SMBRCBattery18 Current(A)",
"SMBRCBattery18 Voltage(V)",
"SMBRCBattery18 Capacity(Ah)",
"SMBRCBattery19 Current(A)",
"SMBRCBattery19 Voltage(V)",
"SMBRCBattery19 Capacity(Ah)",
"SMBRCBattery20 Current(A)",
"SMBRCBattery20 Voltage(V)",
"SMBRCBattery20 Capacity(Ah)",//�����ӵ���豸end
"SMBAT/BRC1 BLOCK1 Voltage(V)",
"SMBAT/BRC1 BLOCK2 Voltage(V)",
"SMBAT/BRC1 BLOCK3 Voltage(V)",
"SMBAT/BRC1 BLOCK4 Voltage(V)",
"SMBAT/BRC1 BLOCK5 Voltage(V)",
"SMBAT/BRC1 BLOCK6 Voltage(V)",
"SMBAT/BRC1 BLOCK7 Voltage(V)",
"SMBAT/BRC1 BLOCK8 Voltage(V)",
"SMBAT/BRC1 BLOCK9 Voltage(V)",
"SMBAT/BRC1 BLOCK10 Voltage(V)",
"SMBAT/BRC1 BLOCK11 Voltage(V)",
"SMBAT/BRC1 BLOCK12 Voltage(V)",
"SMBAT/BRC1 BLOCK13 Voltage(V)",
"SMBAT/BRC1 BLOCK14 Voltage(V)",
"SMBAT/BRC1 BLOCK15 Voltage(V)",
"SMBAT/BRC1 BLOCK16 Voltage(V)",
"SMBAT/BRC1 BLOCK17 Voltage(V)",
"SMBAT/BRC1 BLOCK18 Voltage(V)",
"SMBAT/BRC1 BLOCK19 Voltage(V)",
"SMBAT/BRC1 BLOCK20 Voltage(V)",
"SMBAT/BRC1 BLOCK21 Voltage(V)",
"SMBAT/BRC1 BLOCK22 Voltage(V)",
"SMBAT/BRC1 BLOCK23 Voltage(V)",
"SMBAT/BRC1 BLOCK24 Voltage(V)",
"SMBAT/BRC2 BLOCK1 Voltage(V)",
"SMBAT/BRC2 BLOCK2 Voltage(V)",
"SMBAT/BRC2 BLOCK3 Voltage(V)",
"SMBAT/BRC2 BLOCK4 Voltage(V)",
"SMBAT/BRC2 BLOCK5 Voltage(V)",
"SMBAT/BRC2 BLOCK6 Voltage(V)",
"SMBAT/BRC2 BLOCK7 Voltage(V)",
"SMBAT/BRC2 BLOCK8 Voltage(V)",
"SMBAT/BRC2 BLOCK9 Voltage(V)",
"SMBAT/BRC2 BLOCK10 Voltage(V)",
"SMBAT/BRC2 BLOCK11 Voltage(V)",
"SMBAT/BRC2 BLOCK12 Voltage(V)",
"SMBAT/BRC2 BLOCK13 Voltage(V)",
"SMBAT/BRC2 BLOCK14 Voltage(V)",
"SMBAT/BRC2 BLOCK15 Voltage(V)",
"SMBAT/BRC2 BLOCK16 Voltage(V)",
"SMBAT/BRC2 BLOCK17 Voltage(V)",
"SMBAT/BRC2 BLOCK18 Voltage(V)",
"SMBAT/BRC2 BLOCK19 Voltage(V)",
"SMBAT/BRC2 BLOCK20 Voltage(V)",
"SMBAT/BRC2 BLOCK21 Voltage(V)",
"SMBAT/BRC2 BLOCK22 Voltage(V)",
"SMBAT/BRC2 BLOCK23 Voltage(V)",
"SMBAT/BRC2 BLOCK24 Voltage(V)",
"SMBAT/BRC3 BLOCK1 Voltage(V)",
"SMBAT/BRC3 BLOCK2 Voltage(V)",
"SMBAT/BRC3 BLOCK3 Voltage(V)",
"SMBAT/BRC3 BLOCK4 Voltage(V)",
"SMBAT/BRC3 BLOCK5 Voltage(V)",
"SMBAT/BRC3 BLOCK6 Voltage(V)",
"SMBAT/BRC3 BLOCK7 Voltage(V)",
"SMBAT/BRC3 BLOCK8 Voltage(V)",
"SMBAT/BRC3 BLOCK9 Voltage(V)",
"SMBAT/BRC3 BLOCK10 Voltage(V)",
"SMBAT/BRC3 BLOCK11 Voltage(V)",
"SMBAT/BRC3 BLOCK12 Voltage(V)",
"SMBAT/BRC3 BLOCK13 Voltage(V)",
"SMBAT/BRC3 BLOCK14 Voltage(V)",
"SMBAT/BRC3 BLOCK15 Voltage(V)",
"SMBAT/BRC3 BLOCK16 Voltage(V)",
"SMBAT/BRC3 BLOCK17 Voltage(V)",
"SMBAT/BRC3 BLOCK18 Voltage(V)",
"SMBAT/BRC3 BLOCK19 Voltage(V)",
"SMBAT/BRC3 BLOCK20 Voltage(V)",
"SMBAT/BRC3 BLOCK21 Voltage(V)",
"SMBAT/BRC3 BLOCK22 Voltage(V)",
"SMBAT/BRC3 BLOCK23 Voltage(V)",
"SMBAT/BRC3 BLOCK24 Voltage(V)",
"SMBAT/BRC4 BLOCK1 Voltage(V)",
"SMBAT/BRC4 BLOCK2 Voltage(V)",
"SMBAT/BRC4 BLOCK3 Voltage(V)",
"SMBAT/BRC4 BLOCK4 Voltage(V)",
"SMBAT/BRC4 BLOCK5 Voltage(V)",
"SMBAT/BRC4 BLOCK6 Voltage(V)",
"SMBAT/BRC4 BLOCK7 Voltage(V)",
"SMBAT/BRC4 BLOCK8 Voltage(V)",
"SMBAT/BRC4 BLOCK9 Voltage(V)",
"SMBAT/BRC4 BLOCK10 Voltage(V)",
"SMBAT/BRC4 BLOCK11 Voltage(V)",
"SMBAT/BRC4 BLOCK12 Voltage(V)",
"SMBAT/BRC4 BLOCK13 Voltage(V)",
"SMBAT/BRC4 BLOCK14 Voltage(V)",
"SMBAT/BRC4 BLOCK15 Voltage(V)",
"SMBAT/BRC4 BLOCK16 Voltage(V)",
"SMBAT/BRC4 BLOCK17 Voltage(V)",
"SMBAT/BRC4 BLOCK18 Voltage(V)",
"SMBAT/BRC4 BLOCK19 Voltage(V)",
"SMBAT/BRC4 BLOCK20 Voltage(V)",
"SMBAT/BRC4 BLOCK21 Voltage(V)",
"SMBAT/BRC4 BLOCK22 Voltage(V)",
"SMBAT/BRC4 BLOCK23 Voltage(V)",
"SMBAT/BRC4 BLOCK24 Voltage(V)",
"SMBAT/BRC5 BLOCK1 Voltage(V)",
"SMBAT/BRC5 BLOCK2 Voltage(V)",
"SMBAT/BRC5 BLOCK3 Voltage(V)",
"SMBAT/BRC5 BLOCK4 Voltage(V)",
"SMBAT/BRC5 BLOCK5 Voltage(V)",
"SMBAT/BRC5 BLOCK6 Voltage(V)",
"SMBAT/BRC5 BLOCK7 Voltage(V)",
"SMBAT/BRC5 BLOCK8 Voltage(V)",
"SMBAT/BRC5 BLOCK9 Voltage(V)",
"SMBAT/BRC5 BLOCK10 Voltage(V)",
"SMBAT/BRC5 BLOCK11 Voltage(V)",
"SMBAT/BRC5 BLOCK12 Voltage(V)",
"SMBAT/BRC5 BLOCK13 Voltage(V)",
"SMBAT/BRC5 BLOCK14 Voltage(V)",
"SMBAT/BRC5 BLOCK15 Voltage(V)",
"SMBAT/BRC5 BLOCK16 Voltage(V)",
"SMBAT/BRC5 BLOCK17 Voltage(V)",
"SMBAT/BRC5 BLOCK18 Voltage(V)",
"SMBAT/BRC5 BLOCK19 Voltage(V)",
"SMBAT/BRC5 BLOCK20 Voltage(V)",
"SMBAT/BRC5 BLOCK21 Voltage(V)",
"SMBAT/BRC5 BLOCK22 Voltage(V)",
"SMBAT/BRC5 BLOCK23 Voltage(V)",
"SMBAT/BRC5 BLOCK24 Voltage(V)",
"SMBAT/BRC6 BLOCK1 Voltage(V)",
"SMBAT/BRC6 BLOCK2 Voltage(V)",
"SMBAT/BRC6 BLOCK3 Voltage(V)",
"SMBAT/BRC6 BLOCK4 Voltage(V)",
"SMBAT/BRC6 BLOCK5 Voltage(V)",
"SMBAT/BRC6 BLOCK6 Voltage(V)",
"SMBAT/BRC6 BLOCK7 Voltage(V)",
"SMBAT/BRC6 BLOCK8 Voltage(V)",
"SMBAT/BRC6 BLOCK9 Voltage(V)",
"SMBAT/BRC6 BLOCK10 Voltage(V)",
"SMBAT/BRC6 BLOCK11 Voltage(V)",
"SMBAT/BRC6 BLOCK12 Voltage(V)",
"SMBAT/BRC6 BLOCK13 Voltage(V)",
"SMBAT/BRC6 BLOCK14 Voltage(V)",
"SMBAT/BRC6 BLOCK15 Voltage(V)",
"SMBAT/BRC6 BLOCK16 Voltage(V)",
"SMBAT/BRC6 BLOCK17 Voltage(V)",
"SMBAT/BRC6 BLOCK18 Voltage(V)",
"SMBAT/BRC6 BLOCK19 Voltage(V)",
"SMBAT/BRC6 BLOCK20 Voltage(V)",
"SMBAT/BRC6 BLOCK21 Voltage(V)",
"SMBAT/BRC6 BLOCK22 Voltage(V)",
"SMBAT/BRC6 BLOCK23 Voltage(V)",
"SMBAT/BRC6 BLOCK24 Voltage(V)",
"SMBAT/BRC7 BLOCK1 Voltage(V)",
"SMBAT/BRC7 BLOCK2 Voltage(V)",
"SMBAT/BRC7 BLOCK3 Voltage(V)",
"SMBAT/BRC7 BLOCK4 Voltage(V)",
"SMBAT/BRC7 BLOCK5 Voltage(V)",
"SMBAT/BRC7 BLOCK6 Voltage(V)",
"SMBAT/BRC7 BLOCK7 Voltage(V)",
"SMBAT/BRC7 BLOCK8 Voltage(V)",
"SMBAT/BRC7 BLOCK9 Voltage(V)",
"SMBAT/BRC7 BLOCK10 Voltage(V)",
"SMBAT/BRC7 BLOCK11 Voltage(V)",
"SMBAT/BRC7 BLOCK12 Voltage(V)",
"SMBAT/BRC7 BLOCK13 Voltage(V)",
"SMBAT/BRC7 BLOCK14 Voltage(V)",
"SMBAT/BRC7 BLOCK15 Voltage(V)",
"SMBAT/BRC7 BLOCK16 Voltage(V)",
"SMBAT/BRC7 BLOCK17 Voltage(V)",
"SMBAT/BRC7 BLOCK18 Voltage(V)",
"SMBAT/BRC7 BLOCK19 Voltage(V)",
"SMBAT/BRC7 BLOCK20 Voltage(V)",
"SMBAT/BRC7 BLOCK21 Voltage(V)",
"SMBAT/BRC7 BLOCK22 Voltage(V)",
"SMBAT/BRC7 BLOCK23 Voltage(V)",
"SMBAT/BRC7 BLOCK24 Voltage(V)",
"SMBAT/BRC8 BLOCK1 Voltage(V)",
"SMBAT/BRC8 BLOCK2 Voltage(V)",
"SMBAT/BRC8 BLOCK3 Voltage(V)",
"SMBAT/BRC8 BLOCK4 Voltage(V)",
"SMBAT/BRC8 BLOCK5 Voltage(V)",
"SMBAT/BRC8 BLOCK6 Voltage(V)",
"SMBAT/BRC8 BLOCK7 Voltage(V)",
"SMBAT/BRC8 BLOCK8 Voltage(V)",
"SMBAT/BRC8 BLOCK9 Voltage(V)",
"SMBAT/BRC8 BLOCK10 Voltage(V)",
"SMBAT/BRC8 BLOCK11 Voltage(V)",
"SMBAT/BRC8 BLOCK12 Voltage(V)",
"SMBAT/BRC8 BLOCK13 Voltage(V)",
"SMBAT/BRC8 BLOCK14 Voltage(V)",
"SMBAT/BRC8 BLOCK15 Voltage(V)",
"SMBAT/BRC8 BLOCK16 Voltage(V)",
"SMBAT/BRC8 BLOCK17 Voltage(V)",
"SMBAT/BRC8 BLOCK18 Voltage(V)",
"SMBAT/BRC8 BLOCK19 Voltage(V)",
"SMBAT/BRC8 BLOCK20 Voltage(V)",
"SMBAT/BRC8 BLOCK21 Voltage(V)",
"SMBAT/BRC8 BLOCK22 Voltage(V)",
"SMBAT/BRC8 BLOCK23 Voltage(V)",
"SMBAT/BRC8 BLOCK24 Voltage(V)",
"SMBAT/BRC9 BLOCK1 Voltage(V)",
"SMBAT/BRC9 BLOCK2 Voltage(V)",
"SMBAT/BRC9 BLOCK3 Voltage(V)",
"SMBAT/BRC9 BLOCK4 Voltage(V)",
"SMBAT/BRC9 BLOCK5 Voltage(V)",
"SMBAT/BRC9 BLOCK6 Voltage(V)",
"SMBAT/BRC9 BLOCK7 Voltage(V)",
"SMBAT/BRC9 BLOCK8 Voltage(V)",
"SMBAT/BRC9 BLOCK9 Voltage(V)",
"SMBAT/BRC9 BLOCK10 Voltage(V)",
"SMBAT/BRC9 BLOCK11 Voltage(V)",
"SMBAT/BRC9 BLOCK12 Voltage(V)",
"SMBAT/BRC9 BLOCK13 Voltage(V)",
"SMBAT/BRC9 BLOCK14 Voltage(V)",
"SMBAT/BRC9 BLOCK15 Voltage(V)",
"SMBAT/BRC9 BLOCK16 Voltage(V)",
"SMBAT/BRC9 BLOCK17 Voltage(V)",
"SMBAT/BRC9 BLOCK18 Voltage(V)",
"SMBAT/BRC9 BLOCK19 Voltage(V)",
"SMBAT/BRC9 BLOCK20 Voltage(V)",
"SMBAT/BRC9 BLOCK21 Voltage(V)",
"SMBAT/BRC9 BLOCK22 Voltage(V)",
"SMBAT/BRC9 BLOCK23 Voltage(V)",
"SMBAT/BRC9 BLOCK24 Voltage(V)",
"SMBAT/BRC10 BLOCK1 Voltage(V)",
"SMBAT/BRC10 BLOCK2 Voltage(V)",
"SMBAT/BRC10 BLOCK3 Voltage(V)",
"SMBAT/BRC10 BLOCK4 Voltage(V)",
"SMBAT/BRC10 BLOCK5 Voltage(V)",
"SMBAT/BRC10 BLOCK6 Voltage(V)",
"SMBAT/BRC10 BLOCK7 Voltage(V)",
"SMBAT/BRC10 BLOCK8 Voltage(V)",
"SMBAT/BRC10 BLOCK9 Voltage(V)",
"SMBAT/BRC10 BLOCK10 Voltage(V)",
"SMBAT/BRC10 BLOCK11 Voltage(V)",
"SMBAT/BRC10 BLOCK12 Voltage(V)",
"SMBAT/BRC10 BLOCK13 Voltage(V)",
"SMBAT/BRC10 BLOCK14 Voltage(V)",
"SMBAT/BRC10 BLOCK15 Voltage(V)",
"SMBAT/BRC10 BLOCK16 Voltage(V)",
"SMBAT/BRC10 BLOCK17 Voltage(V)",
"SMBAT/BRC10 BLOCK18 Voltage(V)",
"SMBAT/BRC10 BLOCK19 Voltage(V)",
"SMBAT/BRC10 BLOCK20 Voltage(V)",
"SMBAT/BRC10 BLOCK21 Voltage(V)",
"SMBAT/BRC10 BLOCK22 Voltage(V)",
"SMBAT/BRC10 BLOCK23 Voltage(V)",
"SMBAT/BRC10 BLOCK24 Voltage(V)",
"SMBAT/BRC11 BLOCK1 Voltage(V)",
"SMBAT/BRC11 BLOCK2 Voltage(V)",
"SMBAT/BRC11 BLOCK3 Voltage(V)",
"SMBAT/BRC11 BLOCK4 Voltage(V)",
"SMBAT/BRC11 BLOCK5 Voltage(V)",
"SMBAT/BRC11 BLOCK6 Voltage(V)",
"SMBAT/BRC11 BLOCK7 Voltage(V)",
"SMBAT/BRC11 BLOCK8 Voltage(V)",
"SMBAT/BRC11 BLOCK9 Voltage(V)",
"SMBAT/BRC11 BLOCK10 Voltage(V)",
"SMBAT/BRC11 BLOCK11 Voltage(V)",
"SMBAT/BRC11 BLOCK12 Voltage(V)",
"SMBAT/BRC11 BLOCK13 Voltage(V)",
"SMBAT/BRC11 BLOCK14 Voltage(V)",
"SMBAT/BRC11 BLOCK15 Voltage(V)",
"SMBAT/BRC11 BLOCK16 Voltage(V)",
"SMBAT/BRC11 BLOCK17 Voltage(V)",
"SMBAT/BRC11 BLOCK18 Voltage(V)",
"SMBAT/BRC11 BLOCK19 Voltage(V)",
"SMBAT/BRC11 BLOCK20 Voltage(V)",
"SMBAT/BRC11 BLOCK21 Voltage(V)",
"SMBAT/BRC11 BLOCK22 Voltage(V)",
"SMBAT/BRC11 BLOCK23 Voltage(V)",
"SMBAT/BRC11 BLOCK24 Voltage(V)",
"SMBAT/BRC12 BLOCK1 Voltage(V)",
"SMBAT/BRC12 BLOCK2 Voltage(V)",
"SMBAT/BRC12 BLOCK3 Voltage(V)",
"SMBAT/BRC12 BLOCK4 Voltage(V)",
"SMBAT/BRC12 BLOCK5 Voltage(V)",
"SMBAT/BRC12 BLOCK6 Voltage(V)",
"SMBAT/BRC12 BLOCK7 Voltage(V)",
"SMBAT/BRC12 BLOCK8 Voltage(V)",
"SMBAT/BRC12 BLOCK9 Voltage(V)",
"SMBAT/BRC12 BLOCK10 Voltage(V)",
"SMBAT/BRC12 BLOCK11 Voltage(V)",
"SMBAT/BRC12 BLOCK12 Voltage(V)",
"SMBAT/BRC12 BLOCK13 Voltage(V)",
"SMBAT/BRC12 BLOCK14 Voltage(V)",
"SMBAT/BRC12 BLOCK15 Voltage(V)",
"SMBAT/BRC12 BLOCK16 Voltage(V)",
"SMBAT/BRC12 BLOCK17 Voltage(V)",
"SMBAT/BRC12 BLOCK18 Voltage(V)",
"SMBAT/BRC12 BLOCK19 Voltage(V)",
"SMBAT/BRC12 BLOCK20 Voltage(V)",
"SMBAT/BRC12 BLOCK21 Voltage(V)",
"SMBAT/BRC12 BLOCK22 Voltage(V)",
"SMBAT/BRC12 BLOCK23 Voltage(V)",
"SMBAT/BRC12 BLOCK24 Voltage(V)",
"SMBAT/BRC13 BLOCK1 Voltage(V)",
"SMBAT/BRC13 BLOCK2 Voltage(V)",
"SMBAT/BRC13 BLOCK3 Voltage(V)",
"SMBAT/BRC13 BLOCK4 Voltage(V)",
"SMBAT/BRC13 BLOCK5 Voltage(V)",
"SMBAT/BRC13 BLOCK6 Voltage(V)",
"SMBAT/BRC13 BLOCK7 Voltage(V)",
"SMBAT/BRC13 BLOCK8 Voltage(V)",
"SMBAT/BRC13 BLOCK9 Voltage(V)",
"SMBAT/BRC13 BLOCK10 Voltage(V)",
"SMBAT/BRC13 BLOCK11 Voltage(V)",
"SMBAT/BRC13 BLOCK12 Voltage(V)",
"SMBAT/BRC13 BLOCK13 Voltage(V)",
"SMBAT/BRC13 BLOCK14 Voltage(V)",
"SMBAT/BRC13 BLOCK15 Voltage(V)",
"SMBAT/BRC13 BLOCK16 Voltage(V)",
"SMBAT/BRC13 BLOCK17 Voltage(V)",
"SMBAT/BRC13 BLOCK18 Voltage(V)",
"SMBAT/BRC13 BLOCK19 Voltage(V)",
"SMBAT/BRC13 BLOCK20 Voltage(V)",
"SMBAT/BRC13 BLOCK21 Voltage(V)",
"SMBAT/BRC13 BLOCK22 Voltage(V)",
"SMBAT/BRC13 BLOCK23 Voltage(V)",
"SMBAT/BRC13 BLOCK24 Voltage(V)",
"SMBAT/BRC14 BLOCK1 Voltage(V)",
"SMBAT/BRC14 BLOCK2 Voltage(V)",
"SMBAT/BRC14 BLOCK3 Voltage(V)",
"SMBAT/BRC14 BLOCK4 Voltage(V)",
"SMBAT/BRC14 BLOCK5 Voltage(V)",
"SMBAT/BRC14 BLOCK6 Voltage(V)",
"SMBAT/BRC14 BLOCK7 Voltage(V)",
"SMBAT/BRC14 BLOCK8 Voltage(V)",
"SMBAT/BRC14 BLOCK9 Voltage(V)",
"SMBAT/BRC14 BLOCK10 Voltage(V)",
"SMBAT/BRC14 BLOCK11 Voltage(V)",
"SMBAT/BRC14 BLOCK12 Voltage(V)",
"SMBAT/BRC14 BLOCK13 Voltage(V)",
"SMBAT/BRC14 BLOCK14 Voltage(V)",
"SMBAT/BRC14 BLOCK15 Voltage(V)",
"SMBAT/BRC14 BLOCK16 Voltage(V)",
"SMBAT/BRC14 BLOCK17 Voltage(V)",
"SMBAT/BRC14 BLOCK18 Voltage(V)",
"SMBAT/BRC14 BLOCK19 Voltage(V)",
"SMBAT/BRC14 BLOCK20 Voltage(V)",
"SMBAT/BRC14 BLOCK21 Voltage(V)",
"SMBAT/BRC14 BLOCK22 Voltage(V)",
"SMBAT/BRC14 BLOCK23 Voltage(V)",
"SMBAT/BRC14 BLOCK24 Voltage(V)",
"SMBAT/BRC15 BLOCK1 Voltage(V)",
"SMBAT/BRC15 BLOCK2 Voltage(V)",
"SMBAT/BRC15 BLOCK3 Voltage(V)",
"SMBAT/BRC15 BLOCK4 Voltage(V)",
"SMBAT/BRC15 BLOCK5 Voltage(V)",
"SMBAT/BRC15 BLOCK6 Voltage(V)",
"SMBAT/BRC15 BLOCK7 Voltage(V)",
"SMBAT/BRC15 BLOCK8 Voltage(V)",
"SMBAT/BRC15 BLOCK9 Voltage(V)",
"SMBAT/BRC15 BLOCK10 Voltage(V)",
"SMBAT/BRC15 BLOCK11 Voltage(V)",
"SMBAT/BRC15 BLOCK12 Voltage(V)",
"SMBAT/BRC15 BLOCK13 Voltage(V)",
"SMBAT/BRC15 BLOCK14 Voltage(V)",
"SMBAT/BRC15 BLOCK15 Voltage(V)",
"SMBAT/BRC15 BLOCK16 Voltage(V)",
"SMBAT/BRC15 BLOCK17 Voltage(V)",
"SMBAT/BRC15 BLOCK18 Voltage(V)",
"SMBAT/BRC15 BLOCK19 Voltage(V)",
"SMBAT/BRC15 BLOCK20 Voltage(V)",
"SMBAT/BRC15 BLOCK21 Voltage(V)",
"SMBAT/BRC15 BLOCK22 Voltage(V)",
"SMBAT/BRC15 BLOCK23 Voltage(V)",
"SMBAT/BRC15 BLOCK24 Voltage(V)",
"SMBAT/BRC16 BLOCK1 Voltage(V)",
"SMBAT/BRC16 BLOCK2 Voltage(V)",
"SMBAT/BRC16 BLOCK3 Voltage(V)",
"SMBAT/BRC16 BLOCK4 Voltage(V)",
"SMBAT/BRC16 BLOCK5 Voltage(V)",
"SMBAT/BRC16 BLOCK6 Voltage(V)",
"SMBAT/BRC16 BLOCK7 Voltage(V)",
"SMBAT/BRC16 BLOCK8 Voltage(V)",
"SMBAT/BRC16 BLOCK9 Voltage(V)",
"SMBAT/BRC16 BLOCK10 Voltage(V)",
"SMBAT/BRC16 BLOCK11 Voltage(V)",
"SMBAT/BRC16 BLOCK12 Voltage(V)",
"SMBAT/BRC16 BLOCK13 Voltage(V)",
"SMBAT/BRC16 BLOCK14 Voltage(V)",
"SMBAT/BRC16 BLOCK15 Voltage(V)",
"SMBAT/BRC16 BLOCK16 Voltage(V)",
"SMBAT/BRC16 BLOCK17 Voltage(V)",
"SMBAT/BRC16 BLOCK18 Voltage(V)",
"SMBAT/BRC16 BLOCK19 Voltage(V)",
"SMBAT/BRC16 BLOCK20 Voltage(V)",
"SMBAT/BRC16 BLOCK21 Voltage(V)",
"SMBAT/BRC16 BLOCK22 Voltage(V)",
"SMBAT/BRC16 BLOCK23 Voltage(V)",
"SMBAT/BRC16 BLOCK24 Voltage(V)",
"SMBAT/BRC17 BLOCK1 Voltage(V)",
"SMBAT/BRC17 BLOCK2 Voltage(V)",
"SMBAT/BRC17 BLOCK3 Voltage(V)",
"SMBAT/BRC17 BLOCK4 Voltage(V)",
"SMBAT/BRC17 BLOCK5 Voltage(V)",
"SMBAT/BRC17 BLOCK6 Voltage(V)",
"SMBAT/BRC17 BLOCK7 Voltage(V)",
"SMBAT/BRC17 BLOCK8 Voltage(V)",
"SMBAT/BRC17 BLOCK9 Voltage(V)",
"SMBAT/BRC17 BLOCK10 Voltage(V)",
"SMBAT/BRC17 BLOCK11 Voltage(V)",
"SMBAT/BRC17 BLOCK12 Voltage(V)",
"SMBAT/BRC17 BLOCK13 Voltage(V)",
"SMBAT/BRC17 BLOCK14 Voltage(V)",
"SMBAT/BRC17 BLOCK15 Voltage(V)",
"SMBAT/BRC17 BLOCK16 Voltage(V)",
"SMBAT/BRC17 BLOCK17 Voltage(V)",
"SMBAT/BRC17 BLOCK18 Voltage(V)",
"SMBAT/BRC17 BLOCK19 Voltage(V)",
"SMBAT/BRC17 BLOCK20 Voltage(V)",
"SMBAT/BRC17 BLOCK21 Voltage(V)",
"SMBAT/BRC17 BLOCK22 Voltage(V)",
"SMBAT/BRC17 BLOCK23 Voltage(V)",
"SMBAT/BRC17 BLOCK24 Voltage(V)",
"SMBAT/BRC18 BLOCK1 Voltage(V)",
"SMBAT/BRC18 BLOCK2 Voltage(V)",
"SMBAT/BRC18 BLOCK3 Voltage(V)",
"SMBAT/BRC18 BLOCK4 Voltage(V)",
"SMBAT/BRC18 BLOCK5 Voltage(V)",
"SMBAT/BRC18 BLOCK6 Voltage(V)",
"SMBAT/BRC18 BLOCK7 Voltage(V)",
"SMBAT/BRC18 BLOCK8 Voltage(V)",
"SMBAT/BRC18 BLOCK9 Voltage(V)",
"SMBAT/BRC18 BLOCK10 Voltage(V)",
"SMBAT/BRC18 BLOCK11 Voltage(V)",
"SMBAT/BRC18 BLOCK12 Voltage(V)",
"SMBAT/BRC18 BLOCK13 Voltage(V)",
"SMBAT/BRC18 BLOCK14 Voltage(V)",
"SMBAT/BRC18 BLOCK15 Voltage(V)",
"SMBAT/BRC18 BLOCK16 Voltage(V)",
"SMBAT/BRC18 BLOCK17 Voltage(V)",
"SMBAT/BRC18 BLOCK18 Voltage(V)",
"SMBAT/BRC18 BLOCK19 Voltage(V)",
"SMBAT/BRC18 BLOCK20 Voltage(V)",
"SMBAT/BRC18 BLOCK21 Voltage(V)",
"SMBAT/BRC18 BLOCK22 Voltage(V)",
"SMBAT/BRC18 BLOCK23 Voltage(V)",
"SMBAT/BRC18 BLOCK24 Voltage(V)",
"SMBAT/BRC19 BLOCK1 Voltage(V)",
"SMBAT/BRC19 BLOCK2 Voltage(V)",
"SMBAT/BRC19 BLOCK3 Voltage(V)",
"SMBAT/BRC19 BLOCK4 Voltage(V)",
"SMBAT/BRC19 BLOCK5 Voltage(V)",
"SMBAT/BRC19 BLOCK6 Voltage(V)",
"SMBAT/BRC19 BLOCK7 Voltage(V)",
"SMBAT/BRC19 BLOCK8 Voltage(V)",
"SMBAT/BRC19 BLOCK9 Voltage(V)",
"SMBAT/BRC19 BLOCK10 Voltage(V)",
"SMBAT/BRC19 BLOCK11 Voltage(V)",
"SMBAT/BRC19 BLOCK12 Voltage(V)",
"SMBAT/BRC19 BLOCK13 Voltage(V)",
"SMBAT/BRC19 BLOCK14 Voltage(V)",
"SMBAT/BRC19 BLOCK15 Voltage(V)",
"SMBAT/BRC19 BLOCK16 Voltage(V)",
"SMBAT/BRC19 BLOCK17 Voltage(V)",
"SMBAT/BRC19 BLOCK18 Voltage(V)",
"SMBAT/BRC19 BLOCK19 Voltage(V)",
"SMBAT/BRC19 BLOCK20 Voltage(V)",
"SMBAT/BRC19 BLOCK21 Voltage(V)",
"SMBAT/BRC19 BLOCK22 Voltage(V)",
"SMBAT/BRC19 BLOCK23 Voltage(V)",
"SMBAT/BRC19 BLOCK24 Voltage(V)",
"SMBAT/BRC20 BLOCK1 Voltage(V)",
"SMBAT/BRC20 BLOCK2 Voltage(V)",
"SMBAT/BRC20 BLOCK3 Voltage(V)",
"SMBAT/BRC20 BLOCK4 Voltage(V)",
"SMBAT/BRC20 BLOCK5 Voltage(V)",
"SMBAT/BRC20 BLOCK6 Voltage(V)",
"SMBAT/BRC20 BLOCK7 Voltage(V)",
"SMBAT/BRC20 BLOCK8 Voltage(V)",
"SMBAT/BRC20 BLOCK9 Voltage(V)",
"SMBAT/BRC20 BLOCK10 Voltage(V)",
"SMBAT/BRC20 BLOCK11 Voltage(V)",
"SMBAT/BRC20 BLOCK12 Voltage(V)",
"SMBAT/BRC20 BLOCK13 Voltage(V)",
"SMBAT/BRC20 BLOCK14 Voltage(V)",
"SMBAT/BRC20 BLOCK15 Voltage(V)",
"SMBAT/BRC20 BLOCK16 Voltage(V)",
"SMBAT/BRC20 BLOCK17 Voltage(V)",
"SMBAT/BRC20 BLOCK18 Voltage(V)",
"SMBAT/BRC20 BLOCK19 Voltage(V)",
"SMBAT/BRC20 BLOCK20 Voltage(V)",
"SMBAT/BRC20 BLOCK21 Voltage(V)",
"SMBAT/BRC20 BLOCK22 Voltage(V)",
"SMBAT/BRC20 BLOCK23 Voltage(V)",
"SMBAT/BRC20 BLOCK24 Voltage(V)",
"EIB1Block1Voltage(V)",	
"EIB1Block2Voltage(V)",	
"EIB1Block3Voltage(V)",	
"EIB1Block4Voltage(V)",	
"EIB1Block5Voltage(V)",	
"EIB1Block6Voltage(V)",	
"EIB1Block7Voltage(V)",	
"EIB1Block8Voltage(V)",	
"EIB2Block1Voltage(V)",	
"EIB2Block2Voltage(V)",	
"EIB2Block3Voltage(V)",	
"EIB2Block4Voltage(V)",	
"EIB2Block5Voltage(V)",	
"EIB2Block6Voltage(V)",	
"EIB2Block7Voltage(V)",	
"EIB2Block8Voltage(V)",	
"EIB3Block1Voltage(V)",	
"EIB3Block2Voltage(V)",	
"EIB3Block3Voltage(V)",	
"EIB3Block4Voltage(V)",	
"EIB3Block5Voltage(V)",	
"EIB3Block6Voltage(V)",	
"EIB3Block7Voltage(V)",	
"EIB3Block8Voltage(V)",	
"EIB4Block1Voltage(V)",	
"EIB4Block2Voltage(V)",	
"EIB4Block3Voltage(V)",	
"EIB4Block4Voltage(V)",	
"EIB4Block5Voltage(V)",	
"EIB4Block6Voltage(V)",	
"EIB4Block7Voltage(V)",	
"EIB4Block8Voltage(V)",	
"Temperature1(deg.C)",	
"Temperature2(deg.C)",		
"Temperature3(deg.C)",		
"Temperature4(deg.C)",		
"Temperature5(deg.C)",		
"Temperature6(deg.C)",		
"Temperature7(deg.C)"};

#define MAX_BATT_NUM 114

/*==========================================================================*
* FUNCTION :    Web_MakeQueryBatteryTestLogBuffer
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   IN int iQueryTimes:
OUT char **ppBuf:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-6-19 11:20
*==========================================================================*/
static int Web_MakeQueryBatteryTestLogBuffer(IN int iQueryTimes, 
					     OUT char **ppBuf)
{
	BATTERY_LOG_INFO		*pBatteryLogInfo	= NULL;
	BATTERY_HEAD_INFO		*stGeneralInfo = NULL;
	char					*pMakeBuf = NULL, *pFileMakeBuf = NULL;
	int						iLen = 0, iFileLen = 0;
	int						iQtyBatt, iQtyRecord;
	int						i = 0, k = 0, j = 0;
	char					szTime1[32], szTime2[32];
	//int						iBatteryNum = 0;
	time_t					tmUTC;
	BATTERY_GENERAL_INFO			stOutBatteryGeneralInfo;
	BATTERY_GENERAL_INFO1			stOutBatteryGeneralInfo1;
	BATTERY_LOG_INFO1			stBatteryLogInfo1;
	int					iStatus[1024];
	BYTE					byTest[1024];


	char					szBatteryStartReason[5][128]={"Start Planned Test",
		"Start Manual Test",
		"Start AC Fail Test",
		"Start Master Battery Test",
		"Start Test for Other Reasons"};
	char					szBatteryEndReason[12][128]={"End Test Manually",
		"End Test for Alarm",
		"End Test for Test Time-Out",
		"End Test for Capacity Condition",
		"End Test for Voltage Condition",
		"End Test for AC Fail",
		"End AC Fail Test for AC Restore",
		"End AC Fail Test for Being Disabled",
		"End Master Battery Test",
		"End PowerSplit BT for Auto to Manual",
		"End PowerSplit Man-BT for Manual to Auto",
		"End Test for Other Reasons"};
	char					szBatteryTestResult[5][64]={"No Result",
		"Battery is OK",
		"Battery is Bad",
		"It's PowerSplit Test",
		"Other Result"};


	for(i = 0; i < 1024; i++)
	{
		iStatus[i] = 0;
	}
	TRACE_WEB_USER_NOT_CYCLE("Now inquire the battery log\n");
	if(Web_QueryBatteryTestLog(iQueryTimes, &stGeneralInfo, &pBatteryLogInfo, &stOutBatteryGeneralInfo) != TRUE)//ERR_WEB_COMM_GET_RESULT_FAIL)
	{
		return FALSE;
	}

	transferTostOutBatteryGeneralInfo1(&stOutBatteryGeneralInfo1, stOutBatteryGeneralInfo);// ת��Ϊ��׼�ṹ����

	iQtyBatt	= stGeneralInfo->btSummary.iQtyBatt;
	iQtyRecord	= stGeneralInfo->btSummary.iQtyRecord;

	TRACE_WEB_USER_NOT_CYCLE("iQtyBatt is %d, iQtyRecord is %d\n", iQtyBatt, iQtyRecord);

	pMakeBuf = NEW(char, 20 * MAX_QUERY_BATTERY_GROUP_LEN * MAX_QUERY_BATTERY_GROUP_NUM);
	pFileMakeBuf = NEW(char, 30 * MAX_QUERY_BATTERY_GROUP_LEN * MAX_QUERY_BATTERY_GROUP_NUM);
	if(pMakeBuf == NULL || pFileMakeBuf == NULL)
	{
		if(pBatteryLogInfo != NULL)
		{
			DELETE(pBatteryLogInfo);
			pBatteryLogInfo = NULL;
		}
		if(pMakeBuf != NULL)
		{
			DELETE(pMakeBuf);
			pMakeBuf = NULL;
		}
		if(pFileMakeBuf != NULL)
		{
			DELETE(pFileMakeBuf);
			pFileMakeBuf = NULL;
		}
		return FALSE;
	}

	iLen += sprintf(pMakeBuf + iLen, "[");
	iLen += sprintf(pMakeBuf + iLen,"[%d,%d,%d,%d,%d],",
		(int)stGeneralInfo->btSummary.tStartTime,//szTime1,
		(int)stGeneralInfo->btSummary.tEndTime,//szTime2,
		stGeneralInfo->btSummary.iStartReason,
		stGeneralInfo->btSummary.iEndReason,
		stGeneralInfo->btSummary.iTestResult);

	//print test log status	
	iLen += sprintf(pMakeBuf + iLen,"[%d,%d,%d,",1,1,1 );

	memset(byTest, 0x0, sizeof(byTest));
	memcpy(byTest, &stOutBatteryGeneralInfo, sizeof(byTest));
	for(i = 0; i < MAX_BATT_NUM; i++)
	{
		/*iLen += sprintf(pMakeBuf + iLen," %d,%d,%d,",(stOutBatteryGeneralInfo.BatteryInfo[i].byBatteryCurrent == 1) ? 1 : 0, (stOutBatteryGeneralInfo.BatteryInfo[i].byBatteryVolage == 1) ? 1 : 0 ,(stOutBatteryGeneralInfo.BatteryInfo[i].byBatteryCapacity== 1) ? 1 : 0);
		iStatus[i*3 + 0] =(stOutBatteryGeneralInfo.BatteryInfo[i].byBatteryCurrent == 1) ? 1 : 0;
		iStatus[i*3 + 1] =(stOutBatteryGeneralInfo.BatteryInfo[i].byBatteryVolage == 1) ? 1 : 0;
		iStatus[i*3 + 2] =(stOutBatteryGeneralInfo.BatteryInfo[i].byBatteryCapacity == 1) ? 1 : 0;*/
		iLen += sprintf(pMakeBuf + iLen,"%d,%d,%d,",(stOutBatteryGeneralInfo1.AllTypeBatt[i].byBatteryCurrent == 1) ? 1 : 0, (stOutBatteryGeneralInfo1.AllTypeBatt[i].byBatteryVolage == 1) ? 1 : 0 ,(stOutBatteryGeneralInfo1.AllTypeBatt[i].byBatteryCapacity== 1) ? 1 : 0);
		iStatus[i*3 + 0] =(stOutBatteryGeneralInfo1.AllTypeBatt[i].byBatteryCurrent == 1) ? 1 : 0;
		iStatus[i*3 + 1] =(stOutBatteryGeneralInfo1.AllTypeBatt[i].byBatteryVolage == 1) ? 1 : 0;
		iStatus[i*3 + 2] =(stOutBatteryGeneralInfo1.AllTypeBatt[i].byBatteryCapacity == 1) ? 1 : 0;

	}

	for(i = 0; i < 20; i++)
	{
		for(j = 0; j < 24; j++)
		{
			iLen += sprintf(pMakeBuf + iLen,"%d,",(stOutBatteryGeneralInfo.stSmbatSmbrcBlock[i].bySMBlockVoltage[j] == 1) ? 1 : 0);
			iStatus[MAX_BATT_NUM * 3 + i * 24 + j] =(stOutBatteryGeneralInfo.stSmbatSmbrcBlock[i].bySMBlockVoltage[j] == 1) ? 1 : 0;
		}
	}

	for(i = 0; i < 4; i++)
	{
		for(j = 0; j < 8; j++)
		{
			iLen += sprintf(pMakeBuf + iLen,"%d,",(stOutBatteryGeneralInfo.stBatteryBlock[i].byBlockVoltage[j] == 1) ? 1 : 0);
			iStatus[MAX_BATT_NUM * 3 + 480 + i*8 + j] =(stOutBatteryGeneralInfo.stBatteryBlock[i].byBlockVoltage[j] == 1) ? 1 : 0;
			//TRACE_WEB_USER("Battery %d Block %d = %d",i, j, stOutBatteryGeneralInfo.stBatteryBlock[i].byBlockVoltage[j]);
			//iLen += sprintf(pMakeBuf + iLen,"%d,",(byTest[i + iQtyBatt * 3]== 1) ? 1 : 0);
			//iStatus[iQtyBatt*3 + i] =(byTest[i + iQtyBatt * 3]== 1) ? 1 : 0;
		}
	}

	for(i = 0; i < 7; i++)
	{
		if(i == 6)
		{
			iLen += sprintf(pMakeBuf + iLen,"%d",(stOutBatteryGeneralInfo.byTemperatureInfo[i] == 1) ? 1 : 0 );
		}
		else
		{
			iLen += sprintf(pMakeBuf + iLen,"%d,",(stOutBatteryGeneralInfo.byTemperatureInfo[i] == 1) ? 1 : 0 );
		}
		iStatus[MAX_BATT_NUM * 3 + 480 + 32 + i] =(stOutBatteryGeneralInfo.byTemperatureInfo[i] == 1) ? 1 : 0  ;

	}
	iLen += sprintf(pMakeBuf + iLen, "],");
	/*for(i = 0; i < 861; i++)
	{
	TRACE_WEB_USER("iStatus[%d] = %d    ",i, iStatus[i]);
	}
	TRACE_WEB_USER("\n");*/
	//print test result	
	for(i = 0; i < iQtyRecord; i++)
	{
		/*print the common information of every battery test record*/
		tmUTC = pBatteryLogInfo[i].tRecordTime;

		TimeToString(tmUTC, TIME_CHN_FMT, 
			szTime1, sizeof(szTime1));


		iLen += sprintf(pMakeBuf + iLen, "[%d,%ld,%8.2f,", (i + 1), tmUTC, pBatteryLogInfo[i].fVoltage);
		iFileLen += sprintf(pFileMakeBuf + iFileLen, "\t<tr><td>%6d</td> \t<td>%-32s</td> \t<td>%-8.2f</td> ",  (i + 1), szTime1, pBatteryLogInfo[i].fVoltage);

		iFileLen += sprintf(pFileMakeBuf + iFileLen, "\t\t%s"," ");
		transferTostBatteryLogInfo1(&stBatteryLogInfo1, pBatteryLogInfo[i]);// ת��Ϊ��׼�ṹ����
		for(k = 0; k < MAX_BATT_NUM; k++)		{


			if(iStatus[k*3 + 0])
			{
				/*iLen += sprintf(pMakeBuf + iLen, "<td>%8.2f</td> ",pBatteryLogInfo[i].pBatteryRealData[k].fBatCurrent);
				iFileLen += sprintf(pFileMakeBuf + iFileLen, " \t%-16.2f \t",	
				pBatteryLogInfo[i].pBatteryRealData[k].fBatCurrent);
				TRACE_WEB_USER("fBatCurrent %d is %f\n", k, pBatteryLogInfo[i].pBatteryRealData[k].fBatCurrent);*/
				iLen += sprintf(pMakeBuf + iLen, "%8.2f,",stBatteryLogInfo1.AllTypeData[k].fBatCurrent);
				iFileLen += sprintf(pFileMakeBuf + iFileLen, " \t<td>%-16.2f</td> \t",	
					stBatteryLogInfo1.AllTypeData[k].fBatCurrent);
				//TRACE_WEB_USER("fBatCurrent %d is %f\n", k, stBatteryLogInfo1.AllTypeData[k].fBatCurrent);


			}
			if(iStatus[k*3 + 1])
			{
				/*iLen += sprintf(pMakeBuf + iLen, "<td>%8.2f</td> ",pBatteryLogInfo[i].pBatteryRealData[k].fBatVoltage);
				iFileLen += sprintf(pFileMakeBuf + iFileLen, " \t%-16.2f \t",	
				pBatteryLogInfo[i].pBatteryRealData[k].fBatVoltage);
				TRACE_WEB_USER("fBatVoltage %d is %f\n", k, pBatteryLogInfo[i].pBatteryRealData[k].fBatVoltage);*/
				iLen += sprintf(pMakeBuf + iLen, "%8.2f,",stBatteryLogInfo1.AllTypeData[k].fBatVoltage);
				iFileLen += sprintf(pFileMakeBuf + iFileLen, " \t<td>%-16.2f</td> \t",	
					stBatteryLogInfo1.AllTypeData[k].fBatVoltage);
				//TRACE_WEB_USER("fBatVoltage %d is %f\n", k, stBatteryLogInfo1.AllTypeData[k].fBatVoltage);

			}

			if(iStatus[k*3 + 2])
			{
				/*iLen += sprintf(pMakeBuf + iLen, "<td>%8.0f</td> ",pBatteryLogInfo[i].pBatteryRealData[k].fBatCapacity);
				iFileLen += sprintf(pFileMakeBuf + iFileLen, " \t%-16.0f \t",	
				pBatteryLogInfo[i].pBatteryRealData[k].fBatCapacity);		
				TRACE_WEB_USER("fBatCapacity %d is %f\n", k, pBatteryLogInfo[i].pBatteryRealData[k].fBatCapacity);*/
				iLen += sprintf(pMakeBuf + iLen, "%8.0f,",stBatteryLogInfo1.AllTypeData[k].fBatCapacity);
				iFileLen += sprintf(pFileMakeBuf + iFileLen, " \t<td>%-16.0f</td> \t",	
					stBatteryLogInfo1.AllTypeData[k].fBatCapacity);		
				//TRACE_WEB_USER("fBatCapacity %d is %f\n", k, stBatteryLogInfo1.AllTypeData[k].fBatCapacity);
			}			

		}

		for(j = 0; j < 480; j++)
		{
			if(iStatus[MAX_BATT_NUM * 3 + j])
			{
				iLen += sprintf(pMakeBuf + iLen, "%8.2f,", pBatteryLogInfo[i].fBatSMBlock[j / 24].fSMBlockVoltage[j % 24]);
				iFileLen += sprintf(pFileMakeBuf + iFileLen, "\t<td>%-22.2f</td> ", pBatteryLogInfo[i].fBatSMBlock[j / 24].fSMBlockVoltage[j % 24]);
			}


		}

		for(j = 0; j < 32; j++)
		{
			if(iStatus[MAX_BATT_NUM * 3 + 480 + j])
			{
				iLen += sprintf(pMakeBuf + iLen, "%8.2f,", pBatteryLogInfo[i].fBatBlock[j/8].fBatBlockVoltage[j%8]);
				iFileLen += sprintf(pFileMakeBuf + iFileLen, "\t<td>%-22.2f</td> ", pBatteryLogInfo[i].fBatBlock[j/8].fBatBlockVoltage[j%8]);
			}


		}

		for(j = 0; j < 7; j++)
		{
			if(iStatus[MAX_BATT_NUM * 3 + 480 + 32 + j])
			{
				iLen += sprintf(pMakeBuf + iLen, " %8.0f,",pBatteryLogInfo[i].fTemp[j]);
				if(j == 0)
				{
					iFileLen += sprintf(pFileMakeBuf + iFileLen, " <td>%-20.0f</td> ",pBatteryLogInfo[i].fTemp[j]);
				}
				else
				{
					iFileLen += sprintf(pFileMakeBuf + iFileLen, " \t<td>%-20.0f</td> ",pBatteryLogInfo[i].fTemp[j]);
				}

			}
		}
		if(iLen >= 1)
		{
			iLen = iLen - 1;	// ȥ�����һ�������Է���JSON��ʽ
		}
		iLen += sprintf(pMakeBuf + iLen, "],");
		iFileLen += sprintf(pFileMakeBuf + iFileLen, "</tr>\n");
	}
	if(iLen >= 1)
	{
		iLen = iLen - 1;	// ȥ�����һ�������Է���JSON��ʽ
	}
	iLen += sprintf(pMakeBuf + iLen, "]");

	*ppBuf = pMakeBuf;

	FILE *fp;
	if((fp = fopen(WEB_LOG_DIR_BT,"wb")) != NULL)
	{

		TimeToString(stGeneralInfo->btSummary.tStartTime, TIME_CHN_FMT, 
			szTime1, sizeof(szTime1));

		TimeToString(stGeneralInfo->btSummary.tEndTime, TIME_CHN_FMT, 
			szTime2, sizeof(szTime2));

		char	szFileTitle[4096];
		iLen = sprintf(szFileTitle, "<html xmlns=\"http://www.w3.org/1999/xhtml\"> \n" \
		    "<head><meta	http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /></head><body> \n" \
		    "<style>body,html{padding:10px;margin:0;font-size:14px;line-height:25px;font-family:Arial;}table{border-left:#ddd solid 1px;border-top:#ddd solid 1px;margin-top:20px;}table td{border-right:#ddd solid 1px;border-bottom:#ddd solid 1px;height:30px;line-height:30px;padding:0 0 0 10px;font-size:14px;color:#333}table tr:first-child td{font-weight:bold;height:40px;line-height:40px;}</style>\n"\
		    "Query Battery Test Log<br/> \n" \
			"Start time: %s<br/> \n" \
			"End time  : %s<br/>\n" \
			"Start reason   : %s<br/>\n" \
			"End reason     : %s<br/>\n" \
			"Test result    : %s<br/>\n\n\n" \
			"<table border=\"1\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n" \
			"\t<tr><td>%6s</td> \t<td>%-32s</td> \t<td>%-16s</td>",
			szTime1,
			szTime2,
			szBatteryStartReason[stGeneralInfo->btSummary.iStartReason],
			szBatteryEndReason[stGeneralInfo->btSummary.iEndReason],
			szBatteryTestResult[stGeneralInfo->btSummary.iTestResult],
			"Index",
			"Record Time",
			"System Voltage(V)");
		i = 0;						
		while(i < 861)
		{

			if(iStatus[i])
			{
				iLen += sprintf(szFileTitle + iLen, "\t<td>%-20s</td> ", szBatterTitle[i]);
				i++;
			}
			else
			{
				i++;
			}
		}						
		sprintf(szFileTitle + iLen, "</tr>%s ", "\n");						



		fwrite(szFileTitle,strlen(szFileTitle), 1, fp);
		fwrite(pFileMakeBuf,strlen(pFileMakeBuf), 1, fp);
		//TRACE_WEB_USER("szFileTitle is %s\n", szFileTitle);
		memset(szFileTitle, 0, sizeof(szFileTitle));
		sprintf(szFileTitle, "</table></body>\n" \
		    "</html>\n");
		fwrite(szFileTitle,strlen(szFileTitle), 1, fp);
		fclose(fp);
	}

	TRACE_WEB_USER("pMakeBuf is %s\n", pMakeBuf);
	//TRACE_WEB_USER("pFileMakeBuf is %s\n", pFileMakeBuf);


	if(stGeneralInfo != NULL)
	{
		DELETE(stGeneralInfo);
		stGeneralInfo = NULL;
	}


	if(pBatteryLogInfo != NULL)
	{
		DELETE(pBatteryLogInfo);
		pBatteryLogInfo = NULL;
	}

	//added by wankun in order to avoid mem leak
	if(pFileMakeBuf != NULL )
	{
		DELETE(pFileMakeBuf);
		pFileMakeBuf = NULL;
	}
	//ended by wankun
	return TRUE;
}

/*==========================================================================*
* FUNCTION :    Web_SendQueryData
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   char *buf:
char *filename:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
*==========================================================================*/
static int Web_SendQueryData(IN char *buf, 
			     IN char *filename)
{
	char		*ptr	= NULL;				/*pointer to buf*/
	char		*pBuf	= NULL;				/*output buffer*/
	int			fd2;				/*fd2: handle of fifo*/
	int			iEquipID;			/**/
	int			iQueryType;			/*Query type*/
	int			iLanguage;			/*language*/
	time_t		toTime = 0, fromTime = 0;	/*query time*/
	char		szGetData[64];		/*temp buffer*/
	int			iQueryTimes;



	//open the fifo
	if((fd2 = open(filename,O_WRONLY  )) < 0)
	{
		AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_WARNING,"Fail to open FIFO");
		return FALSE;

	}

	/*offset 12 */
	ptr = buf +  MAX_COMM_PID_LEN + MAX_COMM_GET_TYPE;

	strncpyz(szGetData, ptr, COMM_QUERY_TYPE_LEN + 1);
	iQueryType = atoi(szGetData);
	//TRACE("iQueryType string is %s.\n",szGetData);
	ptr = ptr + COMM_QUERY_TYPE_LEN;

	/*get query time (from time)*/
	strncpyz(szGetData, ptr, COMM_QUERY_TIME_LEN + 1);
	//fromTime = Web_transferCharToTime(szGetData, TRUE);
	fromTime = (time_t)atoi(szGetData);
	//TRACE("fromTime string is %s.\n",szGetData);
	ptr = ptr + COMM_QUERY_TIME_LEN;


	/*get query time (to time)*/
	strncpyz(szGetData, ptr, COMM_QUERY_TIME_LEN + 1);
	//toTime = Web_transferCharToTime(szGetData, FALSE);
	toTime = (time_t)atoi(szGetData);
	//TRACE("toTime string is %s.\n",szGetData);
	ptr = ptr + COMM_QUERY_TIME_LEN;

	//#ifdef _SHOW_WEB_INFO
	////TRACE("fromTime[%d][%s]  toTime[%d][%s]\n",fromTime, ctime(&fromTime),toTime,"ctime(&toTime)");
	//#endif

	/*get query equipID */
	strncpyz(szGetData, ptr, MAX_EQUIPID_LEN + 1);
	iEquipID = atoi(szGetData);
	ptr = ptr + MAX_EQUIPID_LEN;

	/*get Language type*/
	strncpyz(szGetData, ptr, MAX_LANGUAGE_TYPE_LEN + 1);
	iLanguage = atoi(szGetData);

	//TRACE("In Web_SendQueryData iQueryType is %ld, fromTime %ld,toTime %ld\n",iQueryType, fromTime,toTime);
	int iReturn = 0;
	switch(iQueryType)
	{
	case QUERY_HIS_DATA:

		iReturn = Web_MakeQueryHisDataBuffer(iEquipID, 
			fromTime, 
			toTime, 
			iLanguage, 
			&pBuf);

		break;
	case QUERY_STAT_DATA:

		/*iReturn = Web_MakeQueryStatDataBuffer(iEquipID, 
		fromTime, 
		toTime, 
		iLanguage, 
		&pBuf);*/
		//TRACE("Web_MakeQueryStatDataBuffer[%d]\n", iReturn);
		break;
	case QUERY_HISALARM_DATA:
		TRACE_WEB_USER_NOT_CYCLE("Deal with QUERY_HISALARM_DATA\n");
		iReturn = Web_MakeQueryAlarmDataBuffer(iEquipID, 
			fromTime, 
			toTime, 
			iLanguage, 
			&pBuf);
		break;

	case QUERY_CONTROLCOMMAND_DATA:

		iReturn = Web_MakeQueryControlDataBuffer(fromTime, 
			toTime, 
			iLanguage, 
			&pBuf);
		break;

	case QUERY_BATTERYTEST_DATA:
		iQueryTimes = iEquipID;			//special for Battery test, 

		TRACE("Start to Web_MakeQueryBatteryTestLogBuffer");
		#define MAX_BATTERY_TEST_LOGS	10
		#define BATTERY_TEST_LOG_FILE  "/var/download/BatteryTestLog"
		char szCmd[100];
		//refer to the hard-coded value '10' in Web_QueryBatteryTestLog() for max Battery Test Logs
		//if this number is greater than 10; then it indicates that 'ALL' the 10 Battery Test Logs are to be generated
		if(iQueryTimes > MAX_BATTERY_TEST_LOGS)
		{
		    int i;
		    for(i=1; i<=MAX_BATTERY_TEST_LOGS; i++)
		    {
			iReturn = Web_MakeQueryBatteryTestLogBuffer(i, &pBuf);

			if(pBuf != NULL)
			{
			    DELETE(pBuf);
			    pBuf = NULL;
			}
			szCmd[0] = 0;
			if(iReturn != FALSE)
			{
			    sprintf(szCmd, "mv %s %s%d.html", WEB_LOG_DIR_BT, BATTERY_TEST_LOG_FILE, i);
			    _SYSTEM(szCmd);
			}
		    }
		}
		else
		{
			iReturn = Web_MakeQueryBatteryTestLogBuffer(iQueryTimes, &pBuf);
		}

		TRACE("End to Web_MakeQueryBatteryTestLogBuffer");
		break;
	//changed by Frank Wu,10/N/14,20140527, for system log
	case QUERY_HISLOG_DATA:
		TRACE("Start to Web_MakeQueryHisLogBuffer\n");
		iReturn = Web_MakeQueryHisLogBuffer(fromTime, toTime, &pBuf);
		TRACE("End to Web_MakeQueryHisLogBuffer\n");
		break;
	case QUERY_CLEAR_ALARM:
		//iReturn  = DAT_StorageDeleteRecord(HIST_ALARM_LOG);
		break;
	case QUERY_DISEL_TEST:
		//iReturn	 = Web_MakeQueryDiselTestLog(fromTime, toTime,iLanguage,&pBuf);
		break;
	default:
		break;
	}

	if(iReturn == FALSE)
	{

		pBuf = NEW(char, 3);
		sprintf(pBuf, "%2d", 2);
	}

	int iPipeLen = strlen(pBuf)/(PIPE_BUF - 1)  + 1;

#ifdef _SHOW_WEB_INFO
	//TRACE("iPipeLen = %d\n", iPipeLen);
#endif


	//#ifdef _DEBUG
	//	FILE *fp = fopen("/scup/www/html/query.htm","w");
	//	fwrite(pBuf,strlen(pBuf), 1, fp);
	//	fclose(fp);
	//#endif

	if(iPipeLen <= 1)
	{
		if((write(fd2, pBuf, strlen(pBuf) + 1)) < 0)
		{
			close(fd2);
			return FALSE;
		}
	}
	else
	{
		int m;
		char szP[PIPE_BUF - 1];
		for(m = 0; m < iPipeLen; m++)
		{
			strncpyz(szP, pBuf + m * (PIPE_BUF - 2), PIPE_BUF - 1);
#ifdef _SHOW_WEB_INFO
			//TRACE("m =%d \n", m);
#endif


			if((write(fd2, szP, PIPE_BUF - 1)) < 0)
			{
				close(fd2);
				return FALSE;
			}
		}

	}

	if(pBuf != NULL)
	{
		DELETE(pBuf);
		pBuf = NULL;
	}
	close(fd2);
	return TRUE;
}

/*==========================================================================*
* FUNCTION : str_ltrim
* PURPOSE  : No any memery changed, return the pointer that point to 
*            first non-space position in the string
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: char*  strSrc : 
* RETURN   : char* : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-02-25 14:55
*==========================================================================*/
char* str_ltrim(char* strSrc)
{
	char*	pStr;

	if(*(pStr = strSrc) != ' ')
	{
		return strSrc;
	}

	while (*(++pStr) == ' ');

	return pStr;
}

/*==========================================================================*
* FUNCTION :   Web_ModifySignalValue
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN int equipID:
IN int signalType:
IN int signalID:
IN VAR_VALUE value: 
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-21 11:20
*==========================================================================*/
static int Web_ModifySignalValue(IN int iEquipID,
				 IN int iSignalType,
				 IN int iSignalID,
				 IN VAR_VALUE_EX value)
{
	int			iError = -1;
	int			iBufLen = sizeof(VAR_VALUE_EX);
	int	iVarSubID = 0;
	char	szFile[128];
	int	iReturn = 0;
	char *pHtml = NULL;
	char *ptr = NULL;
	FILE    *fp;


	iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);


	//int iTimeout = 6000;
	int iTimeout = 12000;//changed by Frank Wu,1/1/1,20140801, for setting pages

	//changed by Frank Wu,34/34,20140513, for adding new web pages to the tab of "power system"
	if((iEquipID >= SMDUH_EQUIP_START)
		&& (iEquipID < SMDUH_EQUIP_START + SMDUH_UNIT_NUM)
		&& ( (SIG_TYPE_CONTROL == iSignalType) || (SIG_TYPE_SETTING == iSignalType) )
		)
	{
		iTimeout = 20000;
	}
	if((iEquipID >= SMDUHH_EQUIP_START)
		&& (iEquipID < SMDUHH_EQUIP_START + SMDUHH_UNIT_NUM)
		&& ( (SIG_TYPE_CONTROL == iSignalType) || (SIG_TYPE_SETTING == iSignalType) )
		)
	{
		iTimeout = 20000;
	}
	//changed by Frank Wu,N/N/N,20140611, for adding new tab pages "shunt" and "solar" to settings menu
	else if((EQUIP_SOLAR_GROUP == iEquipID)
		&& ( (SIG_TYPE_CONTROL == iSignalType) || (SIG_TYPE_SETTING == iSignalType) )
		)
	{
		iTimeout = 15000;
	}
	//changed for triggle the generating of Efficiency Tracker data 
	if(iEquipID == 2 && SIG_TYPE_SETTING == iSignalType && 
		(iSignalID >=61 && iSignalID <= 64 ))
	{
		WEB_ET_Info.bETDataRefresh = TRUE;
	}

	iError = DxiSetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		iVarSubID,		
		iBufLen,			
		&value,			
		//6000);
		iTimeout);

	if (iError == ERR_DXI_OK )
	{
		if((iEquipID == 1) && (iSignalType == SIG_TYPE_SETTING) && (iSignalID == ID_PROTOCOL_TYPE1))
		{
			TRACE_WEB_USER_NOT_CYCLE("Protocol type(1,2,457) changed to %ld, restart the controller!\n", value.varValue.enumValue);
			AppLogOut("Web_ModifySignalValue", APP_LOG_INFO, "Protocol type(1,2,457) changed, restart the controller!");
			_SYSTEM("reboot");
		}
		else if((iEquipID == 1) && (iSignalType == SIG_TYPE_SETTING) && (iSignalID == 180))
		{
			TRACE_WEB_USER_NOT_CYCLE("Rectifier Expansion (1,2,180) changed to %ld, restart the controller!\n", value.varValue.enumValue);
			AppLogOut("Web_ModifySignalValue", APP_LOG_INFO, "Expansion(1,2,180) changed, restart the controller!");
			_SYSTEM("reboot");
		}
		else if((iEquipID == 1) && (iSignalType == SIG_TYPE_SETTING) && ((iSignalID == 479) || (iSignalID == 480))) 
		{
		    if(iSignalID == 479)
		    {
			TRACE_WEB_USER_NOT_CYCLE("Cabinet X Axis Number changed to %ld!\n", value.varValue.ulValue);
			AppLogOut("Web_ModifySignalValue", APP_LOG_INFO, "Cabinet X Axis Number changed!");
		    }
		    else
		    {
			TRACE_WEB_USER_NOT_CYCLE("Cabinet Y Axis Number changed to %ld!\n", value.varValue.ulValue);
			AppLogOut("Web_ModifySignalValue", APP_LOG_INFO, "Cabinet Y Axis Number changed!");
		    }
		    memset(szFile, 0, sizeof(szFile));
		    sprintf(szFile, "%s", CONFIG_FILE_CONSUMPTION_ROOT);
		    iReturn = LoadHtmlFile(szFile, &pHtml);
		    if(iReturn > 0)
		    {
			ptr = strstr(pHtml, CONFIG_FLAG);
			if(ptr != NULL)
			{
			    while((*ptr != 0x30) && (*ptr != 0x31))
			    {
				ptr++;
			    }
			    *ptr = 0x31;
			}
			
			if(szFile != NULL && (fp = fopen(szFile,"wb")) != NULL && pHtml != NULL)
			{
			    fwrite(pHtml,strlen(pHtml), 1, fp);
			    fclose(fp);
			}
			else
			{}
		    }
		    if(pHtml)
		    {
			DELETE(pHtml);
			pHtml = NULL;
		    }
		    return TRUE;
		}
		else
		{
			return TRUE;
		}
	}
	else
	{
	    TRACE_WEB_USER_NOT_CYCLE("iError is %d\n", iError);
		if(iError == ERR_EQP_CTRL_SUPPRESSED)
		{
			return MODIFY_SIGNALVALUE_FAIL;
		}
		else if(iError == ERR_DXI_HARDWARE_SWITCH_STATUS)
		{
			return WEB_RETURN_PROTECTED_ERROR;
		}
		else
		{
			return FALSE;
		}
	}

}
/*==========================================================================*
* FUNCTION :  Web_ModifyACUInfo
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN char *pNewConfigInfo:
IN int iLanguage:
IN int iModifyType:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:35
*==========================================================================*/
static int Web_ModifyACUInfo(IN char *pNewConfigInfo, IN int iLanguage, 
			     IN int iModifyType)
{
	int		iVarID, iVarSubID;
	int		iError = 0;
	int		iInterfaceType = VAR_ACU_PUBLIC_CONFIG;
	BOOL		bNeedRefreshWebPage = FALSE;
	int	iBufLen = 0;
	int	iTimeOut = 0;

	Cfg_RemoveWhiteSpace(pNewConfigInfo);
	switch (iModifyType)
	{
	case SITE_NAME:
		{
			iVarID = SITE_NAME;
			if(iLanguage >0)
			{
				//if(iLanguage == 1)
				//{
				iVarSubID = MODIFY_SITE_ENGLISH_FULL_NAME;
				iBufLen = strlen(pNewConfigInfo);

				iError += DxiSetData(iInterfaceType,
					iVarID,			
					iVarSubID,		
					iBufLen,			
					pNewConfigInfo,			
					iTimeOut);
				//}
				//////////////////////////////////////////////////////////////////////////
				//Added by wj for three languages 2006.5.9
				/*else
				{
				iVarSubID = MODIFY_SITE_ENGLISH_FULL_NAME;
				iBufLen = strlen(pNewConfigInfo);

				iError += DxiSetData(iInterfaceType,
				iVarID,			
				iVarSubID,		
				iBufLen,			
				pNewConfigInfo,			
				iTimeOut);
				}*/
				//end////////////////////////////////////////////////////////////////////////



			}
			else
			{
				iVarSubID = MODIFY_SITE_ENGLISH_FULL_NAME;
				iBufLen = strlen(pNewConfigInfo);

				iError += DxiSetData(iInterfaceType,
					iVarID,			
					iVarSubID,		
					iBufLen,			
					pNewConfigInfo,			
					iTimeOut);
			}

			bNeedRefreshWebPage = TRUE;
			break;
		}
	case SITE_SW_VERSION:
		{		
			iVarID = SITE_SW_VERSION;
			int dwVersion = atoi(pNewConfigInfo);
			iBufLen = sizeof(dwVersion);

			iError += DxiSetData(iInterfaceType,
				iVarID,			
				iVarSubID,		
				iBufLen,			
				&dwVersion,			
				iTimeOut);
			break;
		}
	case ACU_MANUFACTURER:
		{		
			iVarID = ACU_MANUFACTURER;
			iBufLen = strlen(pNewConfigInfo);

			iError += DxiSetData(iInterfaceType,
				iVarID,			
				iVarSubID,		
				iBufLen,			
				pNewConfigInfo,			
				iTimeOut);
			break;
		}
	case SITE_ID:
		{		
			iVarID = SITE_ID;
			int nSiteID = atoi(pNewConfigInfo);
			iBufLen = sizeof(nSiteID);

			iError += DxiSetData(iInterfaceType,
				iVarID,			
				iVarSubID,		
				iBufLen,			
				&nSiteID,			
				iTimeOut);
			break;
		}
	case SITE_LOCATION:
		{		
			iVarID = SITE_LOCATION;

			if(iLanguage > 0)
			{
				if(iLanguage == 1)
				{
					iVarSubID = MODIFY_LOCATION_LOCAL_FULL;

					iBufLen = strlen(pNewConfigInfo);

					iError += DxiSetData(iInterfaceType,
						iVarID,			
						iVarSubID,		
						iBufLen,			
						pNewConfigInfo,			
						iTimeOut);
				}
				//////////////////////////////////////////////////////////////////////////
				//Added by wj for three languages 2006.5.9

				else
				{
					iVarSubID = MODIFY_LOCATION_LOCAL2_FULL;

					iBufLen = strlen(pNewConfigInfo);

					iError += DxiSetData(iInterfaceType,
						iVarID,			
						iVarSubID,		
						iBufLen,			
						pNewConfigInfo,			
						iTimeOut);
				}

				//end////////////////////////////////////////////////////////////////////////


			}
			else
			{
				iVarID = SITE_LOCATION;
				iVarSubID = MODIFY_LOCATION_ENGLISH_FULL;

				iBufLen = strlen(pNewConfigInfo);

				iError += DxiSetData(iInterfaceType,
					iVarID,			
					iVarSubID,		
					iBufLen,			
					pNewConfigInfo,			
					iTimeOut);
			}
			bNeedRefreshWebPage = TRUE;
			break;
		}
	case SITE_DESCRIPTION:
		{		
			iVarID = SITE_DESCRIPTION;
			if(iLanguage > 0)
			{
				if(iLanguage == 1)
				{
					iVarSubID = MODIFY_DESCRIPTION_LOCAL_FULL;
					iBufLen = strlen(pNewConfigInfo);

					iError += DxiSetData(iInterfaceType,
						iVarID,			
						iVarSubID,		
						iBufLen,			
						pNewConfigInfo,			
						iTimeOut);
				}

				//////////////////////////////////////////////////////////////////////////
				//Added by wj for three languages 2006.5.9

				else
				{
					iVarSubID = MODIFY_DESCRIPTION_LOCAL2_FULL;
					iBufLen = strlen(pNewConfigInfo);

					iError += DxiSetData(iInterfaceType,
						iVarID,			
						iVarSubID,		
						iBufLen,			
						pNewConfigInfo,			
						iTimeOut);
				}

				//end////////////////////////////////////////////////////////////////////////


			}
			else
			{
				iVarSubID = MODIFY_DESCRIPTION_ENGLISH_FULL;
				iBufLen = strlen(pNewConfigInfo);

				iError += DxiSetData(iInterfaceType,
					iVarID,			
					iVarSubID,		
					iBufLen,			
					pNewConfigInfo,			
					iTimeOut);
			}
			bNeedRefreshWebPage = TRUE;
			break;
		}
	default:
		break;
	}

	if (iError == ERR_DXI_OK)
	{
		if(bNeedRefreshWebPage == TRUE)
		{
			Web_WriteVersionOfWebPages_r(0.99, stWebCompareVersion.szLanguage);


		}
		return TRUE;
	}
	else
	{
		if(iError == ERR_DXI_HARDWARE_SWITCH_STATUS)
		{
			return WEB_RETURN_PROTECTED_ERROR;
		}
		else
		{
			return FALSE;
		}
	}
}
/*==========================================================================*
* FUNCTION :  Web_GetACUInfo
* PURPOSE  :  get ACU Information
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	OUT SITE_INFO	**ppSiteInfo: 
OUT DWORD *pDwGetData:
OUT char szGetStr[128]: 
OUT char **ppLangCode:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:35
*==========================================================================*/
static int Web_GetACUInfo(OUT SITE_INFO	**ppSiteInfo, OUT DWORD *pDwGetData, 
			  OUT char szGetStr[128], OUT char **ppLangCode)
{
	SITE_INFO			*pSiteInfo;
	//LANG_TEXT			*pLangInfo;
	DWORD	   			dwGetData;
	char	   			szInGetStr[128];
	char           		*pLocalLangCode;

	int		iError = 0;
	int		iInterfaceType = VAR_ACU_PUBLIC_CONFIG;
	int		iVarID = SITE_INFO_POINTER;
	int	iVarSubID = 0;
	int	iBufLen = 0;
	int	iTimeOut = 0;

	iError += DxiGetData(iInterfaceType,
		iVarID,			
		iVarSubID,		
		&iBufLen,			
		&pSiteInfo,			
		iTimeOut);

	iVarID = SITE_SW_VERSION;
	iError += DxiGetData(iInterfaceType,
		iVarID,			
		iVarSubID,		
		&iBufLen,			
		&dwGetData,			
		iTimeOut);


	iVarID = ACU_MANUFACTURER;
	iError += DxiGetData(iInterfaceType,
		iVarID,			
		iVarSubID,		
		&iBufLen,			
		szInGetStr,			
		iTimeOut);

	iVarID = LOCAL_LANGUAGE_CODE;

	iError += DxiGetData(iInterfaceType,
		iVarID,			
		iVarSubID,		
		&iBufLen,			
		&pLocalLangCode,			
		iTimeOut);

	if (iError == ERR_DXI_OK)
	{
		sprintf(szGetStr,"%s", szInGetStr);
		*ppLangCode = pLocalLangCode;
		pDwGetData = &dwGetData;
		*ppSiteInfo = pSiteInfo;

		return TRUE;
	}
	else
	{
		return FALSE;
	}

}
/*==========================================================================*
* FUNCTION :  Web_MakeACUInfoBufferA
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	OUT char **ppBuf:
IN int iLanguage:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:35
*==========================================================================*/
static int Web_MakeACUInfoBufferA(OUT char **ppBuf, IN int iLanguage)
{
	SITE_INFO		*pSiteInfo = NULL;
	char			*pOutBuf = NULL;
	DWORD			dwGetData;
	char			szGetStr[128];
	char			*pLangCode = NULL;
	int				i = 1, iLen = 0;
	char			*pSearchValue = NULL;


	if(Web_GetACUInfo(&pSiteInfo, &dwGetData, szGetStr, &pLangCode) == FALSE)
	{
		return FALSE;
	}
#define MAX_ACU_INFO_BUFFER_LEN_A 10000	

	pOutBuf = NEW(char, MAX_ACU_INFO_BUFFER_LEN_A);
	ASSERT(pOutBuf);
	if(pOutBuf == NULL)
	{
		return FALSE;
	}
	memset(pOutBuf,0,MAX_ACU_INFO_BUFFER_LEN_A);
	if(iLanguage > 0)
	{

		//if(iLanguage==1)
		//{


		pOutBuf[iLen] = ACU_LOC_INFO_START;
		/*Loc ACU Info*/
		iLen += sprintf(pOutBuf + iLen + 1, "[%d,\"��վ��\",\"%s\"],\n", SITE_NAME, pSiteInfo->langSiteName.pFullName[0]);
		//iLen += sprintf(pOutBuf + iLen, "%d,\"Version\",%ld,\n",    SITE_SW_VERSION, dwGetData);
		//iLen += sprintf(pOutBuf + iLen, "%d,\"Manufacture\",\"%s\",\n", ACU_MANUFACTURER, szGetStr);
		//iLen += sprintf(pOutBuf + iLen, "%d,\"SiteID\", %d,\n",	  SITE_ID, pSiteInfo->iSiteID);
		//iLen += sprintf(pOutBuf + iLen, "%d,\"Language Code\",\"%s\",\n", LOCAL_LANGUAGE_CODE, pLangCode);
		iLen += sprintf(pOutBuf + iLen, "[%d,\"��վλ��\",\"%s\"],\n", SITE_LOCATION, pSiteInfo->langSiteLocation.pFullName[1]);
		iLen += sprintf(pOutBuf + iLen, "[%d,\"��վ����\",\"%s\"],\n", SITE_DESCRIPTION, pSiteInfo->langDescription.pFullName[1]);
		//iLen += sprintf(pOutBuf + iLen, "%d,\"AlarmOutgoingBlocked\",%d,\n", i++, pSiteInfo->bAlarmOutgoingBlocked);

		pSearchValue = strrchr(pOutBuf,44);  //get the last ','
		if(pSearchValue != NULL)
		{
			*pSearchValue = 32;   //replace ',' with ' '
		}
		//}
		//else
		//{
		//	pOutBuf[iLen] = ACU_LOC2_INFO_START;
		//	/*Loc ACU Info*/
		//	iLen += sprintf(pOutBuf + iLen + 1, "%d,\"SiteName\",\"%s\",\n", SITE_NAME, pSiteInfo->langSiteName.pFullName[2]);
		//	//iLen += sprintf(pOutBuf + iLen, "%d,\"Version\",%ld,\n",    SITE_SW_VERSION, dwGetData);
		//	//iLen += sprintf(pOutBuf + iLen, "%d,\"Manufacture\",\"%s\",\n", ACU_MANUFACTURER, szGetStr);
		//	//iLen += sprintf(pOutBuf + iLen, "%d,\"SiteID\", %d,\n",	  SITE_ID, pSiteInfo->iSiteID);
		//	//iLen += sprintf(pOutBuf + iLen, "%d,\"Language Code\",\"%s\",\n", LOCAL_LANGUAGE_CODE, pLangCode);
		//	iLen += sprintf(pOutBuf + iLen, "%d,\"Site Location\",\"%s\",\n", SITE_LOCATION, pSiteInfo->langSiteLocation.pFullName[2]);
		//	iLen += sprintf(pOutBuf + iLen, "%d,\"Site Description\",\"%s\",\n", SITE_DESCRIPTION, pSiteInfo->langDescription.pFullName[2]);
		//	//iLen += sprintf(pOutBuf + iLen, "%d,\"AlarmOutgoingBlocked\",%d,\n", i++, pSiteInfo->bAlarmOutgoingBlocked);

		//	pSearchValue = strrchr(pOutBuf,44);  //get the last ','
		//	if(pSearchValue != NULL)
		//	{
		//		*pSearchValue = 32;   //replace ',' with ' '
		//	}
		//}



	}
	else
	{

		/*Eng ACU Infor*/
		i = 1;
		pOutBuf[iLen] = ACU_ENG_INFO_START ;
		iLen += sprintf(pOutBuf + iLen + 1, "[%d,\"SiteName\",\"%s\"],\n", SITE_NAME, pSiteInfo->langSiteName.pFullName[0]);
		//iLen += sprintf(pOutBuf + iLen, "%d,\"Version\",%ld,\n",    SITE_SW_VERSION, dwGetData);
		//iLen += sprintf(pOutBuf + iLen, "%d,\"Manufacture\",\"%s\",\n", ACU_MANUFACTURER, szGetStr);
		//iLen += sprintf(pOutBuf + iLen, "%d, \"SiteID\",%d ,\n", SITE_ID, pSiteInfo->iSiteID);
		//iLen += sprintf(pOutBuf + iLen, "%d,\"Language Code\",\"%s\",\n", LOCAL_LANGUAGE_CODE, pLangCode);
		iLen += sprintf(pOutBuf + iLen, "[%d,\"Site Location\",\"%s\"],\n", SITE_LOCATION, pSiteInfo->langSiteLocation.pFullName[0]);
		iLen += sprintf(pOutBuf + iLen, "[%d,\"Site Description\",\"%s\"],\n", SITE_DESCRIPTION, pSiteInfo->langDescription.pFullName[0]);
		//iLen += sprintf(pOutBuf + iLen, "%d,\"AlarmOutgoingBlocked\",%d,\n", i++, pSiteInfo->bAlarmOutgoingBlocked);

		pSearchValue = strrchr(pOutBuf,44);  //get the last ','

		if(pSearchValue != NULL)
		{
			*pSearchValue = 32;   //replace ',' with ' '
		}
	}

	*ppBuf = pOutBuf;
	return iLen;
}

/*==========================================================================*
* FUNCTION :  Web_ModifySignalAlarmLevel
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN int equipID:
IN int signalType:
IN int signalID:
IN int alarmLevel:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-21 11:20
*==========================================================================*/

static int Web_ModifySignalAlarmLevel(IN int iEquipID,
				      IN int iSignalType,
				      IN int iSignalID,
				      IN int iAlarmLevel,
				      IN char *szWriteUserName)
{
    SET_A_SIGNAL_INFO_STU		stSetASignalInfo;

    int iError	= 0;
    int iBufLen = 0;
    int iVarSubID	= DXI_MERGE_SIG_ID(iSignalType,iSignalID);


    strncpyz(stSetASignalInfo.cModifyUser, 
	szWriteUserName,
	strlen(szWriteUserName) + 1);

    stSetASignalInfo.byModifyType		= MODIFY_ALARM_LEVEL;
    stSetASignalInfo.bModifyAlarmLevel	= iAlarmLevel;

    /*iError += DxiSetData(VAR_A_SIGNAL_INFO_STRU,
    iEquipID,			
    iVarSubID,		
    iBufLen,			
    (void *)&stSetASignalInfo,			
    iTimeOut);*/
    iBufLen = sizeof(SET_A_SIGNAL_INFO_STU);
#ifdef _SHOW_WEB_INFO
    //TRACE("iEquipID,%d, iSignalType, %d, iSignalID, %d, iAlarmLevel, %d, iBufLen \n" ,
    //iEquipID, iSignalType, iSignalID, iAlarmLevel);

#endif	



    iError += DxiSetData(VAR_A_SIGNAL_INFO_THRO_STD_EQUIP,
	iEquipID,			
	iVarSubID,		
	iBufLen,			
	(void *)&stSetASignalInfo,			
	0);
#ifdef _SHOW_WEB_INFO
    //TRACE("iError :%d\n", iError);
#endif	

    if (iError == ERR_DXI_OK )
    {
	return TRUE;
    }
    else
    {
	AppLogOut(CGI_APP_LOG_COMM_NAME, APP_LOG_WARNING, "Modify signal alarm level fail");
	if(iError == ERR_DXI_HARDWARE_SWITCH_STATUS)
	{
	    return WEB_RETURN_PROTECTED_ERROR;
	}
	else
	{
	    return FALSE;
	}
    }

}

//changed by Frank Wu,26/N/35,20140527, for adding the the web setting tab page 'DI'
/*==========================================================================*
 * FUNCTION :   Web_ModifyStdEquipSignalName
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int equipID:
				IN int signalType:
				IN int signalID:
				IN const char *signalName:
				IN int iLanguage: 
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-21 11:20
 *==========================================================================*/
static int Web_ModifyStdEquipSignalName(IN int iEquipID,
										IN int iSignalType,
										IN int iSignalID,
										IN char *szSignalName, 
										IN int iLanguage,
										IN int iModifyNameType)
{

	ASSERT(szSignalName);
	SET_A_SIGNAL_INFO_STU		stSetASignalInfo;
	char	*szTrimText = NULL;

	int iError		= 0;
	int iBufLen		= sizeof(SET_A_SIGNAL_INFO_STU);
	int iVarSubID	= DXI_MERGE_SIG_ID(iSignalType,iSignalID);

	if(iLanguage == ENGLISH_LANGUAGE_NAME )
	{
		if(iModifyNameType == 1)		//abbr name
		{
			stSetASignalInfo.byModifyType  = (BYTE)MODIFY_SIGNAL_ENGLISH_ABBR_NAME;
		}
		else							//full name
		{
			stSetASignalInfo.byModifyType  = (BYTE)MODIFY_SIGNAL_ENGLISH_FULL_NAME;
		}
	}
	else if(iLanguage == LOCAL_LANGUAGE_NAME)
	{
		if(iModifyNameType == 1)		//abbr name
		{
			stSetASignalInfo.byModifyType  = (BYTE)MODIFY_SIGNAL_LOCAL_ABBR_NAME;
		}
		else							//full name
		{
            stSetASignalInfo.byModifyType  = (BYTE)MODIFY_SIGNAL_LOCAL_FULL_NAME;
		}
	}

    //////////////////////////////////////////////////////////////////////////
    //Added by wj for three languages 2006.5.9
    else 
    {
        if(iModifyNameType == 1)		//abbr name
        {
            stSetASignalInfo.byModifyType  = (BYTE)MODIFY_SIGNAL_LOCAL2_ABBR_NAME;
        }
        else							//full name
        {
            stSetASignalInfo.byModifyType  = (BYTE)MODIFY_SIGNAL_LOCAL2_FULL_NAME;
        }
    }

    //end////////////////////////////////////////////////////////////////////////
    

	szTrimText = Cfg_RemoveWhiteSpace(szSignalName);
	strncpyz(stSetASignalInfo.szModifyBuf,szTrimText, 
											(int)sizeof(stSetASignalInfo.szModifyBuf));

	
	iError += DxiSetData(VAR_A_SIGNAL_INFO_THRO_STD_EQUIP,
						iEquipID,			
						iVarSubID,		
						iBufLen,			
						(void *)&stSetASignalInfo,			
						0);
	TRACE_WEB_USER("Web_ModifyStdEquipSignalName(iError=%d, iEquipID=%d, iSignalType=%d, iSignalID=%d, byModifyType=%d, szModifyBuf=%s)\n",
		iError,
		iEquipID,
		iSignalType,
		iSignalID,
		stSetASignalInfo.byModifyType,
		stSetASignalInfo.szModifyBuf);
	szTrimText = NULL;
	
	if (iError == ERR_DXI_OK )
	{
		return TRUE;
	}
	else
	{
		if(iError == ERR_DXI_HARDWARE_SWITCH_STATUS)
		{
			return WEB_RETURN_PROTECTED_ERROR;
		}
		else
		{
			return FALSE;
		}
	}
}


static int Web_ModifyDIState(IN int iEquipID,
	IN int iSignalType,
	IN int iSignalID,
	IN int iAlarmState,
	IN char *szUserName)
{
	VAR_VALUE_EX sigValue;
	int nBufLen = sizeof(sigValue);
	int iErr;
	char szTempUserName[SINGLE_ARG_MAX_LEN];

	memset(&sigValue, 0, sizeof(sigValue));
	memset(szTempUserName, 0, sizeof(szTempUserName));

	snprintf(szTempUserName, SINGLE_ARG_MAX_LEN, "Web:%s  ",szUserName);

	sigValue.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
	sigValue.nSenderType = SERVICE_OF_LOGIC_CONTROL;
	sigValue.pszSenderName = szTempUserName;
	sigValue.varValue.enumValue = iAlarmState;


	iErr = DxiSetData(VAR_A_SIGNAL_VALUE,
		iEquipID,
		DXI_MERGE_SIG_ID(iSignalType, iSignalID),
		nBufLen,
		&sigValue,
		0);
	if(ERR_DXI_OK == iErr)
	{
		return TRUE;
	}
	else 
	{
		AppLogOut(CGI_APP_LOG_COMM_NAME, APP_LOG_WARNING, "Modify DI state fail");
		if(ERR_DXI_HARDWARE_SWITCH_STATUS == iErr)
		{
			return WEB_RETURN_PROTECTED_ERROR;
		}
		else
		{
			return FALSE;
		}
	}
}

//changed by Frank Wu,27/N/35,20140527, for adding the the web setting tab page 'DI'
static int Web_ModifyDISingalInfo(IN char *szCommandArgs,
									IN char *szWriteUserName)
{
#define DIALARM_PROC_NEXT_RETURN_INT(iArgIntVal, iArgSplitChar)		\
	{	\
		pSearchValue = strchr(ptr, iArgSplitChar);	\
		if(pSearchValue != NULL)	\
		{	\
			iPosition = pSearchValue - ptr;	\
			if(iPosition > 0)	\
			{	\
				strncpyz(szExchange, ptr, iPosition + 1);	\
				iArgIntVal = atoi(szExchange);		\
				TRACE_WEB_USER("DIALARM_PROC_NEXT_RETURN_INT===%d\n", iArgIntVal);		\
				ptr = ptr + iPosition;		\
			}	\
			else	\
			{	\
				return FALSE;	\
			}	\
		}	\
		else	\
		{	\
			return FALSE;	\
		}	\
		ptr = ptr + 1;	\
	}

#define DIALARM_PROC_NEXT_RETURN_STR(iArgStrVal, iArgSplitChar)		\
	{	\
		pSearchValue = strchr(ptr, iArgSplitChar);	\
		if(pSearchValue != NULL)	\
		{	\
			iPosition = pSearchValue - ptr;	\
			if(iPosition > 0)	\
			{	\
				strncpyz(iArgStrVal, ptr, iPosition + 1);	\
				TRACE_WEB_USER("DIALARM_PROC_NEXT_RETURN_INT===%s\n", iArgStrVal);		\
				ptr = ptr + iPosition;		\
			}	\
			else	\
			{	\
				return FALSE;	\
			}	\
		}	\
		else	\
		{	\
			return FALSE;	\
		}	\
		ptr = ptr + 1;	\
	}

	ASSERT(szCommandArgs);

	char *ptr = NULL;
	char szExchange[SINGLE_ARG_MAX_LEN], *pSearchValue = NULL;
	int iPosition = 0;
	int iModifyType = 0;
	int iReturn = 0;

	int iError = 0;
	EQUIP_INFO *pEquipInfo = NULL;
	int iBufLen = 0;
	int iEquipTypeID = 0;

	int iLanguage = 0;
	int iEquipID = 0;
	int iSignalType = 0;
	int iSignalID = 0;
	char szSignalName[SINGLE_ARG_MAX_LEN];
	char szSignalAbbrName[SINGLE_ARG_MAX_LEN];
	int iAlarmLevel = 0;
	int iAlarmRelay = 0;
	int iEquip2ID = 0;
	int iSignal2Type = 0;
	int iSignal2ID = 0;
	int iAlarmState = 0;
	char xOut[SINGLE_ARG_MAX_LEN];

	char *pLangCode = NULL;


	TRACE_WEB_USER_NOT_CYCLE("pszCommandArgs is %s\n", szCommandArgs);
	if((ptr = szCommandArgs) != NULL)
	{
		//the macro's contents
		//pSearchValue = strchr(ptr, COMMAND_ARGS_SPLIT_CHAR);
		//if(pSearchValue != NULL)
		//{
		//	iPosition = pSearchValue - ptr;
		//	if(iPosition > 0)
		//	{
		//		strncpyz(szExchange, ptr, iPosition + 1);
		//		iModifyType = atoi(szExchange);
		//		ptr = ptr + iPosition;
		//	}
		//	else
		//	{
		//		return FALSE;
		//	}
		//}
		//else
		//{
		//	return FALSE;
		//}
		//ptr = ptr + 1;
		//must be the same order as which in 'Web_CTL_GetCommandParam'
		//if there is any error, it will return FALSE
		DIALARM_PROC_NEXT_RETURN_INT( iLanguage, COMMAND_ARGS_SPLIT_CHAR);	//1
		DIALARM_PROC_NEXT_RETURN_INT( iEquipID, COMMAND_ARGS_SPLIT_CHAR);	//2
		DIALARM_PROC_NEXT_RETURN_INT( iSignalType, COMMAND_ARGS_SPLIT_CHAR);	//3
		DIALARM_PROC_NEXT_RETURN_INT( iSignalID, COMMAND_ARGS_SPLIT_CHAR);	//4
		DIALARM_PROC_NEXT_RETURN_STR( szSignalName, COMMAND_ARGS_SPLIT_CHAR);	//5
		DIALARM_PROC_NEXT_RETURN_STR( szSignalAbbrName, COMMAND_ARGS_SPLIT_CHAR);	//6
		DIALARM_PROC_NEXT_RETURN_INT( iAlarmLevel, COMMAND_ARGS_SPLIT_CHAR);	//7
		DIALARM_PROC_NEXT_RETURN_INT( iAlarmRelay, COMMAND_ARGS_SPLIT_CHAR);	//8
		DIALARM_PROC_NEXT_RETURN_INT( iEquip2ID, COMMAND_ARGS_SPLIT_CHAR);	//9
		DIALARM_PROC_NEXT_RETURN_INT( iSignal2Type, COMMAND_ARGS_SPLIT_CHAR);	//10
		DIALARM_PROC_NEXT_RETURN_INT( iSignal2ID, COMMAND_ARGS_SPLIT_CHAR);	//11
		DIALARM_PROC_NEXT_RETURN_INT( iAlarmState, COMMAND_ARGS_SPLIT_CHAR);	//12

		//get iEquipTypeId
		iError = DxiGetData(VAR_A_EQUIP_INFO,
							iEquipID,
							0,		
							&iBufLen,			
							&pEquipInfo,			
							0);
		if(iError != ERR_DXI_OK)
		{
			AppLogOut(CGI_APP_LOG_COMM_NAME,
					APP_LOG_WARNING,
					"Web_ModifyDISingalInfo, getting VAR_A_EQUIP_INFO failed, iEquipID=%d",
					iEquipID);
			return FALSE;
		}
		else
		{
			iEquipTypeID = pEquipInfo->iEquipTypeID;
		}



		iReturn = Web_ModifyStdEquipSignalName(iEquipTypeID,
											iSignalType,
											iSignalID,
											szSignalName,
											iLanguage,
											0);//Modified by wj for three languages 2006.5.9
		if(iReturn != TRUE)
		{
			AppLogOut(CGI_APP_LOG_COMM_NAME, APP_LOG_WARNING, "Modify signal alarm full name fail");
			if(FALSE == iReturn)
			{
				return FALSE;
			}
		}

		iError = DxiGetData(VAR_ACU_PUBLIC_CONFIG,
			LOCAL_LANGUAGE_CODE, 
			0, 
			&iBufLen,
			&(pLangCode),
			0);
		if(iError == ERR_DXI_OK)
		{
			if(((iLanguage == 1) 
			    && (strcmp((const char*)pLangCode, "zh") == 0)) || ((iLanguage == 1)
			    && (strcmp((const char*)pLangCode, "tw") == 0)))
			{
				UTF8toGB2312(szSignalAbbrName,64,xOut,32);
				iReturn = Web_ModifyStdEquipSignalName(iEquipTypeID,
				    iSignalType,
				    iSignalID,
				    xOut,
				    iLanguage,
				    1);//Modified by wj for three languages 2006.5.9
			}
			/*else if((iLanguage == 1)
				&& (strcmp((const char*)pLangCode, "tw") == 0))
			{
				;
			}*/
			else
			{
				iReturn = Web_ModifyStdEquipSignalName(iEquipTypeID,
					iSignalType,
					iSignalID,
					szSignalAbbrName,
					iLanguage,
					1);//Modified by wj for three languages 2006.5.9
				/*if(iReturn != TRUE)
				{
					AppLogOut(CGI_APP_LOG_COMM_NAME, APP_LOG_WARNING, "Modify signal alarm abbr name fail");
					if(FALSE == iReturn)
					{
						return FALSE;
					}
				}*/
			}
			if(iReturn != TRUE)
			{
			    AppLogOut(CGI_APP_LOG_COMM_NAME, APP_LOG_WARNING, "Modify signal alarm abbr name fail");
			    if(FALSE == iReturn)
			    {
				return FALSE;
			    }
			}
		}


		iReturn = Web_ModifySignalAlarmLevel(iEquipTypeID,
											iSignalType,
											iSignalID,
											iAlarmLevel,
											szWriteUserName);
		if(iReturn != TRUE)
		{
			//AppLogOut(CGI_APP_LOG_COMM_NAME, APP_LOG_WARNING, "Modify signal alarm level fail");
			if(FALSE == iReturn)
			{
				return FALSE;
			}
		}

		iReturn = Web_ModifyAlarmRelay(iEquipTypeID, iSignalID, iAlarmRelay, szWriteUserName);
		if(iReturn != TRUE)
		{
			//AppLogOut(CGI_APP_LOG_COMM_NAME, APP_LOG_WARNING, "Modify signal alarm relay fail");
			if(FALSE == iReturn)
			{
				return FALSE;
			}
		}

		iReturn = Web_ModifyDIState(iEquip2ID, iSignal2Type, iSignal2ID, iAlarmState, szWriteUserName);
		if(iReturn != TRUE)
		{
			//AppLogOut(CGI_APP_LOG_COMM_NAME, APP_LOG_WARNING, "Modify signal alarm state fail");
			if(FALSE == iReturn)
			{
				return FALSE;
			}
		}

		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

static int Web_ModifyEquipSignalName(IN int iEquipID,
										IN int iSignalType,
										IN int iSignalID,
										IN char *szSignalName, 
										IN int iLanguage,
										IN int iModifyNameType)
{

	ASSERT(szSignalName);
	SET_A_SIGNAL_INFO_STU		stSetASignalInfo;
	char	*szTrimText = NULL;

	int iError		= 0;
	int iBufLen		= sizeof(SET_A_SIGNAL_INFO_STU);
	int iVarSubID	= DXI_MERGE_SIG_ID(iSignalType,iSignalID);
	if(iLanguage == ENGLISH_LANGUAGE_NAME )
	{
		if(iModifyNameType == 1)		//abbr name
		{
			stSetASignalInfo.byModifyType  = (BYTE)MODIFY_SIGNAL_ENGLISH_ABBR_NAME;
		}
		else							//full name
		{
			stSetASignalInfo.byModifyType  = (BYTE)MODIFY_SIGNAL_ENGLISH_FULL_NAME;
		}
	}
	else if(iLanguage == LOCAL_LANGUAGE_NAME)
	{
		if(iModifyNameType == 1)		//abbr name
		{
			stSetASignalInfo.byModifyType  = (BYTE)MODIFY_SIGNAL_LOCAL_ABBR_NAME;
		}
		else							//full name
		{
            stSetASignalInfo.byModifyType  = (BYTE)MODIFY_SIGNAL_LOCAL_FULL_NAME;
		}
	}

    //////////////////////////////////////////////////////////////////////////
    //Added by wj for three languages 2006.5.9
    else 
    {
        if(iModifyNameType == 1)		//abbr name
        {
            stSetASignalInfo.byModifyType  = (BYTE)MODIFY_SIGNAL_LOCAL2_ABBR_NAME;
        }
        else							//full name
        {
            stSetASignalInfo.byModifyType  = (BYTE)MODIFY_SIGNAL_LOCAL2_FULL_NAME;
        }
    }

    //end////////////////////////////////////////////////////////////////////////
    

	szTrimText = Cfg_RemoveWhiteSpace(szSignalName);
	strncpyz(stSetASignalInfo.szModifyBuf,szTrimText, 
						(int)sizeof(stSetASignalInfo.szModifyBuf));
	
	iError += DxiSetData(VAR_A_SIGNAL_INFO_STRU,
						iEquipID,			
						iVarSubID,		
						iBufLen,			
						(void *)&stSetASignalInfo,			
						0);
	TRACE_WEB_USER("Web_ModifyEquipSignalName(iError=%d, iEquipID=%d, iSignalType=%d, iSignalID=%d, byModifyType=%d, szModifyBuf=%s)\n",
		iError,
		iEquipID,
		iSignalType,
		iSignalID,
		stSetASignalInfo.byModifyType,
		stSetASignalInfo.szModifyBuf);
	szTrimText = NULL;
	
	if (iError == ERR_DXI_OK )
	{
		return TRUE;
	}
	else
	{
//		printf("iError = %d\n",iError);
		if(iError == ERR_DXI_HARDWARE_SWITCH_STATUS)
		{
			return WEB_RETURN_PROTECTED_ERROR;
		}
		else
		{
			return FALSE;
		}
	}
}


BOOL  GetShuntValue(VAR_VALUE_EX *value, int iValueType, char *pGetData)		
{			
	if(iValueType == VAR_ENUM)	
	{	
		value->varValue.enumValue = atoi(pGetData);
	}	
	else if(iValueType == VAR_FLOAT)	
	{
		value->varValue.fValue = atof(pGetData);
	}
	else if(iValueType == VAR_UNSIGNED_LONG)	
	{
		value->varValue.ulValue = (unsigned long)atol(pGetData);
	}
	else if(iValueType == VAR_LONG)	
	{	
		value->varValue.lValue = atol(pGetData);
	}	
	else if(iValueType == VAR_DATE_TIME)	
	{	
		value->varValue.dtValue = (time_t)atol(pGetData);
	}	
	else	
	{	
		return FALSE;	
	}
	return TRUE;
}

//changed from ESNA ,20181219, for adding the the web setting tab page 'shunt'
static int Web_ModifyShuntSingalInfo(IN char *szCommandArgs,
									IN char *szWriteUserName)
{
#define DIALARM_PROC_NEXT_RETURN_INT(iArgIntVal, iArgSplitChar)		\
	{	\
		pSearchValue = strchr(ptr, iArgSplitChar);	\
		if(pSearchValue != NULL)	\
		{	\
			iPosition = pSearchValue - ptr;	\
			if(iPosition > 0)	\
			{	\
				strncpyz(szExchange, ptr, iPosition + 1);	\
				iArgIntVal = atoi(szExchange);		\
				TRACE_WEB_USER("DIALARM_PROC_NEXT_RETURN_INT===%d\n", iArgIntVal);		\
				ptr = ptr + iPosition;		\
			}	\
			else	\
			{	\
				return FALSE;	\
			}	\
		}	\
		else	\
		{	\
			return FALSE;	\
		}	\
		ptr = ptr + 1;	\
	}

#define DIALARM_PROC_NEXT_RETURN_STR(iArgStrVal, iArgSplitChar)		\
	{	\
		pSearchValue = strchr(ptr, iArgSplitChar);	\
		if(pSearchValue != NULL)	\
		{	\
			iPosition = pSearchValue - ptr;	\
			if(iPosition > 0)	\
			{	\
				strncpyz(iArgStrVal, ptr, iPosition + 1);	\
				TRACE_WEB_USER("DIALARM_PROC_NEXT_RETURN_INT===%s\n", iArgStrVal);		\
				ptr = ptr + iPosition;		\
			}	\
			else	\
			{	\
				return FALSE;	\
			}	\
		}	\
		else	\
		{	\
			return FALSE;	\
		}	\
		ptr = ptr + 1;	\
	}



	ASSERT(szCommandArgs);

	char *ptr = NULL;
	char szExchange[SINGLE_ARG_MAX_LEN], *pSearchValue = NULL;
	int iPosition = 0;
	int iModifyType = 0;
	int iReturn = 0;

	int iError = 0;
	EQUIP_INFO *pEquipInfo = NULL;
	int iBufLen = 0;
	int iEquipTypeID = 0;

	int iLanguage = 0;
	int iEquipID = 0;
	int iSignalType1 = 0,iSignalType2 = 0,iSignalType3 = 0,iSignalType4 = 0;
	int iSignalType5 = 0,iSignalType6 = 0,iSignalType7 = 0,iSignalType8 = 0,iSignalType9 = 0;
	int iSignalID1 = 0,iSignalID2 = 0,iSignalID3 = 0,iSignalID4 = 0;
	int iSignalID5 = 0,iSignalID6 = 0,iSignalID7 = 0,iSignalID8= 0 ,iSignalID9= 0;
	int iValueType1 = 0,iValueType3 = 0,iValueType4 = 0,iValueType5 = 0,iValueType6 = 0,iValueType7 = 0, iValueType8 = 0, iValueType9 = 0;
	char szSignalName[SINGLE_ARG_MAX_LEN];
	char szSignalAbbrName[SINGLE_ARG_MAX_LEN];
	int iAlarmLevel1 = 0,iAlarmLevel2 = 0;
	int iAlarmRelay1 = 0,iAlarmRelay2 = 0;
	char szValue1[32],szValue2[32],szValue3[32],szValue4[32],szValue5[32],szValue6[32],szValue8[32];
	char szHigh1Full[SINGLE_ARG_MAX_LEN],szHigh1Abbrev[SINGLE_ARG_MAX_LEN],szHigh2Full[SINGLE_ARG_MAX_LEN],szHigh2Abbrev[SINGLE_ARG_MAX_LEN];

	int iEquipBattId=0;
	int iShuntType;

	BOOL bCorrectValue = FALSE;

	char xOut[SINGLE_ARG_MAX_LEN];

	char *pLangCode = NULL;
	VAR_VALUE_EX value;
	char szTempUserName[SINGLE_ARG_MAX_LEN];
	memset(szTempUserName, 0, sizeof(szTempUserName));



	printf("pszCommandArgs is %s\n", szCommandArgs);
	if((ptr = szCommandArgs) != NULL)
	{		
		snprintf(szTempUserName, SINGLE_ARG_MAX_LEN, "Web:%s  ",szWriteUserName);
		value.nSenderType =SERVICE_OF_LOGIC_CONTROL;//SERVICE_OF_USER_INTERFACE;
		value.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
		value.pszSenderName = szTempUserName;

		//must be the same order as which in 'Web_CTL_GetCommandParam'
		//if there is any error, it will return FALSE
		DIALARM_PROC_NEXT_RETURN_INT( iLanguage, COMMAND_ARGS_SPLIT_CHAR);	//1
		DIALARM_PROC_NEXT_RETURN_INT( iEquipID, COMMAND_ARGS_SPLIT_CHAR);	//2
		
		DIALARM_PROC_NEXT_RETURN_INT( iSignalType1, COMMAND_ARGS_SPLIT_CHAR);	//3
		DIALARM_PROC_NEXT_RETURN_INT( iSignalID1, COMMAND_ARGS_SPLIT_CHAR);	//4
		DIALARM_PROC_NEXT_RETURN_INT( iValueType1, COMMAND_ARGS_SPLIT_CHAR);	//4

		DIALARM_PROC_NEXT_RETURN_STR( szValue1, COMMAND_ARGS_SPLIT_CHAR);	//5
		iShuntType = atoi(szValue1);
		TRACE_WEB_USER("iEquipID= %d, iSignalType1= %d, iSignalID1 = %d, szValue1 = %s  iValueType1 = %d bCorrectValue = %d\n", iEquipID, iSignalType1, iSignalID1, szValue1,iValueType1, bCorrectValue);

		if(iEquipID!=115&& iEquipID!=116)		
		{
			bCorrectValue = GetShuntValue(&value,iValueType1 , szValue1);
			if(bCorrectValue)
			{
				iReturn = Web_ModifySignalValue(iEquipID,iSignalType1,iSignalID1,value);
				TRACE_WEB_USER("iReturn = %d\n", iReturn);
			}
		}		
		
		
		DIALARM_PROC_NEXT_RETURN_INT( iSignalType2, COMMAND_ARGS_SPLIT_CHAR);	//3
		DIALARM_PROC_NEXT_RETURN_INT( iSignalID2, COMMAND_ARGS_SPLIT_CHAR);//4
		DIALARM_PROC_NEXT_RETURN_STR( szSignalName, COMMAND_ARGS_SPLIT_CHAR);	//5
		DIALARM_PROC_NEXT_RETURN_STR( szSignalAbbrName, COMMAND_ARGS_SPLIT_CHAR);	//6
		TRACE_WEB_USER("iSignalType2 = %d, iSignalID2 = %d, szSignalName= %s,szSignalAbbrName = %s\n", iSignalType2, iSignalID2, szSignalName,szSignalAbbrName);
	
		DIALARM_PROC_NEXT_RETURN_INT( iSignalType3, COMMAND_ARGS_SPLIT_CHAR);	//3
		DIALARM_PROC_NEXT_RETURN_INT( iSignalID3, COMMAND_ARGS_SPLIT_CHAR);//4
		DIALARM_PROC_NEXT_RETURN_INT( iValueType3, COMMAND_ARGS_SPLIT_CHAR);	//4

		DIALARM_PROC_NEXT_RETURN_STR( szValue3, COMMAND_ARGS_SPLIT_CHAR);	//5
		bCorrectValue = GetShuntValue(&value,iValueType3 , szValue3);
		TRACE_WEB_USER("iEquipID= %d, iSignalType3= %d, iSignalID3 = %d, szValue3 = %s iValueType3 = %d bCorrectValue = %d\n", iEquipID, iSignalType3, iSignalID3, szValue3,iValueType3, bCorrectValue);

		if(bCorrectValue)
		{
			iReturn = Web_ModifySignalValue(iEquipID,iSignalType3,iSignalID3,value);
			TRACE_WEB_USER("iReturn = %d\n", iReturn);

		}

		DIALARM_PROC_NEXT_RETURN_INT( iSignalType4, COMMAND_ARGS_SPLIT_CHAR);	//3
		DIALARM_PROC_NEXT_RETURN_INT( iSignalID4, COMMAND_ARGS_SPLIT_CHAR);//4
		DIALARM_PROC_NEXT_RETURN_INT( iValueType4, COMMAND_ARGS_SPLIT_CHAR);	//4

		DIALARM_PROC_NEXT_RETURN_STR( szValue4, COMMAND_ARGS_SPLIT_CHAR);	//5
	
		bCorrectValue = GetShuntValue(&value,iValueType4 , szValue4);
		printf("iEquipID= %d, iSignalType4= %d, iSignalID4 = %d, szValue4 = %s iValueType4=%d bCorrectValue = %d\n", iEquipID, iSignalType4, iSignalID4, szValue4,iValueType4,bCorrectValue);
	
		if(bCorrectValue)
		{
			iReturn = Web_ModifySignalValue(iEquipID,iSignalType4,iSignalID4,value);
		}

		DIALARM_PROC_NEXT_RETURN_INT( iSignalType5, COMMAND_ARGS_SPLIT_CHAR);	//3
		DIALARM_PROC_NEXT_RETURN_INT( iSignalID5, COMMAND_ARGS_SPLIT_CHAR);//4
		DIALARM_PROC_NEXT_RETURN_INT( iValueType5, COMMAND_ARGS_SPLIT_CHAR);	//4

		DIALARM_PROC_NEXT_RETURN_STR( szValue5, COMMAND_ARGS_SPLIT_CHAR);	//5
		bCorrectValue = GetShuntValue(&value,iValueType5 , szValue5);
		printf("iEquipID= %d, iSignalType5= %d, iSignalID5 = %d, szValue5 = %s  iValueType5 = %d bCorrectValue = %d\n", iEquipID, iSignalType5, iSignalID5, szValue5,iValueType5,bCorrectValue);
		
		if(bCorrectValue)
		{
			iReturn = Web_ModifySignalValue(iEquipID,iSignalType5,iSignalID5,value);
		}

		DIALARM_PROC_NEXT_RETURN_INT( iSignalType6, COMMAND_ARGS_SPLIT_CHAR);	//3
		DIALARM_PROC_NEXT_RETURN_INT( iSignalID6, COMMAND_ARGS_SPLIT_CHAR);//4
		DIALARM_PROC_NEXT_RETURN_INT( iValueType6, COMMAND_ARGS_SPLIT_CHAR);	//4

		DIALARM_PROC_NEXT_RETURN_STR( szValue6, COMMAND_ARGS_SPLIT_CHAR);	//5
		bCorrectValue = GetShuntValue(&value,iValueType6 , szValue6);
		printf("iEquipID= %d, iSignalType6= %d, iSignalID6 = %d, szValue6 = %s iValueType6 = %d bCorrectValue = %d\n", iEquipID, iSignalType6, iSignalID6, szValue6,iValueType6, bCorrectValue);
	
		if(bCorrectValue)
		{
			iReturn = Web_ModifySignalValue(iEquipID,iSignalType6,iSignalID6,value);
			TRACE_WEB_USER("----iReturn = %d\n", iReturn);
		}

		DIALARM_PROC_NEXT_RETURN_INT( iSignalType7, COMMAND_ARGS_SPLIT_CHAR);	//3
		DIALARM_PROC_NEXT_RETURN_INT( iSignalID7, COMMAND_ARGS_SPLIT_CHAR);//4
		DIALARM_PROC_NEXT_RETURN_INT( iAlarmLevel1, COMMAND_ARGS_SPLIT_CHAR);	//7
		DIALARM_PROC_NEXT_RETURN_INT( iAlarmRelay1, COMMAND_ARGS_SPLIT_CHAR);	//8
	
		TRACE_WEB_USER("iSignalType7 = %d,iSignalID7 = %d,iAlarmLevel1 = %d, iAlarmRelay1 = %d \n", iSignalType7,iSignalID7,iAlarmLevel1, iAlarmRelay1);


		DIALARM_PROC_NEXT_RETURN_INT( iSignalType8, COMMAND_ARGS_SPLIT_CHAR);	//3
		DIALARM_PROC_NEXT_RETURN_INT( iSignalID8, COMMAND_ARGS_SPLIT_CHAR);//4
		DIALARM_PROC_NEXT_RETURN_INT( iValueType8, COMMAND_ARGS_SPLIT_CHAR);	//4

		DIALARM_PROC_NEXT_RETURN_STR( szValue8, COMMAND_ARGS_SPLIT_CHAR);	//5
		bCorrectValue = GetShuntValue(&value,iValueType8 , szValue8);
		TRACE_WEB_USER("iEquipID= %d, iSignalType8= %d, iSignalID8 = %d, szValue8 = %s  iValueType8 = %d bCorrectValue = %d\n", iEquipID, iSignalType8, iSignalID8, szValue8,iValueType8, bCorrectValue);

		if(bCorrectValue)
		{
			iReturn = Web_ModifySignalValue(iEquipID,iSignalType8,iSignalID8,value);
		}

		DIALARM_PROC_NEXT_RETURN_INT( iSignalType9, COMMAND_ARGS_SPLIT_CHAR);	//3
		DIALARM_PROC_NEXT_RETURN_INT( iSignalID9, COMMAND_ARGS_SPLIT_CHAR);//4
		DIALARM_PROC_NEXT_RETURN_INT( iAlarmLevel2, COMMAND_ARGS_SPLIT_CHAR);	//7
		DIALARM_PROC_NEXT_RETURN_INT( iAlarmRelay2, COMMAND_ARGS_SPLIT_CHAR);	//8
		TRACE_WEB_USER("iSignalType9 = %d,iSignalID9 = %d,iAlarmLevel2 = %d, iAlarmRelay2 = %d \n", iSignalType9,iSignalID9,iAlarmLevel2, iAlarmRelay2);

		DIALARM_PROC_NEXT_RETURN_STR( szHigh1Full, COMMAND_ARGS_SPLIT_CHAR);	//3
		DIALARM_PROC_NEXT_RETURN_STR( szHigh1Abbrev, COMMAND_ARGS_SPLIT_CHAR);//4
		DIALARM_PROC_NEXT_RETURN_STR( szHigh2Full, COMMAND_ARGS_SPLIT_CHAR);	//7
		DIALARM_PROC_NEXT_RETURN_STR( szHigh2Abbrev, COMMAND_ARGS_SPLIT_CHAR);	//8

		DIALARM_PROC_NEXT_RETURN_INT( iEquipBattId, COMMAND_ARGS_SPLIT_CHAR);	//8
printf("==iEquipBattId=%d, option=%s, %s %s\n",iEquipBattId,szValue1,szHigh1Full,szHigh1Abbrev);

		//get iEquipTypeId
		iError = DxiGetData(VAR_A_EQUIP_INFO,
							iEquipID,
							0,		
							&iBufLen,			
							&pEquipInfo,			
							0);
		if(iError != ERR_DXI_OK)
		{
			AppLogOut(CGI_APP_LOG_COMM_NAME,
					APP_LOG_WARNING,
					"Web_ModifyDISingalInfo, getting VAR_A_EQUIP_INFO failed, iEquipID=%d",
					iEquipID);
			return FALSE;
		}
		else
		{
			iEquipTypeID = pEquipInfo->iEquipTypeID;
		}

		if(iShuntType != 0)
		{
			if(iEquipBattId==-1 )
			{
				iReturn = Web_ModifyEquipSignalName(iEquipID,
									iSignalType2,
									iSignalID2,
									szSignalName,
									iLanguage,
									0); 
			}
			else// if(strcmp(szValue1, "3") == 0)
			{
				iReturn = Web_ModifyEquipSignalName(iEquipBattId,
									iSignalType2,
									iSignalID2,
									szSignalName,
									iLanguage,
									0); 
			}

			//Change the alarm name
			iReturn = Web_ModifyEquipSignalName(iEquipID,
								iSignalType7,
								iSignalID7,
								szHigh1Full,
								iLanguage,
								0);

			iReturn = Web_ModifyEquipSignalName(iEquipID,
								iSignalType9,
								iSignalID9,
								szHigh2Full,
								iLanguage,
								0);
		

			printf("iEquipTypeID=%d, iSignalType1=%d,iSignalID1=%d,szSignalName=%s,iLanguage = %d\n ",iEquipTypeID, iSignalType1,iSignalID1,szSignalName,iLanguage);

			if(FALSE == iReturn)
			{
				AppLogOut(CGI_APP_LOG_COMM_NAME, APP_LOG_WARNING, "Modify signal alarm full name fail");
				return FALSE;
			}

			iError = DxiGetData(VAR_ACU_PUBLIC_CONFIG,
				LOCAL_LANGUAGE_CODE, 
				0, 
				&iBufLen,
				&(pLangCode),
				0);
			if(iError == ERR_DXI_OK)
			{
				if(((iLanguage == 1) 
				&& (strcmp((const char*)pLangCode, "zh") == 0)) || ((iLanguage == 1)
				&& (strcmp((const char*)pLangCode, "tw") == 0)))
				{
					TRACE_WEB_USER("22 iEquipTypeID=%d, iSignalType1=%d,iSignalID1=%d,szSignalName=%s,iLanguage = %d\n ",iEquipTypeID, iSignalType1,iSignalID1,szSignalName,iLanguage);

					UTF8toGB2312(szSignalAbbrName,64,xOut,32);
					
					if(iEquipBattId==-1)
					{
						iReturn = Web_ModifyEquipSignalName(iEquipID,
							iSignalType2,
							iSignalID2,
							xOut,
							iLanguage,
							1);//Modified by wj for three languages 2006.5.9
					}
					else
					{
						iReturn = Web_ModifyEquipSignalName(iEquipBattId,
							iSignalType2,
							iSignalID2,
							xOut,
							iLanguage,
							1);//Modified by wj for three languages 2006.5.9
					}
					UTF8toGB2312(szHigh1Abbrev,64,xOut,32);
					//Change alarm name
					iReturn = Web_ModifyEquipSignalName(iEquipID,
						iSignalType7,
						iSignalID7,
						xOut,
						iLanguage,
						1);//Modified by wj for three languages 2006.5.9
					
					UTF8toGB2312(szHigh2Abbrev,64,xOut,32);
					iReturn = Web_ModifyEquipSignalName(iEquipID,
						iSignalType9,
						iSignalID9,
						xOut,
						iLanguage,
						1);//Modified by wj for three languages 2006.5.9

				}			
				else
				{
					TRACE_WEB_USER("33iEquipTypeID=%d, iSignalType1=%d,iSignalID1=%d,szSignalName=%s,iLanguage = %d\n ",iEquipTypeID, iSignalType1,iSignalID1,szSignalName,iLanguage);

					if(iEquipBattId==-1)
					{
						iReturn = Web_ModifyEquipSignalName(iEquipID,
							iSignalType2,
							iSignalID2,
							szSignalAbbrName,
							iLanguage,
							1);//Modified by wj for three languages 2006.5.9
					}
					else
					{
						iReturn = Web_ModifyEquipSignalName(iEquipBattId,
							iSignalType2,
							iSignalID2,
							szSignalAbbrName,
							iLanguage,
							1);//Modified by wj for three languages 2006.5.9

					}
					iReturn = Web_ModifyEquipSignalName(iEquipID,
						iSignalType7,
						iSignalID7,
						szHigh1Abbrev,
						iLanguage,
						1);//Modified by wj for three languages 2006.5.9

					iReturn = Web_ModifyEquipSignalName(iEquipID,
						iSignalType9,
						iSignalID9,
						szHigh2Abbrev,
						iLanguage,
						1);//Modified by wj for three languages 2006.5.9

				}
				if(FALSE == iReturn)
				{
					AppLogOut(CGI_APP_LOG_COMM_NAME, APP_LOG_WARNING, "Modify signal alarm abbr name fail");
					return FALSE;
				}
			}
		}
		

		iReturn = Web_ModifySignalAlarmLevel(iEquipTypeID,
							iSignalType7,
							iSignalID7,
							iAlarmLevel1,
							szWriteUserName);
		printf("44 iEquipTypeID = %d, iSignalType7 = %d,iSignalID7 = %d,iAlarmLevel1 = %d iReturn = %d\n ",iEquipTypeID, iSignalType7,iSignalID7,iAlarmLevel1,iReturn);

		if(iReturn != TRUE)
		{
			//AppLogOut(CGI_APP_LOG_COMM_NAME, APP_LOG_WARNING, "Modify signal alarm level fail");
			if(FALSE == iReturn)
			{
				return FALSE;
			}
		}

		iReturn = Web_ModifyAlarmRelay(iEquipTypeID, iSignalID7, iAlarmRelay1, szWriteUserName);
		printf("55 iEquipTypeID = %d, iSignalType7 = %d,iSignalID7 = %d,iAlarmRelay1 = %d iReturn = %d\n ",iEquipTypeID, iSignalType7,iSignalID7,iAlarmRelay1,iReturn);

		if(iReturn != TRUE)
		{
			//AppLogOut(CGI_APP_LOG_COMM_NAME, APP_LOG_WARNING, "Modify signal alarm relay fail");
			if(FALSE == iReturn)
			{
				return FALSE;
			}
		}

		iReturn = Web_ModifySignalAlarmLevel(iEquipTypeID,
							iSignalType9,
							iSignalID9,
							iAlarmLevel2,
							szWriteUserName);
		printf("66 iEquipTypeID = %d, iSignalType7 = %d,iSignalID7 = %d,iAlarmLevel2 = %d iReturn = %d\n ",iEquipTypeID, iSignalType9,iSignalID9,iAlarmLevel2,iReturn);

		if(iReturn != TRUE)
		{
			//AppLogOut(CGI_APP_LOG_COMM_NAME, APP_LOG_WARNING, "Modify signal alarm level fail");
			if(FALSE == iReturn)
			{
				return FALSE;
			}
		}

		iReturn = Web_ModifyAlarmRelay(iEquipTypeID, iSignalID9, iAlarmRelay2, szWriteUserName);
printf("77 iEquipTypeID = %d, iSignalType7 = %d,iSignalID7 = %d,iAlarmRelay2 = %d iReturn = %d\n ",iEquipTypeID, iSignalType9,iSignalID9,iAlarmRelay2, iReturn);
	
		if(iReturn != TRUE)
		{
			//AppLogOut(CGI_APP_LOG_COMM_NAME, APP_LOG_WARNING, "Modify signal alarm relay fail");
			if(FALSE == iReturn)
			{
				return FALSE;
			}
		}										
		

		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

/*==========================================================================*
* FUNCTION :  Web_GetEquipInfoByTypeId
* PURPOSE  :  Get A EQUIP_INFO struct point used StdEquipID
* CALLS    : 
* CALLED BY:  Web_MakeAlarmSupExpWebPageBuf 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
static int Web_GetEquipInfoByTypeId(IN int iEquipTypeId, OUT EQUIP_INFO **ppszEquipInfo)
{

    EQUIP_INFO *pEquipInfo = NULL;
    EQUIP_INFO *pTempEquipInfo = NULL;
    int iEquipNum;
    int iBufLen;
    int i;

    /*get equip information*/
    int iError1 = DxiGetData(VAR_ACU_EQUIPS_LIST,
	0,			
	0,		
	&iBufLen,			
	&pEquipInfo,			
	0);

    /*get equip number*/
    int iError2 = DxiGetData(VAR_ACU_EQUIPS_NUM,
	0,			
	0,		
	&iBufLen,			
	(void *)&iEquipNum,			
	0);

    if(iError1 != ERR_DXI_OK || iError2 != ERR_DXI_OK)
    {

	return FALSE;
    }

    pTempEquipInfo = pEquipInfo;
    for(i = 0; i< iEquipNum; i++, pTempEquipInfo++)
    {
	if(pTempEquipInfo->iEquipTypeID == iEquipTypeId)
	{
	    break;
	}
    }

    *ppszEquipInfo = pTempEquipInfo;

    return TRUE;

}

/*==========================================================================*
* FUNCTION :  Web_ModifyAlarmRelay
* PURPOSE  :  
* CALLS    : 
* CALLED BY:
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Zhao Zicheng               DATE: 2014-01-14 Time elapse quickly
*==========================================================================*/
int Web_ModifyAlarmRelay(IN int iEquipID,
			 IN int iSignalID,
			 IN int iAlarmRelay,
			 IN char *szUserName)
{
    int iError = 0;
    char szTempUserName[40];
    EQUIP_INFO *pszEquipInfo = NULL;

    Web_GetEquipInfoByTypeId(iEquipID, &pszEquipInfo);
    TRACE("\n_________iStdEquipTypeID[%d]__________\n",pszEquipInfo->iEquipID);

    SET_A_SIGNAL_INFO_STU		stSetASignalInfo;


    int iBufLen = 0;
    int iVarSubID	= DXI_MERGE_SIG_ID(3, iSignalID);


    sprintf(szTempUserName,"Web:%s  ",szUserName);
    strncpyz(stSetASignalInfo.cModifyUser,szTempUserName,strlen(szTempUserName)+1);

    stSetASignalInfo.byModifyType		= MODIFY_ALARM_RELAY;
    stSetASignalInfo.bModifyAlarmRelay	= iAlarmRelay;


    iBufLen = sizeof(SET_A_SIGNAL_INFO_STU);


    iError += DxiSetData(VAR_A_SIGNAL_INFO_STRU,
	pszEquipInfo->iEquipID,			
	iVarSubID,		
	iBufLen,			
	(void *)&stSetASignalInfo,			
	0);

    if (iError == ERR_DXI_OK )
    {
	return 1;
    }
    else
    {
	AppLogOut(CGI_APP_LOG_COMM_NAME, APP_LOG_WARNING, "Modify signal alarm level fail");
	if(iError == ERR_DXI_HARDWARE_SWITCH_STATUS)
	{
	    return WEB_RETURN_PROTECTED_ERROR;
	}
	else
	{
	    return 0;
	}
    }
}

/*==========================================================================*
* FUNCTION :  Web_GetStdEquipAlarmSingalInfo
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN int iEquipTypeID: 
IN int iSignalType:
IN int iSignalID:
IN int iLanguage:
OUT char **szSignalInfo:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Zhao Zicheng               DATE: 2014-01-14
*==========================================================================*/
static int Web_GetStdEquipAlarmSingalInfo(IN int iEquipTypeID, 
					  IN int iSignalType, 
					  IN int iSignalID, 
					  IN int iLanguage, 
					  OUT char **szSignalInfo)
{
    UNUSED(iSignalID);

    char		*szSendBuffer = NULL;
    STDEQUIP_TYPE_INFO		*pTStdEquipType = NULL, *pStdEquipType = NULL;
    ALARM_SIG_INFO			*pAlarmSigInfo = NULL;
    int					iStdEquipTypeNum = 0;//, iSignalType = 0;
    int					iStdEquipWorkNum = 0;//, iSignalType = 0;
    int			i = 0, j = 0;
    int			iError = 0;
    int			iLen = 0 ;
    int			iSignalNum = 0;
    int iBufLen = 0;
    EQUIP_INFO *pszEquipInfo = NULL;

    iError +=  DxiGetData(VAR_STD_EQUIPS_NUM,
	0,
	0,
	&iBufLen,			
	(void *)&iStdEquipTypeNum,			
	0);

    iError +=  DxiGetData(VAR_STD_EQUIPS_LIST,
	0,			
	0,		
	&iBufLen,			
	(void *)&pTStdEquipType,			
	0);

    if(iError != ERR_DXI_OK)
    {
	return FALSE;
    }
    pStdEquipType = pTStdEquipType;
    TRACE_WEB_USER_NOT_CYCLE("The equip type number is %d\n", iStdEquipTypeNum);

    for (i = 0; i < iStdEquipTypeNum && pStdEquipType != NULL; i++, pStdEquipType++)
    {
	if (pStdEquipType->iTypeID == iEquipTypeID)
	{
	    pAlarmSigInfo = pStdEquipType->pAlarmSigInfo;
	    iSignalNum = pStdEquipType->iAlarmSigNum;


	    szSendBuffer = NEW(char, iStdEquipTypeNum * MAX_ITEM_BUF + iSignalNum * 100 + 5);
	    if(szSendBuffer == NULL )
	    {
		return FALSE;
	    }

	    memset(szSendBuffer, 0, sizeof(szSendBuffer));
	    //TRACE_WEB_USER_NOT_CYCLE("The alarm number is %d\n", iSignalNum);

	    iLen += sprintf(szSendBuffer + iLen, "[");
	    for(j = 0; j < iSignalNum && pAlarmSigInfo != NULL; j++, pAlarmSigInfo++)
	    {
		iLen += sprintf(szSendBuffer + iLen, "[%d,\"%s\",%d,%d],",
		    pAlarmSigInfo->iSigID,
		    pAlarmSigInfo->pSigName->pFullName[iLanguage],
		    pAlarmSigInfo->iAlarmRelayNo,
		    pAlarmSigInfo->iAlarmLevel);
	    }
	    pAlarmSigInfo = NULL;

	    if(iLen > 2)
	    {
		iLen = iLen - 1;
	    }
	    iLen += sprintf(szSendBuffer + iLen,"],");

	    break;
	}

    }

    pStdEquipType = pTStdEquipType;
    iLen += sprintf(szSendBuffer + iLen, "[");
    for(i = 0; i < iStdEquipTypeNum; i++, pStdEquipType++)
    {
	if(DXI_GetStdEquipIDStatus(pStdEquipType->iTypeID))
	{
	    iStdEquipWorkNum++;
	    iLen += sprintf(szSendBuffer + iLen, "[%4d,\"%s\"],",
	    pStdEquipType->iTypeID,
	    pStdEquipType->pTypeName->pFullName[iLanguage]);
	}
    }
    if(iStdEquipWorkNum > 0)
    {
	iLen = iLen - 1;
    }
    iLen += sprintf(szSendBuffer + iLen,"]");
    //TRACE_WEB_USER_NOT_CYCLE("szSendBuffer: %s\n", szSendBuffer);
    *szSignalInfo = szSendBuffer;

    return TRUE;
}

static BOOL Web_ClearHistoryData(IN int iClearType, IN char *szUserName)
{
    TRACE_WEB_USER_NOT_CYCLE("\n____Into Web_ClearHistoryData iClearType:%d__\n",iClearType);
    if((iClearType < 0) || (iClearType >= ITEM_OF(szClearName)))	// added by maofuhua. 2005-05-10
    {
	AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_INFO,
	    "User [%s] try to clear non-existed data type: %d\n",
	    szUserName, iClearType);
	return FALSE;
    }

    if(DAT_StorageDeleteRecord(szClearName[iClearType]) == TRUE)
    {
	TRACE_WEB_USER_NOT_CYCLE("\n____Into Web_ClearHistoryData OK!!___\n");

	AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_INFO,"User [%s] Successfully to Clear [%s]", szUserName, szClearName[iClearType]);
	return TRUE;
    }
    else
    {
	AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_INFO,"User [%s] Fail to Clear [%s]", szUserName, szClearName[iClearType]);
	return FALSE;
    }
}

/*==========================================================================*
* FUNCTION :   Web_SendControlCommandResult
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN const char *buf:
IN const char *filename :
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-21 11:20
*==========================================================================*/
static int Web_SendControlCommandResult(IN char *buf, 
					IN  char *filename)
{
#define		CGI_WEB_USER			"WEB:"
	int			fd2;
	char		pGetData[100];
	char		*ptr = NULL;
	int			iEquipID,iSignalType,iSignalID,iControltype,iAlarmLevel, iAlarmRelay, iReturn = 0;
	int			iReturn1 = 0;
	char		/*szSignalName[33],szEquipName[33],*/szAcuInfo[100],szUserName1[64];
	VAR_VALUE_EX	value;
	char		*szReturnResult = NULL;
	int			iLanguage;
	int			iValueType = 0;
	char		szUserName[33], szWriteUserName[40];
	int			iModifyNameType = 0;
	int			iLen = 0;
	//char		*pUserDefPageInfo = NULL;
	//char		*pUserDefPageTemp = NULL;
	//int			iBufLen = 0;
	//int		iAutoCFGTemp = 0;
	char		*szSignalInfo = NULL;
	char *szFile = NULL;
	char *pHtml = NULL;
	int  itype;
	FILE    *fp;
	int nFind;
	char szSignalName[66];
	char		*szSignalName_temp;
	char	*pLangCode = NULL;
	char	xOut[SINGLE_ARG_MAX_LEN];
	//changed by Frank Wu,28/N/35,20140527, for adding the the web setting tab page 'DI'
	char szCommandArgs[COMMAND_ARGS_BUF_MAX];
	int		iBufLen = 0;
	DxiGetData(VAR_ACU_PUBLIC_CONFIG,
		LOCAL_LANGUAGE_CODE, 
		0, 
		&iBufLen,
		&(pLangCode),
		0);

	/*offset*/
	ptr = buf;
	TRACE_WEB_USER_NOT_CYCLE("ptr is %s\n", ptr);
	ptr = ptr + MAX_COMM_PID_LEN + MAX_COMM_GET_TYPE;

	/*Get Equip ID*/
	strncpyz(pGetData,ptr,MAX_EQUIPID_LEN + 1);
	iEquipID = atoi(pGetData);
	ptr = ptr + MAX_EQUIPID_LEN;

	/*Get signal type*/
	strncpyz(pGetData,ptr,MAX_SIGNALTYPE_LEN + 1);
	iSignalType = atoi(pGetData);
	ptr = ptr + MAX_SIGNALTYPE_LEN;

	/*Get signal ID*/
	strncpyz(pGetData,ptr,MAX_SIGNALID_LEN + 1);
	iSignalID = atoi(pGetData);
	ptr =  ptr + MAX_SIGNALID_LEN;

	/*Get control type, refer to GetCommandParam in control_command.c */
	strncpyz(pGetData,ptr,MAX_CONTROLTYPE_LEN + 1);    
	iControltype = atoi(pGetData); 
	ptr =  ptr + MAX_CONTROLTYPE_LEN;

	/*Get language type*/
	strncpyz(pGetData,ptr,MAX_LANGUAGE_TYPE_LEN + 1);
	iLanguage = atoi(pGetData); 
	ptr =  ptr + MAX_LANGUAGE_TYPE_LEN;

	/*Get Value type*/
	strncpyz(pGetData,ptr,MAX_SIGNAL_VALUE_TYPE + 1);
	iValueType = atoi(pGetData); 
	ptr =  ptr + MAX_SIGNAL_VALUE_TYPE;

	/*Get Alarm type*/
	strncpyz(pGetData,ptr,MAX_SIGNA_NEW_ALARM_LEVEL + 1);
	iAlarmLevel = atoi(pGetData); 
	ptr =  ptr + MAX_SIGNA_NEW_ALARM_LEVEL;

	/*Get Modify name type : 0 : full 1:abbr*/
	strncpyz(pGetData,ptr,MAX_MODIFY_NAME_TYPE + 1);
	iModifyNameType = atoi(pGetData); 
	ptr =  ptr + MAX_MODIFY_NAME_TYPE;

	/*Get Alarm Relay*/
	strncpyz(pGetData,ptr,MAX_SIGNA_NEW_ALARM_RELAY + 1);
	iAlarmRelay = atoi(pGetData); 
	ptr =  ptr + MAX_SIGNA_NEW_ALARM_RELAY;

	/*the modify content*/

	strncpyz(pGetData,ptr,MAX_CONTROL_VALUE_LEN + 1);
	ptr =  ptr + MAX_CONTROL_VALUE_LEN;

	strncpyz(szUserName, ptr, MAX_CONTROL_USER_NAME_LEN + 1);
	sprintf(szWriteUserName, "%s %s", CGI_WEB_USER, str_ltrim(szUserName));
	//changed by Frank Wu,29/N/35,20140527, for adding the the web setting tab page 'DI'
	ptr =  ptr + MAX_CONTROL_USER_NAME_LEN;

	//changed by Frank Wu,30/N/35,20140527, for adding the the web setting tab page 'DI'
	//new items must be added before the item SET_DI_INFO, so that pszCommandArgs points to the right position
	if(SET_DI_INFO == iControltype)
	{
		memset(szCommandArgs, 0, sizeof(szCommandArgs));
		strncpyz(szCommandArgs, ptr, sizeof(szCommandArgs));
	}
	else if(SET_SHUNT_INFO == iControltype)
	{
		memset(szCommandArgs, 0, sizeof(szCommandArgs));
		strncpyz(szCommandArgs, ptr, sizeof(szCommandArgs));
	}
		
	switch(iControltype)
	{
		case MODIFY_SIGNAL_NAME:
		{
//changed by Song Xu 20160603, for modifying the sSignalAbbrName_en in fuse web
			
			strncpyz(szSignalName,pGetData,sizeof(szSignalName));

			szSignalName_temp =  strtok(szSignalName, ",") ;

			printf("MODIFY_SIGNAL_NAME---------[%s]----[%s]-[%s]-", szSignalName, pGetData,szSignalName_temp);

			iReturn = Web_ModifyEquipSignalName(iEquipID,
													iSignalType,
													iSignalID,
													szSignalName_temp, 
													iLanguage,
													iModifyNameType);//Modified by wj for three languages 2006.5.9

			szSignalName_temp =  strtok(NULL, ",") ;
			
			if(((iLanguage == 1) 
			&& (strcmp((const char*)pLangCode, "zh") == 0)) || ((iLanguage == 1)
			&& (strcmp((const char*)pLangCode, "tw") == 0)))
			{
				UTF8toGB2312(szSignalName_temp,64,xOut,32);
				printf("-1---[%s]-\n",szSignalName_temp);
				if(szSignalName_temp!= NULL)
				{
				iReturn = Web_ModifyEquipSignalName(iEquipID,
											iSignalType,
											iSignalID,
											xOut,
											iLanguage,
											1);//0:full name ,1:abbr name	
				}
			}
			else
			{
				printf("-0---[%s]-\n",szSignalName_temp);
				if(szSignalName_temp!= NULL)
				{
					iReturn = Web_ModifyEquipSignalName(iEquipID,
											iSignalType,
											iSignalID,
											szSignalName_temp,
											iLanguage,
											1);//0:full name ,1:abbr name
				}
			}
			break;
		}
	case MODIFY_SIGNAL_VALUE:
		{
			TRACE_WEB_USER_NOT_CYCLE("Set the signal value\n");
			value.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
			value.pszSenderName = szWriteUserName;
			value.nSenderType = SERVICE_OF_USER_INTERFACE;

			if(iValueType == VAR_ENUM)
			{
				value.varValue.enumValue = atoi(pGetData);
			}
			else if(iValueType == VAR_FLOAT)
			{
				value.varValue.fValue = atof(pGetData);
			}
			else if(iValueType == VAR_UNSIGNED_LONG)
			{
				value.varValue.ulValue = (unsigned long)atol(pGetData);
			}
			else if(iValueType == VAR_LONG)
			{
				value.varValue.lValue = atol(pGetData);
			}
			else if(iValueType == VAR_DATE_TIME)
			{
				value.varValue.dtValue = (time_t)atol(pGetData);
			}
			else
			{
				iReturn = -1;
				break;
			}
			iReturn = Web_ModifySignalValue(iEquipID,iSignalType,iSignalID,value);
			break;
		}
	case CLEAR_HISTORY_DATA:
	    {
		int		iClearType = 0;
		iClearType = atoi(pGetData);
		if(Web_GetProtectedStatus() == TRUE)
		{
		    iReturn = 5;
		}
		else
		{
		    iReturn += Web_ClearHistoryData(iClearType, szUserName);
		}
		break;
	    }

	case GET_ACU_INFO:
		{
			iReturn += Web_MakeACUInfoBufferA(&szSignalInfo, iLanguage);
			iReturn = 2;
			break;
		}
	case MODIFY_ACU_INFO:
		{
			TRACE_WEB_USER_NOT_CYCLE("MODIFY_ACU_INFO\n");
			/*the equipID is acu id when it is in acu*/
			strncpyz(szAcuInfo,pGetData,sizeof(szAcuInfo));
			int iModifyType = iEquipID;			//special for modify ACU Info,iModifyType == iEquipID;
			iReturn = Web_ModifyACUInfo(szAcuInfo, iLanguage, iModifyType);//Modified by wj for three languages 2006.5.9
			Web_MakeACUInfoBufferA(&szSignalInfo, iLanguage);//Modified by wj for three languages 2006.5.9
			break;
		}
	case MODIFY_LOC_LANG:
		{
			TRACE_WEB_USER_NOT_CYCLE("MODIFY local language\n");
			szFile = NEW(char, 128);
			if(szFile == NULL)
			{
				iReturn = 0;
			}
			sprintf(szFile, "%s", WEB_RES_VER);
			iReturn1 = LoadHtmlFile(szFile, &pHtml);
			//TRACE_WEB_USER_NOT_CYCLE("iReturn1 is %d	pHtml is %s\n", iReturn1, pHtml);
			if(iReturn1 > 0)
			{
				ReplaceString(&pHtml, "1.00", "0.99");
				//TRACE_WEB_USER_NOT_CYCLE("pHtml is %s\n", pHtml);
				memset(szFile, 0, sizeof(szFile));
				sprintf(szFile, "%s", WEB_RES_VER);
				if(szFile != NULL && (fp = fopen(szFile,"wb")) != NULL && pHtml != NULL)
				{
					fwrite(pHtml,strlen(pHtml), 1, fp);
					fclose(fp);
				}
				else
				{}
			}
			else
			{
				iReturn = 0;
			}
			Sleep(1000);//yield
			/*modify the language flag*/
			itype = atoi(pGetData);
			printf("Web_SendControlCommandResult():  itype = %d, szWebLangType[itype] = %s   !!!!!!\n ",itype,szWebLangType[itype]);
			DXI_ChangeLanguage(szWebLangType[itype]);
			//system("reboot");
			break;
		}
	case FORGET_PASSWORD:
		{
		    char szEmailSendTo[128] = {0};// the public email
		    char szEmailSendTo1[128] = {0};// the private email
		    char *szTemp;
		    int iError;
		    int iBufLength;

		    TRACE_WEB_USER_NOT_CYCLE("Send password to email address\n");
		    strncpyz(szUserName1, pGetData, sizeof(szUserName1));
		    nFind = FindUser(Cfg_RemoveWhiteSpace(szUserName1));
		    if(nFind >= 0)
		    {
			szTemp = FindUserAddr(nFind);
			strncpy(szEmailSendTo1, szTemp, CONTACT_ADDR_LEN);
			if(szEmailSendTo1[0] == 0)
			{
			    iError = DxiGetData(VAR_SMTP_INFO, SMTP_INFO_SENDTO, 0,  &iBufLength,  &szEmailSendTo, 0);
			    if(iError == ERR_DXI_OK)
			    {
				if(szEmailSendTo[0] == 0)
				{
				    iReturn = 3;
				    break;
				}
				else
				{
				    strncpy(szEmailSendTo1, szEmailSendTo, 128);
				}
			    }
			    else
			    {
				iReturn = 3;
				break;
			    }
			}
			TRACE_WEB_USER_NOT_CYCLE("szEmailSendTo1 is %s\n", szEmailSendTo1);
			RUN_THREAD_MSG msgSendPasswd;
			RUN_THREAD_MAKE_MSG(&msgSendPasswd, NULL, MSG_SEND_PASSWORD, nFind, 0);
			RunThread_PostMessage(-1, &msgSendPasswd, FALSE);
			iReturn = 1;		// successful
		    }
		    else
		    {
			iReturn = 2;
		    }
			break;
		}

	case SET_ALARM_INFO:
	    {
		//iAlarmLevel = atoi(pGetData);
		// modify the level
		TRACE_WEB_USER_NOT_CYCLE("Set the alarm[%d][%d][%d] level[%d]!\n", iEquipID, iSignalType, iSignalID, iAlarmLevel);
		iReturn = Web_ModifySignalAlarmLevel(iEquipID,iSignalType,iSignalID,iAlarmLevel,szWriteUserName);

		// modify the relay
		if(iReturn == TRUE || iReturn == WEB_RETURN_PROTECTED_ERROR)
		{
		    TRACE_WEB_USER_NOT_CYCLE("Set the alarm[%d][2][%d] relay[%d]!\n", iEquipID, iSignalID, iAlarmRelay);
		    iReturn = Web_ModifyAlarmRelay(iEquipID, iSignalID, iAlarmRelay, szWriteUserName);
		}
		else
		{
		    iReturn = FALSE;
		}

		break;
	    }

	case GET_ALARM_INFO:
	    {
		TRACE_WEB_USER_NOT_CYCLE("Get the alarm[%d] level and relay!\n", iEquipID);
		iReturn =  Web_GetStdEquipAlarmSingalInfo(iEquipID, 0, 0, iLanguage, &szSignalInfo);
		break;
	    }
	//changed by Frank Wu,31/N/35,20140527, for adding the the web setting tab page 'DI'
	case SET_DI_INFO:
	{
		//TRACE_WEB_USER_NOT_CYCLE("Set the DI info[%d][%d][%d]!\n",
		printf("Set the DI info[%d][%d][%d]!\n",
			iEquipID,
			iSignalType,
			iSignalID);
		iReturn = Web_ModifyDISingalInfo(szCommandArgs, szWriteUserName);
		if(iReturn != TRUE)
		{
			AppLogOut(CGI_APP_LOG_COMM_NAME, APP_LOG_WARNING, "Set the DI info fail");
		}
		break;
	}
	//not be used,just for reserve, the data will be generated per circle period in Web_MakeDISetDataFile
	case GET_DI_INFO:
	{
		//TRACE_WEB_USER_NOT_CYCLE("Get the DI info!\n");
		printf("Get the DI info!\n");
		iReturn = TRUE;
		break;
	}
	case SET_SHUNT_INFO:
	{
		iReturn = Web_ModifyShuntSingalInfo(szCommandArgs, szWriteUserName);

		if(iReturn != TRUE)
		{
			AppLogOut(CGI_APP_LOG_COMM_NAME, APP_LOG_WARNING, "Set the Shunt info fail");
		}
		break;
	}
	//not be used,just for reserve, the data will be generated per circle period in Web_MakeDISetDataFile
	case GET_SHUNT_INFO:
	{
		iReturn = TRUE;
		break;
	}

	case RESTORE_DEFAULT_CONFIG:
	    {
		DXI_ReloadDefaultConfig();
		break;
	    }

	case RESTART_ACU:
	    {
		DXI_RebootACUorSCU(TRUE,TRUE);
		break;
	    }

	default:
		{
			break;
		}
	}

	//TRACE_WEB_USER_NOT_CYCLE("Begin to write the result\n");
	/*send result*/
	szReturnResult = NEW(char, 20);
	memset(szReturnResult,0,20);
	iLen = sprintf(szReturnResult,"%10d",iReturn);
	
	if(szSignalInfo != NULL && strlen(szSignalInfo) != 0)
	{
		szReturnResult = RENEW(char, szReturnResult, 20 + strlen(szSignalInfo));

		memset(szReturnResult,0,20 + strlen(szSignalInfo));

		sprintf(szReturnResult,"%10d%s" ,iReturn,szSignalInfo);

	}

	if(szSignalInfo != NULL)
	{
		DELETE(szSignalInfo);
		szSignalInfo = NULL;
	}
	//TRACE_WEB_USER_NOT_CYCLE("szReturnResult is %s\n", szReturnResult);

	if((fd2 = open(filename,O_WRONLY ))<0)
	{
		return FALSE;

	}
	int iPipeLen = strlen(szReturnResult)/(PIPE_BUF - 1)  + 1;

	if(iPipeLen <= 1)
	{
		if((write(fd2,szReturnResult,strlen(szReturnResult) + 1)) < 0)
		{
			AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_WARNING,"Fail to write FIFO when write the control result!");
			close(fd2);
			char			szCommand[30];
			iLen = sprintf(szCommand,"kill ");
			strncpyz(szCommand + iLen,buf, MAX_COMM_PID_LEN + 1);

			TRACEX("%s\n", szCommand);
			_SYSTEM(szCommand);
			return FALSE;
		}
	}
	else
	{
		int m;
		char szP[PIPE_BUF - 1];
		for(m = 0; m < iPipeLen; m++)
		{
			strncpyz(szP, szReturnResult + m * (PIPE_BUF - 2), PIPE_BUF - 1);

			if((write(fd2, szP, PIPE_BUF - 1)) < 0)
			{
				close(fd2);
				return FALSE;
			}
		}
	}
	if(szReturnResult != NULL)
	{
		DELETE(szReturnResult);
		szReturnResult  = NULL;
	}
	close(fd2);
	return TRUE;
}

/*==========================================================================*
* FUNCTION :  Web_GetAuthorityByUser
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN char *pszUserInfo:
IN char *pszPasswordInfo:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:15
*==========================================================================*/
static int Web_GetAuthorityByUser(IN char *pszUserInfo,IN char *pszPasswordInfo)
{
	USER_INFO_STRU		*pUserInfo;
	int					iError;
	int					iAuthorLevel = 0;



	pUserInfo = NEW(USER_INFO_STRU,1);
	if(pUserInfo == NULL)
	{
		return FALSE;
	}

	//strncpyz(pUserInfo->szUserName , pszUserInfo,strlen(pUserInfo->szUserName));
	iError = FindUserInfo(pszUserInfo,pUserInfo);

	//TRACE("-----------[%s][%s]----[%s][%s]\n", pszUserInfo, pszPasswordInfo,pUserInfo->szUserName, pUserInfo->szPassword);
	if(iError == ERR_SEC_USER_NOT_EXISTED)
	{

		DELETE(pUserInfo);
		pUserInfo = NULL;
		return NO_THIS_USER;
	}
	else
	{
		if(strcmp(pszPasswordInfo,pUserInfo->szPassword) != 0)
		{
			DELETE(pUserInfo);
			pUserInfo = NULL;
			return NO_MATCH_PASSWORD;
		}
		else if(strcmp("Enabled",pUserInfo->szLcdEnable) == 0)//added to check the LCD user
		{
			DELETE(pUserInfo);
			pUserInfo = NULL;
			return LCD_LOGIN_ONLY;
		}
		else
		{
			iAuthorLevel = pUserInfo->byLevel;
			DELETE(pUserInfo);
			pUserInfo = NULL;
			return iAuthorLevel;
		}
	}
}

/*==========================================================================*
* FUNCTION :  Web_SendEquipList
* PURPOSE  :  return the EquipList to the CGI
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN char *buf:
IN char *filename:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-6 15:01
*==========================================================================*/
static int Web_SendEquipList(IN char *buf,IN char *filename)
{
	char	*ptr = NULL;
	char	szUser[MAX_USERNAME_LEN + 1];
	char	szPassword[MAX_PASSWORD_LEN + 1];
	char    szPasswordRadius[65];
	char	szLanguage[5];
	int		fd2;
	char	*pszSendBuf = NULL;
	int		iReturn, iLen = 0, /*iNewLen = 0, */iLanguage,iReturnRadius;
	//char	*pTempBuf = NULL;

	if((fd2 = open(filename,O_WRONLY )) < 0)
	{
		return FALSE;

	}

	signal(SIGPIPE, SIG_IGN);
	/*offset*/
	ptr = buf + MAX_COMM_PID_LEN + MAX_COMM_GET_TYPE;

	//get password ,User name is 16 bit long ;
	strncpyz(szUser, ptr, MAX_USERNAME_LEN + 1);	
	ptr = ptr + MAX_USERNAME_LEN;

	//get password ,password is 16 bit long ;
	strncpyz(szPasswordRadius, ptr, MAX_PASSWORD_LEN + 1 );
	strcpy(szPasswordRadius,Cfg_RemoveWhiteSpace(szPasswordRadius));
	ptr = ptr + MAX_PASSWORD_LEN;

	strncpyz(szLanguage, ptr, MAX_LANGUAGE_TYPE_LEN + 1);
	iLanguage = atoi(szLanguage);

	iReturn = Web_GetAuthorityByUser(Cfg_RemoveWhiteSpace(szUser),Cfg_RemoveWhiteSpace(szPasswordRadius));

	// added by for radius Authentication and to check whether the user is LCD_LOGIN_ONLY
		if (iReturn<0 && iReturn!= LCD_LOGIN_ONLY)
		{
			   //AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_WARNING,"Login Failure : Login failed due to incorrect User name or password");

			iReturnRadius=Radius_Web_GetAuthorityByUser(Cfg_RemoveWhiteSpace(szUser),szPasswordRadius);

			if (iReturnRadius !=-2)
			{
				if (iReturnRadius ==-1)
				{
				    AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_WARNING,"Login Failure : Login failed due to Unsupported response from radius server");
				}
				iReturn=iReturnRadius;
			}


		}

	TRACE_WEB_USER_NOT_CYCLE("Login iReturn is %d\n", iReturn);

	pszSendBuf = NEW(char , 3);
	if(pszSendBuf == NULL)
	{
		close(fd2);
		return FALSE;
	}

	if(StartFlage == 0)
	{
		iLen = sprintf(pszSendBuf,"%2d",WAIT_FOR_START);
		pszSendBuf[iLen] = 0;
		if((write(fd2,pszSendBuf,sizeof(pszSendBuf) + 1)) < 0)
		{
			DELETE(pszSendBuf);
			pszSendBuf = NULL;
			close(fd2);
			char			szCommand[30];
			iLen = sprintf(szCommand,"kill ");
			strncpyz(szCommand + iLen,buf, MAX_COMM_PID_LEN + 1);
			TRACEX("%s\n", szCommand);
			_SYSTEM(szCommand);
			return FALSE;
		}
	}
	else if(iAutoCFGStartFlage == 1)
	{
		iLen = sprintf(pszSendBuf,"%2d",AUTO_CONFIG_START);
		pszSendBuf[iLen] = 0;
		if((write(fd2,pszSendBuf,sizeof(pszSendBuf) + 1)) < 0)
		{
			DELETE(pszSendBuf);
			pszSendBuf = NULL;
			close(fd2);
			char			szCommand[30];
			iLen = sprintf(szCommand,"kill ");
			strncpyz(szCommand + iLen,buf, MAX_COMM_PID_LEN + 1);
			TRACEX("%s\n", szCommand);
			_SYSTEM(szCommand);
			return FALSE;
		}
	}
	else if(iSlaveMode == 1)
	{
		iLen = sprintf(pszSendBuf,"%2d",-5);
		pszSendBuf[iLen] = 0;
		if((write(fd2,pszSendBuf,sizeof(pszSendBuf) + 1)) < 0)
		{
			DELETE(pszSendBuf);
			pszSendBuf = NULL;
			close(fd2);
			char			szCommand[30];
			iLen = sprintf(szCommand,"kill ");
			strncpyz(szCommand + iLen,buf, MAX_COMM_PID_LEN + 1);
			TRACEX("%s\n", szCommand);
			_SYSTEM(szCommand);
			return FALSE;
		}
	}
	else
	{
		if(iReturn == NO_THIS_USER)
		{
			iLen = sprintf(pszSendBuf,"%2d",NO_THIS_USER);
			pszSendBuf[iLen] = 0;
			if((write(fd2,pszSendBuf,sizeof(pszSendBuf) + 1)) < 0)
			{
				DELETE(pszSendBuf);
				pszSendBuf = NULL;
				close(fd2);
				char			szCommand[30];
				iLen = sprintf(szCommand,"kill ");
				strncpyz(szCommand + iLen,buf, MAX_COMM_PID_LEN + 1);
				TRACEX("%s\n", szCommand);
				_SYSTEM(szCommand);
				return FALSE;
			}
		}


		else if(iReturn == LCD_LOGIN_ONLY)
				{
			AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_WARNING,"Login Failure : User is Enabled for Lcd Login Only");
					iLen = sprintf(pszSendBuf,"%2d",NO_MATCH_PASSWORD);
					pszSendBuf[iLen] = 0;
					if((write(fd2,pszSendBuf,sizeof(pszSendBuf) + 1))<0)
					{
						DELETE(pszSendBuf);
						pszSendBuf = NULL;
						close(fd2);
						char			szCommand[30];
						iLen = sprintf(szCommand,"kill ");
						strncpyz(szCommand + iLen,buf, MAX_COMM_PID_LEN + 1);
						TRACEX("%s\n", szCommand);
						_SYSTEM(szCommand);
						return FALSE;
					}
				}


		else if(iReturn == NO_MATCH_PASSWORD)
		{
			iLen = sprintf(pszSendBuf,"%2d",NO_MATCH_PASSWORD);
			pszSendBuf[iLen] = 0;
			if((write(fd2,pszSendBuf,sizeof(pszSendBuf) + 1))<0)
			{
				DELETE(pszSendBuf);
				pszSendBuf = NULL;
				close(fd2);
				char			szCommand[30];
				iLen = sprintf(szCommand,"kill ");
				strncpyz(szCommand + iLen,buf, MAX_COMM_PID_LEN + 1);
				TRACEX("%s\n", szCommand);
				_SYSTEM(szCommand);
				return FALSE;
			}
		}
		else
		{
			iLen  += sprintf(pszSendBuf,"%2d",(int)iReturn);
			int iPipeLen = iLen /(PIPE_BUF - 2)  + 1;
			if(iPipeLen <= 1)
			{
				if((write(fd2, pszSendBuf, PIPE_BUF - 1)) < 0)
				{
					close(fd2);
					return FALSE;
				}
			}
			else
			{
				int m;
				char szP[PIPE_BUF -1];
				for(m = 0; m < iPipeLen; m++)
				{
					memset(szP, 0x0, PIPE_BUF -1);
					strncpyz(szP, pszSendBuf + m * (PIPE_BUF - 2), PIPE_BUF - 1);
					if((write(fd2, szP, PIPE_BUF - 1)) < 0)
					{
						close(fd2);
						return FALSE;
					}
				}
			}
		}
	}
	close(fd2);
	if(pszSendBuf != NULL)
	{
		DELETE(pszSendBuf);
		pszSendBuf = NULL;
	}
	return TRUE;
}

static int GetSlaveMode(void)
{
	int iSystemID = 1;
	SIG_BASIC_VALUE* pSigValue;
	int		iVarSubID =DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 180);
	int		iError = 0;
	int	iBufLen = 0;
	int	iTimeOut = 0;

	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		iSystemID,	//DCD1 ID		
		iVarSubID,		
		&iBufLen,			
		(void *)&pSigValue,			
		iTimeOut);

	if(iError == ERR_DXI_OK)
	{
		if(pSigValue->varValue.enumValue == 2 )
		{
			return 1;
		}
	}
	else
	{

		return 0;
	}
}

/*==========================================================================*
* FUNCTION :  Web_MakeV6NetWorkInfoBuffer
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN char * szIP:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Zhao Zicheng               DATE: 2014-08-11
*==========================================================================*/
static char *Web_MakeV6NetWorkInfoBuffer(void)
{
    char			*szMakeBuf;
    int				iLen = 0;
    struct IN6_ADDR	*pIn;
    int				iBufLen, iVarID, iError, iTimeOut;
    char szV6IP[128];
    ACU_V6_NET_INFO 	stACUV6NetInfo = {0};
    int nBufLen;
    int nDHCP = APP_DHCP_OFF;
    int iDHCPState = 0;

    /////////////////////////////////////
    //Get local IP address,add port number
    iTimeOut = 0;
    iError = ERR_DXI_OK;
    iVarID = NET_INFO_ALL;
    iBufLen = sizeof(ACU_V6_NET_INFO);

    iError += DxiGetData(VAR_NET_IPV6_INFO,
	0,			
	0,		
	&iBufLen,			
	&stACUV6NetInfo,
	iTimeOut);

    if(DxiGetData(VAR_APP_DHCP_INFO,
	DXI_VAR_ID_IPV6,
	0,
	&nBufLen,
	&nDHCP,
	0) == ERR_DXI_OK)
    {
	if(nDHCP == APP_DHCP_ON)
	{
	    iDHCPState = 1;
	}
	else
	{
	    iDHCPState = 0;
	}
    }
    //debug begin
    TRACE_WEB_USER_NOT_CYCLE("Local IP\n");
    TRACE_WEB_USER_NOT_CYCLE("%d    %d	%d  %d	%d\n", stACUV6NetInfo.stLocalAddr.ifr6_addr.in6_u.u6_addr32[0],
	stACUV6NetInfo.stLocalAddr.ifr6_addr.in6_u.u6_addr32[1],
	stACUV6NetInfo.stLocalAddr.ifr6_addr.in6_u.u6_addr32[2],
	stACUV6NetInfo.stLocalAddr.ifr6_addr.in6_u.u6_addr32[3],
	stACUV6NetInfo.stLocalAddr.ifr6_prefixlen);
    TRACE_WEB_USER_NOT_CYCLE("Global IP\n");
    TRACE_WEB_USER_NOT_CYCLE("%d    %d	%d  %d	%d\n", stACUV6NetInfo.stGlobalAddr.ifr6_addr.in6_u.u6_addr32[0],
	stACUV6NetInfo.stGlobalAddr.ifr6_addr.in6_u.u6_addr32[1],
	stACUV6NetInfo.stGlobalAddr.ifr6_addr.in6_u.u6_addr32[2],
	stACUV6NetInfo.stGlobalAddr.ifr6_addr.in6_u.u6_addr32[3],
	stACUV6NetInfo.stGlobalAddr.ifr6_prefixlen);
    TRACE_WEB_USER_NOT_CYCLE("Gateway IP\n");
    TRACE_WEB_USER_NOT_CYCLE("%d    %d	%d  %d\n", stACUV6NetInfo.stGateWay.in6_u.u6_addr32[0],
	stACUV6NetInfo.stGateWay.in6_u.u6_addr32[1],
	stACUV6NetInfo.stGateWay.in6_u.u6_addr32[2],
	stACUV6NetInfo.stGateWay.in6_u.u6_addr32[3]);
    //debug end

    szMakeBuf = NEW(char, 500);
    if(szMakeBuf == NULL)
    {
	return NULL;
    }
    if(iError == ERR_DXI_OK)
    {
	// local address
	memset((void *)szV6IP, 0, 128);
	pIn = &(stACUV6NetInfo.stLocalAddr.ifr6_addr);
	inet_ntop(AF_INET6, (const void *)pIn, szV6IP, 128);
	TRACE_WEB_USER_NOT_CYCLE("stLocalAddr is %s\n", szV6IP);
	iLen += sprintf(szMakeBuf + iLen, "%s,", szV6IP);

	//local prefix
	iLen += sprintf(szMakeBuf + iLen, "%d,", stACUV6NetInfo.stLocalAddr.ifr6_prefixlen);

	//global address
	memset((void *)szV6IP, 0, 128);
	pIn = &(stACUV6NetInfo.stGlobalAddr.ifr6_addr);
	inet_ntop(AF_INET6, (const void *)pIn, szV6IP, 128);
	TRACE_WEB_USER_NOT_CYCLE("stGlobalAddr is %s\n", szV6IP);
	iLen += sprintf(szMakeBuf + iLen, "%s,", szV6IP);

	//global prefix
	iLen += sprintf(szMakeBuf + iLen, "%d,", stACUV6NetInfo.stGlobalAddr.ifr6_prefixlen);

	//gate way
	memset((void *)szV6IP, 0, 128);
	pIn = &(stACUV6NetInfo.stGateWay);
	inet_ntop(AF_INET6, (const void *)pIn, szV6IP, 128);
	TRACE_WEB_USER_NOT_CYCLE("stGateWay is %s\n", szV6IP);
	iLen += sprintf(szMakeBuf + iLen, "%s,", szV6IP);

	iLen += sprintf(szMakeBuf + iLen, "%d,", iDHCPState);

	return szMakeBuf;
    }
    else
    {
	if(szMakeBuf != NULL)
	{
	    DELETE(szMakeBuf);
	    szMakeBuf = NULL;
	}
	return NULL;
    }

}

/*==========================================================================*
* FUNCTION :  Web_MakeNetWorkInfoBuffer
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN char * szIP:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
*==========================================================================*/
static char *Web_MakeNetWorkInfoBuffer(void)
{
	char			*szMakeBuf;
	int				iLen = 0;
	struct in_addr	*pIn;
	int				iBufLen, iVarID, iError, iTimeOut;
	ACU_NET_INFO 	stACUNetInfo;


	/////////////////////////////////////
	//Get local IP address,add port number
	iTimeOut = 100;
	iError = ERR_DXI_OK;
	iVarID = NET_INFO_ALL;
	iBufLen = sizeof(ACU_NET_INFO);


	iError += DxiGetData(VAR_ACU_NET_INFO,
		iVarID,			
		0,		
		&iBufLen,			
		&stACUNetInfo,			
		iTimeOut);

	szMakeBuf = NEW(char, 128);
	if(szMakeBuf == NULL)
	{
		return NULL;
	}
	if (iError == ERR_DXI_OK)
	{
		pIn = (struct in_addr*)(&(stACUNetInfo.ulIp));
		iLen += sprintf(szMakeBuf + iLen, "%32s,",inet_ntoa(*pIn));

		pIn = (struct in_addr*)(&(stACUNetInfo.ulMask));
		iLen += sprintf(szMakeBuf + iLen, "%32s,",inet_ntoa(*pIn));

		pIn = (struct in_addr*)(&(stACUNetInfo.ulGateway));
		iLen += sprintf(szMakeBuf + iLen, "%32s,",inet_ntoa(*pIn));


		return szMakeBuf;

	}
	else
	{
	    if(szMakeBuf != NULL)
	    {
		DELETE(szMakeBuf);
		szMakeBuf = NULL;
	    }
		return NULL;
	}

}

static void Web_Arping(void)
{
	char			szMakeBuf[80];
	int			iLen = 0;
	struct in_addr		*pIn;
	int			iBufLen, iVarID, iError, iTimeOut;
	ACU_NET_INFO 		stACUNetInfo;


	/////////////////////////////////////
	//Get local IP address,add port number
	iTimeOut = 100;
	iError = ERR_DXI_OK;
	iVarID = NET_INFO_ALL;
	iBufLen = sizeof(ACU_NET_INFO);

	iError += DxiGetData(VAR_ACU_NET_INFO,
		iVarID,			
		0,		
		&iBufLen,			
		&stACUNetInfo,			
		iTimeOut);

	memset(szMakeBuf, 0x0, sizeof(szMakeBuf));
	if (iError == ERR_DXI_OK)
	{
		pIn = (struct in_addr*)(&(stACUNetInfo.ulIp));
		sprintf(szMakeBuf, "arping -U -c 4 -I eth0 %32s",inet_ntoa(*pIn));
		printf("szMakeBuf = %s\n",szMakeBuf);
		_SYSTEM(szMakeBuf);
	}
	
}


static BOOL Web_GetProtectedStatus(void) 
{
	return FALSE;

	SITE_INFO  *pSiteInfo = NULL;
	int	iBufLen = 0;

	iBufLen = sizeof(SITE_INFO);
	int iError = DxiGetData(VAR_ACU_PUBLIC_CONFIG,
		SITE_INFO_POINTER,			
		0,		
		&iBufLen,			
		&pSiteInfo,			
		0);

	if (iError == ERR_DXI_OK)
	{
		return pSiteInfo->bSettingChangeDisabled;
	}
	else
	{
		return FALSE;
	}
}

/*==========================================================================*
* FUNCTION :  Web_Modify_V6_AcuIP_Addr
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN char * szIP:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Zhao Zicheng               DATE: 2014-8-12
*==========================================================================*/
static int Web_Modify_V6_AcuIP_Addr(IN char * szIP)
{
#define DXI_VAR_ID_IPV6          1

    ASSERT(szIP);
    IN6_ADDR			stIPAddr, stGateWay;
    ACU_V6_NET_INFO	stV6ACUNetInfo;
    int				iBufLen, iError;
    char			*pSearchValue = NULL, *ptr = NULL;
    int				iPosition = 0;
    char			szExchange[64];
    char			*szTrimText = NULL;
    int				iDHCPState = 0;

    int nDHCP = APP_DHCP_OFF;

    int nBufLen;

    if(DxiGetData(VAR_APP_DHCP_INFO,
	DXI_VAR_ID_IPV6,
	0,
	&nBufLen,
	&nDHCP,
	0) == ERR_DXI_OK)
    {
	if(nDHCP == APP_DHCP_ON)
	{
	    iDHCPState = 1;
	}
	else
	{
	    iDHCPState = 0;
	}
    }

    ptr = szIP;
    TRACE_WEB_USER_NOT_CYCLE("ptr is %s\n", ptr);
    /*IP*/
    pSearchValue = strchr(ptr, 59);
    if(pSearchValue != NULL)
    {
	iPosition = pSearchValue - ptr;
	if(iPosition > 0)
	{
	    strncpyz(szExchange, ptr, iPosition + 1);
	    szTrimText = Cfg_RemoveWhiteSpace(szExchange);
	    TRACE_WEB_USER_NOT_CYCLE("szTrimText is %s\n", szTrimText);
	    inet_pton(AF_INET6, (const char *)szTrimText, (void *)(&stIPAddr));
	    ptr = ptr + iPosition;
	}
    }
    ptr = ptr + 1;

    /*gate way*/
    pSearchValue = strchr(ptr, 59);
    if(pSearchValue != NULL)
    {
	iPosition = pSearchValue - ptr;
	if(iPosition > 0)
	{
	    strncpyz(szExchange, ptr, iPosition + 1);
	    szTrimText = Cfg_RemoveWhiteSpace(szExchange);
	    TRACE_WEB_USER_NOT_CYCLE("szTrimText is %s\n", szTrimText);
	    inet_pton(AF_INET6, (const char *)szTrimText, (void *)(&stGateWay));
	    ptr = ptr + iPosition;
	}
    }
    ptr = ptr + 1;

    /*Prefix*/
    pSearchValue = strchr(ptr, 59);
    if(pSearchValue != NULL)
    {
	iPosition = pSearchValue - ptr;
	if(iPosition > 0)
	{
	    strncpyz(szExchange, ptr, iPosition + 1);
	    szTrimText = Cfg_RemoveWhiteSpace(szExchange);
	    TRACE_WEB_USER_NOT_CYCLE("szTrimText is %s\n", szTrimText);
	    stV6ACUNetInfo.stGlobalAddr.ifr6_prefixlen = atoi(szTrimText);
	}
    }
    iBufLen  = sizeof(ACU_V6_NET_INFO);

    memcpy((void *)(&(stV6ACUNetInfo.stGlobalAddr.ifr6_addr)), (const void*)(&stIPAddr), sizeof(IN6_ADDR));
    memcpy((void *)(&(stV6ACUNetInfo.stGateWay)), (const void*)(&stGateWay), sizeof(IN6_ADDR));

    //debug begin
    TRACE_WEB_USER_NOT_CYCLE("Gateway IP\n");
    TRACE_WEB_USER_NOT_CYCLE("%d    %d	%d  %d\n", stV6ACUNetInfo.stGateWay.in6_u.u6_addr32[0],
	stV6ACUNetInfo.stGateWay.in6_u.u6_addr32[1],
	stV6ACUNetInfo.stGateWay.in6_u.u6_addr32[2],
	stV6ACUNetInfo.stGateWay.in6_u.u6_addr32[3]);
    //debug end

    if(iDHCPState == 0)
    {
	TRACE_WEB_USER_NOT_CYCLE("DHCP is off\n");
	iError = DxiSetData(VAR_NET_IPV6_INFO,
	IPV6_CHANGE_ALL,			
	0,		
	iBufLen,			
	&stV6ACUNetInfo,			
	0);
    }
    else
    {
	TRACE_WEB_USER_NOT_CYCLE("DHCP is on\n");
	iError = DxiSetData(VAR_NET_IPV6_INFO,
	    IPV6_CHANGE_GW,			
	    0,		
	    iBufLen,			
	    &stV6ACUNetInfo,			
	    0);
    }
    if(iError == ERR_DXI_OK)
    {
	return TRUE;
    }
    else  
    {
	if(iError == ERR_DXI_HARDWARE_SWITCH_STATUS)
	{
	    return WEB_RETURN_PROTECTED_ERROR;
	}
	else
	{
	    return FALSE;
	}
    }
}
/*==========================================================================*
* FUNCTION :  Web_Modify_AcuIP_Addr
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN char * szIP:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
*==========================================================================*/

static int Web_Modify_AcuIP_Addr(IN char * szIP)
{
#ifdef _SHOW_WEB_INFO
	TRACE("szIP : %s\n", szIP);
#endif	

	ASSERT(szIP);
	ULONG			ulIPAddr, ulMask, ulGateWay;
	ACU_NET_INFO	stACUNetInfo;
	int				iBufLen, iError;
	char			*pSearchValue = NULL, *ptr = NULL;
	int				iPosition = 0;
	char			szExchange[33];
	char			*szTrimText = NULL;

	int nDHCP = APP_DHCP_OFF;

	int nBufLen;

	if(DxiGetData(VAR_APP_DHCP_INFO,
		0,
		0,
		&nBufLen,
		&nDHCP,
		0) == ERR_DXI_OK)
	{
		if(nDHCP == APP_DHCP_ON)
		{
			return 6;
		}
	}

	ptr = szIP;
	/*IP*/
	pSearchValue = strchr(ptr, 59);
	if(pSearchValue != NULL)
	{
		iPosition = pSearchValue - ptr;
		if(iPosition > 0)
		{
			strncpyz(szExchange, ptr, iPosition + 1);
			szTrimText = Cfg_RemoveWhiteSpace(szExchange);
			TRACE_WEB_USER_NOT_CYCLE("szTrimText is %s\n", szTrimText);
			if(atoi(szTrimText) == 0)
			{
				ulIPAddr = inet_addr("0.0.0.0");
			}
			else
			{
				ulIPAddr = inet_addr(szTrimText);
			}
			TRACE_WEB_USER_NOT_CYCLE("ulIPAddr is %ld\n", ulIPAddr);
			ptr = ptr + iPosition;
		}
	}
	ptr = ptr + 1;

	/*Mask*/
	pSearchValue = strchr(ptr, 59);
	if(pSearchValue != NULL)
	{
		iPosition = pSearchValue - ptr;
		if(iPosition > 0)
		{
			strncpyz(szExchange, ptr, iPosition + 1);
			szTrimText = Cfg_RemoveWhiteSpace(szExchange);
			if(atoi(szTrimText) == 0)
			{
				ulMask = inet_addr("0.0.0.0");
			}
			else
			{
				ulMask = inet_addr(szTrimText);
			}
			ptr = ptr + iPosition;
		}
	}
	ptr = ptr + 1;

	/*Gateway*/
	pSearchValue = strchr(ptr, 59);
	if(pSearchValue != NULL)
	{
		iPosition = pSearchValue - ptr;
		if(iPosition > 0)
		{
			strncpyz(szExchange, ptr, iPosition + 1);

			szTrimText = Cfg_RemoveWhiteSpace(szExchange);
#ifdef _SHOW_WEB_INFO
			TRACE("szTrimText:[%s]", szTrimText);
#endif

			if(atoi(szTrimText) == 0)
			{
				ulGateWay = inet_addr("0.0.0.0");
			}
			else
			{
				ulGateWay = inet_addr(szTrimText);
			}

			//ptr = ptr + iPosition;
		}
	}
	iBufLen  = sizeof(ACU_NET_INFO);

	stACUNetInfo.ulIp = ulIPAddr;
	stACUNetInfo.ulMask = ulMask;;
	stACUNetInfo.ulGateway = ulGateWay;
	stACUNetInfo.ulBroardcast= (ULONG)0;

	iError = DxiSetData(VAR_ACU_NET_INFO,
		NET_INFO_ALL,			
		0,		
		iBufLen,			
		&stACUNetInfo,			
		0);
#ifdef _SHOW_WEB_INFO
	TRACE("iError :[%d]\n", iError);
#endif

	if (iError == ERR_DXI_OK)
	{
		return TRUE;
	}
	else  
	{

		if(iError == ERR_DXI_HARDWARE_SWITCH_STATUS)
		{
			return WEB_RETURN_PROTECTED_ERROR;
		}
		else
		{
			return FALSE;
		}
	}


}

#define GET_SERVICE_OF_ESR_NAME			"eem_soc.so"
#define GET_SERVICE_OF_NMS_NAME			"snmp_agent.so"
#define GET_SERVICE_OF_YDN_NAME			"ydn23.so"
#define GET_SERVICE_OF_MODBUS_NAME			"modbus.so"
#define GET_SERVICE_OF_WEB_USER_NAME			"web_user.so"
/*==========================================================================*
* FUNCTION :  Web_MakeESRPrivateConfigreBuffer
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
*==========================================================================*/

static char *Web_MakeESRPrivateConfigreBuffer(void)
{
	COMMON_CONFIG	stCommonConfig;
	APP_SERVICE		*stAppService = NULL;
	int				iLen = 0;
	char			*szReturn = NULL;
	int				iReturn = 0;
	stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_ESR_NAME);

	if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
	{

		iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
			stAppService->bServiceRunning,
			&stAppService->args, 
			0x0,
			0,
			(void *)&stCommonConfig);


		szReturn = NEW(char, 950);

		if(szReturn != NULL)
		{
			//TRACE("stAppService ---- 4\n");
			//iLen += sprintf(szReturn + iLen, "%2d", iReturn);
			if(iReturn == ERR_SERVICE_CFG_OK)
			{
				iLen += sprintf(szReturn + iLen, "%d,%d,%d,%d,%d,%d,%d,", 
					stCommonConfig.iProtocolType,
					stCommonConfig.iMediaType,
					stCommonConfig.iAttemptElapse/1000,
					stCommonConfig.iMaxAttempts,
					stCommonConfig.bCallbackInUse,
					stCommonConfig.bReportInUse,
					stCommonConfig.iSecurityLevel);
				iLen += sprintf(szReturn + iLen, "\"%s\",", stCommonConfig.szAlarmReportPhoneNumber[0]);
				iLen += sprintf(szReturn + iLen, "\"%s\",", stCommonConfig.szAlarmReportPhoneNumber[1]);
				iLen += sprintf(szReturn + iLen, "\"%s\",", stCommonConfig.szCallbackPhoneNumber[0]);
				iLen += sprintf(szReturn + iLen, "\"%s\",", stCommonConfig.szReportIP[0]);
				iLen += sprintf(szReturn + iLen, "\"%s\",", stCommonConfig.szReportIP[1]);
				iLen += sprintf(szReturn + iLen, "\"%s\",", stCommonConfig.szSecurityIP[0]);
				iLen += sprintf(szReturn + iLen, "\"%s\",", stCommonConfig.szSecurityIP[1]);
				iLen += sprintf(szReturn + iLen, "%d,",		stCommonConfig.byCCID);
				iLen += sprintf(szReturn + iLen, "%d,",		stCommonConfig.iSOCID);
				iLen += sprintf(szReturn + iLen, "\"%s\"",		stCommonConfig.szCommPortParam);

				return szReturn;


			}
		}

	}
	TRACE("return NULL\n");
	return NULL;
}

static void Web_TransferESRCfgToFormat(IN char *szFormat, OUT int *iValidate)
{
	//ASSERT(szFormat);
	char	*pSearchValue = NULL;
	if(szFormat != NULL)
	{
		pSearchValue = strrchr(szFormat,'*');
		if(pSearchValue != NULL)
		{
			//TRACE("[%s][%s][%d]\n", pSearchValue, pSearchValue + 1, atoi(pSearchValue + 1));

			if(atoi(pSearchValue + 1) == 0)
			{
				*iValidate = 0;
			}
			else if(atoi(pSearchValue + 1) == 1)
			{
				*iValidate = 1;
			}
			else
			{
				*iValidate = -1;	
			}
			*pSearchValue = 0;

		}
	}
	else
	{
		*iValidate = -1;
	}

}
/*==========================================================================*
* FUNCTION :  Web_GetESRModifyInfo
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN char *szBuffer:
OUT COMMON_CONFIG *stCommonConfig:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
*==========================================================================*/
static int Web_GetESRModifyInfo(IN char *szBuffer, OUT COMMON_CONFIG *stCommonConfig)
{
	ASSERT(szBuffer);
	ASSERT(stCommonConfig);

	char			*ptr = NULL;
	char			szExchange[64], *pSearchValue = NULL;
	int				iPosition = 0;
	char			*pTrim = NULL;
	int				iModifyType = ESR_CFG_W_MODE;

	//Aanalyze szBuffer
	int			iValdateModify = 0;
	TRACE_WEB_USER_NOT_CYCLE("szBuffer is %s\n", szBuffer);
	if((ptr = szBuffer) != NULL)
	{

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					stCommonConfig->iProtocolType = atoi(szExchange) ;
					//TRACE("stCommonConfig->iProtocolType :[%s][%d]\n", szExchange, stCommonConfig->iProtocolType);
					iModifyType = iModifyType | ESR_CFG_PROTOCOL_TYPE;
				}
				ptr = ptr + iPosition;

			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					stCommonConfig->iMediaType = atoi(szExchange);
					//TRACE("stCommonConfig->iMediaType :[%s][%d]\n", szExchange, stCommonConfig->iMediaType);
					iModifyType = iModifyType | ESR_CFG_MEDIA_TYPE;
				}
				ptr = ptr + iPosition;

			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					stCommonConfig->iAttemptElapse = 1000 * atoi(szExchange);
					//TRACE("stCommonConfig->iAttemptElapse :[%s][%d]\n", szExchange, stCommonConfig->iAttemptElapse);
					iModifyType = iModifyType | ESR_CFG_ATTEMPT_ELAPSE;
				}
				ptr = ptr + iPosition;
			}
		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					stCommonConfig->iMaxAttempts = atoi(szExchange);
					//TRACE("stCommonConfig->iMaxAttempts :[%s][%d]\n", szExchange,stCommonConfig->iMaxAttempts);
					iModifyType = iModifyType | ESR_CFG_MAX_ATTEMPTS;
				}
				ptr = ptr + iPosition;
			}
		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					if(iPosition > 2)
					{
						stCommonConfig->bCallbackInUse = TRUE;
					}
					else
					{
						stCommonConfig->bCallbackInUse = FALSE;
					}

					//TRACE("stCommonConfig->bCallbackInUse :[%s][%d]\n", szExchange,stCommonConfig->bCallbackInUse);
					iModifyType = iModifyType | ESR_CFG_CALLBACK_IN_USE;
				}
				ptr = ptr + iPosition;
			}


		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					if(iPosition > 2)
					{
						stCommonConfig->bReportInUse = TRUE;
					}
					else
					{
						stCommonConfig->bReportInUse = FALSE;
					}
					//TRACE("stCommonConfig->bReportInUse :[%s][%d]\n", szExchange, stCommonConfig->bReportInUse);
					iModifyType = iModifyType | ESR_CFG_REPORT_IN_USE;
				}
				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{

				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					stCommonConfig->iSecurityLevel = atoi(szExchange);// + 1; the front end code has add 1
					//TRACE("stCommonConfig->iSecurityLevel :[%s][%d]\n", szExchange,stCommonConfig->iSecurityLevel);
					iModifyType = iModifyType | ESR_CFG_SECURITY_LEVEL;
				}
				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					pTrim = Cfg_RemoveWhiteSpace(szExchange);
					strncpyz(stCommonConfig->szAlarmReportPhoneNumber[0],
						pTrim,
						sizeof(stCommonConfig->szAlarmReportPhoneNumber[0]));

					//TRACE("stCommonConfig->szAlarmReportPhoneNumber :[%s][%s]\n", szExchange,stCommonConfig->szAlarmReportPhoneNumber[0]);
					iModifyType = iModifyType | ESR_CFG_REPORT_NUMBER_1;
				}
				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					pTrim = Cfg_RemoveWhiteSpace(szExchange);
					strncpyz(stCommonConfig->szAlarmReportPhoneNumber[1],
						pTrim,
						sizeof(stCommonConfig->szAlarmReportPhoneNumber[1]));

					//TRACE("stCommonConfig->szAlarmReportPhoneNumber :[%s][%s]\n", szExchange, stCommonConfig->szAlarmReportPhoneNumber[1]);
					iModifyType = iModifyType | ESR_CFG_REPORT_NUMBER_2;
				}
				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;


		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);

				if(iValdateModify == 1)
				{
					pTrim = Cfg_RemoveWhiteSpace(szExchange);
					strncpyz(stCommonConfig->szCallbackPhoneNumber[0],
						pTrim,
						(int)sizeof(stCommonConfig->szCallbackPhoneNumber[0]));

					//TRACE("stCommonConfig->szCallbackPhoneNumber :[%s][%s]\n", szExchange, stCommonConfig->szCallbackPhoneNumber[0]);
					iModifyType = iModifyType | ESR_CFG_CALLBACK_NUMBER;
				}
				ptr = ptr + iPosition;

			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					pTrim = Cfg_RemoveWhiteSpace(szExchange);
					TRACE_WEB_USER_NOT_CYCLE("pTrim is %s\n", pTrim);
					strncpyz(stCommonConfig->szReportIP[0],
						pTrim,
						sizeof(stCommonConfig->szReportIP[0]));

					//TRACE("stCommonConfig->szReportIP :[%s][%s]\n", szExchange,stCommonConfig->szReportIP[0]);
					iModifyType = iModifyType | ESR_CFG_IPADDR_1;
				}
				ptr = ptr + iPosition;

			}
		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					pTrim = Cfg_RemoveWhiteSpace(szExchange);
					TRACE_WEB_USER_NOT_CYCLE("pTrim is %s\n", pTrim);
					strncpyz(stCommonConfig->szReportIP[1],
						pTrim,
						sizeof(stCommonConfig->szReportIP[1]));

					//TRACE("stCommonConfig->szReportIP :[%s][%s]\n", szExchange,stCommonConfig->szReportIP[1]);
					iModifyType = iModifyType | ESR_CFG_IPADDR_2;
				}
				ptr = ptr + iPosition;

			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					pTrim = Cfg_RemoveWhiteSpace(szExchange);
					TRACE_WEB_USER_NOT_CYCLE("pTrim is %s\n", pTrim);
					strncpyz(stCommonConfig->szSecurityIP[0],
						pTrim,
						sizeof(stCommonConfig->szSecurityIP[0]));

					//TRACE("stCommonConfig->szSecurityIP :[%s][%s]\n", szExchange,stCommonConfig->szSecurityIP[0]);
					iModifyType = iModifyType | ESR_CFG_SECURITY_IP_1;
				}
				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					pTrim = Cfg_RemoveWhiteSpace(szExchange);
					TRACE_WEB_USER_NOT_CYCLE("pTrim is %s\n", pTrim);
					//TRACE("pTrim : %s\n", pTrim);
					strncpyz(stCommonConfig->szSecurityIP[1],
						pTrim,
						sizeof(stCommonConfig->szSecurityIP[1]));

					//TRACE("stCommonConfig->szSecurityIP :[%s][%s]\n", szExchange, stCommonConfig->szSecurityIP[0]);
					iModifyType = iModifyType | ESR_CFG_SECURITY_IP_2;
				}
				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					pTrim = Cfg_RemoveWhiteSpace(szExchange);
					stCommonConfig->byCCID = (BYTE)atoi(pTrim);

					//TRACE("stCommonConfig->byCCID :[%s][%d]\n", szExchange, stCommonConfig->byCCID);
					iModifyType = iModifyType | ESR_CFG_CCID;
				}
				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					pTrim = Cfg_RemoveWhiteSpace(szExchange);
					stCommonConfig->iSOCID = atoi(pTrim);
					//TRACE("stCommonConfig->iSOCID :[%s][%d]\n", szExchange, stCommonConfig->iSOCID);
					iModifyType = iModifyType | ESR_CFG_SOCID;
				}
				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;


		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				if(iValdateModify == 1)
				{
					pTrim = Cfg_RemoveWhiteSpace(szExchange);
					sprintf(stCommonConfig->szCommPortParam, "%s", pTrim);
					//TRACE("stCommonConfig->szCommPortParam :[%s][%s]\n", szExchange, stCommonConfig->szCommPortParam);
					iModifyType = iModifyType | ESR_CFG_MEDIA_PORT_PARAM;
				}
				ptr = ptr + iPosition;
			}

		}

		return iModifyType;
	}
	return FALSE;

}


/*==========================================================================*
* FUNCTION :  Web_ModifyESRPrivateConfigure
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN char *szBuffer:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
*==========================================================================*/
static int Web_ModifyESRPrivateConfigure(IN char *szBuffer)
{

	ASSERT(szBuffer);
	COMMON_CONFIG	stCommonConfig ;
	//YDN_COMMON_CONFIG	stYDNCommonConfig ;
	APP_SERVICE		*stAppService = NULL;
	int				iModifyType = 0;
	//int				iYDNModifyType = 0;
	int				iReturn = 0;
#ifdef _SHOW_WEB_INFO
	//TRACE("szBuffer : %s\n", szBuffer);
#endif


	if((iModifyType = Web_GetESRModifyInfo(szBuffer, &stCommonConfig)) != FALSE)
	{
		////TRACE("iModifyType : %x ----- 1\n", iModifyType);
		////Change ydn protocol to EEM .
		//if(stCommonConfig.iProtocolType != YDN23)
		//{
		//	stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_YDN_NAME);
		//
		//	if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
		//	{
		//		stYDNCommonConfig.iProtocolType = YDN23;			
		//		iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
		//					stAppService->bServiceRunning,
		//					&stAppService->args, 
		//					0x0,
		//					0,
		//					(void *)&stYDNCommonConfig);
		//		 if(iReturn == ERR_SERVICE_CFG_OK)
		//		 {
		//			stYDNCommonConfig.iProtocolType = EEM;
		//			iYDNModifyType = iYDNModifyType | YDN_CFG_W_MODE;
		//			iYDNModifyType = iYDNModifyType | YDN_CFG_ALL | YDN_CFG_PROTOCOL_TYPE | YDN_CFG_MEDIA_TYPE | YDN_CFG_MEDIA_PORT_PARAM;
		//			stYDNCommonConfig.iMediaType = 2;//tcp/ip
		//			strncpyz(stYDNCommonConfig.szCommPortParam,"5050",5);
		//			TRACE("stYDNCommonConfig.szCommPortParam = %s\n", stYDNCommonConfig.szCommPortParam);

		//			iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
		//				stAppService->bServiceRunning,
		//				&stAppService->args, 
		//				iYDNModifyType,
		//				0,
		//				(void *)&stYDNCommonConfig);// == ERR_SERVICE_CFG_OK)
		//			if(iReturn != ERR_SERVICE_CFG_OK)
		//			{
		//				return iReturn;
		//			}
		//		 }				
		//	}
		//}


		stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_ESR_NAME);

		if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
		{
			//TRACE("iModifyType : %x ----- 2", iModifyType);
			//TRACE("stAppService->bServiceRunning : %d\n",stAppService->bServiceRunning);


			iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
				stAppService->bServiceRunning,
				&stAppService->args, 
				iModifyType,
				0,
				(void *)&stCommonConfig);// == ERR_SERVICE_CFG_OK)

			//TRACE("iReturn : %d ----- 3\n", iReturn);
			return iReturn;
		}
	}
	return ERR_SERVICE_CFG_FAIL;
}

#define WEB_MODIFY_TIMESRV_IP			1
#define WEB_MODIFY_TIME					2


/*==========================================================================*
* FUNCTION :  Web_MakeACUTimeSrv
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
*==========================================================================*/

static char *Web_MakeACUTimeSrv(void)
{
	TIME_SRV_INFO	*pTimeSvrInfo = NULL;
	int				iError = 0, iLen = 0;
	char			*szTime = NULL;
	struct in_addr	inIP;
	int	iBufLen = 0, iBufLen1 = 0;
	SIG_BASIC_VALUE* pNTPEnableSigValue = NULL;

	iBufLen = sizeof(TIME_SRV_INFO);
	iError += DxiGetData(VAR_TIME_SERVER_INFO,
		TIME_SRV_CONFIG,			
		0,		
		&iBufLen,			
		&pTimeSvrInfo,			
		0);

	iError += DxiGetData(VAR_A_SIGNAL_VALUE,
		1,
		DXI_MERGE_SIG_ID(2, 701),
		&iBufLen1,
		(void *)&pNTPEnableSigValue,
		0);

	if (iError == ERR_DXI_OK && pNTPEnableSigValue != NULL)
	{	
		//TRACE("iError : %d\n", iError);
		if((szTime = NEW(char, 128)) != NULL)
		{
			inIP.s_addr = pTimeSvrInfo->ulMainTmSrvAddr;

			iLen += sprintf(szTime + iLen, "%s%c",inet_ntoa(inIP), 59);

			inIP.s_addr = pTimeSvrInfo->ulBakTmSrvAddr;
			iLen += sprintf(szTime + iLen, "%s%c%ld%c",
				inet_ntoa(inIP),59, pTimeSvrInfo->dwJustTimeInterval, 59);
			iLen += sprintf(szTime + iLen, "%ld%c",
				pTimeSvrInfo->lTimeZone, 59);

			iLen += sprintf(szTime + iLen, "%d%c",
				pNTPEnableSigValue->varValue.enumValue, 59);

			return szTime;
		}
		else
		{
			return NULL;
		}
	}
	else
	{
		return NULL;
	}

}
/*==========================================================================*
* FUNCTION :  Web_SetACUTimeSrv
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN int iModiyType:
IN void *pModiyValue:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
*==========================================================================*/
static int Web_SetACUTimeSrv(IN int iModiyType, IN void *pModiyValue, char *szUserName)
{
	int		iError = ERR_DXI_OK;
	ULONG	ulIP;
	char	szIP[33], szExchange[33];
	//char	szTimeZone[16];
	time_t	tmModiy;
	int		iInterval = 0;
	char	*pSearchValue = NULL;
	int		iPosition = 0;
	char	*ptr = NULL;
	char	*szTrimText = NULL;
	int	iBufLen = 0;
	char szTempUserName[SINGLE_ARG_MAX_LEN];
	memset(szTempUserName, 0, sizeof(szTempUserName));
	snprintf(szTempUserName, SINGLE_ARG_MAX_LEN, "Web:%s  ",szUserName);

	ptr = (char *)pModiyValue;
	VAR_VALUE_EX	value;
	value.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
	value.pszSenderName = szTempUserName;
	value.nSenderType = SERVICE_OF_USER_INTERFACE;

	//TRACE("Web_SetACUTimeSrv : %s\n", (char *)pModiyValue);
	if(iModiyType == WEB_MODIFY_TIMESRV_IP)
	{
		//Modify IP
		iBufLen = sizeof(ULONG);

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 1)
			{
				strncpyz(szIP, ptr, iPosition + 1);
				szTrimText = Cfg_RemoveWhiteSpace(szIP);
				if((strcmp(szTrimText,"0.0.0.0") == 0) || atoi(szTrimText) == 0)
				{
					ulIP = inet_addr("0.0.0.0");
				}
				else
				{
					ulIP = inet_addr(szTrimText);
				}

				iError += DxiSetData(VAR_TIME_SERVER_INFO,
					TIME_SRV_IP,			
					0,		
					iBufLen,			
					&ulIP,			
					0);
				ptr = ptr + iPosition;
			}
		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 1)
			{
				strncpyz(szIP, ptr, iPosition + 1);
				szTrimText = Cfg_RemoveWhiteSpace(szIP);

				if((strcmp(szTrimText,"0.0.0.0") == 0) || atoi(szTrimText) == 0)
				{
					ulIP = inet_addr("0.0.0.0");
				}
				else
				{
					ulIP = inet_addr(szTrimText);
				}

				iError += DxiSetData(VAR_TIME_SERVER_INFO,
					TIME_SRV_BACKUP_IP,			
					0,		
					iBufLen,			
					&ulIP,			
					0);

				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;



		pSearchValue = strchr(ptr, 59);

		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition >= 1)
			{
				strncpyz(szIP, ptr, iPosition + 1);
				iInterval = atoi(szIP);
				//TRACE("\niInterval %d\n", iInterval);
				iBufLen = sizeof(DWORD);
				iError += DxiSetData(VAR_TIME_SERVER_INFO,
					TIME_SYNC_INTERVAL,			
					0,		
					iBufLen,			
					&iInterval,			
					0);
				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition >= 1)
			{

				strncpyz(szIP, ptr, iPosition + 1);
				szTrimText = Cfg_RemoveWhiteSpace(szIP);
				iInterval = atoi(szTrimText);

				iBufLen = sizeof(LONG);
				iError += DxiSetData(VAR_TIME_SERVER_INFO,
					TIME_SYNC_TIMEZONE,			
					0,		
					iBufLen,			
					&iInterval,			
					0);
			}

		}
		//Added to run NTP funcion immediately after setting by SongXu20180202.
		value.varValue.enumValue = 1;
		Web_ModifySignalValue(1,SIG_TYPE_SETTING,701,value);

		iError += DxiSetData(VAR_TIME_SERVER_INFO,
			TIME_SYNC_RUN_IMMEDIATELY,			
			0,		
			iBufLen,			
			&iInterval,			
			0);

//printf("web set NTP iError %d\n",iError);
	}
	else if(iModiyType == WEB_MODIFY_TIME)
	{
		//�޸�ʱ��
		pSearchValue = strchr(ptr, 59);
		iBufLen = sizeof(time_t);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 1)
			{

				strncpyz(szExchange, ptr, iPosition + 1);
				tmModiy = (time_t)atoi(szExchange);
				iError += DxiSetData(VAR_TIME_SERVER_INFO,
					SYSTEM_TIME,			
					0,		
					iBufLen,			
					(void *)&tmModiy,			
					0);
			}
		}
printf("web set time iError%d \n",iError);
		value.varValue.enumValue = 0;
		Web_ModifySignalValue(1,SIG_TYPE_SETTING,701,value);
	}
	else
	{
		return FALSE;
	}

	if (iError == ERR_DXI_OK)
	{
		return TRUE;
	}
	else
	{
		return 2;
	}
}

/*==========================================================================*
* FUNCTION :  Web_ReSetCBPara
* PURPOSE  :  
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  ptr: the string including the data
  string: the return string including the data
* RETURN   :  
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2015-06-15
* MODIFY   :  
*==========================================================================*/
static int Web_ReSetCBPara(char *ptr, char* string)
{
    char *pSearchValue1, *pSearchValue2;
    int	iPosition = 0;
    char szReadBuffer[50];
    int iCabinet, iDU, iBranch;
    int iCabinet1, iDU1, iBranch1;
    int iFileLen = 0;
    int iFlag = 0;
    int i;

    TRACE_WEB_USER_NOT_CYCLE("Web_ReSetCBPara ptr is %s\n", ptr);
    pSearchValue2 = ptr;
    pSearchValue1 = strchr(pSearchValue2, 59);
    if(pSearchValue1 != NULL)
    {
	TRACE_WEB_USER_NOT_CYCLE("pSearchValue1 is %s\n", pSearchValue1);
	iPosition = pSearchValue1 - pSearchValue2;
	TRACE_WEB_USER_NOT_CYCLE("iPosition is %d\n", iPosition);
	strncpyz(szReadBuffer, pSearchValue2, iPosition + 1);
	TRACE_WEB_USER_NOT_CYCLE("szReadBuffer is %s\n", szReadBuffer);
	iCabinet = atoi(szReadBuffer);
	pSearchValue2 = pSearchValue2 + iPosition + 1;
    }
    else
    {
	return FALSE;
    }

    pSearchValue1 = strchr(pSearchValue2, 59);
    if(pSearchValue1 != NULL)
    {
	TRACE_WEB_USER_NOT_CYCLE("pSearchValue1 is %s\n", pSearchValue1);
	iPosition = pSearchValue1 - pSearchValue2;
	TRACE_WEB_USER_NOT_CYCLE("iPosition is %d\n", iPosition);
	strncpyz(szReadBuffer, pSearchValue2, iPosition + 1);
	TRACE_WEB_USER_NOT_CYCLE("szReadBuffer is %s\n", szReadBuffer);
	iDU = atoi(szReadBuffer);
	pSearchValue2 = pSearchValue2 + iPosition + 1;
    }
    else
    {
	return FALSE;
    }

    pSearchValue1 = strchr(pSearchValue2, 59);
    if(pSearchValue1 != NULL)
    {
	TRACE_WEB_USER_NOT_CYCLE("pSearchValue1 is %s\n", pSearchValue1);
	iPosition = pSearchValue1 - pSearchValue2;
	TRACE_WEB_USER_NOT_CYCLE("iPosition is %d\n", iPosition);
	strncpyz(szReadBuffer, pSearchValue2, iPosition + 1);
	TRACE_WEB_USER_NOT_CYCLE("szReadBuffer is %s\n", szReadBuffer);
	iBranch = atoi(szReadBuffer);
	pSearchValue2 = pSearchValue2 + iPosition + 1;
    }
    else
    {
	return FALSE;
    }

    
    iFileLen += snprintf(string + iFileLen, (100 - iFileLen), "[");
    if((gs_pWebCabinetInfo[iCabinet - 1].stWebDU[iDU -1].pSignalInfo[iBranch - 1].bBranchBind != 1) ||
	(gs_pWebCabinetInfo[iCabinet - 1].stWebDU[iDU -1].pSignalInfo[iBranch - 1].bBranchConfig == 1))
    {
	iFileLen += snprintf(string + iFileLen, (100 - iFileLen), "]");
	return 9;
    }
    for(i = 0; i < 4; i++)
    {
	if(gs_pWebCabinetInfo[iCabinet - 1].stWebDU[iDU -1].pSignalInfo[iBranch - 1].stBranch[i].bValid == TRUE)
	{
	    iFlag = 1;
	    gs_pWebCabinetInfo[iCabinet - 1].stWebDU[iDU -1].pSignalInfo[iBranch - 1].stBranch[i].bValid = 0;
	    iCabinet1 = gs_pWebCabinetInfo[iCabinet - 1].stWebDU[iDU -1].pSignalInfo[iBranch - 1].stBranch[i].iCabinetID;
	    iDU1 = gs_pWebCabinetInfo[iCabinet - 1].stWebDU[iDU -1].pSignalInfo[iBranch - 1].stBranch[i].iDUID;
	    iBranch1 = gs_pWebCabinetInfo[iCabinet - 1].stWebDU[iDU -1].pSignalInfo[iBranch - 1].stBranch[i].iBranchID;
	    iFileLen += snprintf(string + iFileLen, (100 - iFileLen), "\"DU%d.%d.%d\",", iCabinet1, iDU1, iBranch1);
	    gs_pWebCabinetInfo[iCabinet1 - 1].stWebDU[iDU1 -1].pSignalInfo[iBranch1 - 1].bBranchBind = 0;
	}
    }
    gs_pWebCabinetInfo[iCabinet - 1].stWebDU[iDU -1].pSignalInfo[iBranch - 1].bBranchBind = 0;

    if(iFlag == 1)
    {
	iFileLen = iFileLen - 1;
    }
    iFileLen += snprintf(string + iFileLen, (100 - iFileLen), "]");
    if(UpdateBranchInfoToFlash() != TRUE)
    {
	return FALSE;
    }
    return TRUE;
}

/*==========================================================================*
* FUNCTION :  Web_SetCBPara
* PURPOSE  :  Set the branch data(the rating current), then update to flash.
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  ptr: the string including the data
	      string: the return string including the data
* RETURN   :  
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2014-05-13
* MODIFY   :  
*==========================================================================*/
static int Web_SetCBPara(char *ptr, char* string)
{
    char *pSearchValue1, *pSearchValue2, *pSearchValue3, *pReadptr;
    int	iPosition = 0, iPosition1 = 0;
    char szReadBuffer[50];
    char szReadBuffer1[10];
    int iCabinet, iDU, iBranch;
    int iCabinet1, iDU1, iBranch1;
    int iBranchNum = 0;
    int iFileLen = 0;
    int iFlag = 0;

    TRACE_WEB_USER_NOT_CYCLE("Web_SetCBPara ptr is %s\n", ptr);
    pSearchValue2 = ptr;
    pSearchValue1 = strchr(pSearchValue2, 59);
    if(pSearchValue1 != NULL)
    {
	TRACE_WEB_USER_NOT_CYCLE("pSearchValue1 is %s\n", pSearchValue1);
	iPosition = pSearchValue1 - pSearchValue2;
	TRACE_WEB_USER_NOT_CYCLE("iPosition is %d\n", iPosition);
	strncpyz(szReadBuffer, pSearchValue2, iPosition + 1);
	TRACE_WEB_USER_NOT_CYCLE("szReadBuffer is %s\n", szReadBuffer);
	iCabinet = atoi(szReadBuffer);
	pSearchValue2 = pSearchValue2 + iPosition + 1;
    }
    else
    {
	return FALSE;
    }

    pSearchValue1 = strchr(pSearchValue2, 59);
    if(pSearchValue1 != NULL)
    {
	TRACE_WEB_USER_NOT_CYCLE("pSearchValue1 is %s\n", pSearchValue1);
	iPosition = pSearchValue1 - pSearchValue2;
	TRACE_WEB_USER_NOT_CYCLE("iPosition is %d\n", iPosition);
	strncpyz(szReadBuffer, pSearchValue2, iPosition + 1);
	TRACE_WEB_USER_NOT_CYCLE("szReadBuffer is %s\n", szReadBuffer);
	iDU = atoi(szReadBuffer);
	pSearchValue2 = pSearchValue2 + iPosition + 1;
    }
    else
    {
	return FALSE;
    }

    pSearchValue1 = strchr(pSearchValue2, 59);
    if(pSearchValue1 != NULL)
    {
	TRACE_WEB_USER_NOT_CYCLE("pSearchValue1 is %s\n", pSearchValue1);
	iPosition = pSearchValue1 - pSearchValue2;
	TRACE_WEB_USER_NOT_CYCLE("iPosition is %d\n", iPosition);
	strncpyz(szReadBuffer, pSearchValue2, iPosition + 1);
	TRACE_WEB_USER_NOT_CYCLE("szReadBuffer is %s\n", szReadBuffer);
	iBranch = atoi(szReadBuffer);
	pSearchValue2 = pSearchValue2 + iPosition + 1;
    }
    else
    {
	return FALSE;
    }

    pSearchValue1 = strchr(pSearchValue2, 59);
    if(pSearchValue1 != NULL)
    {
	TRACE_WEB_USER_NOT_CYCLE("pSearchValue1 is %s\n", pSearchValue1);
	iPosition = pSearchValue1 - pSearchValue2;
	TRACE_WEB_USER_NOT_CYCLE("iPosition is %d\n", iPosition);
	strncpyz(szReadBuffer, pSearchValue2, iPosition + 1);
	TRACE_WEB_USER_NOT_CYCLE("szReadBuffer is %s\n", szReadBuffer);
	iFileLen += snprintf(string + iFileLen, (100 - iFileLen), "\"%s\",", szReadBuffer);
	gs_pWebCabinetInfo[iCabinet - 1].stWebDU[iDU - 1].pSignalInfo[iBranch - 1].fRatingCurrent = atof(szReadBuffer);
	gs_pWebCabinetInfo[iCabinet - 1].stWebDU[iDU - 1].pSignalInfo[iBranch - 1].bRatingSet = 1;
	pSearchValue2 = pSearchValue2 + iPosition + 1;
    }
    else
    {
	return FALSE;
    }

    pSearchValue1 = strchr(pSearchValue2, 59);
    if(pSearchValue1 != NULL)
    {
	TRACE_WEB_USER_NOT_CYCLE("pSearchValue1 is %s\n", pSearchValue1);
	iPosition = pSearchValue1 - pSearchValue2;
	TRACE_WEB_USER_NOT_CYCLE("iPosition is %d\n", iPosition);
	strncpyz(szReadBuffer, pSearchValue2, iPosition + 1);
	TRACE_WEB_USER_NOT_CYCLE("szReadBuffer is %s\n", szReadBuffer);
	iFileLen += snprintf(string + iFileLen, (100 - iFileLen), "\"%s\",[", szReadBuffer);
	gs_pWebCabinetInfo[iCabinet - 1].stWebDU[iDU - 1].pSignalInfo[iBranch - 1].fRatingPower = atof(szReadBuffer);
	gs_pWebCabinetInfo[iCabinet - 1].stWebDU[iDU - 1].pSignalInfo[iBranch - 1].bRatingPowerSet = 1;
	pSearchValue2 = pSearchValue2 + iPosition + 1;
    }
    else
    {
	return FALSE;
    }

    TRACE_WEB_USER_NOT_CYCLE("Web_SetCBPara pSearchValue2 is %s\n", pSearchValue2);
    pSearchValue1 = strchr(pSearchValue2, 59);
    while(pSearchValue1 != NULL)
    {
	TRACE_WEB_USER_NOT_CYCLE("pSearchValue1 is %s\n", pSearchValue1);
	iPosition = pSearchValue1 - pSearchValue2;
	TRACE_WEB_USER_NOT_CYCLE("iPosition is %d\n", iPosition);
	if((iPosition < 50) && (iPosition > 0))
	{
	    if(gs_pWebCabinetInfo[iCabinet - 1].stWebDU[iDU - 1].pSignalInfo[iBranch - 1].bBranchBind != 0)
	    {
		if(iFlag == 1)
		{
		    iFileLen = iFileLen - 1;
		}
		iFileLen += snprintf(string + iFileLen, (100 - iFileLen), "]");
		return 8;
	    }

	    strncpyz(szReadBuffer, pSearchValue2, iPosition + 2);
	    TRACE_WEB_USER_NOT_CYCLE("szReadBuffer is %s\n", szReadBuffer);
	    pReadptr = szReadBuffer;
	    pSearchValue3 = strchr(pReadptr, ',');
	    if(pSearchValue3 != NULL)
	    {
		TRACE_WEB_USER_NOT_CYCLE("pSearchValue3 is %s\n", pSearchValue3);
		iPosition1 = pSearchValue3 - pReadptr;
		if(iPosition1 < 10)
		{
		    TRACE_WEB_USER_NOT_CYCLE("iPosition1 is %d\n", iPosition1);
		    strncpyz(szReadBuffer1, pReadptr, iPosition1 + 1);
		    TRACE_WEB_USER_NOT_CYCLE("szReadBuffer1 is %s\n", szReadBuffer1);
		    iCabinet1 = atoi(szReadBuffer1);
		    if(iCabinet1 < 1)
		    {
			iCabinet1 = 1;
		    }
		    else if( iCabinet1 > WEB_MAX_CABINET_NUM )//Fengel modify 
		    {
			    return FALSE;	
		    }
		    else
		    {
		    }
		    gs_pWebCabinetInfo[iCabinet - 1].stWebDU[iDU - 1].pSignalInfo[iBranch - 1].stBranch[iBranchNum].iCabinetID = iCabinet1;
		    memset(szReadBuffer1, 0, sizeof(szReadBuffer1));
		}
		else
		{
		    return FALSE;
		}
		pReadptr = pReadptr + iPosition1 + 1;
	    }
	    else
	    {
		  return FALSE;//Fengel modify
	    }

	    pSearchValue3 = strchr(pReadptr, ',');
	    if(pSearchValue3 != NULL)
	    {
		TRACE_WEB_USER_NOT_CYCLE("pSearchValue3 is %s\n", pSearchValue3);
		iPosition1 = pSearchValue3 - pReadptr;
		if(iPosition1 < 10)
		{
		    TRACE_WEB_USER_NOT_CYCLE("iPosition1 is %d\n", iPosition1);
		    strncpyz(szReadBuffer1, pReadptr, iPosition1 + 1);
		    TRACE_WEB_USER_NOT_CYCLE("szReadBuffer1 is %s\n", szReadBuffer1);
		    iDU1 = atoi(szReadBuffer1);
		    if(iDU1 < 1)
		    {
			iDU1 = 1;
		    }
		    else if( iDU1 > gs_pWebCabinetInfo[iCabinet1-1].iNumber  ) //Fengel modify 
		    {
			    return FALSE;	
		    }
		    else
		    {
		    }
		    gs_pWebCabinetInfo[iCabinet - 1].stWebDU[iDU - 1].pSignalInfo[iBranch - 1].stBranch[iBranchNum].iDUID = iDU1;
		    memset(szReadBuffer1, 0, sizeof(szReadBuffer1));
		}
		else
		{
		    return FALSE;
		}
		pReadptr = pReadptr + iPosition1 + 1;
	    }
	    else
	    {
		  return FALSE;//Fengel modify
	    }


	    pSearchValue3 = strchr(pReadptr, ';');
	    if(pSearchValue3 != NULL)
	    {
		TRACE_WEB_USER_NOT_CYCLE("pSearchValue3 is %s\n", pSearchValue3);
		iPosition1 = pSearchValue3 - pReadptr;
		if(iPosition1 < 10)
		{
		    TRACE_WEB_USER_NOT_CYCLE("iPosition1 is %d\n", iPosition1);
		    strncpyz(szReadBuffer1, pReadptr, iPosition1 + 1);
		    TRACE_WEB_USER_NOT_CYCLE("szReadBuffer1 is %s\n", szReadBuffer1);
		    iBranch1 = atoi(szReadBuffer1);
		    if(iBranch1 < 1)
		    {
			iBranch1 = 1;
		    }
		     else if ( iBranch1 > gs_pWebCabinetInfo[iCabinet1-1].stWebDU[iDU-1].iSignalNum )//Fengel modify
		    {
			    return FALSE;	
		    }
		    else
		    {
		    }
		    
		    gs_pWebCabinetInfo[iCabinet - 1].stWebDU[iDU - 1].pSignalInfo[iBranch - 1].stBranch[iBranchNum].iBranchID = iBranch1;
		    memset(szReadBuffer1, 0, sizeof(szReadBuffer1));
		}
		else
		{
		    return FALSE;
		}
	    }
	    else
	    {
		  return FALSE;//Fengel modify
	    }


	    if((gs_pWebCabinetInfo[iCabinet1 - 1].stWebDU[iDU1 - 1].pSignalInfo[iBranch1 - 1].bBranchConfig == 0) &&//not configured, not binded
		(gs_pWebCabinetInfo[iCabinet1 - 1].stWebDU[iDU1 - 1].pSignalInfo[iBranch1 - 1].bBranchBind == 0))
	    {
		gs_pWebCabinetInfo[iCabinet1 - 1].stWebDU[iDU1 - 1].pSignalInfo[iBranch1 - 1].bBranchBind = 2;//is binded
	    }
	    else
	    {
		if(iFlag == 1)
		{
		    iFileLen = iFileLen - 1;
		}
		iFileLen += snprintf(string + iFileLen, (100 - iFileLen), "]");
		if(gs_pWebCabinetInfo[iCabinet1 - 1].stWebDU[iDU1 - 1].pSignalInfo[iBranch1 - 1].bBranchConfig == 1)
		{
		    return 4;
		}
		else if(gs_pWebCabinetInfo[iCabinet1 - 1].stWebDU[iDU1 - 1].pSignalInfo[iBranch1 - 1].bBranchBind == 2)
		{
		    return 5;
		}
		else if(gs_pWebCabinetInfo[iCabinet1 - 1].stWebDU[iDU1 - 1].pSignalInfo[iBranch1 - 1].bBranchBind == 1)
		{
		    return 6;
		}
	    }
	    gs_pWebCabinetInfo[iCabinet - 1].stWebDU[iDU - 1].pSignalInfo[iBranch - 1].stBranch[iBranchNum].bValid = TRUE;
	    TRACE_WEB_USER_NOT_CYCLE("iCabinetID is %d iDUID is %d iBranchID is %d\n", iCabinet1, iDU1, iBranch1);
	    iFlag = 1;
	    iFileLen += snprintf(string + iFileLen, (100 - iFileLen), "\"DU%d.%d.%d\",", iCabinet1, iDU1, iBranch1);
	    iBranchNum++;
	    if(iBranchNum > 4)
	    {
		return FALSE;
	    }
	}
	else if(iPosition == 0)
	{
	    if(iFlag == 1)
	    {
		iFileLen = iFileLen - 1;
	    }
	    iFileLen += snprintf(string + iFileLen, (100 - iFileLen), "]");
	    if(UpdateBranchInfoToFlash() != TRUE)
	    {
		return FALSE;
	    }
	    return TRUE;
	}
	else
	{
	    return FALSE;
	}
	pSearchValue2 = pSearchValue2 + iPosition + 1;
	TRACE_WEB_USER_NOT_CYCLE("pSearchValue2 is %s\n", pSearchValue2);
	pSearchValue1 = strchr(pSearchValue2, 59);
    }
    if(iFlag == 1)
    {
	iFileLen = iFileLen - 1;
    }
    iFileLen += snprintf(string + iFileLen, (100 - iFileLen), "]");
    gs_pWebCabinetInfo[iCabinet - 1].stWebDU[iDU - 1].pSignalInfo[iBranch - 1].bBranchBind = 1;//bind
    if(UpdateBranchInfoToFlash() != TRUE)
    {
	return FALSE;
    }
    return TRUE;
}

/*==========================================================================*
* FUNCTION :  Web_SetCabinetPara
* PURPOSE  :  Set the cabinet data(include the name, alarm level1 and alarm level2), then update to flash.
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  iCabinetNum: the cabinet number
	      ptr: the string including the data
	      string: the return string including the data
* RETURN   :  
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2014-05-13
* MODIFY   :  
*==========================================================================*/
static int Web_SetCabinetPara(int iCabinetNum, char *ptr, char *string)
{
    CABINET_INFO *pstCabInfo;
    char *pSearchValue1, *pSearchValue2;
    int	iPosition = 0;
    char szReadBuffer[64];
    int iLen = 0;

    TRACE_WEB_USER_NOT_CYCLE("Web_SetCabinetPara ptr is %s\n", ptr);
    if((iCabinetNum > gs_stConsumMapInfo.iNumber) || (iCabinetNum < 1))
    {
	return FALSE;
    }

    pstCabInfo = &(gs_stConsumMapInfo.pstCabInfo[iCabinetNum - 1]);
    pSearchValue2 = ptr;
    pSearchValue1 = strchr(pSearchValue2, 59);
    if(pSearchValue1 != NULL)
    {
	TRACE_WEB_USER_NOT_CYCLE("pSearchValue1 is %s\n", pSearchValue1);
	iPosition = pSearchValue1 - pSearchValue2;
	TRACE_WEB_USER_NOT_CYCLE("iPosition is %d\n", iPosition);
	if(iPosition < 64)
	{
	    strncpyz(szReadBuffer, pSearchValue2, iPosition + 1);
	    TRACE_WEB_USER_NOT_CYCLE("szReadBuffer is %s\n", szReadBuffer);
	    memset(pstCabInfo->cName, 0, 64);
	    snprintf(pstCabInfo->cName, 64, szReadBuffer);
	    iLen += snprintf(string + iLen, (100 - iLen), "\"%s\",", szReadBuffer);
	}
	pSearchValue2 = pSearchValue2 + iPosition + 1;
    }
    else
    {
	return FALSE;
    }

    pSearchValue1 = strchr(pSearchValue2, 59);
    if(pSearchValue1 != NULL)
    {
	TRACE_WEB_USER_NOT_CYCLE("pSearchValue1 is %s\n", pSearchValue1);
	iPosition = pSearchValue1 - pSearchValue2;
	TRACE_WEB_USER_NOT_CYCLE("iPosition is %d\n", iPosition);
	strncpyz(szReadBuffer, pSearchValue2, iPosition + 1);
	TRACE_WEB_USER_NOT_CYCLE("szReadBuffer is %s\n", szReadBuffer);
	pstCabInfo->fAlarmLevel1 = atof(szReadBuffer);
	iLen += snprintf(string + iLen, (100 - iLen), "\"%s\",", szReadBuffer);
	pSearchValue2 = pSearchValue2 + iPosition + 1;
    }
    else
    {
	return FALSE;
    }

    pSearchValue1 = strchr(pSearchValue2, 59);
    if(pSearchValue1 != NULL)
    {
	TRACE_WEB_USER_NOT_CYCLE("pSearchValue1 is %s\n", pSearchValue1);
	iPosition = pSearchValue1 - pSearchValue2;
	TRACE_WEB_USER_NOT_CYCLE("iPosition is %d\n", iPosition);
	strncpyz(szReadBuffer, pSearchValue2, iPosition + 1);
	TRACE_WEB_USER_NOT_CYCLE("szReadBuffer is %s\n", szReadBuffer);
	pstCabInfo->fAlarmLevel2 = atof(szReadBuffer);
	iLen += snprintf(string + iLen, (100 - iLen), "\"%s\",", szReadBuffer);
	pSearchValue2 = pSearchValue2 + iPosition + 1;
    }
    else
    {
	return FALSE;
    }

    pSearchValue1 = strchr(pSearchValue2, 59);
    if(pSearchValue1 != NULL)
    {
	TRACE_WEB_USER_NOT_CYCLE("pSearchValue1 is %s\n", pSearchValue1);
	iPosition = pSearchValue1 - pSearchValue2;
	TRACE_WEB_USER_NOT_CYCLE("iPosition is %d\n", iPosition);
	strncpyz(szReadBuffer, pSearchValue2, iPosition + 1);
	TRACE_WEB_USER_NOT_CYCLE("szReadBuffer is %s\n", szReadBuffer);
	pstCabInfo->fAlarmPower = atof(szReadBuffer);
	iLen += snprintf(string + iLen, (100 - iLen), "\"%s\"", szReadBuffer);
    }
    else
    {
	return FALSE;
    }
    if(UpdateConsumptionMapInfoToFlash() != TRUE)
    {
	return FALSE;
    }
    return TRUE;
}

/*==========================================================================*
* FUNCTION :  Web_SetCabinetData
* PURPOSE  :
* CALLS    :
* CALLED BY:
* ARGUMENTS:
* RETURN   :
* COMMENTS :
* CREATOR  : Zhao Zicheng               DATE: 2013-04-22
*==========================================================================*/
static int Web_SetCabinetData(int iCabinetNum, char *ptr)
{
    char *pSearchValue1, *pSearchValue2, *pSearchValue3, *pReadptr;
    int	iPosition = 0;
    char szReadBuffer[50];
    char szReadBuffer1[10];
    int iBranchNum = 0;
    int	iPosition1 = 0;
    BRANCH_INFO *pstBranch;

    TRACE_WEB_USER_NOT_CYCLE("Web_SetCabinetData ptr is %s\n", ptr);
    if((iCabinetNum > gs_stConsumMapInfo.iNumber) || (iCabinetNum < 1))
    {
	return FALSE;
    }
    pstBranch = &(gs_stConsumMapInfo.pstCabInfo[iCabinetNum - 1].stBranch[0]);
    pSearchValue2 = ptr;
    TRACE_WEB_USER_NOT_CYCLE("Web_SetCabinetData pSearchValue2 is %s\n", pSearchValue2);
    pSearchValue1 = strchr(pSearchValue2, 59);
    while(pSearchValue1 != NULL)
    {
	TRACE_WEB_USER_NOT_CYCLE("pSearchValue1 is %s\n", pSearchValue1);
	iPosition = pSearchValue1 - pSearchValue2;
	TRACE_WEB_USER_NOT_CYCLE("iPosition is %d\n", iPosition);
	if(iPosition < 50)
	{
	    strncpyz(szReadBuffer, pSearchValue2, iPosition + 2);
	    TRACE_WEB_USER_NOT_CYCLE("szReadBuffer is %s\n", szReadBuffer);
	    pReadptr = szReadBuffer;
	    pSearchValue3 = strchr(pReadptr, ',');
	    if(pSearchValue3 != NULL)
	    {
		TRACE_WEB_USER_NOT_CYCLE("pSearchValue3 is %s\n", pSearchValue3);
		iPosition1 = pSearchValue3 - pReadptr;
		if(iPosition1 < 10)
		{
		    TRACE_WEB_USER_NOT_CYCLE("iPosition1 is %d\n", iPosition1);
		    strncpyz(szReadBuffer1, pReadptr, iPosition1 + 1);
		    TRACE_WEB_USER_NOT_CYCLE("szReadBuffer1 is %s\n", szReadBuffer1);
		    pstBranch->iEquipID = atoi(szReadBuffer1);
		    memset(szReadBuffer1, 0, sizeof(szReadBuffer1));
		}
		else
		{
		    return FALSE;
		}
		pReadptr = pReadptr + iPosition1 + 1;
	    }

	    pSearchValue3 = strchr(pReadptr, ',');
	    if(pSearchValue3 != NULL)
	    {
		TRACE_WEB_USER_NOT_CYCLE("pSearchValue3 is %s\n", pSearchValue3);
		iPosition1 = pSearchValue3 - pReadptr;
		if(iPosition1 < 10)
		{
		    TRACE_WEB_USER_NOT_CYCLE("iPosition1 is %d\n", iPosition1);
		    strncpyz(szReadBuffer1, pReadptr, iPosition1 + 1);
		    TRACE_WEB_USER_NOT_CYCLE("szReadBuffer1 is %s\n", szReadBuffer1);
		    pstBranch->iSignalID = atoi(szReadBuffer1);
		    memset(szReadBuffer1, 0, sizeof(szReadBuffer1));
		}
		else
		{
		    return FALSE;
		}
		pReadptr = pReadptr + iPosition1 + 1;
	    }

	    pSearchValue3 = strchr(pReadptr, ',');
	    if(pSearchValue3 != NULL)
	    {
		TRACE_WEB_USER_NOT_CYCLE("pSearchValue3 is %s\n", pSearchValue3);
		iPosition1 = pSearchValue3 - pReadptr;
		if(iPosition1 < 10)
		{
		    TRACE_WEB_USER_NOT_CYCLE("iPosition1 is %d\n", iPosition1);
		    strncpyz(szReadBuffer1, pReadptr, iPosition1 + 1);
		    TRACE_WEB_USER_NOT_CYCLE("szReadBuffer1 is %s\n", szReadBuffer1);
		    pstBranch->iCabinetID = atoi(szReadBuffer1);
		    if(pstBranch->iCabinetID < 1)
		    {
			pstBranch->iCabinetID = 1;
		    }
		    memset(szReadBuffer1, 0, sizeof(szReadBuffer1));
		}
		else
		{
		    return FALSE;
		}
		pReadptr = pReadptr + iPosition1 + 1;
	    }

	    pSearchValue3 = strchr(pReadptr, ',');
	    if(pSearchValue3 != NULL)
	    {
		TRACE_WEB_USER_NOT_CYCLE("pSearchValue3 is %s\n", pSearchValue3);
		iPosition1 = pSearchValue3 - pReadptr;
		if(iPosition1 < 10)
		{
		    TRACE_WEB_USER_NOT_CYCLE("iPosition1 is %d\n", iPosition1);
		    strncpyz(szReadBuffer1, pReadptr, iPosition1 + 1);
		    TRACE_WEB_USER_NOT_CYCLE("szReadBuffer1 is %s\n", szReadBuffer1);
		    pstBranch->iDUID = atoi(szReadBuffer1);
		    if(pstBranch->iDUID < 1)
		    {
			pstBranch->iDUID = 1;
		    }
		    memset(szReadBuffer1, 0, sizeof(szReadBuffer1));
		}
		else
		{
		    return FALSE;
		}
		pReadptr = pReadptr + iPosition1 + 1;
	    }

	    pSearchValue3 = strchr(pReadptr, ';');
	    if(pSearchValue3 != NULL)
	    {
		TRACE_WEB_USER_NOT_CYCLE("pSearchValue3 is %s\n", pSearchValue3);
		iPosition1 = pSearchValue3 - pReadptr;
		if(iPosition1 < 10)
		{
		    TRACE_WEB_USER_NOT_CYCLE("iPosition1 is %d\n", iPosition1);
		    strncpyz(szReadBuffer1, pReadptr, iPosition1 + 1);
		    TRACE_WEB_USER_NOT_CYCLE("szReadBuffer1 is %s\n", szReadBuffer1);
		    pstBranch->iBranchID = atoi(szReadBuffer1);
		    if(pstBranch->iBranchID < 1)
		    {
			pstBranch->iBranchID = 1;
		    }
		    memset(szReadBuffer1, 0, sizeof(szReadBuffer1));
		}
		else
		{
		    return FALSE;
		}
	    }
	    pstBranch->bValid = 1;
	    gs_pWebCabinetInfo[pstBranch->iCabinetID - 1].stWebDU[pstBranch->iDUID - 1].pSignalInfo[pstBranch->iBranchID - 1].bBranchConfig = TRUE;//the branch has been configured
	    TRACE_WEB_USER_NOT_CYCLE("iEquipID is %d iSignalID is %d iCabinetID is %d iDUID is %d iBranchID is %d\n", pstBranch->iEquipID,
		pstBranch->iSignalID, pstBranch->iCabinetID, pstBranch->iDUID, pstBranch->iBranchID);
	    pstBranch++;
	    iBranchNum++;
	    if(iBranchNum > 20)
	    {
		return FALSE;
	    }

	}
	else
	{
	    return FALSE;
	}
	pSearchValue2 = pSearchValue2 + iPosition + 1;
	TRACE_WEB_USER_NOT_CYCLE("pSearchValue2 is %s\n", pSearchValue2);
	pSearchValue1 = strchr(pSearchValue2, 59);
    }
    gs_stConsumMapInfo.pstCabInfo[iCabinetNum - 1].iBranchNum = iBranchNum;
    gs_stConsumMapInfo.pstCabInfo[iCabinetNum - 1].iConfig = TRUE;//the cabinet has been configured
    if(UpdateConsumptionMapInfoToFlash() != TRUE)
    {
	return FALSE;
    }
    return TRUE;

}

/*==========================================================================*
* FUNCTION :  Web_ResetCabinetData
* PURPOSE  :  Release the designated branches in cabinet, then update the flash data
* CALLS    :
* CALLED BY:
* ARGUMENTS:iCabinetNum: the cabinet number
* RETURN   :
* COMMENTS :
* CREATOR  : Zhao Zicheng               DATE: 2013-05-04
*==========================================================================*/
static int Web_ResetCabinetData(int iCabinetNum)
{
    int i;
    int iCabinet, iDU, iBranch;
    CABINET_INFO *pstCabInfo;

    TRACE_WEB_USER_NOT_CYCLE("gs_stConsumMapInfo.iNumber is %d\n", gs_stConsumMapInfo.iNumber);
    if((iCabinetNum > gs_stConsumMapInfo.iNumber) || (iCabinetNum < 1))
    {
	return FALSE;
    }
    else
    {
	pstCabInfo = &(gs_stConsumMapInfo.pstCabInfo[iCabinetNum - 1]);
	for(i = 0; i < 20; i++)
	{
	    if(pstCabInfo->stBranch[i].bValid == TRUE)
	    {
		iCabinet = pstCabInfo->stBranch[i].iCabinetID;
		iDU = pstCabInfo->stBranch[i].iDUID;
		iBranch = pstCabInfo->stBranch[i].iBranchID;
		gs_pWebCabinetInfo[iCabinet - 1].stWebDU[iDU - 1].pSignalInfo[iBranch - 1].bBranchConfig = FALSE;//recover the branch status
	    }
	    pstCabInfo->stBranch[i].bValid = FALSE;
	}
	pstCabInfo->iConfig = FALSE;
	pstCabInfo->fAlarmLevel1 = 100;
	pstCabInfo->fAlarmLevel2 = 100;
	pstCabInfo->fPeakPowLast24H = 0;
	pstCabInfo->fPeakPowLastMonth = 0;
	pstCabInfo->fPeakPowLastWeek = 0;
	pstCabInfo->fTotalEnergy = 0;
	pstCabInfo->iBranchNum = 0;
	if(UpdateConsumptionMapInfoToFlash() != TRUE)
	{
	    return FALSE;
	}
	return TRUE;
    }
}

/*==========================================================================*
* FUNCTION :  Web_MakeCabinetDataBuffer
* PURPOSE  :
* CALLS    :
* CALLED BY:
* ARGUMENTS:
* RETURN   :
* COMMENTS :
* CREATOR  : Zhao Zicheng               DATE: 2013-04-21
*==========================================================================*/
static char *Web_MakeCabinetDataBuffer(int iCabinetNum)
{
    char	*szUserBuf = NULL;
    int i;
    int iTemp = 0;
    int iBufLength;
    BRANCH_INFO *pstBranch;

    TRACE_WEB_USER_NOT_CYCLE("gs_stConsumMapInfo.iNumber is %d\n", gs_stConsumMapInfo.iNumber);
    if((iCabinetNum > gs_stConsumMapInfo.iNumber) || (iCabinetNum < 1))
    {
	return NULL;
    }
    else
    {
	szUserBuf = NEW(char, 50 * 20);// for 20 branches
	iBufLength = 50 * 20;
	memset(szUserBuf, 0, iBufLength);
	TRACE_WEB_USER_NOT_CYCLE("Run here\n");
	//iTemp += snprintf(szUserBuf + iTemp, (iBufLength - iTemp), "[");
	for(i = 0; i < 20; i++)
	{
	    pstBranch = &(gs_stConsumMapInfo.pstCabInfo[iCabinetNum - 1].stBranch[i]);
	    if(pstBranch->bValid == 1)
	    {
		iTemp += snprintf(szUserBuf + iTemp, (iBufLength - iTemp), "\"DU%d.%d.%d\",", pstBranch->iCabinetID,
		    pstBranch->iDUID, pstBranch->iBranchID);
	    }
	}
	TRACE_WEB_USER_NOT_CYCLE("Run here 1\n");
	if(iTemp >= 1)
	{
	    iTemp = iTemp - 1;	// get rid of the last "," to meet json format
	    iTemp += snprintf(szUserBuf + iTemp, (iBufLength - iTemp), "");
	}
	else
	{
	    DELETE(szUserBuf);
	    szUserBuf = NULL;
	    return NULL;
	}
	
	//iTemp += snprintf(szUserBuf + iTemp, (iBufLength - iTemp), "]");
	return szUserBuf;
    }
}

/*==========================================================================*
* FUNCTION :  Web_MakeACUUserInfoBuffer
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
*==========================================================================*/
static char *Web_MakeACUUserInfoBuffer(void)
{
	USER_INFO_STRU		*pUserInfo = NULL;
	int					i, iLen = 0;
	int					iUserNum = 0;
	char				*szUserBuf = NULL;

	if((iUserNum = GetUserInfo(&pUserInfo)) > 0)
	{
		szUserBuf = NEW(char, iUserNum * 64);
		if(szUserBuf != NULL)
		{
			for(i = 0; i < iUserNum && pUserInfo != NULL; i++, pUserInfo++)
			{
			    if(strcmp((const char*)(pUserInfo->szUserName), "vertivadmin") != 0 && 
					strcmp((const char*)(pUserInfo->szUserName), "emersonadmin") != 0 )
			    {
				//changed by Frank Wu,8/N/15,20140527, for e-mail
				//iLen += sprintf(szUserBuf + iLen, "[%d,\"%s\",%2d],", 
				//    i, pUserInfo->szUserName,pUserInfo->byLevel);
					iLen += snprintf(szUserBuf + iLen, MAX_SPRINTF_LINE_LEN, "[%d,\"%s\",%2d,\"%s\"],", 
									i,
									pUserInfo->szUserName,
									pUserInfo->byLevel,
									pUserInfo->szLcdEnable);
			    }
			}
		}
		if(iLen > 0)
		{
#ifdef _SHOW_WEB_INFO
			//TRACE("iUserNum : %d szUserBuf : %s", iUserNum, szUserBuf);
#endif

			szUserBuf[iLen-1] = 32;
		}
		return szUserBuf;
	}
	return NULL;
}

/*==========================================================================*
* FUNCTION :  Web_MakeV6DHCPServerBuffer
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	
* RETURN   :  
* COMMENTS : 
* CREATOR  : Zhao Zicheng               DATE: 2014-08-12
*==========================================================================*/
static char *Web_MakeV6DHCPServerBuffer(void)
{
    DHCP_SERVER_NET_INFO_IPV6		szDHCPInfo = {0};
    int iBufLength = sizeof(DHCP_SERVER_NET_INFO_IPV6);
    int iLen = 0;
    char V6IP[128];

    int iError = DxiGetData(VAR_DHCP_SERVER_INFO, DHCP_SERVER_INFO_ALL, DXI_VAR_ID_IPV6,  &iBufLength,  &szDHCPInfo, 0);

    //debug begin
    TRACE_WEB_USER_NOT_CYCLE("DHCP server IP\n");
    TRACE_WEB_USER_NOT_CYCLE("%d    %d	%d  %d	%d\n", szDHCPInfo.stServerAddr.ifr6_addr.in6_u.u6_addr32[0],
	szDHCPInfo.stServerAddr.ifr6_addr.in6_u.u6_addr32[1],
	szDHCPInfo.stServerAddr.ifr6_addr.in6_u.u6_addr32[2],
	szDHCPInfo.stServerAddr.ifr6_addr.in6_u.u6_addr32[3],
	szDHCPInfo.stServerAddr.ifr6_prefixlen);
    //debug end

    char *szReturn = NEW(char, 200);
    memset(szReturn,0,200);

    //printf("DHCP Server in!!\n");
    if(iError == ERR_DXI_OK)
    {
	//printf("%d",szDHCPInfo.ulIp);
	inet_ntop(AF_INET6, (const void *)(&(szDHCPInfo.stServerAddr.ifr6_addr)), V6IP, 128);
	iLen += sprintf(szReturn + iLen, "\"%s\",", V6IP);

	//prefix
	iLen += sprintf(szReturn + iLen, "\"%d\"", szDHCPInfo.stServerAddr.ifr6_prefixlen);

	TRACE_WEB_USER_NOT_CYCLE("szReturn is %s\n", szReturn);

	//printf("%s\n",szReturn);
	return szReturn;

    }
    //printf("DHCP Server out!!\n");

    if(szReturn != NULL)
    {
	DELETE(szReturn);
	szReturn = NULL;
    }
    return NULL;

}

/*==========================================================================*
* FUNCTION :  Web_MakeDHCPServerBuffer
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
*==========================================================================*/
static char *Web_MakeDHCPServerBuffer(void)
{
	DHCP_SERVER_NET_INFO		szDHCPInfo = {0};
	int iBufLength = sizeof(DHCP_SERVER_NET_INFO);
	int iLen = 0;

	int iError = DxiGetData(VAR_DHCP_SERVER_INFO, DHCP_SERVER_INFO_ALL, 0,  &iBufLength,  &szDHCPInfo, 0); 

	char *szReturn = NEW(char, 100);
	memset(szReturn,0,100);

	//printf("DHCP Server in!!\n");
	if(iError == ERR_DXI_OK)
	{
		//printf("%d",szDHCPInfo.ulIp);

		iLen += sprintf(szReturn + iLen, "%d",szDHCPInfo.ulIp);

		//printf("%s\n",szReturn);
		return szReturn;

	}
	//printf("DHCP Server out!!\n");

	if(szReturn != NULL)
	{
	    DELETE(szReturn);
	    szReturn = NULL;
	}
	return NULL;

}
/*==========================================================================*
* FUNCTION :  Web_MakeSMSBuffer
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
*==========================================================================*/
static char *Web_MakeSMSBuffer(void)
{
	SMS_PHONE_INFO		szACUSMSInfo = {0};
	int iBufLength = sizeof(SMS_PHONE_INFO);
	int iLen = 0;

	int iError = DxiGetData(VAR_SMS_PHONE_INFO, SMS_INFO_PHONE_ALL, 0,  &iBufLength,  &szACUSMSInfo, 0);

	char *szReturn = NEW(char, 500);
	memset(szReturn,0,500);


	//printf("SMS Server in!!\n");
	if(iError == ERR_DXI_OK)
	{
		//printf("%s\n",szACUSMSInfo.szPhoneNumber1);
		//printf("%d\n",szACUSMSInfo.iAlarmLevel);


		iLen += sprintf(szReturn + iLen, "\" %s\",\" %s\",\" %s\",%d", 
			szACUSMSInfo.szPhoneNumber1 ,
			szACUSMSInfo.szPhoneNumber2 ,
			szACUSMSInfo.szPhoneNumber3 ,
			szACUSMSInfo.iAlarmLevel );

		//printf("%s\n",szReturn);


		return szReturn;

	}
	//printf("SMS Server out!!\n");

	if(szReturn != NULL)
	{
	    DELETE(szReturn);
	    szReturn = NULL;
	}
	return NULL;

}

/*==========================================================================*
* FUNCTION :  Web_MakeVPNBuffer
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
*==========================================================================*/
static char *Web_MakeVPNBuffer(void)
{
	ACU_VPN_INFO		szACUVPNInfo = {0};
	int iBufLength = sizeof(ACU_VPN_INFO);
	int iLen = 0;

	int iError = DxiGetData(VAR_VPN_INFO, VPN_INFO_ALL, 0,  &iBufLength,  &szACUVPNInfo, 0);

	char *szReturn = NEW(char, 500);
	memset(szReturn,0,500);

	//printf("VPN Server in!!\n");

	if(iError == ERR_DXI_OK)
	{

		iLen += sprintf(szReturn + iLen, "%d,%d,%d", 
			szACUVPNInfo.iVPNEnable,
			szACUVPNInfo.ulRemoteIp ,
			szACUVPNInfo.iRemotePort);

		//printf("%s\n",szReturn);


		return szReturn;

	}

	//printf("VPN Server out!!\n");

	if(szReturn != NULL)
	{
	    DELETE(szReturn);
	    szReturn = NULL;
	}
	return NULL;

}

/*==========================================================================*
* FUNCTION :  Web_MakeSMTPBuffer
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
*==========================================================================*/
static char *Web_MakeSMTPBuffer(void)
{
	ACU_SMTP_INFO		szACUSMTPInfo = {0};
	int iBufLength = sizeof(ACU_SMTP_INFO);
	int iLen = 0;

	int iError = DxiGetData(VAR_SMTP_INFO, SMTP_INFO_ALL, 0,  &iBufLength,  &szACUSMTPInfo, 0);

	char *szReturn = NEW(char, 500);

	if(iError == ERR_DXI_OK)
	{

		iLen += sprintf(szReturn + iLen, "\"%s\",\"%s\",%d,\"%s\",\"%s\",%d,%d,\"%s\" ", 
			szACUSMTPInfo.szEmailSendTo,
			szACUSMTPInfo.szEmailSeverIP ,
			szACUSMTPInfo.iEmailServerPort ,
			szACUSMTPInfo.szEmailAccount,
			szACUSMTPInfo.szEmailPasswd ,
			szACUSMTPInfo.iEmailAuthen,
			szACUSMTPInfo.iAlarmLevel,
			szACUSMTPInfo.szEmailFrom);


		return szReturn;

	}

	if(szReturn != NULL)
	{
	    DELETE(szReturn);
	    szReturn = NULL;
	}
	return NULL;

}
/*==========================================================================*
* FUNCTION :  Web_GetSetUserInfo
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN char *szGetUserInfo:
OUT char *szUserName:
OUT char *szUserPassword:
OUT char *szUserLevel:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
*==========================================================================*/
//static int Web_GetSetUserInfo(IN char *szGetUserInfo, OUT char *szUserName, 
//			      OUT char *szUserPassword, OUT char *szUserLevel)
//{
//	char		*pUserInfo = szGetUserInfo;
//	char		*pSearchValue = NULL;
//	int			iPosition = 0;
//	//char		szUserNameInfo[33], szUserPasswordInfo[33], szUserLevelInfo[33];
//
//	//TRACE("szGetUserInfo : %s\n", szGetUserInfo);
//	if(pUserInfo != NULL)
//	{
//		pSearchValue = strchr(pUserInfo, 59);
//		if(pSearchValue != NULL)
//		{
//			iPosition = pSearchValue - pUserInfo;
//			if(iPosition  > 0 )
//			{
//				strncpyz(szUserName, pUserInfo, 
//					((iPosition + 1) < 33) ? (iPosition + 1) : 33);
//				//szUserName = Cfg_RemoveWhiteSpace(szUserNameInfo);
//				////TRACE("szUserNameInfo : %s szUserName :%s\n", szUserNameInfo, szUserName);
//				pUserInfo = pUserInfo + iPosition;
//
//			}
//			else
//			{
//				return FALSE;
//			}
//			pUserInfo = pUserInfo + 1;
//		}
//		else
//		{
//			return FALSE;
//		}
//
//		pSearchValue = strchr(pUserInfo, 59);
//		if(pSearchValue != NULL)
//		{
//			iPosition = pSearchValue - pUserInfo;
//			if(iPosition  > 0)
//			{
//				strncpyz(szUserPassword, pUserInfo, 
//					((iPosition + 1) < 33) ? (iPosition + 1) : 33);
//				//szUserPassword = Cfg_RemoveWhiteSpace(szUserPasswordInfo);
//				////TRACE("szUserPasswordInfo : %s szUserPassword %s:\n", szUserPasswordInfo, szUserPassword);
//				pUserInfo = pUserInfo + iPosition;
//			}
//			else
//			{
//				return FALSE;
//			}
//			pUserInfo = pUserInfo + 1;
//		}
//		else
//		{
//			return FALSE;
//		}
//
//		pSearchValue = strchr(pUserInfo, 59);
//		if(pSearchValue != NULL)
//		{
//			iPosition = pSearchValue - pUserInfo;
//			if(iPosition  > 0)
//			{
//				strncpyz(szUserLevel, pUserInfo, 
//					((iPosition + 1) < 33) ? (iPosition + 1) : 33);
//				//szUserLevel = Cfg_RemoveWhiteSpace(szUserLevelInfo);
//				////TRACE("szUserLevelInfo : %s szUserLevel : %s\n", szUserLevelInfo, szUserLevel);
//				//pUserInfo = pUserInfo + iPosition;
//			}
//			else
//			{
//				return FALSE;
//			}
//			//pUserInfo = pUserInfo + 1;
//		}
//		else
//		{
//			return FALSE;
//		}
//	}
//	return TRUE;
//}
static int Web_GetUserAuthority(IN char *szUserName)
{
	USER_INFO_STRU		pUserInfo ;
	ASSERT(szUserName);

	if(FindUserInfo(szUserName, &pUserInfo) == ERR_SEC_OK)
	{
		return (int)pUserInfo.byLevel;
	}
	return 0;
}



//changed by Frank Wu,9/N/15,20140527, for e-mail
static int Web_GetSetUserInfo2(IN char *szGetUserInfo,
								OUT char *szUserName, 
								OUT char *szUserPassword,
								OUT char *szUserLevel,
								OUT char *szUserContactAddr)
{
	char		*pUserInfo = szGetUserInfo;
	char		*pSearchValue = NULL;
	int			iPosition = 0;
	//char		szUserNameInfo[33], szUserPasswordInfo[33], szUserLevelInfo[33];

	TRACE("szGetUserInfo : %s\n", szGetUserInfo);
	if(pUserInfo != NULL)
	{
		//szUserName
		pSearchValue = strchr(pUserInfo, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - pUserInfo;
			if(iPosition  > 0 )
			{
				strncpyz(szUserName, pUserInfo, 
					((iPosition + 1) < 33) ? (iPosition + 1) : 33);
				//szUserName = Cfg_RemoveWhiteSpace(szUserNameInfo);
				////TRACE("szUserNameInfo : %s szUserName :%s\n", szUserNameInfo, szUserName);
				pUserInfo = pUserInfo + iPosition;

			}
			else
			{
				return FALSE;
			}
			pUserInfo = pUserInfo + 1;
		}
		else
		{
			return FALSE;
		}
		//szUserPassword
		pSearchValue = strchr(pUserInfo, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - pUserInfo;
			if(iPosition  > 0)
			{
				strncpyz(szUserPassword, pUserInfo, 
					((iPosition + 1) < 33) ? (iPosition + 1) : 33);
				//szUserPassword = Cfg_RemoveWhiteSpace(szUserPasswordInfo);
				////TRACE("szUserPasswordInfo : %s szUserPassword %s:\n", szUserPasswordInfo, szUserPassword);
				pUserInfo = pUserInfo + iPosition;
			}
			else
			{
				return FALSE;
			}
			pUserInfo = pUserInfo + 1;
		}
		else
		{
			return FALSE;
		}
		//szUserLevel
		pSearchValue = strchr(pUserInfo, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - pUserInfo;
			if(iPosition  > 0)
			{
				strncpyz(szUserLevel, pUserInfo, 
					((iPosition + 1) < 33) ? (iPosition + 1) : 33);
				//szUserLevel = Cfg_RemoveWhiteSpace(szUserLevelInfo);
				////TRACE("szUserLevelInfo : %s szUserLevel : %s\n", szUserLevelInfo, szUserLevel);
				pUserInfo = pUserInfo + iPosition;
			}
			else
			{
				return FALSE;
			}
			pUserInfo = pUserInfo + 1;
		}
		else
		{
			return FALSE;
		}
		//szUserContactAddr
		pSearchValue = strchr(pUserInfo, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - pUserInfo;
			if(iPosition  > 0)
			{
				strncpyz(szUserContactAddr, pUserInfo, 
					((iPosition + 1) < 128) ? (iPosition + 1) : 128);
				//szUserPassword = Cfg_RemoveWhiteSpace(szUserPasswordInfo);
				////TRACE("szUserPasswordInfo : %s szUserPassword %s:\n", szUserPasswordInfo, szUserPassword);
				pUserInfo = pUserInfo + iPosition;
			}
			else
			{
				return FALSE;
			}
			pUserInfo = pUserInfo + 1;
		}
		else
		{
			return FALSE;
		}
	}
	TRACE("szUserName=%s,szUserPassword=%s,szUserLevel=%s,szUserContactAddr=%s\n",
		szUserName,
		szUserPassword,
		szUserLevel,
		szUserContactAddr);

	return TRUE;
}


/*==========================================================================*
* FUNCTION :  Web_MakeMODBUSPrivateConfigreBuffer
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	
* RETURN   :  
* COMMENTS : 
* CREATOR  : WangJing               DATE: 2006-7-31 11:20
*==========================================================================*/

static char *Web_MakeMODBUSPrivateConfigreBuffer(void)
{
	MODBUS_COMMON_CONFIG	stCommonConfig = {0};
	APP_SERVICE		*stAppService = NULL;
	int				iLen = 0;
	char			*szReturn = NULL;
	int				iReturn = 0;
	stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_MODBUS_NAME);
	stCommonConfig.iProtocolType = 5;//Modbus
	if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
	{

		iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
			stAppService->bServiceRunning,
			&stAppService->args, 
			0x0,
			0,
			(void *)&stCommonConfig);


		szReturn = NEW(char, 650);

		if(szReturn != NULL)
		{
			//TRACE("stAppService ---- 4\n");
			//iLen += sprintf(szReturn + iLen, "%2d", iReturn);
			if(iReturn == ERR_SERVICE_CFG_OK)
			{
				iLen += sprintf(szReturn + iLen, "%d,%d,", 
					stCommonConfig.iProtocolType,
					stCommonConfig.iMediaType);

				iLen += sprintf(szReturn + iLen, "\"%s\",",	stCommonConfig.szCommPortParam);
				iLen += sprintf(szReturn + iLen, "%d",	stCommonConfig.byADR);

				return szReturn;

			}
		}

	}
	TRACE("return NULL\n");
	return NULL;
}

/*==========================================================================*
* FUNCTION :  Web_MakeYDNPrivateConfigreBuffer
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	
* RETURN   :  
* COMMENTS : 
* CREATOR  : WangJing               DATE: 2006-7-31 11:20
*==========================================================================*/

static char *Web_MakeYDNPrivateConfigreBuffer(void)
{
	YDN_COMMON_CONFIG	stCommonConfig;
	APP_SERVICE		*stAppService = NULL;
	int				iLen = 0;
	char			*szReturn = NULL;
	int				iReturn = 0;
	stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_YDN_NAME);
	stCommonConfig.iProtocolType = YDN23;
	if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
	{

		iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
			stAppService->bServiceRunning,
			&stAppService->args, 
			0x0,
			0,
			(void *)&stCommonConfig);


		szReturn = NEW(char, 650);

		if(szReturn != NULL)
		{
			//TRACE("stAppService ---- 4\n");
			//iLen += sprintf(szReturn + iLen, "%2d", iReturn);
			if(iReturn == ERR_SERVICE_CFG_OK)
			{
				iLen += sprintf(szReturn + iLen, "%d,%d,%d,%d,%d,", 
					stCommonConfig.iProtocolType,
					stCommonConfig.iMediaType,
					stCommonConfig.iAttemptElapse/1000,
					stCommonConfig.iMaxAttempts,
					stCommonConfig.bReportInUse);
				iLen += sprintf(szReturn + iLen, "\"%s\",", stCommonConfig.szAlarmReportPhoneNumber[0]);
				iLen += sprintf(szReturn + iLen, "\"%s\",", stCommonConfig.szAlarmReportPhoneNumber[1]);
				iLen += sprintf(szReturn + iLen, "\"%s\",", stCommonConfig.szAlarmReportPhoneNumber[2]);
				iLen += sprintf(szReturn + iLen, "\"%s\",",	stCommonConfig.szCommPortParam);
				iLen += sprintf(szReturn + iLen, "%d",	stCommonConfig.byADR);

				return szReturn;


			}
		}

	}
	TRACE("return NULL\n");
	return NULL;
}

/*==========================================================================*
* FUNCTION :  Web_GetModbusModifyInfo
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN char *szBuffer:
OUT COMMON_CONFIG *stCommonConfig:
* RETURN   :  
* COMMENTS : 
* CREATOR  : WangJing               DATE: 2006-7-31 11:20
*==========================================================================*/
static int Web_GetModbusModifyInfo(IN char *szBuffer, OUT YDN_COMMON_CONFIG *stCommonConfig)
{
	ASSERT(szBuffer);
	ASSERT(stCommonConfig);

	char			*ptr = NULL;
	char			szExchange[64], *pSearchValue = NULL;
	int				iPosition = 0;
	char			*pTrim = NULL;
	int				iModifyType = MODBUS_CFG_W_MODE;

	//Aanalyze szBuffer
	int			iValdateModify = 0;
	if((ptr = szBuffer) != NULL)
	{

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					stCommonConfig->iProtocolType = atoi(szExchange) ;
					TRACE("stCommonConfig->iProtocolType :[%s][%d]\n", szExchange, stCommonConfig->iProtocolType);
					iModifyType = iModifyType | MODBUS_CFG_PROTOCOL_TYPE;
				}
				ptr = ptr + iPosition;

			}

		}
		//iModifyType = iModifyType | MODBUS_CFG_PROTOCOL_TYPE;
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					stCommonConfig->iMediaType = atoi(szExchange);
					TRACE("stCommonConfig->iMediaType :[%s][%d]\n", szExchange, stCommonConfig->iMediaType);
					iModifyType = iModifyType | MODBUS_CFG_MEDIA_TYPE;
				}
				ptr = ptr + iPosition;

			}

		}
		ptr = ptr + 1;

		
		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					pTrim = Cfg_RemoveWhiteSpace(szExchange);
					sprintf(stCommonConfig->szCommPortParam, "%s", pTrim);
					TRACE("stCommonConfig->szCommPortParam :[%s][%s]\n", szExchange, stCommonConfig->szCommPortParam);
					iModifyType = iModifyType | MODBUS_CFG_MEDIA_PORT_PARAM;
				}
				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					pTrim = Cfg_RemoveWhiteSpace(szExchange);
					stCommonConfig->byADR = atoi(pTrim);

					TRACE("stCommonConfig->byADR :[%s][%d]\n", szExchange, stCommonConfig->byADR);
					iModifyType = iModifyType | MODBUS_CFG_ADR;
				}
				ptr = ptr + iPosition;
			}

		}



		return iModifyType;
	}
	return FALSE;

}

/*==========================================================================*
* FUNCTION :  Web_GetYDNModifyInfo
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN char *szBuffer:
OUT COMMON_CONFIG *stCommonConfig:
* RETURN   :  
* COMMENTS : 
* CREATOR  : WangJing               DATE: 2006-7-31 11:20
*==========================================================================*/
static int Web_GetYDNModifyInfo(IN char *szBuffer, OUT YDN_COMMON_CONFIG *stCommonConfig)
{
	ASSERT(szBuffer);
	ASSERT(stCommonConfig);

	char			*ptr = NULL;
	char			szExchange[64], *pSearchValue = NULL;
	int				iPosition = 0;
	char			*pTrim = NULL;
	int				iModifyType = YDN_CFG_W_MODE;

	//Aanalyze szBuffer
	int			iValdateModify = 0;
	if((ptr = szBuffer) != NULL)
	{

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					stCommonConfig->iProtocolType = atoi(szExchange) ;
					TRACE("stCommonConfig->iProtocolType :[%s][%d]\n", szExchange, stCommonConfig->iProtocolType);
					iModifyType = iModifyType | YDN_CFG_PROTOCOL_TYPE;
				}
				ptr = ptr + iPosition;

			}

		}
		//iModifyType = iModifyType | YDN_CFG_PROTOCOL_TYPE;
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					stCommonConfig->iMediaType = atoi(szExchange);
					TRACE("stCommonConfig->iMediaType :[%s][%d]\n", szExchange, stCommonConfig->iMediaType);
					iModifyType = iModifyType | YDN_CFG_MEDIA_TYPE;
				}
				ptr = ptr + iPosition;

			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					stCommonConfig->iAttemptElapse = 1000 * atoi(szExchange);
					TRACE("stCommonConfig->iAttemptElapse :[%s][%d]\n", szExchange, stCommonConfig->iAttemptElapse);
					iModifyType = iModifyType | YDN_CFG_ATTEMPT_ELAPSE;
				}
				ptr = ptr + iPosition;
			}
		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					stCommonConfig->iMaxAttempts = atoi(szExchange);
					TRACE("stCommonConfig->iMaxAttempts :[%s][%d]\n", szExchange,stCommonConfig->iMaxAttempts);
					iModifyType = iModifyType | YDN_CFG_MAX_ATTEMPTS;
				}
				ptr = ptr + iPosition;
			}
		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					if(iPosition > 2)
					{
						stCommonConfig->bReportInUse = TRUE;
					}
					else
					{
						stCommonConfig->bReportInUse = FALSE;
					}
					TRACE("stCommonConfig->bReportInUse :[%s][%d]\n", szExchange, stCommonConfig->bReportInUse);
					iModifyType = iModifyType | YDN_CFG_REPORT_IN_USE;
				}
				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					pTrim = Cfg_RemoveWhiteSpace(szExchange);
					strncpyz(stCommonConfig->szAlarmReportPhoneNumber[0],
						pTrim,
						sizeof(stCommonConfig->szAlarmReportPhoneNumber[0]));

					TRACE("stCommonConfig->szAlarmReportPhoneNumber :[%s][%s]\n", szExchange,stCommonConfig->szAlarmReportPhoneNumber[0]);
					iModifyType = iModifyType | YDN_CFG_REPORT_NUMBER_1;
				}
				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					pTrim = Cfg_RemoveWhiteSpace(szExchange);
					strncpyz(stCommonConfig->szAlarmReportPhoneNumber[1],
						pTrim,
						sizeof(stCommonConfig->szAlarmReportPhoneNumber[1]));

					TRACE("stCommonConfig->szAlarmReportPhoneNumber :[%s][%s]\n", szExchange, stCommonConfig->szAlarmReportPhoneNumber[1]);
					iModifyType = iModifyType | YDN_CFG_REPORT_NUMBER_2;
				}
				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;


		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				TRACE("[%s][%d]\n", szExchange, iValdateModify);

				if(iValdateModify == 1)
				{
					pTrim = Cfg_RemoveWhiteSpace(szExchange);
					strncpyz(stCommonConfig->szAlarmReportPhoneNumber[2],
						pTrim,
						(int)sizeof(stCommonConfig->szAlarmReportPhoneNumber[2]));

					TRACE("stCommonConfig->szCallbackPhoneNumber :[%s][%s]\n", szExchange, stCommonConfig->szAlarmReportPhoneNumber[2]);
					iModifyType = iModifyType | YDN_CFG_REPORT_NUMBER_3;
				}
				ptr = ptr + iPosition;

			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					pTrim = Cfg_RemoveWhiteSpace(szExchange);
					sprintf(stCommonConfig->szCommPortParam, "%s", pTrim);
					TRACE("stCommonConfig->szCommPortParam :[%s][%s]\n", szExchange, stCommonConfig->szCommPortParam);
					iModifyType = iModifyType | YDN_CFG_MEDIA_PORT_PARAM;
				}
				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					pTrim = Cfg_RemoveWhiteSpace(szExchange);
					stCommonConfig->byADR = atoi(pTrim);

					TRACE("stCommonConfig->byADR :[%s][%d]\n", szExchange, stCommonConfig->byADR);
					iModifyType = iModifyType | YDN_CFG_ADR;
				}
				ptr = ptr + iPosition;
			}

		}



		return iModifyType;
	}
	return FALSE;

}
/*==========================================================================*
* FUNCTION :  Web_GetSMSModifyInfo
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN char *szBuffer:
OUT COMMON_CONFIG *stCommonConfig:
* RETURN   :  
* COMMENTS : 
* CREATOR  : WangJing               DATE: 2006-7-31 11:20
*==========================================================================*/
static int Web_GetSMSModifyInfo(IN char *szBuffer, OUT SMS_PHONE_INFO *stACUSMSInfo)
{
	ASSERT(szBuffer);
	ASSERT(stACUSMSInfo);

	char			*ptr = NULL;
	char			szExchange[64], *pSearchValue = NULL;
	int				iPosition = 0;
	char			*pTrim = NULL;


	//Aanalyze szBuffer
	int			iValdateModify = 0;
	if((ptr = szBuffer) != NULL)
	{

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);

				TRACE("[%s][%d]\n", szExchange, iValdateModify);

				pTrim = Cfg_RemoveWhiteSpace(szExchange);

				sprintf(stACUSMSInfo->szPhoneNumber1, "%s", pTrim);

				ptr = ptr + iPosition;

			}

		}

		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);

		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);

				TRACE("[%s][%d]\n", szExchange, iValdateModify);

				pTrim = Cfg_RemoveWhiteSpace(szExchange);

				sprintf(stACUSMSInfo->szPhoneNumber2, "%s", pTrim);


				ptr = ptr + iPosition;

			}

		}

		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);

				TRACE("[%s][%d]\n", szExchange, iValdateModify);

				pTrim = Cfg_RemoveWhiteSpace(szExchange);

				sprintf(stACUSMSInfo->szPhoneNumber3, "%s", pTrim);


				ptr = ptr + iPosition;

			}

		}

		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);

				TRACE("[%s][%d]\n", szExchange, iValdateModify);

				pTrim = Cfg_RemoveWhiteSpace(szExchange);

				stACUSMSInfo->iAlarmLevel = atoi(pTrim);

				ptr = ptr + iPosition;

			}

		}



		return TRUE;
	}
	return FALSE;

}

/*==========================================================================*
* FUNCTION :  Web_GetVPNModifyInfo
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN char *szBuffer:
OUT COMMON_CONFIG *stCommonConfig:
* RETURN   :  
* COMMENTS : 
* CREATOR  : WangJing               DATE: 2006-7-31 11:20
*==========================================================================*/
static int Web_GetVPNModifyInfo(IN char *szBuffer, OUT ACU_VPN_INFO *stACUVPNInfo)
{
	ASSERT(szBuffer);
	ASSERT(stACUVPNInfo);

	char			*ptr = NULL;
	char			szExchange[64], *pSearchValue = NULL;
	int				iPosition = 0;
	char			*pTrim = NULL;


	//Aanalyze szBuffer
	int			iValdateModify = 0;
	if((ptr = szBuffer) != NULL)
	{

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);

				TRACE("[%s][%d]\n", szExchange, iValdateModify);

				pTrim = Cfg_RemoveWhiteSpace(szExchange);

				stACUVPNInfo->iVPNEnable = atoi(pTrim);

				ptr = ptr + iPosition;

			}

		}

		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);

		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);

				TRACE("[%s][%d]\n", szExchange, iValdateModify);

				pTrim = Cfg_RemoveWhiteSpace(szExchange);

				stACUVPNInfo->ulRemoteIp = inet_addr(pTrim);

				ptr = ptr + iPosition;

			}

		}

		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);

				TRACE("[%s][%d]\n", szExchange, iValdateModify);

				pTrim = Cfg_RemoveWhiteSpace(szExchange);

				stACUVPNInfo->iRemotePort = atoi(pTrim);

				ptr = ptr + iPosition;

			}

		}



		return TRUE;
	}
	return FALSE;

}


/*==========================================================================*
* FUNCTION :  Web_GetSMTPModifyInfo
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN char *szBuffer:
OUT COMMON_CONFIG *stCommonConfig:
* RETURN   :  
* COMMENTS : 
* CREATOR  : WangJing               DATE: 2006-7-31 11:20
*==========================================================================*/
static int Web_GetSMTPModifyInfo(IN char *szBuffer, OUT ACU_SMTP_INFO *stACUSMTPInfo)
{
	ASSERT(szBuffer);
	ASSERT(stACUSMTPInfo);

	char			*ptr = NULL;
	char			szExchange[64], *pSearchValue = NULL;
	int				iPosition = 0;
	char			*pTrim = NULL;


	//Aanalyze szBuffer
	int			iValdateModify = 0;
	if((ptr = szBuffer) != NULL)
	{

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);

				TRACE("[%s][%d]\n", szExchange, iValdateModify);

				pTrim = Cfg_RemoveWhiteSpace(szExchange);

				strncpyz(stACUSMTPInfo->szEmailSendTo,
					pTrim,
					sizeof(stACUSMTPInfo->szEmailSendTo));

				ptr = ptr + iPosition;

			}

		}

		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);

		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);

				TRACE("[%s][%d]\n", szExchange, iValdateModify);

				pTrim = Cfg_RemoveWhiteSpace(szExchange);

				strncpyz(stACUSMTPInfo->szEmailSeverIP,
					pTrim,
					sizeof(stACUSMTPInfo->szEmailSeverIP));

				ptr = ptr + iPosition;

			}

		}

		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);

				TRACE("[%s][%d]\n", szExchange, iValdateModify);

				pTrim = Cfg_RemoveWhiteSpace(szExchange);

				stACUSMTPInfo->iEmailServerPort = atoi(pTrim);

				ptr = ptr + iPosition;

			}

		}

		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);

				TRACE("[%s][%d]\n", szExchange, iValdateModify);

				pTrim = Cfg_RemoveWhiteSpace(szExchange);

				strncpyz(stACUSMTPInfo->szEmailAccount,
					pTrim,
					sizeof(stACUSMTPInfo->szEmailAccount));

				ptr = ptr + iPosition;

			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);

				TRACE("[%s][%d]\n", szExchange, iValdateModify);

				pTrim = Cfg_RemoveWhiteSpace(szExchange);

				strncpyz(stACUSMTPInfo->szEmailPasswd,
					pTrim,
					sizeof(stACUSMTPInfo->szEmailPasswd));

				ptr = ptr + iPosition;

			}


		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);

				TRACE("[%s][%d]\n", szExchange, iValdateModify);

				pTrim = Cfg_RemoveWhiteSpace(szExchange);

				stACUSMTPInfo->iEmailAuthen = atoi(pTrim);

				ptr = ptr + iPosition;

			}


		}

		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);

				TRACE("[%s][%d]\n", szExchange, iValdateModify);

				pTrim = Cfg_RemoveWhiteSpace(szExchange);

				stACUSMTPInfo->iAlarmLevel = atoi(pTrim);

				ptr = ptr + iPosition;

			}


		}

		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);

				TRACE("[%s][%d]\n", szExchange, iValdateModify);

				pTrim = Cfg_RemoveWhiteSpace(szExchange);

				strncpyz(stACUSMTPInfo->szEmailFrom,
					pTrim,
					sizeof(stACUSMTPInfo->szEmailFrom));

				ptr = ptr + iPosition;

			}

		}

		return TRUE;
	}
	return FALSE;

}
/*==========================================================================*
* FUNCTION :  Web_ModifySMSPrivateConfigure
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN char *szBuffer:
* RETURN   :  
* COMMENTS : 
* CREATOR  : WangJing               DATE: 2006-7-31 11:20
*==========================================================================*/
static int Web_ModifySMSPrivateConfigure(IN char *szBuffer,IN char *szUserName)
{

	ASSERT(szBuffer);
	ASSERT(szUserName);
	int iBufLength = sizeof(SMS_PHONE_INFO);

	SMS_PHONE_INFO stACUSMSInfo;

	if(Web_GetSMSModifyInfo(szBuffer, &stACUSMSInfo) != FALSE)
	{
		//printf("Web User Change SMS Config Info OK!:%s,%s,%s,%d",stACUSMSInfo.szPhoneNumber1 ,stACUSMSInfo.szPhoneNumber2,stACUSMSInfo.szPhoneNumber3,stACUSMSInfo.iAlarmLevel);


		if(DxiSetData(VAR_SMS_PHONE_INFO, SMS_INFO_PHONE_ALL, 0,  iBufLength,  &stACUSMSInfo, 0) == ERR_DXI_OK)
		{
			AppLogOut(CGI_APP_LOG_COMM_NAME, APP_LOG_INFO,"Web User Change SMS Config Info OK!:%s,%s,%s,%d",stACUSMSInfo.szPhoneNumber1 ,stACUSMSInfo.szPhoneNumber2,stACUSMSInfo.szPhoneNumber3,stACUSMSInfo.iAlarmLevel);

		}
		else
		{
			return FALSE;
		}


	}

	return TRUE;

}

/*==========================================================================*
* FUNCTION :  Web_ModifyVPNPrivateConfigure
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN char *szBuffer:
* RETURN   :  
* COMMENTS : 
* CREATOR  : WangJing               DATE: 2006-7-31 11:20
*==========================================================================*/
static int Web_ModifyVPNPrivateConfigure(IN char *szBuffer,IN char *szUserName)
{

	ASSERT(szBuffer);
	ASSERT(szUserName);
	int iBufLength = sizeof(ACU_VPN_INFO);

	ACU_VPN_INFO stACUVPNInfo;

	if(Web_GetVPNModifyInfo(szBuffer, &stACUVPNInfo) != FALSE)
	{

		if(DxiSetData(VAR_VPN_INFO, VPN_INFO_ALL, 0,  iBufLength,  &stACUVPNInfo, 0) == ERR_DXI_OK)
		{
			AppLogOut(CGI_APP_LOG_COMM_NAME, APP_LOG_INFO, "Web User Change VPN Config Info OK!:%d,%d,%d",stACUVPNInfo.iRemotePort,stACUVPNInfo.iVPNEnable,stACUVPNInfo.ulRemoteIp);

		}
		else
		{
			return FALSE;
		}


	}

	return TRUE;

}

/*==========================================================================*
* FUNCTION :  Web_ModifySMTPPrivateConfigure
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN char *szBuffer:
* RETURN   :  
* COMMENTS : 
* CREATOR  : WangJing               DATE: 2006-7-31 11:20
*==========================================================================*/
static int Web_ModifySMTPPrivateConfigure(IN char *szBuffer,IN char *szUserName)
{

	ASSERT(szBuffer);
	ASSERT(szUserName);
	int iBufLength = sizeof(ACU_SMTP_INFO);

	ACU_SMTP_INFO stACUSMTPInfo = {0};

	//printf("%s\n",szBuffer);

	if( Web_GetSMTPModifyInfo(szBuffer, &stACUSMTPInfo) != FALSE)
	{
		if(Web_JudgeXSS_Illegal_Char(stACUSMTPInfo.szEmailSendTo)==FALSE||
		Web_JudgeXSS_Illegal_Char(stACUSMTPInfo.szEmailFrom)==FALSE)
		{
			return FALSE;
		}

		if(DxiSetData(VAR_SMTP_INFO, SMTP_INFO_ALL, 0,  iBufLength,  &stACUSMTPInfo, 0) == ERR_DXI_OK)
		{
			AppLogOut(CGI_APP_LOG_COMM_NAME, APP_LOG_INFO,"Web User Change SMTP Config Info OK!:%s,%s,%s,%s,%d,%d",stACUSMTPInfo.szEmailAccount,stACUSMTPInfo.szEmailFrom,stACUSMTPInfo.szEmailPasswd,stACUSMTPInfo.szEmailSendTo,stACUSMTPInfo.szEmailSeverIP,stACUSMTPInfo.iEmailAuthen,stACUSMTPInfo.iEmailServerPort);

		}
		else
		{
			return FALSE;
		}


	}

	return TRUE;

}

/*==========================================================================*
* FUNCTION :  Web_ModifyMODBUSPrivateConfigure
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN char *szBuffer:
* RETURN   :  
* COMMENTS : 
* CREATOR  : WangJing               DATE: 2006-7-31 11:20
*==========================================================================*/
static int Web_ModifyMODBUSPrivateConfigure(IN char *szBuffer,IN char *szUserName)
{

	ASSERT(szBuffer);
	MODBUS_COMMON_CONFIG	stCommonConfig = {0};;
	//COMMON_CONFIG	stEEMCommonConfig = {0} ;
	APP_SERVICE		*stAppService = NULL;
	int				iModifyType = 0;
	//int				iEEMModifyType = 0;
	int				iReturn = 0;
	//char            szTemp[2048];
	char            szTempUserName[40];
#ifdef _SHOW_WEB_INFO
	//TRACE("szBuffer : %s\n", szBuffer);
#endif

	sprintf(szTempUserName,"Web:%s ",szUserName);
	strncpyz(stCommonConfig.cModifyUser,szTempUserName,strlen(szTempUserName)+1);

	TRACE("\n----------- ModifyUser : %s ----- \n", stCommonConfig.cModifyUser);
	if((iModifyType = Web_GetModbusModifyInfo(szBuffer, &stCommonConfig)) != FALSE)
	{
		//TRACE("iModifyType : %x ----- 1\n", iModifyType);
		stCommonConfig.iProtocolType = 5; 
		//Change mode from EEM to YDN23
		//if(stCommonConfig.iProtocolType == YDN23)
		//{
		//	stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_ESR_NAME);
		//	if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
		//	{
		//		iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
		//			stAppService->bServiceRunning,
		//			&stAppService->args, 
		//			0x0,
		//			0,
		//			(void *)&stEEMCommonConfig);


		//		if(iReturn == ERR_SERVICE_CFG_OK)
		//		{
		//			stEEMCommonConfig.iProtocolType = YDN23;
		//			stEEMCommonConfig.iMediaType = 2;//tcp/ip
		//			strncpyz(stEEMCommonConfig.szCommPortParam,"2000",5);


		//			iEEMModifyType = iEEMModifyType | ESR_CFG_W_MODE;
		//			iEEMModifyType = iEEMModifyType | ESR_CFG_ALL | ESR_CFG_PROTOCOL_TYPE |ESR_CFG_MEDIA_TYPE | ESR_CFG_MEDIA_PORT_PARAM;
		//			TRACE("iEEMModifyType = %d\n", iEEMModifyType);
		//			iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
		//				stAppService->bServiceRunning,
		//				&stAppService->args, 
		//				iEEMModifyType,
		//				0,
		//				(void *)&stEEMCommonConfig);// == ERR_SERVICE_CFG_OK)
		//			TRACE("\nEEMiReturn = %d\n", iReturn);
		//			if(iReturn != ERR_SERVICE_CFG_OK)
		//			{
		//				return iReturn;
		//			}
		//		}
		//		else
		//		{
		//			return iReturn;
		//		}

		//	}
		//}


		stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_MODBUS_NAME);
		stCommonConfig.iProtocolType = 5; 

		if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
		{
			TRACE("iModifyType : %x ----- 2", iModifyType);
			TRACE("stAppService->bServiceRunning : %d\n",stAppService->bServiceRunning);

			iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
				stAppService->bServiceRunning,
				&stAppService->args, 
				iModifyType,
				0,
				(void *)&stCommonConfig);// == ERR_SERVICE_CFG_OK)
			TRACE("\nYDNiReturn = %d\n", iReturn);

			return iReturn;
		}
	}
	return ERR_SERVICE_CFG_FAIL;
}
/*==========================================================================*
* FUNCTION :  Web_ModifyYDNPrivateConfigure
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN char *szBuffer:
* RETURN   :  
* COMMENTS : 
* CREATOR  : WangJing               DATE: 2006-7-31 11:20
*==========================================================================*/
static int Web_ModifyYDNPrivateConfigure(IN char *szBuffer,IN char *szUserName)
{

	ASSERT(szBuffer);
	YDN_COMMON_CONFIG	stCommonConfig = {0};
	//COMMON_CONFIG	stEEMCommonConfig = {0} ;
	APP_SERVICE		*stAppService = NULL;
	int				iModifyType = 0;
	//int				iEEMModifyType = 0;
	int				iReturn = 0;
	//char            szTemp[2048];
	char            szTempUserName[40];
#ifdef _SHOW_WEB_INFO
	//TRACE("szBuffer : %s\n", szBuffer);
#endif

	sprintf(szTempUserName,"Web:%s ",szUserName);
	strncpyz(stCommonConfig.cModifyUser,szTempUserName,strlen(szTempUserName)+1);

	TRACE("\n----------- ModifyUser : %s ----- \n", stCommonConfig.cModifyUser);
	if((iModifyType = Web_GetYDNModifyInfo(szBuffer, &stCommonConfig)) != FALSE)
	{
		//TRACE("iModifyType : %x ----- 1\n", iModifyType);
		stCommonConfig.iProtocolType = 4; 
		//Change mode from EEM to YDN23
		//if(stCommonConfig.iProtocolType == YDN23)
		//{
		//	stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_ESR_NAME);
		//	if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
		//	{
		//		iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
		//			stAppService->bServiceRunning,
		//			&stAppService->args, 
		//			0x0,
		//			0,
		//			(void *)&stEEMCommonConfig);


		//		if(iReturn == ERR_SERVICE_CFG_OK)
		//		{
		//			stEEMCommonConfig.iProtocolType = YDN23;
		//			stEEMCommonConfig.iMediaType = 2;//tcp/ip
		//			strncpyz(stEEMCommonConfig.szCommPortParam,"2000",5);


		//			iEEMModifyType = iEEMModifyType | ESR_CFG_W_MODE;
		//			iEEMModifyType = iEEMModifyType | ESR_CFG_ALL | ESR_CFG_PROTOCOL_TYPE |ESR_CFG_MEDIA_TYPE | ESR_CFG_MEDIA_PORT_PARAM;
		//			TRACE("iEEMModifyType = %d\n", iEEMModifyType);
		//			iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
		//				stAppService->bServiceRunning,
		//				&stAppService->args, 
		//				iEEMModifyType,
		//				0,
		//				(void *)&stEEMCommonConfig);// == ERR_SERVICE_CFG_OK)
		//			TRACE("\nEEMiReturn = %d\n", iReturn);
		//			if(iReturn != ERR_SERVICE_CFG_OK)
		//			{
		//				return iReturn;
		//			}
		//		}
		//		else
		//		{
		//			return iReturn;
		//		}

		//	}
		//}


		stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_YDN_NAME);
		stCommonConfig.iProtocolType = 4; 

		if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
		{
			TRACE("iModifyType : %x ----- 2", iModifyType);
			TRACE("stAppService->bServiceRunning : %d\n",stAppService->bServiceRunning);

			iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
				stAppService->bServiceRunning,
				&stAppService->args, 
				iModifyType,
				0,
				(void *)&stCommonConfig);// == ERR_SERVICE_CFG_OK)
			TRACE("\nYDNiReturn = %d\n", iReturn);

			return iReturn;
		}
	}
	return ERR_SERVICE_CFG_FAIL;
}

/*==========================================================================*
* FUNCTION :  Web_MakeNMSPrivateConfigureBuffer
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:		
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
* MODIFY   : Zhao Zicheng 2013-11-07 To modify the snmpv2/v3 trap strategy
*==========================================================================*/
static char *Web_MakeNMSV3PrivateConfigureBuffer(void)
{
    APP_SERVICE		*stAppService = NULL;
    int				iBufLen = 1;
    V3NMS_INFO		*stNmsInfo = NULL;
    char			*szNmsInfo = NULL;
    struct in_addr	inIP;
    int				i = 0, iLen = 0;
    int				iNMSNum = 0;
    char szV6IP[128];

    //stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,(void *)GET_SERVICE_OF_NMS_NAME);
    if(g_SiteInfo.iSNMPV3Flag == NO_SNMPV3)
    {
	//stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB, (void *)GET_SERVICE_OF_NMSV2_NAME);
	return NULL;
    }
    else
    {
	stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB, (void *)GET_SERVICE_OF_NMSV3_NAME);
    }

    //TRACE("**********stAppService \n");

    if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
    {
	//TRACE("**********stAppService->pfnServiceConfig : TRUE\n");

	if(stAppService->pfnServiceConfig(stAppService->hServiceThread,
	    stAppService->bServiceRunning,
	    &stAppService->args, 
	    GET_NMS_V3_USER_INFO,
	    &iBufLen,
	    (void *)&stNmsInfo) == ERR_SNP_OK)
	{
	    //print serviceconfig

	    iNMSNum =  iBufLen /sizeof(V3NMS_INFO);
	    if(iNMSNum > 0)
	    {
		szNmsInfo = NEW(char, 200 * iNMSNum);
		//TRACE("**********stAppService->pfnServiceConfig : TRUE  iBufLen : %d %d\n", iBufLen, iNMSNum);
		//iLen += sprintf(szNmsInfo + iLen, "[");
		for(i = 0; i < iNMSNum && stNmsInfo != NULL; i++, stNmsInfo++)
		{
		    if(stNmsInfo->bIPV6 == FALSE)
		    {
			inIP.s_addr = stNmsInfo->ulTrapIpAddress;
		    //memcpy(&inIP.s_addr, &stNmsInfo->ulIpAddress, sizeof(ULONG));
		    iLen += sprintf(szNmsInfo + iLen,"[%d,\"%s\",\" %s \",%d,\"%s\",\"%s\",%d,%d],",
			i,
			stNmsInfo->szUserName,
			inet_ntoa(inIP),
			(stNmsInfo->nTrapSecurityLevel),
			stNmsInfo->szPrivPwd,
			stNmsInfo->szAuthPwd,
			stNmsInfo->nV3TrapEnable,
			stNmsInfo->bIPV6
			);
		    }
		    else
		    {
			inet_ntop(AF_INET6, (const void *)(&(stNmsInfo->ulTrapIpV6Address)), szV6IP, 128);
			iLen += sprintf(szNmsInfo + iLen,"[%d,\"%s\",\" %s \",%d,\"%s\",\"%s\",%d,%d],",
			    i,
			    stNmsInfo->szUserName,
			    szV6IP,
			    (stNmsInfo->nTrapSecurityLevel),
			    stNmsInfo->szPrivPwd,
			    stNmsInfo->szAuthPwd,
			    stNmsInfo->nV3TrapEnable,
			    stNmsInfo->bIPV6
			    );
		    }
		}
		if(iLen >= 1)
		{
		    iLen = iLen - 1;	// get rid of the last "," to meet json format
		    iLen += sprintf(szNmsInfo + iLen, "");
		}
		else
		{
		    DELETE(szNmsInfo);
		    szNmsInfo = NULL;

		}
		printf("szNmsInfo is %s\n", szNmsInfo);
		return szNmsInfo;
	    }
	}
    }
    //TRACE("**********stAppService->pfnServiceConfig : FALSE\n");
    return NULL;
}

/*==========================================================================*
* FUNCTION :  Web_ModifyNMSV3PrivateConfigure
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN char *szNmsBuffer:	
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
* MODIFY   : Zhao Zicheng 2013-11-07 To modify the snmpv2/v3 trap strategy
*==========================================================================*/
static int Web_ModifyNMSV3PrivateConfigure(IN char *szNmsBuffer)
{
#define ADD_TYPE_OF_NMS					1
#define MODIFY_TYPE_OF_NMS				2
#define DELETE_TYPE_OF_NMS				3
#define CHANGETRAP_TYPE_OF_NMS				4
#define ADD_TYPE_OF_NMS_IPV6				5
#define MODIFY_TYPE_OF_NMS_IPV6				6

    ASSERT(szNmsBuffer);
    V3NMS_INFO		stNmsInfo;
    APP_SERVICE		*stAppService = NULL;
    int				iBufLen = sizeof(NMS_INFO);
    char			*ptr = NULL;
    char			szExchange[64], *pSearchValue = NULL;
    int				iPosition = 0;
    int				iModifyType = 0;
    int				iReturn = 0;
    char			*szTrim = NULL;


    TRACE_WEB_USER_NOT_CYCLE("szNmsBuffer : %s\n", szNmsBuffer);
    if((ptr = szNmsBuffer) != NULL)
    {
	pSearchValue = strchr(ptr, 59);
	if(pSearchValue != NULL)
	{
	    iPosition = pSearchValue - ptr;
	    if(iPosition > 0)
	    {
		strncpyz(szExchange, ptr, iPosition + 1);
		iModifyType = atoi(szExchange);
		ptr = ptr + iPosition;
	    }
	    else
	    {
		return FALSE;
	    }

	}
	TRACE_WEB_USER_NOT_CYCLE("1--szExchange : %s\n", szExchange);
	ptr = ptr + 1;

	if((iModifyType == ADD_TYPE_OF_NMS) || (iModifyType == MODIFY_TYPE_OF_NMS) || (iModifyType == DELETE_TYPE_OF_NMS) || (iModifyType == CHANGETRAP_TYPE_OF_NMS))
	{
	    stNmsInfo.bIPV6 = FALSE;
	}
	else
	{
	    stNmsInfo.bIPV6 = TRUE;
	}

	pSearchValue = strchr(ptr, 59);
	if(pSearchValue != NULL)
	{
	    iPosition = pSearchValue - ptr;
	    if(iPosition > 0)
	    {
		strncpyz(szExchange, ptr, iPosition + 1);
		szTrim = Cfg_RemoveWhiteSpace(szExchange);
		sprintf(stNmsInfo.szUserName, szTrim, sizeof(stNmsInfo.szUserName));		

		ptr = ptr + iPosition;
	    }

	}
	ptr = ptr + 1;

	TRACE_WEB_USER_NOT_CYCLE("2--szExchange : %s\n", szExchange);
	pSearchValue = strchr(ptr, 59);
	if(pSearchValue != NULL)
	{
	    iPosition = pSearchValue - ptr;
	    if(iPosition > 0)
	    {
		strncpyz(szExchange, ptr, iPosition + 1);
		char		*szIP = NULL;
		szIP = Cfg_RemoveWhiteSpace(szExchange + 1);
		if((iModifyType == ADD_TYPE_OF_NMS) || (iModifyType == MODIFY_TYPE_OF_NMS) || (iModifyType == DELETE_TYPE_OF_NMS) || (iModifyType == CHANGETRAP_TYPE_OF_NMS))
		{
		    stNmsInfo.bIPV6 = FALSE;
		    stNmsInfo.ulTrapIpAddress = inet_addr(szIP);
		}
		else
		{
		    stNmsInfo.bIPV6 = TRUE;
		    inet_pton(AF_INET6, (const char *)szIP, (void *)(&stNmsInfo.ulTrapIpV6Address));
		}

		ptr = ptr + iPosition;
	    }

	}
	TRACE_WEB_USER_NOT_CYCLE("3--szExchange : %s\n", szExchange);
	ptr = ptr + 1;

	pSearchValue = strchr(ptr, 59);
	if(pSearchValue != NULL)
	{
	    iPosition = pSearchValue - ptr;
	    if(iPosition > 0)
	    {
		strncpyz(szExchange, ptr, iPosition + 1);
		szTrim = Cfg_RemoveWhiteSpace(szExchange);
		sprintf(stNmsInfo.szPrivPwd, szTrim, sizeof(stNmsInfo.szPrivPwd));
		//TRACE("szPublicCommunity : %s\n", stNmsInfo.szPublicCommunity);
		ptr = ptr + iPosition;
	    }

	}
	ptr = ptr + 1;
	TRACE_WEB_USER_NOT_CYCLE("4--szExchange : %s\n", szExchange);
	pSearchValue = strchr(ptr, 59);
	if(pSearchValue != NULL)
	{
	    iPosition = pSearchValue - ptr;
	    if(iPosition > 0)
	    {
		strncpyz(szExchange, ptr, iPosition + 1);
		szTrim = Cfg_RemoveWhiteSpace(szExchange);
		sprintf(stNmsInfo.szAuthPwd, szTrim, sizeof(stNmsInfo.szAuthPwd));
		ptr = ptr + iPosition;
	    }

	}
	ptr = ptr + 1;

	TRACE_WEB_USER_NOT_CYCLE("5--szExchange : %s\n", szExchange);
	pSearchValue = strchr(ptr, 59);
	if(pSearchValue != NULL)
	{
	    iPosition = pSearchValue - ptr;
	    if(iPosition > 0)
	    {
		strncpyz(szExchange, ptr, iPosition + 1);
		//szTrim = Cfg_RemoveWhiteSpace(szExchange);
		//sprintf(stNmsInfo.nTrapSecurityLevel, szTrim, sizeof(stNmsInfo.nTrapSecurityLevel));
		stNmsInfo.nTrapSecurityLevel = atoi(szExchange);
		ptr = ptr + iPosition;			
	    }
	}
	ptr = ptr + 1;
	TRACE_WEB_USER_NOT_CYCLE("6--szExchange : %s\n", szExchange);

	pSearchValue = strchr(ptr, 59);
	if(pSearchValue != NULL)
	{
	    iPosition = pSearchValue - ptr;
	    if(iPosition > 0)
	    {
		strncpyz(szExchange, ptr, iPosition + 1);
		//szTrim = Cfg_RemoveWhiteSpace(szExchange);
		//sprintf(stNmsInfo.nV3TrapEnable, szTrim, sizeof(stNmsInfo.nV3TrapEnable));
		stNmsInfo.nV3TrapEnable = atoi(szExchange);
		ptr = ptr + iPosition;			
	    }
	}
	TRACE_WEB_USER_NOT_CYCLE("7--szExchange : %s\n", szExchange);
	if(stNmsInfo.nV3TrapEnable == 0)    // no v3 trap function
	{
	    stNmsInfo.ulTrapIpAddress = 0;
	    stNmsInfo.nTrapSecurityLevel = 0;
	    stNmsInfo.ulTrapIpV6Address.in6_u.u6_addr32[0] = 0;
	    stNmsInfo.ulTrapIpV6Address.in6_u.u6_addr32[1] = 0;
	    stNmsInfo.ulTrapIpV6Address.in6_u.u6_addr32[2] = 0;
	    stNmsInfo.ulTrapIpV6Address.in6_u.u6_addr32[3] = 0;
	}
	TRACE_WEB_USER_NOT_CYCLE("iModifyType is %d, szUserName is %s, ulTrapIpAddress is %ld, szPrivPwd is %s, szAuthPwd is %s, nTrapSecurityLevel is %d, nV3TrapEnable is %d\n", 
	    iModifyType, stNmsInfo.szUserName, stNmsInfo.ulTrapIpAddress, stNmsInfo.szPrivPwd, stNmsInfo.szAuthPwd, stNmsInfo.nTrapSecurityLevel, stNmsInfo.nV3TrapEnable);
	TRACE_WEB_USER_NOT_CYCLE("ulTrapIpV6Address is %ld  %ld	%ld %ld\n", stNmsInfo.ulTrapIpV6Address.in6_u.u6_addr32[0],
	    stNmsInfo.ulTrapIpV6Address.in6_u.u6_addr32[1], stNmsInfo.ulTrapIpV6Address.in6_u.u6_addr32[2], stNmsInfo.ulTrapIpV6Address.in6_u.u6_addr32[3]);
    }
    else
    {
	return FALSE;
    }



    if(g_SiteInfo.iSNMPV3Flag == NO_SNMPV3)
    {
	stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB, (void *)GET_SERVICE_OF_NMSV2_NAME);
    }
    else
    {
	stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB, (void *)GET_SERVICE_OF_NMSV3_NAME);
    }
    //TRACE_WEB_USER_NOT_CYCLE("\n iModifyType = %d[%d] [%d]\n",iModifyType, stAppService != NULL, stAppService->pfnServiceConfig != NULL);
    //stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB, (void *)GET_SERVICE_OF_NMS_NAME);
    if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
    {
	//added by YangGuoxin,3/20/2007
	if(iModifyType == DELETE_TYPE_OF_NMS)// delete
	{
	    TRACE_WEB_USER_NOT_CYCLE("Deal with DELETE_NMS_V3_USER_INFO\n");
	    iReturn += stAppService->pfnServiceConfig(stAppService->hServiceThread,
		stAppService->bServiceRunning,
		&stAppService->args, 
		DELETE_NMS_V3_USER_INFO,
		&iBufLen,
		(void *)&stNmsInfo);// == ERR_SNP_OK)
	    TRACE_WEB_USER_NOT_CYCLE("DELETE_TYPE_OF_NMS iReturn = %d\n", iReturn);		
	}
	else if((iModifyType == MODIFY_TYPE_OF_NMS ) || (iModifyType == MODIFY_TYPE_OF_NMS_IPV6))//modify 
	{
	    TRACE_WEB_USER_NOT_CYCLE("Deal with MODIFY_NMS_V3_USER_INFO\n");
	    iReturn += stAppService->pfnServiceConfig(stAppService->hServiceThread,
		stAppService->bServiceRunning,
		&stAppService->args, 
		MODIFY_NMS_V3_USER_INFO,
		&iBufLen,
		(void *)&stNmsInfo);
	    TRACE_WEB_USER_NOT_CYCLE("MODIFY_TYPE_OF_NMS iReturn = %d\n", iReturn);
	}
	else if((iModifyType == ADD_TYPE_OF_NMS) || (iModifyType == ADD_TYPE_OF_NMS_IPV6))//add 
	{
	    TRACE_WEB_USER_NOT_CYCLE("Deal with ADD_NMS_V3_USER_INFO\n");
	    iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
		stAppService->bServiceRunning,
		&stAppService->args, 
		ADD_NMS_V3_USER_INFO,
		&iBufLen,
		(void *)&stNmsInfo);// == ERR_SNP_OK)	
	    TRACE_WEB_USER_NOT_CYCLE("ADD_TYPE_OF_NMS iReturn = %d\n", iReturn);		
	}


	//end by YangGuoxin, 3/20/2007

	if(iReturn == ERR_SNP_OK)
	{
	    return TRUE;//iReturn;
	}
	else if(iReturn == ERR_NMS_TOO_MANY_USERS)
	{
	    return 5;
	}
	else
	{
	    return FALSE;
	}

    }
    return -1;
}

/*==========================================================================*
* FUNCTION :  Web_MakeNMSTrapConfigureBuffer
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:		
* RETURN   :  
* COMMENTS : 
* CREATOR  : Zhao Zicheng               DATE: 2013-11-08
*==========================================================================*/
static char *Web_MakeNMSTrapConfigureBuffer(void)
{
#define GETTRAP_TYPE_OF_NMS			11

    APP_SERVICE		*stAppService = NULL;
    int				iBufLen = 1;
    char			*szNmsInfo = NULL;
    int				nTrapLevel;
    int				*pnTrapLevel = &nTrapLevel;
    int		iLen = 0;

    //TRACE_WEB_USER_NOT_CYCLE("The address of the point is %x\n", pnTrapLevel);
    if(g_SiteInfo.iSNMPV3Flag == NO_SNMPV3)
    {
	TRACE_WEB_USER_NOT_CYCLE("Load snmpv2 lib\n");
	stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,(void *)GET_SERVICE_OF_NMSV2_NAME);
    }
    else
    {
	TRACE_WEB_USER_NOT_CYCLE("Load snmpv3 lib\n");
	stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,(void *)GET_SERVICE_OF_NMSV3_NAME);
    }

    if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
    {
	if(stAppService->pfnServiceConfig(stAppService->hServiceThread,
	    stAppService->bServiceRunning,
	    &stAppService->args, 
	    GETTRAP_TYPE_OF_NMS,
	    &iBufLen,
	    (void *)pnTrapLevel) == ERR_SNP_OK)
	{
	    TRACE_WEB_USER_NOT_CYCLE("pnTrapLevel is %d\n", (*pnTrapLevel));
	    szNmsInfo = NEW(char, 10);
	    //TRACE("**********stAppService->pfnServiceConfig : TRUE  iBufLen : %d %d\n", iBufLen, iNMSNum);
	    iLen += sprintf(szNmsInfo + iLen,"%d", (*pnTrapLevel));
	    if(iLen >= 1)
	    {
		//*(szNmsInfo + iLen) = 32;
	    }
	    else
	    {
		DELETE(szNmsInfo);
		szNmsInfo = NULL;

	    }
	    //TRACE("szNmsInfo : %s\n", szNmsInfo);
	    TRACE_WEB_USER_NOT_CYCLE("szNmsInfo is %s\n", szNmsInfo);
	    return szNmsInfo;
	}
    }
    //TRACE("**********stAppService->pfnServiceConfig : FALSE\n");
    return NULL;
}

/*==========================================================================*
* FUNCTION :  Web_MakeNMSPrivateConfigureBuffer
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:		
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
* MODIFY   : Zhao Zicheng 2013-11-07 To modify the snmpv2/v3 trap strategy
*==========================================================================*/
static char *Web_MakeNMSPrivateConfigureBuffer(void)
{
    APP_SERVICE		*stAppService = NULL;
    int				iBufLen = 1;
    NMS_INFO		*stNmsInfo = NULL;
    char			*szNmsInfo = NULL;
    struct in_addr	inIP;
    int				i = 0, iLen = 0;
    int				iNMSNum = 0;
    char szV6IP[128];

    if(g_SiteInfo.iSNMPV3Flag == NO_SNMPV3)
    {
	stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,(void *)GET_SERVICE_OF_NMSV2_NAME);
    }
    else
    {
	stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,(void *)GET_SERVICE_OF_NMSV3_NAME);
    }	


    //stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,(void *)GET_SERVICE_OF_NMS_NAME);

    //TRACE("**********stAppService \n");

    if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
    {
	//TRACE("**********stAppService->pfnServiceConfig : TRUE\n");

	if(stAppService->pfnServiceConfig(stAppService->hServiceThread,
	    stAppService->bServiceRunning,
	    &stAppService->args, 
	    GET_NMS_USER_INFO,
	    &iBufLen,
	    (void *)&stNmsInfo) == ERR_SNP_OK)
	{
	    //print serviceconfig

	    iNMSNum =  iBufLen /sizeof(NMS_INFO);
	    if(iNMSNum > 0)
	    {
		szNmsInfo = NEW(char, 200 * iNMSNum);
		//TRACE("**********stAppService->pfnServiceConfig : TRUE  iBufLen : %d %d\n", iBufLen, iNMSNum);
		//iLen += sprintf(szNmsInfo + iLen, "[");
		for(i = 0; i < iNMSNum && stNmsInfo != NULL; i++, stNmsInfo++)
		{
		    if(stNmsInfo->bIPV6 == FALSE)
		    {
			inIP.s_addr = stNmsInfo->ulIpAddress;
			//memcpy(&inIP.s_addr, &stNmsInfo->ulIpAddress, sizeof(ULONG));
			iLen += sprintf(szNmsInfo + iLen,"[%d,\"%s\",\"%s\",\"%s\",%d,%d],",
			    i,
			    inet_ntoa(inIP),
			    stNmsInfo->szPublicCommunity,
			    stNmsInfo->szPrivateCommunity,
			    stNmsInfo->nTrapEnable,
			    stNmsInfo->bIPV6
			    );
		    }
		    else
		    {
			inet_ntop(AF_INET6, (const void *)(&(stNmsInfo->unIPV6Address)), szV6IP, 128);
			TRACE_WEB_USER_NOT_CYCLE("szV6IP is %s\n", szV6IP);
			iLen += sprintf(szNmsInfo + iLen,"[%d,\"%s\",\"%s\",\"%s\",%d,%d],",
			    i,
			    szV6IP,
			    stNmsInfo->szPublicCommunity,
			    stNmsInfo->szPrivateCommunity,
			    stNmsInfo->nTrapEnable,
			    stNmsInfo->bIPV6
			    );
		    }
		}
		if(iLen >= 1)
		{
		    iLen = iLen - 1;	// get rid of the last "," to meet json format
		    iLen += sprintf(szNmsInfo + iLen, "");
		}
		else
		{
		    DELETE(szNmsInfo);
		    szNmsInfo = NULL;

		}
		//TRACE("szNmsInfo : %s\n", szNmsInfo);
		TRACE_WEB_USER_NOT_CYCLE("szNmsInfo is %s\n", szNmsInfo);
		return szNmsInfo;
	    }
	}
    }
    //TRACE("**********stAppService->pfnServiceConfig : FALSE\n");
    return NULL;
}

/*==========================================================================*
* FUNCTION :  Web_ModifyNMSTrapConfigure
* PURPOSE  :
* CALLS    :
* CALLED BY:
* ARGUMENTS:	IN char *szNmsBuffer:
* RETURN   :
* COMMENTS :
* CREATOR  : Zhao Zicheng               DATE: 2013-11-08
*==========================================================================*/
static int Web_ModifyNMSTrapConfigure(IN char *szNmsBuffer)
{
#define CHANGETRAP_TYPE_OF_NMS			4

    ASSERT(szNmsBuffer);
    APP_SERVICE		*stAppService = NULL;
    int				iBufLen = sizeof(NMS_INFO);
    char			*ptr = NULL;
    char			szExchange[64], *pSearchValue = NULL;
    int				iPosition = 0;
    int				iReturn = 0;
    int				nTrapLevel;

    TRACE_WEB_USER_NOT_CYCLE("szNmsBuffer is %s\n", szNmsBuffer);
    if((ptr = szNmsBuffer) != NULL)
    {
	pSearchValue = strchr(ptr, 59);
	if(pSearchValue != NULL)
	{
	    iPosition = pSearchValue - ptr;
	    if(iPosition > 0)
	    {
		strncpyz(szExchange, ptr, iPosition + 1);
		nTrapLevel = atoi(szExchange);
		ptr = ptr + iPosition;
	    }
	    else
	    {
		return FALSE;
	    }
	}
    }
    else
    {
	return FALSE;
    }

    TRACE_WEB_USER_NOT_CYCLE("nTrapLevel is %d\n", nTrapLevel);

    if(g_SiteInfo.iSNMPV3Flag == NO_SNMPV3)
    {
	stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB, (void *)GET_SERVICE_OF_NMSV2_NAME);
    }
    else
    {
	stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB, (void *)GET_SERVICE_OF_NMSV3_NAME);
    }

    //stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB, (void *)GET_SERVICE_OF_NMS_NAME);

    if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
    {
	//added by YangGuoxin,3/20/2007
	    iReturn += stAppService->pfnServiceConfig(stAppService->hServiceThread,
		stAppService->bServiceRunning,
		&stAppService->args, 
		CHANGETRAP_TYPE_OF_NMS,
		&iBufLen,
		(void *)&nTrapLevel);// == ERR_SNP_OK)

	//end by YangGuoxin, 3/20/2007
	if(iReturn == ERR_SNP_OK)
	{
	    return TRUE;//iReturn;
	}
	else if(iReturn == ERR_NMS_TOO_MANY_USERS)
	{
	    return 5;
	}
	else
	{
	    return FALSE;
	}
    }
    return -1;
}

/*==========================================================================*
* FUNCTION :  Web_ModifyNMSPrivateConfigure
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN char *szNmsBuffer:	
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
* MODIFY   : Zhao Zicheng 2013-11-07 To modify the snmpv2/v3 trap strategy
* MODIFY   : Zhao Zicheng 2014-08-24 To add the IPV6 SNMP function
*==========================================================================*/
static int Web_ModifyNMSPrivateConfigure(IN char *szNmsBuffer)
{
#define ADD_TYPE_OF_NMS					1
#define MODIFY_TYPE_OF_NMS				2
#define DELETE_TYPE_OF_NMS				3
#define ADD_TYPE_OF_NMS_IPV6				4
#define MODIFY_TYPE_OF_NMS_IPV6				5
#define DELETE_TYPE_OF_NMS_IPV6				6

    ASSERT(szNmsBuffer);
    NMS_INFO		stNmsInfo;
    APP_SERVICE		*stAppService = NULL;
    int				iBufLen = sizeof(NMS_INFO);
    char			*ptr = NULL;
    char			szExchange[64], *pSearchValue = NULL;
    int				iPosition = 0;
    int				iModifyType = 0;
    int				iReturn = 0;
    char			*szTrim = NULL;


    TRACE_WEB_USER_NOT_CYCLE("szNmsBuffer is %s\n", szNmsBuffer);
    if((ptr = szNmsBuffer) != NULL)
    {
	pSearchValue = strchr(ptr, 59);
	if(pSearchValue != NULL)
	{
	    iPosition = pSearchValue - ptr;
	    if(iPosition > 0)
	    {
		strncpyz(szExchange, ptr, iPosition + 1);
		TRACE_WEB_USER_NOT_CYCLE("szExchange is %s\n", szExchange);
		iModifyType = atoi(szExchange);
		ptr = ptr + iPosition;
	    }
	    else
	    {
		return FALSE;
	    }

	}
	ptr = ptr + 1;


	pSearchValue = strchr(ptr, 59);
	if(pSearchValue != NULL)
	{
	    iPosition = pSearchValue - ptr;
	    if(iPosition > 0)
	    {
		strncpyz(szExchange, ptr, iPosition + 1);
		TRACE_WEB_USER_NOT_CYCLE("szExchange is %s\n", szExchange);
		char		*szIP = NULL;
		szIP = Cfg_RemoveWhiteSpace(szExchange + 1);
		TRACE_WEB_USER_NOT_CYCLE("szIP is %s\n", szIP);
		//IPV4 or IPV6
		if((iModifyType == ADD_TYPE_OF_NMS) || (iModifyType == MODIFY_TYPE_OF_NMS) || (iModifyType == DELETE_TYPE_OF_NMS))
		{
		    stNmsInfo.ulIpAddress = inet_addr(szIP);
		    stNmsInfo.bIPV6 = FALSE;
		    TRACE_WEB_USER_NOT_CYCLE("ulIpAddress is %ld\n", stNmsInfo.ulIpAddress);
		}
		else
		{
		    inet_pton(AF_INET6, (const char *)szIP, (void *)(&stNmsInfo.unIPV6Address));
		    stNmsInfo.bIPV6 = TRUE;
		}

		ptr = ptr + iPosition;
	    }

	}
	ptr = ptr + 1;

	pSearchValue = strchr(ptr, 59);
	if(pSearchValue != NULL)
	{
	    iPosition = pSearchValue - ptr;
	    if(iPosition > 0)
	    {
		strncpyz(szExchange, ptr, iPosition + 1);
		szTrim = Cfg_RemoveWhiteSpace(szExchange);
		TRACE_WEB_USER_NOT_CYCLE("szTrim is %s\n", szTrim);
		sprintf(stNmsInfo.szPublicCommunity, szTrim, sizeof(stNmsInfo.szPublicCommunity));
		TRACE_WEB_USER_NOT_CYCLE("stNmsInfo.szPublicCommunity is %s\n", stNmsInfo.szPublicCommunity);
		//TRACE("szPublicCommunity : %s\n", stNmsInfo.szPublicCommunity);
		ptr = ptr + iPosition;
	    }

	}
	ptr = ptr + 1;

	pSearchValue = strchr(ptr, 59);
	if(pSearchValue != NULL)
	{
	    iPosition = pSearchValue - ptr;
	    if(iPosition > 0)
	    {
		strncpyz(szExchange, ptr, iPosition + 1);
		szTrim = Cfg_RemoveWhiteSpace(szExchange);
		TRACE_WEB_USER_NOT_CYCLE("szTrim is %s\n", szTrim);
		sprintf(stNmsInfo.szPrivateCommunity, szTrim, sizeof(stNmsInfo.szPrivateCommunity));
		TRACE_WEB_USER_NOT_CYCLE("stNmsInfo.szPrivateCommunity is %s\n", stNmsInfo.szPrivateCommunity);
		//TRACE("szPrivateCommunity : %s\n", stNmsInfo.szPrivateCommunity);
		ptr = ptr + iPosition;
	    }

	}
	ptr = ptr + 1;
	pSearchValue = strchr(ptr, 59);
	if(pSearchValue != NULL)
	{
	    iPosition = pSearchValue - ptr;
	    if(iPosition > 0)
	    {
		strncpyz(szExchange, ptr, iPosition + 1);
		szTrim = Cfg_RemoveWhiteSpace(szExchange);
		TRACE_WEB_USER_NOT_CYCLE("szTrim is %s\n", szTrim);
		stNmsInfo.nTrapEnable = atoi(szTrim);
		TRACE_WEB_USER_NOT_CYCLE("stNmsInfo.nTrapEnable is %d\n", stNmsInfo.nTrapEnable);
	    }
	}

	//here we don't use this wau to fill with szExchange
	/*strncpyz(szExchange, ptr, (int)strlen(ptr) + 1);
	TRACE_WEB_USER_NOT_CYCLE("szExchange is %s\n", szExchange);
	stNmsInfo.nTrapEnable = atoi(szExchange);*/

	TRACE_WEB_USER_NOT_CYCLE("unIPV6Address is %ld	%ld %ld	%ld\n", stNmsInfo.unIPV6Address.in6_u.u6_addr32[0],
	    stNmsInfo.unIPV6Address.in6_u.u6_addr32[1], stNmsInfo.unIPV6Address.in6_u.u6_addr32[2], stNmsInfo.unIPV6Address.in6_u.u6_addr32[3]);
	TRACE_WEB_USER_NOT_CYCLE("iModifyType is %d, ulIpAddress is %ld, szPublicCommunity is %s, szPrivateCommunity is %s, nTrapEnable is %d\n", 
	    iModifyType, stNmsInfo.ulIpAddress, stNmsInfo.szPublicCommunity, stNmsInfo.szPrivateCommunity, stNmsInfo.nTrapEnable);

    }
    else
    {
	return FALSE;
    }

    stNmsInfo.nPrivilege = 0;

    if(g_SiteInfo.iSNMPV3Flag == NO_SNMPV3)
    {
	stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB, (void *)GET_SERVICE_OF_NMSV2_NAME);
    }
    else
    {
	stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB, (void *)GET_SERVICE_OF_NMSV3_NAME);
    }

    //stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB, (void *)GET_SERVICE_OF_NMS_NAME);

    if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
    {
	//added by YangGuoxin,3/20/2007
	if((iModifyType == DELETE_TYPE_OF_NMS) || (iModifyType == DELETE_TYPE_OF_NMS_IPV6))// delete
	{
	    TRACE_WEB_USER_NOT_CYCLE("Deal with DELETE_TYPE_OF_NMS or DELETE_TYPE_OF_NMS_IPV6\n");
	    iReturn += stAppService->pfnServiceConfig(stAppService->hServiceThread,
		stAppService->bServiceRunning,
		&stAppService->args, 
		DELETE_TYPE_OF_NMS,
		&iBufLen,
		(void *)&stNmsInfo);// == ERR_SNP_OK)
	}
	else if((iModifyType == MODIFY_TYPE_OF_NMS ) || (iModifyType == MODIFY_TYPE_OF_NMS_IPV6 ))//modify 
	{
	    TRACE_WEB_USER_NOT_CYCLE("Deal with MODIFY_TYPE_OF_NMS or MODIFY_TYPE_OF_NMS_IPV6\n");
	    iReturn += stAppService->pfnServiceConfig(stAppService->hServiceThread,
		stAppService->bServiceRunning,
		&stAppService->args, 
		MODIFY_TYPE_OF_NMS,
		&iBufLen,
		(void *)&stNmsInfo);
	}
	else if((iModifyType == ADD_TYPE_OF_NMS) || (iModifyType == ADD_TYPE_OF_NMS_IPV6))//add 
	{
	    TRACE_WEB_USER_NOT_CYCLE("Deal with ADD_TYPE_OF_NMS or ADD_TYPE_OF_NMS_IPV6\n");
	    iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
		stAppService->bServiceRunning,
		&stAppService->args, 
		ADD_TYPE_OF_NMS,
		&iBufLen,
		(void *)&stNmsInfo);// == ERR_SNP_OK)
	}


	//end by YangGuoxin, 3/20/2007

	if(iReturn == ERR_SNP_OK)
	{
	    return TRUE;//iReturn;
	}
	else if(iReturn == ERR_NMS_TOO_MANY_USERS)
	{
	    return 5;
	}
	else
	{
	    return FALSE;
	}

    }
    return -1;
}

//changed by Frank Wu,16/N/27,20140527, for power split

/*==========================================================================*
* FUNCTION :  Web_GetSampleSignalByEquipID
* PURPOSE  :   
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN int iEquipID:
* OUT SAMPLE_SIG_VALUE **ppSigValue:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:08
*==========================================================================*/
static int Web_GetSampleSignalByEquipID(IN int iEquipID, 
	OUT SAMPLE_SIG_VALUE **ppSigValue)
{
	SAMPLE_SIG_VALUE *pSigValue = NULL;
	int				iSignalNum = 0;
	int				iVarSubID = 0;
	int				iBufLen = 0;
	int				iTimeOut = 0;

	int				iError = DxiGetData(VAR_SAM_SIG_VALUE_OF_EQUIP,
		iEquipID,			
		iVarSubID,		
		&iBufLen,			
		(void*)&pSigValue,			
		iTimeOut);

	iSignalNum = iBufLen /sizeof(SAMPLE_SIG_VALUE);

	if(iError == ERR_DXI_OK)
	{
		*ppSigValue = pSigValue;
		return iSignalNum;
	}
	else
	{
		return FALSE;
	}

}




/*==========================================================================*
 * FUNCTION :  Web_GetControlSignalByEquipID
 * PURPOSE  :   
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int iEquipID:
				OUT CTRL_SIG_VALUE **ppSigValue:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:08
 *==========================================================================*/
static int Web_GetControlSignalByEquipID(IN int iEquipID, 
										 OUT CTRL_SIG_VALUE **ppSigValue)
{
		CTRL_SIG_VALUE	*pCtrlSigInfo = NULL;
		int				iSignalNum = 0;
		int				iVarSubID = 0;
		int				iBufLen = 0;
		int				iTimeOut = 0;
		int				iError = DxiGetData(VAR_CON_SIG_VALUE_OF_EQUIP,
											iEquipID,			
											iVarSubID,		
											&iBufLen,			
											(void*)&pCtrlSigInfo,			
											iTimeOut);

		iSignalNum = iBufLen /sizeof(CTRL_SIG_VALUE);
		
		if(iError == ERR_DXI_OK)
		{
			*ppSigValue = pCtrlSigInfo;
			return iSignalNum;
		}
		else
		{
			return FALSE;
		}
 
}


/*==========================================================================*
 * FUNCTION :  Web_GetSettingSignalByEquipID
 * PURPOSE  :   
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int iEquipID:
				OUT SET_SIG_VALUE **ppSigValue:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:08
 *==========================================================================*/
static int Web_GetSettingSignalByEquipID(IN int iEquipID, 
										 OUT SET_SIG_VALUE **ppSigValue)
{
	SET_SIG_VALUE *pSetSigValue = NULL;

	int iError = 0;

	int	iSignalNum = 0;
	int iVarSubID = 0;
	int iBufLen = 0;

	iError = DxiGetData(VAR_SET_SIG_VALUE_OF_EQUIP,
						iEquipID,			
						iVarSubID,		
						&iBufLen,			
						(void *)&pSetSigValue,			
						0);

	iSignalNum = iBufLen / sizeof(SET_SIG_VALUE);
	////TRACE("Web_GetSettingSignalByEquipID:iSignalNum[%d]\n", iSignalNum);
	if (iError == ERR_DXI_OK)
	{
		*ppSigValue = pSetSigValue;
		return iSignalNum;
	}
	else
	{
		return FALSE;
	}
}


/*==========================================================================*
 * FUNCTION :  Web_GetAlarmSigStruofEquip
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int iEquipID:
				OUT void **ppBuf:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:08
 *==========================================================================*/
static int Web_GetAlarmSigStruofEquip(IN int iEquipID,
									  OUT void **ppBuf)
{
	ALARM_SIG_INFO		*pAlarmSigInfo = NULL;
	int					iVarID,iInterfaceType,iError ;
	int					iLen = 0;
	int					iVarSubID = 0;
	int					iBufLen = 0;
	int					iTimeOut = 0;

	iVarID = iEquipID; //Equip ID
	iInterfaceType = VAR_ALARM_SIG_STRU_OF_EQUIP;
	
	iError = DxiGetData(iInterfaceType,
						iVarID,			
						iVarSubID,		
						&iBufLen,			
						&pAlarmSigInfo,			
						iTimeOut);
	
	iLen = iBufLen / sizeof(ALARM_SIG_INFO);
	if(iError == ERR_DXI_OK)
	{
		*ppBuf = (void *)pAlarmSigInfo;
		return iLen;
	}
	else
	{
		return FALSE;
			
	}
}


/*==========================================================================*
* FUNCTION :  Web_MakePlcWebPage
* PURPOSE  :  
* CALLS    : 
* CALLED BY:  Web_SendConfigureData 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
static int Web_MakeGCWebPage(OUT char **ppszReturn, IN int iLanguage)
{
	TRACE(" ___into Web_MakeGCWebPage___\n");

	int		    i, /*iFirstRectID = 0,*/ iEquipNum = 0, iBufLen;
	int         isigNum;
	//FILE        *fp          = NULL;
	EQUIP_INFO  *pEquipInfo  = NULL;
	//char        *pSearchValue= NULL;
	char        *pszReturnBuf = NULL;
	//char        *szFile      = NULL;
	//char        *pPageFile   = NULL;
	//char        *pHtml       = NULL;
	//char        *pszMakeVar  = NULL;
	//char        *pszID       = NULL;
	//char        *pszTemp     = NULL;
	char        *pszGCInfo  = NULL;
	//int         iReturn;
	int         iReturnBufLen = 0, iPLCInfoLen = 0;
	int			iReturnBufMaxLen = 0;
	int         iSamplingNum = 0, iControlNum = 0,iSettingNum = 0, iAlarmNum = 0;
	SAMPLE_SIG_VALUE  *pSampleSigValue  = NULL;
	CTRL_SIG_VALUE    *pControlSigValue = NULL;
	SET_SIG_VALUE     *pSettingSigValue = NULL;
	ALARM_SIG_INFO   *pAlarmSigInfo   = NULL;

	const char sPSModeList[][20] = {
		"SLAVE",
		"MASTER"
	};
	int iPSModeIndex = 0;

	/*get equip information*/
	int iError1 = DxiGetData(VAR_ACU_EQUIPS_LIST,
		0,			
		0,		
		&iBufLen,			
		&pEquipInfo,			
		0);

	/*get equip number*/
	int iError2 = DxiGetData(VAR_ACU_EQUIPS_NUM,
		0,			
		0,		
		&iBufLen,			
		(void *)&iEquipNum,			
		0);

	if(iError1 != ERR_DXI_OK || iError2 != ERR_DXI_OK)
	{
		return 0;
	}

	TRACE(" ______into Web_MakeGCWebPage________------2\n");
	//max config line == 100
	iPLCInfoLen = Web_LoadGCConfigFile(&pszGCInfo);//get GC Configure file Info
	TRACE(" ______into Web_MakeGCWebPage________------3\n");
	if(iPLCInfoLen == -1)
	{
		return 0;
	}

	char *pPSMode = NULL;
	int iError;
	iError = Web_GetGCPSMode(&pPSMode);
	if(iError != 1)
	{
		SAFELY_DELETE(pszGCInfo);

		return 0;
	}
	//compute index value in sPSModeList for pPSMode
	iPSModeIndex = 0;
	for(i = 0; i < sizeof(sPSModeList)/sizeof(sPSModeList[0]); i++)
	{
		if(  strnicmp( &pPSMode[1], sPSModeList[i], strlen(&pPSMode[1]) ) == 0  )
		{
			iPSModeIndex = i;
			break;
		}
	}
	TRACE("______Web_GetGCPSMode___OUT[%s]",pPSMode);

	pszReturnBuf = NEW(char,MAX_BUFFER);
	if (pszReturnBuf == NULL)
	{
		AppLogOut("WEB", APP_LOG_ERROR, 
			"[%s]--Web_MakePlcWebPage: "
			"ERROR: There is no enough memory! ", 
			__FILE__);

		SAFELY_DELETE(pszGCInfo);
		SAFELY_DELETE(pPSMode);

		return ERR_NO_MEMORY;
	}
	iReturnBufMaxLen = MAX_BUFFER;
	memset(pszReturnBuf,0,MAX_BUFFER);
	//output data format:
	//[
	//	"iPSModeIndex",
	//	"iPSModeName_en",
	//	[
	//		[
	//			"iOutSigIndex",
	//			"sOutSigName_en",
	//			"iInEquipId",
	//			"iInSigTypeId",
	//			"iInSigId",
	//			["sInEquipName_en", "sInEquipName_loc"],
	//			["sInSigName_en", "sInSigName_loc"]
	//		],
	//	],
	//	[
	//		[
	//			[
	//				"iEquipId",
	//				["sEquipName_en", "sEquipName_loc"]
	//			],
	//			[
	//				"iSampTypeId",
	//				[
	//					["iSampSigId1", ["sSampSigName_en", "sSampSigName_loc"]],
	//				]
	//			],
	//			[
	//				"iCtrlTypeId",
	//				[
	//					["iCtrlSigId1", ["sCtrlSigName_en", "sCtrlSigName_loc"]],
	//				]
	//			],
	//			[
	//				"iSetTypeId",
	//				[
	//					["iSetSigId1", ["sSetSigName_en", "sSetSigName_loc"]],
	//				]
	//			],
	//			[
	//				"iAlmTypeId",
	//				[
	//					["iAlmSigId1", ["sAlmSigName_en", "sAlmSigName_loc"]],
	//				]
	//			]
	//		],
	//	]
	//]

	//[
	//	"iPSModeIndex",
	//	"iPSModeName_en",
	iReturnBufLen += snprintf(pszReturnBuf + iReturnBufLen,
						MAX_SPRINTF_LINE_LEN,
						"%d,\"%s\",",
						iPSModeIndex,
						sPSModeList[iPSModeIndex]);

	//	[
	//		[
	//			"iOutSigIndex",
	//			"sOutSigName_en",
	//			"iInEquipId",
	//			"iInSigTypeId",
	//			"iInSigId",
	//			["sInEquipName_en", "sInEquipName_loc"],
	//			["sInSigName_en", "sInSigName_loc"]
	//		],
	//	],
	//	[
	iReturnBufLen += snprintf(pszReturnBuf + iReturnBufLen,
						MAX_SPRINTF_LINE_LEN*100,
						"%s[",
						pszGCInfo);



	for(i = 0; i < iEquipNum; i++)
	{
		//if( pEquipInfo->pStdEquip->iTypeID == 1101 ||
		//	pEquipInfo->pStdEquip->iTypeID == 1600 || pEquipInfo->pStdEquip->iTypeID == 1601 ||
		//	pEquipInfo->pStdEquip->iTypeID == 1700 || pEquipInfo->pStdEquip->iTypeID == 1701 ||
		//	pEquipInfo->pStdEquip->iTypeID == 1800 || pEquipInfo->pStdEquip->iTypeID == 1801 ||
		//	pEquipInfo->pStdEquip->iTypeID == 1900 || pEquipInfo->pStdEquip->iTypeID == 1901 ||
		//	pEquipInfo->pStdEquip->iTypeID == 1300 || pEquipInfo->pStdEquip->iTypeID == 1301 ||
		//	pEquipInfo->pStdEquip->iTypeID == 1400 || pEquipInfo->pStdEquip->iTypeID == 1401 ||
		//	pEquipInfo->pStdEquip->iTypeID == 2000 || pEquipInfo->pStdEquip->iTypeID == 2001 ||
		//	pEquipInfo->pStdEquip->iTypeID == 305 || pEquipInfo->pStdEquip->iTypeID == 306)
		if(pEquipInfo->pStdEquip->iTypeID == 100 || pEquipInfo->pStdEquip->iTypeID == 200 || 
			pEquipInfo->pStdEquip->iTypeID == 300 || 
			pEquipInfo->pStdEquip->iTypeID == 900 || pEquipInfo->pStdEquip->iTypeID == 901 || 
			pEquipInfo->pStdEquip->iTypeID == 800 || pEquipInfo->pStdEquip->iTypeID == 801)
		{
		}
		else
		{
			pEquipInfo++;
			continue;
		}

		//malloc enough memory
		if(iReturnBufMaxLen - iReturnBufLen < MAX_ONE_EQUIP_LEN)
		{
			iReturnBufMaxLen += MAX_BUFFER;
			 pszReturnBuf = RENEW(char, pszReturnBuf, iReturnBufMaxLen);
			 if(NULL == pszReturnBuf)
			 {
				 AppLogOut("WEB", APP_LOG_ERROR, 
					 "[%s]--Web_MakePlcWebPage: "
					 "ERROR: There is no enough memory! ", 
					 __FILE__);

				 SAFELY_DELETE(pszGCInfo);
				 SAFELY_DELETE(pPSMode);

				 return ERR_NO_MEMORY;
			 }
		}

		//start output a Equipment
		//		[
		//			[
		//				"iEquipId",
		//				["sEquipName_en", "sEquipName_loc"]
		//			],
		iReturnBufLen += snprintf(pszReturnBuf + iReturnBufLen,
						MAX_SPRINTF_LINE_LEN,
						"[[%d,[\"%s\",\"%s\"]],",
						pEquipInfo->iEquipID,
						//pEquipInfo->pEquipName->pFullName[0],
						(0 == iLanguage)? pEquipInfo->pEquipName->pFullName[0]: "",
						//pEquipInfo->pEquipName->pFullName[1]);
						(1 == iLanguage)? pEquipInfo->pEquipName->pFullName[1]: "");

		//			[
		//				"iSampTypeId",
		//				[
		iReturnBufLen += snprintf(pszReturnBuf + iReturnBufLen,
			MAX_SPRINTF_LINE_LEN,
			"[%d,[",
			SIG_TYPE_SAMPLING);

		iSamplingNum = Web_GetSampleSignalByEquipID(pEquipInfo->iEquipID, &pSampleSigValue);

		for(isigNum = 0; isigNum < iSamplingNum; isigNum++)
		{
			if(STD_SIG_IS_DISPLAY_ON_UI(pSampleSigValue->pStdSig,DISPLAY_WEB))
			{
				//					["iSampSigId1", ["sSampSigName_en", "sSampSigName_loc"]],
				iReturnBufLen += snprintf(pszReturnBuf + iReturnBufLen,
					MAX_SPRINTF_LINE_LEN,
					"[%d,[\"%s\",\"%s\"]],",
					pSampleSigValue->pStdSig->iSigID,
					//pSampleSigValue->pStdSig->pSigName->pFullName[0],
					(0 == iLanguage)? pSampleSigValue->pStdSig->pSigName->pFullName[0]: "",
					//pSampleSigValue->pStdSig->pSigName->pFullName[1]);
					(1 == iLanguage)? pSampleSigValue->pStdSig->pSigName->pFullName[1]: "");
			}

			pSampleSigValue++;
		}
		//delete the last ',', eg. convert '],' to ']'
		if(',' == pszReturnBuf[iReturnBufLen - 1])
		{
			pszReturnBuf[iReturnBufLen - 1] = '\0';
			iReturnBufLen--;
		}

		//				]
		//			],
		//			[
		//				"iCtrlTypeId",
		//				[
		iReturnBufLen += snprintf(pszReturnBuf + iReturnBufLen,
			MAX_SPRINTF_LINE_LEN,
			"]],[%d,[",
			SIG_TYPE_CONTROL);

		iControlNum  = Web_GetControlSignalByEquipID(pEquipInfo->iEquipID, &pControlSigValue);

		for(isigNum = 0; isigNum < iControlNum; isigNum++)
		{

			//					["iCtrlSigId1", ["sCtrlSigName_en", "sCtrlSigName_loc"]],
			iReturnBufLen += snprintf(pszReturnBuf + iReturnBufLen,
				MAX_SPRINTF_LINE_LEN,
				"[%d,[\"%s\",\"%s\"]],",
				pControlSigValue->pStdSig->iSigID,
				//pControlSigValue->pStdSig->pSigName->pFullName[0],
				(0 == iLanguage)? pControlSigValue->pStdSig->pSigName->pFullName[0]: "",
				//pControlSigValue->pStdSig->pSigName->pFullName[1]);
				(1 == iLanguage)? pControlSigValue->pStdSig->pSigName->pFullName[1]: "");

			pControlSigValue++;
		}

		//delete the last ',', eg. convert '],' to ']'
		if(',' == pszReturnBuf[iReturnBufLen - 1])
		{
			pszReturnBuf[iReturnBufLen - 1] = '\0';
			iReturnBufLen--;
		}

		//				]
		//			],
		//			[
		//				"iSetTypeId",
		//				[
		iReturnBufLen += snprintf(pszReturnBuf + iReturnBufLen,
			MAX_SPRINTF_LINE_LEN,
			"]],[%d,[",
			SIG_TYPE_SETTING);

		iSettingNum  = Web_GetSettingSignalByEquipID(pEquipInfo->iEquipID, &pSettingSigValue);

		for(isigNum = 0; isigNum < iSettingNum; isigNum++)
		{
			//					["iSetSigId1", ["sSetSigName_en", "sSetSigName_loc"]],
			iReturnBufLen += snprintf(pszReturnBuf + iReturnBufLen,
				MAX_SPRINTF_LINE_LEN,
				"[%d,[\"%s\",\"%s\"]],",
				pSettingSigValue->pStdSig->iSigID,
				//pSettingSigValue->pStdSig->pSigName->pFullName[0],
				(0 == iLanguage)? pSettingSigValue->pStdSig->pSigName->pFullName[0]: "",
				//pSettingSigValue->pStdSig->pSigName->pFullName[1]);
				(1 == iLanguage)? pSettingSigValue->pStdSig->pSigName->pFullName[1]: "");
			pSettingSigValue++;
		}

		//delete the last ',', eg. convert '],' to ']'
		if(',' == pszReturnBuf[iReturnBufLen - 1])
		{
			pszReturnBuf[iReturnBufLen - 1] = '\0';
			iReturnBufLen--;
		}

		//				]
		//			],
		//			[
		//				"iAlmTypeId",
		//				[
		iReturnBufLen += snprintf(pszReturnBuf + iReturnBufLen,
			MAX_SPRINTF_LINE_LEN,
			"]],[%d,[",
			SIG_TYPE_ALARM);


		iAlarmNum    = Web_GetAlarmSigStruofEquip(pEquipInfo->iEquipID, &pAlarmSigInfo);

		for(isigNum = 0; isigNum < iAlarmNum; isigNum++)
		{
			//					["iAlmSigId1", ["sAlmSigName_en", "sAlmSigName_loc"]],
			iReturnBufLen += snprintf(pszReturnBuf + iReturnBufLen,
				MAX_SPRINTF_LINE_LEN,
				"[%d,[\"%s\",\"%s\"]],",
				pAlarmSigInfo->iSigID,
				//pAlarmSigInfo->pSigName->pFullName[0],
				(0 == iLanguage)? pAlarmSigInfo->pSigName->pFullName[0]: "",
				//pAlarmSigInfo->pSigName->pFullName[1]);
				(1 == iLanguage)? pAlarmSigInfo->pSigName->pFullName[1]: "");

			pAlarmSigInfo++;
		}

		//delete the last ',', eg. convert '],' to ']'
		if(',' == pszReturnBuf[iReturnBufLen - 1])
		{
			pszReturnBuf[iReturnBufLen - 1] = '\0';
			iReturnBufLen--;
		}

		//				]
		//			]
		//		],
		iReturnBufLen += snprintf(pszReturnBuf + iReturnBufLen,
						MAX_SPRINTF_LINE_LEN,
						"]]],");

		pEquipInfo++;

	}

	//delete the last ',', eg. convert '],' to ']'
	if(',' == pszReturnBuf[iReturnBufLen - 1])
	{
		pszReturnBuf[iReturnBufLen - 1] = '\0';
		iReturnBufLen--;
	}

	//	]
	//]
	iReturnBufLen += snprintf(pszReturnBuf + iReturnBufLen,
		MAX_SPRINTF_LINE_LEN,
		"]\0");

	*ppszReturn = pszReturnBuf;

	//FILE *pf = fopen("/var/replaceStr.txt","a+");
	//fwrite(pResult,strlen(pResult),1,pf);
	//fclose(pf);

	SAFELY_DELETE(pszGCInfo);
	SAFELY_DELETE(pPSMode);

	TRACE("___Out Web_MakeGCWebPage___\n");
	return 1;

}

/*==========================================================================*
* FUNCTION :  Web_LoadGCConfigFile
* PURPOSE  :  
* CALLS    : 
* CALLED BY:   
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
static int Web_LoadGCConfigFile(char **ppszReturn)
{    
	char *szFullPath = WEB_GC_CFG_FILE_PATH;
	WEB_GC_CFG_INFO_LINE *temp = NULL;
	int i = 0;
	int iLen = 0;

	EQUIP_INFO	*pEquipInfo = NULL;
	SIG_BASIC_VALUE* pSigValue = NULL;
	SAMPLE_SIG_VALUE  *pSampleSigValue  = NULL;
	CTRL_SIG_VALUE    *pControlSigValue = NULL;
	SET_SIG_VALUE     *pSettingSigValue = NULL;
	ALARM_SIG_VALUE   *pAlarmSigValue   = NULL;
	char *pEquipName[2] = {NULL, NULL};
	char *pSigName[2] = {NULL, NULL};
	int iBufLen = 0;
	int iError = -1;
	int iTimeOut = 0;

	int iTmpEquipId = -1;
	int iTmpSigTypeId = -1;
	int iTmpSigId = -1;

	WEB_GC_CFC_INFO *stGCConfigInfo = NULL;
	stGCConfigInfo = NEW(WEB_GC_CFC_INFO,1);
	if (stGCConfigInfo == NULL)
	{
		AppLogOut("WEB", APP_LOG_ERROR, 
			"[%s]--Web_MakePlcConfigFile: "
			"ERROR: There is no enough memory! ", 
			__FILE__);
		return ERR_NO_MEMORY;
	}
	stGCConfigInfo->iNumber = 0;

	char *pszResult = NULL;
	pszResult = NEW(char, MAX_LINE_SIZE*100);
	memset(pszResult,0,MAX_LINE_SIZE*100);
	if (pszResult == NULL)
	{
		AppLogOut("WEB", APP_LOG_ERROR, 
			"[%s]--Web_MakeGCConfigFile: "
			"ERROR: There is no enough memory! ", 
			__FILE__);

		SAFELY_DELETE(stGCConfigInfo);

		return ERR_NO_MEMORY;
	}

	if (Mutex_Lock(GetMutexLoadGCConfig(), MAX_TIME_WAITING_LOAD_PLC_CONFIG) 
		== ERR_MUTEX_OK)
	{

		if(Cfg_LoadConfigFile(szFullPath,LoadGCConfigFile,stGCConfigInfo)!=0)
		{
			TRACEX("____Cfg_LoadConfigFile error!_______\n");
			Mutex_Unlock(GetMutexLoadGCConfig());
			TRACEX("____stGCConfigInfo->iNumber[%d]_______\n",stGCConfigInfo->iNumber);
			if(stGCConfigInfo->iNumber>0)
			{
				TRACEX("____stGCConfigInfo->iNumber[%d]_______\n",stGCConfigInfo->iNumber);
				TRACEX("____Cfg_LoadConfigFile error2!_______\n");
				temp = stGCConfigInfo->stWebGCConfigLine;
				for(i = 0; i < stGCConfigInfo->iNumber; i++)
				{
					TRACEX("____Cfg_LoadConfigFile error3!_______\n");
					SAFELY_DELETE(temp->pszPSSigName);
					SAFELY_DELETE(temp->pszEquipId);
					SAFELY_DELETE(temp->pszSigType);
					SAFELY_DELETE(temp->pszSigId);


					temp++; 
				}

				SAFELY_DELETE(stGCConfigInfo->stWebGCConfigLine);
			}
			SAFELY_DELETE(stGCConfigInfo);
			return -1;
		}

		Mutex_Unlock(GetMutexLoadGCConfig());

	}

	//TRACE("Web_LoadPlcConfigFile OK!!!\n");
	//pszResult[iLen++] = ';';
	//output format:
	//[
	//	[
	//		"iOutSigIndex",
	//		"sOutSigName_en",
	//		"iInEquipId",
	//		"iInSigTypeId",
	//		"iInSigId",
	//		["sInEquipName_en", "sInEquipName_loc"],
	//		["sInSigName_en", "sInSigName_loc"]
	//	],
	//],

	//[
	iLen += snprintf(pszResult + iLen,
					MAX_SPRINTF_LINE_LEN,
					"[");

	temp = stGCConfigInfo->stWebGCConfigLine;

	if(stGCConfigInfo->iNumber > 0)
	{
		for(i = 0; i < stGCConfigInfo->iNumber; i++)
		{
			if((stricmp(temp->pszEquipId, "NA") == 0)
				|| (stricmp(temp->pszSigType, "NA") == 0)
				|| (stricmp(temp->pszSigId, "NA") == 0))
			{
				iTmpEquipId = -1;
				iTmpSigTypeId = -1;
				iTmpSigId = -1;
			}
			else
			{
				iTmpEquipId = atoi(temp->pszEquipId);
				iTmpSigTypeId = atoi(temp->pszSigType);
				iTmpSigId = atoi(temp->pszSigId);
			}

			TRACE("name=%s, iTmpEquipId=%s, iTmpSigTypeId=%s, iTmpSigId=%s\n",
					temp->pszPSSigName,
					temp->pszEquipId,
					temp->pszSigType,
					temp->pszSigId);

			pEquipName[0] = NULL;
			pEquipName[1] = NULL;
			pSigName[0] = NULL;
			pSigName[1] = NULL;

			iError = DxiGetData(VAR_A_EQUIP_INFO,
					iTmpEquipId,
					0,
					&iBufLen,
					&pEquipInfo,
					0);
			if(iError == ERR_DXI_OK)
			{

				iError = DxiGetData(VAR_A_SIGNAL_VALUE,
						iTmpEquipId,
						DXI_MERGE_SIG_ID(iTmpSigTypeId, iTmpSigId),
						&iBufLen,
						(void *)&pSigValue,
						iTimeOut);
				if(iError == ERR_DXI_OK)
				{
					pEquipName[0] = pEquipInfo->pEquipName->pFullName[0];
					pEquipName[1] = pEquipInfo->pEquipName->pFullName[1];

					if(SIG_TYPE_SAMPLING == iTmpSigTypeId)
					{
						pSampleSigValue = (SAMPLE_SIG_VALUE *)pSigValue;
						pSigName[0] = pSampleSigValue->pStdSig->pSigName->pFullName[0];
						pSigName[1] = pSampleSigValue->pStdSig->pSigName->pFullName[1];

					}
					else if(SIG_TYPE_CONTROL == iTmpSigTypeId)
					{
						pControlSigValue = (CTRL_SIG_VALUE *)pSigValue;
						pSigName[0] = pControlSigValue->pStdSig->pSigName->pFullName[0];
						pSigName[1] = pControlSigValue->pStdSig->pSigName->pFullName[1];
					}
					else if(SIG_TYPE_SETTING == iTmpSigTypeId)
					{
						pSettingSigValue = (SET_SIG_VALUE *)pSigValue;
						pSigName[0] = pSettingSigValue->pStdSig->pSigName->pFullName[0];
						pSigName[1] = pSettingSigValue->pStdSig->pSigName->pFullName[1];
					}
					else if(SIG_TYPE_ALARM == iTmpSigTypeId)
					{
						pAlarmSigValue = (ALARM_SIG_VALUE *)pSigValue;
						pSigName[0] = pAlarmSigValue->pStdSig->pSigName->pFullName[0];
						pSigName[1] = pAlarmSigValue->pStdSig->pSigName->pFullName[1];
					}
					else
					{
						pSampleSigValue = (SAMPLE_SIG_VALUE *)pSigValue;
						pSigName[0] = pSampleSigValue->pStdSig->pSigName->pFullName[0];
						pSigName[1] = pSampleSigValue->pStdSig->pSigName->pFullName[1];
					}
				}

			}

			//	[
			//		"iOutSigIndex",
			//		"sOutSigName_en",
			//		"iInEquipId",
			//		"iInSigTypeId",
			//		"iInSigId",
			//		["sInEquipName_en", "sInEquipName_loc"],
			//		["sInSigName_en", "sInSigName_loc"]
			//	],
			iLen += snprintf(pszResult + iLen,
				MAX_SPRINTF_LINE_LEN,
				"[%d,\"%s\",%d,%d,%d,[\"%s\",\"%s\"],[\"%s\",\"%s\"]],",
				i,
				temp->pszPSSigName,
				iTmpEquipId,
				iTmpSigTypeId,
				iTmpSigId,
				(NULL == pEquipName[0])? "NA": pEquipName[0],
				(NULL == pEquipName[1])? "NA": pEquipName[1],
				(NULL == pSigName[0])? "NA": pSigName[0],
				(NULL == pSigName[1])? "NA": pSigName[1]);

			temp++;
		}
	}

	//delete the last ',', eg. convert '],' to ']'
	if(',' == pszResult[iLen - 1])
	{
		pszResult[iLen - 1] = '\0';
		iLen--;
	}

	//],
	iLen += snprintf(pszResult + iLen,
					MAX_SPRINTF_LINE_LEN,
					"],\0");

	*ppszReturn = pszResult;

	/*FILE *pf = NULL;
	pf = fopen("/var/wj1.txt","w+");
	fwrite(pszResult,strlen(pszResult),1,pf);
	fclose(pf);*/


	temp = stGCConfigInfo->stWebGCConfigLine;
	for(i = 0; i < stGCConfigInfo->iNumber; i++)
	{
		SAFELY_DELETE(temp->pszPSSigName);
		SAFELY_DELETE(temp->pszEquipId);
		SAFELY_DELETE(temp->pszSigType);
		SAFELY_DELETE(temp->pszSigId);


		temp++; 
	}

	SAFELY_DELETE(stGCConfigInfo->stWebGCConfigLine);
	SAFELY_DELETE(stGCConfigInfo);

	return iLen+1;

}


/*==========================================================================*
* FUNCTION :  LoadGCConfigFile
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
static int LoadGCConfigFile(IN void *pCfg, OUT void *pLoadToBuf)
{
	//TRACE("Into LoadPlcConfigFile!!!\n");
	CONFIG_TABLE_LOADER loader[1];
	WEB_GC_CFC_INFO *pBuf = NULL;
	pBuf = (WEB_GC_CFC_INFO *)pLoadToBuf;



	DEF_LOADER_ITEM(&loader[0],
		NULL,
		&(pBuf->iNumber), 
		GC_PS_INFO,
		&(pBuf->stWebGCConfigLine), 
		ParseGCTableProc);

	if(Cfg_LoadTables(pCfg, 1, loader)!= ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	//TRACE("LoadPlcConfigFile OK!!!\n");

	return ERR_CFG_OK;

}
/*==========================================================================*
* FUNCTION :  ParsePLCTableProc
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
static int ParseGCTableProc(IN char *szBuf, OUT WEB_GC_CFG_INFO_LINE *pStructData)
{
	//TRACE("Into ParseGCTableProc!!!\n");
	//#InputFromMaster	EquipmentId	SignalType	SignalId
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);


	//TRACE("INTO ParsePLCTableProc!!!!\n");

	/* 1.InputFromMaster field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField != NULL)
	{
		pStructData->pszPSSigName = NEW_strdup(pField);
	}
	else
	{
		SAFELY_DELETE(pStructData->pszPSSigName);
		return 1;
	}
	//TRACE("Operator field OK!!!%s\n", pStructData->pszOperator);
	/* 2.EquipmentId field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField != NULL)
	{
		pStructData->pszEquipId = NEW_strdup(pField);
	}
	else
	{
		SAFELY_DELETE(pStructData->pszPSSigName);
		SAFELY_DELETE(pStructData->pszEquipId);
		return 2;
	}
	//TRACE("Input1 field OK!!!%s\n",pStructData->pszInput1);
	/* 3.SignalType field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField != NULL)
	{
		pStructData->pszSigType = NEW_strdup(pField);
	}
	else
	{
		SAFELY_DELETE(pStructData->pszPSSigName);
		SAFELY_DELETE(pStructData->pszEquipId);
		SAFELY_DELETE(pStructData->pszSigType);
		return 3;
	}
	//TRACE("Input2 field OK!!!%s\n",pStructData->pszInput2);
	/* 4.SignalId field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField != NULL)
	{
		pStructData->pszSigId = NEW_strdup(pField);
	}
	else
	{
		SAFELY_DELETE(pStructData->pszPSSigName);
		SAFELY_DELETE(pStructData->pszEquipId);
		SAFELY_DELETE(pStructData->pszSigType);
		SAFELY_DELETE(pStructData->pszSigId);
		return 4;
	}


	//TRACE("ParsePLCTableProc OK!!!\n");

	return 0;



}
/*==========================================================================*
* FUNCTION :  GetMutexLoadPLCConfig
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/

HANDLE GetMutexLoadGCConfig(void)
{
	return s_hMutexLoadGCCFGFile;
}

/*==========================================================================*
* FUNCTION :  Web_ModifyGCPSMode
* PURPOSE  :  
* CALLS    : 
* CALLED BY: Web_SendConfigureData  
* ARGUMENTS:  
* RETURN   : int:6 for success!
* COMMENTS : new function,added for CR# 0132-06-ACU
* CREATOR  : Wang Jing              DATE: 2006-11-23 11:08
*==========================================================================*/
static int Web_ModifyGCPSMode(IN char *pszMode)
{
	TRACE("________INTO__Web_ModifyGCPSMode______\n");
	TRACE("pszMode=%s\n", pszMode);

#define PSMODE_MAX_READ_LINE_LEN			256

	char			*pSearchValue = NULL, *str_string = NULL, szReadString[PSMODE_MAX_READ_LINE_LEN];
	FILE			*fp;
	int flage = 0;

	if (Mutex_Lock(GetMutexLoadGCConfig(), MAX_TIME_WAITING_LOAD_PLC_CONFIG) 
		== ERR_MUTEX_OK)
	{
		if((fp = fopen(WEB_GC_CFG_FILE_PATH, "rb+")) != NULL)
		{
			while(fgets(szReadString, PSMODE_MAX_READ_LINE_LEN, fp) != NULL)
			{
				if((pSearchValue = strstr(szReadString,GC_PS_MODE)) != NULL)
				{
					while( flage == 0)
					{
						if(fgets(szReadString, PSMODE_MAX_READ_LINE_LEN, fp) != NULL)
						{
							if(szReadString[0] != '#')
							{
								fseek(fp,(long)-strlen(szReadString),SEEK_CUR);
								if(pszMode != NULL)
								{
									str_string = NEW(char , strlen(szReadString) + 1);
									memset(str_string,0, strlen(szReadString) + 1);
									sprintf(str_string,"%s ",Cfg_RemoveWhiteSpace(pszMode));
									//TRACE("str_string[%s]\n", str_string);
									//strncpyz(str_string,Cfg_RemoveWhiteSpace(szLanguage),strlen(szReadString) + 1);
									fputs(str_string, fp);

									DELETE(str_string);
									str_string = NULL;
								}

								flage = 1;
							}

						}
					}

				}

			}
			fclose(fp);
		}
		else
		{
			Mutex_Unlock(GetMutexLoadGCConfig());
			return  4;
		}
		Mutex_Unlock(GetMutexLoadGCConfig());
	}
	else
	{
		//Mutex_Unlock(GetMutexLoadGCConfig());
		return  5;
	}

	TRACE("______Web_ModifyGCPSMode__OK!!!______\n");
	return 6;
}
/*==========================================================================*
* FUNCTION :  Web_ModifyGCPSInfo
* PURPOSE  :  
* CALLS    : 
* CALLED BY: Web_SendConfigureData  
* ARGUMENTS:  
* RETURN   : int:6 for success!
* COMMENTS : new function,added for CR# 0132-06-ACU
* CREATOR  : Wang Jing              DATE: 2006-11-23 11:08
*==========================================================================*/

static int Web_ModifyGCPSInfo(IN char *pszGCPSInfo)
{ 

	FILE *pFile = NULL;
	long lFileLen;
	long lCurrPos;
	char *pszFile = NULL;
	char *pszOutFile = NULL;
	void *pProf = NULL;
	int flage = 0;
	char pLineBuf[MAX_LINE_SIZE];
	//char *pPosition = NULL;
	//int iIDBufLen = 0;
	int iLen = 0;



	WEB_GC_CFG_INFO_LINE *pszINGCPSInfo;
	pszINGCPSInfo = NEW(WEB_GC_CFG_INFO_LINE,1);
	if (pszINGCPSInfo == NULL)
	{
		AppLogOut("WEB", APP_LOG_ERROR, 
			"[%s]--Web_Web_ModifyGCPSInfo: "
			"ERROR: There is no enough memory! ", 
			__FILE__);

		return 5;
	}

	if(ParseGCTableProc(pszGCPSInfo, pszINGCPSInfo)!=0)
	{
		SAFELY_DELETE(pszINGCPSInfo);
		return 5;
	}

	TRACE("pszINGCPSInfo:pszPSSigName=%s,pszEquipId=%s,pszSigType=%s,pszSigId=%s,\n", 
		pszINGCPSInfo->pszPSSigName,
		pszINGCPSInfo->pszEquipId,
		pszINGCPSInfo->pszSigType,
		pszINGCPSInfo->pszSigId);

	WEB_GC_CFG_INFO_LINE *pszTempGCPSInfo;
	pszTempGCPSInfo = NEW(WEB_GC_CFG_INFO_LINE,1);
	if (pszTempGCPSInfo == NULL)
	{
		AppLogOut("WEB", APP_LOG_ERROR, 
			"[%s]--Web_Web_ModifyGCPSInfo: "
			"ERROR: There is no enough memory! ", 
			__FILE__);

		SAFELY_DELETE(pszINGCPSInfo->pszPSSigName);
		SAFELY_DELETE(pszINGCPSInfo->pszEquipId);
		SAFELY_DELETE(pszINGCPSInfo->pszSigType);
		SAFELY_DELETE(pszINGCPSInfo->pszSigId);
		SAFELY_DELETE(pszINGCPSInfo);

		return 5;
	}


	if (Mutex_Lock(GetMutexLoadGCConfig(), MAX_TIME_WAITING_LOAD_PLC_CONFIG) 
		== ERR_MUTEX_OK)
	{
		pFile = fopen(WEB_GC_CFG_FILE_PATH,"r+");
		if(pFile == NULL)
		{
			Mutex_Unlock(GetMutexLoadGCConfig());

			SAFELY_DELETE(pszINGCPSInfo->pszPSSigName);
			SAFELY_DELETE(pszINGCPSInfo->pszEquipId);
			SAFELY_DELETE(pszINGCPSInfo->pszSigType);
			SAFELY_DELETE(pszINGCPSInfo->pszSigId);
			SAFELY_DELETE(pszINGCPSInfo);

			SAFELY_DELETE(pszTempGCPSInfo);

			return 5;
		}

		lFileLen = GetFileLength(pFile);
		TRACE("FileLen : %ld\n",lFileLen);

		pszFile = NEW(char, lFileLen + 1);
		if (pszFile == NULL)
		{
			AppLogOut("WEB", APP_LOG_ERROR, 
				"[%s]--Web_SetAlarmSuppressingExp: "
				"ERROR: There is no enough memory! ", 
				__FILE__);
			fclose(pFile);
			Mutex_Unlock(GetMutexLoadGCConfig());

			SAFELY_DELETE(pszINGCPSInfo->pszPSSigName);
			SAFELY_DELETE(pszINGCPSInfo->pszEquipId);
			SAFELY_DELETE(pszINGCPSInfo->pszSigType);
			SAFELY_DELETE(pszINGCPSInfo->pszSigId);
			SAFELY_DELETE(pszINGCPSInfo);
			SAFELY_DELETE(pszTempGCPSInfo);

			return 5;
		}
		memset(pszFile, 0, lFileLen + 1);

		pszOutFile = NEW(char, lFileLen + 512);//MAX_LINE_SIZE=512
		if (pszOutFile == NULL)
		{
			AppLogOut("WEB", APP_LOG_ERROR, 
				"[%s]--Web_SetAlarmSuppressingExp: "
				"ERROR: There is no enough memory! ", 
				__FILE__);
			fclose(pFile);
			Mutex_Unlock(GetMutexLoadGCConfig());

			SAFELY_DELETE(pszINGCPSInfo->pszPSSigName);
			SAFELY_DELETE(pszINGCPSInfo->pszEquipId);
			SAFELY_DELETE(pszINGCPSInfo->pszSigType);
			SAFELY_DELETE(pszINGCPSInfo->pszSigId);
			SAFELY_DELETE(pszINGCPSInfo);
			SAFELY_DELETE(pszTempGCPSInfo);
			SAFELY_DELETE(pszFile);

			return 5;
		}
		memset(pszOutFile, 0, lFileLen + 1);

		fread(pszFile, 1, (size_t)lFileLen, pFile);
		fclose(pFile);

		pProf = Cfg_ProfileOpen(pszFile,lFileLen);
		if(Cfg_ProfileFindSection(pProf,GC_PS_INFO) == 0)
		{
			AppLogOut("WEB", APP_LOG_ERROR, 
				"[%s]--Web_SetAlarmSuppressingExp: "
				"ERROR: Can't find Section! ", 
				__FILE__);      
			DELETE(pProf);
			Mutex_Unlock(GetMutexLoadGCConfig());

			SAFELY_DELETE(pszINGCPSInfo->pszPSSigName);
			SAFELY_DELETE(pszINGCPSInfo->pszEquipId);
			SAFELY_DELETE(pszINGCPSInfo->pszSigType);
			SAFELY_DELETE(pszINGCPSInfo->pszSigId);
			SAFELY_DELETE(pszINGCPSInfo);
			SAFELY_DELETE(pszTempGCPSInfo);
			SAFELY_DELETE(pszFile);
			SAFELY_DELETE(pszOutFile);

			return 5;
		}

		lCurrPos = Cfg_ProfileTell(pProf);
		TRACE("lCurrPos : %ld\n",lCurrPos);

		memmove(pszOutFile, pszFile, lCurrPos);

		char *pLineTemp = NULL;

		while(Cfg_ProfileReadLine(pProf, pLineBuf, MAX_LINE_SIZE) != 0)
		{
			//TRACE("\n1________%s__________\n",pLineBuf);
			if(pLineBuf[0] == '#' && flage != 1)
			{
				iLen += sprintf(pszOutFile + lCurrPos + iLen,"\r\n%s", pLineBuf);
				memset(pLineBuf,0,MAX_LINE_SIZE);
			}
			else if(pLineBuf[0] != '#' && flage != 1 && pLineBuf[0] != '[')
			{
				pLineTemp = NEW_strdup(pLineBuf);
				if(ParseGCTableProc(pLineTemp,pszTempGCPSInfo)!=0)
				{
					iLen += sprintf(pszOutFile + lCurrPos + iLen,"\r\n%s",pLineBuf);
					memset(pLineBuf,0,MAX_LINE_SIZE);

					SAFELY_DELETE(pLineTemp);
					continue;
				}

				if(!strcmp(pszINGCPSInfo->pszPSSigName,pszTempGCPSInfo->pszPSSigName))
				{

					iLen += sprintf(pszOutFile + lCurrPos + iLen,"\r\n%s\t\t%s\t\t%s\t\t%s", 
						pszINGCPSInfo->pszPSSigName,
						pszINGCPSInfo->pszEquipId,
						pszINGCPSInfo->pszSigType,
						pszINGCPSInfo->pszSigId);


					//TRACE("2\n________%s__________\n",pszGCPSInfo);
					memset(pLineBuf,0,MAX_LINE_SIZE);

				}
				else
				{
					iLen += sprintf(pszOutFile + lCurrPos + iLen,"\r\n%s\t\t%s\t\t%s\t\t%s", 
						pszTempGCPSInfo->pszPSSigName,
						pszTempGCPSInfo->pszEquipId,
						pszTempGCPSInfo->pszSigType,
						pszTempGCPSInfo->pszSigId);
					//TRACE("3\n________%s__________\n",pLineBuf);
					memset(pLineBuf,0,MAX_LINE_SIZE);
				}

				TRACE("pszTempGCPSInfo:pszPSSigName=%s,pszEquipId=%s,pszSigType=%s,pszSigId=%s,\n", 
					pszTempGCPSInfo->pszPSSigName,
					pszTempGCPSInfo->pszEquipId,
					pszTempGCPSInfo->pszSigType,
					pszTempGCPSInfo->pszSigId);

				SAFELY_DELETE(pszTempGCPSInfo->pszEquipId);
				SAFELY_DELETE(pszTempGCPSInfo->pszPSSigName);
				SAFELY_DELETE(pszTempGCPSInfo->pszSigId);
				SAFELY_DELETE(pszTempGCPSInfo->pszSigType);
				SAFELY_DELETE(pLineTemp);

			}
			else if(pLineBuf[0] == '[' && flage != 1 )
			{
				iLen += sprintf(pszOutFile + lCurrPos + iLen,"\r\n%s", pLineBuf);
				memset(pLineBuf,0,MAX_LINE_SIZE);
				flage = 1;
			}
			else if(flage == 1 )
			{
				iLen += sprintf(pszOutFile + lCurrPos + iLen,"\r\n%s", pLineBuf);
				memset(pLineBuf,0,MAX_LINE_SIZE);
			}


		}

		pFile = fopen(WEB_GC_CFG_FILE_PATH,"w+");
		fwrite(pszOutFile,strlen(pszOutFile),1,pFile);
		fclose(pFile);

		SAFELY_DELETE(pszINGCPSInfo->pszEquipId);
		SAFELY_DELETE(pszINGCPSInfo->pszPSSigName);
		SAFELY_DELETE(pszINGCPSInfo->pszSigId);
		SAFELY_DELETE(pszINGCPSInfo->pszSigType);
		SAFELY_DELETE(pszINGCPSInfo);
		SAFELY_DELETE(pszTempGCPSInfo);


		SAFELY_DELETE(pszFile);
		pszFile = NULL;
		SAFELY_DELETE(pszOutFile);
		pszOutFile = NULL;
		SAFELY_DELETE(pProf);

		Mutex_Unlock(GetMutexLoadGCConfig());
	}
	TRACE("___Web_ModifyGCPSInfo OK___\n");
	return 6;

}

/*==========================================================================*
* FUNCTION :  Web_GetGCPSMode
* PURPOSE  :  
* CALLS    : 
* CALLED BY: Web_SendConfigureData  
* ARGUMENTS:  
* RETURN   : int:6 for success!
* COMMENTS : new function,added for CR# 0132-06-ACU
* CREATOR  : Wang Jing              DATE: 2006-11-23 11:08
*==========================================================================*/
static int Web_GetGCPSMode(char **ppszGCPSMode)
{
	TRACE(" ______into Web_GetGCPSMode________------\n");
	int		iRst;
	void	*pProf = NULL;  
	FILE	*pFile = NULL;
	char	*szInFile = NULL;
	size_t	ulFileLen;



	char *szCfgFileName = WEB_GC_CFG_FILE_PATH; 

	//Cfg_GetFullConfigPath(CONFIG_FILE_WEB_PRIVATE_R, szCfgFileName, MAX_FILE_PATH);
	if (Mutex_Lock(GetMutexLoadGCConfig(), MAX_TIME_WAITING_LOAD_PLC_CONFIG) 
		== ERR_MUTEX_OK)
	{

		pFile = fopen(szCfgFileName, "r");
		if (pFile == NULL)
		{		
			Mutex_Unlock(GetMutexLoadGCConfig());
			return FALSE;

		}

		ulFileLen = GetFileLength(pFile);

		szInFile = NEW(char, ulFileLen + 1);
		if (szInFile == NULL)
		{
			Mutex_Unlock(GetMutexLoadGCConfig());
			fclose(pFile);
			return FALSE;
		}

		/* read file */
		ulFileLen = fread(szInFile, sizeof(char), ulFileLen, pFile);
		fclose(pFile);

		if (ulFileLen == 0) 
		{
			/* clear the memory */
			Mutex_Unlock(GetMutexLoadGCConfig());
			DELETE(szInFile);
			return FALSE;
		}
		szInFile[ulFileLen] = '\0';  /* end with NULL */

		/* create SProfile */
		pProf = Cfg_ProfileOpen(szInFile, (int)ulFileLen);

		if (pProf == NULL)
		{
			Mutex_Unlock(GetMutexLoadGCConfig());
			DELETE(szInFile);
			return FALSE;
		}
		char	*pszMode = NEW(char,10);
		pszMode[0] = ';';
		//1.Read Local language version
		char   *ptemp = pszMode + 1;
		iRst = Cfg_ProfileGetString(pProf,
			GC_PS_MODE, 
			ptemp,
			9); 
		*ppszGCPSMode = pszMode;

		TRACE(" ______into Web_GetGCPSMode________2[%s]------\n",*ppszGCPSMode);

		//TRACE("Successfully to  Cfg_ProfileGetInt  %d\n", iPagesNumber);
		if (iRst != 1)
		{
			AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
				"There are no GCPSMode in gen_ctl.cfg.");
			Mutex_Unlock(GetMutexLoadGCConfig());
			DELETE(pProf); 
			DELETE(szInFile);
			return FALSE;
		}

		Mutex_Unlock(GetMutexLoadGCConfig());

	}

	DELETE(pProf); 
	DELETE(szInFile);
	TRACE(" ______into Web_GetGCPSMode________OK!!!------\n");
	return TRUE;

}


/*==========================================================================*
* FUNCTION :  Web_GetSetParamNum
* PURPOSE  :  
* CALLS    : 
* CALLED BY: Web_MakeGetSetParamWebPage  
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : new function,added for TR# 62-ACU
* CREATOR  : Wang Jing              DATE: 2006-11-23 11:08
*==========================================================================*/
static int Web_GetSetParamNum(void)
{
    int iAllSetSigNum = 0;
    EQUIP_INFO		*pEquipInfo = NULL;
    int iEquipNumber = 0;
    int i = 0;

    if((iEquipNumber = Web_GetEquipListFromInterface(&pEquipInfo)) <= 0)
    {
	return FALSE;
    }

    for ( i = 0; i < iEquipNumber && pEquipInfo != NULL; i++, pEquipInfo++)
    {
	iAllSetSigNum = iAllSetSigNum + pEquipInfo->pStdEquip->iSetSigNum;
    }

    return iAllSetSigNum;
}
static int RemoveAdminInfo(int iUserNumber, USER_INFO_STRU *pTUserInfo)
{
	int nFind = -1;
	int i;
	USER_INFO_STRU *pUserInfo = pTUserInfo;	

	for (i = 0; i < iUserNumber;i++,pUserInfo++)
	{
		if (strcasecmp(pUserInfo->szUserName, SUPER_ADMIN_NAME) == 0)
		{
			nFind = i;
			break;
		}
	}
	
	if (nFind >= 0)
	{
		pUserInfo = pTUserInfo;
		memmove((pUserInfo + nFind), 
			(pUserInfo + nFind + 1), 
			sizeof(USER_INFO_STRU)* (iUserNumber - nFind - 1));

		memset((pUserInfo + iUserNumber - 1), 0,
			sizeof(USER_INFO_STRU));	

		return iUserNumber -1;
	}

	return iUserNumber;

}
/*==========================================================================*
* FUNCTION :  Web_MakeGetSetParamWebPage
* PURPOSE  :  
* CALLS    : 
* CALLED BY: Web_SendConfigureData  
* ARGUMENTS: readable_param : if 1; SettingParam.run is also to be made human readable(text). // CR - Log/settings fetching  
* RETURN   : int:1 for success!
* COMMENTS : new function,added for TR# 62-ACU
* CREATOR  : Wang Jing              DATE: 2006-11-23 11:08
*==========================================================================*/
static int Web_MakeGetSetParamWebPage(int readable_param)
{
//#define WEB_SETTINGPARAM_FILE_NAME			"/var/SettingParam.run"
#define WEB_SETTINGPARAM_FILE_NAME			"/app/config/run/SettingParam.run"
#define WEB_SETTINGPARAM_TEXT				"/var/download/SettingParam.txt"
    CFG_PARAM_RUN_INFO  *pCFGParam = NULL;

    EQUIP_INFO		*pEquipInfo = NULL;
    SET_SIG_VALUE *pTempSetSigVal = NULL;
    FILE *pParamFile = NULL;
    int iEquipNumber = 0;
    int iSetSigNum = 0;
    WORD dwSum = 0;
    WORD dwUserSum = 0;
    int iparamNum = 0;
    int i = 0;
    int j = 0;
    int iParamNo = -1;
    int sigType = 2;

    FILE *fp = NULL;
    char szParamValStr[50], szTime[30];
    struct tm *pTimeInfo;

    iparamNum = Web_GetSetParamNum();
    if(iparamNum == 0)
    {
	return  2;
    }
    pCFGParam = NEW(CFG_PARAM_RUN_INFO,1);
    if(pCFGParam == NULL)
    {
	return  2;
    }

    strcpy(pCFGParam->sFileHeadInfo,PARAM_FILE_HEAD_INFO);
    pCFGParam->iVersion = VER_PARAM_RECORD;
    pCFGParam->pParam = NEW(PERSISTENT_SIG_RECORD,iparamNum);
    if(pCFGParam->pParam == NULL)
    {
	DELETE(pCFGParam);
	return  2;

    }

    if((iEquipNumber = Web_GetEquipListFromInterface(&pEquipInfo)) <= 0)
    {
	return 2;
    }

   	if(readable_param == 1)
    {
		fp = fopen(WEB_SETTINGPARAM_TEXT, "w");
		if(fp != NULL)
		{
			fprintf(fp, "File Head Info:%s\n", pCFGParam->sFileHeadInfo);
			fprintf(fp, "Version       :%d\n\n", pCFGParam->iVersion);
			//print parameters headers
			fprintf(fp, "%16s%16s%16s%32s%32s\n", "EquipmentID", "SignalType", "SignalID", "SignalValue", "SignalTime");
		}
		else
		  {
	    	readable_param = 0;
	  	}
    }

    for ( i = 0; i < iEquipNumber && pEquipInfo != NULL; i++, pEquipInfo++)
    {
	iSetSigNum = pEquipInfo->pStdEquip->iSetSigNum;
	pTempSetSigVal = pEquipInfo->pSetSigValue;
	for(j = 0;j < iSetSigNum; pTempSetSigVal++, j++)
	{
	    if((pTempSetSigVal->pStdSig->bPersistentFlag) &&
		(pTempSetSigVal->bv.tmCurrentSampled != 0))
	    {
		iParamNo++;
		pCFGParam->pParam[iParamNo].iEquipID = pEquipInfo->iEquipID;
		pCFGParam->pParam[iParamNo].iSignalID = DXI_MERGE_SIG_ID(sigType,pTempSetSigVal->pStdSig->iSigID);

		switch(pTempSetSigVal->pStdSig->iSigValueType)
		{
			case VAR_LONG:
				pCFGParam->pParam[iParamNo].varSignalVal.lValue = pTempSetSigVal->bv.varValue.lValue;
				sprintf(szParamValStr, "%ld", pTempSetSigVal->bv.varValue.lValue);
				break;
			case VAR_FLOAT:
				pCFGParam->pParam[iParamNo].varSignalVal.fValue = pTempSetSigVal->bv.varValue.fValue;
				sprintf(szParamValStr, "%f", pTempSetSigVal->bv.varValue.fValue);
				break;
			case VAR_UNSIGNED_LONG:
				pCFGParam->pParam[iParamNo].varSignalVal.ulValue = pTempSetSigVal->bv.varValue.ulValue;
				sprintf(szParamValStr, "%lu", pTempSetSigVal->bv.varValue.ulValue);
				break;
			case VAR_DATE_TIME:
				pCFGParam->pParam[iParamNo].varSignalVal.dtValue = pTempSetSigVal->bv.varValue.dtValue;
				sprintf(szParamValStr, "%ld", pTempSetSigVal->bv.varValue.dtValue);
				break;
			case VAR_ENUM:
				pCFGParam->pParam[iParamNo].varSignalVal.enumValue = pTempSetSigVal->bv.varValue.enumValue;
				sprintf(szParamValStr, "%ld", pTempSetSigVal->bv.varValue.enumValue);
				break;
			}

			pCFGParam->pParam[iParamNo].tmSignalTime = pTempSetSigVal->bv.tmCurrentSampled;

			if(readable_param == 1)
			{
				pTimeInfo = localtime(&pCFGParam->pParam[iParamNo].tmSignalTime);
				sprintf(szTime, "%02d/%02d/%02d %02d:%02d:%02d", pTimeInfo->tm_year - 100, pTimeInfo->tm_mon + 1, pTimeInfo->tm_mday, pTimeInfo->tm_hour, pTimeInfo->tm_min, pTimeInfo->tm_sec);
				fprintf(fp, "%16d%16d%16d%32s%32s\n", pEquipInfo->iEquipID, sigType, pTempSetSigVal->pStdSig->iSigID, szParamValStr, szTime);
			}
	    }


	}

    }
    pCFGParam->iUserNum = GetUserInfoForWeb(&(pCFGParam->pUserInfo));
    pCFGParam->iUserNum = RemoveAdminInfo(pCFGParam->iUserNum, pCFGParam->pUserInfo);



    APP_SERVICE		*stAppService = NULL;
    int				iNMSNum = 0;
    int			iBufLen = 0;

    if(g_SiteInfo.iSNMPV3Flag == NO_SNMPV3)
    {
	stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,(void *)GET_SERVICE_OF_NMSV2_NAME);
    }
    else
    {
	stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,(void *)GET_SERVICE_OF_NMSV3_NAME);
    }	

    if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
    {
	if(stAppService->pfnServiceConfig(stAppService->hServiceThread,
	    stAppService->bServiceRunning,
	    &stAppService->args, 
	    GET_NMS_USER_INFO,
	    &iBufLen,
	    (void *)&pCFGParam->pNMSInfo) == ERR_SNP_OK)
	{			
	    pCFGParam->iNMSNum =  iBufLen /sizeof(NMS_INFO);
	}
    }
    else
    {
	pCFGParam->iNMSNum = 0;
	pCFGParam->pNMSInfo = NULL;
    }

    if(g_SiteInfo.iSNMPV3Flag == NO_SNMPV3)
    {
	pCFGParam->iNMS3Num = 0;
	pCFGParam->pNMS3Info = NULL;
	stAppService =  NULL;
    }
    else
    {
	stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB, (void *)GET_SERVICE_OF_NMSV3_NAME);
    }



    if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
    {
	if(stAppService->pfnServiceConfig(stAppService->hServiceThread,
	    stAppService->bServiceRunning,
	    &stAppService->args, 
	    GET_NMS_V3_USER_INFO,
	    &iBufLen,
	    (void *)&pCFGParam->pNMS3Info) == ERR_SNP_OK)
	{			
	    pCFGParam->iNMS3Num =  iBufLen /sizeof(V3NMS_INFO);	

	}
    }
    else
    {
	pCFGParam->iNMS3Num = 0;
	pCFGParam->pNMS3Info = NULL;
    }

    pCFGParam->iParamNum = iParamNo + 1;

    TRACE("\n_______pCFGParam->iParamNum = %d________\n",pCFGParam->iParamNum);


    int  nChrCount = pCFGParam->iParamNum * sizeof(PERSISTENT_SIG_RECORD);
    char *pFrame = (char *)pCFGParam->pParam;

    for (i = 0; i < nChrCount; i++)
    {
	dwSum += *pFrame;
	pFrame++;
    }

    nChrCount = pCFGParam->iUserNum * sizeof(USER_INFO_STRU);
    pFrame = (char *)pCFGParam->pUserInfo;
    dwUserSum = 0;
    for (i = 0; i < nChrCount; i++)
    {
	dwUserSum += *pFrame;
	pFrame++;
    }

    nChrCount = pCFGParam->iNMSNum * sizeof(NMS_INFO);
    pFrame = (char *)pCFGParam->pNMSInfo;

    for (i = 0; i < nChrCount; i++)
    {
	dwUserSum += *pFrame;
	pFrame++;
    }

    nChrCount = pCFGParam->iNMS3Num * sizeof(V3NMS_INFO);
    pFrame = (char *)pCFGParam->pNMS3Info;

    for (i = 0; i < nChrCount; i++)
    {
	dwUserSum += *pFrame;
	pFrame++;
    }


    pCFGParam->iCheckSum = (WORD)-dwSum;
    pCFGParam->iCheckUserSum = (WORD)-dwUserSum;


    /*printf("\npCFGParam->iCheckUserSum=%d\n",pCFGParam->iCheckUserSum);
    printf("\ndwSum=%d\n",dwSum);*/


    pParamFile = fopen(WEB_SETTINGPARAM_FILE_NAME,"wb+");
    fwrite(&pCFGParam->sFileHeadInfo,(size_t)ITEM_OF(pCFGParam->sFileHeadInfo),1,pParamFile);
    fwrite(&pCFGParam->iVersion,sizeof(int),1,pParamFile);
    fwrite(&pCFGParam->iParamNum,sizeof(int),1,pParamFile);
    fwrite(pCFGParam->pParam,sizeof(PERSISTENT_SIG_RECORD),pCFGParam->iParamNum,pParamFile);

    fwrite(&pCFGParam->iUserNum,sizeof(int),1,pParamFile);
    fwrite(pCFGParam->pUserInfo,sizeof(USER_INFO_STRU),pCFGParam->iUserNum,pParamFile);
    fwrite(&pCFGParam->iNMSNum,sizeof(int),1,pParamFile);
    fwrite(pCFGParam->pNMSInfo,sizeof(NMS_INFO),pCFGParam->iNMSNum,pParamFile);
    fwrite(&pCFGParam->iNMS3Num,sizeof(int),1,pParamFile);
    fwrite(pCFGParam->pNMS3Info,sizeof(V3NMS_INFO),pCFGParam->iNMS3Num,pParamFile);

    fwrite(&pCFGParam->iCheckUserSum,sizeof(int),1,pParamFile);
    fwrite(&pCFGParam->iCheckSum,sizeof(int),1,pParamFile);

    fclose(pParamFile);

if(readable_param == 1)
    {
	// write NMS info
	NMS_INFO *pNms = NULL;
	struct in_addr	inIP;
	char szV6IP[128];
	fprintf(fp,"\n\n[NMS Information]\n%32s%32s%32s%15s\n", "IP", "Public Community", "Private Community", "Trap Enable");
	pNms = pCFGParam->pNMSInfo;
	for(i=0; i < pCFGParam->iNMSNum && pNms != NULL; i++, pNms++)
	{
	    if(pNms->bIPV6 == FALSE)
	    {
			inIP.s_addr = pNms->ulIpAddress;
			fprintf(fp,"%32s%32s%32s%15s\n",
				inet_ntoa(inIP),
				pNms->szPublicCommunity,
				pNms->szPrivateCommunity,
				(pNms->nTrapEnable?"Yes":"No"));
	    }
	    else
	    {
			szV6IP[0] = 0;
			inet_ntop(AF_INET6, (const void *)(&(pNms->unIPV6Address)), szV6IP, 128);
			fprintf(fp,"%32s%32s%32s%15s\n",
				szV6IP,
				pNms->szPublicCommunity,
				pNms->szPrivateCommunity,
				(pNms->nTrapEnable?"Yes":"No"));
	    }
	}

	// write NMS3 info
	V3NMS_INFO *pNmsv3;
	fprintf(fp,"\n\n[NMS3 Information]\n%16s%20s%20s%15s%20s%20s\n", "UserName", "PrivPassword", "AuthPassword", "TrapEnabled", "TrapIP", "SecurityLevel");
	pNmsv3 = pCFGParam->pNMS3Info;
	for(i=0; i < pCFGParam->iNMS3Num && pNmsv3 != NULL; i++, pNmsv3++)
	{
	    if(pNmsv3->bIPV6 == FALSE)
	    {
			inIP.s_addr = pNmsv3->ulTrapIpAddress;
			fprintf(fp,"%16s%20s%20s%15s%20s%20d\n",
				pNmsv3->szUserName,
				pNmsv3->szPrivPwd,
				pNmsv3->szAuthPwd,
				(pNmsv3->nV3TrapEnable?"Yes":"No"),
				inet_ntoa(inIP),
				pNmsv3->nTrapSecurityLevel);
	    }
	    else
	    {
			szV6IP[0] = 0;
			inet_ntop(AF_INET6, (const void *)(&(pNmsv3->ulTrapIpV6Address)), szV6IP, 128);
			fprintf(fp,"%16s%20s%20s%15s%20s%20d\n",
				pNmsv3->szUserName,
				pNmsv3->szPrivPwd,
				pNmsv3->szAuthPwd,
				(pNmsv3->nV3TrapEnable?"Yes":"No"),
				szV6IP,
				pNmsv3->nTrapSecurityLevel);
	    }
	}
	fclose(fp);//close the file '/var/download/SettingParam.txt'
    }

    DELETE(pCFGParam->pParam);

    //Do not delete the user/SNMP/SNMPV3 information	DELETE(pCFGParam->pUserInfo);
    //DELETE(pCFGParam->pNMSInfo);
    //DELETE(pCFGParam->pNMS3Info);

    DELETE(pCFGParam);
	if(readable_param != 1)	// if readable_param = 1; then all the log files are required along with SettingParam.tar
    {	
		//remove the /var/log file in case /var/ is not enough
		char szCommandStr[128];
		sprintf(szCommandStr, "rm  %s %s %s %s %s *.tar -f",
		WEB_LOG_DIR_HA, WEB_LOG_DIR_BT, WEB_LOG_DIR_EL, WEB_LOG_DIR_SL, WEB_LOG_DIR_DL);
		_SYSTEM(szCommandStr);
	}
	char szZip[256];

#ifdef _CODE_FOR_MINI
	sprintf(szZip, "cp -f /app/config/run/SettingParam.run /var/");
#else
	sprintf(szZip, "cd /app/;tar -cf /var/download/SettingParam.tar ./config/run/SettingParam.run ./config/run/MainConfig.run");
#endif
	_SYSTEM(szZip);
	sprintf(szZip, "rm -rf %s", WEB_SETTINGPARAM_FILE_NAME);
	_SYSTEM(szZip);
    return 1;
}

/*==========================================================================*
* FUNCTION :  Web_GetRealTimeAlarmData
* PURPOSE  :
* CALLS    :
* CALLED BY:
* ARGUMENTS:	OUT ALARM_SIG_VALUE **ppSigValue:
* RETURN   :
* COMMENTS : Copied from jsonFile.c as it is. For CR - Retrieve logs
* CREATOR  :                DATE: 2019-12-13
*==========================================================================*/
static int Web_GetRealTimeAlarmData(OUT ALARM_SIG_VALUE **ppSigValue)
{
    int					iError = 0;
    ALARM_SIG_VALUE			*stAlarmSigValue = NULL;
    int					iSignalNum = 0;
    int	iVarSubID = 0;
    int	iBufLen = 0;
    int iTimeOut = 0;
    int	iRAlarmSigNumRet = 0;

    iError = DxiGetData(VAR_ACTIVE_ALARM_NUM,
	ALARM_LEVEL_NONE,
	iVarSubID,
	&iBufLen,
	&iRAlarmSigNumRet,
	iTimeOut);

    if(iRAlarmSigNumRet > 0)
    {
		stAlarmSigValue = NEW(ALARM_SIG_VALUE, iRAlarmSigNumRet);
		if(stAlarmSigValue == NULL)
		{
			AppLogOut("Web_GetRealTimeAlarmData", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
			return FALSE;
		}
		memset(stAlarmSigValue, 0x0, sizeof(ALARM_SIG_VALUE) * iRAlarmSigNumRet);
    }
    else
    {
		return FALSE;
    }

    iBufLen = sizeof(ALARM_SIG_VALUE) * iRAlarmSigNumRet;//Should assign size

    iError += DxiGetData(VAR_ACTIVE_ALARM_INFO,
	ALARM_LEVEL_NONE,
	0,
	&iBufLen,
	stAlarmSigValue,
	0);

    iSignalNum = iBufLen / sizeof(ALARM_SIG_VALUE);
    if(iError == ERR_DXI_OK)
    {
		if(stAlarmSigValue != NULL)
		{
			*ppSigValue = (ALARM_SIG_VALUE *)stAlarmSigValue;
		}

		return iSignalNum;

    }
    else
    {
		if(stAlarmSigValue != NULL)
		{
			DELETE(stAlarmSigValue);
			stAlarmSigValue = NULL;
		}
		return FALSE;
    }
}
 /*==========================================================================*
* FUNCTION :  Web_MakeSettingsInfoFile
* PURPOSE  :  Settings are written in the file; SetingParam.tar is created along with readable SettingParam.run; Current alarms are written in the file
* CALLS    :
* CALLED BY:
* ARGUMENTS:
* RETURN   :
* COMMENTS :  Created for Change Request - Retrieve all logs from web page
* CREATOR  : Hrishikesh Oak               DATE: 2019-11-05
*==========================================================================*/
static int Web_MakeSettingsInfoFile(void)
{
    int iLen = 0;
    int iBufLen, iError, iTimeOut;
    iTimeOut = 100;
    iError = ERR_DXI_OK;
    FILE *pFile = NULL;

    RunThread_Heartbeat(RunThread_GetId(NULL));
	#define SETTINGS_FILE "/var/download/settings.txt"
	pFile = fopen(SETTINGS_FILE, "w");	//truncate or create a new file.
	if(pFile == NULL)
	{
		return -1;
	}

    /* Get Ethernet settings */
    ACU_NET_INFO 	stACUNetInfo;
    ACU_NET_INFO 	stACUFrontNetInfo;
    iBufLen = sizeof(ACU_NET_INFO);

    iError = DxiGetData(VAR_ACU_NET_INFO,
			NET_INFO_ALL,
			0,
			&iBufLen,
			&stACUNetInfo,
			iTimeOut);

    if (iError == ERR_DXI_OK)
    {
		fprintf(pFile, "\n[Ethernet Settings]\n");

		iError = DxiGetData(VAR_NET_FRONT_IP_INFO,
					0,
					0,
					&iBufLen,
					&(stACUFrontNetInfo.ulIp),
				iTimeOut);
		if (iError == ERR_DXI_OK)
		{
			fprintf(pFile, "Front: %s\n", inet_ntoa(*((struct in_addr *)&stACUFrontNetInfo.ulIp)));
		}
		else
		{
			stACUFrontNetInfo.ulIp = 0;
			fprintf(pFile, "Front: Error\n");
		}
		
		if(stACUFrontNetInfo.ulIp != stACUNetInfo.ulIp)
		{
			fprintf(pFile, "Network: %s\n", inet_ntoa(*((struct in_addr *)&stACUNetInfo.ulIp)));
		}
		fprintf(pFile, "Mask: %s\n", inet_ntoa(*((struct in_addr *)&stACUNetInfo.ulMask)));
		fprintf(pFile, "Gateway: %s\n", inet_ntoa(*((struct in_addr *)&stACUNetInfo.ulGateway)));

    }else{}
    iLen = 0;
    iError = ERR_DXI_OK;
    /* Get Ethernet settings Ends */

    /* Get SNMP settings */
    APP_SERVICE			*stAppService = NULL;
    char			*szNmsInfo = NULL;
    int				nTrapLevel, i = 0, iNMSNum = 0;
    int				*pnTrapLevel = &nTrapLevel;
    NMS_INFO			*stNmsInfo = NULL;
    struct in_addr		inIP;
    char 			szV6IP[128];
    iBufLen = 1;

    fprintf(pFile,"\n[SNMP Settings]\n");
    if(g_SiteInfo.iSNMPV3Flag == NO_SNMPV3)
    {
	stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,(void *)GET_SERVICE_OF_NMSV2_NAME);
    }
    else
    {
	stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,(void *)GET_SERVICE_OF_NMSV3_NAME);
    }

    if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
    {
		if(stAppService->pfnServiceConfig(stAppService->hServiceThread,
			stAppService->bServiceRunning,
			&stAppService->args,
			GETTRAP_TYPE_OF_NMS,
			&iBufLen,
			(void *)pnTrapLevel) == ERR_SNP_OK)
		{
			fprintf(pFile,"Accepted Trap Level=%d\n", (*pnTrapLevel));
		}
		iBufLen = 1;

		fprintf(pFile,"\n@NMSV2 Configuration\n%-32s%-32s%-32s%-15s\n", "NMS IP", "Public Community", "Private Community", "Trap Enabled");
		if(stAppService->pfnServiceConfig(stAppService->hServiceThread,
			stAppService->bServiceRunning,
			&stAppService->args,
			GET_NMS_USER_INFO,
			&iBufLen,
			(void *)&stNmsInfo) == ERR_SNP_OK)
		{
			iNMSNum =  iBufLen /sizeof(NMS_INFO);
			if(iNMSNum > 0)
			{
			for(i = 0; i < iNMSNum && stNmsInfo != NULL; i++, stNmsInfo++)
			{
				if(stNmsInfo->bIPV6 == FALSE)
				{
				inIP.s_addr = stNmsInfo->ulIpAddress;
				fprintf(pFile,"%-32s%-32s%-32s%-15s\n",
					inet_ntoa(inIP),
					stNmsInfo->szPublicCommunity,
					stNmsInfo->szPrivateCommunity,
					(stNmsInfo->nTrapEnable?"Yes":"No")
					);
				}
				else
				{
				inet_ntop(AF_INET6, (const void *)(&(stNmsInfo->unIPV6Address)), szV6IP, 128);
				fprintf(pFile,"%-32s%-32s%-32s%-15s\n",
					szV6IP,
					stNmsInfo->szPublicCommunity,
					stNmsInfo->szPrivateCommunity,
					(stNmsInfo->nTrapEnable?"Yes":"No")
					);
				}
			}
			}
		}
		iBufLen = 1;
		iNMSNum = 0;
		stNmsInfo = NULL;
		szV6IP[0] = 0;

		if(g_SiteInfo.iSNMPV3Flag != NO_SNMPV3)
		{

			V3NMS_INFO		*stNmsInfo = NULL;

			fprintf(pFile,"\n@NMSV3 Configuration\n%-16s%-20s%-20s%-15s%-20s%-20s\n", "UserName", "PrivPassword", "AuthPassword", "TrapEnabled", "TrapIP", "TrapSecurityLevel");
			if(stAppService->pfnServiceConfig(stAppService->hServiceThread,
			stAppService->bServiceRunning,
			&stAppService->args,
			GET_NMS_V3_USER_INFO,
			&iBufLen,
			(void *)&stNmsInfo) == ERR_SNP_OK)
			{
			iNMSNum =  iBufLen /sizeof(V3NMS_INFO);
			if(iNMSNum > 0)
			{
				for(i = 0; i < iNMSNum && stNmsInfo != NULL; i++, stNmsInfo++)
				{
				if(stNmsInfo->bIPV6 == FALSE)
				{
					inIP.s_addr = stNmsInfo->ulTrapIpAddress;
					fprintf(pFile,"%-16s%-20s%-20s%-15s%-20s%-20d\n",
					stNmsInfo->szUserName,
					stNmsInfo->szPrivPwd,
					stNmsInfo->szAuthPwd,
					(stNmsInfo->nV3TrapEnable?"Yes":"No"),
					inet_ntoa(inIP),
					(stNmsInfo->nTrapSecurityLevel)
					);
				}
				else
				{
					inet_ntop(AF_INET6, (const void *)(&(stNmsInfo->ulTrapIpV6Address)), szV6IP, 128);
					fprintf(pFile,"%-16s%-20s%-20s%-15s%-20s%-20d\n",
					stNmsInfo->szUserName,
					stNmsInfo->szPrivPwd,
					stNmsInfo->szAuthPwd,
					(stNmsInfo->nV3TrapEnable?"Yes":"No"),
					szV6IP,
					(stNmsInfo->nTrapSecurityLevel)
					);
				}
				}
			}
			}
		}
    }
    stAppService = NULL;
    /* Get SNMP settings Ends */

    /* Get EEM protocol settings */
    COMMON_CONFIG	stCommonConfig;
    int			iReturn = 0;
    char 		szBuf[16];

    stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_ESR_NAME);

    if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
    {
		iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
			stAppService->bServiceRunning,
			&stAppService->args,
			0x0,
			0,
			(void *)&stCommonConfig);

		if(iReturn == ERR_SERVICE_CFG_OK)
		{
			fprintf(pFile,"\n[EEM Settings]\n");

			/* Write a string of Protocol Type */
			szBuf[0] = 0;
			if(stCommonConfig.iProtocolType == 1)
			{
				strcpy(szBuf, "EEM");
			}
			else if(stCommonConfig.iProtocolType == 2)
			{
				strcpy(szBuf, "RSOC");
			}
			else if(stCommonConfig.iProtocolType == 3)
			{
				strcpy(szBuf, "SOC/TPE");
			}
			else{}

			fprintf(pFile, "%-32s%s\n", "Protocol Type:", szBuf);

			/* Write a string of Protocol Media */
			szBuf[0] = 0;
			if(stCommonConfig.iMediaType == 0)
			{
				strcpy(szBuf, "RS-232");
			}
			else if(stCommonConfig.iMediaType == 1)
			{
				strcpy(szBuf, "Modem");
			}
			else if(stCommonConfig.iMediaType == 2)
			{
				strcpy(szBuf, "IPV4");
			}
			else if(stCommonConfig.iMediaType == 3)
			{
				strcpy(szBuf, "IPV6");
			}
			else{}

			fprintf(pFile, "%-32s%s\n", "Protocol Media:", szBuf);
			fprintf(pFile, "%-32s%s\n", "Port Parameter:", stCommonConfig.szCommPortParam);
			fprintf(pFile, "%-32s%s\n", "Callback Enabled:", (stCommonConfig.bCallbackInUse?"Yes":"No"));
			fprintf(pFile, "%-32s%s\n", "Report Enabled:", (stCommonConfig.bReportInUse?"Yes":"No"));
			fprintf(pFile, "%-32s%d\n", "CCID:", stCommonConfig.byCCID);
			fprintf(pFile, "%-32s%d\n", "SOCID:", stCommonConfig.iSOCID);
			fprintf(pFile, "%-32s%d\n", "Maximum Alarm Report Attempts:", stCommonConfig.iMaxAttempts);
			fprintf(pFile, "%-32s%d\n", "Call Elapse Time:", stCommonConfig.iAttemptElapse/1000);
			fprintf(pFile, "%-32s%s\n", "Main Report IP:", stCommonConfig.szReportIP[0]);
			fprintf(pFile, "%-32s%s\n", "Second Report IP:", stCommonConfig.szReportIP[1]);
			fprintf(pFile, "%-32s%s\n", "Security Connection IP 1:", stCommonConfig.szSecurityIP[0]);
			fprintf(pFile, "%-32s%s\n", "Security Connection IP 2:", stCommonConfig.szSecurityIP[1]);
			fprintf(pFile, "%-32s%s\n", "Main Report Phone Number:", stCommonConfig.szAlarmReportPhoneNumber[0]);
			fprintf(pFile, "%-32s%s\n", "Second Report Phone Number:", stCommonConfig.szAlarmReportPhoneNumber[1]);
			fprintf(pFile, "%-32s%s\n", "Callback Phone Number:", stCommonConfig.szCallbackPhoneNumber[0]);
			fprintf(pFile, "%-32s%d\n", "Safety Level:", stCommonConfig.iSecurityLevel);
		}
    }
    stAppService = NULL;
    iReturn = 0;
    /* Get EEM protocol settings Ends */

    /* Get YDN23 settings */
    YDN_COMMON_CONFIG	stYdnCommonConfig;

    stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_YDN_NAME);
    stYdnCommonConfig.iProtocolType = YDN23;
    if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
    {
		iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
				stAppService->bServiceRunning,
				&stAppService->args,
				0x0,
				0,
				(void *)&stYdnCommonConfig);

		if(iReturn == ERR_SERVICE_CFG_OK)
		{
			fprintf(pFile,"\n[YDN23 Settings]\n");
			fprintf(pFile,"%-32s%s\n", "Protocol Type:", "YDN23");

			/* Write a string of Protocol Media */
			szBuf[0] = 0;
			if(stYdnCommonConfig.iMediaType == 0)
			{
			strcpy(szBuf, "RS-232");
			}
			else if(stYdnCommonConfig.iMediaType == 1)
			{
			strcpy(szBuf, "Modem");
			}
			else if(stYdnCommonConfig.iMediaType == 2)
			{
			strcpy(szBuf, "IPV4");
			}
			else{}

			fprintf(pFile,"%-32s%s\n", "Protocol Media:", szBuf);
			fprintf(pFile,"%-32s%s\n", "Port Parameter:", stYdnCommonConfig.szCommPortParam);
			fprintf(pFile,"%-32s%d\n", "Self Address:", stYdnCommonConfig.byADR);
		}
    }
    stAppService = NULL;
    iReturn = 0;
    /* Get YDN23 settings Ends */

    /* Get Modbus protocol settings */
    MODBUS_COMMON_CONFIG	stModCommonConfig;

    stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_MODBUS_NAME);
    stModCommonConfig.iProtocolType = 5;//Modbus
    if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
    {
		iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
				stAppService->bServiceRunning,
				&stAppService->args,
				0x0,
				0,
				(void *)&stModCommonConfig);

		if(iReturn == ERR_SERVICE_CFG_OK)
		{

			fprintf(pFile,"\n[Modbus Settings]\n");
			fprintf(pFile,"%-32s%s\n", "Protocol Type:", "Modbus");

			/* Write a string of Protocol Media */
			szBuf[0] = 0;
			if(stModCommonConfig.iMediaType == 0)
			{
			strcpy(szBuf, "RS-232");
			}
			else if(stModCommonConfig.iMediaType == 1)
			{
			strcpy(szBuf, "RS-485");
			}
			else if(stModCommonConfig.iMediaType == 2)
			{
			strcpy(szBuf, "IPV4");
			}
			else{}

			fprintf(pFile,"%-32s%s\n", "Protocol Media:", szBuf);
			fprintf(pFile,"%-32s%s\n", "Port Parameter:", stModCommonConfig.szCommPortParam);
			fprintf(pFile,"%-32s%d\n", "Self Address:", stModCommonConfig.byADR);
		}
    }
    stAppService = NULL;
    iReturn = 0;
    /* Get Modbus protocol settings Ends */

   /* Get TL1 protocol settings Ends */
    fclose(pFile);

    RunThread_Heartbeat(RunThread_GetId(NULL));
    // Create SettingParam.tar and SettingParam.txt
    Web_MakeGetSetParamWebPage(TRUE);
    RunThread_Heartbeat(RunThread_GetId(NULL));

    /* Create current alarms file */
    ALARM_SIG_VALUE *pAlarmSigValue = NULL;
    time_t tmSample;
    char szTime[32];
    int iNum;
    int iSignalNumber = Web_GetRealTimeAlarmData(&pAlarmSigValue);
    #define CURRENT_ALARMS_FILE	"/var/download/current_alarms.txt"
    pFile = fopen(CURRENT_ALARMS_FILE, "w");
    if(pFile != NULL)
    {
		fprintf(pFile, "%-32s%-32s%-15s%-32s\n\n", "Relative Device", "Signal Name", "Alarm Level", "Time" );
		if(iSignalNumber > 0)
		{
			if(iSignalNumber > 200)
			{
			iSignalNumber = 200;
			}

			for(iNum =0; iNum < iSignalNumber && pAlarmSigValue != NULL; iNum++, pAlarmSigValue++)
			{
				if(pAlarmSigValue->pStdSig->iAlarmLevel != ALARM_LEVEL_NONE)
				{
					tmSample = pAlarmSigValue->sv.tmStartTime;
					TimeToString(tmSample, TIME_HISDATA_FMT, szTime, sizeof(szTime));
					fprintf(pFile, "%-32s%-32s%-15d%-32s\n",
					pAlarmSigValue->pEquipInfo->pEquipName->pFullName[0],
					pAlarmSigValue->pStdSig->pSigName->pFullName[0],
					pAlarmSigValue->pStdSig->iAlarmLevel,
					szTime);
				}
			}
		}
		fclose(pFile);
    }
    /* Create current alarms file Ends */

    RunThread_Heartbeat(RunThread_GetId(NULL));
    /* Create controller information file */
    #define CONTROLLER_INFO_FILE	"/var/download/controller_info.txt"
    pFile = fopen(CONTROLLER_INFO_FILE, "w");
    if(pFile != NULL)
    {
#define ID_VOLTAGE		1
#define ID_LOAD_CURRENT		2
		SIG_BASIC_VALUE* pSigValue;
		int iVarSubID = 0;
		iBufLen = 0;

		iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, ID_VOLTAGE);
		iError = DxiGetData(VAR_A_SIGNAL_VALUE,
			1,
			iVarSubID,
			&iBufLen,
			(void *)&pSigValue,
			0);
		if(iError == ERR_DXI_OK)
		{
			fprintf(pFile, "%-20s%-.1f\n", "Voltage:", pSigValue->varValue.fValue);
		}

		iBufLen = 0;
		iVarSubID = 0;

		iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, ID_LOAD_CURRENT);
		iError = DxiGetData(VAR_A_SIGNAL_VALUE,
			1,
			iVarSubID,
			&iBufLen,
			(void *)&pSigValue,
			0);
		if(iError == ERR_DXI_OK)
		{
			fprintf(pFile, "%-20s%-.1f\n", "Load_Current:", pSigValue->varValue.fValue);
		}

		fprintf(pFile, "%-20s%-20s\n", "System Name:", g_SiteInfo.langDescription.pFullName[0]);

		ACU_PRODUCT_INFO sAcuProductInfo;
		memset(&sAcuProductInfo, 0, sizeof(sAcuProductInfo));
		iBufLen = sizeof(sAcuProductInfo);
		iError = DxiGetData(VAR_ACU_PUBLIC_CONFIG,
			ACU_PRODUCT_INFO_GET,
			0,
			&iBufLen,
			&(sAcuProductInfo),
			0);
		if(iError == ERR_DXI_OK)
		{
			fprintf(pFile, "%-20s%-20s\n%-20s%-20s\n%-20s%-20s\n%-20s%-20s\n%-20s%-20s\n"
				, "Product Model:", sAcuProductInfo.szPartNumber
				, "Serial Number:", sAcuProductInfo.szACUSerialNo
				, "Hardware Version:", sAcuProductInfo.szHWRevision
				, "Software Version:", sAcuProductInfo.szSWRevision
				, "Config Version:", sAcuProductInfo.szCfgFileVersion);
		}

		fprintf(pFile, "%-20s%-20s\n%-20s%-20s\n"
			, "Site Name:", g_SiteInfo.langSiteName.pFullName[0]
			, "Site Location:", g_SiteInfo.langSiteLocation.pFullName[0]);
		fclose(pFile);
    }
    /* Create controller information file Ends */
    return 0;
}

/*==========================================================================*
* FUNCTION :  Judge XXS Illegal Character 
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	
* RETURN   :  
* COMMENTS : 
* CREATOR  : John               DATE: 2017-1-13 16:36
*==========================================================================*/
static int Web_JudgeXSS_Illegal_Char(char *PszContactAddr)
{
	
	char *desdata=NULL;
	int i,iContactAddrLength;

	iContactAddrLength = strlen(PszContactAddr);
	desdata = PszContactAddr;

	
	for(i=0;i<iContactAddrLength;i++)
	{
		 
		if(desdata[i]=='&'||desdata[i]=='<'||desdata[i]=='>'||desdata[i]=='"'||desdata[i]==' ')
		{
			return FALSE;//error
		}

	}
	
	if(Web_Judge_Mail_Format(desdata)==FALSE)
	{
		return FALSE;
	}

	return TRUE;

}

static int Web_Judge_Mail_Format(char *PszContactAddr)
{
	
	int status,iReturnValue;
	int cflags=REG_EXTENDED;
	regmatch_t pmatch[1];
	const size_t nmatch=1;
	regex_t reg;
	
	//Compile regular expression 
	const char *pattern="^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*.\\w+([-.]\\w+)*$";
	char *PszDesdata= PszContactAddr;


	regcomp(&reg,pattern,cflags);
	//Apply a regular expression to each line to match 
	status=regexec(&reg,PszDesdata,nmatch,pmatch,0);

	if(status==REG_NOMATCH)
	{
		iReturnValue = 0;
	}
	else if(status==0)
	{
		iReturnValue = 1;
	}

	regfree(&reg);
	return iReturnValue;
	
	
	 
}



/*==========================================================================*
* FUNCTION :  Web_SendConfigureData
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN char *buf:
IN char *filename:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
*==========================================================================*/
static int Web_SendConfigureData(IN char *buf, 
				 IN char *filename)
{
	int			fd2 = 0;
	char		pGetData[100];
	char		*ptr = NULL;
	int			iLanguage = 0, iLen  = 0;;
	int			iCommandType = 0;
	char		*pReturnBuf = NULL;
	int			iReturnValue = 0;
	char		szReturnResultSet[6000] = {0};//Added by wj because buffer size is about 600k
	int			iModifyPassword = 0;
	//char		szReturnProductInfo[12800];//Product Information
	int         iresult = 0;
	char        szUserName[33] = {0};
	int	    iCabinetNum;
	char cReturnstring[100];
	//char        szTempUserName[64] = {0};

	//changed by Frank Wu,17/N/27,20140527, for power split
	char iLargeResultValue = iReturnValue;
	char *pLargeResultSet = NULL;

	if((fd2 = open(filename,O_WRONLY )) > 0)
	{
		/*offset*/
		ptr = buf;
		ptr = ptr + MAX_COMM_PID_LEN + MAX_COMM_GET_TYPE;
#define MAX_MODIFY_PASS_LEN				2
		/*GET IF MODIFY PASSWORD*/
		strncpyz(pGetData,ptr,MAX_MODIFY_PASS_LEN + 1);
		iModifyPassword = atoi(pGetData);
		ptr = ptr + MAX_MODIFY_PASS_LEN;
#define MAX_COMMAND_TYPE_LEN			2

#define MAX_USER_NAME_LEN              32

		strncpyz(szUserName,ptr,MAX_USER_NAME_LEN + 1);

		ptr = ptr + MAX_USER_NAME_LEN;

		TRACE("\n_____________USER_NAME______________:%s",szUserName);


		/*Get command type*/
		strncpyz(pGetData,ptr,MAX_COMMAND_TYPE_LEN + 1);
		iCommandType = atoi(pGetData);
		ptr = ptr + MAX_COMMAND_TYPE_LEN;

		if(iModifyPassword == SET_USER_PASSWORD)
		{
			iCommandType = SET_USER_PASSWORD;
		}
		/*Get Language type*/

		strncpyz(pGetData,ptr,MAX_LANGUAGE_TYPE_LEN + 1);
		iLanguage = atoi(pGetData);
		TRACE("\n_____________iLanguage______________:%d",iLanguage);
		ptr = ptr + MAX_LANGUAGE_TYPE_LEN;
		TRACE("\n______________iCommandType_____________%d\n", iCommandType);
		switch(iCommandType)
		{
		case GET_NETWORK_INFO:

			pReturnBuf = Web_MakeNetWorkInfoBuffer();
			if( Web_GetProtectedStatus() == TRUE)
			{
				sprintf(szReturnResultSet,"%2d%s",WEB_RETURN_PROTECTED_ERROR, pReturnBuf);
			}
			else
			{
				sprintf(szReturnResultSet,"%2d%s",99,pReturnBuf);
			}
#ifdef _SHOW_WEB_INFO
			//TRACE("GET_NETWORK_INFO : %s\n", szReturnResultSet);
#endif 

			if(pReturnBuf != NULL)
			{
				DELETE(pReturnBuf);
				pReturnBuf = NULL;
			}

			break;

		case GET_V6_NETWORK_INFO:

		    TRACE_WEB_USER_NOT_CYCLE("Deal with GET_V6_NETWORK_INFO\n");
		    pReturnBuf = Web_MakeV6NetWorkInfoBuffer();
		    if(Web_GetProtectedStatus() == TRUE)
		    {
			sprintf(szReturnResultSet,"%2d%s",WEB_RETURN_PROTECTED_ERROR, pReturnBuf);
		    }
		    else
		    {
			sprintf(szReturnResultSet, "%2d%s", 99, pReturnBuf);
		    }
		    TRACE_WEB_USER_NOT_CYCLE("szReturnResultSet is %s\n", szReturnResultSet);

		    if(pReturnBuf != NULL)
		    {
			DELETE(pReturnBuf);
			pReturnBuf = NULL;
		    }

		    break;

		case SET_NETWORK_INFO:

			TRACE_WEB_USER_NOT_CYCLE("Deal with SET_NETWORK_INFO\n");
			iReturnValue = Web_Modify_AcuIP_Addr(ptr);
			pReturnBuf = Web_MakeNetWorkInfoBuffer();
			sprintf(szReturnResultSet,"%2d%s",iReturnValue,pReturnBuf);

			if(pReturnBuf != NULL)
			{
				DELETE(pReturnBuf);
				pReturnBuf = NULL;
			}


			break;

		case SET_V6_NETWORK_INFO:
		
		    TRACE_WEB_USER_NOT_CYCLE("Deal with SET_V6_NETWORK_INFO\n");
		    iReturnValue = Web_Modify_V6_AcuIP_Addr(ptr);
		    pReturnBuf = Web_MakeV6NetWorkInfoBuffer();
		    sprintf(szReturnResultSet, "%2d%s", iReturnValue, pReturnBuf);
		    TRACE_WEB_USER_NOT_CYCLE("szReturnResultSet is %s\n", szReturnResultSet);
		    if(pReturnBuf != NULL)
		    {
			DELETE(pReturnBuf);
			pReturnBuf = NULL;
		    }
		    break;

		case SET_NMS_INFO:
		    TRACE_WEB_USER_NOT_CYCLE("Deal with SET_NMS_INFO\n");
			iReturnValue = Web_ModifyNMSPrivateConfigure(ptr);
			pReturnBuf = Web_MakeNMSPrivateConfigureBuffer();

			iLen += sprintf(szReturnResultSet + iLen,"%2d", iReturnValue);
			if(pReturnBuf != NULL)
			{
			    iLen += sprintf(szReturnResultSet + iLen,"%s",pReturnBuf);
			}

			if(pReturnBuf != NULL)
			{
			DELETE(pReturnBuf);
			pReturnBuf = NULL;
			}
			break;
		case GET_NMS_INFO:
		    TRACE_WEB_USER_NOT_CYCLE("Deal with GET_NMS_INFO\n");
			pReturnBuf = Web_MakeNMSPrivateConfigureBuffer();
			if(Web_GetProtectedStatus() == TRUE)
			{
			iLen += sprintf(szReturnResultSet + iLen,"%2d", WEB_RETURN_PROTECTED_ERROR_NMS);
			}
			else
			{
			iLen += sprintf(szReturnResultSet + iLen,"%2d", 1);
			}
			if(pReturnBuf != NULL)
			{
			    iLen += sprintf(szReturnResultSet + iLen,"%s",pReturnBuf);
			}
			TRACE_WEB_USER_NOT_CYCLE("szReturnResultSet is %s\n", szReturnResultSet);
			#ifdef _SHOW_WEB_INFO
			//TRACE("szReturnResultSet : %s\n", szReturnResultSet);
			#endif 


			if(pReturnBuf != NULL)
			{
			DELETE(pReturnBuf);
			pReturnBuf = NULL;
			}

			break;

		case READ_CABINET:
		    TRACE_WEB_USER_NOT_CYCLE("Deal with READ_CABINET\n");
		    TRACE_WEB_USER_NOT_CYCLE("ptr is %s\n", ptr);
		    strncpyz(pGetData,ptr,MAX_CABINET_NUM_LEN + 1);
		    iCabinetNum = atoi(pGetData);
		    TRACE_WEB_USER_NOT_CYCLE("iCabinetNum is %d\n", iCabinetNum);
		    pReturnBuf = Web_MakeCabinetDataBuffer(iCabinetNum);
		    if(Web_GetProtectedStatus() == TRUE)
		    {
			iLen += sprintf(szReturnResultSet + iLen,"%2d", WEB_RETURN_PROTECTED_ERROR_CABINET);
		    }
		    else
		    {
			iLen += sprintf(szReturnResultSet + iLen,"%2d", 1);
		    }
		    if(pReturnBuf != NULL)
		    {
			iLen += sprintf(szReturnResultSet + iLen,"%s",pReturnBuf);
		    }
		    TRACE_WEB_USER_NOT_CYCLE("szReturnResultSet is %s\n", szReturnResultSet);

		    if(pReturnBuf != NULL)
		    {
			DELETE(pReturnBuf);
			pReturnBuf = NULL;
		    }
		    break;

		case RESET_CABINET:
		    TRACE_WEB_USER_NOT_CYCLE("Deal with RESET_CABINET\n");
		    TRACE_WEB_USER_NOT_CYCLE("ptr is %s\n", ptr);
		    strncpyz(pGetData,ptr,MAX_CABINET_NUM_LEN + 1);
		    iCabinetNum = atoi(pGetData);
		    TRACE_WEB_USER_NOT_CYCLE("iCabinetNum is %d\n", iCabinetNum);
		    if(Web_GetProtectedStatus() == TRUE)
		    {
			iLen += sprintf(szReturnResultSet + iLen,"%2d", WEB_RETURN_PROTECTED_ERROR_CABINET);
		    }
		    else
		    {
			iReturnValue = Web_ResetCabinetData(iCabinetNum);
			iLen += sprintf(szReturnResultSet + iLen,"%2d", iReturnValue);
		    }
		    break;

		case SET_CB_PARA:
		    TRACE_WEB_USER_NOT_CYCLE("Deal with SET_CB_PARA\n");
		    TRACE_WEB_USER_NOT_CYCLE("ptr is %s\n", ptr);
		    strncpyz(pGetData,ptr,MAX_CABINET_NUM_LEN + 1);
		    iCabinetNum = atoi(pGetData);
		    TRACE_WEB_USER_NOT_CYCLE("iCabinetNum is %d\n", iCabinetNum);
		    ptr = ptr + MAX_CABINET_NUM_LEN + 1;// skip the ";"
		    TRACE_WEB_USER_NOT_CYCLE("ptr is %s\n", ptr);
		    if(Web_GetProtectedStatus() == TRUE)
		    {
			iLen += sprintf(szReturnResultSet + iLen,"%2d", WEB_RETURN_PROTECTED_ERROR_CABINET);
		    }
		    else
		    {
			iReturnValue = Web_SetCBPara(ptr, cReturnstring);
			iLen += sprintf(szReturnResultSet + iLen,"%2d", iReturnValue);
			iLen += sprintf(szReturnResultSet + iLen,"%s",cReturnstring);
		    }
		    TRACE_WEB_USER_NOT_CYCLE("szReturnResultSet is %s\n", szReturnResultSet);
		    break;

		case RESET_BINDING:
		    TRACE_WEB_USER_NOT_CYCLE("Deal with RESET_BINDING\n");
		    TRACE_WEB_USER_NOT_CYCLE("ptr is %s\n", ptr);
		    strncpyz(pGetData,ptr,MAX_CABINET_NUM_LEN + 1);
		    iCabinetNum = atoi(pGetData);
		    TRACE_WEB_USER_NOT_CYCLE("iCabinetNum is %d\n", iCabinetNum);
		    ptr = ptr + MAX_CABINET_NUM_LEN + 1;// skip the ";"
		    TRACE_WEB_USER_NOT_CYCLE("ptr is %s\n", ptr);
		    if(Web_GetProtectedStatus() == TRUE)
		    {
			iLen += sprintf(szReturnResultSet + iLen,"%2d", WEB_RETURN_PROTECTED_ERROR_CABINET);
		    }
		    else
		    {
			iReturnValue = Web_ReSetCBPara(ptr, cReturnstring);
			iLen += sprintf(szReturnResultSet + iLen,"%2d", iReturnValue);
			iLen += sprintf(szReturnResultSet + iLen,"%s",cReturnstring);
		    }
		    TRACE_WEB_USER_NOT_CYCLE("szReturnResultSet is %s\n", szReturnResultSet);
		    break;

		case SET_CABINET_PARA:
		    TRACE_WEB_USER_NOT_CYCLE("Deal with SET_CABINET_PARA\n");
		    TRACE_WEB_USER_NOT_CYCLE("ptr is %s\n", ptr);
		    strncpyz(pGetData,ptr,MAX_CABINET_NUM_LEN + 1);
		    iCabinetNum = atoi(pGetData);
		    TRACE_WEB_USER_NOT_CYCLE("iCabinetNum is %d\n", iCabinetNum);
		    ptr = ptr + MAX_CABINET_NUM_LEN + 1;// skip the ";"
		    TRACE_WEB_USER_NOT_CYCLE("ptr is %s\n", ptr);
		    if(Web_GetProtectedStatus() == TRUE)
		    {
			iLen += sprintf(szReturnResultSet + iLen,"%2d", WEB_RETURN_PROTECTED_ERROR_CABINET);
		    }
		    else
		    {
			iReturnValue = Web_SetCabinetPara(iCabinetNum, ptr, cReturnstring);
			iLen += sprintf(szReturnResultSet + iLen,"%2d", iReturnValue);
			iLen += sprintf(szReturnResultSet + iLen,"%s",cReturnstring);
		    }
		    TRACE_WEB_USER_NOT_CYCLE("szReturnResultSet is %s\n", szReturnResultSet);
		    break;

		case SET_CABINET:
		    TRACE_WEB_USER_NOT_CYCLE("Deal with SET_CABINET\n");
		    TRACE_WEB_USER_NOT_CYCLE("ptr is %s\n", ptr);
		    strncpyz(pGetData,ptr,MAX_CABINET_NUM_LEN + 1);
		    iCabinetNum = atoi(pGetData);
		    TRACE_WEB_USER_NOT_CYCLE("iCabinetNum is %d\n", iCabinetNum);
		    ptr = ptr + MAX_CABINET_NUM_LEN + 1;// skip the ";"
		    TRACE_WEB_USER_NOT_CYCLE("ptr is %s\n", ptr);
		    if(Web_GetProtectedStatus() == TRUE)
		    {
			iLen += sprintf(szReturnResultSet + iLen,"%2d", WEB_RETURN_PROTECTED_ERROR_CABINET);
		    }
		    else
		    {
			iReturnValue = Web_SetCabinetData(iCabinetNum, ptr);
			iLen += sprintf(szReturnResultSet + iLen,"%2d", iReturnValue);
			pReturnBuf = Web_MakeCabinetDataBuffer(iCabinetNum);
			if(pReturnBuf != NULL)
			{
			    iLen += sprintf(szReturnResultSet + iLen,"%s",pReturnBuf);
			}
		    }
		    TRACE_WEB_USER_NOT_CYCLE("szReturnResultSet is %s\n", szReturnResultSet);
		    if(pReturnBuf != NULL)
		    {
			DELETE(pReturnBuf);
			pReturnBuf = NULL;
		    }
		    break;

		case GET_ESR_INFO:

			pReturnBuf = Web_MakeESRPrivateConfigreBuffer();


			if(pReturnBuf != NULL)
			{
				if(Web_GetProtectedStatus() == TRUE)
				{
					iLen += sprintf(szReturnResultSet + iLen,"%2d%s",15,pReturnBuf);//WEB_RETURN_PROTECTED_ERROR
				}
				else
				{
					iLen += sprintf(szReturnResultSet + iLen,"%2d%s",99,pReturnBuf);
				}
				DELETE(pReturnBuf);
				pReturnBuf = NULL;
			}
			else
			{
				iLen += sprintf(szReturnResultSet + iLen,"%2d",1);
			}

			break;
		case SET_ESR_INFO:
		    TRACE_WEB_USER_NOT_CYCLE("Deal with SET_ESR_INFO\n");
			iReturnValue = Web_ModifyESRPrivateConfigure(ptr); 
			pReturnBuf = Web_MakeESRPrivateConfigreBuffer();

			iLen += sprintf(szReturnResultSet + iLen,"%2d", iReturnValue);


			if(pReturnBuf != NULL)
			{
				iLen += sprintf(szReturnResultSet + iLen,"%s",pReturnBuf);

				DELETE(pReturnBuf);
				pReturnBuf = NULL;
			}
			else
			{
			}
			break;


		case GET_TIME_INFO:

			pReturnBuf = Web_MakeACUTimeSrv();
			if(Web_GetProtectedStatus() == TRUE)
			{
				iLen += sprintf(szReturnResultSet + iLen,"%2d%s",WEB_RETURN_PROTECTED_ERROR, pReturnBuf);
			}
			else
			{
				iLen +=sprintf(szReturnResultSet + iLen,"%2d%s",1, pReturnBuf);
			}
#ifdef _SHOW_WEB_INFO
			//TRACE("GET_TIME_INFO : %s\n", pReturnBuf);
#endif 


			if(pReturnBuf != NULL)
			{
				DELETE(pReturnBuf);
				pReturnBuf = NULL;
			}
			break;

		case SET_TIME_IP:
			{	
				iReturnValue = Web_SetACUTimeSrv(WEB_MODIFY_TIMESRV_IP, ptr,szUserName);
				pReturnBuf = Web_MakeACUTimeSrv();
				sprintf(szReturnResultSet,"%2d%s",iReturnValue,pReturnBuf);
#ifdef _SHOW_WEB_INFO
				//TRACE("szReturnResultSet : %s\n", szReturnResultSet);
#endif 

				if(pReturnBuf != NULL)
				{
					DELETE(pReturnBuf);
					pReturnBuf = NULL;
				}
				break;
			}
		case SET_TIME_VALUE:
			{
				printf("SET_TIME_VALUE");
				iReturnValue = Web_SetACUTimeSrv(WEB_MODIFY_TIME, ptr,szUserName);

				pReturnBuf = Web_MakeACUTimeSrv();
				sprintf(szReturnResultSet,"%2d%s",iReturnValue,pReturnBuf);
				if(pReturnBuf != NULL)
				{
					DELETE(pReturnBuf);
					pReturnBuf = NULL;
				}
				break;

			}
		case GET_USER_INFO:
			pReturnBuf = Web_MakeACUUserInfoBuffer();

			if(Web_GetProtectedStatus() == TRUE)
			{
				iLen +=sprintf(szReturnResultSet + iLen,"%2d",WEB_RETURN_PROTECTED_ERROR);
			}
			else
			{
				iLen += sprintf(szReturnResultSet + iLen,"%2d", 0);
			}

			iLen += sprintf(szReturnResultSet + iLen,"%s",pReturnBuf);

			if(pReturnBuf != NULL)
			{
				DELETE(pReturnBuf);
				pReturnBuf = NULL;
			}

			////TRACE("GET_USER_INFO : s\n", szReturnResultSet);
			break;
		case SET_USER_INFO:
			{	
				int			iNewAuthority;
				int			iLen = 0;
				char		szName[33], szNewPassword[33], szAuthority[33];
				char		*szTrimName = NULL, *szTrimPassword = NULL;
				//changed by Frank Wu,10/N/15,20140527, for e-mail
				char		szContactAddr[128];
				char		*szTrimContactAddr = NULL;

				//if(Web_GetSetUserInfo(ptr, szName, szNewPassword, szAuthority) == TRUE)
				if(Web_GetSetUserInfo2(ptr, szName, szNewPassword, szAuthority, szContactAddr) == TRUE)
				{
					iNewAuthority = atoi(szAuthority);
					szTrimName = Cfg_RemoveWhiteSpace(szName);
					szTrimPassword = Cfg_RemoveWhiteSpace(szNewPassword);
					//changed by Frank Wu,11/N/15,20140527, for e-mail
					szTrimContactAddr = Cfg_RemoveWhiteSpace(szContactAddr);

					/***************added by john*****************/
					//if(Web_JudgeXSS_Illegal_Char(szTrimContactAddr)==FALSE)
					//{
					//	break;
					//}
					/*********************************************/
#ifdef _SHOW_WEB_INFO
					//TRACE("szTrimPassword : *%s*  %d  %d  [%s]\n", szTrimPassword, strlen(szTrimPassword), iNewAuthority, szTrimName);
#endif 					


					//iReturnValue = ModifyUserInfo(szTrimName, szTrimPassword, iNewAuthority);
					iReturnValue = ModifyUserInfo2(szTrimName, szTrimPassword, iNewAuthority, szTrimContactAddr);
					////TRACE("iReturnValue : %d\n", iReturnValue);
					pReturnBuf = Web_MakeACUUserInfoBuffer();
					if(iReturnValue == ERR_SEC_USER_NOT_EXISTED)
					{
						iReturnValue = 11;
					}
					iLen += sprintf(szReturnResultSet + iLen,"%2d", iReturnValue);
					iLen += sprintf(szReturnResultSet + iLen,"%s",pReturnBuf);

					if(pReturnBuf != NULL)
					{
						DELETE(pReturnBuf);
						pReturnBuf = NULL;
					}
					/*DELETE(szName);
					szName = NULL;

					DELETE(szNewPassword);
					szNewPassword = NULL;

					DELETE(szAuthority);
					szAuthority = NULL;*/
				}
				break;
			}
		case SET_USER_PASSWORD:
			{
				int			iNewAuthority;
				int			iLen = 0;
				char		szName[33], szNewPassword[33], szAuthority[33];
				char		*szTrimName = NULL, *szTrimPassword = NULL;
				//changed by Frank Wu,12/N/15,20140527, for e-mail
				char		szContactAddr[128];
				char		*szTrimContactAddr = NULL;

				//printf("SET_USER_PASSWORD[%d]\n", SET_USER_PASSWORD);
				//if(Web_GetSetUserInfo(ptr, szName, szNewPassword, szAuthority) == TRUE)
				if(Web_GetSetUserInfo2(ptr, szName, szNewPassword, szAuthority, szContactAddr) == TRUE)
				{
					iNewAuthority = Web_GetUserAuthority(Cfg_RemoveWhiteSpace(szName));
					szTrimName = Cfg_RemoveWhiteSpace(szName);
					szTrimPassword = Cfg_RemoveWhiteSpace(szNewPassword);
					//changed by Frank Wu,13/N/15,20140527, for e-mail
					szTrimContactAddr = Cfg_RemoveWhiteSpace(szContactAddr);

					//iReturnValue = ModifyUserInfo(szTrimName, szTrimPassword, iNewAuthority);
					iReturnValue = ModifyUserInfo2(szTrimName, szTrimPassword, iNewAuthority, szTrimContactAddr);
					pReturnBuf = Web_MakeACUUserInfoBuffer();
					if(iReturnValue == ERR_SEC_USER_NOT_EXISTED)
					{
						iReturnValue = 11;
					}
					iLen += sprintf(szReturnResultSet + iLen,"%2d", iReturnValue);
					iLen += sprintf(szReturnResultSet + iLen,"%s",pReturnBuf);

					if(pReturnBuf != NULL)
					{
						DELETE(pReturnBuf);
						pReturnBuf = NULL;
					}
				}
				break;
			}
		case ADD_USER_INFO:
			{
				int			iNewAuthority;
				int			iLen = 0;
				char		szName[33], szNewPassword[33], szAuthority[33];
				char		*szTrimName = NULL, *szTrimPassword = NULL;
				//changed by Frank Wu,14/N/15,20140527, for e-mail
				char		szContactAddr[128];
				char		*szTrimContactAddr = NULL;

				//if(Web_GetSetUserInfo(ptr, szName, szNewPassword, szAuthority) == TRUE)
				if(Web_GetSetUserInfo2(ptr, szName, szNewPassword, szAuthority, szContactAddr) == TRUE)
				{
					iNewAuthority = atoi(szAuthority);

					szTrimName = Cfg_RemoveWhiteSpace(szName);
					szTrimPassword = Cfg_RemoveWhiteSpace(szNewPassword);
					//changed by Frank Wu,15/N/15,20140527, for e-mail
					szTrimContactAddr = Cfg_RemoveWhiteSpace(szContactAddr);

					/***************added by john*****************/
					//if(Web_JudgeXSS_Illegal_Char(szTrimContactAddr)==FALSE)
					//{
					//	break;
					//}
					/*********************************************/

					//iReturnValue = AddUserInfo(szTrimName,szTrimPassword, iNewAuthority);
					iReturnValue = AddUserInfo2(szTrimName,szTrimPassword, iNewAuthority, szTrimContactAddr);
					pReturnBuf = Web_MakeACUUserInfoBuffer();

					if(iReturnValue == ERR_SEC_USER_ALREADY_EXISTED)
					{
						iLen += sprintf(szReturnResultSet + iLen,"%2d", 9);
					}
					else if(iReturnValue == ERR_SEC_TOO_MANY_USERS)
					{
						iLen += sprintf(szReturnResultSet + iLen,"%2d", 10);
					}
					else
					{
						iLen += sprintf(szReturnResultSet + iLen,"%2d", iReturnValue);
					}
					iLen += sprintf(szReturnResultSet + iLen,"%s",pReturnBuf);

					if(pReturnBuf != NULL)
					{
						DELETE(pReturnBuf);
						pReturnBuf = NULL;
					}

				}

				break;
			}
		case DELETE_USER_INFO:
			{	
				char		szName[33];
				char		*szTrimName = NULL;
				strncpyz(szName, ptr, sizeof(szName));
				szTrimName = Cfg_RemoveWhiteSpace(szName);
				//TRACE("DELETE_USER_INFODELETE_USER_INFODELETE_USER_INFODELETE_USER_INFO\n");
#ifdef _SHOW_WEB_INFO
				//TRACE("DELETE_USER_INFO---szTrimName:%s\n", szTrimName);
#endif

				if(szTrimName != NULL)
				{
#ifdef _SHOW_WEB_INFO
					//TRACE("DELETE_USER_INFO---szTrimName:%s\n", szTrimName);
#endif

					iReturnValue = DeleteUserInfo(szTrimName);
					if(iReturnValue == ERR_SEC_CANNOT_DELETE_ADMIN)
					{
						iReturnValue = 7;
					}
					else if(iReturnValue == ERR_SEC_CANNOT_DELETE_ENGINEER)
					{
					    iReturnValue = 12;
					}
					else if(iReturnValue == ERR_SEC_USER_NOT_EXISTED)
					{
						iReturnValue = 11;
					}

					//DELETE(szTrimName);
					//szTrimName = NULL;
				}
				else
				{
					iReturnValue = 0;
				}

				pReturnBuf = Web_MakeACUUserInfoBuffer();
				iLen += sprintf(szReturnResultSet + iLen,"%2d", iReturnValue);
				iLen += sprintf(szReturnResultSet + iLen,"%s",pReturnBuf);
				if(pReturnBuf != NULL)
				{
					DELETE(pReturnBuf);
					pReturnBuf = NULL;
				}

				break;
			}
			////////////////////////////Added by COE Pune///////////////////
			case SET_RADIUS_INFO:
			{
				if(ptr!=NULL)
				{
					SetRadiusInfo(ptr);
				}

				break;
			}
			///////////////////////////////////////////////////////////////
			//#ifdef PRODUCT_INFO_SUPPORT
			//		case GET_PRODUCT_INFO:
			//			{
			//				//pReturnBuf = Web_MakeNetWorkInfoBuffer();
			//				//printf("\nGET_PRODUCT_INFO\n");
			//
			//				Web_GetBarcode_Info(&pReturnBuf,iLanguage);
			//				sprintf(szReturnProductInfo,"%s",pReturnBuf);
			//
			//				if(pReturnBuf != NULL)
			//				{
			//					DELETE(pReturnBuf);
			//					pReturnBuf = NULL;
			//				}
			//
			//				break;
			//			}
			//#endif
			//			//////////////////////////////////////////////////////////////////////////
			//			//Added by wj for Config PLC private config file 2006.5.13
			//
			//		case SET_PLC_CONFIG :
			//			{
			//				TRACE("SET_PLC_CONFIG:%s/n",ptr);
			//
			//				//1����ptrд��PLC Configure File��ע�⻥�⣬ͬʱֻ����һ���˽����޸ģ��޸ĵ�ͬʱ���ܶ�ȡ��
			//				//2����Webҳ��p39_edit_config_plc.htm�е�PLC Configure File��Ϣ���µ���Ϣ�����滻����Web_MakePlcWebPage()ʵ��
			//				//3�����ز������iresult = 6��ʾ�ɹ���iresult = 5��ʾʧ��
			//				int itemp = 0;
			//				RunThread_Heartbeat(RunThread_GetId(NULL));
			//				iresult = Web_WritePLCCFGInfo(ptr);
			//
			//				char *pszReturn = NULL;
			//				char *ptemp = "  ;  ;  ;  ;  ;  ";
			//
			//				if(iresult == 4)
			//				{
			//
			//					iresult = 0;
			//					pszReturn = NEW_strdup(ptemp);
			//				}
			//				else
			//				{
			//					RunThread_Heartbeat(RunThread_GetId(NULL));
			//					itemp = Web_MakePlcWebPage( &pszReturn,iLanguage);
			//					if(itemp == 0 )
			//					{
			//						iresult = 0;
			//						pszReturn = NEW_strdup(ptemp);
			//
			//					}
			//					else if(itemp == 1)
			//					{
			//
			//					}
			//					else if(itemp == ERR_NO_MEMORY)
			//					{
			//						iresult = 1;
			//						pszReturn = NEW_strdup(ptemp);
			//
			//					}
			//
			//				}
			//
			//				if(iresult == 6)
			//				{
			//					sprintf(szTempUserName,"Web:%s  SetPLC  ",szUserName);
			//					AppLogOut(szTempUserName,APP_LOG_MILESTONE,ptr);
			//					TRACE("\n________ptr:%s_________",ptr);
			//				}
			//
			//				memset(szReturnResultSet,0,60000);
			//				sprintf(szReturnResultSet,"%2d%s", iresult, pszReturn);
			//
			//				DELETE(pszReturn);
			//
			//				break;
			//			}
			//
			//		case DEL_PLC_CONFIG :
			//			{
			//
			//				RunThread_Heartbeat(RunThread_GetId(NULL));
			//				//1��ɾ��PLC Configure File��ptrָ�����У�ע�⻥�⣬ͬʱֻ����һ���˽����޸ģ��޸ĵ�ͬʱ���ܶ�ȡ��
			//				//2����Webҳ��p39_edit_config_plc.htm�е�PLC Configure File��Ϣ���µ���Ϣ�����滻����Web_MakePlcWebPage()ʵ��
			//				//3�����ز������iresult = 6��ʾ�ɹ���iresult = 5��ʾʧ��,iresult = 4��ʾ�ļ�������
			//				int itemp = 0;
			//				TRACE("DEL_PLC_CONFIG:%s/n",ptr);
			//				iresult = Web_DeletePLCCFGInfo(ptr);
			//
			//				char *pszReturn = NULL;
			//				char *ptemp = "  ;  ;  ;  ;  ;  ";
			//
			//				if(iresult == 4)
			//				{
			//					iresult = 0;
			//					pszReturn = NEW_strdup(ptemp);
			//				}
			//				else
			//				{
			//					RunThread_Heartbeat(RunThread_GetId(NULL));
			//					itemp = Web_MakePlcWebPage( &pszReturn,iLanguage);
			//					if(itemp == 0 )
			//					{
			//						iresult = 0;
			//						pszReturn = NEW_strdup(ptemp);
			//
			//					}
			//					else if(itemp == 1)
			//					{
			//
			//					}
			//					else if(itemp == ERR_NO_MEMORY)
			//					{
			//						iresult = 1;
			//						pszReturn = NEW_strdup(ptemp);
			//
			//					}
			//
			//				}
			//
			//				if(iresult == 6)
			//				{
			//					sprintf(szTempUserName,"Web:%s  DelPLC  ",szUserName);
			//					AppLogOut(szTempUserName,APP_LOG_MILESTONE,ptr);
			//					TRACE("\n________ptr:%s_________",ptr);
			//				}
			//				memset(szReturnResultSet,0,60000);
			//				sprintf(szReturnResultSet,"%2d%s", iresult, pszReturn);
			//
			//				DELETE(pszReturn);
			//
			//				break;
			//			}
			//
			//		case GET_PLC_CONFIG :
			//			{
			//				//��Webҳ��p39_edit_config_plc.htm�е�PLC Configure File��Ϣ���豸��Ϣ���ź���Ϣ�����µ���Ϣ�����滻,���ڵ�һ����ʾ
			//
			//				TRACE("into GET_PLC_CONFIG!!!:%s",ptr);
			//
			//				iresult = 7;
			//				char *pszReturn = NULL;
			//				char *ptemp = "  ;  ;  ;  ;  ;  ";
			//
			//
			//				RunThread_Heartbeat(RunThread_GetId(NULL));
			//				iresult = Web_MakePlcWebPage( &pszReturn,iLanguage);
			//				if(iresult == 0 )
			//				{
			//					pszReturn = NEW_strdup(ptemp);
			//
			//				}
			//				else if(iresult == 1)
			//				{
			//					iresult = 7;
			//				}
			//				else if(iresult == ERR_NO_MEMORY)
			//				{
			//					iresult = 1;
			//					pszReturn = NEW_strdup(ptemp);
			//
			//				}
			//
			//
			//				memset(szReturnResultSet,0,60000);
			//				sprintf(szReturnResultSet,"%2d%s", iresult, pszReturn);
			//
			//				TRACE("Web_MakePlcWebPage OK!!!");
			//				DELETE(pszReturn);
			//				break;
			//			}
			//
			//			//end////////////////////////////////////////////////////////////////////////
			//
			//			//////////////////////////////////////////////////////////////////////////
			//			//Added by wj for Config Alarm Suppressing Exp 2006.5.22
			//		case GET_ALARM_CONFIG:
			//			{
			//				TRACE("into GET_ALARM_CONFIG!!!:%s",ptr);
			//
			//				iresult = 7;
			//				char *pszReturn = NULL;
			//
			//
			//				RunThread_Heartbeat(RunThread_GetId(NULL));
			//				Web_MakeAlarmSupExpWebPageBuf(ptr,&pszReturn,iLanguage);
			//
			//				memset(szReturnResultSet,0,60000);
			//				sprintf(szReturnResultSet,"%2d%s", iresult, pszReturn);
			//
			//				TRACE("Web_MakeAlarmSupExpWebPageBuf OK!!!");
			//				DELETE(pszReturn);
			//
			//				break;
			//			}
			//
			//		case SET_ALARM_CONFIG:
			//			{
			//				TRACE("into SET_ALARM_CONFIG!!!:%s",ptr);
			//
			//
			//				iresult = Web_SetAlarmSuppressingExp(ptr,szUserName);
			//				char *pszReturn = NULL;
			//
			//
			//				RunThread_Heartbeat(RunThread_GetId(NULL));
			//				Web_MakeAlarmSupExpWebPageBuf(ptr,&pszReturn,iLanguage);
			//
			//				if(iresult == 6)
			//				{
			//					sprintf(szTempUserName,"Web:%s  SetAlarm  ",szUserName);
			//					AppLogOut(szTempUserName,APP_LOG_MILESTONE,ptr);
			//					TRACE("\n________ptr:%s_________",ptr);
			//				}
			//
			//
			//				memset(szReturnResultSet,0,60000);
			//				sprintf(szReturnResultSet,"%2d%s", iresult, pszReturn);
			//
			//				TRACE("Web_MakeAlarmSupExpWebPageBuf OK!!!");
			//				DELETE(pszReturn);
			//
			//				break;
			//			}
			//
			//			//end////////////////////////////////////////////////////////////////////////
			//
			//			//////////////////////////////////////////////////////////////////////////
			//			//Added by wj for Config Alarm Relay 2006.6.22
			//		case GET_ALARMREG_CONFIG:
			//			{
			//				TRACE("into GET_ALARMREG_CONFIG!!!:%s",ptr);
			//
			//				iresult = 7;
			//				char *pszReturn = NULL;
			//
			//
			//				RunThread_Heartbeat(RunThread_GetId(NULL));
			//				Web_MakeAlarmRegWebPageBuf(ptr,&pszReturn,iLanguage);
			//
			//				memset(szReturnResultSet,0,60000);
			//				sprintf(szReturnResultSet,"%2d%s", iresult, pszReturn);
			//
			//				TRACE("Web_MakeAlarmRegWebPageBuf OK!!!");
			//				DELETE(pszReturn);
			//
			//				break;
			//			}
			//
			//		case SET_ALARMREG_CONFIG:
			//			{
			//				TRACE("into SET_ALARMREG_CONFIG!!!:%s",ptr);
			//
			//
			//				iresult = Web_SetAlarmRelay(ptr,szUserName);
			//				char *pszReturn = NULL;
			//
			//
			//				RunThread_Heartbeat(RunThread_GetId(NULL));
			//				Web_MakeAlarmRegWebPageBuf(ptr,&pszReturn,iLanguage);
			//
			//
			//				memset(szReturnResultSet,0,60000);
			//				sprintf(szReturnResultSet,"%2d%s", iresult, pszReturn);
			//
			//				TRACE("Web_MakeAlarmRegWebPageBuf OK!!!");
			//				DELETE(pszReturn);
			//
			//				break;
			//			}
			//
			//			//end////////////////////////////////////////////////////////////////////////
					//changed by Frank Wu,18/N/27,20140527, for power split
					case GET_GC_PS_CONFIG:
						{
							TRACE("into GET_GC_PS_CONFIG!!!:%s",ptr);
							iresult = 7;
							char *pszReturn = NULL;
							//char *ptemp = "  ;  ;  ;  ;  ;  ;   ";

							RunThread_Heartbeat(RunThread_GetId(NULL));
							iresult = Web_MakeGCWebPage( &pszReturn,iLanguage);
							if(1 == iresult)
							{
								iresult = 1;
							}
							else
							{
								if(0 == iresult)
								{
									pszReturn = NEW_strdup("process failed!");
								}
								else if(ERR_NO_MEMORY == iresult)
								{
									pszReturn = NEW_strdup("no memory!");
								}

								iresult = 0;
							}

							//if(iresult == 0 )
							//{
							//	pszReturn = NEW_strdup(ptemp);

							//}
							//else if(iresult == 1)
							//{
							//	iresult = 7;
							//}
							//else if(iresult == ERR_NO_MEMORY)
							//{
							//	iresult = 1;
							//	pszReturn = NEW_strdup(ptemp);

							//}

							//TRACE("___iLen:%d_______\n",strlen(pszReturn));
							TRACE("into Web_MakeGCWebPage!!!End\n");

							//changed by Frank Wu,19/N/27,20140527, for power split
							//memset(szReturnResultSet,0,1600000);
							//sprintf(szReturnResultSet,"%2d%s", iresult, pszReturn);
							iLargeResultValue = iresult;//0, failed; 1, success
							pLargeResultSet = pszReturn;//must free it later
							pszReturn = NULL;

							TRACE("Web_MakeGCWebPage OK!!!");
							SAFELY_DELETE(pszReturn);
							break;
						}

					case SET_GC_PS_MODE:
						{
							TRACE("into SET_GC_PS_MODE!!!:%s\n",ptr);
							//int itemp = 0;
							iresult = 7;
							char *pszReturn = NULL;
							//char *ptemp = "  ;  ;  ;  ;  ;  ;   ";

							iresult = Web_ModifyGCPSMode(ptr);
							if(6 == iresult)
							{
								iresult = 1;//success
							}
							else
							{
								if(4 == iresult)
								{
									pszReturn = NEW_strdup("open file failed!");
								}
								else if(5 == iresult)
								{
									pszReturn = NEW_strdup("try to lock failed!");
								}
								else
								{
									pszReturn = NEW_strdup("set mode failed!");
								}
								iresult = 0;//failed
							}

							//if(iresult == 4)
							//{
							//	iresult = 0;
							//	pszReturn = NEW_strdup(ptemp);
							//}
							//else
							//{
							//	RunThread_Heartbeat(RunThread_GetId(NULL));
							//	itemp = Web_MakeGCWebPage( &pszReturn,iLanguage);
							//	if(itemp == 0 )
							//	{
							//		iresult = 0;
							//		pszReturn = NEW_strdup(ptemp);

							//	}
							//	else if(itemp == 1)
							//	{

							//	}
							//	else if(itemp == ERR_NO_MEMORY)
							//	{
							//		iresult = 1;
							//		pszReturn = NEW_strdup(ptemp);

							//	}
							//}

							TRACE("into Web_MakeGCWebPage!!!End\n");

							//changed by Frank Wu,20/N/27,20140527, for power split
							//memset(szReturnResultSet,0,60000);
							//sprintf(szReturnResultSet,"%2d%s", iresult, pszReturn);
							iLargeResultValue = iresult;//0, failed; 1, success
							pLargeResultSet = pszReturn;//must free it later
							pszReturn = NULL;

							TRACE("Web_MakeGCWebPage OK!!!");
							SAFELY_DELETE(pszReturn);
							break;
						}

					case SET_GC_PS_CONFIG:
						{
							TRACE("into SET_GC_PS_CONFIG!!!:%s",ptr);
							//int itemp = 0;
							iresult = 7;
							char *pszReturn = NULL;
							//char *ptemp = "  ;  ;  ;  ;  ;  ;   ";

							iresult = Web_ModifyGCPSInfo(ptr);
							if(6 == iresult)
							{
								iresult = 1;//success
							}
							else
							{
								if(5 == iresult)
								{
									pszReturn = NEW_strdup("set ps config failed1!");
								}
								else
								{
									pszReturn = NEW_strdup("set ps config failed2!");
								}
								iresult = 0;//failed
							}

							//if(iresult == 4)
							//{
							//	iresult = 0;
							//	pszReturn = NEW_strdup(ptemp);
							//}
							//else
							//{
							//	RunThread_Heartbeat(RunThread_GetId(NULL));
							//	itemp = Web_MakeGCWebPage( &pszReturn,iLanguage);
							//	if(itemp == 0 )
							//	{
							//		iresult = 0;
							//		pszReturn = NEW_strdup(ptemp);

							//	}
							//	else if(itemp == 1)
							//	{

							//	}
							//	else if(itemp == ERR_NO_MEMORY)
							//	{
							//		iresult = 1;
							//		pszReturn = NEW_strdup(ptemp);

							//	}
							//}

							TRACE("into Web_MakeGCWebPage!!!End\n");

							//changed by Frank Wu,21/N/27,20140527, for power split
							//memset(szReturnResultSet,0,60000);
							//sprintf(szReturnResultSet,"%2d%s", iresult, pszReturn);
							iLargeResultValue = iresult;
							pLargeResultSet = pszReturn;//must free it later
							pszReturn = NULL;

							TRACE("Web_MakeGCWebPage OK!!!");
							SAFELY_DELETE(pszReturn);
							break;
						}
			//
			//
			//			//////////////////////////////////////////////////////////////////////////
			//			//Added by wj for YDN Setting Config

		case GET_YDN_CONFIG:
			{
				pReturnBuf = Web_MakeYDNPrivateConfigreBuffer();

				if(pReturnBuf != NULL)
				{
					if(Web_GetProtectedStatus() == TRUE)
					{
						iLen += sprintf(szReturnResultSet + iLen,"%2d%s",15,pReturnBuf);//WEB_RETURN_PROTECTED_ERROR
					}
					else
					{
						iLen += sprintf(szReturnResultSet + iLen,"%2d%s",99,pReturnBuf);
					}
					DELETE(pReturnBuf);
					pReturnBuf = NULL;
				}
				else
				{
					iLen += sprintf(szReturnResultSet + iLen,"%2d",1);
				}

				break;
			}

		case SET_YDN_CONFIG:
			{
				iReturnValue = Web_ModifyYDNPrivateConfigure(ptr,szUserName); 
				pReturnBuf = Web_MakeYDNPrivateConfigreBuffer();

				iLen += sprintf(szReturnResultSet + iLen,"%2d", iReturnValue);


				if(pReturnBuf != NULL)
				{
					iLen += sprintf(szReturnResultSet + iLen,"%s",pReturnBuf);

					DELETE(pReturnBuf);
					pReturnBuf = NULL;
				}
				else
				{
				}
				break;
			}

		case GET_MODBUS_CONFIG:
			{
				pReturnBuf = Web_MakeMODBUSPrivateConfigreBuffer();

				if(pReturnBuf != NULL)
				{
					if(Web_GetProtectedStatus() == TRUE)
					{
						iLen += sprintf(szReturnResultSet + iLen,"%2d%s",15,pReturnBuf);//WEB_RETURN_PROTECTED_ERROR
					}
					else
					{
						iLen += sprintf(szReturnResultSet + iLen,"%2d%s",99,pReturnBuf);
					}
					DELETE(pReturnBuf);
					pReturnBuf = NULL;
				}
				else
				{
					iLen += sprintf(szReturnResultSet + iLen,"%2d",1);
				}

				break;
			}

		case SET_MODBUS_CONFIG:
			{
				iReturnValue = Web_ModifyMODBUSPrivateConfigure(ptr,szUserName); 
				pReturnBuf = Web_MakeMODBUSPrivateConfigreBuffer();

				iLen += sprintf(szReturnResultSet + iLen,"%2d", iReturnValue);


				if(pReturnBuf != NULL)
				{
					iLen += sprintf(szReturnResultSet + iLen,"%s",pReturnBuf);

					DELETE(pReturnBuf);
					pReturnBuf = NULL;
				}
				else
				{
				}
				break;
			}

			case GET_SETTING_PARAM:
			{
			    TRACE_WEB_USER_NOT_CYCLE("GET_SETTING_PARAM!!!\n");
			    RunThread_Heartbeat(RunThread_GetId(NULL));
			    iresult = Web_MakeGetSetParamWebPage(FALSE);

			    TRACE_WEB_USER_NOT_CYCLE("GET_SETTING_PARAM!!!End %d\n",iresult);

			    memset(szReturnResultSet, 0, sizeof(szReturnResultSet));
			    sprintf(szReturnResultSet,"%2d", iresult);

			    TRACE_WEB_USER_NOT_CYCLE("Web_MakeGetSetParamWebPage OK!!!");

			    break;
			}

		case GET_AUTO_CONFIG:
			{
				TRACE("\n into GET_AUTO_CONFIG!!!\n");
				/*RunThread_Heartbeat(RunThread_GetId(NULL));
				HANDLE hCmdThread = RunThread_Create("Auto Config",
					(RUN_THREAD_START_PROC)DIX_AutoConfigMain,
					(void*)NULL,
					(DWORD*)NULL,
					0);*/
				AppLogOut(CGI_APP_LOG_CONTROL_NAME,APP_LOG_INFO,"Start Auto Config, System will be reboot!");
				close(fd2);

				char szFullPath[MAX_FILE_PATH];
				char szCmdLine[128];

				Cfg_GetFullConfigPath(LOADALLEQUIP_FLAGFILE, szFullPath, MAX_FILE_PATH);
				snprintf(szCmdLine, sizeof(szCmdLine), "rm -rf %s",
					szFullPath);	
				_SYSTEM(szCmdLine);
				_SYSTEM("reboot");

				
				return TRUE;

				break;
			}
		case GET_SMTP_CONFIG: 
			{
				pReturnBuf = Web_MakeSMTPBuffer();
				if(pReturnBuf != NULL)
				{
					if(Web_GetProtectedStatus() == TRUE)
					{
						iLen += sprintf(szReturnResultSet + iLen,"%2d%s",15,pReturnBuf);//WEB_RETURN_PROTECTED_ERROR
					}
					else
					{
						iLen += sprintf(szReturnResultSet + iLen,"%2d%s",99,pReturnBuf);
					}
					DELETE(pReturnBuf);
					pReturnBuf = NULL;

					//printf("%s",szReturnResultSet);
				}
				else
				{
					iLen += sprintf(szReturnResultSet + iLen,"%2d  ",1);
				}


				break;
			}

		case SET_SMTP_CONFIG:
			{
				iReturnValue = Web_ModifySMTPPrivateConfigure(ptr,szUserName); 
				pReturnBuf = Web_MakeSMTPBuffer();

				iLen += sprintf(szReturnResultSet + iLen,"%2d", iReturnValue);


				if(pReturnBuf != NULL)
				{
					iLen += sprintf(szReturnResultSet + iLen,"%s",pReturnBuf);

					DELETE(pReturnBuf);
					pReturnBuf = NULL;
				}
				else
				{
					iLen += sprintf(szReturnResultSet + iLen,"%s","  ");

				}

				break;
			}

		case GET_DHCPSERVER_CONFIG: 
			{
				pReturnBuf = Web_MakeDHCPServerBuffer();
				if(pReturnBuf != NULL)
				{
					if(Web_GetProtectedStatus() == TRUE)
					{
						iLen += sprintf(szReturnResultSet + iLen,"%2d%s",15,pReturnBuf);//WEB_RETURN_PROTECTED_ERROR
					}
					else
					{
						iLen += sprintf(szReturnResultSet + iLen,"%2d%s",99,pReturnBuf);
					}
					DELETE(pReturnBuf);
					pReturnBuf = NULL;
				}
				else
				{
					iLen += sprintf(szReturnResultSet + iLen,"%2d  ",1);
				}


				break;
			}

		case GET_V6_DHCPSERVER_CONFIG: 
		    {
			TRACE_WEB_USER_NOT_CYCLE("Deal with GET_V6_DHCPSERVER_CONFIG\n");
			pReturnBuf = Web_MakeV6DHCPServerBuffer();
			if(pReturnBuf != NULL)
			{
			    if(Web_GetProtectedStatus() == TRUE)
			    {
				iLen += sprintf(szReturnResultSet + iLen,"%2d%s",15,pReturnBuf);//WEB_RETURN_PROTECTED_ERROR
			    }
			    else
			    {
				iLen += sprintf(szReturnResultSet + iLen,"%2d%s",99,pReturnBuf);
			    }
			    TRACE_WEB_USER_NOT_CYCLE("szReturnResultSet is %s\n", szReturnResultSet);
			    DELETE(pReturnBuf);
			    pReturnBuf = NULL;
			}
			else
			{
			    iLen += sprintf(szReturnResultSet + iLen,"%2d  ",1);
			}


			break;
		    }

		case GET_VPN_CONFIG:
			{
				pReturnBuf = Web_MakeVPNBuffer();
				if(pReturnBuf != NULL)
				{
					if(Web_GetProtectedStatus() == TRUE)
					{
						iLen += sprintf(szReturnResultSet + iLen,"%2d%s",15,pReturnBuf);//WEB_RETURN_PROTECTED_ERROR
					}
					else
					{
						iLen += sprintf(szReturnResultSet + iLen,"%2d%s",99,pReturnBuf);
					}
					DELETE(pReturnBuf);
					pReturnBuf = NULL;
				}
				else
				{
					iLen += sprintf(szReturnResultSet + iLen,"%2d  ",1);
				}


				break;

			}

		case SET_VPN_CONFIG:
			{
				iReturnValue = Web_ModifyVPNPrivateConfigure(ptr,szUserName); 
				pReturnBuf = Web_MakeVPNBuffer();

				iLen += sprintf(szReturnResultSet + iLen,"%2d", iReturnValue);


				if(pReturnBuf != NULL)
				{
					iLen += sprintf(szReturnResultSet + iLen,"%s",pReturnBuf);

					DELETE(pReturnBuf);
					pReturnBuf = NULL;
				}
				else
				{
					iLen += sprintf(szReturnResultSet + iLen,"%s","  ");
				}

				break;

			}

		case GET_SMS_CONFIG:
			{
				pReturnBuf = Web_MakeSMSBuffer();
				if(pReturnBuf != NULL)
				{
					if(Web_GetProtectedStatus() == TRUE)
					{
						iLen += sprintf(szReturnResultSet + iLen,"%2d%s",15,pReturnBuf);//WEB_RETURN_PROTECTED_ERROR
					}
					else
					{
						iLen += sprintf(szReturnResultSet + iLen,"%2d%s",99,pReturnBuf);
					}
					DELETE(pReturnBuf);
					pReturnBuf = NULL;
				}
				else
				{
					iLen += sprintf(szReturnResultSet + iLen,"%2d  ",1);
				}


				break;

			}

		case SET_SMS_CONFIG:
			{
				iReturnValue = Web_ModifySMSPrivateConfigure(ptr,szUserName); 
				pReturnBuf = Web_MakeSMSBuffer();

				iLen += sprintf(szReturnResultSet + iLen,"%2d", iReturnValue);


				if(pReturnBuf != NULL)
				{
					iLen += sprintf(szReturnResultSet + iLen,"%s",pReturnBuf);

					DELETE(pReturnBuf);
					pReturnBuf = NULL;
				}
				else
				{
					iLen += sprintf(szReturnResultSet + iLen,"%s","  ");
				}
				break;
			}

		case SET_NMSV3_INFO:
		    TRACE_WEB_USER_NOT_CYCLE("Deal with SET_NMSV3_INFO\n");
		    iReturnValue = Web_ModifyNMSV3PrivateConfigure(ptr);
		    pReturnBuf = Web_MakeNMSV3PrivateConfigureBuffer();

		    if(g_SiteInfo.iSNMPV3Flag == NO_SNMPV3)
		    {
			iLen += sprintf(szReturnResultSet + iLen,"%2d", WEB_RETURN_NOT_SUPPORT_V3);
		    }
		    else
		    {
			iLen += sprintf(szReturnResultSet + iLen,"%2d", iReturnValue);
		    }
		    if(pReturnBuf != NULL)
		    {
			iLen += sprintf(szReturnResultSet + iLen,"%s",pReturnBuf);
		    }
		    TRACE_WEB_USER_NOT_CYCLE("szReturnResultSet is %s\n", szReturnResultSet);

		    if(pReturnBuf != NULL)
		    {
			DELETE(pReturnBuf);
			pReturnBuf = NULL;
		    }
		    break;

		case GET_NMSV3_INFO:
		    TRACE_WEB_USER_NOT_CYCLE("Deal with GET_NMSV3_INFO\n");
		    pReturnBuf = Web_MakeNMSV3PrivateConfigureBuffer();
		    if(Web_GetProtectedStatus() == TRUE)
		    {
			iLen += sprintf(szReturnResultSet + iLen,"%2d", WEB_RETURN_PROTECTED_ERROR_NMS);
		    }
		    else if(g_SiteInfo.iSNMPV3Flag == NO_SNMPV3)
		    {
			iLen += sprintf(szReturnResultSet + iLen,"%2d", WEB_RETURN_NOT_SUPPORT_V3);
		    }
		    else
		    {
			iLen += sprintf(szReturnResultSet + iLen,"%2d", 1);
		    }
		    if(pReturnBuf != NULL)
		    {
			iLen += sprintf(szReturnResultSet + iLen,"%s",pReturnBuf);
		    }
		    TRACE_WEB_USER_NOT_CYCLE("szReturnResultSet is %s\n", szReturnResultSet);


		    if(pReturnBuf != NULL)
		    {
			DELETE(pReturnBuf);
			pReturnBuf = NULL;
		    }

		    break;

		case SET_SNMP_TRAP:
		    TRACE_WEB_USER_NOT_CYCLE("Deal with SET_SNMP_TRAP\n");
		    iReturnValue = Web_ModifyNMSTrapConfigure(ptr);
		    pReturnBuf = Web_MakeNMSTrapConfigureBuffer();

		    iLen += sprintf(szReturnResultSet + iLen,"%2d", iReturnValue);
		    iLen += sprintf(szReturnResultSet + iLen,"%s",pReturnBuf);

		    TRACE_WEB_USER_NOT_CYCLE("szReturnResultSet is %s\n", szReturnResultSet);

		    if(pReturnBuf != NULL)
		    {
			DELETE(pReturnBuf);
			pReturnBuf = NULL;
		    }
		    break;

		case GET_SNMP_TRAP:
		    TRACE_WEB_USER_NOT_CYCLE("Deal with GET_SNMP_TRAP\n");
		    pReturnBuf = Web_MakeNMSTrapConfigureBuffer();
		    TRACE_WEB_USER_NOT_CYCLE("pReturnBuf is %s\n", pReturnBuf);
		    if(Web_GetProtectedStatus() == TRUE)
		    {
			iLen += sprintf(szReturnResultSet + iLen,"%2d", WEB_RETURN_PROTECTED_ERROR_NMS);
		    }
		    else
		    {
			iLen += sprintf(szReturnResultSet + iLen,"%2d", 1);
		    }
		    iLen += sprintf(szReturnResultSet + iLen,"%s",pReturnBuf);
		    TRACE_WEB_USER_NOT_CYCLE("szReturnResultSet is %s\n", szReturnResultSet);
#ifdef _SHOW_WEB_INFO
		    //TRACE("szReturnResultSet : %s\n", szReturnResultSet);
#endif
		    if(pReturnBuf != NULL)
		    {
			DELETE(pReturnBuf);
			pReturnBuf = NULL;
		    }
		    break;
			//end////////////////////////////////////////////////////////////////////////

		case EXPORT_SETTINGS:
		    Web_MakeSettingsInfoFile();
		    sprintf(szReturnResultSet,"%2d", 0);	//TODO: Right now Web_MakeSettingsInfoFile() returns ONLY '0'
		break;
		case LOG_OUT:
		    {
		 	TRACE_WEB_USER_NOT_CYCLE("Logout the controller!\n");
			break;
		    }

		default:
			break;
		}

		/* FILE *pf = NULL;
		pf = fopen("/var/replaceStr.txt","a+");
		fwrite(szReturnResultSet,strlen(szReturnResultSet),1,pf);
		fclose(pf);*/

		//printf("szReturnResultSet : %s\n", szReturnResultSet);

#ifdef _SHOW_WEB_INFO
		//TRACE("szReturnResultSet : %s\n", szReturnResultSet);
#endif		
		if(iCommandType != GET_PRODUCT_INFO && 
			iCommandType != GET_PLC_CONFIG && 
			iCommandType != SET_PLC_CONFIG && 
			iCommandType != DEL_PLC_CONFIG &&
			iCommandType != GET_ALARM_CONFIG &&
			iCommandType != SET_ALARM_CONFIG &&
			iCommandType != GET_ALARMREG_CONFIG &&
			iCommandType != SET_ALARMREG_CONFIG &&
			iCommandType != GET_GC_PS_CONFIG && 
			iCommandType != SET_GC_PS_CONFIG &&
			iCommandType != SET_GC_PS_MODE )
		{

			if((write(fd2,szReturnResultSet,4095)) < 0)
			{
				AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_WARNING,"Fail to write FIFO when write the control result!");
				close(fd2);
				char			szCommand[30];
				iLen = sprintf(szCommand,"kill ");
				strncpyz(szCommand + iLen,buf, MAX_COMM_PID_LEN + 1);

				TRACEX("%s\n", szCommand);
				_SYSTEM(szCommand);
				return FALSE;
			}
		}
		//changed by Frank Wu,22/N/27,20140527, for power split
		else if( (iCommandType == GET_GC_PS_CONFIG)
			|| (iCommandType == SET_GC_PS_CONFIG)
			|| (iCommandType == SET_GC_PS_MODE) )
		{
			char szP[PIPE_BUF];
			int iBlockSize = PIPE_BUF - 5;//must be smaller than (PIPE_BUF - 2)
			int iTotalSize = (NULL == pLargeResultSet)? 0: strlen(pLargeResultSet);
			int iCurPos = 0;
			int iCopySize = 0;

			snprintf(szP, 10, "%2d\0", iLargeResultValue);//first block,just 2 bytes
			do
			{
				if(write(fd2,szP, PIPE_BUF - 1) < 0)//each string must be end with '\0'
				{
					SAFELY_DELETE(pLargeResultSet);

					AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_WARNING,"Fail to write FIFO when write the control result!");
					close(fd2);
					char			szCommand[30];
					iLen = sprintf(szCommand,"kill ");
					strncpyz(szCommand + iLen,buf, MAX_COMM_PID_LEN + 1);

					TRACEX("%s\n", szCommand);
					_SYSTEM(szCommand);
					return FALSE;
				}
				if(iCurPos < iTotalSize)
				{
					iCopySize = iTotalSize - iCurPos;
					iCopySize = (iCopySize > iBlockSize)? iBlockSize: iCopySize;
					strncpyz(szP, pLargeResultSet + iCurPos, iCopySize + 1);
					iCurPos += iCopySize;
				}
				else
				{
					break;
				}

			}while(1);

			SAFELY_DELETE(pLargeResultSet);
		}
		//////////////////////////////////////////////////////////////////////////
		//Added by wj for PLC Config
		else if(iCommandType == GET_PLC_CONFIG || 
			iCommandType == SET_PLC_CONFIG || 
			iCommandType == DEL_PLC_CONFIG ||
			iCommandType == GET_ALARM_CONFIG ||
			iCommandType == SET_ALARM_CONFIG ||
			iCommandType == GET_ALARMREG_CONFIG ||
			//changed by Frank Wu,23/N/27,20140527, for power split
			//iCommandType == SET_ALARMREG_CONFIG ||
			iCommandType == SET_ALARMREG_CONFIG
			//iCommandType == GET_GC_PS_CONFIG ||
			//iCommandType == SET_GC_PS_CONFIG ||
			//iCommandType == SET_GC_PS_MODE)
			)
		{
			int iPipeLen = strlen(szReturnResultSet)/(PIPE_BUF - 1)  + 2;
			if(iPipeLen <= 1)
			{
				if((write(fd2, szReturnResultSet, strlen(szReturnResultSet) + 1)) < 0)
				{
					close(fd2);
					return FALSE;
				}
			}
			else
			{
				int m;
				char szP[PIPE_BUF - 1];
				for(m = 0; m < iPipeLen; m++)
				{
					strncpyz(szP, szReturnResultSet + m * (PIPE_BUF - 2), PIPE_BUF - 1);
					if((write(fd2, szP, PIPE_BUF - 1)) < 0)
					{
						close(fd2);
						return FALSE;
					}
				}

			}
		}
		//end////////////////////////////////////////////////////////////////////////  
		else
		{
			/*int iPipeLen = strlen(szReturnProductInfo)/(PIPE_BUF - 1)  + 1;
			if(iPipeLen <= 1)
			{
			if((write(fd2, szReturnProductInfo, strlen(szReturnProductInfo) + 1)) < 0)
			{
			close(fd2);
			return FALSE;
			}
			}
			else
			{
			int m;
			char szP[PIPE_BUF - 1];
			for(m = 0; m < iPipeLen; m++)
			{
			strncpyz(szP, szReturnProductInfo + m * (PIPE_BUF - 2), PIPE_BUF - 1);
			if((write(fd2, szP, PIPE_BUF - 1)) < 0)
			{
			close(fd2);
			return FALSE;
			}
			}

			}*/
		}

		close(fd2);
		return TRUE;
	}
	else
	{
		return FALSE;
	}

}

/*==========================================================================*
* FUNCTION :  CloseACU
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
*==========================================================================*/
static void CloseACU(void)
{
    //stop acu
	_SYSTEM("killall g3_lui");
	raise(SIGTERM); 
	raise(SIGTERM); 
    
}

/*==========================================================================*
* FUNCTION :  StartWebCommunicate
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN int *iQuitCommand:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-6 15:01
*==========================================================================*/
int StartWebCommunicate(IN int *iQuitCommand)
{
	int				fd;
	int				iLen;
	char			buf[4096] = {0};   // fifo read buffer ,get parameter from CGI
	char			filename[FIFO_NAME_LEN] = {0};  //fifo write buffer,init according to the CGI process
	mode_t			mode = 0777;  //open fifo mode ,
	char			*ptr = NULL;
	char			szPid[8] = {0};
	char			szControlType[8] = {0};
	int				iControltype = 0;
	BOOL			bContinueRunning = TRUE;
	int				iReturnError = 0;
	char szCommand[128];
	//int				iTimerSet = 0;
	//char			szCommand1[128];
	//int				iWorkStatus = 0;
	//SIG_BASIC_VALUE		*pSigValue;
	//int		iRst = 0;
	//int			iError = 0;
	APP_SERVICE		*WEB_AppService = NULL;
	WEB_AppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_WEB_USER_NAME);
	TRACE_WEB_USER_NOT_CYCLE("Begin the Web User task\n");
	//��ʼ���ļ���Ȩ��
	char	szCommandStr[30] = "rm -f ";
	strcat(szCommandStr,MAIN_FIFO_NAME);
	_SYSTEM(szCommandStr);
//	Web_Arping();
	
	/*if(Web_ReadStatusConfig(&stStatusConfig) != ERR_WEB_OK)
	{
		AppLogOut(CGI_APP_LOG_COMM_NAME, APP_LOG_WARNING, "Error : Fail to read web private configuration");
	}*/

	umask(0);
	if((mkfifo(MAIN_FIFO_NAME,mode))<0)
	{
		return FALSE;
	}
	//make the umask to be the default value in case introduce some problem.
	umask(0022);

	struct stat statBuf;
	stat(MAIN_FIFO_NAME, &statBuf);

	if(statBuf.st_mode != 010777)
	{// add write permission if it not 777
		//printf("get permission\n");
		sprintf(szCommand, "chmod 777 %s", MAIN_FIFO_NAME);
		_SYSTEM(szCommand);
		//stat(MAIN_FIFO_NAME, &statBuf);
		//printf("mode2--------[%o]\n",statBuf.st_mode);
	}

	sprintf(szCommand, "chmod 777 %s", HTML_SAVE_REAL_PATH);
	_SYSTEM(szCommand);

	sprintf(szCommand, "chmod 777 %s", HTML_SAVE_ENG_PATH);
	_SYSTEM(szCommand);

	sprintf(szCommand,"mkdir %s", WEB_LOC_VAR);
	_SYSTEM(szCommand);

	sprintf(szCommand,"chmod 777 %s", WEB_LOC_VAR);
	_SYSTEM(szCommand);

	sprintf(szCommand,"mkdir %s", WEB_ENG_VAR);
	_SYSTEM(szCommand);

	sprintf(szCommand,"chmod 777 %s", WEB_ENG_VAR);
	_SYSTEM(szCommand);

	sprintf(szCommand,"chmod -R 777 %s", "/app");
	_SYSTEM(szCommand);

	strcpy(szCommand, "mkdir /var/datas/");
	_SYSTEM(szCommand);

	strcpy(szCommand, "chmod -R 777 /var/datas/");
	_SYSTEM(szCommand);

	/*strcpy(szCommand, "cp /app/www_user/html/datas/data.consumption_map.html /var/datas");
	_SYSTEM(szCommand);*/

	if((fd = open(MAIN_FIFO_NAME,O_RDONLY | O_NONBLOCK))<0)
	{
		return FALSE;
	}

	RunThread_Heartbeat(RunThread_GetId(NULL));

	//DxiRegisterNotification("Web User", 
	//	ServiceNotification,
	//	NULL, // it shall be changed to the actual arg
	//	(_ALARM_OCCUR_MASK | _ALARM_CLEAR_MASK | _SET_MASK | _CONFIG_MASK), 
	//	RunThread_GetId(NULL),
	//	_REGISTER_FLAG);

	signal(SIGPIPE, SIG_IGN);

	//�����ҳ����ID
	if(StartTranslateFile(iQuitCommand) == FALSE)
	{
		AppLogOut(CGI_APP_LOG_COMM_NAME, APP_LOG_WARNING, "Error : Fail to read web resource  configuration");
	}

	RunThread_Heartbeat(RunThread_GetId(NULL));

	g_hMutexLoadCurrent = Mutex_Create(TRUE);
	g_hMutexSetDataFile = Mutex_Create(TRUE);
	//changed by Frank Wu,24/N/27,20140527, for power split
	s_hMutexLoadGCCFGFile = Mutex_Create(TRUE);

	HANDLE hCmdThread = RunThread_Create("Json File",
		(RUN_THREAD_START_PROC)Web_MakeDataFile,
		iQuitCommand,
		(DWORD*)NULL,
		RUN_THREAD_FLAG_HAS_MSG);

	HANDLE hCmdThread1 = RunThread_Create("NotificationProcess",
	    (RUN_THREAD_START_PROC)Web_Notification,
	    iQuitCommand,
	    (DWORD*)NULL,
	    RUN_THREAD_FLAG_HAS_MSG);

	HANDLE hCmdThread2 = RunThread_Create("Efficiency Tracker",
	    (RUN_THREAD_START_PROC)WEB_EfficiencyTracker,
	    iQuitCommand,
	    (DWORD*)NULL,
	    RUN_THREAD_FLAG_HAS_MSG);

#ifdef G3_OPT_DEBUG_THREAD
	AppLogOut("G3 DEBUG THREAD", APP_LOG_UNUSED,
		"Thread G3 WEB Service was created, Thread Id = %d.\n", gettid());
#endif	
	if(WEB_AppService != NULL)
	{
		WEB_AppService->bReadyforNextService = TRUE;
	}
	while (bContinueRunning)
	{
		if(StartFlage == 0)
		{
			iRollFlage++;
			if(iRollFlage > 50)
			{
				StartFlage = 1;
				/*if(iWorkStatus == 1)
				{
					RunThread_Heartbeat(RunThread_GetId(NULL));
					HANDLE hCmdThread = RunThread_Create("Auto Config",
						(RUN_THREAD_START_PROC)DIX_AutoConfigMain,
						(void*)NULL,
						(DWORD*)NULL,
						0);
					Sleep(300);
				}*/
			}
		}
		//#ifdef AUTO_CONFIG
		int	iRst1;
		RUN_THREAD_MSG msg1;

		iRst1 = RunThread_GetMessage(RunThread_GetId(NULL), &msg1, FALSE, 0);

		if(iRst1 == ERR_OK)
		{
			TRACE("msg1.dwMsg : %d\n",msg1.dwMsg);
			if(msg1.dwMsg == MSG_AUTOCONFIG)
			{
				switch(msg1.dwParam1)
				{
				case ID_AUTOCFG_START:
					TRACE("WEB START AUTO CONFIG!!!\n");
					iAutoCFGStartFlage = 1;
					break;

				case ID_AUTOCFG_REFRESH_DATA:
					TRACE("WEB START AUTO CONFIG REFRESH!!!\n");
					//Web_MakeRectifierRefresh(FALSE);
					TRACE("Web_MakeRectifierRefresh!!!\n");
					iAutoCFGStartFlage = 0;
					break;
				default:
					break;
				}

			}
		}
		//#endif
		iSlaveMode = GetSlaveMode();
		while ((WaitFiledReadable(fd, 1000) > 0))
		{
			RunThread_Heartbeat(RunThread_GetId(NULL));
			if((iLen = read(fd,buf,PIPE_BUF)) > 0)
			{;}
			else
			{
				Sleep(1000);//yield
				break;
			}
			iSlaveMode = GetSlaveMode();
			buf[iLen] = '\0';
			ptr = buf;
			strncpyz(szPid,ptr, MAX_COMM_PID_LEN +1);
			if (atoi(szPid) > 0)
			{
				sprintf(filename,"%s/fifo.%-d",CGI_CLIENT_FIFO_PATH,atoi(szPid));
				ptr = ptr + MAX_COMM_PID_LEN;
				strncpyz(szControlType,ptr,MAX_COMM_GET_TYPE + 1);
				iControltype = atoi(szControlType);
				ptr = ptr + MAX_COMM_GET_TYPE;

				switch(iControltype)
				{
				case LOG_IN:
					{
						TRACE_WEB_USER_NOT_CYCLE("Deal with Login in\n");
						iReturnError = Web_SendEquipList(buf, filename);
						break;
					}
				case HISTORY_QUERY:
					{
						TRACE_WEB_USER_NOT_CYCLE("Deal with History Query\n");
						iReturnError = Web_SendQueryData(buf,filename);
						break;
					}
				case CONTROL_COMMAND:
					{
						TRACE_WEB_USER_NOT_CYCLE("Deal with controlling!\n");
						iReturnError = Web_SendControlCommandResult(buf,filename);
						break;
					}
				case CONFIGURE_MANAGE:
					{	
						////TRACE("Web_SendConfigureData \n");
						//TRACE("\n_____GetBuffer2: buf %s \n iLen : %d \n",buf, iLen);
						TRACE_WEB_USER_NOT_CYCLE("Deal with Config\n");
						iReturnError = Web_SendConfigureData(buf,filename);
						break;
					}
				case FILE_MANAGE:
				    {
					TRACE_WEB_USER_NOT_CYCLE("Close the controller!\n");
					CloseACU();
					_SYSTEM("mount -t tmpfs tmpfs /usb -o size=16M");
					break;
				    }
				default:
					{
						break;
					}
				}
			}
		}
		RUN_THREAD_MSG msg3;
		if(RunThread_GetMessage(RunThread_GetId(NULL), &msg3, FALSE, 0) == ERR_THREAD_OK)
		{
			if(msg3.dwMsg == MSG_AUTOCONFIG)
			{
				switch(msg3.dwParam1)
				{
				case ID_AUTOCFG_START:
					TRACE("WEB START AUTO CONFIG!!!\n");
					iAutoCFGStartFlage = 1;
					break;

				case ID_AUTOCFG_REFRESH_DATA:
					TRACE("WEB START AUTO CONFIG REFRESH!!!\n");
					//Web_MakeRectifierRefresh(FALSE);
					TRACE("Web_MakeRectifierRefresh!!!\n");
					iAutoCFGStartFlage = 0;
					break;
				default:
					break;
				}
			}
			if(msg3.dwMsg == MSG_CONIFIRM_ID)
			{
				//Web_MakeRectifierRefresh(TRUE);
				iAutoCFGStartFlage = 2;
			}
		}
		else if(iAutoCFGStartFlage == 3)
		{
			iAutoCFGStartFlage = 0;
		}
		RunThread_Heartbeat(RunThread_GetId(NULL));
		if (*iQuitCommand == SERVICE_STOP_RUNNING)
		{
			bContinueRunning = FALSE;
		}
	}

	/*if (stStatusConfig != NULL)
	{
		if (stStatusConfig->stWebStatusConfig != NULL)
		{
			DELETE(stStatusConfig->stWebStatusConfig);	
			stStatusConfig->stWebStatusConfig = NULL;
		}
		DELETE(stStatusConfig);
		stStatusConfig = NULL;
	}*/
	int iThreadStatus, iThreadStatus1, iThreadStatus2;

	iThreadStatus = RunThread_GetStatus(hCmdThread);
	iThreadStatus1 = RunThread_GetStatus(hCmdThread1);
	iThreadStatus2 = RunThread_GetStatus(hCmdThread2);


	while((iThreadStatus != RUN_THREAD_IS_INVALID) || (iThreadStatus1 != RUN_THREAD_IS_INVALID) || (iThreadStatus2 != RUN_THREAD_IS_INVALID))
	{
	    iThreadStatus = RunThread_GetStatus(hCmdThread);
	    iThreadStatus1 = RunThread_GetStatus(hCmdThread1);
	    iThreadStatus2 = RunThread_GetStatus(hCmdThread2);

	}

	close(fd);
	//changed by Frank Wu,25/N/27,20140527, for power split
	Mutex_Destroy(s_hMutexLoadGCCFGFile);
	Mutex_Destroy(g_hMutexLoadCurrent);
	Mutex_Destroy(g_hMutexSetDataFile);

	//DxiRegisterNotification("Web User", 
	//	ServiceNotification,
	//	NULL, // it shall be changed to the actual arg
	//	(_ALARM_OCCUR_MASK | _ALARM_CLEAR_MASK | _SET_MASK | _CONFIG_MASK | _LOAD_CURRENT_MASK),
	//	RunThread_GetId(NULL),
	//	_CANCEL_FLAG);

	TRACE_WEB_USER_NOT_CYCLE("*******************************Exit the Web_user task!********************************\n");

	return SERVICE_EXIT_OK;
}

DWORD ServiceMain(SERVICE_ARGUMENTS *pArgs)
{
	DWORD dwExitCode;
	dwExitCode = StartWebCommunicate(&pArgs->nQuitCommand);
	return dwExitCode;
}

/*==========================================================================*
* FUNCTION :  Radius_Web_GetAuthorityByUser
* PURPOSE  :  To authenticate user from radius server
* CALLS    :
* CALLED BY:
* ARGUMENTS:  char *szUserName,char *szPass
* RETURN   :  int iAuthorLevel
* COMMENTS :
* CREATOR  : Prayjit Tribhuwan              DATE: 2019-03-13 10:00
*==========================================================================*/

static int Radius_Web_GetAuthorityByUser(char *szUserName,char *szPass)
{

	SITE_INFO		*pSiteInfo = NULL;
	DWORD			dwGetData;
	char			szGetStr[128];
	char			*pLangCode = NULL;
	FILE *fptest;
	FILE *fp1;
    #define RADIUS_SERVER_SETTINGS "/app/radius/radius_config/rserver_param.txt"
	char buf[100],buffer0[20],buffer[20],NAS_Identifier[20];
	char 		radiusReply[100],username[16],pass[65];
	//char 		serviceType[4][20]={"Login-User","Outbound-User","NAS-Prompt-User","Administrative-User"};
	//char 	serviceType[4][20]={"Viewer","Operator","Technican","Admin"};
	char 		serviceType[4][20]={"Browser","Operator","Engineer","Administrator"};
    char 		serviceType_in[20];
	int i=0,j=0,c_count=0;
	int 		iAuthorLevel=0,i1=0,j1=0;
	char 		radiusCommand[200];
	char rEnable[3],nEnable[3];
	char *pbuf;
	char buff3[20];
	FILE *fp_log;
	memset(rEnable, 0, 3);
	memset(nEnable, 0, 3);

	fptest = fopen(RADIUS_SERVER_SETTINGS,"r");    //read whether radius and NAS identifier is enabled or not
	fread(buf, 100, 1, fptest);
	fclose(fptest);
	pbuf=buf;


		i=0;
		while (*pbuf!=',')
		{
			rEnable[i]=*pbuf;
			i++;
			pbuf++;
		}
		rEnable[2]='\0';
		pbuf++;
		i=0;
		while (*pbuf!=',')

		{
			nEnable[i]=*pbuf;
			i++;
			pbuf++;

		}
		nEnable[2]='\0';
		if(Web_GetACUInfo(&pSiteInfo, &dwGetData, szGetStr, &pLangCode) != FALSE)
		{
			strcpy(NAS_Identifier,pSiteInfo->langSiteName.pFullName[0]);  // site name as NAS Identifier
		}
		else
		{
			strcpy(NAS_Identifier,"G3");

		}


		if (!strcmp(rEnable,"on"))
		{

			strcpy(username,szUserName);
			strcpy(pass,szPass);

			if (!strcmp(nEnable,"on"))
		{
				sprintf(radiusCommand,"radiusclient -f /app/radius/radius_config/radiusclient.conf -p 1812 User-name=%s password=%s NAS-Identifier=%s ",username,pass,NAS_Identifier); // radius authentication request with NAS Identifier

		}
			else

			{
					sprintf(radiusCommand,"radiusclient -f /app/radius/radius_config/radiusclient.conf -p 1812 User-name=%s password=%s ",username,pass); // radius authentication request without NAS Identifier

				}

			memset(radiusReply, 0, 100);
			//fflush(fp1);

			fp1 = popen(radiusCommand,"r");
			while(fgets(radiusReply, sizeof(radiusReply), fp1))
                        {
                                strncpy(buff3, radiusReply, 7);
                                 buff3[7]='\0';
                                 if (! strcmp(buff3,"VV-Priv"))
                                 {
                                         break;
                                 }
                        }

			pclose(fp1);
		   // DELETE(fp1);
			fflush(fp1);
			strncpy(buff3, radiusReply, 7);
			buff3[7]='\0';

			AppLogOut(CGI_APP_LOG_QUERY_NAME, APP_LOG_INFO,radiusReply);

			//log file
			//fp_log=fopen("/app/rad_reply_log.txt","a+");
			//fprintf(fp_log,"%s\n",radiusReply);
			//fclose(fp_log);

			if (! strcmp(buff3,"VV-Priv"))		//Service-Type
			{

			//fp_log=fopen("/app/rad_reply_log.txt","a+");
			//fprintf(fp_log,"%s\n",buff3);
			//fclose(fp_log);
			while (radiusReply[i1]!= '\'')
			{
			i1++;
			}
			i1=i1+1;
			j1=0;
			while(radiusReply[i1]!='\'')
			{
				serviceType_in[j1]=radiusReply[i1];
			i1++;j1++;
			}
			serviceType_in[j1]='\0';

			iAuthorLevel=-1;
			i1=0;
			while (i1<=3)
			{
			if (!(strcmp(serviceType_in,serviceType[i1])))  //assignment of  privilege level to the user as per the service type returned by server
				{
				printf("%d", i1);
				iAuthorLevel=i1;
				break;
				}
			i1++;
			}


			if ((0<= iAuthorLevel )&&(iAuthorLevel <=3))
			{
			      return (iAuthorLevel+1);
			}
			else
			{
				return -1;

			}

			}
			else
			{
				return -1;
			}
		}
		else
		{
			return -2;
	}


}

/*==========================================================================*
* FUNCTION : SetRadiusInfo
* PURPOSE  : To save Radius Settings into config files
* CALLS    :
* CALLED BY: To be called by the services
* ARGUMENTS: char  *ptr     : Data
*
*
* RETURN   : int : error code
* COMMENTS :
* CREATOR  : Prayjit Tribhuwan               DATE: 2020-06-22 17:36
*==========================================================================*/
int SetRadiusInfo(char *ptr)
{
	char	buf[100];
	char	sRadiusEnable[3],sNASEnable[3];
	char 	sP_serverIP[20],sP_Port[6];
	char 	sS_serverIP[20],sS_Port[6],skey[32];
	char 	scmd[150];
	int		i=0,j=0;
	FILE	*fp;

	//printf("\n%s\n",ptr);
	fp=fopen(RADIUS_SERVER_SETTINGS,"wb");
	while(i<7)
	{
		if(*ptr==';')
		{
			i++;

			if (i==1)
			{
			sRadiusEnable[j]='\0';
			//printf("\nsRadiusEnable:%s",sRadiusEnable);
			fprintf(fp,"%s,",sRadiusEnable);
			}
			if (i==2)
			{
			sNASEnable[j]='\0';
			//printf("\nsNASEnable:%s",sNASEnable);
			fprintf(fp,"%s,",sNASEnable);
			}
			if (i==3)
			{
			sP_serverIP[j]='\0';
			//printf("\nServerIP_P:%s",sP_serverIP);
			fprintf(fp,"%s,",sP_serverIP);
			}
			if (i==4)
			{
			sS_serverIP[j]='\0';
			//printf("\nServerIP_S:%s",sS_serverIP);
			fprintf(fp,"%s,",sS_serverIP);

			}
			if (i==5)
			{
			sP_Port[j]='\0';
			//printf("\nPort_P:%s",sP_Port);
			fprintf(fp,"%s,",sP_Port);
			}
			if (i==6)
			{
			sS_Port[j]='\0';
			//printf("\nPort_S:%s",sS_Port);
			fprintf(fp,"%s,",sS_Port);
			}
			if (i==7)
			{
			skey[j]='\0';
			//printf("\nkey:%s",skey);
			fprintf(fp,"%s,",skey);
			}

			j=0;
			ptr++;
		}
		else
		{

			if (i==0)
			{
			sRadiusEnable[j]=*ptr;
			}
			if (i==1)
			{
			sNASEnable[j]=*ptr;
			}
			if (i==2)
			{
			sP_serverIP[j]=*ptr;
			}
			if (i==3)
			{
			sS_serverIP[j]=*ptr;
			}
			if (i==4)
			{
			sP_Port[j]=*ptr;
			}
		    if (i==5)
			{
		    sS_Port[j]=*ptr;
			}
			if (i==6)
			{
			skey[j]=*ptr;
			}

			ptr++;
			j++;

		}


	}
	fclose(fp);
	//printf("\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n",sRadiusEnable,sNASEnable,sP_serverIP,sS_serverIP,sP_Port,sS_Port,skey);

//	sprintf(scmd,"sed -i 34s/.*/\"authserver \t %s:%s\"/ /app/radius/radius_config/radiusclient.conf",sP_serverIP,sP_Port);
//	system(scmd);
//
//	sprintf(scmd,"sed -i 2s/.*/\"%s \t %s\"/ /app/radius/radius_config/servers",sP_serverIP,skey);
//	system(scmd);
//	if(strlen(sS_serverIP)>1)
//	{
//		sprintf(scmd,"sed -i 36s/.*/\"authserver \t %s:%s\"/ /app/radius/radius_config/radiusclient.conf",sS_serverIP,sS_Port);
//		system(scmd);
//
//		sprintf(scmd,"sed -i 3s/.*/\"%s \t %s\"/ /app/radius/radius_config/servers",sS_serverIP,skey);
//		system(scmd);
//	}

	SetRadiusConfig(PRIMARY_SEL,sP_serverIP,sP_Port,skey);

	if(strlen(sS_serverIP)>1)
	{
		SetRadiusConfig(SECONDARY_SEL,sS_serverIP,sS_Port,skey);
	}
	return 0;
}


/*==========================================================================*
* FUNCTION : SetRadiusConfig
* PURPOSE  : To save Radius Settings into config files
* CALLS    :
* CALLED BY: To be called by the Services
* ARGUMENTS: int IpSelection : Primary/Secondary IP and Port selection
* 			 char *serverIP  : Primary/Secondary IP
* 			 char *Port      : Primary/Secondary Port
* 			 char *skey		 : Secret key
*
*
* RETURN   : int : error code
* COMMENTS :
* CREATOR  : Ajit Varat	               DATE: 2020-08-28 19:50
*==========================================================================*/

int SetRadiusConfig(int IpSelection, char *serverIP, char *Port, char *skey)
{
	char TempStrConfig[100] = "authserver		";
	char TempStrServer[100] = "\0";

	strcat(TempStrConfig,serverIP);
	strcat(TempStrConfig,":");
	strcat(TempStrConfig,Port);
	strcat(TempStrConfig,"\n");

	strcat(TempStrServer,serverIP);
	strcat(TempStrServer,"		");
	strcat(TempStrServer,skey);
	strcat(TempStrServer,"\n");

	switch(IpSelection)
	{
		case 1: UpdateRadiusConfigFile(34,TempStrConfig,RADIUSCLIENT_CONFIG);
				UpdateRadiusConfigFile(3,TempStrServer,RADIUSCLENT_SERVER);
				break;
		case 2: UpdateRadiusConfigFile(36,TempStrConfig,RADIUSCLIENT_CONFIG);
				UpdateRadiusConfigFile(4,TempStrServer,RADIUSCLENT_SERVER);
				break;
		default:
				break;
	}
	return 0;
}


/*==========================================================================*
* FUNCTION : UpdateRadiusConfigFile
* PURPOSE  : To update Radius Settings into config files
* CALLS    :
* CALLED BY: To be called by the services
* ARGUMENTS: int lineNo		: Parameter update Location
* 			 char *newline  : Data to be update
* 			 char *filename : Config file names
*
*
* RETURN   : int : error code
* COMMENTS :
* CREATOR  : Ajit Varat               DATE: 2020-08-28 19:55
*==========================================================================*/

int UpdateRadiusConfigFile(int lineNo, char *newline, char *filename)
{
	char buffer[BUFFER_SIZE];
	int count = 0;

	    FILE * fPtr;
	    FILE * fTemp;

	    fPtr  = fopen(filename, "r");
	    fTemp = fopen("replace.tmp", "wb");

	    if (fPtr == NULL || fTemp == NULL)
	    {
//		printf("\nUnable to open file.\n");
//		printf("Please check whether file exists and you have read/write privilege.\n");
		exit(EXIT_SUCCESS);
	    }

	    count = 0;
	    while ((fgets(buffer, BUFFER_SIZE, fPtr)) != NULL)
	    {
		count++;

		if (count == lineNo)
		    fputs(newline, fTemp);
		else
		    fputs(buffer, fTemp);
	    }

	    fclose(fPtr);
	    fclose(fTemp);


	    remove(filename);

	    rename("replace.tmp", filename);
//	    sleep(1);
//	    printf("\nSuccessfully replaced '%d' line with '%s'.\n", lineNo, newline);

	    return 0;
}

