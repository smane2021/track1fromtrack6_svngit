/*==========================================================================*
*    Copyright(c) 2021, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : NSC3.0(Net Sure Controller)
*
*  FILENAME : web_egy_sav.c
*  CREATOR  : Song Xu              DATE: 2018-12-5
*  VERSION  : V1.00
*  PURPOSE  : This file is efficiency tracker for e4 rectifier.
*
*  HISTORY  :
*
*==========================================================================*/


#include <signal.h>
#include "stdsys.h"  
#include <time.h>

#include <sys/dir.h> 
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <stdio.h>

#include "web_user.h"
#include "cgi_pub.h"
#include	"../../lib/data_mgmt/his_data.h"

#define		WEB_EFFICIENCY_TRACKER_CFG_FILE		"/app/config/private/web_user/cfg_efficiency_tracker.cfg"
#define		ET_APP_LOG_NAME		"EFFI TRACK"

#define		DISPLAY_ON_WEB		"[DISPLAY_ON_WEB]"
#define		ET_ENABLE			"[ET_ENABLE]"
#define		ET_MUTEX_LOCK_WAIT_TIMEOUT		1000
#define		WEB_ET_RUN_INTERVAL			5
#define		ET_CFG_BENCHMARK_NUM		3
#define		ET_CFG_MIX_RECT_NUM		9
static HANDLE g_hMutexETDataSavedInfoToFlash = NULL;
static		sETInitSucc = FALSE;
struct tagETEffiCurveTable{
	float	fLoadPercent[10];
};
typedef struct tagETEffiCurveTable	stETEffiCurveTable;
stETEffiCurveTable	gstETEffiCurveTable[ET_CFG_BENCHMARK_NUM + ET_CFG_MIX_RECT_NUM];
static SIG_BASIC_VALUE* pETPercent98SigValue = NULL;
static SIG_BASIC_VALUE* pETCostSigValue = NULL;
static SIG_BASIC_VALUE* pETBenchmarkCurrSigValue = NULL;
static SIG_BASIC_VALUE* pETCurrencySigValue = NULL;
static SIG_BASIC_VALUE* pSysVoltSigValue = NULL;
static SIG_BASIC_VALUE* pSysLoadSigValue = NULL;
static SIG_BASIC_VALUE* pSysRateCurrSigValue = NULL;

enum ET_ECO_RECT_USING
{
	TYPE_3500E4_10PERCENT = 0,
	TYPE_3500E4_20PERCENT,
	TYPE_3500E4_30PERCENT,
	TYPE_3500E4_40PERCENT,
	TYPE_3500E4_50PERCENT,
	TYPE_3500E4_60PERCENT,
	TYPE_3500E4_70PERCENT,
	TYPE_3500E4_80PERCENT,
	TYPE_3500E4_90PERCENT,
	TYPE_3500E4_100PERCENT,
	
	MAX_3500E4_PERCENT,
};

int ET_GetSignalPoint(int iEquipID,
						int iSignalType,
						int iSignalID,
						SIG_BASIC_VALUE **pSignalInfo)
{
	int iVarSubID, iError, iBufLen;
	iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);
	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
			iEquipID,
			iVarSubID,
			&iBufLen,
			(void *)pSignalInfo,
			0);
	if(iError != ERR_DXI_OK)
	{
		AppLogOut(ET_APP_LOG_NAME, APP_LOG_ERROR, 
			"Get signal[%d %d %d] point error!\n",iEquipID,iSignalType,iSignalID);
	}
	return iError;
}


static WEB_ET_init()
{
	int		iError;

	iError = ET_GetSignalPoint(2,SIG_TYPE_SETTING,64,&pETPercent98SigValue);
	if(iError != ERR_DXI_OK)
	{
		return FALSE;
	}

	iError = ET_GetSignalPoint(2,SIG_TYPE_SETTING,61,&pETBenchmarkCurrSigValue);
	if(iError != ERR_DXI_OK)
	{
		return FALSE;
	}
	
	iError = ET_GetSignalPoint(2,SIG_TYPE_SETTING,62,&pETCostSigValue);
	if(iError != ERR_DXI_OK)
	{
		return FALSE;
	}
		
	iError = ET_GetSignalPoint(2,SIG_TYPE_SETTING,63,&pETCurrencySigValue);
	if(iError != ERR_DXI_OK)
	{
		return FALSE;
	}
	
	iError = ET_GetSignalPoint(1,SIG_TYPE_SAMPLING,1,&pSysVoltSigValue);
	if(iError != ERR_DXI_OK)
	{
		return;
	}
	
	iError = ET_GetSignalPoint(2,SIG_TYPE_SAMPLING,1,&pSysLoadSigValue);
	if(iError != ERR_DXI_OK)
	{
		return;
	}

	iError = ET_GetSignalPoint(2,SIG_TYPE_SAMPLING,31,&pSysRateCurrSigValue);
	if(iError != ERR_DXI_OK)
	{
		return;
	}
	
	memset(WEB_ET_Info.szETBufferHisData, 0, ET_DATA_BUF_MAX_NUM);//string buf for jsonfile
	memset(WEB_ET_Info.szETBufferPresData, 0, ET_DATA_PRESENT_BUF_MAX_NUM);//string buf for jsonfile

	return TRUE;
}
/*==========================================================================*
* FUNCTION :  ParsedWebETCurveTableProc
* PURPOSE  :  
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  szBuf: the string in one line 
* RETURN   :  int: success or not
* COMMENTS :  
* CREATOR  :  Song Xu               DATE: 2018-12-5
*==========================================================================*/
int ParsedWebETCurveTableProc(IN char *szBuf, OUT stETEffiCurveTable *pStructData)
{
	char *pField = NULL;

	ASSERT(szBuf);
	ASSERT(pStructData);
	//10%
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if(pField == NULL)
	{
		return 1;
	}
	pStructData->fLoadPercent[0] = atof(pField);

	//20%
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if(pField == NULL)
	{
		return 2;
	}
	pStructData->fLoadPercent[1] = atof(pField);

	//30%
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField == NULL)
	{
		return 3;
	}
	pStructData->fLoadPercent[2] = atof(pField);

	//40%
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField == NULL)
	{
		return 4;
	}
	pStructData->fLoadPercent[3] = atof(pField);
	
	//50%
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField == NULL)
	{
		return 5;
	}
	pStructData->fLoadPercent[4] = atof(pField);
	
	//60%
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField == NULL)
	{
		return 6;
	}
	pStructData->fLoadPercent[5] = atof(pField);

	//70%
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField == NULL)
	{
		return 7;
	}
	pStructData->fLoadPercent[6] = atof(pField);

	//80%
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField == NULL)
	{
		return 8;
	}
	pStructData->fLoadPercent[7] = atof(pField);

	//90%
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField == NULL)
	{
		return 9;
	}
	pStructData->fLoadPercent[8] = atof(pField);

	//100%
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField == NULL)
	{
		return 10;
	}
	pStructData->fLoadPercent[9] = atof(pField);

	return ERR_WEB_OK;
}
/*==========================================================================*
* FUNCTION :  WEB_ET_getEffiCurveFromCfg
* PURPOSE  :  
* CALLS    : 
* CALLED BY:   
* ARGUMENTS:  
* RETURN   : int:TRUE for success!
* COMMENTS : 
* CREATOR  : Song Xu              DATE: 2018-12-5 11:08
*==========================================================================*/
static BOOL WEB_ET_getEffiCurveFromCfg(void)
{
	FILE	*pFile;
	size_t	ulFileLen;
	char	*szInFile = NULL;
	void	*pProf = NULL;
	int		iDisplayOnWeb, i, iRst, iEnable;
	int		iLoadLevel,iLoadLevel1;
	char	cReadMsg[128];
	CONFIG_TABLE_LOADER loader,loader1;
	stETEffiCurveTable		*stETCurveTable;
	stETEffiCurveTable		*stETMixCurveTable;

	if((pFile = fopen(WEB_EFFICIENCY_TRACKER_CFG_FILE, "r")) == NULL)
	{
		AppLogOut(ET_APP_LOG_NAME, APP_LOG_WARNING, "Failure to open ET cfg file.");
		return FALSE;
	}

	ulFileLen = GetFileLength(pFile);
	szInFile = NEW(char, ulFileLen + 1);
	if (szInFile == NULL)
	{
		AppLogOut(ET_APP_LOG_NAME, APP_LOG_WARNING, "NEW operation fails! Out of memory!\n");
		fclose(pFile);
		return FALSE;
	}
	ulFileLen = fread(szInFile, sizeof(char), ulFileLen, pFile);
	fclose(pFile);

	if (ulFileLen == 0) 
	{
		DELETE(szInFile);
		AppLogOut(ET_APP_LOG_NAME, APP_LOG_WARNING, "Read failed from ET cfg file.");
		return FALSE;
	}
	szInFile[ulFileLen] = '\0';  /* end with NULL */
	pProf = Cfg_ProfileOpen(szInFile, (int)ulFileLen);

	if (pProf == NULL)
	{
		DELETE(pProf); 
		DELETE(szInFile);
		return FALSE;
	}

	iRst = Cfg_ProfileGetInt(pProf,
		ET_ENABLE, 
		&iEnable);
	if(iRst != 1)
	{
		AppLogOut(ET_APP_LOG_NAME, APP_LOG_WARNING, "Get no ET_ENABLE on ET cfg file.");
		iEnable = FALSE;
	}
	WEB_ET_Info.bETEnable = iEnable;
//printf("ET Enable %d\n",iEnable);

	if(WEB_ET_Info.bETEnable == FALSE)
	{
		DAT_StorageDeleteRecord(EFFI_TRACK_FLASH_DATA);
		DELETE(pProf); 
		DELETE(szInFile);
		return FALSE;
	}

	iRst = Cfg_ProfileGetInt(pProf,
		DISPLAY_ON_WEB, 
		&iDisplayOnWeb);
	if(iRst != 1)
	{
		AppLogOut(ET_APP_LOG_NAME, APP_LOG_WARNING, "Get no DISPLAY_ON_WEB on ET cfg file.");
		DELETE(pProf); 
		DELETE(szInFile);
		return FALSE;
	}
	WEB_ET_Info.bDisplayOnWeb = iDisplayOnWeb;
//printf("display on web %d\n",iDisplayOnWeb);

	DEF_LOADER_ITEM(&loader,
		NULL,
		&iLoadLevel,
		"[RECT_CURVE]",
		&stETCurveTable,
		ParsedWebETCurveTableProc);

	if(Cfg_LoadSingleTable(pProf, &loader) != ERR_CFG_OK || iLoadLevel < ET_CFG_BENCHMARK_NUM)
	{
		DELETE(pProf); 
		DELETE(szInFile);
		return FALSE;
	}
	
	DEF_LOADER_ITEM(&loader1,
		NULL,
		&iLoadLevel1,
		"[MIX_E4_E3]",
		&stETMixCurveTable,
		ParsedWebETCurveTableProc);

	if(Cfg_LoadSingleTable(pProf, &loader1) != ERR_CFG_OK || iLoadLevel1 < ET_CFG_MIX_RECT_NUM)
	{
		DELETE(stETCurveTable);
		DELETE(pProf); 
		DELETE(szInFile);
		return FALSE;
	}

//	printf("%f %f %f %f %d|||%d %d  %d\n",stETCurveTable[0].fLoadPercent[1],stETCurveTable[2].fLoadPercent[5],
//		stETMixCurveTable[0].fLoadPercent[3],stETMixCurveTable[7].fLoadPercent[1],
//		iLoadLevel,
//		sizeof(stETCurveTable),sizeof(stETMixCurveTable) , sizeof(gstETEffiCurveTable) );

	memcpy(gstETEffiCurveTable, stETCurveTable, ET_CFG_BENCHMARK_NUM * sizeof(stETEffiCurveTable));
	memcpy(&gstETEffiCurveTable[ET_CFG_BENCHMARK_NUM], stETMixCurveTable, ET_CFG_MIX_RECT_NUM * sizeof(stETEffiCurveTable));
//printf("%f %f %f %f %f\n",
//	   gstETEffiCurveTable[0].fLoadPercent[1],
//	   gstETEffiCurveTable[0].fLoadPercent[2],
//	   gstETEffiCurveTable[1].fLoadPercent[3],
//	   gstETEffiCurveTable[5].fLoadPercent[4],
//	   gstETEffiCurveTable[7].fLoadPercent[5]);

	DELETE(stETCurveTable);
	DELETE(stETMixCurveTable);


	DELETE(pProf); 
	DELETE(szInFile);
	return TRUE;
}

void ET_FlashDataInit(time_t nowTime)
{
	WEB_ET_Info.stETDataSaveFlash.fTotalEgy = 0;
	WEB_ET_Info.stETDataSaveFlash.stETData24.iDataIndex = 0;
	WEB_ET_Info.stETDataSaveFlash.stETData365.iDataIndex = 0;
	WEB_ET_Info.stETDataSaveFlash.stETData24.tNewestHourFlag = nowTime/3600*3600;
	WEB_ET_Info.stETDataSaveFlash.stETData365.tNewestDayFlag = nowTime/(3600*24)*(3600*24);

	return;
}
/*==========================================================================*
* FUNCTION :  GetETDataInfoFromFlash
* PURPOSE  :  Get the ET data from flash, if the ET data structure or the record number have been changed, then create the storage in flash.
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS :  
* MODIFY   :  SongXu					 DATE: 2018-12-6
*==========================================================================*/
BOOL GetETDataInfoFromFlash(void)
{
	HANDLE hETDataSaveInfoHandle = NULL;
	ETDataSaveFlash* pETDataSaveFlash;
	int iReadRecordNum;
	ETDataSaveFlash *pstETDataSaveTemp;
	int num;
	int iStartPos = 1;
	int i, j;
	int iPanelID, iSignalID;
	int nUserNum = 1;
	time_t nowTime = time((time_t *)0);
	ETDataSaveFlash* pETDataSaveFlashRead = NULL;

printf("ET get flash\n");
	g_hMutexETDataSavedInfoToFlash = Mutex_Create(TRUE);
	if(g_hMutexETDataSavedInfoToFlash == NULL)
	{

		AppLogOut(ET_APP_LOG_NAME, APP_LOG_ERROR, 
			"GeInfoFromFlash: ERROR: "
			"mutex create failed!\n\r");

		return FALSE;
	}

	ZERO_POBJS(&(WEB_ET_Info.stETDataSaveFlash),	nUserNum);
	ET_FlashDataInit(nowTime);


	if (g_hMutexETDataSavedInfoToFlash != NULL && Mutex_Lock(g_hMutexETDataSavedInfoToFlash, ET_MUTEX_LOCK_WAIT_TIMEOUT) != ERR_MUTEX_OK)
	{
		AppLogOut(ET_APP_LOG_NAME, APP_LOG_ERROR, 
			"GeInfoFromFlash: ERROR: "
			"mutex Lock timeout failed!\n\r");

		return FALSE;
	}

	hETDataSaveInfoHandle = DAT_StorageOpen(EFFI_TRACK_FLASH_DATA);

	if((hETDataSaveInfoHandle != NULL) && (((RECORD_SET_TRACK *)hETDataSaveInfoHandle)->iRecordSize == sizeof(ETDataSaveFlash))
	&& (((RECORD_SET_TRACK *)hETDataSaveInfoHandle)->iMaxRecords == 1))
	{
		num = sizeof(ETDataSaveFlash);
printf("No need to Re-Create the ET storage,%d", num);
		pETDataSaveFlash = NEW(ETDataSaveFlash, nUserNum);
		if(pETDataSaveFlash == NULL)
		{
			if(g_hMutexETDataSavedInfoToFlash)
			{
				Mutex_Unlock(g_hMutexETDataSavedInfoToFlash);
			}
			return FALSE;
		}

		pETDataSaveFlashRead = pETDataSaveFlash;

		if (!DAT_StorageReadRecords(hETDataSaveInfoHandle, &iStartPos,
			&nUserNum,(void *)pETDataSaveFlash, 0, TRUE))
		{
			nUserNum = 0;
		}
printf("nUserNum is %d\n",nUserNum);

		DAT_StorageClose(hETDataSaveInfoHandle);

		if(nUserNum == 0)
		{
			ET_FlashDataInit(nowTime);
		}
		else
		{
			pstETDataSaveTemp = &(WEB_ET_Info.stETDataSaveFlash);
			if(pETDataSaveFlash != NULL)
			{
				memcpy(pstETDataSaveTemp, pETDataSaveFlash, sizeof(ETDataSaveFlash));
				//if(isnan(pstETDataSaveTemp->st_totalSavedFee.fMoneySaved))
				//{
				//	pstETDataSaveTemp->st_totalSavedFee.fMoneySaved = 0;
				//}
			}
		}

		DELETE(pETDataSaveFlashRead);
		pETDataSaveFlashRead = NULL;
	}
	else //Create  info log
	{
printf("Now create the ET info in flash!\n");

		if(DAT_StorageDeleteRecord(EFFI_TRACK_FLASH_DATA) != TRUE)
		{
			AppLogOut(ET_APP_LOG_NAME, APP_LOG_ERROR, 
			"Delete ET data file error");
			Mutex_Unlock(g_hMutexETDataSavedInfoToFlash);

			return FALSE;
		}

		hETDataSaveInfoHandle = DAT_StorageCreate(EFFI_TRACK_FLASH_DATA,
			sizeof(ETDataSaveFlash), 
			1,
			FREQ_ET_LOG);

		// log after created...
		if (hETDataSaveInfoHandle)
		{
			AppLogOut(ET_APP_LOG_NAME, APP_LOG_INFO, 
				"Creating ET data file '%s'"
				"(RecSize:%d, MaxRec:%d, RecFreq:%1.5f)...%s\n", 
			EFFI_TRACK_FLASH_DATA, sizeof(ETDataSaveFlash), 
			1, FREQ_ET_LOG, "OK");
		}
		else
		{
			AppLogOut(ET_APP_LOG_NAME, APP_LOG_ERROR, 
				"Creating money saved data file '%s'"
				"(RecSize:%d, MaxRec:%d, RecFreq:%1.5f)...%s\n", 
			EFFI_TRACK_FLASH_DATA, sizeof(ETDataSaveFlash), 
			1, FREQ_ET_LOG, "FAILED");
		}
//printf("ET init flash %d %d %f|| %d %d %f\n",WEB_ET_Info.stETDataSaveFlash.stETData24.tNewestHourFlag,
//			WEB_ET_Info.stETDataSaveFlash.stETData24.iDataIndex,
//			WEB_ET_Info.stETDataSaveFlash.stETData24.fPeriodEgy[WEB_ET_Info.stETDataSaveFlash.stETData24.iDataIndex],
//			WEB_ET_Info.stETDataSaveFlash.stETData365.tNewestDayFlag,
//			WEB_ET_Info.stETDataSaveFlash.stETData365.iDataIndex,
//			WEB_ET_Info.stETDataSaveFlash.stETData365.fPeriodEgy[WEB_ET_Info.stETDataSaveFlash.stETData365.iDataIndex]);
		//ET_FlashDataInit(nowTime);

		DAT_StorageClose(hETDataSaveInfoHandle);
	}
	if(g_hMutexETDataSavedInfoToFlash)
	{
		Mutex_Unlock(g_hMutexETDataSavedInfoToFlash);
	}
	return TRUE;
}



/*==========================================================================*
* FUNCTION :  UpdateETDataToFlash
* PURPOSE  :  Update the ET data to flash.
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS :   
* MODIFY   :  SongXu					 DATE: 2018-12-5
*==========================================================================*/
BOOL UpdateETDataToFlash(IN ETDataSaveFlash  *stETDataSaveFlash)
{
	HANDLE hETDataSaveFlashHandleW = NULL;
	BOOL	bRst;
	ETDataSaveFlash *pstETDataTemp;

	if(g_hMutexETDataSavedInfoToFlash != NULL && Mutex_Lock(g_hMutexETDataSavedInfoToFlash, ET_MUTEX_LOCK_WAIT_TIMEOUT) != ERR_MUTEX_OK)
	{
		return ERR_USR_MUTEX_WAIT_TIMEOUT;
	}
//printf("ET save flash\n");
	if(DAT_StorageDeleteRecord(EFFI_TRACK_FLASH_DATA) != TRUE)
	{
		Mutex_Unlock(g_hMutexETDataSavedInfoToFlash);
		return FALSE;
	}

	hETDataSaveFlashHandleW = DAT_StorageOpenW(EFFI_TRACK_FLASH_DATA);

	if (hETDataSaveFlashHandleW != NULL)
	{
		ETDataSaveFlash stETDataSaveFlashTemp;
		ETDataSaveFlash  *pWriteInfo;
//printf("total %f %f %d\n",WEB_ET_Info.stETDataSaveFlash.fAverLoad, WEB_ET_Info.stETDataSaveFlash.fTotalEgy, WEB_ET_Info.stETDataSaveFlash.iTotalDays);
//printf("ET save flash %d %d %d|| %d %d %d\n",WEB_ET_Info.stETDataSaveFlash.stETData24.tNewestHourFlag,
			//WEB_ET_Info.stETDataSaveFlash.stETData24.iDataIndex,
			//WEB_ET_Info.stETDataSaveFlash.stETData24.iNewestHourLoadCount,
			//WEB_ET_Info.stETDataSaveFlash.stETData365.tNewestDayFlag,
			//WEB_ET_Info.stETDataSaveFlash.stETData365.iDataIndex,
			//WEB_ET_Info.stETDataSaveFlash.stETData365.iNewestDayLoadCount);
		int i;
		//for(i=0;i<=WEB_ET_Info.stETDataSaveFlash.stETData24.iDataIndex;i++)
		//{
		//	printf("[%f ",WEB_ET_Info.stETDataSaveFlash.stETData24.fAverLoad[i]);
		//	printf("%f]",WEB_ET_Info.stETDataSaveFlash.stETData24.fPeriodEgy[i]);
		//}
		//printf("\n");
		//for(i=0;i<=WEB_ET_Info.stETDataSaveFlash.stETData365.iDataIndex;i++)
		//{
		//	printf("[%f ",WEB_ET_Info.stETDataSaveFlash.stETData365.fAverLoad[i]);
		//	printf("%f]",WEB_ET_Info.stETDataSaveFlash.stETData365.fPeriodEgy[i]);
		//}
		//printf("\n");

		pWriteInfo = &stETDataSaveFlashTemp;
		memset(&stETDataSaveFlashTemp, 0, sizeof(ETDataSaveFlash));

		pstETDataTemp = stETDataSaveFlash;
		memcpy(&stETDataSaveFlashTemp, pstETDataTemp, sizeof(ETDataSaveFlash));

		bRst = DAT_StorageWriteRecord(hETDataSaveFlashHandleW,
			sizeof(ETDataSaveFlash) * 
			1,
			pWriteInfo);
		DAT_StorageClose(hETDataSaveFlashHandleW);

	}
	if(g_hMutexETDataSavedInfoToFlash)
	{
		Mutex_Unlock(g_hMutexETDataSavedInfoToFlash);
	}

	return TRUE;
}

/*==========================================================================*
* FUNCTION :  ET_CalculteLoadAndEgy
* PURPOSE  :  calculte the average load and energy of each code running interval.
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS :   
* MODIFY   :  SongXu					 DATE: 2018-12-5
*==========================================================================*/
void ET_CalculteLoadAndEgy()
{
	static BOOL sbInit = FALSE;
	int iError, iIndex;
	static time_t	s_tLastTime;
	time_t			tNowTime;
	time_t			tTimePeriod;
	float			fEnergyPeriod, fPower, fTotalLoadHour, fTotalLoadDay;

	if(sbInit == FALSE)
	{
		sbInit = TRUE;
		s_tLastTime = time((time_t *)0);
		WEB_ET_Info.stETDataSaveFlash.stETData24.iNewestHourLoadCount = 0;

		return;//��һ�γ�ʼ���󲻽��м���
	}

//printf("load data %f %f %f\n",pSysVoltSigValue->varValue.fValue, pSysLoadSigValue->varValue.fValue, pSysRateCurrSigValue->varValue.fValue);
	if(FLOAT_EQUAL0(pSysRateCurrSigValue->varValue.fValue))
	{
		return;
	}

	tNowTime = time((time_t *)0);
	tTimePeriod = tNowTime - s_tLastTime;
	if(tTimePeriod > WEB_ET_RUN_INTERVAL * 3 || tTimePeriod < 0)
	{
		tTimePeriod = WEB_ET_RUN_INTERVAL;
	}

	fPower = pSysVoltSigValue->varValue.fValue * pSysLoadSigValue->varValue.fValue;
	fEnergyPeriod = fPower * tTimePeriod / 3600 / 1000;
	s_tLastTime = tNowTime;

	//hour data
	iIndex = WEB_ET_Info.stETDataSaveFlash.stETData24.iDataIndex;
	fTotalLoadHour = WEB_ET_Info.stETDataSaveFlash.stETData24.iNewestHourLoadCount
		* WEB_ET_Info.stETDataSaveFlash.stETData24.fAverLoad[iIndex]
		+ (pSysLoadSigValue->varValue.fValue/(pSysRateCurrSigValue->varValue.fValue));

	WEB_ET_Info.stETDataSaveFlash.stETData24.iNewestHourLoadCount++;
	WEB_ET_Info.stETDataSaveFlash.stETData24.fAverLoad[iIndex] = 
		fTotalLoadHour / WEB_ET_Info.stETDataSaveFlash.stETData24.iNewestHourLoadCount;
	WEB_ET_Info.stETDataSaveFlash.stETData24.fPeriodEgy[iIndex] +=
		fEnergyPeriod;

	//date data
	iIndex = WEB_ET_Info.stETDataSaveFlash.stETData365.iDataIndex;
	fTotalLoadDay = WEB_ET_Info.stETDataSaveFlash.stETData365.iNewestDayLoadCount
		* WEB_ET_Info.stETDataSaveFlash.stETData365.fAverLoad[iIndex]
		+ (pSysLoadSigValue->varValue.fValue/(pSysRateCurrSigValue->varValue.fValue));

	WEB_ET_Info.stETDataSaveFlash.stETData365.iNewestDayLoadCount++;
	WEB_ET_Info.stETDataSaveFlash.stETData365.fAverLoad[iIndex] = 
		fTotalLoadDay / WEB_ET_Info.stETDataSaveFlash.stETData365.iNewestDayLoadCount;
	WEB_ET_Info.stETDataSaveFlash.stETData365.fPeriodEgy[iIndex] +=
		fEnergyPeriod;
//printf("load M  %d %d %f %f %f\n",WEB_ET_Info.stETDataSaveFlash.stETData365.iNewestDayLoadCount
	//, iIndex
	//,fTotalLoadDay
	//,WEB_ET_Info.stETDataSaveFlash.stETData365.fAverLoad[iIndex]
	//,WEB_ET_Info.stETDataSaveFlash.stETData365.fPeriodEgy[iIndex]);

	
	return;
}

float ETFindEffiFromCurve(float fLoad, stETEffiCurveTable *stETCurveData)
{
	int		iPart,j;
	float	fPart, fReturn;
	
	if(fLoad < 0.1|| fLoad > (1 + EPSILON)) //fload < 10% or >100%
	{
		return 0;
	}
	else if(FLOAT_EQUAL(1,fLoad))// 100%, ���ý��м���
	{
		return stETCurveData->fLoadPercent[9]/100;
	}

	fLoad = fLoad * 10;

	iPart = (int)fLoad;
	fPart = fLoad - iPart;
	fReturn = (fPart * (stETCurveData->fLoadPercent[iPart] - stETCurveData->fLoadPercent[iPart-1]) + stETCurveData->fLoadPercent[iPart-1]) / 100;
//printf("=%f %d %f %f %f=",fLoad,iPart, fReturn,stETCurveData->fLoadPercent[iPart - 1],stETCurveData->fLoadPercent[iPart]);
	return fReturn;
}


//find the efficiency data from the curve.
float ETGetDeltaEffi(IN float fLoad, OUT float *fCurrEffi)
{
	float fDelta, fEffiE4, fEffiBenchmark;
	int i;
	int iBenchMark = pETBenchmarkCurrSigValue->varValue.enumValue;
	int iECOPercent = pETPercent98SigValue->varValue.enumValue;

	if(fLoad > 1)
	{
		fLoad = 1;
	}

	if(iECOPercent == TYPE_3500E4_100PERCENT) //not mix 
	{
		if(iBenchMark == 0 || iBenchMark == 1)
		{
			if(fLoad < 0.1)
			{
				*fCurrEffi = 1;
				return 0;
			}
			fEffiBenchmark = ETFindEffiFromCurve(fLoad, &gstETEffiCurveTable[iBenchMark + 1]);//gstETEffiCurveTable[1/2] for R483500e3/3200
			fEffiE4 = ETFindEffiFromCurve(fLoad, &gstETEffiCurveTable[0]);//gstETEffiCurveTable[0] for R483500e4
			fDelta = fEffiE4 - fEffiBenchmark;
		}
		else
		{
			fEffiE4 = 0.98;
			fDelta = (float)iBenchMark / 100; //98-96/95/94/93/92/91/90 �պ��Ǻ��ź�ֵ
		}
	}
	else
	{
		if(fLoad < 0.1)
		{
			*fCurrEffi = 1;
			return 0;
		}
		fEffiBenchmark = ETFindEffiFromCurve(fLoad, &gstETEffiCurveTable[1]);//gstETEffiCurveTable[0] for R483500e3
		fEffiE4 = ETFindEffiFromCurve(fLoad, &gstETEffiCurveTable[iECOPercent + 3]);//gstETEffiCurveTable[3-11] for R483500e4
		fDelta = fEffiE4 - fEffiBenchmark;
	}
	*fCurrEffi = fEffiE4;
//printf("|%f %f %f  %f %d %d|\n", fLoad, fDelta, fEffiE4, fEffiBenchmark,iECOPercent, iBenchMark);
	if(fDelta < 0)
	{
		fDelta = 0;
	}
	return fDelta;
}

void ET_MakeSaveEgyAndMoneyData(void)
{
	float	fDeltaEffi, fLoad, fEgy;
	float	fCurrentEffi, fDeltaEgy;
	float	fTotalEgySaved = 0, fMoneySaved = 0, fAverDeltaEffi = 0;
	char	*szTemp = NULL;
	int		iError, iBufLength, iFileLen = 0;
	int		iValidDateStart;
	int		i, itemp, iDateCount = 0;
	int		iE4Perc = pETPercent98SigValue->varValue.enumValue;
	int		iBenchMark = pETBenchmarkCurrSigValue->varValue.enumValue;

	memset(WEB_ET_Info.szETBufferHisData, 0, ET_DATA_BUF_MAX_NUM);
	szTemp = WEB_ET_Info.szETBufferHisData;

	iBufLength = ET_DATA_BUF_MAX_NUM;
	//printf("begin to make the ET data\n");
	
	//Last 24 hour data
	fAverDeltaEffi = 0;
	fTotalEgySaved = 0;
	iDateCount = 0;
	for(i = 0; i < WEB_ET_Info.stETDataSaveFlash.stETData24.iDataIndex; i++)
	{
		fLoad = WEB_ET_Info.stETDataSaveFlash.stETData24.fAverLoad[i];
		fEgy = WEB_ET_Info.stETDataSaveFlash.stETData24.fPeriodEgy[i];
		fDeltaEffi = ETGetDeltaEffi(fLoad, &fCurrentEffi);
		if(!FLOAT_EQUAL0(fCurrentEffi) && !FLOAT_EQUAL0(fEgy))
		{
			fDeltaEgy = fEgy / (fCurrentEffi - fDeltaEffi) - fEgy / fCurrentEffi;
			fTotalEgySaved += fDeltaEgy;
			fAverDeltaEffi += fDeltaEffi;
			iDateCount++;
		}
	//printf("[%f %f %f, %f %f %f] ",fTotalEgySaved,fDeltaEgy,fEgy,fCurrentEffi,fDeltaEffi,fLoad);
	}
	fMoneySaved = fTotalEgySaved * pETCostSigValue->varValue.fValue;
	if(iDateCount != 0)
	{
		fAverDeltaEffi = fAverDeltaEffi / iDateCount;
	}
	else
	{
		fAverDeltaEffi = 0;
	}
	if(iE4Perc == TYPE_3500E4_100PERCENT && iBenchMark > 1)//����ȥ24Сʱû���κ���Ч����ʱ����benchmarkΪ90-96%ʱ����Ҫ��ȫ���ݡ�
	{
		fAverDeltaEffi = (float)iBenchMark/100;
	}
	//printf("\n total %f %f %f %d\n",fTotalEgySaved, fMoneySaved,fAverDeltaEffi,iDateCount); 
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[[%.2f,%.2f,%.2f],",fTotalEgySaved, fMoneySaved, 100*fAverDeltaEffi);

	//Last week data
	fAverDeltaEffi = 0;
	fTotalEgySaved = 0;
	iDateCount = 0;
	iValidDateStart = WEB_ET_Info.stETDataSaveFlash.stETData365.iDataIndex > 7 ? WEB_ET_Info.stETDataSaveFlash.stETData365.iDataIndex - 7 : 0;
	for(i = iValidDateStart; i < WEB_ET_Info.stETDataSaveFlash.stETData365.iDataIndex; i++)
	{
		fLoad = WEB_ET_Info.stETDataSaveFlash.stETData365.fAverLoad[i];
		fEgy = WEB_ET_Info.stETDataSaveFlash.stETData365.fPeriodEgy[i];
		fDeltaEffi = ETGetDeltaEffi(fLoad, &fCurrentEffi);
		if(!FLOAT_EQUAL0(fCurrentEffi) && !FLOAT_EQUAL0(fEgy))
		{
			fDeltaEgy = fEgy / (fCurrentEffi - fDeltaEffi) - fEgy / fCurrentEffi;
			fTotalEgySaved += fDeltaEgy;
			fAverDeltaEffi += fDeltaEffi;
			iDateCount++;
		}
	}
	fMoneySaved = fTotalEgySaved * pETCostSigValue->varValue.fValue;
	if(iDateCount != 0)
	{
		fAverDeltaEffi = fAverDeltaEffi / iDateCount;
	}
	else
	{
		fAverDeltaEffi = 0;
	}
	if(iE4Perc == TYPE_3500E4_100PERCENT && iBenchMark > 1)//����ȥ24Сʱû���κ���Ч����ʱ����benchmarkΪ90-96%ʱ����Ҫ��ȫ���ݡ�
	{
		fAverDeltaEffi = (float)iBenchMark/100;
	}

	//printf("total %f %f %f %d\n",fTotalEgySaved, fMoneySaved,fAverDeltaEffi,iDateCount); 

	if(WEB_ET_Info.stETDataSaveFlash.stETData365.iDataIndex > 0)
	{
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[%.2f,%.2f,%.2f],",fTotalEgySaved, fMoneySaved, 100*fAverDeltaEffi);
	}
	else
	{
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[%.2f,%.2f,-9999],",fTotalEgySaved, fMoneySaved);
	}

	//Last Month data
	fAverDeltaEffi = 0;
	fTotalEgySaved = 0;
	iDateCount = 0;
	iValidDateStart = WEB_ET_Info.stETDataSaveFlash.stETData365.iDataIndex > 30 ? WEB_ET_Info.stETDataSaveFlash.stETData365.iDataIndex - 30 : 0;
	for(i = iValidDateStart; i < WEB_ET_Info.stETDataSaveFlash.stETData365.iDataIndex; i++)
	{
		fLoad = WEB_ET_Info.stETDataSaveFlash.stETData365.fAverLoad[i];
		fEgy = WEB_ET_Info.stETDataSaveFlash.stETData365.fPeriodEgy[i];
		fDeltaEffi = ETGetDeltaEffi(fLoad, &fCurrentEffi);

		if(!FLOAT_EQUAL0(fCurrentEffi) && !FLOAT_EQUAL0(fEgy))
		{
			fDeltaEgy = fEgy / (fCurrentEffi - fDeltaEffi) - fEgy / fCurrentEffi;
			fTotalEgySaved += fDeltaEgy;
			fAverDeltaEffi += fDeltaEffi;
			iDateCount++;
		}
	}
	fMoneySaved = fTotalEgySaved * pETCostSigValue->varValue.fValue;
	if(iDateCount != 0)
	{
		fAverDeltaEffi = fAverDeltaEffi / iDateCount;
	}
	else
	{
		fAverDeltaEffi = 0;
	}
	if(iE4Perc == TYPE_3500E4_100PERCENT && iBenchMark > 1)//����ȥ24Сʱû���κ���Ч����ʱ����benchmarkΪ90-96%ʱ����Ҫ��ȫ���ݡ�
	{
		fAverDeltaEffi = (float)iBenchMark/100;
	}

	if(WEB_ET_Info.stETDataSaveFlash.stETData365.iDataIndex > 7)
	{
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[%.2f,%.2f,%.2f],",fTotalEgySaved, fMoneySaved, 100*fAverDeltaEffi);
	}
	else
	{
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[%.2f,%.2f,-9999],",fTotalEgySaved, fMoneySaved);
	}
	//Last Year data
	fAverDeltaEffi = 0;
	fTotalEgySaved = 0;
	iDateCount = 0;
	for(i = 0; i < WEB_ET_Info.stETDataSaveFlash.stETData365.iDataIndex; i++)
	{
		fLoad = WEB_ET_Info.stETDataSaveFlash.stETData365.fAverLoad[i];
		fEgy = WEB_ET_Info.stETDataSaveFlash.stETData365.fPeriodEgy[i];
		fDeltaEffi = ETGetDeltaEffi(fLoad, &fCurrentEffi);
		if(!FLOAT_EQUAL0(fCurrentEffi) && !FLOAT_EQUAL0(fEgy))
		{
			fDeltaEgy = fEgy / (fCurrentEffi - fDeltaEffi) - fEgy / fCurrentEffi;
			fTotalEgySaved += fDeltaEgy;
			fAverDeltaEffi += fDeltaEffi;
			iDateCount++;
		}
	}
	fMoneySaved = fTotalEgySaved * pETCostSigValue->varValue.fValue;
	if(iDateCount != 0)
	{
		fAverDeltaEffi = fAverDeltaEffi / iDateCount;
	}
	else
	{
		fAverDeltaEffi = 0;
	}
	if(iE4Perc == TYPE_3500E4_100PERCENT && iBenchMark > 1)//����ȥ24Сʱû���κ���Ч����ʱ����benchmarkΪ90-96%ʱ����Ҫ��ȫ���ݡ�
	{
		fAverDeltaEffi = (float)iBenchMark/100;
	}

	if(WEB_ET_Info.stETDataSaveFlash.stETData365.iDataIndex > 30)
	{
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[%.2f,%.2f,%.2f],",fTotalEgySaved, fMoneySaved, 100*fAverDeltaEffi);
	}
	else
	{
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[%.2f,%.2f,-9999],",fTotalEgySaved, fMoneySaved);
	}

	//Total data

	if(WEB_ET_Info.stETDataSaveFlash.stETData365.iDataIndex >= 365)//�����㷨����ƽ��ÿһ��ģ�ֻ�����������Ի�������ݲ�һ�£���С��365ʱ����������һ��
	{
		fLoad = WEB_ET_Info.stETDataSaveFlash.fAverLoad;
		fEgy = WEB_ET_Info.stETDataSaveFlash.fTotalEgy;
		fDeltaEffi = ETGetDeltaEffi(fLoad, &fCurrentEffi);
		if(!FLOAT_EQUAL0(fCurrentEffi))
		{
			fTotalEgySaved += fEgy / (fCurrentEffi - fDeltaEffi) - fEgy / fCurrentEffi;
			fMoneySaved = fTotalEgySaved * pETCostSigValue->varValue.fValue;
			//float fAverDeltaEffi1 = (fDeltaEffi * WEB_ET_Info.stETDataSaveFlash.iTotalDays + fAverDeltaEffi * 365)/(365+WEB_ET_Info.stETDataSaveFlash.iTotalDays);
		}
	}

	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[%.2f,%.2f,-9999],",fTotalEgySaved, fMoneySaved);

	//Currency message
	itemp = pETCurrencySigValue->varValue.enumValue;
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[\"%s\",\"%s\"],",
		((SET_SIG_VALUE *)(pETCurrencySigValue))->pStdSig->pStateText[itemp]->pFullName[0],
		((SET_SIG_VALUE *)(pETCurrencySigValue))->pStdSig->pStateText[itemp]->pFullName[1]);

	if(iE4Perc == TYPE_3500E4_100PERCENT )
	{
		//Benchmark message
		itemp = iBenchMark;
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[\"%s\",\"%s\"],",
			((SET_SIG_VALUE *)(pETBenchmarkCurrSigValue))->pStdSig->pStateText[itemp]->pFullName[0],
			((SET_SIG_VALUE *)(pETBenchmarkCurrSigValue))->pStdSig->pStateText[itemp]->pFullName[1]);
		//ECO mix mode message
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[0],");
	}
	else
	{
		//Benchmark message
		itemp = 0;//3500e3
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[\"%s\",\"%s\"],",
			((SET_SIG_VALUE *)(pETBenchmarkCurrSigValue))->pStdSig->pStateText[itemp]->pFullName[0],
			((SET_SIG_VALUE *)(pETBenchmarkCurrSigValue))->pStdSig->pStateText[itemp]->pFullName[1]);
		//ECO mix mode message
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[1],");
	}

	if(iFileLen > 1)
	{
		iFileLen--;
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "]");
	}

//printf("%s\n",szTemp);
	
	WEB_ET_Info.bETDataRefresh = FALSE;
	return;
}

char* WEB_ET_GetSaveEgyAndMoney(void)
{
	char	*szTemp = NULL;
	szTemp = WEB_ET_Info.szETBufferHisData;

	if(sETInitSucc == FALSE)
	{
		return NULL;
	}
	else
	{
		return szTemp;
	}
}

void ET_MakeSaveEgyPresentData(void)
{
	float	fDeltaEffi, fLoad;
	float	fCurrentEffi;
	char	*szTemp = NULL;
	int		iError, iBufLength, iFileLen = 0;

	//if(sETInitSucc == FALSE)
	//{
	//	return;
	//}
	memset(WEB_ET_Info.szETBufferPresData, 0, ET_DATA_PRESENT_BUF_MAX_NUM);
	szTemp = WEB_ET_Info.szETBufferPresData;

	iBufLength = ET_DATA_PRESENT_BUF_MAX_NUM;


	if(FLOAT_EQUAL0(pSysRateCurrSigValue->varValue.fValue))
	{
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[0]");
		return;
	}

	fLoad = (pSysLoadSigValue->varValue.fValue/(pSysRateCurrSigValue->varValue.fValue));

	fDeltaEffi = 100 * ETGetDeltaEffi(fLoad, &fCurrentEffi);


	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[%.2f]",fDeltaEffi);

	return;
}

char* WEB_ET_GetSaveEgyPresent(void)
{
	char	*szTemp = NULL;
	szTemp = WEB_ET_Info.szETBufferPresData;

	if(sETInitSucc == FALSE)
	{
		return NULL;
	}
	else
	{
		return szTemp;
	}
}

/*==========================================================================*
* FUNCTION :  ET_SaveETData
* PURPOSE  :  Save the data calculted to flash every hour.
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS :   
* MODIFY   :  SongXu					 DATE: 2018-12-5
*==========================================================================*/
void ET_SaveETData()
{
	time_t		tNowTime;
	int			i;

	tNowTime = time((time_t *)0);
	//update the last DAY data , need to run before HOUR data because of save flash.
	if(tNowTime - WEB_ET_Info.stETDataSaveFlash.stETData365.tNewestDayFlag > 3600*24 || //һ�쵽��,���߸�ʱ�䵽ǰһ��
		tNowTime - WEB_ET_Info.stETDataSaveFlash.stETData365.tNewestDayFlag < 0)
	{
		if(WEB_ET_Info.stETDataSaveFlash.stETData365.iDataIndex < 365)//ʵ�ʴ���366�����ݣ������±�365
		{
			WEB_ET_Info.stETDataSaveFlash.stETData365.iDataIndex++;
		}
		else
		{
			//update the total data
			WEB_ET_Info.stETDataSaveFlash.fAverLoad = (WEB_ET_Info.stETDataSaveFlash.fAverLoad * WEB_ET_Info.stETDataSaveFlash.iTotalDays 
				+ WEB_ET_Info.stETDataSaveFlash.stETData365.fAverLoad[0])
				/(WEB_ET_Info.stETDataSaveFlash.iTotalDays + 1);
			WEB_ET_Info.stETDataSaveFlash.fTotalEgy += WEB_ET_Info.stETDataSaveFlash.stETData365.fPeriodEgy[0];
			WEB_ET_Info.stETDataSaveFlash.iTotalDays++;	
			//�ƶ����ݴ���
			for(i=0;i<365;i++)
			{
				WEB_ET_Info.stETDataSaveFlash.stETData365.fAverLoad[i] = WEB_ET_Info.stETDataSaveFlash.stETData365.fAverLoad[i+1];
				WEB_ET_Info.stETDataSaveFlash.stETData365.fPeriodEgy[i] = WEB_ET_Info.stETDataSaveFlash.stETData365.fPeriodEgy[i+1];
			}
		}
		WEB_ET_Info.stETDataSaveFlash.stETData365.fAverLoad[WEB_ET_Info.stETDataSaveFlash.stETData365.iDataIndex] = 0;
		WEB_ET_Info.stETDataSaveFlash.stETData365.fPeriodEgy[WEB_ET_Info.stETDataSaveFlash.stETData365.iDataIndex] = 0;
		WEB_ET_Info.stETDataSaveFlash.stETData365.iNewestDayLoadCount = 0;
		WEB_ET_Info.stETDataSaveFlash.stETData365.tNewestDayFlag = tNowTime / (3600*24) * (3600*24);


	}

	//update the last HOUR data 
	if(tNowTime - WEB_ET_Info.stETDataSaveFlash.stETData24.tNewestHourFlag > 3600 || //һСʱ����,��ʱ��
		tNowTime - WEB_ET_Info.stETDataSaveFlash.stETData24.tNewestHourFlag < 0)
	{
printf("Hour time %d %d\n",tNowTime , WEB_ET_Info.stETDataSaveFlash.stETData24.tNewestHourFlag);
		if(WEB_ET_Info.stETDataSaveFlash.stETData24.iDataIndex < 24)//ʵ�ʴ���25�����ݣ������±�24
		{
			WEB_ET_Info.stETDataSaveFlash.stETData24.iDataIndex++;
		}
		else//�ƶ�����
		{
			for(i=0;i<24;i++)
			{
				WEB_ET_Info.stETDataSaveFlash.stETData24.fAverLoad[i] = WEB_ET_Info.stETDataSaveFlash.stETData24.fAverLoad[i+1];
				WEB_ET_Info.stETDataSaveFlash.stETData24.fPeriodEgy[i] = WEB_ET_Info.stETDataSaveFlash.stETData24.fPeriodEgy[i+1];
			}
		}
		WEB_ET_Info.stETDataSaveFlash.stETData24.fAverLoad[WEB_ET_Info.stETDataSaveFlash.stETData24.iDataIndex] = 0;
		WEB_ET_Info.stETDataSaveFlash.stETData24.fPeriodEgy[WEB_ET_Info.stETDataSaveFlash.stETData24.iDataIndex] = 0;
		WEB_ET_Info.stETDataSaveFlash.stETData24.iNewestHourLoadCount = 0;
		WEB_ET_Info.stETDataSaveFlash.stETData24.tNewestHourFlag = tNowTime / 3600 * 3600;
		
		//save data to flash
		UpdateETDataToFlash(&(WEB_ET_Info.stETDataSaveFlash));
		//update the data for WEB display!!!
		ET_MakeSaveEgyAndMoneyData();
	}

	return;
}

void WEB_EfficiencyTracker(IN int *iQuitCommand)
{
	BOOL	bContinueRunning = TRUE;
	
	if(!WEB_ET_getEffiCurveFromCfg() || !WEB_ET_init() || !GetETDataInfoFromFlash())
	{
		WEB_ET_Info.bDisplayOnWeb = FALSE;
		return;
	}
	sETInitSucc = TRUE;
	
	ET_MakeSaveEgyAndMoneyData();

	while(bContinueRunning)
	{
		ET_CalculteLoadAndEgy();
		ET_SaveETData();
		ET_MakeSaveEgyPresentData();

		if(WEB_ET_Info.bETDataRefresh == TRUE)//��������źŷ����ı䣬��������ҳ��
		{
			ET_MakeSaveEgyAndMoneyData();
		}

		if(*iQuitCommand == SERVICE_STOP_RUNNING)
		{
			bContinueRunning = FALSE;
			printf("****efficiency tracker end*****\n");
			break;
		}
		Sleep(WEB_ET_RUN_INTERVAL * 1000);//yield
	}
	Mutex_Destroy(g_hMutexETDataSavedInfoToFlash);
	return;
}
