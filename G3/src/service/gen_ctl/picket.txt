2 0 8 6		Number of rectifiers ---Rect Group
2 2 1 48	Rated Voltage
2 2 2 100	Rated Current
1 2 38 1	Test State Set
//////////////////////////////////////////////
3 0 1 0		Rectifier on/off---------R1
3 0 2 53.5	Voltage
3 0 3 6		Origin Current
3 0 8 4001	Rectifier ID
3 0 10 3	Communication Interrupt count
3 0 16 0	RT_PRI_AC_ON_OFF_STA
3 3 1 0		RT_PRI_AC_FAIL
3 3 3 0		RT_PRI_FAULT
3 3 5 0		RT_PRI_PROTCTION
////////////////////////////////////////////
4 0 1 0		Rectifier on/off---------R2
4 0 2 53.5	Voltage
4 0 3 6		Origin Current
4 0 8 4002	Rectifier ID
4 0 10 3	Communication Interrupt count
4 0 16 0	RT_PRI_AC_ON_OFF_STA
4 3 1 0		RT_PRI_AC_FAIL
4 3 3 0		RT_PRI_FAULT
4 3 5 0		RT_PRI_PROTCTION
////////////////////////////////////////////
5 0 1 0		Rectifier on/off---------R3
5 0 2 53.5	Voltage
5 0 3 6		Origin Current
5 0 8 4003	Rectifier ID
5 0 10 3	Communication Interrupt count  
5 0 16 0	RT_PRI_AC_ON_OFF_STA
5 3 1 0		RT_PRI_AC_FAIL
5 3 3 0		RT_PRI_FAULT
5 3 5 0		RT_PRI_PROTCTION
///////////////////////////////////////////
6 0 1 0		Rectifier on/off---------R4
6 0 2 53.5	Voltage
6 0 3 6		Origin Current
6 0 8 4004	Rectifier ID
6 0 10 3	Communication Interrupt count
6 0 16 0	RT_PRI_AC_ON_OFF_STA
6 0 19 0	RT_PRI_AC_FAIL
6 0 21 0	RT_PRI_FAULT
6 0 22 0	RT_PRI_PROTCTION
//////////////////////////////////////////
7 0 1 0		Rectifier on/off---------R5
7 0 2 53.5	Voltage
7 0 3 6		Origin Current
7 0 8 4005	Rectifier ID
7 0 10 3	Communication Interrupt count
7 0 16 0	RT_PRI_AC_ON_OFF_STA
7 0 19 0	RT_PRI_AC_FAIL
7 0 21 0	RT_PRI_FAULT
7 0 22 0	RT_PRI_PROTCTION
///////////////////////////////////////////
8 0 1 0		Rectifier on/off---------R6
8 0 2 53.5	Voltage
8 0 3 0		Origin Current
8 0 8 4006	Rectifier ID
8 0 10 3	Communication Interrupt count
8 0 16 0	RT_PRI_AC_ON_OFF_STA
8 0 19 0	RT_PRI_AC_FAIL
8 0 21 0	RT_PRI_FAULT
8 0 22 0	RT_PRI_PROTCTION
//////////////////////////////////////////
//120 2 20 2	BC to FC Delay ---Batt Group
//120 2 31 500	Rated Capacity
//120 0 3 -272.0	Tempureture
//120 0 8 1	Const Curr Test Enb
//120 0 9 10	Const Curr Test Curr
//120 2 47 2	Test Number
///////////////////////////////////////////
//121 0 2 9	Battery Current ---B1
//52 0 3 90	Battery Capacity
//////////////////////////////////////////
//53 0 2 0	Battery Current ---B2
//53 0 3 190	Battery Capacity
//////////////////////////////////////////
//4 0 2 9	Battery Current ---B3
//54 0 3 900	Battery Capacity
//54 0 4 53.7	Voltage
///////////////////////////////////////////
//70 0 1 53.3	Voltage
///////////////////////////////////////////
//83 1 1 1	LVD1_PUB_CTL
//83 2 1 1	LVD1_PUB_ENB		
//83 2 2 0	LVD1_PUB_MODE		
//83 2 3 47.9	LVD1_PUB_VOLT		
//83 2 4 10	LVD1_PUB_TIME		
//83 2 5 52.5	LVD1_PUB_RECON_VOLT	
//83 2 6 2	LVD1_PUB_DELAY		
//83 0 1 0	LVD1_PUB_DISP		
//83 2 7 0	LVD1_PUB_DEPEND		
////////////////////////////////////////////////////////		
//84 1 1 1	LVD2_PUB_CTL
//84 2 1 1	LVD2_PUB_ENB		
//84 2 2 0	LVD2_PUB_MODE		
//84 2 3 47.0	LVD2_PUB_VOLT		
//84 2 4 600	LVD2_PUB_TIME		
//84 2 5 52.5	LVD2_PUB_RECON_VOLT	
//84 2 6 2	LVD2_PUB_DELAY		
//84 0 1 0	LVD2_PUB_DISP		
//84 2 7 0	LVD2_PUB_DEPEND		
///////////////////////////////////////////////////////
//85 1 1 0	LVD3_PUB_CTL		
//85 2 1 0	LVD3_PUB_ENB		
//85 2 2 0	LVD3_PUB_MODE		
//85 2 3 44.0	LVD3_PUB_VOLT		
//85 2 4 600	LVD3_PUB_TIME		
//85 2 5 52.5	LVD3_PUB_RECON_VOLT	
//85 2 6 2	LVD3_PUB_DELAY		
//85 0 1 0	LVD3_PUB_DISP		
//85 2 7 0	LVD3_PUB_DEPEND		
///////////////////////////////////////////
//92 0 7 1		Site door closed and locked	----MainSwitch
//92 0 8 1		Main switch open		
//92 0 9 1		Automatic operation		
//92 0 10 1		Differential relay tripped
//////////////////////////////////////////////////
//86 0 11 1.12		AC_PUB_TOTAL_POWER
//1 2 9 1		Lower consumption in HCH enabled
//1 2 10 1		Max Power Limit Enabled
//1 2 17 0.01		MPCL Control Step
//1 2 19 1		MPCL Battery Discharge Enabled
//1 2 20 0		MPCL Diesel Control Enabled



