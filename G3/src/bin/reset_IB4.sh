#!/bin/sh

IB4_STATUS1=`dmesg | grep "idProduct=9512" `
IB4_STATUS2=` cat /home/app_script/ethernet.log `
#echo IB4_STATUS1= $IB4_STATUS1
#echo IB4_STATUS2= $IB4_STATUS2

if [ -n "$IB4_STATUS1" -o "$IB4_STATUS2" == "eth:2" ]
then

	IB4_DETC=`/sbin/ifconfig | grep "eth1" `
	#echo IB4_DETC= $IB4_DETC
	if [ "$IB4_DETC" == "" ]
	then
		SMSC9500=`/sbin/lsmod | grep smsc9500 `
		if [ -n "$SMSC9500" ]
		then
        		#echo remove IB4 drivers!
			rmmod smsc9500
        		rmmod smscusbnet
		fi
		
		sleep 1
		
		insmod /home/driver/smscusbnet.ko
		insmod /home/driver/smsc9500.ko

		sleep 1
		DHCPsSET=$(ps|grep udhcpd|grep -v grep|cut -c 0-5)
		if [ ! -n "$DHCPsSET" ]
		then
			#echo start DHCP_SERVER ON ETH0
			/home/app_script/eth1_detect
		else
			#echo DHCP_SERVER had on
			/home/app_script/init_eth1_hw
        		/home/app_script/init_eth1
		fi
		
	#else
		#echo eth1 have already exist!	
	fi
	
	

fi
			


