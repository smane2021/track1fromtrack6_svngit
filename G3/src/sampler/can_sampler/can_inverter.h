/*==========================================================================*
*    Copyright(c) 2021, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : NGMC-HF(Next Generation Controller - High Feature)
*
*  FILENAME : can_inverter.h
*  CREATOR  : Frank Cao                DATE: 2008-06-24 16:47
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/


#ifndef __CAN_SAMPLER_INVERTER_H
#define __CAN_SAMPLER_INVERTER_H

#include <stdio.h>

#include "basetypes.h"
#include "err_code.h"
#include "pubfunc.h"
#include "string.h"
#include "can_sampler_main.h"

#define	AC_1_PHASE				0
#define	AC_3_PHASE				1


#define	INVT_COMM_NORMAL_ST				0
#define	INVT_COMM_ALL_INTERRUPT_ST		1
#define	INVT_COMM_INTERRUPT_ST			2


#define INVT_EQUIP_EXISTENT				0
#define INVT_EQUIP_NOT_EXISTENT			1

#define INVT_MAX_INTERRUPT_TIMES			2
#define INVT_OUTPUT_ON_3					3
#define INVT_OUTPUT_OFF_0					0

#define INVT_CMD00_RCV_FRAMES_NUM			10
#define INVT_CMD10_RCV_FRAMES_NUM			4
#define INVT_CMD20_RCV_FRAMES_NUM			9

#define INVT_SINGLE_PHASE					0
#define INVT_THREE_PHASE					1

#define	INVT_GROUP_EQUIPID				1900
#define INVT_START_EQUIP_ID				1901

#define INVT_POS_SET_SIG_ID				1
#define INVT_PHASE_SET_SIG_ID				2
#define INVT_CONFIRM_POS_PHASE_SIGID		21
#define INVT_FLOAT_VOLT_SIGID				16
#define INVT_OVER_VOLTAGE_SIGID			3
#define INVT_DEFAULT_VOLT_SIGID				65

#define INVT_ON_OFF_SIGID		4

#define INVT_WORK_MODE_SIGID	9

//update inverter
#define INVT_UPDATE					54
#define INVT_FORCEUPDATE			55

#define INVT_SAMP_UpLoadOK_Number			39
#define INVT_SAMP_UpLoadOK_State			40

#define	INVT_OUTPUT_ON						0
#define	INVT_OUTPUT_OFF						1
#define	INVT_OUTPUT_NO_CHANGE				2

#define	INVT_DC_ON						0
#define	INVT_DC_OFF						1
#define	INVT_RESET_NORMAL					0
#define	INVT_RESET_ENB					1
#define	INVT_WALKIN_NORMAL				0
#define	INVT_WALKIN_ENB					1
#define	INVT_FAN_AUTO						0
#define	INVT_FAN_FULL_SPEED				1
#define	INVT_LED_NORMAL					0
#define	INVT_LED_BLINK					1
#define	INVT_AC_ON						0
#define	INVT_AC_OFF						1
#define	INVT_CAN_NORMAL					0
#define	INVT_CAN_INIT						1
#define	INVT_OV_RESET_DISB				0
#define	INVT_OV_RESET_ENB					1
#define	INVT_LED_NOT_BLINK				0
#define	INVT_LED_BLINK					1
#define	INVT_AC_OV_PROTECT_ENB			0
#define	INVT_AC_OV_PROTECT_DISB			1
#define	INVT_DEGRADATIVE					0
#define	INVT_FULL_POWER_CAP_ENB			1
#define	INVT_POWER_LIMIT_A			0
#define	INVT_POWER_LIMIT_B			1

#define INVT_DLOAD_WAIT_RESTART_TIME	30000 //30S,after downloading, wait for restarting 

#define	INVT_POSITION_INVAILID	(-1)
#define	AC_PHASE_INVAILID		(-1)

enum INVT_PHASE_DEF
{
	INVT_AC_PHASE_A = 0,
	INVT_AC_PHASE_B,
	INVT_AC_PHASE_C,

	INVT_AC_PHASE_NUM,
};

//The channel No. is defined in "Design for NGMC-HF Software �C CAN Port Sampler Module"
enum INVT_SAMP_CHANNEL
{
	INVT_CH_INVT_NUM = 57001,		//Total Inverter Number
	INVT_CH_OUTPUT_CURR,
	//INVT_CH_TOTAL_USED_CAPACITY,
	//INVT_CH_COMM_INVT_NUM,
	//INVT_CH_OUTPUT_ON_COMM_INVT_NUM,
	INVT_CH_PHASE_TYPE = 57006,
	INVT_CH_TOTAL_OUTPUT_POWER,
	INVT_CH_ALL_NO_RESPONSE,
	//INVT_CH_LOST_ALM,
	INVT_CH_PHASE1_NUM = 57010,
	INVT_CH_PHASE2_NUM,
	INVT_CH_PHASE3_NUM,
	INVT_CH_PHASE1_CURR,
	INVT_CH_PHASE2_CURR,
	INVT_CH_PHASE3_CURR,
	INVT_CH_PHASE1_POWER_kW,
	INVT_CH_PHASE2_POWER_kW,
	INVT_CH_PHASE3_POWER_kW,
	INVT_CH_PHASE1_POWER_kVA,
	INVT_CH_PHASE2_POWER_kVA,
	INVT_CH_PHASE3_POWER_kVA,
	INVT_CH_TOTAL_RATED_CURRENT,
	INVT_CH_TOTAL_INPUT_ENERGY = 57023,
	INVT_CH_TOTAL_OUTPUT_ENERGY,
	//alarm 
	INVT_CH_GROUP_REPO,

	INVT_CH_INPUT_DC_CURR,
	INVT_CH_INPUT_AC_CURR,
	INVT_CH_INVT_WORK_STATUS,
	
	INVT_CH_SAMP_VOLT_TYPE,
	INVT_CH_SAMP_FREQ_LEVEL,
	INVT_CH_SAMP_WORK_MODE,
	//INVT_CH_SAMP_LOW_VOLT_TR,
	//INVT_CH_SAMP_LOW_VOLT_CB,
	//INVT_CH_SAMP_HIGH_VOLT_TR,
	//INVT_CH_SAMP_HIGH_VOLT_CB,
	INVT_CH_INPUT_AC_VOLT = 57036,

	//INVT_CH_AC_PHASE_TYPE, //vaidya change

	INVT_CH_AC_P_A_VOLT = 57038,			            //AC Phase A Voltage
	INVT_CH_AC_P_B_VOLT = 57039,	//vaidya change		//AC Phase B Voltage
	INVT_CH_AC_P_C_VOLT = 57040,			            //AC Phase C Voltage


	INVT_CH_AC_P_A_CURR = 57041,			            //AC Phase A Voltage
	INVT_CH_AC_P_B_CURR = 57042,	//Koustubh change	//AC Phase B Voltage
	INVT_CH_AC_P_C_CURR = 57043,			            //AC Phase C Voltage

	INVT_CH_AC_P_A_OUT_VOLT = 57044,
	INVT_CH_AC_P_B_OUT_VOLT = 57045,
	INVT_CH_AC_P_C_OUT_VOLT = 57046,
	INVT_CH_AC_P_A_IN_VOLT = 57047,
	INVT_CH_AC_P_B_IN_VOLT = 57048,
	INVT_CH_AC_P_C_IN_VOLT = 57049,

	INVT_CH_GROUP_EXIST = 57099,

	INVT_CH_OUTPUT_VOLTAGE = 57101,
	INVT_CH_OUTPUT_CURRENT,
	INVT_CH_INPUT_AC_FREQUENCY,
	INVT_CH_INPUT_AC_POWER,
	INVT_CH_INPUT_DC_VOLTAGE,
	INVT_CH_INPUT_DC_CURRENT,
	INVT_CH_INPUT_DC_POWER,
	INVT_CH_TEMPERATURE,
	INVT_CH_INPUT_AC_VOLTAGE,
	INVT_CH_INPUT_AC_CURRENT,
	INVT_CH_OUTPUT_FACTOR,
	INVT_CH_OUTPUT_FREQUENCY,
	INVT_CH_OUTPUT_CAPACITY_VA,
	INVT_CH_OUTPUT_POWER_VA,
	INVT_CH_OUTPUT_POWER_W,
	INVT_CH_ENERGY_INPUT_AC,
	INVT_CH_ENERGY_OUTPUT,

	INVT_CH_PHASE_BELONGING,

	//alarm
	INVT_CH_INVT_FAIL_SUMMARY,
	INVT_CH_INVT_INPUT_AC_VOLTAGE_ABNORMAL,
	INVT_CH_INVT_INPUT_DC_VOLTAGE_ABNORMAL,
	//INVT_CH_INPUT_AC_FREQUENCY_ABNORMAL,
	INVT_CH_OVER_TEMPERATURE,
	INVT_CH_FAN_FAULT,
	//INVT_CH_INPUT_DC_HIGH_VOLTAGE,
	//INVT_CH_INPUT_DC_VOLTAGE_LOW,
	INVT_CH_ESTOP,
	INVT_CH_OVER_LOAD1,
	INVT_CH_OVER_LOAD2,
	INVT_CH_CAN_ADDR_ERR,
	//INVT_CH_SHARE_FAULT,
	//INVT_CH_LOCAL_SETTING_ASYNC,
	//INVT_CH_OUTPUT_HIGH_VOLTAGE,
	//INVT_CH_OUTPUT_HIGH_CURRENT,
	//INVT_CH_RELAY_WELDED,

	//INVT_CH_PARALLEL_COMM_FAIL,
	INVT_CH_LOW_SN = 57128,
	//INVT_CH_OUTPUT_STATUS,
	INVT_CH_POSITION_ID,
	//for inverter phase count
	//INVT_CH_PHASE_NO,//vaidya change
	//status
	INVT_CH_LOCAL_SYNC_OK,
	INVT_CH_SYSTEM_SYNC_OK,
	//INVT_CH_RESTART_OVER_VOLTAGE,
	INVT_CH_SHUTDOWN_OVER_TEMP,
	INVT_CH_SHUTDOWN_SHORT,
	INVT_CH_START_REMOTE,
	INVT_CH_STOP_REMOTE,
	INVT_CH_INPUT_POWER_SUPPLY,
	INVT_CH_PFC_STATUS,
	INVT_CH_OUTPUT_STATUS,
	INVT_CH_PHASE_INITIAL,
	INVT_CH_VOLTAGE_INITIAL,
	INVT_CH_FREQUENCY_INITIAL,

	INVT_CH_DC_INPUT_VOLT_LEVEL,
	INVT_CH_AC_INPUT_VOLT_LEVEL,
	INVT_CH_RATED_CURR,
	INVT_CH_OUTPUT_VOLT_LEVEL,
	INVT_CH_OVER_LOAD_UP_TIMES,
	INVT_CH_TOTAL_RUN_TIME = 57147,
	INVT_CH_ENERGY_INPUT_DC,
	//alarm
	INVT_CH_PARALLEL_ANOMALY,
	INVT_CH_PARALLEL_OUT_OF_SYNC,
	INVT_CH_PARALLEL_CAN_CMM_FAIL,
	INVT_CH_PHASE_ANOMALY,
	INVT_CH_REPO,
	INVT_CH_COMM_STATUS = 57198,
	INVT_CH_EXIST_STATUS,

};


//Please refer to the document of CAN protocol 
/////////////////////////////////////////////
#define INVT_VAL_TYPE_R_OUTPUT_CURR_VOLT			0x01
#define INVT_VAL_TYPE_R_INPUT_AC_FRE_POW			0x02
#define INVT_VAL_TYPE_R_INPUT_DC_VOLT_CURR			0x03
#define INVT_VAL_TYPE_R_INPUT_DC_POW_TEMP			0x04
#define INVT_VAL_TYPE_R_INPUT_AC_VOLT_CURR			0x05
#define INVT_VAL_TYPE_R_OUTPUT_POW_FACTOR_FRE		0x06
#define INVT_VAL_TYPE_R_OUTPUT_CAPACITY				0x07
#define INVT_VAL_TYPE_R_OUTPUT_POWER				0x08
#define INVT_VAL_TYPE_R_ENERGY_INPUT_AC			0x09
#define INVT_VAL_TYPE_R_ENERGY_INPUT_DC			0x0A
#define INVT_VAL_TYPE_R_ENERGY_OUTPUT			0x0B
#define INVT_VAL_TYPE_R_U1BOARD_TEMP		0x0B
#define INVT_VAL_TYPE_R_P_A_VOLT			0x0C
#define INVT_VAL_TYPE_R_P_B_VOLT			0x0D
#define INVT_VAL_TYPE_R_P_C_VOLT			0x0E
#define INVT_VAL_TYPE_R_RATED_VOLT		0x0F
#define INVT_VAL_TYPE_R_PFC_TEMP			0x10
#define INVT_VAL_TYPE_R_RATED_POWER		0x11
#define INVT_VAL_TYPE_R_RATED_CURR		0x12
#define INVT_VAL_TYPE_R_RATED_INPUT_VOLT	0x13
#define INVT_VAL_TYPE_R_POWER_LMT_POINT	0x14

#define INVT_VAL_TYPE_R_ALARM_BITS		0x40
#define INVT_VAL_TYPE_R_STATUS_BITS		0x41


#define INVT_VAL_TYPE_R_FEATURE			0x51

#define INVT_VAL_TYPE_R_SN_LOW			0x54
#define INVT_VAL_TYPE_R_SN_HIGH			0x55
#define INVT_VAL_TYPE_R_VER_NO			0x56

#define INVT_VAL_TYPE_R_RUN_TIME			0x58

#define INVT_VAL_TYPE_R_BARCODE1			0x5A
#define INVT_VAL_TYPE_R_BARCODE2			0x5B
#define INVT_VAL_TYPE_R_BARCODE3			0x5C
#define INVT_VAL_TYPE_R_BARCODE4			0x5D
#define INVT_VAL_TYPE_R_13F_COMPLEX_LOW				0x3F
#define INVT_VAL_TYPE_R_13F_COMPLEX_HIGH			0x1
////////////////////////////////////////////

//Following is the data to be wrote to inverter
#define INVT_VAL_TYPE_W_AC_OUTPUT_ON_OFF			0x30
#define INVT_VAL_TYPE_W_ESTOP_ENB			0x16
//#define INVT_VAL_TYPE_W_SEQ_START_TIME			0x2A	//Sequence start interval
#define INVT_VAL_TYPE_W_PHASE_1_3				0x120	//1phase or 3phase
#define INVT_VAL_TYPE_W_PHASE_SETTING			0x121	//phase setting
#define INVT_VAL_TYPE_W_VOLT_LEVEL				0x122	//voltage level
#define INVT_VAL_TYPE_W_FREQUENCE_LEVEL			0x123	//frequence level
#define INVT_VAL_TYPE_W_SOURCE_POWER_RATIO		0x124	//Source power ratio DC vs AC
#define INVT_VAL_TYPE_W_INVT_WORK_MODE			0x125	//restart time on over voltage
#define INVT_VAL_TYPE_W_DC_LOW_TRANSFER			0x126	//DC Input low voltage level transfer 
#define INVT_VAL_TYPE_W_DC_LOW_COMEBACK			0x127	//DC Input low voltage level comeback 
#define INVT_VAL_TYPE_W_DC_HIGH_TRANSFER		0x128	//DC Input high voltage level transfer
#define INVT_VAL_TYPE_W_DC_HIGH_COMEBACK		0x129	//DC Input high voltage level comeback
#define INVT_VAL_TYPE_W_CLEAR_FAULT				0x105	//DC Input high voltage level transfer
#define INVT_VAL_TYPE_W_RESET_ENERGY			0x109	//DC Input high voltage level comeback

#define INVT_VAL_TYPE_W_PHASE_SET				0x121	//Phase setting


#define INVT_VAL_TYPE_W_CURR_LMT			0x22
#define INVT_VAL_TYPE_W_HIGH_VOLT			0x23
#define INVT_VAL_TYPE_W_DC_VOLT			0x21
#define INVT_VAL_TYPE_W_DEFAULT_VOLT		0x24
//#define	INVT_VAL_TYPE_W_FULL_POWER_ENB	0x27		//Error!! It should be 0x2F
#define	INVT_VAL_TYPE_W_FULL_POWER_ENB	0x2F
#define INVT_VAL_TYPE_W_RESTART_ON_OV		0x31
//#define INVT_VAL_TYPE_W_FAN_FULL_SPEED	0x33
#define INVT_VAL_TYPE_W_LED_BLINK			0x34
#define INVT_VAL_TYPE_W_AC_ON_OFF			0x35
#define INVT_VAL_TYPE_W_AC_CURR_LIMIT		0x1a	//Input current limit

#define INVT_VAL_TYPE_W_RUN_TIME			0x58


struct	tagINVT_TYPE_INFO
{
	UINT		uiTypeNo;
	float		fRatedVolt;
	float		fRatedCurr;
	int			iDcDcType;
	int			iAcInputType;
	float		fAcRatedVolt;
	float		fPower;
	int			iEfficiencyNo;
};
typedef struct tagINVT_TYPE_INFO INVT_TYPE_INFO;

struct	tagINVT_POS_ADDR
{
	DWORD			dwSerialNo;//��Barcode1����
	DWORD			dwHiSN; 
	int			iPositionNo;
	int			iPhaseNo; //vaidya change
	BOOL			bIsNewInserted; //�Ƿ��²����?
	int			iSeqNo; //���ź��λ��?
};
typedef struct tagINVT_POS_ADDR INVT_POS_ADDR;

struct tagSynAlarm{
	int				iSetSigID;
	SAMPLING_VALUE	valInvt;
};
typedef struct tagSynAlarm INVT_SYN_ALARM;

void INVT_Reconfig(BOOL bScanAllAddr);
void INVT_TriggerAllocation(void);
void INVT_InitPosAndPhase(void);
void INVT_InitRoughValue(void);
void INVT_SendCtlCmd(int iChannelNo, float fParam);
void INVT_Sample(void);
void INVT_StuffChannel(ENUMSIGNALPROC EnumProc,	//Callback function for stuffing channels
		     LPVOID lpvoid);			//Parameter of the callback function
BOOL InvtIsNeedReConfig();
BOOL InvtIsNeedReConfigWhenACFail();
INT32 INVT_WaitAllocation();
VOID Invt_GetProdctInfo(HANDLE hComm, int nUnitNo, PRODUCT_INFO *pPI);
#endif
