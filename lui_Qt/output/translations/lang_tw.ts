<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="zh_TW">
<context>
    <name>CMenuData</name>
    <message>
        <source>Settings</source>
        <translation type="obsolete">參數</translation>
    </message>
    <message>
        <source>Maintenance</source>
        <translation type="obsolete">控制輸出</translation>
    </message>
    <message>
        <source>Energy Saving</source>
        <translation type="obsolete">節能參數</translation>
    </message>
    <message>
        <source>Alarm Settings</source>
        <translation type="obsolete">告警參數</translation>
    </message>
    <message>
        <source>Rect Settings</source>
        <translation type="obsolete">模塊參數</translation>
    </message>
    <message>
        <source>FCUP Settings</source>
        <translation type="obsolete">FCUP參數</translation>
    </message>
    <message>
        <source>FCUP1 Settings</source>
        <translation type="obsolete">FCUP1參數</translation>
    </message>
    <message>
        <source>FCUP2 Settings</source>
        <translation type="obsolete">FCUP2參數</translation>
    </message>
    <message>
        <source>FCUP3 Settings</source>
        <translation type="obsolete">FCUP3參數</translation>
    </message>
    <message>
        <source>FCUP4 Settings</source>
        <translation type="obsolete">FCUP4參數</translation>
    </message>
    <message>
        <source>Batt Settings</source>
        <translation type="obsolete">電池參數</translation>
    </message>
    <message>
        <source>LVD Settings</source>
        <translation type="obsolete">LVD參數</translation>
    </message>
    <message>
        <source>AC Settings</source>
        <translation type="obsolete">交流參數</translation>
    </message>
    <message>
        <source>Sys Settings</source>
        <translation type="obsolete">系統參數</translation>
    </message>
    <message>
        <source>Comm Settings</source>
        <translation type="obsolete">通信參數</translation>
    </message>
    <message>
        <source>Other Settings</source>
        <translation type="obsolete">其他參數</translation>
    </message>
    <message>
        <source>Slave Settings</source>
        <translation type="obsolete">從機參數</translation>
    </message>
    <message>
        <source>DO Normal Settings</source>
        <translation type="obsolete">DO參數</translation>
    </message>
    <message>
        <source>Basic Settings</source>
        <translation type="obsolete">基本參數</translation>
    </message>
    <message>
        <source>Charge</source>
        <translation type="obsolete">充電管理</translation>
    </message>
    <message>
        <source>Battery Test</source>
        <translation type="obsolete">電池測試</translation>
    </message>
    <message>
        <source>Temp Comp</source>
        <translation type="obsolete">溫補參數</translation>
    </message>
    <message>
        <source>Batt1 Settings</source>
        <translation type="obsolete">電池1參數</translation>
    </message>
    <message>
        <source>Batt2 Settings</source>
        <translation type="obsolete">電池2參數</translation>
    </message>
    <message>
        <source>Restore Default</source>
        <translation type="obsolete">恢複默認配置</translation>
    </message>
    <message>
        <source>Update App</source>
        <translation type="obsolete">升級應用程序</translation>
    </message>
    <message>
        <source>Clear data</source>
        <translation type="obsolete">清除數據</translation>
    </message>
    <message>
        <source>Auto Config</source>
        <translation type="obsolete">自動配置</translation>
    </message>
    <message>
        <source>LCD Display Wizard</source>
        <translation type="obsolete">LCD顯示向導</translation>
    </message>
    <message>
        <source>Start Wizard Now</source>
        <translation type="obsolete">啓動向導</translation>
    </message>
    <message>
        <source>System DO</source>
        <translation type="obsolete">系統DO</translation>
    </message>
    <message>
        <source>IB2 DO</source>
        <translation type="obsolete">IB2 DO</translation>
    </message>
    <message>
        <source>EIB1 DO</source>
        <translation type="obsolete">EIB1 DO</translation>
    </message>
    <message>
        <source>EIB2 DO</source>
        <translation type="obsolete">EIB2 DO</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">是</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">否</translation>
    </message>
    <message>
        <source>Protocol</source>
        <translation type="obsolete">協議</translation>
    </message>
    <message>
        <source>Address</source>
        <translation type="obsolete">地址</translation>
    </message>
    <message>
        <source>Media</source>
        <translation type="obsolete">媒質</translation>
    </message>
    <message>
        <source>Baudrate</source>
        <translation type="obsolete">波特率</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="obsolete">日期</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">時間</translation>
    </message>
    <message>
        <source>IP Address</source>
        <translation type="obsolete">IP地址</translation>
    </message>
    <message>
        <source>Mask</source>
        <translation type="obsolete">子網掩碼</translation>
    </message>
    <message>
        <source>Gateway</source>
        <translation type="obsolete">網關</translation>
    </message>
    <message>
        <source>DHCP</source>
        <translation type="obsolete">DHCP功能</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="obsolete">禁止</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation type="obsolete">開啓</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">錯誤</translation>
    </message>
    <message>
        <source>IPV6 IP</source>
        <translation type="obsolete">IPV6 IP</translation>
    </message>
    <message>
        <source>IPV6 Prefix</source>
        <translation type="obsolete">IPV6前綴</translation>
    </message>
    <message>
        <source>IPV6 Gateway</source>
        <translation type="obsolete">IPV6網關</translation>
    </message>
    <message>
        <source>IPV6 DHCP</source>
        <translation type="obsolete">IPV6 DHCP</translation>
    </message>
    <message>
        <source>Clear Data</source>
        <translation type="obsolete">清除數據</translation>
    </message>
    <message>
        <source>Alarm History</source>
        <translation type="obsolete">曆史告警</translation>
    </message>
</context>
<context>
    <name>CtrlInputChar</name>
    <message>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="291"/>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="304"/>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="317"/>
        <source>.</source>
        <translation>.</translation>
    </message>
</context>
<context>
    <name>DlgInfo</name>
    <message>
        <source>ENT to OK</source>
        <translation type="obsolete">按ENT確定</translation>
    </message>
    <message>
        <source>ESC to Cancel</source>
        <translation type="obsolete">按ESC取消</translation>
    </message>
    <message>
        <source>Update OK Num</source>
        <translation type="obsolete">升級成功數</translation>
    </message>
    <message>
        <source>Open File Failed</source>
        <translation type="obsolete">打開文件失敗</translation>
    </message>
    <message>
        <source>Comm Time-Out</source>
        <translation type="obsolete">通信超時</translation>
    </message>
    <message>
        <source>ENT or ESC to exit</source>
        <translation type="obsolete">按ENT或ESC退出</translation>
    </message>
    <message>
        <source>Rebooting</source>
        <translation type="obsolete">重啓</translation>
    </message>
</context>
<context>
    <name>DlgUpdateApp</name>
    <message>
        <location filename="../../g3_lui/util/DlgUpdateApp.cpp" line="44"/>
        <source>Qt Exited</source>
        <translation>Qt退出</translation>
    </message>
</context>
<context>
    <name>FirstStackedWdg</name>
    <message>
        <source>Alarm</source>
        <translation type="obsolete">告警</translation>
    </message>
    <message>
        <source>Active Alarms</source>
        <translation type="obsolete">當前告警</translation>
    </message>
    <message>
        <source>Alarm History</source>
        <translation type="obsolete">曆史告警</translation>
    </message>
</context>
<context>
    <name>GuideWindow</name>
    <message>
        <source>Installation Wizard</source>
        <translation type="obsolete">安裝向導</translation>
    </message>
    <message>
        <source>ENT to continue</source>
        <translation type="obsolete">按ENT繼續</translation>
    </message>
    <message>
        <source>ESC to skip </source>
        <translation type="obsolete">按ESC跳過</translation>
    </message>
    <message>
        <source>OK to exit</source>
        <translation type="obsolete">是否退出</translation>
    </message>
    <message>
        <source>ESC to exit</source>
        <translation type="obsolete">按ESC退出</translation>
    </message>
    <message>
        <source>Wizard finished</source>
        <translation type="obsolete">向導結束</translation>
    </message>
    <message>
        <source>Site Name</source>
        <translation type="obsolete">站名</translation>
    </message>
    <message>
        <source>Battery Settings</source>
        <translation type="obsolete">電池設置</translation>
    </message>
    <message>
        <source>Capacity Settings</source>
        <translation type="obsolete">容量設置</translation>
    </message>
    <message>
        <source>ECO Parameter</source>
        <translation type="obsolete">節能參數</translation>
    </message>
    <message>
        <source>Alarm Settings</source>
        <translation type="obsolete">告警設置</translation>
    </message>
    <message>
        <source>Common Settings</source>
        <translation type="obsolete">通用設置</translation>
    </message>
    <message>
        <source>IP address</source>
        <translation type="obsolete">IP地址</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="obsolete">日期</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">時間</translation>
    </message>
    <message>
        <source>DHCP</source>
        <translation type="obsolete">DHCP功能</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="obsolete">禁止</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation type="obsolete">開啓</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">錯誤</translation>
    </message>
    <message>
        <source>IP Address</source>
        <translation type="obsolete">IP地址</translation>
    </message>
    <message>
        <source>MASK</source>
        <translation type="obsolete">子網掩碼</translation>
    </message>
    <message>
        <source>Gateway</source>
        <translation type="obsolete">網關</translation>
    </message>
    <message>
        <source>Rebooting</source>
        <translation type="obsolete">重啓</translation>
    </message>
    <message>
        <source>Set language failed</source>
        <translation type="obsolete">設置語言失敗</translation>
    </message>
    <message>
        <source>Adjust LCD</source>
        <translation type="obsolete">LCD自適應</translation>
    </message>
</context>
<context>
    <name>LoginWindow</name>
    <message>
        <source>Select User</source>
        <translation type="obsolete">選擇用戶</translation>
    </message>
    <message>
        <source>Enter Password</source>
        <translation type="obsolete">輸入密碼</translation>
    </message>
    <message>
        <source>Password error</source>
        <translation type="obsolete">密碼錯誤</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>OK to reboot</source>
        <translation type="obsolete">確定重啓嗎</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="347"/>
        <source>OK to clear</source>
        <translation>確定清除嗎</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="348"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="402"/>
        <source>Please wait</source>
        <translation>請等待</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="349"/>
        <source>Set successful</source>
        <translation>設置成功</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="54"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="350"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="614"/>
        <source>Set failed</source>
        <translation>設置失敗</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="364"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="375"/>
        <source>OK to change</source>
        <translation>確定更改嗎</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="401"/>
        <source>OK to update</source>
        <translation>確定升級嗎</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="403"/>
        <source>Update successful</source>
        <translation>升級成功</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="404"/>
        <source>Update failed</source>
        <translation>升級失敗</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="289"/>
        <source>Installation Wizard</source>
        <translation>安裝向導</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="293"/>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="352"/>
        <source>ENT to continue</source>
        <translation>按ENT繼續</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="297"/>
        <source>ESC to skip </source>
        <translation>按ESC跳過</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="344"/>
        <source>OK to exit</source>
        <translation>是否退出</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="348"/>
        <source>ESC to exit</source>
        <translation>按ESC退出</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="366"/>
        <source>Wizard finished</source>
        <translation>向導結束</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="518"/>
        <source>Site Name</source>
        <translation>站名</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="522"/>
        <source>Battery Settings</source>
        <translation>電池設置</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="526"/>
        <source>Capacity Settings</source>
        <translation>容量設置</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="530"/>
        <source>ECO Parameter</source>
        <translation>節能參數</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="534"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="229"/>
        <source>Alarm Settings</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="538"/>
        <source>Common Settings</source>
        <translation>通用設置</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="542"/>
        <source>IP address</source>
        <translation>IP地址</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="560"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="901"/>
        <source>Date</source>
        <translation>日期</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="575"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="906"/>
        <source>Time</source>
        <translation>時間</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="603"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="927"/>
        <source>DHCP</source>
        <translation>DHCP功能</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="610"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="928"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="961"/>
        <source>Disabled</source>
        <translation>禁止</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="611"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="929"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="962"/>
        <source>Enabled</source>
        <translation>開啓</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="612"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="930"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="963"/>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="632"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="912"/>
        <source>IP Address</source>
        <translation>IP地址</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="636"/>
        <source>MASK</source>
        <translation>子網掩碼</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="640"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="922"/>
        <source>Gateway</source>
        <translation>網關</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="864"/>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2222"/>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="570"/>
        <source>Rebooting</source>
        <translation>重啓</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="1324"/>
        <source>Set language failed</source>
        <translation>設置語言失敗</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="1355"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="536"/>
        <source>Adjust LCD</source>
        <translation>LCD自適應</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="853"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="1464"/>
        <location filename="../../g3_lui/equipWidget/Wdg2DCABranch.cpp" line="340"/>
        <location filename="../../g3_lui/equipWidget/Wdg2P5BattRemainTime.cpp" line="263"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1167"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="502"/>
        <source>No Data</source>
        <translation>沒有數據</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="132"/>
        <source>Select User</source>
        <translation>選擇用戶</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="135"/>
        <source>Enter Password</source>
        <translation>輸入密碼</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="349"/>
        <source>Password error</source>
        <translation>密碼錯誤</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/mainwindow.cpp" line="422"/>
        <source>OK to reboot</source>
        <translation>確定重啓嗎</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="203"/>
        <source>Settings</source>
        <translation>參數</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="213"/>
        <source>Maintenance</source>
        <translation>控制輸出</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="221"/>
        <source>Energy Saving</source>
        <translation>節能參數</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="237"/>
        <source>Rect Settings</source>
        <translation>模塊參數</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="245"/>
        <source>Batt Settings</source>
        <translation>電池參數</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="253"/>
        <source>LVD Settings</source>
        <translation>LVD參數</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="261"/>
        <source>AC Settings</source>
        <translation>交流參數</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="269"/>
        <source>Sys Settings</source>
        <translation>系統參數</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="277"/>
        <source>Comm Settings</source>
        <translation>通信參數</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="285"/>
        <source>Other Settings</source>
        <translation>其他參數</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="294"/>
        <source>Slave Settings</source>
        <translation>從機參數</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="302"/>
        <source>FCUP Settings</source>
        <translation>FCUP參數</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="310"/>
        <source>DO Normal Settings</source>
        <translation>DO參數</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="322"/>
        <source>Basic Settings</source>
        <translation>基本參數</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="330"/>
        <source>Charge</source>
        <translation>充電管理</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="338"/>
        <source>Battery Test</source>
        <translation>電池測試</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="346"/>
        <location filename="../../g3_lui/equipWidget/WdgFP8BattDegCurve.cpp" line="106"/>
        <source>Temp Comp</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="354"/>
        <source>Batt1 Settings</source>
        <translation>電池1參數</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="362"/>
        <source>Batt2 Settings</source>
        <translation>電池2參數</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="371"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="808"/>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1997"/>
        <source>Restore Default</source>
        <translation>恢複默認配置</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="379"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="421"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="814"/>
        <source>Update App</source>
        <translation>升級應用程序</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="387"/>
        <source>Clear data</source>
        <translation>清除數據</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="396"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="840"/>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2183"/>
        <source>Auto Config</source>
        <translation>自動配置</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="404"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="820"/>
        <source>LCD Display Wizard</source>
        <translation>LCD顯示向導</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="412"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="834"/>
        <source>Start Wizard Now</source>
        <translation>啓動向導</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="432"/>
        <source>System DO</source>
        <translation>系統DO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="441"/>
        <source>IB2 DO</source>
        <translation>IB2 DO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="450"/>
        <source>EIB1 DO</source>
        <translation>EIB1 DO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="459"/>
        <source>EIB2 DO</source>
        <translation>EIB2 DO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="470"/>
        <source>FCUP1 Settings</source>
        <translation>FCUP1參數</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="478"/>
        <source>FCUP2 Settings</source>
        <translation>FCUP2參數</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="486"/>
        <source>FCUP3 Settings</source>
        <translation>FCUP3參數</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="494"/>
        <source>FCUP4 Settings</source>
        <translation>FCUP4參數</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="809"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="815"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="821"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="835"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="841"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="810"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="816"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="822"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="836"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="842"/>
        <source>No</source>
        <translation>否</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="848"/>
        <source>Protocol</source>
        <translation>協議</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="855"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="877"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="860"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="882"/>
        <source>Media</source>
        <translation>媒質</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="867"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="889"/>
        <source>Baudrate</source>
        <translation>波特率</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="917"/>
        <source>Mask</source>
        <translation>子網掩碼</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="935"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="940"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="967"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="972"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="977"/>
        <source>IPV6 IP</source>
        <translation>IPV6 IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="945"/>
        <source>IPV6 Prefix</source>
        <translation>IPV6前綴</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="950"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="955"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="982"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="987"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="992"/>
        <source>IPV6 Gateway</source>
        <translation>IPV6網關</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="960"/>
        <source>IPV6 DHCP</source>
        <translation>IPV6 DHCP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="997"/>
        <source>Clear Data</source>
        <translation>清除數據</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="998"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="522"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="74"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="100"/>
        <source>Alarm History</source>
        <translation>曆史告警</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1996"/>
        <source>OK to restore default</source>
        <translation>恢複默認配置嗎</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2076"/>
        <source>OK to update app</source>
        <translation>確定升級應用程序嗎</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2096"/>
        <source>Without USB drive</source>
        <translation>沒有U盤</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2105"/>
        <source>USB drive is empty</source>
        <translation>U盤是空的</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2114"/>
        <source>Update is not needed</source>
        <translation>不需要升級</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2123"/>
        <source>App program not found</source>
        <translation>App程序不存在</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2138"/>
        <source>Without script file</source>
        <translation>沒有腳本文件</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2155"/>
        <source>OK to clear data</source>
        <translation>確定清除數據嗎</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2182"/>
        <source>Start auto config</source>
        <translation>確認開始自動配置</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2223"/>
        <source>Restore failed</source>
        <translation>恢複失敗</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2DCABranch.cpp" line="134"/>
        <source>Load</source>
        <translation>負載</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2DCDeg1Branch.cpp" line="46"/>
        <location filename="../../g3_lui/equipWidget/Wdg2P6BattDeg.cpp" line="136"/>
        <source>Temp</source>
        <translation>溫度</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2P5BattRemainTime.cpp" line="45"/>
        <source>Batteries</source>
        <translation>電池</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="516"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="67"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="97"/>
        <source>Active Alarms</source>
        <translation>當前告警</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="690"/>
        <source>Observation</source>
        <translation>一般告警</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="694"/>
        <source>Major</source>
        <translation>重要告警</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="698"/>
        <source>Critical</source>
        <translation>緊急告警</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="893"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="933"/>
        <source>Index</source>
        <translation>序號</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="898"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="942"/>
        <source>Iout(A)</source>
        <translation>Iout(A)</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="903"/>
        <source>State</source>
        <translation>狀態</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="938"/>
        <source>Vin(V)</source>
        <translation>Vin(V)</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1262"/>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="138"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1266"/>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="143"/>
        <source>SN</source>
        <translation>SN</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1270"/>
        <source>Number</source>
        <translation>編碼</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1274"/>
        <source>Product Ver</source>
        <translation>産品版本</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1278"/>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="167"/>
        <source>SW Ver</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="25"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="26"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="596"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="597"/>
        <source>OA Alarm</source>
        <translation>一般告警</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="27"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="598"/>
        <source>MA Alarm</source>
        <translation>重要告警</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="28"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="599"/>
        <source>CA Alarm</source>
        <translation>緊急告警</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="58"/>
        <source>Alarm</source>
        <translation>告警</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP0AC.cpp" line="146"/>
        <source>AC Input</source>
        <translation>交流輸入</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="85"/>
        <source>ENT to Inventory</source>
        <translation>ENT進入産品信息</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="152"/>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="173"/>
        <source>HW Ver</source>
        <translation>硬件版本</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="179"/>
        <source>Config Ver</source>
        <translation>配置版本</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="204"/>
        <source>File Sys</source>
        <translation>文件系統</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="210"/>
        <source>MAC Address</source>
        <translation>MAC地址</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="225"/>
        <source>DHCP Server IP</source>
        <translation>DHCP服務器IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="249"/>
        <source>Link-Local Addr</source>
        <translation>連接-本地地址</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="271"/>
        <source>Global Addr</source>
        <translation>IPV6地址</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="293"/>
        <source>DHCP Server IPV6</source>
        <translation>DHCP服務器IPV6</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP12ScreenSaverOut.cpp" line="58"/>
        <source>Sys Used</source>
        <translation>系統使用</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP1Module.cpp" line="85"/>
        <source>Module</source>
        <translation>模塊</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP3DCA.cpp" line="117"/>
        <location filename="../../g3_lui/equipWidget/WdgFP5Deg2Curve.cpp" line="105"/>
        <location filename="../../g3_lui/equipWidget/WdgFP8BattDegCurve.cpp" line="105"/>
        <source>Last 7 days</source>
        <translation>最近7天</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP3DCA.cpp" line="166"/>
        <source>Total Load</source>
        <translation>總負載</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP5Deg2Curve.cpp" line="107"/>
        <source>Ambient Temp</source>
        <translation>環境溫度</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="148"/>
        <source>ENT to OK</source>
        <translation>按ENT確定</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="150"/>
        <source>ESC to Cancel</source>
        <translation>按ESC取消</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="301"/>
        <source>Update OK Num</source>
        <translation>升級成功數</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="310"/>
        <source>Open File Failed</source>
        <translation>打開文件失敗</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="314"/>
        <source>Comm Time-Out</source>
        <translation>通信超時</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="536"/>
        <source>ENT or ESC to exit</source>
        <translation>按ENT或ESC退出</translation>
    </message>
</context>
<context>
    <name>Wdg2P5BattRemainTime</name>
    <message>
        <source>No Data</source>
        <translation type="obsolete">沒有數據</translation>
    </message>
</context>
<context>
    <name>Wdg2Table</name>
    <message>
        <source>Active Alarms</source>
        <translation type="obsolete">當前告警</translation>
    </message>
    <message>
        <source>Alarm History</source>
        <translation type="obsolete">曆史告警</translation>
    </message>
    <message>
        <source>Observation</source>
        <translation type="obsolete">一般告警</translation>
    </message>
    <message>
        <source>Major</source>
        <translation type="obsolete">重要告警</translation>
    </message>
    <message>
        <source>Critical</source>
        <translation type="obsolete">緊急告警</translation>
    </message>
    <message>
        <source>Index</source>
        <translation type="obsolete">序號</translation>
    </message>
    <message>
        <source>Iout(A)</source>
        <translation type="obsolete">Iout(A)</translation>
    </message>
    <message>
        <source>State</source>
        <translation type="obsolete">狀態</translation>
    </message>
    <message>
        <source>Vin(V)</source>
        <translation type="obsolete">Vin(V)</translation>
    </message>
    <message>
        <source>No Data</source>
        <translation type="obsolete">無數據</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">名稱</translation>
    </message>
    <message>
        <source>SN</source>
        <translation type="obsolete">SN</translation>
    </message>
    <message>
        <source>Number</source>
        <translation type="obsolete">編碼</translation>
    </message>
    <message>
        <source>Product Ver</source>
        <translation type="obsolete">産品版本</translation>
    </message>
    <message>
        <source>SW Ver</source>
        <translation type="obsolete">軟體版本</translation>
    </message>
</context>
<context>
    <name>Wdg3Table</name>
    <message>
        <source>OA Alarm</source>
        <translation type="obsolete">一般告警</translation>
    </message>
    <message>
        <source>MA Alarm</source>
        <translation type="obsolete">重要告警</translation>
    </message>
    <message>
        <source>CA Alarm</source>
        <translation type="obsolete">緊急告警</translation>
    </message>
    <message>
        <source>No Data</source>
        <translation type="obsolete">沒有數據</translation>
    </message>
</context>
<context>
    <name>WdgFCfgGroup</name>
    <message>
        <source>OK to restore default</source>
        <translation type="obsolete">恢複默認配置嗎</translation>
    </message>
    <message>
        <source>Restore Default</source>
        <translation type="obsolete">恢複默認配置</translation>
    </message>
    <message>
        <source>OK to update app</source>
        <translation type="obsolete">確定升級應用程序嗎</translation>
    </message>
    <message>
        <source>Without USB drive</source>
        <translation type="obsolete">沒有U盤</translation>
    </message>
    <message>
        <source>USB drive is empty</source>
        <translation type="obsolete">U盤是空的</translation>
    </message>
    <message>
        <source>Update is not needed</source>
        <translation type="obsolete">不需要升級</translation>
    </message>
    <message>
        <source>App program not found</source>
        <translation type="obsolete">App程序不存在</translation>
    </message>
    <message>
        <source>Without script file</source>
        <translation type="obsolete">沒有腳本文件</translation>
    </message>
    <message>
        <source>OK to clear data</source>
        <translation type="obsolete">確定清除數據嗎</translation>
    </message>
    <message>
        <source>Start auto config</source>
        <translation type="obsolete">確認開始自動配置</translation>
    </message>
    <message>
        <source>Auto Config</source>
        <translation type="obsolete">自動配置</translation>
    </message>
    <message>
        <source>Rebooting</source>
        <translation type="obsolete">重啓</translation>
    </message>
    <message>
        <source>Restore failed</source>
        <translation type="obsolete">恢複失敗</translation>
    </message>
</context>
<context>
    <name>WdgFP0AC</name>
    <message>
        <source>AC</source>
        <translation type="obsolete">交流</translation>
    </message>
</context>
<context>
    <name>WdgFP10Inventory</name>
    <message>
        <source>ENT to Inventory</source>
        <translation type="obsolete">ENT進入産品信息</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">名稱</translation>
    </message>
    <message>
        <source>SN</source>
        <translation type="obsolete">SN</translation>
    </message>
    <message>
        <source>IP</source>
        <translation type="obsolete">IP</translation>
    </message>
    <message>
        <source>SW Ver</source>
        <translation type="obsolete">軟件版本</translation>
    </message>
    <message>
        <source>HW Ver</source>
        <translation type="obsolete">硬件版本</translation>
    </message>
    <message>
        <source>Config Ver</source>
        <translation type="obsolete">配置版本</translation>
    </message>
    <message>
        <source>File Sys</source>
        <translation type="obsolete">文件系統</translation>
    </message>
    <message>
        <source>MAC Address</source>
        <translation type="obsolete">MAC地址</translation>
    </message>
    <message>
        <source>DHCP Server IP</source>
        <translation type="obsolete">DHCP服務器IP</translation>
    </message>
    <message>
        <source>Link-Local Addr</source>
        <translation type="obsolete">連接-本地地址</translation>
    </message>
    <message>
        <source>Global Addr</source>
        <translation type="obsolete">IPV6地址</translation>
    </message>
    <message>
        <source>DHCP Server IPV6</source>
        <translation type="obsolete">DHCP服務器IPV6</translation>
    </message>
</context>
<context>
    <name>WdgFP12ScreenSaverOut</name>
    <message>
        <source>Sys Used</source>
        <translation type="obsolete">系統使用</translation>
    </message>
</context>
<context>
    <name>WdgFP1Module</name>
    <message>
        <source>Module</source>
        <translation type="obsolete">模塊</translation>
    </message>
</context>
<context>
    <name>WdgFP3DCA</name>
    <message>
        <source>Last 7 days</source>
        <translation type="obsolete">最近7天</translation>
    </message>
    <message>
        <source>Total Load</source>
        <translation type="obsolete">總負載</translation>
    </message>
</context>
<context>
    <name>WdgFP5Deg2Curve</name>
    <message>
        <source>Last 7 days</source>
        <translation type="obsolete">最近7天</translation>
    </message>
    <message>
        <source>Ambient Temp</source>
        <translation type="obsolete">環境溫度</translation>
    </message>
</context>
<context>
    <name>WdgFP8BattDegCurve</name>
    <message>
        <source>Last 7 days</source>
        <translation type="obsolete">最近7天</translation>
    </message>
    <message>
        <source>Temp Comp</source>
        <translation type="obsolete">溫補溫度</translation>
    </message>
</context>
</TS>
