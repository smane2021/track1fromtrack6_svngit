<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>CMenuData</name>
    <message>
        <source>Settings</source>
        <translation type="obsolete">Configuration</translation>
    </message>
    <message>
        <source>Maintenance</source>
        <translation type="obsolete">Maintenance</translation>
    </message>
    <message>
        <source>Energy Saving</source>
        <translation type="obsolete">MODE ECO</translation>
    </message>
    <message>
        <source>Alarm Settings</source>
        <translation type="obsolete">Config Alarmes</translation>
    </message>
    <message>
        <source>FCUP Settings</source>
        <translation type="obsolete">Config FCUP</translation>
    </message>
    <message>
        <source>FCUP1 Settings</source>
        <translation type="obsolete">Config FCUP1</translation>
    </message>
    <message>
        <source>FCUP2 Settings</source>
        <translation type="obsolete">Config FCUP2</translation>
    </message>
    <message>
        <source>FCUP3 Settings</source>
        <translation type="obsolete">Config FCUP3</translation>
    </message>
    <message>
        <source>FCUP4 Settings</source>
        <translation type="obsolete">Config FCUP4</translation>
    </message>
    <message>
        <source>Rect Settings</source>
        <translation type="obsolete">Config Red</translation>
    </message>
    <message>
        <source>Batt Settings</source>
        <translation type="obsolete">Config Batt</translation>
    </message>
    <message>
        <source>LVD Settings</source>
        <translation type="obsolete">Config LVD</translation>
    </message>
    <message>
        <source>AC Settings</source>
        <translation type="obsolete">Config AC</translation>
    </message>
    <message>
        <source>Sys Settings</source>
        <translation type="obsolete">Config Sys</translation>
    </message>
    <message>
        <source>Comm Settings</source>
        <translation type="obsolete">Config Com</translation>
    </message>
    <message>
        <source>Other Settings</source>
        <translation type="obsolete">Config Autre</translation>
    </message>
    <message>
        <source>Slave Settings</source>
        <translation type="obsolete">Config Esclave</translation>
    </message>
    <message>
        <source>DO Normal Settings</source>
        <translation type="obsolete">DO Réglages normaux</translation>
    </message>
    <message>
        <source>Basic Settings</source>
        <translation type="obsolete">Config Basic</translation>
    </message>
    <message>
        <source>Charge</source>
        <translation type="obsolete">Charge</translation>
    </message>
    <message>
        <source>Battery Test</source>
        <translation type="obsolete">Test Batt</translation>
    </message>
    <message>
        <source>Temp Comp</source>
        <translation type="obsolete">Temp Comp</translation>
    </message>
    <message>
        <source>Batt1 Settings</source>
        <translation type="obsolete">Config Batt1</translation>
    </message>
    <message>
        <source>Batt2 Settings</source>
        <translation type="obsolete">Config Batt2</translation>
    </message>
    <message>
        <source>Restore Default</source>
        <translation type="obsolete">Restauration Defaut</translation>
    </message>
    <message>
        <source>Update App</source>
        <translation type="obsolete">Mise à jour de l&apos;app</translation>
    </message>
    <message>
        <source>Clear data</source>
        <translation type="obsolete">Données Claires</translation>
    </message>
    <message>
        <source>Auto Config</source>
        <translation type="obsolete">Auto config</translation>
    </message>
    <message>
        <source>LCD Display Wizard</source>
        <translation type="obsolete">Wizard sur afficheur LCD</translation>
    </message>
    <message>
        <source>Start Wizard Now</source>
        <translation type="obsolete">Démarrer Wizard maintenant</translation>
    </message>
    <message>
        <source>System DO</source>
        <translation type="obsolete">Sistema DO</translation>
    </message>
    <message>
        <source>IB2 DO</source>
        <translation type="obsolete">IB2 DO</translation>
    </message>
    <message>
        <source>EIB1 DO</source>
        <translation type="obsolete">EIB1 DO</translation>
    </message>
    <message>
        <source>EIB2 DO</source>
        <translation type="obsolete">EIB2 DO</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Oui</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Non</translation>
    </message>
    <message>
        <source>Protocol</source>
        <translation type="obsolete">Protocole</translation>
    </message>
    <message>
        <source>Address</source>
        <translation type="obsolete">Adresse</translation>
    </message>
    <message>
        <source>Media</source>
        <translation type="obsolete">communication</translation>
    </message>
    <message>
        <source>Baudrate</source>
        <translation type="obsolete">Baudrate</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="obsolete">Date</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">Heure</translation>
    </message>
    <message>
        <source>IP Address</source>
        <translation type="obsolete">IP</translation>
    </message>
    <message>
        <source>Mask</source>
        <translation type="obsolete">MASK</translation>
    </message>
    <message>
        <source>Gateway</source>
        <translation type="obsolete">Passerelle</translation>
    </message>
    <message>
        <source>DHCP</source>
        <translation type="obsolete">DHCP</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="obsolete">Invalidé</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation type="obsolete">Validé</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Erreur</translation>
    </message>
    <message>
        <source>IPV6 IP</source>
        <translation type="obsolete">IPV6 IP</translation>
    </message>
    <message>
        <source>IPV6 Prefix</source>
        <translation type="obsolete">Prefixe IPV6</translation>
    </message>
    <message>
        <source>IPV6 Gateway</source>
        <translation type="obsolete">Passerelle IPV6</translation>
    </message>
    <message>
        <source>IPV6 DHCP</source>
        <translation type="obsolete">IPV6 DHCP</translation>
    </message>
    <message>
        <source>Clear Data</source>
        <translation type="obsolete">Données Claires</translation>
    </message>
    <message>
        <source>Alarm History</source>
        <translation type="obsolete">Historique alarmes</translation>
    </message>
</context>
<context>
    <name>CtrlInputChar</name>
    <message>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="291"/>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="304"/>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="317"/>
        <source>.</source>
        <translation>.</translation>
    </message>
</context>
<context>
    <name>DlgInfo</name>
    <message>
        <source>ENT to OK</source>
        <translation type="obsolete">ENT pour OK</translation>
    </message>
    <message>
        <source>ESC to Cancel</source>
        <translation type="obsolete">ESC pour Annuler</translation>
    </message>
    <message>
        <source>Update OK Num</source>
        <translation type="obsolete">Nombre M a J OK</translation>
    </message>
    <message>
        <source>Open File Failed</source>
        <translation type="obsolete">Fichier ouvert HS</translation>
    </message>
    <message>
        <source>Comm Time-Out</source>
        <translation type="obsolete">Tempo COM OUT</translation>
    </message>
    <message>
        <source>ENT or ESC to exit</source>
        <translation type="obsolete">ENT ou ESC pour Quitter</translation>
    </message>
    <message>
        <source>Rebooting</source>
        <translation type="obsolete">Redémarrer</translation>
    </message>
</context>
<context>
    <name>DlgUpdateApp</name>
    <message>
        <location filename="../../g3_lui/util/DlgUpdateApp.cpp" line="44"/>
        <source>Qt Exited</source>
        <translation>Quitter QT</translation>
    </message>
</context>
<context>
    <name>FirstStackedWdg</name>
    <message>
        <source>Alarm</source>
        <translation type="obsolete">Alarme</translation>
    </message>
    <message>
        <source>Active Alarms</source>
        <translation type="obsolete">Alarmes actives</translation>
    </message>
    <message>
        <source>Alarm History</source>
        <translation type="obsolete">Histor Alarmes</translation>
    </message>
</context>
<context>
    <name>GuideWindow</name>
    <message>
        <source>Installation Wizard</source>
        <translation type="obsolete">Installation Wizard</translation>
    </message>
    <message>
        <source>ENT to continue</source>
        <translation type="obsolete">ENT pour Continuer</translation>
    </message>
    <message>
        <source>ESC to skip </source>
        <translation type="obsolete">ESC pour Passer</translation>
    </message>
    <message>
        <source>OK to exit</source>
        <translation type="obsolete">OK pour Sortir</translation>
    </message>
    <message>
        <source>ESC to exit</source>
        <translation type="obsolete">ESC pour Quitter</translation>
    </message>
    <message>
        <source>Wizard finished</source>
        <translation type="obsolete">Wizard terminé</translation>
    </message>
    <message>
        <source>Site Name</source>
        <translation type="obsolete">Nom du site</translation>
    </message>
    <message>
        <source>Battery Settings</source>
        <translation type="obsolete">Config Batt</translation>
    </message>
    <message>
        <source>Capacity Settings</source>
        <translation type="obsolete">Config capacite</translation>
    </message>
    <message>
        <source>ECO Parameter</source>
        <translation type="obsolete">Parametres ECO</translation>
    </message>
    <message>
        <source>Alarm Settings</source>
        <translation type="obsolete">Config Alarmes</translation>
    </message>
    <message>
        <source>Common Settings</source>
        <translation type="obsolete">Config de base</translation>
    </message>
    <message>
        <source>IP address</source>
        <translation type="obsolete">IP</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="obsolete">Date</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">Heure</translation>
    </message>
    <message>
        <source>DHCP</source>
        <translation type="obsolete">DHCP</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="obsolete">Invalidé</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation type="obsolete">Validé</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Erreur</translation>
    </message>
    <message>
        <source>IP Address</source>
        <translation type="obsolete">IP</translation>
    </message>
    <message>
        <source>MASK</source>
        <translation type="obsolete">MASK</translation>
    </message>
    <message>
        <source>Gateway</source>
        <translation type="obsolete">Passerelle</translation>
    </message>
    <message>
        <source>Rebooting</source>
        <translation type="obsolete">Redémarrer</translation>
    </message>
    <message>
        <source>Set language failed</source>
        <translation type="obsolete">Echec de config de la langue</translation>
    </message>
    <message>
        <source>Adjust LCD</source>
        <translation type="obsolete">Regler LCD</translation>
    </message>
</context>
<context>
    <name>LoginWindow</name>
    <message>
        <source>Select User</source>
        <translation type="obsolete">Select un utilisateur</translation>
    </message>
    <message>
        <source>Enter Password</source>
        <translation type="obsolete">Entrer mot de passe</translation>
    </message>
    <message>
        <source>Password error</source>
        <translation type="obsolete">Erreur de mot de passe</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>OK to reboot</source>
        <translation type="obsolete">OK pour reboot</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="347"/>
        <source>OK to clear</source>
        <translation>OK pour init</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="348"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="402"/>
        <source>Please wait</source>
        <translation>Attendre SVP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="349"/>
        <source>Set successful</source>
        <translation>Config OK</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="54"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="350"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="614"/>
        <source>Set failed</source>
        <translation>Config HS</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="364"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="375"/>
        <source>OK to change</source>
        <translation>OK pour modif</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="401"/>
        <source>OK to update</source>
        <translation>OK pour M a J</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="403"/>
        <source>Update successful</source>
        <translation>Mise à jour succès</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="404"/>
        <source>Update failed</source>
        <translation>M a J HS</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="289"/>
        <source>Installation Wizard</source>
        <translation>Installation Wizard</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="293"/>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="352"/>
        <source>ENT to continue</source>
        <translation>ENT pour Continuer</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="297"/>
        <source>ESC to skip </source>
        <translation>ESC pour Passer</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="344"/>
        <source>OK to exit</source>
        <translation>OK pour Sortir</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="348"/>
        <source>ESC to exit</source>
        <translation>ESC pour Quitter</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="366"/>
        <source>Wizard finished</source>
        <translation>Wizard terminé</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="518"/>
        <source>Site Name</source>
        <translation>Nom du site</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="522"/>
        <source>Battery Settings</source>
        <translation>Config Batt</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="526"/>
        <source>Capacity Settings</source>
        <translation>Config capacite</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="530"/>
        <source>ECO Parameter</source>
        <translation>Parametres ECO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="534"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="229"/>
        <source>Alarm Settings</source>
        <translation>Config Alarmes</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="538"/>
        <source>Common Settings</source>
        <translation>Config de base</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="542"/>
        <source>IP address</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="560"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="901"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="575"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="906"/>
        <source>Time</source>
        <translation>Heure</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="603"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="927"/>
        <source>DHCP</source>
        <translation>DHCP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="610"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="928"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="961"/>
        <source>Disabled</source>
        <translation>Invalidé</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="611"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="929"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="962"/>
        <source>Enabled</source>
        <translation>Validé</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="612"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="930"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="963"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="632"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="912"/>
        <source>IP Address</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="636"/>
        <source>MASK</source>
        <translation>MASK</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="640"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="922"/>
        <source>Gateway</source>
        <translation>Passerelle</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="864"/>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2222"/>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="570"/>
        <source>Rebooting</source>
        <translation>Redémarrer</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="1324"/>
        <source>Set language failed</source>
        <translation>Echec de config de la langue</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="1355"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="536"/>
        <source>Adjust LCD</source>
        <translation>Regler LCD</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="853"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="1464"/>
        <location filename="../../g3_lui/equipWidget/Wdg2DCABranch.cpp" line="340"/>
        <location filename="../../g3_lui/equipWidget/Wdg2P5BattRemainTime.cpp" line="263"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1167"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="502"/>
        <source>No Data</source>
        <translation>Pas de donnée</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="132"/>
        <source>Select User</source>
        <translation>Select un utilisateur</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="135"/>
        <source>Enter Password</source>
        <translation>Entrer mot de passe</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="349"/>
        <source>Password error</source>
        <translation>Erreur de mot de passe</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/mainwindow.cpp" line="422"/>
        <source>OK to reboot</source>
        <translation>OK pour reboot</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="203"/>
        <source>Settings</source>
        <translation>Configuration</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="213"/>
        <source>Maintenance</source>
        <translation>Maintenance</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="221"/>
        <source>Energy Saving</source>
        <translation>MODE ECO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="237"/>
        <source>Rect Settings</source>
        <translation>Config Red</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="245"/>
        <source>Batt Settings</source>
        <translation>Config Batt</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="253"/>
        <source>LVD Settings</source>
        <translation>Config LVD</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="261"/>
        <source>AC Settings</source>
        <translation>Config AC</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="269"/>
        <source>Sys Settings</source>
        <translation>Config Sys</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="277"/>
        <source>Comm Settings</source>
        <translation>Config Com</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="285"/>
        <source>Other Settings</source>
        <translation>Config Autre</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="294"/>
        <source>Slave Settings</source>
        <translation>Config Esclave</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="302"/>
        <source>FCUP Settings</source>
        <translation>Config FCUP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="310"/>
        <source>DO Normal Settings</source>
        <translation>DO Réglages normaux</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="322"/>
        <source>Basic Settings</source>
        <translation>Config Basic</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="330"/>
        <source>Charge</source>
        <translation>Charge</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="338"/>
        <source>Battery Test</source>
        <translation>Test Batt</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="346"/>
        <location filename="../../g3_lui/equipWidget/WdgFP8BattDegCurve.cpp" line="106"/>
        <source>Temp Comp</source>
        <translation>Temp Comp</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="354"/>
        <source>Batt1 Settings</source>
        <translation>Config Batt1</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="362"/>
        <source>Batt2 Settings</source>
        <translation>Config Batt2</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="371"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="808"/>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1997"/>
        <source>Restore Default</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="379"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="421"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="814"/>
        <source>Update App</source>
        <translation>Mise à jour de l&apos;app</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="387"/>
        <source>Clear data</source>
        <translation>Données Claires</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="396"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="840"/>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2183"/>
        <source>Auto Config</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="404"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="820"/>
        <source>LCD Display Wizard</source>
        <translation>Wizard sur afficheur LCD</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="412"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="834"/>
        <source>Start Wizard Now</source>
        <translation>Démarrer Wizard maintenant</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="432"/>
        <source>System DO</source>
        <translation>Sistema DO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="441"/>
        <source>IB2 DO</source>
        <translation>IB2 DO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="450"/>
        <source>EIB1 DO</source>
        <translation>EIB1 DO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="459"/>
        <source>EIB2 DO</source>
        <translation>EIB2 DO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="470"/>
        <source>FCUP1 Settings</source>
        <translation>Config FCUP1</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="478"/>
        <source>FCUP2 Settings</source>
        <translation>Config FCUP2</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="486"/>
        <source>FCUP3 Settings</source>
        <translation>Config FCUP3</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="494"/>
        <source>FCUP4 Settings</source>
        <translation>Config FCUP4</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="809"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="815"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="821"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="835"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="841"/>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="810"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="816"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="822"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="836"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="842"/>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="848"/>
        <source>Protocol</source>
        <translation>Protocole</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="855"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="877"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="860"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="882"/>
        <source>Media</source>
        <translation>communication</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="867"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="889"/>
        <source>Baudrate</source>
        <translation>Baudrate</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="917"/>
        <source>Mask</source>
        <translation>MASK</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="935"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="940"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="967"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="972"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="977"/>
        <source>IPV6 IP</source>
        <translation>IPV6 IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="945"/>
        <source>IPV6 Prefix</source>
        <translation>Prefixe IPV6</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="950"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="955"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="982"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="987"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="992"/>
        <source>IPV6 Gateway</source>
        <translation>Passerelle IPV6</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="960"/>
        <source>IPV6 DHCP</source>
        <translation>IPV6 DHCP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="997"/>
        <source>Clear Data</source>
        <translation>Données Claires</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="998"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="522"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="74"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="100"/>
        <source>Alarm History</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1996"/>
        <source>OK to restore default</source>
        <translation>OK pour restauration</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2076"/>
        <source>OK to update app</source>
        <translation>OK pour M a J Appl</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2096"/>
        <source>Without USB drive</source>
        <translation>Sans Cle USB</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2105"/>
        <source>USB drive is empty</source>
        <translation>Cle USB vide</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2114"/>
        <source>Update is not needed</source>
        <translation>M a J inutile</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2123"/>
        <source>App program not found</source>
        <translation>App introuvable</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2138"/>
        <source>Without script file</source>
        <translation>Pas de fichier script</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2155"/>
        <source>OK to clear data</source>
        <translation>OK pour effacer les données</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2182"/>
        <source>Start auto config</source>
        <translation>Lancer auto config</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2223"/>
        <source>Restore failed</source>
        <translation>Defaut restauration</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2DCABranch.cpp" line="134"/>
        <source>Load</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2DCDeg1Branch.cpp" line="46"/>
        <location filename="../../g3_lui/equipWidget/Wdg2P6BattDeg.cpp" line="136"/>
        <source>Temp</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2P5BattRemainTime.cpp" line="45"/>
        <source>Batteries</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="516"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="67"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="97"/>
        <source>Active Alarms</source>
        <translation>Alarmes actives</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="690"/>
        <source>Observation</source>
        <translation>Observation</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="694"/>
        <source>Major</source>
        <translation>Majeure</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="698"/>
        <source>Critical</source>
        <translation>Critique</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="893"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="933"/>
        <source>Index</source>
        <translation>Num</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="898"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="942"/>
        <source>Iout(A)</source>
        <translation>Iout(A)</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="903"/>
        <source>State</source>
        <translation>Etat</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="938"/>
        <source>Vin(V)</source>
        <translation>Vin(V)</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1262"/>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="138"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1266"/>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="143"/>
        <source>SN</source>
        <translation>SN</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1270"/>
        <source>Number</source>
        <translation>Num</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1274"/>
        <source>Product Ver</source>
        <translation>Ver Mater</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1278"/>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="167"/>
        <source>SW Ver</source>
        <translation>Ver Logic.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="25"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="26"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="596"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="597"/>
        <source>OA Alarm</source>
        <translation>OA Alarme</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="27"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="598"/>
        <source>MA Alarm</source>
        <translation>MA Alarme</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="28"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="599"/>
        <source>CA Alarm</source>
        <translation>CA Alarme</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="58"/>
        <source>Alarm</source>
        <translation>Alarme</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP0AC.cpp" line="146"/>
        <source>AC Input</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="85"/>
        <source>ENT to Inventory</source>
        <translation>ENT pour Inventaire</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="152"/>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="173"/>
        <source>HW Ver</source>
        <translation>Ver Mater.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="179"/>
        <source>Config Ver</source>
        <translation>Ver.Config.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="204"/>
        <source>File Sys</source>
        <translation>Fichier Sys.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="210"/>
        <source>MAC Address</source>
        <translation>MAC Adresse</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="225"/>
        <source>DHCP Server IP</source>
        <translation>IP serveur DHCP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="249"/>
        <source>Link-Local Addr</source>
        <translation>Link-Local Adr</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="271"/>
        <source>Global Addr</source>
        <translation>Global Adr</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="293"/>
        <source>DHCP Server IPV6</source>
        <translation>DHCP Serveur IPV6</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP12ScreenSaverOut.cpp" line="58"/>
        <source>Sys Used</source>
        <translation>Sys utilise</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP1Module.cpp" line="85"/>
        <source>Module</source>
        <translation>Module</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP3DCA.cpp" line="117"/>
        <location filename="../../g3_lui/equipWidget/WdgFP5Deg2Curve.cpp" line="105"/>
        <location filename="../../g3_lui/equipWidget/WdgFP8BattDegCurve.cpp" line="105"/>
        <source>Last 7 days</source>
        <translation>7 derniers jours</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP3DCA.cpp" line="166"/>
        <source>Total Load</source>
        <translation>Charge totale</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP5Deg2Curve.cpp" line="107"/>
        <source>Ambient Temp</source>
        <translation>Temp Amb</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="148"/>
        <source>ENT to OK</source>
        <translation>ENT pour OK</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="150"/>
        <source>ESC to Cancel</source>
        <translation>ESC pour Annuler</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="301"/>
        <source>Update OK Num</source>
        <translation>Nombre M a J OK</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="310"/>
        <source>Open File Failed</source>
        <translation>Fichier ouvert HS</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="314"/>
        <source>Comm Time-Out</source>
        <translation>Tempo COM OUT</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="536"/>
        <source>ENT or ESC to exit</source>
        <translation>ENT ou ESC pour Quitter</translation>
    </message>
</context>
<context>
    <name>Wdg2P5BattRemainTime</name>
    <message>
        <source>No Data</source>
        <translation type="obsolete">Pas de donnée</translation>
    </message>
</context>
<context>
    <name>Wdg2Table</name>
    <message>
        <source>Active Alarms</source>
        <translation type="obsolete">Alarmes actives</translation>
    </message>
    <message>
        <source>Alarm History</source>
        <translation type="obsolete">Histori Alarmes</translation>
    </message>
    <message>
        <source>Observation</source>
        <translation type="obsolete">Observation</translation>
    </message>
    <message>
        <source>Major</source>
        <translation type="obsolete">Majeure</translation>
    </message>
    <message>
        <source>Critical</source>
        <translation type="obsolete">Critique</translation>
    </message>
    <message>
        <source>Index</source>
        <translation type="obsolete">Num</translation>
    </message>
    <message>
        <source>Iout(A)</source>
        <translation type="obsolete">Iout(A)</translation>
    </message>
    <message>
        <source>State</source>
        <translation type="obsolete">Etat</translation>
    </message>
    <message>
        <source>Vin(V)</source>
        <translation type="obsolete">Vin(V)</translation>
    </message>
    <message>
        <source>No Data</source>
        <translation type="obsolete">Pas de donnée</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">Nom</translation>
    </message>
    <message>
        <source>SN</source>
        <translation type="obsolete">SN</translation>
    </message>
    <message>
        <source>Number</source>
        <translation type="obsolete">Num</translation>
    </message>
    <message>
        <source>Product Ver</source>
        <translation type="obsolete">Ver Mater</translation>
    </message>
    <message>
        <source>SW Ver</source>
        <translation type="obsolete">Ver Logic.</translation>
    </message>
</context>
<context>
    <name>Wdg3Table</name>
    <message>
        <source>OA Alarm</source>
        <translation type="obsolete">OA Alarme</translation>
    </message>
    <message>
        <source>MA Alarm</source>
        <translation type="obsolete">MA Alarme</translation>
    </message>
    <message>
        <source>CA Alarm</source>
        <translation type="obsolete">CA Alarme</translation>
    </message>
    <message>
        <source>No Data</source>
        <translation type="obsolete">Pas de donnée</translation>
    </message>
</context>
<context>
    <name>WdgFCfgGroup</name>
    <message>
        <source>OK to restore default</source>
        <translation type="obsolete">OK pour restauration</translation>
    </message>
    <message>
        <source>Restore Default</source>
        <translation type="obsolete">Restaurer configuration par defaut</translation>
    </message>
    <message>
        <source>OK to update app</source>
        <translation type="obsolete">OK pour M a J Appl</translation>
    </message>
    <message>
        <source>Without USB drive</source>
        <translation type="obsolete">Sans Cle USB</translation>
    </message>
    <message>
        <source>USB drive is empty</source>
        <translation type="obsolete">Cle USB vide</translation>
    </message>
    <message>
        <source>Update is not needed</source>
        <translation type="obsolete">M a J inutile</translation>
    </message>
    <message>
        <source>App program not found</source>
        <translation type="obsolete">App introuvable</translation>
    </message>
    <message>
        <source>Without script file</source>
        <translation type="obsolete">Pas de fichier script</translation>
    </message>
    <message>
        <source>OK to clear data</source>
        <translation type="obsolete">OK pour effacer les données</translation>
    </message>
    <message>
        <source>Start auto config</source>
        <translation type="obsolete">Lancer auto config</translation>
    </message>
    <message>
        <source>Auto Config</source>
        <translation type="obsolete">Auto-config</translation>
    </message>
    <message>
        <source>Rebooting</source>
        <translation type="obsolete">Redémarrer</translation>
    </message>
    <message>
        <source>Restore failed</source>
        <translation type="obsolete">Defaut restauration</translation>
    </message>
</context>
<context>
    <name>WdgFP0AC</name>
    <message>
        <source>AC</source>
        <translation type="obsolete">AC</translation>
    </message>
</context>
<context>
    <name>WdgFP10Inventory</name>
    <message>
        <source>ENT to Inventory</source>
        <translation type="obsolete">ENT pour Inventaire</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">Nom</translation>
    </message>
    <message>
        <source>SN</source>
        <translation type="obsolete">SN</translation>
    </message>
    <message>
        <source>IP</source>
        <translation type="obsolete">IP</translation>
    </message>
    <message>
        <source>SW Ver</source>
        <translation type="obsolete">Ver Logic.</translation>
    </message>
    <message>
        <source>HW Ver</source>
        <translation type="obsolete">Ver Mater.</translation>
    </message>
    <message>
        <source>Config Ver</source>
        <translation type="obsolete">Ver.Config.</translation>
    </message>
    <message>
        <source>File Sys</source>
        <translation type="obsolete">Fichier Sys.</translation>
    </message>
    <message>
        <source>MAC Address</source>
        <translation type="obsolete">MAC Adresse</translation>
    </message>
    <message>
        <source>DHCP Server IP</source>
        <translation type="obsolete">IP serveur DHCP</translation>
    </message>
    <message>
        <source>Link-Local Addr</source>
        <translation type="obsolete">Link-Local Adr</translation>
    </message>
    <message>
        <source>Global Addr</source>
        <translation type="obsolete">Global Adr</translation>
    </message>
    <message>
        <source>DHCP Server IPV6</source>
        <translation type="obsolete">DHCP Serveur IPV6</translation>
    </message>
</context>
<context>
    <name>WdgFP12ScreenSaverOut</name>
    <message>
        <source>Sys Used</source>
        <translation type="obsolete">Sys utilise</translation>
    </message>
</context>
<context>
    <name>WdgFP1Module</name>
    <message>
        <source>Module</source>
        <translation type="obsolete">Module</translation>
    </message>
</context>
<context>
    <name>WdgFP3DCA</name>
    <message>
        <source>Last 7 days</source>
        <translation type="obsolete">7 derniers jours</translation>
    </message>
    <message>
        <source>Total Load</source>
        <translation type="obsolete">Charge totale</translation>
    </message>
</context>
<context>
    <name>WdgFP5Deg2Curve</name>
    <message>
        <source>Last 7 days</source>
        <translation type="obsolete">7 derniers jours</translation>
    </message>
    <message>
        <source>Ambient Temp</source>
        <translation type="obsolete">Temp Amb</translation>
    </message>
</context>
<context>
    <name>WdgFP8BattDegCurve</name>
    <message>
        <source>Last 7 days</source>
        <translation type="obsolete">7 derniers jours</translation>
    </message>
    <message>
        <source>Temp Comp</source>
        <translation type="obsolete">Temp Comp</translation>
    </message>
</context>
</TS>
