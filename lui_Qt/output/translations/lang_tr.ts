<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="zh_CN">
<context>
    <name>CMenuData</name>
    <message>
        <source>Settings</source>
        <translation type="obsolete">Ayarlar</translation>
    </message>
    <message>
        <source>Maintenance</source>
        <translation type="obsolete">Bakim</translation>
    </message>
    <message>
        <source>Energy Saving</source>
        <translation type="obsolete">Enerji Tasarrufu</translation>
    </message>
    <message>
        <source>Alarm Settings</source>
        <translation type="obsolete">Alarm Ayarlari</translation>
    </message>
    <message>
        <source>Rect Settings</source>
        <translation type="obsolete">Dogrultucu Ayarlari</translation>
    </message>
    <message>
        <source>Batt Settings</source>
        <translation type="obsolete">Aku Ayarlari</translation>
    </message>
    <message>
        <source>LVD Settings</source>
        <translation type="obsolete">LVD Ayarlari</translation>
    </message>
    <message>
        <source>AC Settings</source>
        <translation type="obsolete">AC Ayarlari</translation>
    </message>
    <message>
        <source>Sys Settings</source>
        <translation type="obsolete">Sistem Ayarlari</translation>
    </message>
    <message>
        <source>Comm Settings</source>
        <translation type="obsolete">Haberlesme Ayarlari</translation>
    </message>
    <message>
        <source>Other Settings</source>
        <translation type="obsolete">Diger Ayarlari</translation>
    </message>
    <message>
        <source>Slave Settings</source>
        <translation type="obsolete">Ikincil Ayarlari</translation>
    </message>
    <message>
        <source>FCUP Settings</source>
        <translation type="obsolete">Fan Ayarlari</translation>
    </message>
    <message>
        <source>DO Normal Settings</source>
        <translation type="obsolete">DO Configurações normais</translation>
    </message>
    <message>
        <source>Basic Settings</source>
        <translation type="obsolete">Temel Ayarlari</translation>
    </message>
    <message>
        <source>Charge</source>
        <translation type="obsolete">Sarj</translation>
    </message>
    <message>
        <source>Battery Test</source>
        <translation type="obsolete">Aku Test</translation>
    </message>
    <message>
        <source>Temp Comp</source>
        <translation type="obsolete">Sicaklik Komp</translation>
    </message>
    <message>
        <source>Batt1 Settings</source>
        <translation type="obsolete">Aku 1 Ayarlari</translation>
    </message>
    <message>
        <source>Batt2 Settings</source>
        <translation type="obsolete">Aku 2 Ayarlari</translation>
    </message>
    <message>
        <source>Restore Default</source>
        <translation type="obsolete">Vars.Geriyukle</translation>
    </message>
    <message>
        <source>Update App</source>
        <translation type="obsolete">Uygulama Günce.</translation>
    </message>
    <message>
        <source>Clear data</source>
        <translation type="obsolete">Verileri temizle</translation>
    </message>
    <message>
        <source>Auto Config</source>
        <translation type="obsolete">Otomatic Yapil.</translation>
    </message>
    <message>
        <source>LCD Display Wizard</source>
        <translation type="obsolete">LCD Ekran Sih.</translation>
    </message>
    <message>
        <source>Start Wizard Now</source>
        <translation type="obsolete">Sih.Simdi Baslatin</translation>
    </message>
    <message>
        <source>System DO</source>
        <translation type="obsolete">Sistem DO</translation>
    </message>
    <message>
        <source>IB2 DO</source>
        <translation type="obsolete">IB2 DO</translation>
    </message>
    <message>
        <source>EIB1 DO</source>
        <translation type="obsolete">EIB1 DO</translation>
    </message>
    <message>
        <source>EIB2 DO</source>
        <translation type="obsolete">EIB2 DO</translation>
    </message>
    <message>
        <source>FCUP1 Settings</source>
        <translation type="obsolete">FCUP1 Ayarlari</translation>
    </message>
    <message>
        <source>FCUP2 Settings</source>
        <translation type="obsolete">FCUP2 Ayarlari</translation>
    </message>
    <message>
        <source>FCUP3 Settings</source>
        <translation type="obsolete">FCUP3 Ayarlari</translation>
    </message>
    <message>
        <source>FCUP4 Settings</source>
        <translation type="obsolete">FCUP4 Ayarlari</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Evet</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Hayir</translation>
    </message>
    <message>
        <source>Protocol</source>
        <translation type="obsolete">Protokol</translation>
    </message>
    <message>
        <source>Address</source>
        <translation type="obsolete">Adres</translation>
    </message>
    <message>
        <source>Media</source>
        <translation type="obsolete">Medya</translation>
    </message>
    <message>
        <source>Baudrate</source>
        <translation type="obsolete">Baudrate</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="obsolete">Tarih</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">Zaman</translation>
    </message>
    <message>
        <source>IP Address</source>
        <translation type="obsolete">IP Adres</translation>
    </message>
    <message>
        <source>Mask</source>
        <translation type="obsolete">Alt Ag Maskesi</translation>
    </message>
    <message>
        <source>Gateway</source>
        <translation type="obsolete">Ag Gecidi</translation>
    </message>
    <message>
        <source>DHCP</source>
        <translation type="obsolete">DHCP</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="obsolete">Devre Disi</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation type="obsolete">Etkin</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Hata</translation>
    </message>
    <message>
        <source>IPV6 IP</source>
        <translation type="obsolete">IPV6 IP</translation>
    </message>
    <message>
        <source>IPV6 Prefix</source>
        <translation type="obsolete">IPV6 On ek</translation>
    </message>
    <message>
        <source>IPV6 Gateway</source>
        <translation type="obsolete">IPV6 Ag Gecidi</translation>
    </message>
    <message>
        <source>IPV6 DHCP</source>
        <translation type="obsolete">IPV6 DHCP</translation>
    </message>
    <message>
        <source>Clear Data</source>
        <translation type="obsolete">Verileri temizle</translation>
    </message>
    <message>
        <source>Alarm History</source>
        <translation type="obsolete">Alarm Gecmisi</translation>
    </message>
</context>
<context>
    <name>CtrlInputChar</name>
    <message>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="291"/>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="304"/>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="317"/>
        <source>.</source>
        <translation>.</translation>
    </message>
</context>
<context>
    <name>DlgInfo</name>
    <message>
        <source>ENT to OK</source>
        <translation type="obsolete">OK icin ENT </translation>
    </message>
    <message>
        <source>ESC to Cancel</source>
        <translation type="obsolete">Iptal icin ESC</translation>
    </message>
    <message>
        <source>Update OK Num</source>
        <translation type="obsolete">Güncelleme OK</translation>
    </message>
    <message>
        <source>Open File Failed</source>
        <translation type="obsolete">Dosya Acma Basarisiz</translation>
    </message>
    <message>
        <source>Comm Time-Out</source>
        <translation type="obsolete">Haberlesme Zaman Asimi</translation>
    </message>
    <message>
        <source>ENT or ESC to exit</source>
        <translation type="obsolete">Cikis icin ENT yada ESC</translation>
    </message>
    <message>
        <source>Rebooting</source>
        <translation type="obsolete">Y.Baslatiliyor</translation>
    </message>
</context>
<context>
    <name>DlgUpdateApp</name>
    <message>
        <location filename="../../g3_lui/util/DlgUpdateApp.cpp" line="44"/>
        <source>Qt Exited</source>
        <translation>Qt Cikildi</translation>
    </message>
</context>
<context>
    <name>FirstStackedWdg</name>
    <message>
        <source>Alarm</source>
        <translation type="obsolete">Alarm</translation>
    </message>
    <message>
        <source>Active Alarms</source>
        <translation type="obsolete">Aktif Alarm</translation>
    </message>
    <message>
        <source>Alarm History</source>
        <translation type="obsolete">Alarm Gecmisi</translation>
    </message>
</context>
<context>
    <name>GuideWindow</name>
    <message>
        <source>Installation Wizard</source>
        <translation type="obsolete">Yukleme Sihirbazi</translation>
    </message>
    <message>
        <source>ENT to continue</source>
        <translation type="obsolete">Devam icin ENT</translation>
    </message>
    <message>
        <source>ESC to skip </source>
        <translation type="obsolete">Atlamak icin ESC</translation>
    </message>
    <message>
        <source>OK to exit</source>
        <translation type="obsolete">Cikis icin OK</translation>
    </message>
    <message>
        <source>ESC to exit</source>
        <translation type="obsolete">Cikis icin ESC</translation>
    </message>
    <message>
        <source>Wizard finished</source>
        <translation type="obsolete">Sihirbaz Bitmis</translation>
    </message>
    <message>
        <source>Site Name</source>
        <translation type="obsolete">Yer Adi</translation>
    </message>
    <message>
        <source>Battery Settings</source>
        <translation type="obsolete">Aku Ayarlari</translation>
    </message>
    <message>
        <source>Capacity Settings</source>
        <translation type="obsolete">Kapasite Ayarlari</translation>
    </message>
    <message>
        <source>ECO Parameter</source>
        <translation type="obsolete">ECO Parametre</translation>
    </message>
    <message>
        <source>Alarm Settings</source>
        <translation type="obsolete">Alarm Ayarlari</translation>
    </message>
    <message>
        <source>Common Settings</source>
        <translation type="obsolete">Ortak Ayarlari</translation>
    </message>
    <message>
        <source>IP address</source>
        <translation type="obsolete">IP Adres</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="obsolete">Tarih</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">Zaman</translation>
    </message>
    <message>
        <source>DHCP</source>
        <translation type="obsolete">DHCP</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="obsolete">Devre Disi</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation type="obsolete">Etkin</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Hata</translation>
    </message>
    <message>
        <source>IP Address</source>
        <translation type="obsolete">IP Adres</translation>
    </message>
    <message>
        <source>MASK</source>
        <translation type="obsolete">ALT AG MASKESI</translation>
    </message>
    <message>
        <source>Gateway</source>
        <translation type="obsolete">Ag Gecidi</translation>
    </message>
    <message>
        <source>Rebooting</source>
        <translation type="obsolete">Y.Baslatiliyor</translation>
    </message>
    <message>
        <source>Set language failed</source>
        <translation type="obsolete">Ayarlanan Dil Basarisiz</translation>
    </message>
    <message>
        <source>Adjust LCD</source>
        <translation type="obsolete">LCD&apos;yi Ayarlayin</translation>
    </message>
</context>
<context>
    <name>LoginWindow</name>
    <message>
        <source>Select User</source>
        <translation type="obsolete">Kullanici Sec</translation>
    </message>
    <message>
        <source>Enter Password</source>
        <translation type="obsolete">Sifre Gir</translation>
    </message>
    <message>
        <source>Password error</source>
        <translation type="obsolete">Sifre Hatasi</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>OK to reboot</source>
        <translation type="obsolete">Y.Baslatmak icin OK</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="347"/>
        <source>OK to clear</source>
        <translation>Temizlemek icin OK</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="348"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="402"/>
        <source>Please wait</source>
        <translation>Lütfen Bekleyiniz</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="349"/>
        <source>Set successful</source>
        <translation>Ayarlama Basarili</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="54"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="350"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="614"/>
        <source>Set failed</source>
        <translation>Ayarlama Basarisiz</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="364"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="375"/>
        <source>OK to change</source>
        <translation>Degistirmek icin OK</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="401"/>
        <source>OK to update</source>
        <translation>Güncellemek icin OK</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="403"/>
        <source>Update successful</source>
        <translation>Güncelleme Basarili</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="404"/>
        <source>Update failed</source>
        <translation>Güncelleme Basarisiz</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="289"/>
        <source>Installation Wizard</source>
        <translation>Yukleme Sihirbazi</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="293"/>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="352"/>
        <source>ENT to continue</source>
        <translation>Devam icin ENT</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="297"/>
        <source>ESC to skip </source>
        <translation>Atlamak icin ESC</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="344"/>
        <source>OK to exit</source>
        <translation>Cikis icin OK</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="348"/>
        <source>ESC to exit</source>
        <translation>Cikis icin ESC</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="366"/>
        <source>Wizard finished</source>
        <translation>Sihirbaz Bitmis</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="518"/>
        <source>Site Name</source>
        <translation>Yer Adi</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="522"/>
        <source>Battery Settings</source>
        <translation>Aku Ayarlari</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="526"/>
        <source>Capacity Settings</source>
        <translation>Kapasite Ayarlari</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="530"/>
        <source>ECO Parameter</source>
        <translation>ECO Parametre</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="534"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="229"/>
        <source>Alarm Settings</source>
        <translation>Alarm Ayarlari</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="538"/>
        <source>Common Settings</source>
        <translation>Ortak Ayarlari</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="542"/>
        <source>IP address</source>
        <translation>IP Adres</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="560"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="901"/>
        <source>Date</source>
        <translation>Tarih</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="575"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="906"/>
        <source>Time</source>
        <translation>Zaman</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="603"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="927"/>
        <source>DHCP</source>
        <translation>DHCP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="610"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="928"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="961"/>
        <source>Disabled</source>
        <translation>Devre Disi</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="611"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="929"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="962"/>
        <source>Enabled</source>
        <translation>Etkin</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="612"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="930"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="963"/>
        <source>Error</source>
        <translation>Hata</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="632"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="912"/>
        <source>IP Address</source>
        <translation>IP Adres</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="636"/>
        <source>MASK</source>
        <translation>ALT AG MASKESI</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="640"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="922"/>
        <source>Gateway</source>
        <translation>Ag Gecidi</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="864"/>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2222"/>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="570"/>
        <source>Rebooting</source>
        <translation>Yeniden</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="1324"/>
        <source>Set language failed</source>
        <translation>Ayarlanan Dil Basarisiz</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="1355"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="536"/>
        <source>Adjust LCD</source>
        <translation>LCD&apos;yi Ayarlayin</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="853"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="1464"/>
        <location filename="../../g3_lui/equipWidget/Wdg2DCABranch.cpp" line="340"/>
        <location filename="../../g3_lui/equipWidget/Wdg2P5BattRemainTime.cpp" line="263"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1167"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="502"/>
        <source>No Data</source>
        <translation>Veri yok</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="132"/>
        <source>Select User</source>
        <translation>Kullanici Sec</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="135"/>
        <source>Enter Password</source>
        <translation>Sifre Gir</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="349"/>
        <source>Password error</source>
        <translation>Sifre Hatasi</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/mainwindow.cpp" line="422"/>
        <source>OK to reboot</source>
        <translation>Y.Baslatmak icin OK</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="203"/>
        <source>Settings</source>
        <translation>Ayarlar</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="213"/>
        <source>Maintenance</source>
        <translation>Bakim</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="221"/>
        <source>Energy Saving</source>
        <translation>Enerji Tasarrufu</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="237"/>
        <source>Rect Settings</source>
        <translation>Dogrultucu Ayarlari</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="245"/>
        <source>Batt Settings</source>
        <translation>Aku Ayarlari</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="253"/>
        <source>LVD Settings</source>
        <translation>LVD Ayarlari</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="261"/>
        <source>AC Settings</source>
        <translation>AC Ayarlari</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="269"/>
        <source>Sys Settings</source>
        <translation>Sistem Ayarlari</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="277"/>
        <source>Comm Settings</source>
        <translation>Haberlesme Ayarlari</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="285"/>
        <source>Other Settings</source>
        <translation>Diger Ayarlari</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="294"/>
        <source>Slave Settings</source>
        <translation>Ikincil Ayarlari</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="302"/>
        <source>FCUP Settings</source>
        <translation>Fan Ayarlari</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="310"/>
        <source>DO Normal Settings</source>
        <translation>DO Configurações normais</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="322"/>
        <source>Basic Settings</source>
        <translation>Temel Ayarlari</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="330"/>
        <source>Charge</source>
        <translation>Sarj</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="338"/>
        <source>Battery Test</source>
        <translation>Aku Test</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="346"/>
        <location filename="../../g3_lui/equipWidget/WdgFP8BattDegCurve.cpp" line="106"/>
        <source>Temp Comp</source>
        <translation>Tazminat sıcaklığı</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="354"/>
        <source>Batt1 Settings</source>
        <translation>Aku 1 Ayarlari</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="362"/>
        <source>Batt2 Settings</source>
        <translation>Aku 2 Ayarlari</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="371"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="808"/>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1997"/>
        <source>Restore Default</source>
        <translation>Varsayıyükle</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="379"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="421"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="814"/>
        <source>Update App</source>
        <translation>Uygulama Günce.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="387"/>
        <source>Clear data</source>
        <translation>Verileri temizle</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="396"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="840"/>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2183"/>
        <source>Auto Config</source>
        <translation>Otomat yapılandırma</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="404"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="820"/>
        <source>LCD Display Wizard</source>
        <translation>LCD Ekran Sih.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="412"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="834"/>
        <source>Start Wizard Now</source>
        <translation>Sih.Simdi Baslatin</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="432"/>
        <source>System DO</source>
        <translation>Sistem DO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="441"/>
        <source>IB2 DO</source>
        <translation>IB2 DO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="450"/>
        <source>EIB1 DO</source>
        <translation>EIB1 DO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="459"/>
        <source>EIB2 DO</source>
        <translation>EIB2 DO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="470"/>
        <source>FCUP1 Settings</source>
        <translation>FCUP1 Ayarlari</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="478"/>
        <source>FCUP2 Settings</source>
        <translation>FCUP2 Ayarlari</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="486"/>
        <source>FCUP3 Settings</source>
        <translation>FCUP3 Ayarlari</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="494"/>
        <source>FCUP4 Settings</source>
        <translation>FCUP4 Ayarlari</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="809"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="815"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="821"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="835"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="841"/>
        <source>Yes</source>
        <translation>Evet</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="810"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="816"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="822"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="836"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="842"/>
        <source>No</source>
        <translation>Hayir</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="848"/>
        <source>Protocol</source>
        <translation>Protokol</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="855"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="877"/>
        <source>Address</source>
        <translation>Adres</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="860"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="882"/>
        <source>Media</source>
        <translation>Medya</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="867"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="889"/>
        <source>Baudrate</source>
        <translation>Baudrate</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="917"/>
        <source>Mask</source>
        <translation>Alt Ag Maskesi</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="935"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="940"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="967"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="972"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="977"/>
        <source>IPV6 IP</source>
        <translation>IPV6 IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="945"/>
        <source>IPV6 Prefix</source>
        <translation>IPV6 On ek</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="950"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="955"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="982"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="987"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="992"/>
        <source>IPV6 Gateway</source>
        <translation>IPV6 Ag Gecidi</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="960"/>
        <source>IPV6 DHCP</source>
        <translation>IPV6 DHCP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="997"/>
        <source>Clear Data</source>
        <translation>Verileri temizle</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="998"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="522"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="74"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="100"/>
        <source>Alarm History</source>
        <translation>Alarm Gecmisi</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1996"/>
        <source>OK to restore default</source>
        <translation>V. Geriyuklemek icin OK</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2076"/>
        <source>OK to update app</source>
        <translation>Uyg Güncellemek icin OK</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2096"/>
        <source>Without USB drive</source>
        <translation>USB sürücüsüz</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2105"/>
        <source>USB drive is empty</source>
        <translation>USB sürücüsü bos</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2114"/>
        <source>Update is not needed</source>
        <translation>Güncellestirme gereksiz</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2123"/>
        <source>App program not found</source>
        <translation>Uygulama programı bulunamadı</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2138"/>
        <source>Without script file</source>
        <translation>Komut dosyasisiz</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2155"/>
        <source>OK to clear data</source>
        <translation>Verileri silmek için Tamam&apos;ı tıklayın.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2182"/>
        <source>Start auto config</source>
        <translation>Oto Config Baslat</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2223"/>
        <source>Restore failed</source>
        <translation>Geri Yukleme Basarisiz</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2DCABranch.cpp" line="134"/>
        <source>Load</source>
        <translation>Yük</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2DCDeg1Branch.cpp" line="46"/>
        <location filename="../../g3_lui/equipWidget/Wdg2P6BattDeg.cpp" line="136"/>
        <source>Temp</source>
        <translation>Sıcaklık</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2P5BattRemainTime.cpp" line="45"/>
        <source>Batteries</source>
        <translation>Akü</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="516"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="67"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="97"/>
        <source>Active Alarms</source>
        <translation>Etkinlik alarmı</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="690"/>
        <source>Observation</source>
        <translation>Minor</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="694"/>
        <source>Major</source>
        <translation>Major</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="698"/>
        <source>Critical</source>
        <translation>Kritik</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="893"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="933"/>
        <source>Index</source>
        <translation>Dizin</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="898"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="942"/>
        <source>Iout(A)</source>
        <translation>Iout(A)</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="903"/>
        <source>State</source>
        <translation>Durum</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="938"/>
        <source>Vin(V)</source>
        <translation>Vin(V)</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1262"/>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="138"/>
        <source>Name</source>
        <translation>İsim</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1266"/>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="143"/>
        <source>SN</source>
        <translation>SN</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1270"/>
        <source>Number</source>
        <translation>Numara</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1274"/>
        <source>Product Ver</source>
        <translation>Ürün Ver</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1278"/>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="167"/>
        <source>SW Ver</source>
        <translation>SW Ver</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="25"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="26"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="596"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="597"/>
        <source>OA Alarm</source>
        <translation>OA Alarm</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="27"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="598"/>
        <source>MA Alarm</source>
        <translation>MA Alarm</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="28"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="599"/>
        <source>CA Alarm</source>
        <translation>CA Alarm</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="58"/>
        <source>Alarm</source>
        <translation>Alarm</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP0AC.cpp" line="146"/>
        <source>AC Input</source>
        <translation>AC giriş</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="85"/>
        <source>ENT to Inventory</source>
        <translation>Envanter icin ENT</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="152"/>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="173"/>
        <source>HW Ver</source>
        <translation>HW Ver</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="179"/>
        <source>Config Ver</source>
        <translation>Yapilandirma Ver</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="204"/>
        <source>File Sys</source>
        <translation>Sistem Dosyasi</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="210"/>
        <source>MAC Address</source>
        <translation>MAC Adres</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="225"/>
        <source>DHCP Server IP</source>
        <translation>DHCP Sunucu IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="249"/>
        <source>Link-Local Addr</source>
        <translation>Yerel Bağlantı Adresi</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="271"/>
        <source>Global Addr</source>
        <translation>IPV6 IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="293"/>
        <source>DHCP Server IPV6</source>
        <translation>DHCP Sunucu IPV6</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP12ScreenSaverOut.cpp" line="58"/>
        <source>Sys Used</source>
        <translation>Kullanilan Sistem</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP1Module.cpp" line="85"/>
        <source>Module</source>
        <translation>Modul</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP3DCA.cpp" line="117"/>
        <location filename="../../g3_lui/equipWidget/WdgFP5Deg2Curve.cpp" line="105"/>
        <location filename="../../g3_lui/equipWidget/WdgFP8BattDegCurve.cpp" line="105"/>
        <source>Last 7 days</source>
        <translation>Son 7 gun</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP3DCA.cpp" line="166"/>
        <source>Total Load</source>
        <translation>Toplam Yuk</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP5Deg2Curve.cpp" line="107"/>
        <source>Ambient Temp</source>
        <translation>Ortam Sicakligi</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="148"/>
        <source>ENT to OK</source>
        <translation>OK icin ENT </translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="150"/>
        <source>ESC to Cancel</source>
        <translation>Iptal icin ESC</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="301"/>
        <source>Update OK Num</source>
        <translation>Güncelleme OK</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="310"/>
        <source>Open File Failed</source>
        <translation>Dosya Acma Basarisiz</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="314"/>
        <source>Comm Time-Out</source>
        <translation>Haberlesme Zaman Asimi</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="536"/>
        <source>ENT or ESC to exit</source>
        <translation>Cikis icin ENT yada ESC</translation>
    </message>
</context>
<context>
    <name>Wdg2P5BattRemainTime</name>
    <message>
        <source>No Data</source>
        <translation type="obsolete">Veri yok</translation>
    </message>
</context>
<context>
    <name>Wdg2Table</name>
    <message>
        <source>Active Alarms</source>
        <translation type="obsolete">Aktif Alarmlar</translation>
    </message>
    <message>
        <source>Alarm History</source>
        <translation type="obsolete">Alarm Gecmisi</translation>
    </message>
    <message>
        <source>Observation</source>
        <translation type="obsolete">Minor</translation>
    </message>
    <message>
        <source>Major</source>
        <translation type="obsolete">Major</translation>
    </message>
    <message>
        <source>Critical</source>
        <translation type="obsolete">Kritik</translation>
    </message>
    <message>
        <source>Index</source>
        <translation type="obsolete">Dizin</translation>
    </message>
    <message>
        <source>Iout(A)</source>
        <translation type="obsolete">Iout(A)</translation>
    </message>
    <message>
        <source>State</source>
        <translation type="obsolete">Durum</translation>
    </message>
    <message>
        <source>Vin(V)</source>
        <translation type="obsolete">Vin(V)</translation>
    </message>
    <message>
        <source>No Data</source>
        <translation type="obsolete">Veri yok</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">İsim</translation>
    </message>
    <message>
        <source>SN</source>
        <translation type="obsolete">SN</translation>
    </message>
    <message>
        <source>Number</source>
        <translation type="obsolete">Numara</translation>
    </message>
    <message>
        <source>Product Ver</source>
        <translation type="obsolete">Ürün Ver</translation>
    </message>
    <message>
        <source>SW Ver</source>
        <translation type="obsolete">SW Ver</translation>
    </message>
</context>
<context>
    <name>Wdg3Table</name>
    <message>
        <source>OA Alarm</source>
        <translation type="obsolete">OA Alarm</translation>
    </message>
    <message>
        <source>MA Alarm</source>
        <translation type="obsolete">MA Alarm</translation>
    </message>
    <message>
        <source>CA Alarm</source>
        <translation type="obsolete">CA Alarm</translation>
    </message>
    <message>
        <source>No Data</source>
        <translation type="obsolete">Veri yok</translation>
    </message>
</context>
<context>
    <name>WdgFCfgGroup</name>
    <message>
        <source>OK to restore default</source>
        <translation type="obsolete">V. Geriyuklemek icin OK</translation>
    </message>
    <message>
        <source>Restore Default</source>
        <translation type="obsolete">Varsayilani Geriyukle</translation>
    </message>
    <message>
        <source>OK to update app</source>
        <translation type="obsolete">Uyg Güncellemek icin OK</translation>
    </message>
    <message>
        <source>Without USB drive</source>
        <translation type="obsolete">USB sürücüsüz</translation>
    </message>
    <message>
        <source>USB drive is empty</source>
        <translation type="obsolete">USB sürücüsü bos</translation>
    </message>
    <message>
        <source>Update is not needed</source>
        <translation type="obsolete">Güncellestirme gereksiz</translation>
    </message>
    <message>
        <source>App program not found</source>
        <translation type="obsolete">Uygulama programı bulunamadı</translation>
    </message>
    <message>
        <source>Without script file</source>
        <translation type="obsolete">Komut dosyasisiz</translation>
    </message>
    <message>
        <source>OK to clear data</source>
        <translation type="obsolete">Verileri silmek için Tamam&apos;ı tıklayın.</translation>
    </message>
    <message>
        <source>Start auto config</source>
        <translation type="obsolete">Oto Config Baslat</translation>
    </message>
    <message>
        <source>Auto Config</source>
        <translation type="obsolete">Oto Config</translation>
    </message>
    <message>
        <source>Rebooting</source>
        <translation type="obsolete">Y. Baslatiliyor</translation>
    </message>
    <message>
        <source>Restore failed</source>
        <translation type="obsolete">Geri Yukleme Basarisiz</translation>
    </message>
</context>
<context>
    <name>WdgFP0AC</name>
    <message>
        <source>AC</source>
        <translation type="obsolete">AC</translation>
    </message>
</context>
<context>
    <name>WdgFP10Inventory</name>
    <message>
        <source>ENT to Inventory</source>
        <translation type="obsolete">Envanter icin ENT</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">İsim</translation>
    </message>
    <message>
        <source>SN</source>
        <translation type="obsolete">SN</translation>
    </message>
    <message>
        <source>IP</source>
        <translation type="obsolete">IP</translation>
    </message>
    <message>
        <source>SW Ver</source>
        <translation type="obsolete">SW Ver</translation>
    </message>
    <message>
        <source>HW Ver</source>
        <translation type="obsolete">HW Ver</translation>
    </message>
    <message>
        <source>Config Ver</source>
        <translation type="obsolete">Yapilandirma Ver</translation>
    </message>
    <message>
        <source>File Sys</source>
        <translation type="obsolete">Sistem Dosyasi</translation>
    </message>
    <message>
        <source>MAC Address</source>
        <translation type="obsolete">MAC Adres</translation>
    </message>
    <message>
        <source>DHCP Server IP</source>
        <translation type="obsolete">DHCP Sunucu IP</translation>
    </message>
    <message>
        <source>Link-Local Addr</source>
        <translation type="obsolete">Yerel Bağlantı Adresi</translation>
    </message>
    <message>
        <source>Global Addr</source>
        <translation type="obsolete">IPV6 IP</translation>
    </message>
    <message>
        <source>DHCP Server IPV6</source>
        <translation type="obsolete">DHCP Sunucu IPV6</translation>
    </message>
</context>
<context>
    <name>WdgFP12ScreenSaverOut</name>
    <message>
        <source>Sys Used</source>
        <translation type="obsolete">Kullanilan Sistem</translation>
    </message>
</context>
<context>
    <name>WdgFP1Module</name>
    <message>
        <source>Module</source>
        <translation type="obsolete">Modul</translation>
    </message>
</context>
<context>
    <name>WdgFP3DCA</name>
    <message>
        <source>Last 7 days</source>
        <translation type="obsolete">Son 7 gun</translation>
    </message>
    <message>
        <source>Total Load</source>
        <translation type="obsolete">Toplam Yuk</translation>
    </message>
</context>
<context>
    <name>WdgFP5Deg2Curve</name>
    <message>
        <source>Last 7 days</source>
        <translation type="obsolete">Son 7 gun</translation>
    </message>
    <message>
        <source>Ambient Temp</source>
        <translation type="obsolete">Ortam Sicakligi</translation>
    </message>
</context>
<context>
    <name>WdgFP8BattDegCurve</name>
    <message>
        <source>Last 7 days</source>
        <translation type="obsolete">Son 7 gun</translation>
    </message>
    <message>
        <source>Temp Comp</source>
        <translation type="obsolete">Sic. Kompanzasyonu</translation>
    </message>
</context>
</TS>
