/******************************************************************************
文件名：    configparam.cpp
功能：      此类定义各种参数的配置，在运行时用户可动态更改
作者：      刘金煌
创建日期：   2013年3月24日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "configparam.h"

#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QMap>
#include <QRegExp>
#include <QTextCodec>
#include <QApplication>
#include "common/pubInclude.h"
#include "common/global.h"
#include "common/uidefine.h"
#include "config/PosBase.h"
#include "config/PosHomepage.h"
#include "config/PosPieChart.h"
#include "config/PosCurve.h"
#include "config/PosBarChart.h"
#include "config/PosThermometer.h"
#include "config/PosTableWidget.h"
#include "config/PosDlgInfo.h"
#include <dirent.h>
#include <string.h>

ConfigParam* g_cfgParam;
Init_Param ConfigParam::ms_initParam;
QString    ConfigParam::pathImg;
int        ConfigParam::ms_maxCharsPerTableRow;

QFont ConfigParam::gFontLargeN;
QFont ConfigParam::gFontLargeB;
QFont ConfigParam::gFontSmallN;

QFontMetrics* ConfigParam::ms_pFmLargeN;
QFontMetrics* ConfigParam::ms_pFmLargeB;
QFontMetrics* ConfigParam::ms_pFmSmallN;

ConfigParam::ConfigParam()
{
}

// read Qt parameter
//
int ConfigParam::iniPos()
{
    PosBase base;
    base.init();

    PosHomepage homepage;
    homepage.init();

    PosPieChart pie;
    pie.init();

    PosCurve curve;
    curve.init();

    PosBarChart barChart;
    barChart.init();

    PosThermometer thermometer;
    thermometer.init();

    PosTableWidget posTableWidget;
    posTableWidget.init();

    PosDlgInfo posDlgInfo;
    posDlgInfo.init();

    return 0;
}

int ConfigParam::initParam(bool bWait)
{
#ifdef TEST_GUI
    return 0;
#endif

    //alarmVoice keypadVoice lcdRotation timeFormat
    CmdItem cmdItem;
    cmdItem.CmdType   = CT_READ;
    cmdItem.ScreenID  = SCREEN_ID_Init;
    TRACEDEBUG( "ConfigParam::initParam() bWait<%d> ScreenID<%x>", bWait, cmdItem.ScreenID );
    void* pData = NULL;
    bool bGetted = false;
    if ( bWait )
    {
        TRACEDEBUG( ">>> ConfigParam::initParam() dlg waiting ******" );
        g_pDlgLoading->start(&cmdItem, &pData);
        g_pDlgLoading->setFocus();
        g_pDlgLoading->exec();
        bGetted = true;
        TRACEDEBUG( "ConfigParam::initParam() dlg finish ******>>>" );
    }
    else
    {
//        int n = MAX_COUNT_GET_DATA;
//        while(n-- > 0) {
//        if ( !data_getDataSync(&cmdItem, &pData) )
//        {
//            int iScreenID = ((Head_t*)pData)->iScreenID;
//            if (iScreenID == cmdItem.ScreenID)
//            {
//                TRACEDEBUG( "ConfigParam::initParam data_getDataSync ScreenID1<%06x> ScreenID2<%06x> is my data", m_cmdItem.ScreenID, iScreenID );
//                bGetted = true;
//                break;
//            }
//            else
//            {
//                TRACEDEBUG( "ConfigParam::initParam data_getDataSync ScreenID1<%06x> ScreenID2<%06x> isn't my data", m_cmdItem.ScreenID, iScreenID );
//                continue;
//            }
//        }
//        else
//        {
//            pData = NULL;
//            TRACELOG1( "data_getDataSync ScreenID<%06x> no data", m_cmdItem.ScreenID );
//        }
        bGetted = (data_getDataSync(&cmdItem, &pData)==0);
    }

    if ( bGetted )
    {
        if(pData == NULL)
        {
            return 0;
        }

        PACK_DATAINFO* info = (PACK_DATAINFO*)pData;
        int nIdxArr = 0;
        ++nIdxArr;
        ms_initParam.alarmVoice = (enum ALARM_VOICE)info->DataInfo[nIdxArr].vValue.enumValue;

        ++nIdxArr;
        ms_initParam.keypadVoice = (enum KEYPAD_VOICE)info->DataInfo[nIdxArr].vValue.enumValue;
        sys_setBuzzKeyboard(ms_initParam.keypadVoice==KEYPAD_VOICE_ON);

        ++nIdxArr;
        ms_initParam.lcdRotation = (enum LCD_ROTATION)info->DataInfo[nIdxArr].vValue.enumValue;

        ++nIdxArr;
        ms_initParam.timeFormat = (enum TIME_FORMAT)info->DataInfo[nIdxArr].vValue.enumValue;

        ++nIdxArr;
        ms_initParam.homePageType =
                (enum HOMEPAGE_TYPE)(info->DataInfo[nIdxArr].vValue.lValue);
    }

    switch ( ms_initParam.langType )
    {
    case LANG_Chinese:
    case LANG_TraditionalChinese:
    {
        ms_initParam.timeFormat = TIME_FORMAT_CN;
    }
        break;

    default:
        break;
    }

    ms_initParam.alarmTime = 0;
    if (g_cfgParam->ms_initParam.homePageType != HOMEPAGE_TYPE_SLAVE)
    {
        switch ( ms_initParam.alarmVoice )
        {
        case ALARM_VOICE_ON:
            ms_initParam.alarmTime = -1;
            break;

        case ALARM_VOICE_OFF:
            ms_initParam.alarmTime = 0;
            break;

        case ALARM_VOICE_3M:
            ms_initParam.alarmTime = 3*60;
            break;

        case ALARM_VOICE_10M:
            ms_initParam.alarmTime = 10*60;
            break;

        case ALARM_VOICE_1H:
            ms_initParam.alarmTime = 1*60*60;
            break;

        case ALARM_VOICE_4H:
            ms_initParam.alarmTime = 4*60*60;
            break;

        default:
            break;
        }
    }

    // test
    //ms_initParam.lcdRotation  = LCD_ROTATION_0DEG;
    //ms_initParam.homePageType = HOMEPAGE_TYPE_MAIN;
    //ms_initParam.homePageType = HOMEPAGE_TYPE_SLAVE;
    TRACELOG1( "ConfigParam::initParam() "
                "alarmVoice<%d>1:OFF alarmTime<%d>-1:ON 0:OFF "
                "keypadVoice<%d>0:ON 1:OFF lcdRotation<%d> "
                "timeFormat<%d> homePageType<%d>",
                ms_initParam.alarmVoice, ms_initParam.alarmTime,
                ms_initParam.keypadVoice, ms_initParam.lcdRotation,
                ms_initParam.timeFormat, ms_initParam.homePageType
                );
    return 0;
}

void ConfigParam::setLanguageFont()
{
    //TRACEDEBUG( "ConfigParam::setLanguageFont" );
#ifdef TEST_GUI
    {
    ms_initParam.langType = LANG_English;
    // 字库
    gFontLargeN.setFamily("WenQuanYi Zen Hei");
    gFontLargeB.setFamily("WenQuanYi Zen Hei");
    gFontSmallN.setFamily("WenQuanYi Zen Hei");

    QTextCodec *codec = NULL;
    codec = QTextCodec::codecForName( "UTF-8" );
    switch (ConfigParam::ms_initParam.lcdRotation)
    {
    case LCD_ROTATION_0DEG:
        ms_maxCharsPerTableRow = 18;
        break;
    case LCD_ROTATION_90DEG:
        ms_maxCharsPerTableRow = 16;
        break;
    case LCD_ROTATION_BIG:
        ms_maxCharsPerTableRow = 30;
        break;
    }

    QTextCodec::setCodecForLocale(codec);
    QTextCodec::setCodecForCStrings(codec);
    QTextCodec::setCodecForTr(codec);
    if(ms_initParam.lcdRotation == LCD_ROTATION_BIG)
    {

        gFontLargeN.setPixelSize(19);
        gFontLargeB.setPixelSize(20);
        gFontSmallN.setPixelSize(18);
    }
    else if(ms_initParam.lcdRotation == LCD_ROTATION_0DEG)
    {
        gFontLargeN.setPixelSize(12);
        gFontLargeB.setPixelSize(12);
        gFontSmallN.setPixelSize(10);
    }
    else
    {
        g_szTimeFormat[0] =    "dd/MM/yy";
        g_szTimeFormat[1] =    "MM/dd/yy";
        g_szTimeFormat[2] =    "yy/MM/dd";
        /* modify by wufang,20131014, modify 2/2,adjust width for display year "YYYY"  */
        //g_szHomePageDateFmt[0] =     "dd/MM/yy";
        //g_szHomePageDateFmt[1] =    "MM/dd/yy";
        //g_szHomePageDateFmt[2] =    "yy/MM/dd";
        gFontLargeN.setPixelSize(12);
        gFontLargeB.setPixelSize(12);
        gFontSmallN.setPixelSize(10);
    }
    gFontLargeN.setWeight( QFont::Normal );
    gFontLargeB.setWeight( QFont::Black );
    gFontSmallN.setWeight( QFont::Normal );

    ms_pFmLargeN = new QFontMetrics( gFontLargeN );
    ms_pFmLargeB = new QFontMetrics( gFontLargeB );
    ms_pFmSmallN = new QFontMetrics( gFontSmallN );

//    QString strTestWidth( "WWWWWWWWWW" );
//    TRACEDEBUG( "width: %d", ms_pFmLargeN->width( strTestWidth ) );

    // Qt多语言文件
    char szLangFile[128] = "";
    sprintf( szLangFile,
             PATH_APP "translations/lang_%s",
             g_mapLangStr[ms_initParam.langType]
             );
    TRACELOG1( "ConfigParam::setLanguageFont lang translate langType<%d> file<%s>", ms_initParam.langType, szLangFile );
    g_translator.load( szLangFile );
    qApp->installTranslator( &g_translator );
    }
    return;
#endif

    LANGUAGE_TYPE langType = ms_initParam.langType;
    TRACELOG1( "ConfigParam::setLanguageFont langType<%d>", langType );

    // 字库
    gFontLargeN.setFamily("WenQuanYi Zen Hei");
    gFontLargeB.setFamily("WenQuanYi Zen Hei");
    gFontSmallN.setFamily("WenQuanYi Zen Hei");

    QTextCodec *codec = NULL;
    switch ( langType )
    {
    case LANG_Chinese:
    case LANG_TraditionalChinese:
    {
        codec = QTextCodec::codecForName( "GBK" );
        switch (ConfigParam::ms_initParam.lcdRotation)
        {
        case LCD_ROTATION_0DEG:
            ms_maxCharsPerTableRow = 11;
            break;
        case LCD_ROTATION_90DEG:
            ms_maxCharsPerTableRow = 8;
            break;
        case LCD_ROTATION_BIG:
            ms_maxCharsPerTableRow = 14;
            break;
        }
        ms_initParam.timeFormat = TIME_FORMAT_CN;
    }
        break;

    case LANG_Russia:
    {
        codec = QTextCodec::codecForName( "KOI8-R" );
        switch (ConfigParam::ms_initParam.lcdRotation)
        {
        case LCD_ROTATION_0DEG:
            ms_maxCharsPerTableRow = 18;
            break;
        case LCD_ROTATION_90DEG:
            ms_maxCharsPerTableRow = 16;
            break;
        case LCD_ROTATION_BIG:
            ms_maxCharsPerTableRow = 30;
            break;
        }
    }
        break;

    default:
    {
        codec = QTextCodec::codecForName( "UTF-8" );
        switch (ConfigParam::ms_initParam.lcdRotation)
        {
        case LCD_ROTATION_0DEG:
            ms_maxCharsPerTableRow = 18;
            break;
        case LCD_ROTATION_90DEG:
            ms_maxCharsPerTableRow = 16;
            break;
        case LCD_ROTATION_BIG:
            ms_maxCharsPerTableRow = 30;
            break;
        }
    }
        break;
    }

    QTextCodec::setCodecForLocale(codec);
    QTextCodec::setCodecForCStrings(codec);
    QTextCodec::setCodecForTr(codec);


    if(ms_initParam.lcdRotation == LCD_ROTATION_BIG)
    {

        gFontLargeN.setPixelSize(19);
        gFontLargeB.setPixelSize(20);
        gFontSmallN.setPixelSize(18);
    }
    else if(ms_initParam.lcdRotation == LCD_ROTATION_0DEG)
    {
        gFontLargeN.setPixelSize(12);
        gFontLargeB.setPixelSize(12);
        gFontSmallN.setPixelSize(10);
    }
    else
    {
        g_szTimeFormat[0] =    "dd/MM/yy";
        g_szTimeFormat[1] =    "MM/dd/yy";
        g_szTimeFormat[2] =    "yy/MM/dd";
		/* modify by wufang,20131014, modify 2/2,adjust width for display year "YYYY"  */
        //g_szHomePageDateFmt[0] =     "dd/MM/yy";
        //g_szHomePageDateFmt[1] =    "MM/dd/yy";
        //g_szHomePageDateFmt[2] =    "yy/MM/dd";
        gFontLargeN.setPixelSize(12);
        gFontLargeB.setPixelSize(12);
        gFontSmallN.setPixelSize(10);
    }
    gFontLargeN.setWeight( QFont::Normal );
    gFontLargeB.setWeight( QFont::Black );
    gFontSmallN.setWeight( QFont::Normal );

    ms_pFmLargeN = new QFontMetrics( gFontLargeN );
    ms_pFmLargeB = new QFontMetrics( gFontLargeB );
    ms_pFmSmallN = new QFontMetrics( gFontSmallN );

//    QString strTestWidth( "WWWWWWWWWW" );
//    TRACEDEBUG( "width: %d", ms_pFmLargeN->width( strTestWidth ) );

    // Qt多语言文件
    char szLangFile[128] = "";
    sprintf( szLangFile,
             PATH_APP "translations/lang_%s",
             g_mapLangStr[ms_initParam.langType]
             );
    TRACELOG1( "ConfigParam::setLanguageFont lang translate langType<%d> file<%s>", ms_initParam.langType, szLangFile );
    g_translator.load( szLangFile );
    qApp->installTranslator( &g_translator );
}

// 用户不选择语言，超时进入主页，若是英语只能存在本地，app无法获得
int ConfigParam::readLanguageLocal()
{
    int nRet = -1;
    QFile file( FILENAME_LANGUAGE );
    QString strLang;
    if ( file.open( QIODevice::ReadOnly | QIODevice::Text) )
    {
        QTextStream textIn( &file );
        strLang = textIn.readLine();
        if ( !strLang.isNull() )
        {
            nRet = strLang.toInt();
        }
        file.close();
    }
    TRACELOG1( "ConfigParam::readLanguageLocal() %d", nRet );
    ConfigParam::ms_initParam.langLocate =
            (enum LANGUAGE_LOCATE)nRet;
    return nRet;
}

int ConfigParam::writeLanguageLocal()
{
    QFile file( FILENAME_LANGUAGE );
    if ( file.open(QIODevice::ReadWrite | QIODevice::Truncate | QIODevice::Text) )
    {
        file.write(
                    QString::number(
                        ConfigParam::ms_initParam.langLocate).
                    toUtf8().constData()
                    );
        file.close();
    }
    return 0;
}

int ConfigParam::readLanguageApp()
{
#ifdef TEST_GUI
    return 0;
#endif

    QFile file( "/app/config/solution/MonitoringSolution.cfg" );
    QString strLang;
    if ( file.open( QIODevice::ReadOnly | QIODevice::Text) )
    {
        TRACEDEBUG( "ConfigParam::readLanguageApp() open file OK" );
        int nIdxLang = -1;
        QTextStream textIn( &file );
        strLang = textIn.readLine();
        while ( !strLang.isNull() )
        {
            static bool bReaded = false;
            nIdxLang = strLang.indexOf("LANGUAGE_INFO", 0, Qt::CaseInsensitive);
            if ( !bReaded && nIdxLang>0 )
            {
                TRACEDEBUG( "ConfigParam::readLanguageApp() find section [LANGUAGE_INFO] index<%d>", nIdxLang );
                bReaded = true;
                strLang = textIn.readLine();
                continue;
            }
            if (bReaded && strLang.trimmed().length()>0 &&
                    strLang.indexOf("#", 0, Qt::CaseInsensitive)<0)
            {
                TRACEDEBUG( "ConfigParam::readLanguageApp() strLang line <%s>", strLang.toUtf8().constData() );
                QRegExp sep("\\s+");
                strLang = strLang.section(sep, 0, 0, QString::SectionSkipEmpty);
                break;
            }
            strLang = textIn.readLine();
        }
        if (strLang.length() > 0)
        {
            QMap<QString, enum LANGUAGE_TYPE> map;
            map["en"] = LANG_English;
            map["zh"] = LANG_Chinese;
            map["fr"] = LANG_French;
            map["de"] = LANG_German;
            map["it"] = LANG_Italian;
            map["ru"] = LANG_Russia;
            map["es"] = LANG_Spanish;
            map["tw"] = LANG_TraditionalChinese;
			map["pt"] = LANG_Portugal;
            map["tr"] = LANG_Turkish;
            LANGUAGE_TYPE langApp = map[QString(strLang).toLower()];
            TRACEDEBUG( "ConfigParam::readLanguageApp() %s langApp<%d>",
                        strLang.toUtf8().constData(), langApp );
            ms_initParam.langApp = langApp;
        }
        file.close();
    }
    else
    {
        TRACELOG2( "ConfigParam::readLanguageApp() open file failed" );
    }

    return 0;
}

int ConfigParam::writeLanguageApp()
{
#ifdef TEST_GUI
    return 0;
#endif

    // ls /app/config_default/lang/tw | wc
    // ls /app/config/lang/tw |wc
    // /app/config_default/lang/tw  /app/config/lang/

    TRACEDEBUG( "ConfigParam::writeLanguageApp() copy lang files" );
    LANGUAGE_TYPE langType = ConfigParam::ms_initParam.langApp;
    const char* pszCountry = g_mapLangStr[langType];
    // rm -rf /app/config/lang/*
//    system( "rm -rf /app/config/lang/*" );
    //removeDirectory( "/app/config/lang" );

//    sprintf( szCmd,
//             "cp /app/config_default/lang/%s  /app/config/lang/ -rf",
//             pszCountry
//             );
//    // cp /app/config_default/lang/zh  /app/config/lang/ -rf
//    system( szCmd );
    QString strFromDir;
    strFromDir.sprintf( "/app/config_default/lang/%s", pszCountry );
    QString strToDir;
    strToDir.sprintf( "/app/config/lang/%s", pszCountry );
    if ( !copyDirectoryFiles(strFromDir, strToDir) )
    {
        TRACELOG2( "ConfigParam::writeLanguageApp copy Dir<%s> error", strFromDir.toUtf8().constData() );
        return 3;
    }

    TRACEDEBUG( "ConfigParam::writeLanguageApp() copy MonitoringSolution.cfg" );
    // MonitoringSolution.cfg 拷贝不全
    QTime t;
    t.start();
    while(t.elapsed() < 10000)
    {
        QCoreApplication::processEvents(
                    QEventLoop::AllEvents, 20);
    }
    //mainSleep( 10000 );
    char szFile[32] = "";
    sprintf( szFile, "MonitoringSolution_%s.cfg", pszCountry );
    TRACEDEBUG("ConfigParam::writeLanguageApp file cp /app/config_default/solution/%s /app/config_default/lang/%s langType<%d>",
               szFile, pszCountry, langType);
//    char szCmd[128] = "";
//    sprintf( szCmd,
//             "cp /app/config_default/solution/%s "
//             "/app/config/solution/MonitoringSolution.cfg -f",
//             szFile
//             );
//    // cp /app/config_default/solution/MonitoringSolution_zh.cfg /app/config/solution/MonitoringSolution.cfg -f
//    system( szCmd );

    QString strSrcFileName;
    strSrcFileName.sprintf(
                "/app/config_default/solution/%s",
                szFile
                );
    QFile srcFile( strSrcFileName );
    QString strDesFileName( "/app/config/solution/MonitoringSolution.cfg" );
    QFile desFile( strDesFileName );
    if ( !srcFile.exists() )
    {
        TRACELOG2( "ConfigParam::writeLanguageApp <%s> not exist", strSrcFileName.toUtf8().constData() );
        return 1;
    }
    if ( desFile.exists() )
    {
        desFile.remove();
    }
    if ( !srcFile.copy(strDesFileName) )
    {
        TRACELOG2( "ConfigParam::writeLanguageApp copy File<%s> error", strSrcFileName.toUtf8().constData() );
        return 2;
    }
    TRACEDEBUG( "ConfigParam::writeLanguageApp() OK" );

    return 0;
}

bool ConfigParam::copyDirectoryFiles(
        const QString &fromDir,
        const QString &toDir)
{
    static int nFiles = 0;
    QDir sourceDir(fromDir);
    QDir targetDir(toDir);
    if ( !targetDir.exists() )
    {    /**< 如果目标目录不存在，则进行创建 */
        if ( !targetDir.mkdir(targetDir.absolutePath()) )
        {
            TRACELOG2( "ConfigParam::copyDirectoryFiles 1" );
            return false;
        }
    }

    QFileInfoList fileInfoList = sourceDir.entryInfoList();
    foreach (QFileInfo fileInfo, fileInfoList)
    {
        if (fileInfo.fileName() == "." ||
                fileInfo.fileName() == "..")
        {
            continue;
        }

        if ( fileInfo.isDir() )
        {
            /**< 当为目录时，递归的进行copy */
            if ( !copyDirectoryFiles(fileInfo.filePath(),
                targetDir.filePath(fileInfo.fileName()))
                 )
            {
                TRACELOG2( "ConfigParam::copyDirectoryFiles 2" );
                return false;
            }
        }
        else
        {
            /**< 当允许覆盖操作时，将旧文件进行删除操作 */
            if ( targetDir.exists(fileInfo.fileName()) )
            {
                targetDir.remove(fileInfo.fileName());
            }
            /// 进行文件copy
            TRACEDEBUG( "ConfigParam::copyDirectoryFiles <%d> <%s> <%s>",
                        ++nFiles,
                        fileInfo.filePath().toUtf8().constData(),
                        targetDir.filePath(fileInfo.fileName()).toUtf8().constData() );
            if ( !QFile::copy(fileInfo.filePath(),
                targetDir.filePath(fileInfo.fileName())) )
            {
                TRACELOG2( "ConfigParam::copyDirectoryFiles 3" );
                    return false;
            }
        }
    }
    nFiles = 0;
    return true;
}

bool ConfigParam::removeDirectory(const QString &dirName)
{
    TRACEDEBUG( "ConfigParam::removeDirectory<%s/*>", dirName.toUtf8().constData() );
    QDir dir(dirName);
    QString tmpdir = "";
    if ( !dir.exists() )
    {
        return false;
    }
    QFileInfoList fileInfoList = dir.entryInfoList();
    foreach (QFileInfo fileInfo, fileInfoList)
    {
        if(fileInfo.fileName() == "." || fileInfo.fileName() == "..")
            continue;

        if (fileInfo.isDir())
        {
            tmpdir = dirName + ("/") + fileInfo.fileName();
            removeDirectory(tmpdir);
            dir.rmdir(fileInfo.fileName()); /**< 移除子目录 */
            //TRACEDEBUG( "ConfigParam::removeDirectory rmSubDir<%s>", fileInfo.fileName().toUtf8().constData() );
        }
        else if (fileInfo.isFile())
        {
            QFile tmpFile(fileInfo.fileName());
            dir.remove(tmpFile.fileName()); /**< 删除临时文件 */
            //TRACEDEBUG( "ConfigParam::removeDirectory rmFile<%s>", tmpFile.fileName().toUtf8().constData() );
        }
    }
//    /**< 返回上级目录，因为只有返回上级目录，才可以删除这个目录 */
//    dir.cdUp();
//    if (dir.exists(dirName))
//    {
//        TRACEDEBUG( "ConfigParam::removeDirectory rmTopDir<%s>", dirName.toUtf8().constData() );
//        if (!dir.rmdir(dirName))
//            return false;
//    }

    return true;
}

