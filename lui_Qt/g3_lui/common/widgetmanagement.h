#ifndef WIDGETMANAGEMENT_H
#define WIDGETMANAGEMENT_H
#include <QWidget>
class WidgetManagement
{
public:
    WidgetManagement();
    void ShowWidget(int iWidgetID);
    void BuildPageBuffer(QWidget* which,int BuildType);//建立下一页或上一页的缓冲区

private:
    QWidget* pPointBufferPage; //用于中间缓冲
    QWidget* pPointLastPageList; //用于建立上一页的缓冲区
    QWidget* pPointNextPageList;//用于建立下一页的缓冲区

};

#endif // WIDGETMANAGEMENT_H
