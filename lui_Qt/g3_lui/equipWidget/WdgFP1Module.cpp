/******************************************************************************
�ļ�����    WdgFP1Module.cpp
���ܣ�      ��һ�����p1 Module
���ߣ�      �����
�������ڣ�   2013��5��13��
����޸����ڣ�2013��8��6�� ����converter �ź���Ҳ��app��ȡ
�޸��ߣ�
�޸����ݣ�
�޸����ڣ�
******************************************************************************/

#include "WdgFP1Module.h"
#include "ui_WdgFP1Module.h"

#include <QKeyEvent>
#include "config/configparam.h"
#include "common/pubInclude.h"
#include "config/PosBase.h"
#include "common/global.h"

#define LED_STATE_RED_ON         0x01
#define LED_STATE_YELLOW_ON      0x02
#define LED_STATE_GREEN_ON       0x04
#define LED_STATE_GREEN_FLASH_ON 0x08
#define MAX_ITEMS_NUM_OF_MODULE     (10)

WdgFP1Module::WdgFP1Module(QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::WdgFP1Module)
{
    ui->setupUi(this);
    TRACEDEBUG("WdgFP1Module::Start----------\n");//hhy
    InitWidget();
    InitConnect();

    m_timerId = 0;
    m_cmdItem.CmdType   = CT_READ;//0
    m_cmdItem.ScreenID  = SCREEN_ID_WdgFP1Module;//1024
    //����������CmdType ScreenID ,Ȼ��APPͨ������������ֵ�����
    //TRACEDEBUG("WdgFP1Module::WdgFP1Module---SCREEN_ID_WdgFP1Module<%d>",SCREEN_ID_WdgFP1Module);//hhy
    m_nSelRow  = 0;
}

WdgFP1Module::~WdgFP1Module()
{
    ui->tableWidget->clearItems();
    delete ui;
}
//��ʼ������
void WdgFP1Module::InitWidget()
{
    SET_GEOMETRY_WIDGET( this );
    SET_BACKGROUND_WIDGET( "WdgFP1Module",PosBase::strImgBack_Line_Title );
    SET_GEOMETRY_LABEL_TITLE( ui->label_title );
    SET_TABLEWDG_STYLE( tableWidget, TT_YES_TITLE_NOT_SCROOL );

    for(int i=0;i< MAX_ITEMS_NUM_OF_MODULE;i++)//���10��
    {
        ui->tableWidget->insertRow(i);
        QTableWidgetItem* item = new QTableWidgetItem;
        ui->tableWidget->setItem(i, 0, item);
        ui->tableWidget->setRowHeight(i, TABLEWDG_ROW_HEIGHT);
    }
}

void WdgFP1Module::InitConnect()
{
//    connect( ui->tableWidget, SIGNAL(sigTableKeyPress(int)),
//             this, SLOT(sltTableKeyPress(int)) );
}
//�س�
void WdgFP1Module::Enter(void* param)
{

    TRACEDEBUG("WdgFP1Module::Enter------start");
    //TRACELOG1( "WdgFP1Module::Enter(void* param)" );
    Q_UNUSED( param );
    INIT_VAR;

    m_timeElapsedKeyPress.restart();
    m_bEnterFirstly = true;
#ifdef TEST_GUI
    ENTER_FIRSTLY;
#else
    m_nRectNum = 0;
    m_nSolNum  = 0;
    ENTER_GET_DATA;//��ȡ����
#endif
   if(g_ConvFlag)
    {
    ui->label_title->setText( QObject::tr("Convertor") );
    }
    else
     { 
      ui->label_title->setText( QObject::tr("Module") );
      }

    if (ui->tableWidget->rowCount() > 0)
    {
        if (g_nEnterPageType != ENTER_PAGE_TYPE_KEY_ESC)
        {
            ui->tableWidget->selectRow( 0 );
        }
    }
    SET_STYLE_SCROOLBAR( m_nRows+1, 1 );
    m_bEnterFirstly = false;
}

void WdgFP1Module::Leave()
{
    LEAVE_WDG( "WdgFP1Module" );
    this->deleteLater();
}

//ÿ��ˢ�»����ScreenID
void WdgFP1Module::Refresh()
{
    REFRESH_DATA_PERIODICALLY(
                m_cmdItem.ScreenID,
                "WdgFP1Module"
                );
}


void WdgFP1Module::ShowData(void* pData)
{
    TRACEDEBUG( "WdgFP1Module::ShowData time<%d> m_bEnterFirstly<%d>", m_timeElapsedKeyPress.elapsed(), m_bEnterFirstly );
    if ( !pData ||
         (m_timeElapsedKeyPress.elapsed()<
          TIME_ELAPSED_KEYPRESS &&
          !m_bEnterFirstly) )
    {
        TRACEDEBUG("WdgFP1Module::NO DATA HERE");//HHY
        return;
    }
    //appendReadOnlyItemΪ�����ݣ�����һ���µģ��������� hhy
#define APPEND_READONLY_DATA \
    info++; \
    appendReadOnlyItem(info, infoEnd, nDataNum); \
    m_nSelRow = ui->tableWidget->selectedRow(); \
    TRACEDEBUG("==================inside nDataNum<%d>",nDataNum); \
    TRACEDEBUG("=================WdgFP1Module::ROW<%d>",m_nSelRow);
    ui->tableWidget->clearItemsTexts();

    PACK_INFO* info    = (PACK_INFO*)pData;//app������
    int nDataNum       = info->iDataNum;
    PACK_INFO* infoEnd = info+nDataNum;
    TRACEDEBUG("========================WdgFP1Module::info<%d> cSigName<%s>",info->stHead,info->cSigName);//hhy
    m_nRows = -1;
    for (int i=0; i<MAX_MODULE_ITEMS; ++i)
    {
        m_mapW2nd[i] = 0;
        m_mapNum[i]  = 0;
    }
    bool bShowRect = true;
    bool bShowSol  = true;
    bool bShowConv = true;
    bool bShowInv = true;
    int nSigVal = 0;

    // Converterģʽ	1-��RectNum���к͡�Sol Conv Num���ж�����ʾ
    --info;
    APPEND_READONLY_DATA;
    long nVal1 = info->vSigValue.lValue;
    //nVal1 =1;
    if ((nVal1 == 1 )|| (g_ConvFlag == true))
    {
        bShowRect = false;
        bShowSol  = false;
        bShowInv  = false;
    }

    // MPPT����ģʽ	0- ��Sol Conv Num���в���ʾ
    // 1-��RectNum���к͡�Sol Conv Num���ж���ʾ
    // 2-��RectNum���в���ʾ
    APPEND_READONLY_DATA;
    long nVal2 = info->vSigValue.lValue;
    if (nVal2 == 0)
    {
        bShowSol  = false;
    }
    else if (nVal2 == 2)
    {
        bShowRect = false;
    }

    // RectNum	Rect����
    APPEND_READONLY_DATA;
    long nVal3 = info->vSigValue.lValue;
    if ( bShowRect )
    {
        TRACEDEBUG("WdgFP1Module::ShowData-----packing line RectNum");//hhy15
        addTableRow_moduleNum(
                    info,
                    WT2_RECTINFO
                    );
    }

    // Sol Conv Num
    APPEND_READONLY_DATA;
    long nVal4 = info->vSigValue.lValue;
    if ( bShowSol )
    {
        addTableRow_moduleNum(
                    info,
                    WT2_SOLINFO
                    );
    }

    // ConvNum	Conv���� =0ʱ��ConvNum�� �в���ʾ��
    APPEND_READONLY_DATA;
    long nVal5 = info->vSigValue.lValue;
    if (nVal5 > 0)
    {
        bShowConv = true;
        addTableRow_moduleNum(
                    info,
                    WT2_CONVINFO
                    );
    }
    else
    {
        bShowConv = false;
    }

    bool bShowS1 = true;
    bool bShowS2 = true;
    bool bShowS3 = true;

    // Controller Mode 0-S1Num��S2Num��S3Num������ʾ
    APPEND_READONLY_DATA;
    long nVal6 = info->vSigValue.lValue;
    int nControllerMode = info->vSigValue.lValue;
    if (nControllerMode == 0)
    {
        bShowS1  = false;
        bShowS2  = false;
        bShowS3  = false;
    }

    // Slave1 State	Controller Mode Ϊ1ʱ��
    //0-	S1Num��ʾ
    //1-	S1Num����ʾ
    APPEND_READONLY_DATA;
    long nVal7 = info->vSigValue.lValue;
    if (nControllerMode == 1)
    {
        nSigVal = info->vSigValue.lValue;
        if (nSigVal == 0)
        {
            bShowS1  = true;
        }
        else if (nSigVal == 1)
        {
            bShowS1  = false;
        }
    }

    // Slave2 State	Controller Mode Ϊ1ʱ��
    //0-	S2Num��ʾ
    //1-	S2Num����ʾ
    APPEND_READONLY_DATA;
    long nVal8 = info->vSigValue.lValue;
    if (nControllerMode == 1)
    {
        nSigVal = info->vSigValue.lValue;
        if (nSigVal == 0)
        {
            bShowS2  = true;
        }
        else if (nSigVal == 1)
        {
            bShowS2  = false;
        }
    }

    // Slave3 State	Controller Mode Ϊ1ʱ��
    //0-	S3Num��ʾ
    //1-	S3Num����ʾ
    APPEND_READONLY_DATA;
    long nVal9 = info->vSigValue.lValue;
    if (nControllerMode == 1)
    {
        nSigVal = info->vSigValue.lValue;
        if (nSigVal == 0)
        {
            bShowS3  = true;
        }
        else if (nSigVal == 1)
        {
            bShowS3  = false;
        }
    }

    // S1Num
    APPEND_READONLY_DATA;
    long nVal10 = info->vSigValue.lValue;
    if ( bShowS1 )
    {
        addTableRow_moduleNum(
                    info,
                    WT2_SLAVE1INFO
                    );
    }

    // S2Num
    APPEND_READONLY_DATA;
    long nVal11 = info->vSigValue.lValue;
    if ( bShowS2 )
    {
        addTableRow_moduleNum(
                    info,
                    WT2_SLAVE2INFO
                    );
    }
    //TRACEDEBUG("===============666info<%d>\n",info->stHead);
    // S3Num
    APPEND_READONLY_DATA;
    long nVal12 = info->vSigValue.lValue;
    if ( bShowS3 )
    {
        addTableRow_moduleNum(
                    info,
                    WT2_SLAVE3INFO
                    );
    }
    
    //INV
    APPEND_READONLY_DATA;//hhy
    long nVal13 = info->vSigValue.lValue;//����
    //TRACEDEBUG("info<%d>",nVal13);
    if ( bShowInv )
    {
        TRACEDEBUG("WdgFP1Module::ShowData-----packing line INV");//hhy15
        addTableRow_moduleNum(
                    info,
                    WT2_INVINFO
                    );
//        TRACEDEBUG("WdgFP1Module::iSigValueType<%d>,iFormat<%d>,vSigValue<%d>,cEnumText<%d>,cSigUnit<%d>",
//                   info->iSigValueType,
//                   info->iFormat,
//                   info->vSigValue,
//                   info->cEnumText,
//                   info->cSigUnit);

//strSigName = QString(info->cSigName)+":"+
//        strSigVal + QString(info->cSigUnit);
    }

    APPEND_READONLY_DATA;
    if (ui->tableWidget->rowCount() >= m_nSelRow)
    {
        ui->tableWidget->selectRow( m_nSelRow );
        SET_STYLE_SCROOLBAR( m_nRows+1, m_nSelRow+1 );
    }

    TRACEDEBUG( "WdgFP1Module::ShowData() readed data "
                "rect<%d> sol<%d> conv<%d> "
                "bShowS1<%d> bShowS2<%d> bShowS3<%d>"
                "nVal1<%d>,nVal2<%d>,nVal3<%d>,nVal4<%d>,nVal5<%d>,nVal6<%d>,nVal7<%d>,nVal8<%d>,nVal9<%d>,nVal10<%d>,nVal11<%d>,nVal12<%d>,nVal13<%d>",
                bShowRect,
                bShowSol,
                bShowConv,
                bShowS1,
                bShowS2,
                bShowS3,
                nVal1,nVal2,nVal3,nVal4,nVal5,nVal6,nVal7,nVal8,nVal9,nVal10,nVal11,nVal12,nVal13
                );
}

void WdgFP1Module::addTableRow_moduleNum(const PACK_INFO* info,
                                         enum WIDGET_TYPE wt,
                                         bool bReadOnly)
{
    //bReadOnly Ĭ��Ϊfalse
    if (NULL == info)
    {
        TRACEDEBUG( "WdgFP1Module::addTableRow_moduleNum NULL == info" );
        return;
    }

    ++m_nRows;
    m_mapW2nd[m_nRows] = wt;
    QString strSigName;
    if ( bReadOnly )
    {
        m_mapNum[m_nRows]  = 0;
        QString strSigVal  =
                varValue2String(info->iSigValueType,
                                info->iFormat,
                                info->vSigValue,
                                info->cEnumText);
        strSigName = QString(info->cSigName)+":"+
                strSigVal + QString(info->cSigUnit);
        //TRACEDEBUG("11name<%d> strSigVal<%d>\n",info->cSigName,strSigVal);//hhy
    }
    else
    {
        int nSigVal = info->vSigValue.lValue;
        m_mapNum[m_nRows]  = nSigVal;
        strSigName = QString(info->cSigName)+":"+
                QString::number(nSigVal) + " >";
        TRACEDEBUG("22name<%d> nSigVal<%d>",info->cSigName,nSigVal);//hhy
    }

    QTableWidgetItem* item = ui->tableWidget->item(m_nRows,0);
    item->setText( strSigName );
}

QString WdgFP1Module::varValue2String(int nSigValueType,
                                      int iFormat,
                                      VAR_VALUE vSigValue,
                                      const char* strEnumText)
{
    QString strValue;
    switch ( nSigValueType )
    {
    case VAR_LONG:
        strValue = QString::number( vSigValue.lValue );
        break;

    case VAR_FLOAT:
        strValue = QString::number(vSigValue.fValue, FORMAT_DECIMAL);
        break;

    case VAR_UNSIGNED_LONG:
        strValue = QString::number( vSigValue.ulValue );
        break;

    case VAR_ENUM:
        strValue = QString( strEnumText );
        break;

    default:
        break;
    }

    return strValue;
}
//�ź������źŽ�����������
bool WdgFP1Module::appendReadOnlyItem(PACK_INFO*& info,
                                      PACK_INFO* infoEnd,
                                      const int nDataNum)
{
    static int nData = 0;
    //TRACEDEBUG("WdgFP1Module::appendReadOnlyItem<%s> disp<%d>", info->cSigName, info->stSpecialID2Info.iDispCtrl );
    //TRACEDEBUG("WdgFP1Module::appendReadOnlyItem info<%04x> infoEnd<%04x> nDataNum<%d> nData<%d>", info, infoEnd, nDataNum, nData );
    if (info >= infoEnd)
    {
        //TRACEDEBUG("WdgFP1Module::appendReadOnlyItem info >= infoEnd return" );
        nData = 0;
        return false;
    }
    //TRACEDEBUG("===========================info->iEquipID:%d",info->iEquipID);//HHY
    for (; info<infoEnd; ++info)
    {
        if (-2 == info->iEquipID)//-2 vaild
        {
            nData++;
            //TRACEDEBUG("WdgFP1Module::appendReadOnlyItem<%s> disp<%d>", info->cSigName, info->stSpecialID2Info.iDispCtrl );
            if (1 == info->stSpecialID2Info.iDispCtrl)
            {
                //Îª1ŸÍÌíŒÓÒ»ÐÐ
                if( g_ConvFlag)
                { 
   
                  if(info->iSigID !=41020001 && info->iSigID !=41020002)   //pooja hide rect info for DC-DC
                  {
                    addTableRow_moduleNum(info, WT1_MODULE, true);
//                TRACEDEBUG("WdgFP1Module::appendReadOnlyItem<%s> iEquipID<%d> iSigID<%d> iSigValueType<%d> stHead<%d> ",
//                           info->cSigName,
//                           info->iEquipID,
//                           info->iSigID,
//                           info->iSigValueType,
//                           info->stHead);//hhy
                 }
               }
               else
                {
                 
                  addTableRow_moduleNum(info, WT1_MODULE, true);
//                TRACEDEBUG("WdgFP1Module::appendReadOnlyItem<%s> iEquipID<%d> iSigID<%d> iSigValueType<%d> stHead<%d> ",
//                           info->cSigName,
//                           info->iEquipID,
//                           info->iSigID,
//                           info->iSigValueType,
//                           info->stHead);//hhy
                    
                }
                  
          
            }
        }
        else
        {
            nData++;
            break;
        }
    }
    //TRACEDEBUG("WdgFP1Module::appendReadOnlyItem info<%04x>", info );

    return true;
}

void WdgFP1Module::timerEvent(QTimerEvent* event)
{
    if(event->timerId() == m_timerId)
    {
        Refresh();
    }
}

void WdgFP1Module::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}
//��wt�뵱ǰ�кŷ��͵�goToBaseWindow
void WdgFP1Module::keyPressEvent(QKeyEvent* keyEvent)
{

    int iSelectRow = ui->tableWidget->selectedRow();
    TRACEDEBUG( "+++++++++++++++++++++WdgFP1Module::keyPressEvent row",m_mapNum[iSelectRow]);
    switch ( keyEvent->key() )
    {
    case Qt::Key_Return:
    case Qt::Key_Enter:
    {
        if (m_mapNum[iSelectRow] > 0)
        {
            //TRACEDEBUG( "WdgFP1Module::keyPressEvent goto widget" );
           // TRACEDEBUG( "==================wt<%d> row<%d>",WIDGET_TYPE(m_mapW2nd[iSelectRow]),iSelectRow);
            emit goToBaseWindow( WIDGET_TYPE(m_mapW2nd[iSelectRow]) );
        }
    }
        break;

    case Qt::Key_Down:
    {
          if(iSelectRow < m_nRows)
          {
              iSelectRow ++;
          }
          else
          {
              iSelectRow = 0;
          }
          ui->tableWidget->setCurrentCell (iSelectRow,0);
    }
        break;

    case Qt::Key_Up:
    {
          if(iSelectRow > 0)
          {
              iSelectRow --;
          }
          else
          {
              iSelectRow = m_nRows;
          }
          ui->tableWidget->setCurrentCell (iSelectRow,0);
    }
        break;

    default:
        break;
    }

    return QWidget::keyPressEvent(keyEvent);
}

void WdgFP1Module::sltTableKeyPress(int key)
{
    Q_UNUSED( key );
    TRACEDEBUG( "WdgFP1Module::sltTableKeyPress" );
    m_timeElapsedKeyPress.restart();
}

void WdgFP1Module::on_tableWidget_itemSelectionChanged()
{
    SET_STYLE_SCROOLBAR( m_nRows+1,
                         ui->tableWidget->selectedRow()+1 );
}
