/******************************************************************************
文件名：    Wdg2Table.h
功能：      第二层界面 表格
    WT2_RECTINFO, 模块信息页
    WT2_SOLINFO,
    WT2_CONVINFO,
    WT2_SLAVE1INFO,
    WT2_SLAVE2INFO,
    WT2_SLAVE3INFO,
            WT2_ACT_ALARM, 告警个数页
            WT2_HIS_ALARM,
            WT2_EVENT_LOG,
            WT2_INVENTORY  产品信息页
作者：      刘金煌
创建日期：   2013年5月9日
最后修改日期：
    2013年08月08日 增加 converter
    2014年01月21日 模块信息超过100条刷新数据的同时选择行会变慢
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "Wdg2Table.h"
#include "ui_Wdg2Table.h"

#include <QKeyEvent>
#include "common/pubInclude.h"
#include "common/global.h"
#include "config/configparam.h"
#include "config/PosTableWidget.h"
#include "equipWidget/Wdg3Table.h"

static QColor clrCommNormal(0, 0, 0, 0);
static QColor clrCommAbnormal(255, 0, 0, 255);

int     Wdg2Table::ms_nRectInv = 0;
MOD_INV Wdg2Table::ms_RectInv[128];

int     Wdg2Table::ms_nSlaveRectInv = 0;
MOD_INV Wdg2Table::ms_SlaveRectInv[180];

int     Wdg2Table::ms_nSMI2CInv = 0;
MOD_INV Wdg2Table::ms_SMI2CInv[80];

int     Wdg2Table::ms_nRS485Inv = 0;
MOD_INV Wdg2Table::ms_RS485Inv[66];

int     Wdg2Table::ms_nConvInv = 0;
MOD_INV Wdg2Table::ms_ConvInv[60];

int     Wdg2Table::ms_nLiBattInv = 0;
MOD_INV Wdg2Table::ms_LiBattInv[69];

int     Wdg2Table::ms_nSolConvInv = 0;
MOD_INV Wdg2Table::ms_SolConvInv[24];

int     Wdg2Table::ms_nInverInv = 0;
MOD_INV Wdg2Table::ms_InverInv[24];

#define CASE_MODULE_ITEM \
    case WT2_RECTINFO: \
    case WT2_SOLINFO: \
    case WT2_CONVINFO: \
    case WT2_SLAVE1INFO: \
    case WT2_SLAVE2INFO: \
    case WT2_SLAVE3INFO: \
    case WT2_INVINFO   //hhy

Wdg2Table::Wdg2Table(enum WIDGET_TYPE wt, QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::Wdg2Table)
{
    ui->setupUi(this);
    TRACEDEBUG("Wdg2Table::Wdg2Table::second floor");
    m_wt      = wt;
    InitWidget();
    InitConnect();

    m_timerId  = 0;
    m_cmdItem.CmdType   = CT_READ;
    m_nPageIdx = 1;
    m_nPages   = 1;
}

Wdg2Table::~Wdg2Table()
{
    delete ui;
}

void Wdg2Table::InitWidget()
{
    SET_GEOMETRY_WIDGET( this );
    SET_STYLE_LABEL_ENTER;
    SET_BACKGROUND_WIDGET( "Wdg2Table",PosBase::strImgBack_Line_Title );

    TRACELOG1( "Wdg2Table::InitWidget()" );
    //设置背景画面
    ui->label_enter->setVisible( false );
    ui->tableWidget->clearItems();
    //把qt定义的screenID传给m_cmdItem.ScreenID，后用于比较与app端的screenID
#define SET_TABLE_RECT(screenID, func) \
    m_cmdItem.ScreenID = screenID; \
    SET_TABLEWDG_STYLE( tableWidget, TT_NOT_TITLE_NOT_SCROOL ); \
    ui->tableWidget->setGeometry( \
                PosTableWidget::xNotTitle, \
                PosTableWidget::yModule, \
                PosTableWidget::widthHasScrool, \
                PosTableWidget::heightHeader+PosTableWidget::rowHeight*PosTableWidget::rowsPerpageTitle \
                ); \
    ui->tableWidget->horizontalHeader()->setHidden( false ); \
    func;

    QTableWidgetItem *item = NULL;
    TRACEDEBUG("Wdg2Table::InitWidget()---m_wt<%d>",m_wt);//hhy
    switch (m_wt)
    {
    case WT2_RECTINFO:
    {
        TRACEDEBUG("Wdg2Table::packing RECT\n");//hhy
        SET_TABLE_RECT(
                    SCREEN_ID_Wdg2Table_RectInfo,
                    setRectTableHeader()
                    );

//        m_cmdItem.ScreenID = SCREEN_ID_Wdg2Table_RectInfo;
//        SET_TABLEWDG_STYLE( tableWidget, TT_NOT_TITLE_NOT_SCROOL );
//        ui->tableWidget->horizontalHeader()->setHidden( false );
//        setRectTableHeader();
    }
        break;

    case WT2_SOLINFO:
    {
        TRACEDEBUG("QT===================getdata WT2_SOLINFO");
        SET_TABLE_RECT(
                    SCREEN_ID_Wdg2Table_SolInfo,
                    setSolTableHeader()
                    );

#ifdef TEST_GUI
        m_nSigNum = 1;
        setMPPTTableRow( m_nSigNum );
        // row item
        item = ui->tableWidget->item(0, 0);
        item->setBackground( clrCommNormal );
        item->setText( "S1" );

        item = ui->tableWidget->item(0, 1);
        item->setBackground( clrCommNormal );
        item->setText( "78.2" );

        item = ui->tableWidget->item(0, 2);
        item->setBackground( clrCommNormal );
        item->setText( "20.5" );
#endif
    }
        break;

     case WT2_INVINFO://hhy
    {
        TRACEDEBUG("send the iscreenID to APP");//hhy
        TRACEDEBUG("===================getdata WT2_INVINFO");
        SET_TABLE_RECT(
                    SCREEN_ID_Wdg2Table_InvInfo,
                    setInvTableHeader()
                    );
//kvv change
        m_nSigNum = 1;
        setRectTableRow( m_nSigNum );
        // row item
        item = ui->tableWidget->item(0, 0);
        item->setBackground( clrCommNormal );
        item->setText( "Invt1" );

        item = ui->tableWidget->item(0, 1);
        item->setBackground( clrCommNormal );
        item->setText( "80.2" );

        item = ui->tableWidget->item(0, 2);
        item->setBackground( clrCommNormal );
        item->setText( "OFF" );
    }
        break;

    case WT2_CONVINFO:
    {
        SET_TABLE_RECT(
                    SCREEN_ID_Wdg2Table_ConvInfo,
                    setConvTableHeader()
                    );
#ifdef TEST_GUI
        m_nSigNum = 1;
        setRectTableRow( m_nSigNum );
        // row item
        item = ui->tableWidget->item(0, 0);
        item->setBackground( clrCommNormal );
        item->setText( "Con1" );

        item = ui->tableWidget->item(0, 1);
        item->setBackground( clrCommNormal );
        item->setText( "80.2" );

        item = ui->tableWidget->item(0, 2);
        item->setBackground( clrCommNormal );
        item->setText( "OFF" );
#endif
    }
        break;

    case WT2_SLAVE1INFO:
    {
        SET_TABLE_RECT(
                    SCREEN_ID_Wdg2Table_S1RectInfo,
                    setSlaveRectTableHeader()
                    );
    }
        break;

    case WT2_SLAVE2INFO:
    {
        SET_TABLE_RECT(
                    SCREEN_ID_Wdg2Table_S2RectInfo,
                    setSlaveRectTableHeader()
                    );
    }
        break;

    case WT2_SLAVE3INFO:
    {
        SET_TABLE_RECT(
                    SCREEN_ID_Wdg2Table_S3RectInfo,
                    setSlaveRectTableHeader()
                    );
#ifdef TEST_GUI
        m_nSigNum = 1;
        setRectTableRow( m_nSigNum );
        // row item
        item = ui->tableWidget->item(0, 0);
        item->setBackground( clrCommNormal );
        item->setText( "S3# 120" );

        item = ui->tableWidget->item(0, 1);
        item->setBackground( clrCommNormal );
        item->setText( "10.8" );

        item = ui->tableWidget->item(0, 2);
        item->setBackground( clrCommNormal );
        item->setText( "ON" );

        ui->tableWidget->selectRow( 0 );
#endif
    }
        break;

    case WT2_ACT_ALARM:
    {
        m_cmdItem.ScreenID = SCREEN_ID_Wdg2Table_ActAlmNum;
        ui->label_enter->setVisible( true );
        ui->label_enter->setStyleSheet(
            "background-color: rgba(77,77,77, 0);"
            "color: rgb(241, 241, 241);"
            );
        SET_TABLEWDG_STYLE( tableWidget, TT_YES_TITLE_NOT_SCROOL ); // 1列
        ui->tableWidget->clearSelection();
        ui->tableWidget->clearFocus();
        ui->tableWidget->setRowCount( 3 );
        ui->verticalScrollBar->setVisible(false);
        for (int i=0; i<3; ++i)
        {
            item = new QTableWidgetItem;
            item->setText( "" );
            ui->tableWidget->setItem(i, 0, item);
            ui->tableWidget->setRowHeight(i, TABLEWDG_ROW_HEIGHT);
        }
#ifdef TEST_GUI
        item = ui->tableWidget->item(0, 0);
        item->setText( "Observation:1" );

        item = ui->tableWidget->item(1, 0);
        item->setText( "Major:1" );

        item = ui->tableWidget->item(2, 0);
        item->setText( "Critical:1" );
#endif
    }
        break;
    // -- 历史告警不要个数页
    case WT2_HIS_ALARM:
    {
        ui->verticalScrollBar->setVisible(false);
        m_cmdItem.ScreenID = SCREEN_ID_Wdg2Table_HisAlmNum;
        ui->label_enter->setVisible( true );
        SET_TABLEWDG_STYLE( tableWidget, TT_YES_TITLE_NOT_SCROOL ); // 1列
        ui->tableWidget->clearSelection();
        ui->tableWidget->clearFocus();
        ui->tableWidget->setRowCount( 3 );
        for (int i=0; i<3; ++i)
        {
            item = new QTableWidgetItem;
            item->setText( "" );
            ui->tableWidget->setItem(i, 0, item);
            ui->tableWidget->setRowHeight(i, TABLEWDG_ROW_HEIGHT);
        }
#ifdef TEST_GUI
        item = ui->tableWidget->item(0, 0);
        item->setText( "Observation:2" );

        item = ui->tableWidget->item(1, 0);
        item->setText( "Major:2" );

        item = ui->tableWidget->item(2, 0);
        item->setText( "Critical:2" );
#endif
    }
        break;

    case WT2_EVENT_LOG:
    {
        m_cmdItem.ScreenID = SCREEN_ID_Wdg2Table_EventLog;
    }
        break;

    case WT2_INVENTORY:
    {
        m_cmdItem.ScreenID = SCREEN_ID_Wdg2Table_Rect_Inv;
        SET_BACKGROUND_WIDGET( "Wdg2Table",PosBase::strImgBack_Line);
        SET_TABLEWDG_STYLE( tableWidget, TT_NOT_TITLE_YES_SCROOL );
        ui->tableWidget->setRowCount( TABLEWDG_ROWS_PERPAGE_NOTITLE );
        for (int i=0; i<TABLEWDG_ROWS_PERPAGE_NOTITLE; ++i)
        {
            item = new QTableWidgetItem;
            item->setText( "" );
            ui->tableWidget->setItem(i, 0, item);
            ui->tableWidget->setRowHeight(i, TABLEWDG_ROW_HEIGHT);
        }
//#ifdef TEST_GUI
        item = ui->tableWidget->item(0, 0);
        item->setText( "1" );

        item = ui->tableWidget->item(1, 0);
        item->setText( "Rect #1" );

        item = ui->tableWidget->item(2, 0);
        item->setText( "R482900" );

        item = ui->tableWidget->item(3, 0);
        item->setText( "01130605211" );

        item = ui->tableWidget->item(4, 0);
        item->setText( "A00" );

        item = ui->tableWidget->item(5, 0);
        item->setText( "V1.00" );
//#endif
    }
        break;

    default:
        //TRACELOG1("Wdg2Table::InitWidget() default wt<%d>", m_wt);
        break;
    }
}

void Wdg2Table::testGUI()
{
        m_pData = g_dataBuff;
        PACK_MODINFO* info = (PACK_MODINFO*)m_pData;
        info->stHead.iScreenID = m_cmdItem.ScreenID;
        info->iModuleNum = 2;

        int sigIdx = 0;
        MODINFO *modInfo = &(info->ModInfo[sigIdx]);
        modInfo->iEquipID = 11;
        strcpy(modInfo->cEqName, "#1");
        int iSigID   = 101;
        int iSigType = 201;
        modInfo->EachMod[0].iSigID = iSigID;
        modInfo->EachMod[0].iSigType = iSigID;
        modInfo->EachMod[0].vSigValue.lValue = 0;

        modInfo->EachMod[1].iSigID = iSigID;
        modInfo->EachMod[1].iSigType = iSigID;
        modInfo->EachMod[1].iFormat = 1;
        modInfo->EachMod[1].vSigValue.fValue = 1.1;

        modInfo->EachMod[2].iSigID = iSigID;
        modInfo->EachMod[2].iSigType = iSigID;
        strcpy(modInfo->EachMod[2].cEnumText, "OFF");
        //modInfo->EachMod[2].iFormat = 1;
        //modInfo->EachMod[2].vSigValue.fValue = 2;

        sigIdx++;

        modInfo = &(info->ModInfo[sigIdx]);
        modInfo->iEquipID = 12;
        strcpy(modInfo->cEqName, "#2");
        iSigID++;
        iSigType++;
        modInfo->EachMod[0].iSigID = iSigID;
        modInfo->EachMod[0].iSigType = iSigID;
        modInfo->EachMod[0].vSigValue.lValue = 0;

        modInfo->EachMod[1].iSigID = iSigID;
        modInfo->EachMod[1].iSigType = iSigID;
        modInfo->EachMod[1].iFormat = 1;
        modInfo->EachMod[1].vSigValue.fValue = 2.2;

        modInfo->EachMod[2].iSigID = iSigID;
        modInfo->EachMod[2].iSigType = iSigID;
        strcpy(modInfo->EachMod[2].cEnumText, "OFF");

        ShowData( m_pData );
}

void Wdg2Table::InitConnect()
{
    connect( &m_QTimerLight, SIGNAL(timeout()),
             this, SLOT(sltTimerHandler()) );
    switch (m_wt)
    {
    CASE_MODULE_ITEM:
    {
        connect( ui->tableWidget, SIGNAL(sigTableKeyPress(int)),
                 this, SLOT(sltTableKeyPress(int)) );
    }
        break;

    default:
        break;
    }
}

void Wdg2Table::Enter(void* param)
{
    //进入第二层界面 回车之后获取数据
    Q_UNUSED( param );
    TRACEDEBUG("===================enter param m_wt:%d",m_wt);
    INIT_VAR;
    m_wt = *(int*)param;
    TRACELOG1( "Wdg2Table::Enter(void* param) m_wt<%d>", m_wt );
    m_nSigNum   = 0;
    m_nPageIdx = 1;
    m_nPages   = 1;
    m_pData = NULL;

    ms_nRectInv      = 0;
    ms_nSolConvInv   = 0;
    ms_nConvInv      = 0;
    ms_nRS485Inv     = 0;
    ms_nSMI2CInv     = 0;
    ms_nLiBattInv    = 0;
    ms_nSlaveRectInv = 0;
    ms_nInverInv = 0;

    m_CtrlLightInfo.iEquipID = -1;
    m_CtrlLightInfo.bSendCmd = false;

#define ENTER_MODULE \
    m_CtrlLightInfo.timeElapsed.restart(); \
    m_QTimerLight.start( TIMER_UPDATE_DATA_INTERVAL/2 ); \
    {\
    ui->tableWidget->clearItems(); \
    m_timeElapsedKeyPress.restart(); \
    m_bEnterFirstly = true; \
    ENTER_GET_DATA; \
    ui->tableWidget->setFocus(); \
    ui->tableWidget->selectRow( 0 ); \
    SET_STYLE_SCROOLBAR( m_nSigNum, 1 ); \
    }

    switch (m_wt)
    { // module
    case WT2_RECTINFO:
    {
#ifdef TEST_GUI
        m_CtrlLightInfo.timeElapsed.restart();
        m_QTimerLight.start( TIMER_UPDATE_DATA_INTERVAL/2 );
        {
            ui->tableWidget->clearItems();
            m_timeElapsedKeyPress.restart();
            m_bEnterFirstly = true;
            ENTER_FIRSTLY;
            testGUI();
            ui->tableWidget->setFocus();
            ui->tableWidget->selectRow( 0 );
            SET_STYLE_SCROOLBAR( m_nSigNum, 1 );
        }
#else
        ENTER_MODULE;
#endif
    }
        break;

    case WT2_SOLINFO:
    {
#ifdef TEST_GUI
        ENTER_FIRSTLY;
#else
        ENTER_MODULE;
#endif
    }
        break;

    case WT2_CONVINFO:
    {
#ifdef TEST_GUI
        ENTER_FIRSTLY;
#else
        ENTER_MODULE;
#endif
    }
        break;

    case WT2_SLAVE1INFO:
    {
#ifdef TEST_GUI
        ENTER_FIRSTLY;
#else
        ENTER_MODULE;
#endif
    }
        break;

    case WT2_SLAVE2INFO:
    {
#ifdef TEST_GUI
        ENTER_FIRSTLY;
#else
        ENTER_MODULE;
#endif
    }
        break;

    case WT2_SLAVE3INFO:
    {
#ifdef TEST_GUI
        ENTER_FIRSTLY;
#else
        ENTER_MODULE;
#endif
    }
        break;

    case WT2_ACT_ALARM:
    {
        m_nSigNum = 0;
#ifdef TEST_GUI
    ENTER_FIRSTLY;
#else
    ENTER_GET_DATA;
#endif
        ui->label_enter->setText( QObject::tr("Active Alarms") );
    }
        break;

    case WT2_HIS_ALARM:
    {
        ui->label_enter->setText( QObject::tr("Alarm History") );
        emit goToBaseWindow( WIDGET_TYPE(WT3_HIS_ALARM) );
        //ENTER_GET_DATA;
    }
        break;

    case WT2_EVENT_LOG:
    {
        ENTER_GET_DATA;
    }
        break;

    case WT2_INVENTORY:
    {
        m_CtrlLightInfo.timeElapsed.restart();
        this->setFocus();
        //clearFocus();
        ui->tableWidget->clearFocus();
        ui->tableWidget->clearSelection();

        static bool bFirstEnter = true;
        if (bFirstEnter)
        {
            InitWidget();
            bFirstEnter = false;
        }

        ms_showingWdg = this;
        g_bSendCmd    = true;
        void *pData = NULL;
        for (int nScreenID=SCREEN_ID_Wdg2Table_Rect_Inv;
             nScreenID<=MAX_SCREEN_ID_INV;
             ++nScreenID)
        {
            m_cmdItem.ScreenID = nScreenID;
            TRACELOG1( "Wdg2Table::Enter data_getDataSync nScreenID<%0x>", nScreenID );
            if ( !data_getDataSync(&m_cmdItem, &pData) )
            {
                //Frank Wu,20151012, for happening segment fault some times
                if(m_cmdItem.ScreenID != ((Head_t*)pData)->iScreenID)
                {
                    continue;
                }

                setInvDataMem( pData );
            }
        } // end for
        SET_STYLE_SCROOLBAR(m_nSigNum, m_nPageIdx);
        ShowData( NULL );
        m_timerId = startTimer( TIMER_UPDATE_DATA_INTERVAL*15 );
        m_QTimerLight.start( TIMER_UPDATE_DATA_INTERVAL/2 );
    }
        break;
        //hhy
    case WT2_INVINFO:
    {
    #ifdef TEST_GUI
        ENTER_FIRSTLY;
    #else
        ENTER_MODULE;
    #endif
    }
    break;
    default:
        break;
    }

    m_bEnterFirstly = false;
}

void Wdg2Table::Leave()
{
    LEAVE_WDG( "Wdg2Table" );

    if ( m_QTimerLight.isActive() )
    {
        m_QTimerLight.stop();
    }

    switch ( m_wt )
    {
    CASE_MODULE_ITEM:
    case WT2_INVENTORY:
    {
        sendCmdCtrlLight(
                    m_CtrlLightInfo.screenID,
                    -1,
                    MODULE_LIGHT_TYPE_ON,
                    m_CtrlLightInfo.moduleType
                    );
    }
        break;

    default:
        break;
    }
    this->deleteLater ();
}

void Wdg2Table::Refresh()
{
    if (m_wt == WT2_INVENTORY)
    {
        static void *pData = NULL;
        static bool bFlag = false;
        bFlag = !bFlag;
        if (bFlag)
        {
            m_cmdItem.ScreenID = ( ++(m_cmdItem.ScreenID)>
                                   MAX_SCREEN_ID_INV ?
                        SCREEN_ID_Wdg2Table_Rect_Inv:
                        m_cmdItem.ScreenID );
            //TRACEDEBUG( "Wdg2Table::Refresh() data_sendCmd ScreenID<%06x>", m_cmdItem.ScreenID );
            if ( ERRDS_HAVEDATA_TOREAD==data_sendCmd(&m_cmdItem) )
            {
                bFlag = false;
            }
        }
        else
        {
            /* 后1S接收数据 */
            if ( !data_recvData(&pData) )
            {
                setInvDataMem( pData );

                //TRACEDEBUG( "Wdg2Table::Refresh() data_recvData readed data ScreenID<%06x>", m_cmdItem.ScreenID );
                ShowData( NULL );
            }
            else
            {
                pData = NULL;
                //TRACEDEBUG( "Wdg2Table::Refresh() data_recvData no data ScreenID<%06x>", m_cmdItem.ScreenID );
            }
        }
    }
    else
    {
        REFRESH_DATA_PERIODICALLY(
                    m_cmdItem.ScreenID,
                    "Wdg2Table"
                    );
    }
}

void Wdg2Table::ShowData(void* pData)
{
    TRACEDEBUG( "Wdg2Table::ShowData(void* pData) %x", pData );
    QTableWidgetItem* item = NULL;
    TRACEDEBUG("m_wt: %d\n",m_wt);
    switch (m_wt)
    {
    CASE_MODULE_ITEM:
    {
        m_pData = pData;
        if ( !pData )
        {
            return;
        }
        // 防止一直刷新，按键查看项时出现卡顿
        if (m_timeElapsedKeyPress.elapsed() >
                TIME_ELAPSED_KEYPRESS ||
                m_bEnterFirstly)
        {
            setModuleItem( pData );
        }
    }
        break;

    case WT2_ACT_ALARM:
    {
        m_pData = pData;
        if ( !pData )
        {
            return;
        }

        PACK_ALMNUM* info = (PACK_ALMNUM*)pData;
        item = ui->tableWidget->item(0, 0);
        item->setText( QObject::tr("Observation") + ":" +
                       QString::number(info->OANum) );

        item = ui->tableWidget->item(1, 0);
        item->setText( QObject::tr("Major") + ":" +
                       QString::number(info->MANum) );

        item = ui->tableWidget->item(2, 0);
        item->setText( QObject::tr("Critical") + ":" +
                       QString::number(info->CANum) );
        m_nSigNum = info->OANum + info->MANum + info->CANum;
    }
        break;

    case WT2_HIS_ALARM:
        break;

    case WT2_EVENT_LOG:
        break;

    case WT2_INVENTORY:
    {
        setInvItem();
    }
        break;

    default:
        break;
    }
}

/////////////////////////////////
//    module
void Wdg2Table::setModuleItem(void *pData)
{
    TRACEDEBUG("===============come in here");
    QTableWidgetItem* item = NULL;
    int nSelRow = ui->tableWidget->selectedRow();
    PACK_MODINFO* info = (PACK_MODINFO*)pData;
    int nSigNum = info->iModuleNum;//行数量
    //TRACEDEBUG("=============stHead<%d> iModuleNum<%d>",
               //info->stHead,info->iModuleNum);
    /*
    //打印太阳能模块的前10条信息（30）

    for (int i=0; i<10; ++i)
    {
    MODINFO *my_modInfo = &(info->ModInfo[i]);//HHY

    TRACEDEBUG("++++++++++++++++++++++++cEqName<%s> iEquipID<%d> EachMod<%d>\n",
               my_modInfo->cEqName,
               my_modInfo->iEquipID,
               my_modInfo->EachMod[0]);//HHY
    //for (int j=0; j<1; ++j) {
    EACHMOD_INFO *my_eachMod = &(info->ModInfo[i].EachMod[0]);
//    TRACEDEBUG("________________________ iSigID<%d> iSigType<%d> cEnumText<%s>",
//               my_eachMod->iSigID,my_eachMod->iSigType,my_eachMod->cEnumText);
    //}
    }
    */
    MODINFO *my_modInfo = &(info->ModInfo[0]);//HHY
    TRACEDEBUG("======================line<%d> stHead<%d> m_wt<%d>",info->iModuleNum,info->stHead,m_wt);//HHY stHead is equal with screenID
    TRACEDEBUG("======================iEquipID<%d>",my_modInfo->iEquipID);//HHY
    if (nSigNum == 0)
    {
        ui->tableWidget->clearItems();
        return;
    }
    //只要信号不为零都处理
    if (m_nSigNum != nSigNum)
    {
        TRACEDEBUG("====================trace the signal");
        ui->tableWidget->clearItems();
        m_nSigNum = nSigNum;
        switch (m_wt)//按键传进来的
        {
        case WT2_RECTINFO:
        case WT2_CONVINFO:
        case WT2_SLAVE1INFO:
        case WT2_SLAVE2INFO:
        case WT2_SLAVE3INFO:
        {
            setRectTableRow( m_nSigNum );
            TRACEDEBUG("===============================setRectTableRow");
        }
            break;

        case WT2_SOLINFO:
        {
            setMPPTTableRow( m_nSigNum );//设置界面样式及写入固定参数
            TRACEDEBUG("===============================setMPPTTableRow");
        }
            break;

        case WT2_INVINFO:
        {
            setInvTableRow( m_nSigNum );
            TRACEDEBUG("===============================setInvTableRow");
        }
            break;

        default:
            break;
        }
    }
    TRACEDEBUG( "Wdg2Table::setModuleItem()SigNum<%d>", m_nSigNum );
    m_nSigNum = (m_nSigNum>MAXNUM_MODULE ?
                     MAXNUM_MODULE:m_nSigNum);
    nSelRow = (nSelRow>=m_nSigNum-1 ?
                     (m_nSigNum-1):nSelRow);
    int iFormat = 0;
    long nState = 0;
    for (int i=0; i<m_nSigNum; ++i)
    {
        MODINFO *modInfo = &(info->ModInfo[i]);
        // 在EachMod中第一个是通讯状态，0-正常；非0-通讯中断。
        nState = modInfo->EachMod[0].vSigValue.lValue;
        //TRACEDEBUG("==================if needed to show nState<%d>",nState);HHY
        if ( nState )
        {
            item = ui->tableWidget->item(i, 0);
            item->setBackground( clrCommAbnormal );

            item = ui->tableWidget->item(i, 1);
            item->setBackground( clrCommAbnormal );

            item = ui->tableWidget->item(i, 2);
            item->setBackground( clrCommAbnormal );

            TRACEDEBUG( "Wdg2Table::setModuleItem() module<%d> state<%d> is not communicated",
                        i, nState );
        }
        else
        {
            TRACEDEBUG("==========================set the background");
            item = ui->tableWidget->item(i, 0);
            item->setBackground( clrCommNormal );

            item = ui->tableWidget->item(i, 1);
            item->setBackground( clrCommNormal );

            item = ui->tableWidget->item(i, 2);
            item->setBackground( clrCommNormal );
        }
        // index
        item = ui->tableWidget->item(i, 0);
        item->setText( modInfo->cEqName );
        TRACEDEBUG("============================cEqName<%s> iEquipID<%d> EachMod<%d> WT2_SOLINFO<%d>\n",
                   modInfo->cEqName,
                   modInfo->iEquipID
                   ,modInfo->EachMod,
                   WT2_SOLINFO);//HHY

        if (m_wt == WT2_SOLINFO)
        {
            TRACEDEBUG("+++++++++++++++++++++++++++send the solar data");//HHY
            // No. Vin(V) Iout(A) 2format
            item = ui->tableWidget->item(i, 1);
            iFormat = modInfo->EachMod[1].iFormat;
            item->setText( QString::number(
                               modInfo->EachMod[1].vSigValue.fValue,
                               FORMAT_DECIMAL)
                           );
            iFormat = modInfo->EachMod[2].iFormat;
            item = ui->tableWidget->item(i, 2);
            item->setText( QString::number(
                               modInfo->EachMod[2].vSigValue.fValue,
                               FORMAT_DECIMAL)
                           );
        }
        else
        {
            // NO. Iout(A) State 1fomat
            item = ui->tableWidget->item(i, 1);
            iFormat = modInfo->EachMod[1].iFormat;
            item->setText( QString::number(
                               modInfo->EachMod[1].vSigValue.fValue,
                               FORMAT_DECIMAL)
                           );

            item = ui->tableWidget->item(i, 2);
            item->setText( modInfo->EachMod[2].cEnumText );
        }
        TRACEDEBUG( "Wdg2Table::setModuleItem() iFormat<%d>", iFormat );
    }

    ui->tableWidget->selectRow( nSelRow );
//    if ( (m_nRowSendCtrlCmd!=nSelRow) &&
//         m_timeElapsed.elapsed()>
//         TIME_ELAPSED_CTRL_LIGHT )
//    {
//        sendCmdCtrlLight_flash();
//        m_nRowSendCtrlCmd = nSelRow;
//    }
    SET_STYLE_SCROOLBAR( m_nSigNum, nSelRow+1 );
}

#define SET_HEADER_STYLE \
    ui->tableWidget->horizontalHeader()->setStyleSheet( \
                "QHeaderView::section {" \
                "background-color:#" LIGHT_COLOR_ENTER ";" \
                "}" \
                ); \
    ui->tableWidget->horizontalHeader()->setFixedHeight( PosTableWidget::heightHeader );


// set module table header
void Wdg2Table::setRectTableHeader()
{
    SET_HEADER_STYLE;

    // column
    ui->tableWidget->setColumnCount(3);
    ui->tableWidget->setColumnWidth(0, PosTableWidget::widthHeadRect1);
    ui->tableWidget->setColumnWidth(1, PosTableWidget::widthHeadRect2);
    ui->tableWidget->setColumnWidth(2, PosTableWidget::widthHeadRect3);
}

void Wdg2Table::setSolTableHeader()
{
    TRACEDEBUG("==================if come in solar tablehead\n");//hhy
    SET_HEADER_STYLE;
//    ui->tableWidget->horizontalHeader()->setWindowOpacity( 0 );
    // column
    ui->tableWidget->setColumnCount(3);
    ui->tableWidget->setColumnWidth(0, PosTableWidget::widthHeadSol1);
    ui->tableWidget->setColumnWidth(1, PosTableWidget::widthHeadSol2);
    ui->tableWidget->setColumnWidth(2, PosTableWidget::widthHeadSol3);
}

//hhy
void Wdg2Table::setInvTableHeader()
{
    TRACEDEBUG("==================if come in Inv tablehead\n");//hhy
    SET_HEADER_STYLE;

    ui->tableWidget->setColumnCount(3);
    ui->tableWidget->setColumnWidth(0, PosTableWidget::widthHeadInv1);
    ui->tableWidget->setColumnWidth(1, PosTableWidget::widthHeadInv2);
    ui->tableWidget->setColumnWidth(2, PosTableWidget::widthHeadInv3);
}
void Wdg2Table::setConvTableHeader()
{
    setRectTableHeader();
}

void Wdg2Table::setSlaveRectTableHeader()
{
    setRectTableHeader();
}

// set module table row
void Wdg2Table::setRectTableRow(int nRowCount)
{
    // header item
    QTableWidgetItem* item = NULL;
    item = new QTableWidgetItem;
    item->setText( QObject::tr("Index") );
    item->setTextAlignment( Qt::AlignCenter );
    item->setTextColor(QColor("white"));
    ui->tableWidget->setHorizontalHeaderItem(0, item);

    item = new QTableWidgetItem;
    item->setText( QObject::tr("Iout(A)") );
    item->setTextAlignment( Qt::AlignCenter );
    item->setTextColor(QColor("white"));
    ui->tableWidget->setHorizontalHeaderItem(1, item);

    item = new QTableWidgetItem;
    item->setText( QObject::tr("State") );
    item->setTextAlignment( Qt::AlignCenter );
    item->setTextColor(QColor("white"));
    ui->tableWidget->setHorizontalHeaderItem(2, item);

    // row
    ui->tableWidget->setRowCount( nRowCount );
    for (int i=0; i<nRowCount; ++i)
    {
        int j=0;
        item = new QTableWidgetItem;
        item->setText( "" );
        item->setTextAlignment( Qt::AlignLeft );
        ui->tableWidget->setItem(i, j, item);

        for (j=1; j<3; ++j)
        {
            item = new QTableWidgetItem;
            item->setTextAlignment( Qt::AlignCenter );
            item->setText( "" );
            ui->tableWidget->setItem(i, j, item);
        }
        ui->tableWidget->setRowHeight(i, TABLEWDG_ROW_HEIGHT );
    }
}

void Wdg2Table::setMPPTTableRow(int nRowCount)
{
    // header item
    QTableWidgetItem* item = NULL;
    item = new QTableWidgetItem;
    item->setText( QObject::tr("Index") );
    item->setTextAlignment( Qt::AlignLeft );
    item->setTextColor(QColor("white"));
    ui->tableWidget->setHorizontalHeaderItem(0, item);

    item = new QTableWidgetItem;
    item->setText( QObject::tr("Vin(V)") );
    item->setTextColor(QColor("white"));
    ui->tableWidget->setHorizontalHeaderItem(1, item);

    item = new QTableWidgetItem;
    item->setText( QObject::tr("Iout(A)") );
    item->setTextColor(QColor("white"));
    ui->tableWidget->setHorizontalHeaderItem(2, item);

    // row
    ui->tableWidget->setRowCount( nRowCount );
    for (int i=0; i<nRowCount; ++i)
    {
        int j=0;
        item = new QTableWidgetItem;
        item->setText( "" );
        item->setTextAlignment( Qt::AlignLeft );
        ui->tableWidget->setItem(i, j, item);

        for (j=1; j<3; ++j)
        {
            item = new QTableWidgetItem;
            item->setTextAlignment( Qt::AlignCenter );
            item->setText( "" );
            ui->tableWidget->setItem(i, j, item);
        }
        ui->tableWidget->setRowHeight(i, TABLEWDG_ROW_HEIGHT);
    }
}

void Wdg2Table::setInvTableRow(int nRowCount)
{
    // header item
    QTableWidgetItem* item = NULL;
    item = new QTableWidgetItem;
    item->setText( QObject::tr("Index") );
    item->setTextAlignment( Qt::AlignLeft );
    item->setTextColor(QColor("white"));
    ui->tableWidget->setHorizontalHeaderItem(0, item);

    item = new QTableWidgetItem;
    item->setText( QObject::tr("Iout(A)") );
    item->setTextColor(QColor("white"));
    ui->tableWidget->setHorizontalHeaderItem(1, item);

    item = new QTableWidgetItem;
    item->setText( QObject::tr("State") );
    item->setTextColor(QColor("white"));
    ui->tableWidget->setHorizontalHeaderItem(2, item);

    ui->tableWidget->setRowCount( nRowCount );
    for (int i=0; i<nRowCount; ++i)
    {
        int j=0;
        item = new QTableWidgetItem;
        item->setText( "" );
        item->setTextAlignment( Qt::AlignLeft );
        ui->tableWidget->setItem(i, j, item);

        for (j=1; j<3; ++j)
        {
            item = new QTableWidgetItem;
            item->setTextAlignment( Qt::AlignCenter );
            item->setText( "" );
            ui->tableWidget->setItem(i, j, item);
        }
        ui->tableWidget->setRowHeight(i, TABLEWDG_ROW_HEIGHT);
    }
}

/*
// 发控制命令
void Wdg2Table::sendCmdCtrlLight_flash()
{
#define SEND_CTRL_MODULE_LIGHT(screenID, lightType) \
    PACK_MODINFO* info = (PACK_MODINFO*)m_pData; \
    MODINFO modInfo = info->ModInfo[ui->tableWidget->selectedRow()]; \
    sendCmdCtrlLight( screenID, modInfo.iEquipID, \
                MODULE_LIGHT_TYPE_GREEN_FLASH, lightType );

    switch (m_wt)
    {
    case WT2_RECTINFO:
    {
        SEND_CTRL_MODULE_LIGHT(
                    SCREEN_ID_Wdg2Table_RectInfo,
                    MODULE_TYPE_RECT
                    );

//        PACK_MODINFO* info = (PACK_MODINFO*)m_pData;
//        MODINFO modInfo = info->ModInfo[nSelRow];

//        CmdItem cmdItem;
//        cmdItem.ScreenID = SCREEN_ID_Wdg2Table_RectInfo;
//        cmdItem.CmdType = CT_CTRL;
//        SetInformation* pSetinfo = &(cmdItem.setinfo);
//        pSetinfo->EquipID = modInfo.iEquipID;
//        pSetinfo->value.lValue = MODULE_LIGHT_TYPE_GREEN_FLASH;
//        pSetinfo->SigID   = MODULE_TYPE_RECT;

//        void *pData = NULL;
//        data_getDataSync(&cmdItem, &pData);
//        TRACELOG1( "Wdg2Table::sendCmdCtrlLight_flash() WT2_RECTINFO row<%d> cmdtype<%d> SigID<%d>",
//                   nSelRow,
//                   cmdItem.CmdType,
//                   pSetinfo->SigID
//                   );
    }
        break;

    case WT2_SOLINFO:
    {
        SEND_CTRL_MODULE_LIGHT(
                    SCREEN_ID_Wdg2Table_SolInfo,
                    MODULE_TYPE_MPPT
                    );
    }
        break;

    case WT2_CONVINFO:
    {
        SEND_CTRL_MODULE_LIGHT(
                    SCREEN_ID_Wdg2Table_ConvInfo,
                    MODULE_TYPE_CONV
                    );
    }
        break;

    case WT2_SLAVE1INFO:
    {
        SEND_CTRL_MODULE_LIGHT(
                    SCREEN_ID_Wdg2Table_S1RectInfo,
                    MODULE_TYPE_S1RECT
                    );
    }
        break;

    case WT2_SLAVE2INFO:
    {
        SEND_CTRL_MODULE_LIGHT(
                    SCREEN_ID_Wdg2Table_S2RectInfo,
                    MODULE_TYPE_S2RECT
                    );
    }
        break;

    case WT2_SLAVE3INFO:
    {
        SEND_CTRL_MODULE_LIGHT(
                    SCREEN_ID_Wdg2Table_S3RectInfo,
                    MODULE_TYPE_S3RECT
                    );
    }
        break;

    default:
            break;
    }
}
*/

/*
// 发退出控制命令
void Wdg2Table::sendCmdCtrlLight_ON()
{
#define SEND_CTRL_CMD_LIGHT_ESCAPE(screenID, moduleType) \
    sendCmdCtrlLight( \
                screenID, \
                -1, \
                MODULE_LIGHT_TYPE_ON, \
                moduleType \
    );

    switch (m_wt)
    {
    case WT2_RECTINFO:
    {
        SEND_CTRL_CMD_LIGHT_ESCAPE(
                    SCREEN_ID_Wdg2Table_RectInfo,
                    MODULE_TYPE_RECT
                    );
//            CmdItem cmdItem;
//            cmdItem.ScreenID = SCREEN_ID_Wdg2Table_RectInfo;
//            cmdItem.CmdType = CT_CTRL;
//            SetInformation* pSetinfo = &(cmdItem.setinfo);
//            pSetinfo->EquipID = -1;
//            pSetinfo->value.lValue = MODULE_LIGHT_TYPE_ON;
//            pSetinfo->SigID = MODULE_TYPE_RECT;

//            void *pData = NULL;
//            data_getDataSync(&cmdItem, &pData);
    }
        break;

    case WT2_SOLINFO:
    {
        SEND_CTRL_CMD_LIGHT_ESCAPE(
                    SCREEN_ID_Wdg2Table_SolInfo,
                    MODULE_TYPE_MPPT
                    );
    }
        break;

    case WT2_CONVINFO:
    {
        SEND_CTRL_CMD_LIGHT_ESCAPE(
                    SCREEN_ID_Wdg2Table_ConvInfo,
                    MODULE_TYPE_CONV
                    );
    }
        break;

    case WT2_SLAVE1INFO:
    {
        SEND_CTRL_CMD_LIGHT_ESCAPE(
                    SCREEN_ID_Wdg2Table_S1RectInfo,
                    MODULE_TYPE_S1RECT
                    );
    }
        break;

    case WT2_SLAVE2INFO:
    {
        SEND_CTRL_CMD_LIGHT_ESCAPE(
                    SCREEN_ID_Wdg2Table_S2RectInfo,
                    MODULE_TYPE_S2RECT
                    );
    }
        break;

    case WT2_SLAVE3INFO:
    {
        SEND_CTRL_CMD_LIGHT_ESCAPE(
                    SCREEN_ID_Wdg2Table_S3RectInfo,
                    MODULE_TYPE_S3RECT
                    );
    }
        break;

    default:
        break;
    }
}
*/

void Wdg2Table::sltScreenSaver()
{
    if (BasicWidget::ms_showingWdg == this)
    {
//        TRACEDEBUG( "Wdg2Table::sltScreenSaver()" );
//        sendCmdCtrlLight_ON();
    }
}

/////////////////////////////////
//    inventory
void Wdg2Table::setInvItem()
{
    TRACEDEBUG("=======================setInvItem");
    if (m_nSigNum < 1)
    {
        QTableWidgetItem* item = NULL;
        for (int i=0; i<TABLEWDG_ROWS_PERPAGE_NOTITLE; ++i)
        {
            item = new QTableWidgetItem;
            item->setText( "" );
            ui->tableWidget->setItem(i, 0, item);
            ui->tableWidget->setRowHeight(i, TABLEWDG_ROW_HEIGHT);
        }

        item = ui->tableWidget->item(2, 0);
        item->setTextAlignment( Qt::AlignCenter );
        item->setText( QObject::tr("No Data") );
        m_CtrlLightInfo.bSendCmd = false;
        return;
    }
    if (m_nPageIdx < 1)
    {
        m_nPageIdx = m_nSigNum;
    }
    else if (m_nPageIdx > m_nSigNum)
    {
        m_nPageIdx = 1;
    }
    SET_STYLE_SCROOLBAR(m_nSigNum, m_nPageIdx);

    TRACEDEBUG( "Wdg2Table::setInvItem m_nSigNum<%d> m_nPageIdx<%d> ms_nRectInv<%d> "
               "ms_nSolConvInv<%d> ms_nConvInv<%d> "
               "ms_nRS485Inv<%d> ms_nSMI2CInv<%d> "
               "ms_nLiBattInv<%d> ms_nSlaveRectInv<%d>",
               m_nSigNum,
               m_nPageIdx, ms_nRectInv,
               ms_nSolConvInv, ms_nConvInv,
               ms_nRS485Inv, ms_nSMI2CInv,
               ms_nLiBattInv, ms_nSlaveRectInv
               );
    MOD_INV* sigItem = NULL;
    int ScreenID = 0;
    if (m_nPageIdx <=
             ms_nRectInv)
    {
        ScreenID  = SCREEN_ID_Wdg2Table_Rect_Inv;
        sigItem   = &(ms_RectInv[m_nPageIdx-1]);
        TRACEDEBUG( "Wdg2Table::setInvItem 1" );
    }
    else if (m_nPageIdx <=
             ms_nRectInv+ms_nSlaveRectInv)
    {
        ScreenID  = SCREEN_ID_Wdg2Table_SlaveRect_Inv;
        sigItem   = &(ms_SlaveRectInv[m_nPageIdx-1-
             ms_nRectInv]);
        TRACEDEBUG( "Wdg2Table::setInvItem 2" );
    }
    else if (m_nPageIdx <=
             ms_nRectInv+ms_nSlaveRectInv+ms_nSMI2CInv)
    {
        ScreenID  = SCREEN_ID_Wdg2Table_SMI2C_Inv;
        sigItem   = &(ms_SMI2CInv[m_nPageIdx-1-
            (ms_nRectInv+ms_nSlaveRectInv)]);
        TRACEDEBUG( "Wdg2Table::setInvItem 3" );
    }
    else if (m_nPageIdx <=
             ms_nRectInv+ms_nSlaveRectInv+ms_nSMI2CInv+ms_nRS485Inv)
    {
        ScreenID  = SCREEN_ID_Wdg2Table_RS485_Inv;
        sigItem   = &(ms_RS485Inv[m_nPageIdx-1-
            (ms_nRectInv+ms_nSlaveRectInv+ms_nSMI2CInv)]);
        TRACEDEBUG( "Wdg2Table::setInvItem 4" );
    }
    else if (m_nPageIdx <=
             ms_nRectInv+ms_nSlaveRectInv+ms_nSMI2CInv+ms_nRS485Inv+ms_nConvInv)
    {
        ScreenID  = SCREEN_ID_Wdg2Table_Conv_Inv;
        sigItem   = &(ms_ConvInv[m_nPageIdx-1-
            (ms_nRectInv+ms_nSlaveRectInv+ms_nSMI2CInv+ms_nRS485Inv)]);
        TRACEDEBUG( "Wdg2Table::setInvItem 5" );
    }
    else if (m_nPageIdx <=
             ms_nRectInv+ms_nSlaveRectInv+ms_nSMI2CInv+ms_nRS485Inv+ms_nConvInv+ms_nLiBattInv)
    {
        ScreenID  = SCREEN_ID_Wdg2Table_LiBatt_Inv;
        sigItem   = &(ms_LiBattInv[m_nPageIdx-1-
            (ms_nRectInv+ms_nSlaveRectInv+ms_nSMI2CInv+ms_nRS485Inv+ms_nConvInv)]);
        TRACEDEBUG( "Wdg2Table::setInvItem 6" );
    }
    else if (m_nPageIdx <=
             ms_nRectInv+ms_nSlaveRectInv+ms_nSMI2CInv+ms_nRS485Inv+ms_nConvInv+ms_nSolConvInv)//hhy
    {
        ScreenID  = SCREEN_ID_Wdg2Table_SolConv_Inv;
        sigItem   = &(ms_SolConvInv[m_nPageIdx-1-
            (ms_nRectInv+ms_nSlaveRectInv+ms_nSMI2CInv+ms_nRS485Inv+ms_nConvInv+ms_nLiBattInv)]);
        TRACEDEBUG( "Wdg2Table::setInvItem 7" );
        TRACEDEBUG("***************ms_SolConvInv<%d>\n",ms_SolConvInv[0]);
    }
    else //hhy
    {
        ScreenID  = SCREEN_ID_Wdg2Table_Inver_Inv;
        sigItem   = &(ms_InverInv[m_nPageIdx-1-
            (ms_nRectInv+ms_nSlaveRectInv+ms_nSMI2CInv+ms_nRS485Inv+ms_nConvInv+ms_nLiBattInv+ms_nSolConvInv)]);
        TRACEDEBUG( "Wdg2Table::setInvItem 8" );
    }
    TRACELOG1( "Wdg2Table::setInvItem sigItem idx<%d> equipName<%s>"
               "serialNumber<%s> cPartNumber<%s> cPVer<%s> cSWver<%s> ScreenID<%06x>",
               m_nPageIdx, sigItem->cEquipName,
               sigItem->cSerialNumber, sigItem->cPartNumber,
               sigItem->cPVer, sigItem->cSWver,
               ScreenID
               );

    QTableWidgetItem* item = NULL;
    item = ui->tableWidget->item(0, 0);
    item->setTextAlignment( Qt::AlignLeft );
    item->setText( QString::number(m_nPageIdx) );

    item = ui->tableWidget->item(1, 0);
    item->setTextAlignment( Qt::AlignLeft );
    item->setText( QObject::tr("Name") + ":" + sigItem->cEquipName );

    item = ui->tableWidget->item(2, 0);
    item->setTextAlignment( Qt::AlignLeft );
    item->setText( QObject::tr("SN") + ":" + sigItem->cSerialNumber );

    item = ui->tableWidget->item(3, 0);
    item->setTextAlignment( Qt::AlignLeft );
    item->setText( QObject::tr("Number") + ":" + sigItem->cPartNumber );

    item = ui->tableWidget->item(4, 0);
    item->setTextAlignment( Qt::AlignLeft );
    item->setText( QObject::tr("Product Ver") + ":" + sigItem->cPVer );

    item = ui->tableWidget->item(5, 0);
    item->setTextAlignment( Qt::AlignLeft );
    item->setText( QObject::tr("SW Ver") + ":" + sigItem->cSWver );

    m_CtrlLightInfo.screenID   = ScreenID;
    m_CtrlLightInfo.iEqIDType  = sigItem->iEqIDType;
    m_CtrlLightInfo.iSigID     = sigItem->iSigID;;
    m_CtrlLightInfo.moduleType = MODULE_TYPE(
                getModuleType(sigItem->iEqIDType) );
    TRACEDEBUG( "Wdg2Table::setInvItem iEqIDType<%d> moduleType<%d> "
                "m_nPageIdx<%d> equipID<%d> oldEquipID<%d>",
                sigItem->iEqIDType, m_CtrlLightInfo.moduleType,
                m_nPageIdx, sigItem->iEquipID, m_CtrlLightInfo.iEquipID );
    if (m_CtrlLightInfo.iEquipID != sigItem->iEquipID)
    {
        m_CtrlLightInfo.iEquipID = sigItem->iEquipID;
        m_CtrlLightInfo.bSendCmd = true;
        m_CtrlLightInfo.timeElapsed.restart();
    }
}

void Wdg2Table::setInvDataMem(void* pData)
{
    PACK_INVINFO* info = (PACK_INVINFO*)pData;
    int nSigNum = info->iModuleNum;
    m_nSigNum  += nSigNum;
    int nScreenID = m_cmdItem.ScreenID;

    switch (nScreenID)
    {
    case SCREEN_ID_Wdg2Table_Rect_Inv:
        m_nSigNum  -= ms_nRectInv;
        ms_nRectInv = nSigNum;
        memcpy( ms_RectInv,
                &(info->ModuleInv[0]),
                nSigNum*sizeof(MOD_INV) );
        break;

    case SCREEN_ID_Wdg2Table_SlaveRect_Inv:
        m_nSigNum     -= ms_nSlaveRectInv;
        ms_nSlaveRectInv = nSigNum;
        memcpy( ms_SlaveRectInv,
                &(info->ModuleInv[0]),
                nSigNum*sizeof(MOD_INV) );
        break;

    case SCREEN_ID_Wdg2Table_SMI2C_Inv:
        m_nSigNum  -= ms_nSMI2CInv;
        ms_nSMI2CInv = nSigNum;
        memcpy( ms_SMI2CInv,
                &(info->ModuleInv[0]),
                nSigNum*sizeof(MOD_INV) );
        break;

    case SCREEN_ID_Wdg2Table_RS485_Inv:
        m_nSigNum   -= ms_nRS485Inv;
        ms_nRS485Inv = nSigNum;
        memcpy( ms_RS485Inv,
                &(info->ModuleInv[0]),
                nSigNum*sizeof(MOD_INV) );
        break;


    case SCREEN_ID_Wdg2Table_Conv_Inv:
        m_nSigNum   -= ms_nConvInv;
        ms_nConvInv = nSigNum;
        memcpy( ms_ConvInv,
                &(info->ModuleInv[0]),
                nSigNum*sizeof(MOD_INV) );
        break;

    case SCREEN_ID_Wdg2Table_LiBatt_Inv:
        m_nSigNum    -= ms_nLiBattInv;
        ms_nLiBattInv = nSigNum;
        memcpy( ms_LiBattInv,
                &(info->ModuleInv[0]),
                nSigNum*sizeof(MOD_INV) );
        break;

    case SCREEN_ID_Wdg2Table_SolConv_Inv:
        m_nSigNum    -= ms_nSolConvInv;
        ms_nSolConvInv = nSigNum;
        memcpy( ms_SolConvInv,
                &(info->ModuleInv[0]),
                nSigNum*sizeof(MOD_INV) );
        break;


    case SCREEN_ID_Wdg2Table_Inver_Inv:
        m_nSigNum    -= ms_nInverInv;
        ms_nInverInv = nSigNum;
        memcpy( ms_InverInv,
                &(info->ModuleInv[0]),
                nSigNum*sizeof(MOD_INV) );
        break;
    default:
        TRACELOG2( "Wdg2Table::setInvDataMem screenID<%d>", nScreenID );
        break;
    }
}

void Wdg2Table::timerEvent(QTimerEvent* event)
{
    if(event->timerId() == m_timerId)
    {
        Refresh();
    }
}

void Wdg2Table::sltTimerHandler()
{
    if ( m_CtrlLightInfo.bSendCmd &&
         m_CtrlLightInfo.timeElapsed.elapsed()>
         TIME_ELAPSED_CTRL_LIGHT )
    {
        TRACEDEBUG( "Wdg2Table::sltTimerHandler()" );
        switch (m_wt)
        {
        CASE_MODULE_ITEM:
        {
            PACK_MODINFO* info = (PACK_MODINFO*)m_pData;
            if (!info || info->stHead.iScreenID!=m_cmdItem.ScreenID)
                return;

            MODINFO* modInfo = &(info->ModInfo[
                                 ui->tableWidget->selectedRow()]
                                 );
            EACHMOD_INFO itemInfo = modInfo->EachMod[1];

            m_CtrlLightInfo.screenID   = m_cmdItem.ScreenID;
            m_CtrlLightInfo.iSigID     = itemInfo.iSigID;
            m_CtrlLightInfo.moduleType = MODULE_TYPE(
                        getModuleTypeByWt(m_wt) );
            if (m_CtrlLightInfo.iEquipID != modInfo->iEquipID)
            {
                m_CtrlLightInfo.iEquipID = modInfo->iEquipID;
            }
            else
            {
                m_CtrlLightInfo.bSendCmd = false;
            }
        }
        break;

        default:
            break;
        }

        TRACEDEBUG( "Wdg2Table::sltTimerHandler() send cmd ctrl light "
                    "screenID<%x> iEquipID<%d> SigID<%d>",
                    m_CtrlLightInfo.screenID, m_CtrlLightInfo.iEquipID, m_CtrlLightInfo.iSigID );
        if ( m_CtrlLightInfo.bSendCmd )
        {
            sendCmdCtrlLight( m_CtrlLightInfo.screenID,
                              m_CtrlLightInfo.iEquipID,
                              MODULE_LIGHT_TYPE_GREEN_FLASH,
                              m_CtrlLightInfo.moduleType
                              );
            m_CtrlLightInfo.bSendCmd = false;
        }
    }
}

void Wdg2Table::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void Wdg2Table::keyPressEvent(QKeyEvent* keyEvent)
{
    TRACEDEBUG( "Wdg2Table::keyPressEvent" );

    switch ( keyEvent->key() )
    {
    case Qt::Key_Return:
    case Qt::Key_Enter:
    {
        g_nEnterPageType = ENTER_PAGE_TYPE_KEY_ENT;
        //TRACELOG1( "Wdg2Table::keyPressEvent Key_Enter" );
        switch (m_wt)
        {
        case WT2_ACT_ALARM:
        {
#ifdef TEST_GUI
            Wdg3Table::ms_bEscapeFromHelp = false;
            emit goToBaseWindow( WIDGET_TYPE(WT3_ACT_ALARM) );
#else
            if (m_nSigNum > 0)
            {
                //int idxItem = ui->tableWidget->selectedRow();
                Wdg3Table::ms_bEscapeFromHelp = false;
                emit goToBaseWindow( WIDGET_TYPE(WT3_ACT_ALARM) );
            }
#endif
        }
            break;

//        case WT2_HIS_ALARM:
//        {
//            //int idxItem = ui->tableWidget->selectedRow();
//            emit goToBaseWindow( WIDGET_TYPE(WT3_HIS_ALARM) );
//        }
//            break;

        default:
            TRACELOG1("Wdg2Table Key_Enter default wt<%d>", m_wt);
            break;
        }
    }
        break;

    case Qt::Key_Escape:
    {
        g_nEnterPageType = ENTER_PAGE_TYPE_KEY_ESC;
        switch (m_wt)
        {
        CASE_MODULE_ITEM:
        {
//            sendCmdCtrlLight_ON();
            emit goToBaseWindow( WT1_MODULE );
        }
        break;

        case WT2_ACT_ALARM:
        case WT2_HIS_ALARM:
        case WT2_EVENT_LOG:
        {
            emit goToBaseWindow( WT1_ALARM );
        }
            break;

        case WT2_INVENTORY:
        {
            //if ( m_CtrlLightInfo.isModule )
//            {
//                sendCmdCtrlLight(
//                            m_CtrlLightInfo.screenID,
//                            -1,
//                            MODULE_LIGHT_TYPE_ON,
//                            m_CtrlLightInfo.moduleType
//                            );
//            }
            emit goToBaseWindow( WT1_INVENTORY );
        }
            break;

        default:
            break;
        }
    }
        break;

    case Qt::Key_Down:
    {
        TRACEDEBUG( "Wdg2Table::keyPressEvent Key_Down" );
        switch (m_wt)
        {
        CASE_MODULE_ITEM:
        {
            ui->tableWidget->selectRow( 0 );
            SET_STYLE_SCROOLBAR( m_nSigNum, 1 );
        }
            break;

        case WT2_INVENTORY:
            ++m_nPageIdx;
            ShowData( NULL );
            break;
        }
    }
        break;

    case Qt::Key_Up:
    {
        TRACEDEBUG( "Wdg2Table::keyPressEvent Key_Up" );
        switch (m_wt)
        {
        CASE_MODULE_ITEM:
        {
            int nRowCount = ui->tableWidget->rowCount();

            ui->tableWidget->selectRow( nRowCount-1 );
            SET_STYLE_SCROOLBAR( m_nSigNum, m_nSigNum );
        }
            break;

        case WT2_INVENTORY:
            --m_nPageIdx;
            ShowData( NULL );
            break;
        }
    }
        break;

    default:
        break;
    }

    return;
    //return QWidget::keyPressEvent(keyEvent);
}

void Wdg2Table::sltTableKeyPress(int key)
{
    TRACEDEBUG( "Wdg2Table::sltTableKeyPress" );
    switch (m_wt)
    {
    CASE_MODULE_ITEM:
    {
        m_timeElapsedKeyPress.restart();
        switch ( key )
        {
        case Qt::Key_Up:
        case Qt::Key_Down:
        {
            m_CtrlLightInfo.timeElapsed.restart();
            int nSelRow = ui->tableWidget->selectedRow();
            SET_STYLE_SCROOLBAR( m_nSigNum, nSelRow+1 );
        }
            break;
        }
    }
        break;
    }
}

void Wdg2Table::on_tableWidget_itemSelectionChanged()
{
    TRACEDEBUG( "Wdg2Table::on_tableWidget_itemSelectionChanged()" );
    switch (m_wt)
    {
    CASE_MODULE_ITEM:
    {
//        PACK_MODINFO* info = (PACK_MODINFO*)m_pData;
//        if ( !info )
//            return;
//        MODINFO* modInfo = &(info->ModInfo[
//                             ui->tableWidget->selectedRow()]
//                             );
//        EACHMOD_INFO itemInfo = modInfo->EachMod[1];

//        m_CtrlLightInfo.screenID   = m_cmdItem.ScreenID;
//        //m_CtrlLightInfo.iEqIDType  = actAlarmItem.iEqIDType;
//        m_CtrlLightInfo.iSigID     = itemInfo.iSigID;
//        m_CtrlLightInfo.moduleType = MODULE_TYPE(
//                    getModuleTypeByWt(m_wt) );
//        TRACEDEBUG( "Wdg2Table::on_tableWidget_itemSelectionChanged "
//                    "m_CtrlLightInfo.iEquipID<%d> modInfo->iEquipID<%d> moduleType<%d>",
//                    m_CtrlLightInfo.iEquipID, modInfo->iEquipID, m_CtrlLightInfo.moduleType );

//        if (m_CtrlLightInfo.iEquipID != modInfo->iEquipID)
//        {
//            m_CtrlLightInfo.iEquipID = modInfo->iEquipID;
            m_CtrlLightInfo.bSendCmd = true;
//        }
    }
    break;

    default:
        break;
    }
}
